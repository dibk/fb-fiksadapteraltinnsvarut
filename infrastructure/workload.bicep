param location string
param WebappName string
param backgroundWorkerName string
param rgSharedResources string
param aspName string
param privateDnsZoneName string
param vnetName string
param subnetName string
param connectivitySubnet string

resource appServicePlan 'Microsoft.Web/serverfarms@2021-01-15' existing = {
  name: aspName
  scope: resourceGroup(rgSharedResources)
}

resource backgroundWorker 'Microsoft.Web/sites@2021-01-15' = {
  name: backgroundWorkerName
  location: location
  identity: {
    type: 'SystemAssigned'
  }

  properties: {
    serverFarmId: appServicePlan.id
    httpsOnly: true
    clientAffinityEnabled: false
    virtualNetworkSubnetId: resourceId(rgSharedResources, 'Microsoft.Network/virtualNetworks/subnets', vnetName, connectivitySubnet)
    siteConfig: {
      appSettings: [
        {
          name: 'WEBSITE_TIME_ZONE'
          value: 'W. Europe Standard Time'
        }
        {
          name: 'WEBSITE_LOAD_ROOT_CERTIFICATES'
          value: '*'
        }
      ]
    }
  }
}

resource web 'Microsoft.Web/sites@2021-01-15' = {
  name: WebappName
  location: location
  identity: {
    type: 'SystemAssigned'
  }

  properties: {
    serverFarmId: appServicePlan.id
    httpsOnly: true
    clientAffinityEnabled: false
    virtualNetworkSubnetId: resourceId(rgSharedResources, 'Microsoft.Network/virtualNetworks/subnets', vnetName, connectivitySubnet)
    siteConfig: {
      appSettings: [
        {
          name: 'WEBSITE_TIME_ZONE'
          value: 'W. Europe Standard Time'
        }
        {
          name: 'WEBSITE_LOAD_ROOT_CERTIFICATES'
          value: '*'
        }
      ]
    }
  }
}

resource stagingSlot 'Microsoft.Web/sites/slots@2021-02-01' = {
  name: 'staging'
  parent: web
  location: location
  kind: 'app'
  identity: {
    type: 'SystemAssigned'
  }
  properties: {
    serverFarmId: appServicePlan.id
  }
}

var privateEndpointNameWeb = 'pe-${WebappName}'
var privateEndpointNameBackgroundWorker = 'pe-${backgroundWorkerName}'

resource privateEndpointWeb 'Microsoft.Network/privateEndpoints@2023-05-01' = {
  name: privateEndpointNameWeb
  location: location
  properties: {
    subnet: {
      id: resourceId(rgSharedResources, 'Microsoft.Network/virtualNetworks/subnets', vnetName, subnetName)
    }
    privateLinkServiceConnections: [
      {
        name: privateEndpointNameWeb
        properties: {
          groupIds: [ 'sites' ]
          privateLinkServiceId: web.id
        }
      }
    ]
  }
}

module addToPrivateDnsWeb 'AddToPrivateDns.bicep' = {
  name: 'addToPrivateDns-web'
  params: {
    privateDnsZoneName: privateDnsZoneName
    privateEndpointName: privateEndpointNameWeb
    appResourceGroupName: resourceGroup().name
    appName: WebappName
  }
  dependsOn: [ privateEndpointWeb ]
  scope: resourceGroup(rgSharedResources)
}

resource privateEndpointBackgroundWorker 'Microsoft.Network/privateEndpoints@2023-05-01' = {
  name: privateEndpointNameBackgroundWorker
  location: location
  properties: {
    subnet: {
      id: resourceId(rgSharedResources, 'Microsoft.Network/virtualNetworks/subnets', vnetName, subnetName)
    }
    privateLinkServiceConnections: [
      {
        name: privateEndpointNameBackgroundWorker
        properties: {
          groupIds: [ 'sites' ]
          privateLinkServiceId: backgroundWorker.id
        }
      }
    ]
  }
}

module addToPrivateDnsBackgroundWOrker 'AddToPrivateDns.bicep' = {
  name: 'addToPrivateDns-bgworker'
  params: {
    privateDnsZoneName: privateDnsZoneName
    privateEndpointName: privateEndpointNameBackgroundWorker
    appResourceGroupName: resourceGroup().name
    appName: backgroundWorkerName
  }
  dependsOn: [ privateEndpointBackgroundWorker ]
  scope: resourceGroup(rgSharedResources)
}
