﻿using KommIT.FIKS.AdapterAltinnSvarUt.Services.Altinn;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Altinn.Prefill;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Prefill;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.WS.AltinnPreFill;
using Moq;
using Serilog;
using Serilog.Events;
using System;
using System.Collections.Generic;
using System.ServiceModel;
using Xunit;
using Xunit.Abstractions;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Tests.Services.Prefill
{

    //public class PrefillPolicyTests
    //{

    //    private readonly ITestOutputHelper output;
    //    public PrefillPolicyTests(ITestOutputHelper output)
    //    {
    //        this.output = output;
    //    }

    //    [Fact]
    //    public void SendPrefill_Ok()
    //    {
    //        var prefillFormTask = BuildTestPrefillFormTask();
    //        var expectedResult = BuildTestReceipt();
    //        var logger = GetConsoleLogger(output);
    //        var client = new Mock<IPrefillClient>();
    //        client.Setup(r => r.SubmitAndInstantiatePrefilledFormTaskBasic(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<PrefillFormTask>(), It.IsAny<bool>(), It.IsAny<bool>(), It.IsAny<int?>(), It.IsAny<DateTime?>(), It.IsAny<string>()))
    //            .Returns(expectedResult);

    //        var factory = new Mock<IPrefillClientFactory>();
    //        factory.Setup(s => s.GetClient()).Returns(client.Object);

    //        var policyProvider = GetTestPolicyProvider();

    //        var altinnPrefillService = new AltinnPrefillService(logger, factory.Object, policyProvider);

    //        var result = altinnPrefillService.SubmitAndInstantiatePrefilledForm(prefillFormTask, DateTime.Now);

    //        Assert.Equal(expectedResult, result);
    //    }

    //    [Fact]
    //    public void SendPrefill_ThrowsTimeoutExceptionRetries1Time_thenSuccess()
    //    {
    //        var prefillFormTask = BuildTestPrefillFormTask();
    //        var expectedResult = BuildTestReceipt();

    //        var exception = new System.TimeoutException("This takes too long time");

    //        var logger = GetConsoleLogger(output);
    //        var client = new Mock<IPrefillClient>();
    //        client.SetupSequence(r => r.SubmitAndInstantiatePrefilledFormTaskBasic(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<PrefillFormTask>(), It.IsAny<bool>(), It.IsAny<bool>(), It.IsAny<int?>(), It.IsAny<DateTime?>(), It.IsAny<string>()))
    //            .Throws(exception).Returns(expectedResult);

    //        var factory = new Mock<IPrefillClientFactory>();
    //        factory.Setup(s => s.GetClient()).Returns(client.Object);

    //        var policyProvider = GetTestPolicyProvider();

    //        var altinnPrefillService = new AltinnPrefillService(logger, factory.Object, policyProvider);

    //        var result = altinnPrefillService.SubmitAndInstantiatePrefilledForm(prefillFormTask, DateTime.Now);

    //        client.Verify(s => s.SubmitAndInstantiatePrefilledFormTaskBasic(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<PrefillFormTask>(), It.IsAny<bool>(), It.IsAny<bool>(), It.IsAny<int?>(), It.IsAny<DateTime?>(), It.IsAny<string>())
    //            , Times.Exactly(2));
    //        Assert.Equal(expectedResult, result);
    //    }

    //    [Fact]
    //    public void SendPrefill_ThrowsServerTooBusyExceptionRetries2Times_thenSuccess()
    //    {
    //        var prefillFormTask = BuildTestPrefillFormTask();
    //        var expectedResult = BuildTestReceipt();

    //        var exception = new System.ServiceModel.ServerTooBusyException("There is currently too much for me to handle!");


    //        var logger = GetConsoleLogger(output);
    //        var client = new Mock<IPrefillClient>();
    //        client.SetupSequence(r => r.SubmitAndInstantiatePrefilledFormTaskBasic(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<PrefillFormTask>(), It.IsAny<bool>(), It.IsAny<bool>(), It.IsAny<int?>(), It.IsAny<DateTime?>(), It.IsAny<string>()))
    //            .Throws(exception).Throws(exception).Returns(expectedResult);

    //        var factory = new Mock<IPrefillClientFactory>();
    //        factory.Setup(s => s.GetClient()).Returns(client.Object);

    //        var policyConfiguration = new Mock<IPrefillPolicyConfigurationProvider>();
    //        policyConfiguration.Setup(s => s.RetryAttemptWaitSecondsStructure).Returns(new List<int>() { 1, 1 });
    //        var policyProvider = new PrefillPolicyProvider(policyConfiguration.Object);

    //        var altinnPrefillService = new AltinnPrefillService(logger, factory.Object, policyProvider);

    //        var result = altinnPrefillService.SubmitAndInstantiatePrefilledForm(prefillFormTask, DateTime.Now);

    //        client.Verify(s => s.SubmitAndInstantiatePrefilledFormTaskBasic(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<PrefillFormTask>(), It.IsAny<bool>(), It.IsAny<bool>(), It.IsAny<int?>(), It.IsAny<DateTime?>(), It.IsAny<string>())
    //            , Times.Exactly(3));
    //        Assert.Equal(expectedResult, result);
    //    }

    //    [Fact]
    //    public void SendPrefill_ThrowsServerTooBusyExceptionRetries1TimeMoreThanPolicy_thenFails()
    //    {
    //        var prefillFormTask = BuildTestPrefillFormTask();
    //        var expectedResult = BuildTestReceipt();

    //        var exception = new System.ServiceModel.ServerTooBusyException("There is currently too much for me to handle!");

    //        var logger = GetConsoleLogger(output);
    //        var client = new Mock<IPrefillClient>();
    //        client.SetupSequence(r => r.SubmitAndInstantiatePrefilledFormTaskBasic(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<PrefillFormTask>(), It.IsAny<bool>(), It.IsAny<bool>(), It.IsAny<int?>(), It.IsAny<DateTime?>(), It.IsAny<string>()))
    //            .Throws(exception).Throws(exception).Throws(exception).Returns(expectedResult);

    //        var factory = new Mock<IPrefillClientFactory>();
    //        factory.Setup(s => s.GetClient()).Returns(client.Object);

    //        var policyConfiguration = new Mock<IPrefillPolicyConfigurationProvider>();
    //        policyConfiguration.Setup(s => s.RetryAttemptWaitSecondsStructure).Returns(new List<int>() { 1, 1 });
    //        var policyProvider = new PrefillPolicyProvider(policyConfiguration.Object);

    //        var altinnPrefillService = new AltinnPrefillService(logger, factory.Object, policyProvider);

    //        Assert.Throws<System.ServiceModel.ServerTooBusyException>(() => altinnPrefillService.SubmitAndInstantiatePrefilledForm(prefillFormTask, DateTime.Now));

    //        client.Verify(s => s.SubmitAndInstantiatePrefilledFormTaskBasic(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<PrefillFormTask>(), It.IsAny<bool>(), It.IsAny<bool>(), It.IsAny<int?>(), It.IsAny<DateTime?>(), It.IsAny<string>())
    //            , Times.Exactly(3));
    //    }

    //    [Fact]
    //    public void SendPrefill_ThrowsFaultException()
    //    {
    //        var prefillFormTask = BuildTestPrefillFormTask();
    //        var expectedResult = BuildTestReceipt();

    //        var exception = new System.ServiceModel.FaultException("Something went wrong!");

    //        var logger = GetConsoleLogger(output);
    //        var client = new Mock<IPrefillClient>();
    //        client.Setup(r => r.SubmitAndInstantiatePrefilledFormTaskBasic(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<PrefillFormTask>(), It.IsAny<bool>(), It.IsAny<bool>(), It.IsAny<int?>(), It.IsAny<DateTime?>(), It.IsAny<string>()))
    //            .Throws(exception);

    //        var factory = new Mock<IPrefillClientFactory>();
    //        factory.Setup(s => s.GetClient()).Returns(client.Object);

    //        var policyProvider = GetTestPolicyProvider();

    //        var altinnPrefillService = new AltinnPrefillService(logger, factory.Object, policyProvider);


    //        Assert.Throws<System.ServiceModel.FaultException>(() => altinnPrefillService.SubmitAndInstantiatePrefilledForm(prefillFormTask, DateTime.Now));

    //        client.Verify(s => s.SubmitAndInstantiatePrefilledFormTaskBasic(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<PrefillFormTask>(), It.IsAny<bool>(), It.IsAny<bool>(), It.IsAny<int?>(), It.IsAny<DateTime?>(), It.IsAny<string>())
    //            , Times.Exactly(1));
    //    }

    //    [Fact]
    //    public void SendPrefill_ThrowsAltinFaultException()
    //    {
    //        var prefillFormTask = BuildTestPrefillFormTask();
    //        var expectedResult = BuildTestReceipt();

    //        var fault = new AltinnFault();
    //        fault.AltinnErrorMessage = "I am Altinn error message";
    //        fault.AltinnExtendedErrorMessage = "I am Altinn extended error message";
    //        fault.ErrorGuid = Guid.NewGuid().ToString();

    //        var faultReason = new FaultReason("Super important error");
    //        var faultCode = new FaultCode("Ich bin Fault");

    //        var exception = new FaultException<AltinnFault>(fault, faultReason, faultCode, "SOAP action ftw");
    //        var logger = GetConsoleLogger(output);
    //        var client = new Mock<IPrefillClient>();
    //        client.Setup(r => r.SubmitAndInstantiatePrefilledFormTaskBasic(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<PrefillFormTask>(), It.IsAny<bool>(), It.IsAny<bool>(), It.IsAny<int?>(), It.IsAny<DateTime?>(), It.IsAny<string>()))
    //            .Throws(exception);

    //        var factory = new Mock<IPrefillClientFactory>();
    //        factory.Setup(s => s.GetClient()).Returns(client.Object);

    //        var policyProvider = GetTestPolicyProvider();

    //        var altinnCorrespondenceService = new AltinnPrefillService(logger, factory.Object, policyProvider);


    //        Assert.Throws<System.ServiceModel.FaultException<AltinnFault>>(() => altinnCorrespondenceService.SubmitAndInstantiatePrefilledForm(prefillFormTask, DateTime.Now));

    //        client.Verify(s => s.SubmitAndInstantiatePrefilledFormTaskBasic(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<PrefillFormTask>(), It.IsAny<bool>(), It.IsAny<bool>(), It.IsAny<int?>(), It.IsAny<DateTime?>(), It.IsAny<string>())
    //            , Times.Exactly(1));
    //    }

    //    private IPrefillPolicyProvider GetTestPolicyProvider()
    //    {
    //        var policyConfiguration = new Mock<IPrefillPolicyConfigurationProvider>();
    //        policyConfiguration.Setup(s => s.RetryAttemptWaitSecondsStructure).Returns(new List<int>() { 1 });

    //        return new PrefillPolicyProvider(policyConfiguration.Object);
    //    }

    //    private PrefillFormTask BuildTestPrefillFormTask()
    //    {
    //        PrefillFormTaskBuilder prefillFormTaskBuilder = new PrefillFormTaskBuilder();
    //        prefillFormTaskBuilder.SetupPrefillFormTask("1234", 1, "reportee", "123456789", "123456789", "1234565789", 14);
    //        prefillFormTaskBuilder.AddPrefillForm("5555", 4444, "XML streng", "123456789");

    //        return prefillFormTaskBuilder.Build();
    //    }

    //    private ReceiptExternal BuildTestReceipt()
    //    {
    //        var retVal = new ReceiptExternal();

    //        retVal.ReceiptStatusCode = ReceiptStatusEnum.OK;
    //        retVal.ReceiptId = 1;
    //        retVal.ReceiptText = "OK";

    //        return retVal;
    //    }

    //    private ILogger GetConsoleLogger(ITestOutputHelper output)
    //    {
    //        ILogger logger = new LoggerConfiguration()
    //            .MinimumLevel.Verbose()
    //            .Enrich.FromLogContext()
    //            .WriteTo.TestOutput(output, LogEventLevel.Debug)
    //            .CreateLogger()
    //            .ForContext<PrefillPolicyTests>();
    //        Log.Logger = logger;

    //        return logger;

    //    }
    //}
}
