﻿using FluentAssertions;
using iTextSharp.text.pdf;
using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Prefill;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.Builders;
using System;
using System.Collections.Generic;
using Xunit;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Tests.Services.Prefill
{

    public class DistributionReceiptTests
    {
        [Fact]
        public void GenerateReceipt_specialCharacter_creationSuccessfull()
        {
            var naboGuids = new[] { new Guid(), new Guid(), new Guid(), new Guid(), new Guid() };
            var kvitterings = new KvitteringForNabovarselBuilder().Kvitterings(naboGuids);
            var prosjektnavn = "Testprosjekt 1&2";
            string kontaktperson = "Kåre# Kontakt %";
            string ansvarligSoeker = "Anne Ansvar";

            List<Attachment> attachments = new List<Attachment>();

            var a1 = new Attachment();
            a1.FileName = "situasjonsplan.pdf";
            a1.AttachmentType = "Situasjonsplan";
            attachments.Add(a1);
            var a2 = new Attachment();
            a2.FileName = "tegning#1234&.pdf";
            a2.AttachmentType = "TegningNyPlan";
            attachments.Add(a2);
            var a3 = new Attachment();
            a3.FileName = "#%&?.pdf";
            a3.AttachmentType = "Annet";
            attachments.Add(a3);


            var distribujonFormList = new List<DistributionForm>()
            {
                new DistributionForm()
                {
                    Id = naboGuids[0],
                    SubmitAndInstantiatePrefilled = new DateTime(2018,05,17,13,00,00),
                },
                new DistributionForm()
                {
                    Id = naboGuids[1],
                    SubmitAndInstantiatePrefilled = new DateTime(2018,05,17,13,10,00),

                },
                new DistributionForm()
                {
                    Id = naboGuids[2],
                    SubmitAndInstantiatePrefilled = new DateTime(2018,05,17,13,20,00),
                },
                new DistributionForm()
                {
                    Id = naboGuids[3],
                    SubmitAndInstantiatePrefilled = new DateTime(2018,05,17,13,30,00),
                },
                new DistributionForm()
                {
                    Id = naboGuids[4],
                    SubmitAndInstantiatePrefilled = new DateTime(2018,05,17,13,30,00),
                }
           };

            var rapport = new DistributionReceiptService().MakeReceipt(kvitterings, distribujonFormList, prosjektnavn, ansvarligSoeker, kontaktperson, attachments);

            var pdfreport = new PdfReader(rapport.AttachmentData);
            pdfreport.Should().NotBeNull();
        }

        [Fact]
        public void GeneratePlanReceipt_specialCharacter_creationSuccessfull()
        {
            var naboGuids = new[] { new Guid(), new Guid(), new Guid(), new Guid(), new Guid() };
            var kvitterings = new KvitteringForNabovarselBuilder().Kvitterings(naboGuids);
            var prosjektnavn = "Testprosjekt 1&2";
            string kontaktperson = "Kåre# Kontakt %";
            string forslagsttiller = "Frida Forslag";

            List<Attachment> attachments = new List<Attachment>();

            var a1 = new Attachment();
            a1.FileName = "situasjonsplan.pdf";
            a1.AttachmentType = "Situasjonsplan";
            attachments.Add(a1);
            var a2 = new Attachment();
            a2.FileName = "tegning#1234&.pdf";
            a2.AttachmentType = "TegningNyPlan";
            attachments.Add(a2);
            var a3 = new Attachment();
            a3.FileName = "#%&?.pdf";
            a3.AttachmentType = "Annet";
            attachments.Add(a3);


            var distribujonFormList = new List<DistributionForm>()
            {
                new DistributionForm()
                {
                    Id = naboGuids[0],
                    SubmitAndInstantiatePrefilled = new DateTime(2018,05,17,13,00,00),
                },
                new DistributionForm()
                {
                    Id = naboGuids[1],
                    SubmitAndInstantiatePrefilled = new DateTime(2018,05,17,13,10,00),

                },
                new DistributionForm()
                {
                    Id = naboGuids[2],
                    SubmitAndInstantiatePrefilled = new DateTime(2018,05,17,13,20,00),
                },
                new DistributionForm()
                {
                    Id = naboGuids[3],
                    SubmitAndInstantiatePrefilled = new DateTime(2018,05,17,13,30,00),
                },
                new DistributionForm()
                {
                    Id = naboGuids[4],
                    SubmitAndInstantiatePrefilled = new DateTime(2018,05,17,13,30,00),
                }
           };

            var rapport = new DistributionPlanReceiptService().MakeReceipt(kvitterings, distribujonFormList, prosjektnavn, forslagsttiller, kontaktperson, attachments);

            var pdfreport = new PdfReader(rapport.AttachmentData);
            pdfreport.Should().NotBeNull();
        }
    }
}
