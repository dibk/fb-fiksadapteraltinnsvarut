using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using iTextSharp.text.pdf;
using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Services;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Prefill;
using KommIT.FIKS.AdapterAltinnSvarUt.Tests.Services.Builders;
using Xunit;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Tests.Services.Prefill
{
   public class KvitteringForNabovarselTest
    {
        
        [Fact(DisplayName = "DistributionReceiptService.MakeReceipt()")]
        public void MakeReceiptTest()
        {
            var naboGuids = new []{new Guid(),new Guid(),new Guid(),new Guid()};
            var kvitterings = new KvitteringForNabovarselBuilder().Kvitterings(naboGuids);
            var prosjektnavn = "Prosjektnavn";
            string kontaktperson = "kontaktperson";
            string ansvarligSoeker = "";

            List<Attachment> attachments = new List<Attachment>();

            var tempAttach = new Attachment();
            tempAttach.FileName = "situasjonsplan.pdf";
            tempAttach.AttachmentType = "Situasjonsplan";
            attachments.Add(tempAttach);
            tempAttach.FileName = "tegning1234.pdf";
            tempAttach.AttachmentType = "TegningNyPlan";
            attachments.Add(tempAttach);

            var distribujonFormList = new List<DistributionForm>()
            {
                new DistributionForm()
                {
                    Id = naboGuids[0],
                    SubmitAndInstantiatePrefilled = new DateTime(2018,05,17,13,00,00),
                },
                new DistributionForm()
                {
                    Id = naboGuids[1],
                    SubmitAndInstantiatePrefilled = new DateTime(2018,05,17,13,10,00),

                },
                new DistributionForm()
                {
                    Id = naboGuids[2],
                    SubmitAndInstantiatePrefilled = new DateTime(2018,05,17,13,20,00),
                },
                new DistributionForm()
                {
                    Id = naboGuids[3],
                    SubmitAndInstantiatePrefilled = new DateTime(2018,05,17,13,30,00),
                },
           };

            var rapport =new DistributionReceiptService().MakeReceipt(kvitterings, distribujonFormList, prosjektnavn, ansvarligSoeker, kontaktperson, attachments);
            
            var pdfreport = new PdfReader(rapport.AttachmentData);
            pdfreport.Should().NotBeNull();
        }
        [Fact(DisplayName = "KvitteringForNabovarsel save PDF", Skip = "to save PDF")]
        public void MakeReceiptSavePDFTest()
        {
            var naboGuids = new []{Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid() };
            var kvitterings = new KvitteringForNabovarselBuilder().Kvitterings(naboGuids);
            List<Attachment> attachments = new List<Attachment>();
            string kontaktperson = "kontaktperson";

            var tempAttach = new Attachment();
            tempAttach.FileName = "situasjonsplan.pdf";
            tempAttach.AttachmentTypeName = "Situasjonsplan";
            attachments.Add(tempAttach);
            tempAttach.FileName = "tegning1234.pdf";
            tempAttach.AttachmentTypeName = "TegningNyPlan";
            attachments.Add(tempAttach);

            var distribujonFormList = new List<DistributionForm>()
            {
                new DistributionForm()
                {
                    Id = naboGuids[0],
                    SubmitAndInstantiatePrefilled = new DateTime(2018,05,17,13,00,00),
                    DistributionStatus = DistributionStatus.submittedPrefilled,
                },
                new DistributionForm()
                {
                    Id = naboGuids[1],
                    SubmitAndInstantiatePrefilled = new DateTime(2018,05,17,14,10,00),
                    DistributionStatus = DistributionStatus.submittedPrefilled,

                },
                new DistributionForm()
                {
                    Id = naboGuids[2],
                    SubmitAndInstantiatePrefilled = new DateTime(2018,05,17,15,20,00),
                    DistributionStatus = DistributionStatus.error,
                },
                new DistributionForm()
                {
                    Id = naboGuids[3],
                    SubmitAndInstantiatePrefilled = new DateTime(2018,05,17,16,30,00),
                    DistributionStatus = DistributionStatus.submittedPrefilled,
                },
           };

            var rapport =new DistributionReceiptService().MakeReceipt(kvitterings, distribujonFormList, "prosjektnavn", "Ansvarlig S�ker", kontaktperson, attachments);
            
            //save PDF to c:\Temp\KvitteringForNabovarsel.pdf
            var filename = Path.GetFileNameWithoutExtension("KvitteringForNabovarsel");
            var path = Path.Combine(@"C:\", "Temp");
            Directory.CreateDirectory(path);
            var testFile = Path.Combine(path, string.Concat(filename,".pdf"));
            System.IO.File.WriteAllBytes(testFile, rapport.AttachmentData);
            File.Exists(testFile).Should().BeTrue();
        }
    }
}
