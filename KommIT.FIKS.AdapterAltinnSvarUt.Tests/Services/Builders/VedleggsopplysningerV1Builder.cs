﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using no.kxml.skjema.dibk.vedlegg;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Tests.Services.Builders
{
    class VedleggsopplysningerV1Builder
    {
        private readonly VedleggsopplysningerType _form;

        public VedleggsopplysningerV1Builder()
        {
            _form = new VedleggsopplysningerType();
        }

        public VedleggsopplysningerType Build()
        {
            return _form;
        }

        public VedleggsopplysningerV1Builder Vedlegg(string ifcValidationId = null, string vedleggstype = null, DateTime? tegningsdato = null, string tegningsnr = null)
        {
            _form.vedlegg = new[] { new VedleggType() };

            if (ifcValidationId != null)
                _form.vedlegg[0].ifcValidationId = ifcValidationId;

            if (vedleggstype != null)
                _form.vedlegg[0].vedleggstype = vedleggstype;

            if (tegningsdato != null)
                _form.vedlegg[0].tegningsdato = tegningsdato;

            if (tegningsnr != null)
                _form.vedlegg[0].tegningsnr = tegningsnr;

            return this;
        }

    }
}
