﻿using no.kxml.skjema.dibk.gjenpartnabovarselV2;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Tests.Services.Builders
{
    class GjenpartnabovarselV2Builder
    {

        private readonly no.kxml.skjema.dibk.gjenpartnabovarselV2.NabovarselType _form;

        public GjenpartnabovarselV2Builder()
        {
            _form = new NabovarselType();

        }

        public NabovarselType Build()
        {
            return _form;
        }
        public GjenpartnabovarselV2Builder EiendomType(string knr, string gnr, string bnr, string fnr, string snr, string kommunenavn, string bolignummer, bool eiendomByggested = true)
        {
            if (!eiendomByggested) return this;
            _form.eiendomByggested = new[]
            {
                new EiendomType
                {
                    eiendomsidentifikasjon =new MatrikkelnummerType
                    {
                        kommunenummer = knr,
                        gaardsnummer = gnr,
                        bruksnummer = bnr,
                        festenummer = fnr,
                        seksjonsnummer = snr
                    },
                    adresse = new EiendommensAdresseType()
                    {
                        adresselinje1 = "Nygate 33",
                        postnr = "3825",
                        poststed = "Lunde"
                    },
                    bygningsnummer = "123456789",
                    bolignummer = bolignummer,
                    kommunenavn = kommunenavn
                }
            };

            return this;
        }

        // beskrivelseAvTiltak
        public GjenpartnabovarselV2Builder BeskrivelseAvTiltak(string tiltakTypeKodeverdi, string tiltakKodebeskrivelse, string tiltaksformaalKode1, string tiltaksformaalkodebeskrivelse1, string tiltaksformaalKode2, string tiltaksformaalkodebeskrivelse2,
            bool beskrivPlanlagtFormaal = true)
        {
            _form.beskrivelseAvTiltak = new[]
            {
                new TiltakType
                {
                    foelgebrev ="Det søkes om bygging av en ny fritidsbolig og et laftet lite anneks. Annekset skal bygges først og benyttes som \"arbeidsbrakke\" mens hovedhytta bygges."
                }
            };

            if (string.IsNullOrEmpty(tiltaksformaalKode1) && string.IsNullOrEmpty(tiltaksformaalkodebeskrivelse1))
            {
                _form.beskrivelseAvTiltak[0].bruk = new FormaalType()
                {
                    tiltaksformaal = new[]{
                            new KodeType()
                            {
                                kodeverdi = tiltaksformaalKode1,
                                kodebeskrivelse = tiltaksformaalkodebeskrivelse1
                            }
                        }
                };
            }
            if (!string.IsNullOrEmpty(tiltaksformaalKode1) && !string.IsNullOrEmpty(tiltaksformaalkodebeskrivelse1))
            {
                _form.beskrivelseAvTiltak[0].bruk = new FormaalType()
                {
                    tiltaksformaal = new[]{
                            new KodeType()
                            {
                                kodeverdi = tiltaksformaalKode1,
                                kodebeskrivelse = tiltaksformaalkodebeskrivelse1
                            }
                        }
                };
            }
            if (!string.IsNullOrEmpty(tiltaksformaalKode2) && !string.IsNullOrEmpty(tiltaksformaalkodebeskrivelse2))
            {
                _form.beskrivelseAvTiltak[0].bruk = new FormaalType()
                {
                    tiltaksformaal = new[]{
                            new KodeType()
                            {
                                kodeverdi = tiltaksformaalKode2,
                                kodebeskrivelse = tiltaksformaalkodebeskrivelse2
                            }
                        }
                };
            }


            if (tiltakTypeKodeverdi != null)
            {
                _form.beskrivelseAvTiltak[0].type = new[]
                {
                    new KodeType
                    {
                        kodeverdi = tiltakTypeKodeverdi,
                        kodebeskrivelse = tiltakKodebeskrivelse
                    }
                };
            }

            if (beskrivPlanlagtFormaal)
                _form.beskrivelseAvTiltak[0].bruk.beskrivPlanlagtFormaal = "Ny fritidsbolig med laftet anneks";
            return this;
        }
        //Dispensasjon
        public GjenpartnabovarselV2Builder Dispensasjon(string dispensasjonTypeKode1, string dispensasjonTypekodebeskrivelse1,string beskrivelse, string begrunnelse)
        {
            _form.dispensasjon = new[]
            {
                new DispensasjonType(),
            };

            _form.dispensasjon[0].dispensasjonstype = new KodeType()
            {
                kodeverdi = dispensasjonTypeKode1,

            };
            if (!string.IsNullOrEmpty(dispensasjonTypekodebeskrivelse1))
                _form.dispensasjon[0].dispensasjonstype.kodebeskrivelse = dispensasjonTypekodebeskrivelse1;


            if (!string.IsNullOrEmpty(beskrivelse))
                _form.dispensasjon[0].beskrivelse = beskrivelse;

            if (!string.IsNullOrEmpty(begrunnelse))
                _form.dispensasjon[0].begrunnelse = begrunnelse;
            return this;
        }
    }
}
