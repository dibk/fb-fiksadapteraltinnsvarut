﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using no.kxml.skjema.dibk.igangsettingstillatelseV2;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Tests.Services.Builders
{
    class IgangsettingstillatelseV2Builder
    {
        private readonly IgangsettingstillatelseType _form;

        public IgangsettingstillatelseV2Builder()
        {
            _form = new IgangsettingstillatelseType();
        }

        public IgangsettingstillatelseType Build()
        {
            return _form;
        }

        public IgangsettingstillatelseV2Builder Arbeidsplasser(bool? beroererArbeidsplasser)
        {
            if (beroererArbeidsplasser.HasValue)
            {
                _form.beroererArbeidsplasser = beroererArbeidsplasser.Value;
            }
            return this;
        }

        public IgangsettingstillatelseV2Builder SoknadsDelerAvTiltaken(bool? gjelderHeleTiltaket, string delAvTiltaket = null)
        {
            if (gjelderHeleTiltaket.HasValue)
            {
                _form.gjelderHeleTiltaket = gjelderHeleTiltaket.Value;
                _form.delAvTiltaket = delAvTiltaket;
            }
            return this;
        }

        public IgangsettingstillatelseV2Builder KommunensSaksnummer(string saksaar, string sakssekvensnummer)
        {
            _form.kommunensSaksnummer = new SaksnummerType();
            if (!string.IsNullOrEmpty(saksaar)) _form.kommunensSaksnummer.saksaar = saksaar;
            if (!string.IsNullOrEmpty(sakssekvensnummer)) _form.kommunensSaksnummer.sakssekvensnummer = sakssekvensnummer;
            return this;
        }

        // eiendomByggested
        public IgangsettingstillatelseV2Builder EiendomType(string knr, string gnr, string bnr, string fnr, string snr, string kommunenavn, string bolignummer)
        {
            _form.eiendomByggested = new[]
            {
                new EiendomType
                {
                    kommunenavn = kommunenavn,

                    eiendomsidentifikasjon =new MatrikkelnummerType
                    {
                        kommunenummer = knr,
                        gaardsnummer = gnr,
                        bruksnummer = bnr,
                        festenummer = fnr,
                        seksjonsnummer = snr
                    },
                    adresse = new EiendommensAdresseType()
                    {
                        adresselinje1 = "Nygate 33",
                        postnr = "3825",
                        poststed = "Lunde"
                    },
                    bygningsnummer = "123456789",
                    bolignummer = bolignummer
                }
            };
            return this;
        }
        // AnsvarligSoeker
        public IgangsettingstillatelseV2Builder AnsvarligSoeker1(string ansvarligSoekerNavn = null, string ansvarligSoekerPostnr = null)
        {
            _form.ansvarligSoeker = new PartType
            {
                partstype = new KodeType
                {
                    kodeverdi = "Foretak",
                    kodebeskrivelse = "Foretak"
                },
                organisasjonsnummer = "914994780", // 914994780 Arkitektum
                // navn = "Siv. Ark. Fjell og Sønner",
                kontaktperson = "Benjamin Fjell",
                adresse = new EnkelAdresseType
                {
                    adresselinje1 = "Lillegata 5",
                    //postnr = "7003",
                    poststed = "Trondheim"
                },
                telefonnummer = "11223344",
                mobilnummer = "99887766",
                epost = "tine@arkitektum.no",
                signaturdato = new System.DateTime(2016, 07, 26),
                signaturdatoSpecified = true
            };

            if (!string.IsNullOrEmpty(ansvarligSoekerNavn))
                _form.ansvarligSoeker.navn = ansvarligSoekerNavn;
            if (!string.IsNullOrEmpty(ansvarligSoekerPostnr))
                _form.ansvarligSoeker.adresse.postnr = ansvarligSoekerPostnr;

            return this;
        }
        public IgangsettingstillatelseV2Builder AnsvarligSoeker(string organisasjonsnummer, string foedselsnummer, string partsTypeKodeverdi = null, string kontaktperson = null, string navn = null, string adressLine = null, string postnr = null, string landkode = null
            , string poststed = null, string telefonnummer = null, string mobilnummer = null)
        {
            _form.ansvarligSoeker = new PartType();

            if (!string.IsNullOrEmpty(partsTypeKodeverdi))
            {
                _form.ansvarligSoeker.partstype = new KodeType
                {
                    kodeverdi = partsTypeKodeverdi,
                    kodebeskrivelse = ""
                };
            }

            _form.ansvarligSoeker.navn = navn;

            if (!string.IsNullOrEmpty(adressLine) || !string.IsNullOrEmpty(postnr) || !string.IsNullOrEmpty(landkode) || !string.IsNullOrEmpty(poststed))
            {
                _form.ansvarligSoeker.adresse = new EnkelAdresseType
                {
                    adresselinje1 = adressLine,
                    postnr = postnr,
                    landkode = landkode,
                    poststed = poststed
                };
            }

            _form.ansvarligSoeker.organisasjonsnummer = organisasjonsnummer; //"974760673" - 974760673 BRREG, Altinn brukerservice
            _form.ansvarligSoeker.kontaktperson = kontaktperson;
            _form.ansvarligSoeker.foedselsnummer = foedselsnummer;
            _form.ansvarligSoeker.telefonnummer = telefonnummer;
            _form.ansvarligSoeker.mobilnummer = mobilnummer;

            return this;
        }


    }
}
