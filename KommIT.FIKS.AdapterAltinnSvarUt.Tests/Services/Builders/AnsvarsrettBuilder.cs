﻿using System.Collections.Generic;
using System.Linq;
using no.kxml.skjema.dibk.ansvarsrettDirekteOpprettet;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Tests.Services.Builders
{
    public class AnsvarsrettBuilder
    {
        private readonly ErklaeringAnsvarsrettType _form = new ErklaeringAnsvarsrettType();

        public AnsvarsrettBuilder()
        {
            _form.ansvarsrett = new AnsvarsrettType()
            {
            };
        }
        public ErklaeringAnsvarsrettType Build()
        {
            return _form;
        }

        public AnsvarsrettBuilder AnsvarligSoeker(string organisasjonsnummer)
        {
            if (_form.ansvarligSoeker == null)
                _form.ansvarligSoeker = new PartType();

            _form.ansvarligSoeker.organisasjonsnummer = organisasjonsnummer;

            return this;
        }

        public AnsvarsrettBuilder Ansvarsrett(string funksjonKodeverdi = null, string funksjonKodebeskrivelse = null, string beskrivelseAvAnsvarsomraade = null, string tiltaksklasseKodeverdi = null, string tiltaksklasseKodebeskrivelse = null, bool samsvarKontrollVedRammetillatelse = true, bool samsvarKontrollVedIgangsettingstillatelse = true,
            bool samsvarKontrollVedMidlertidigBrukstillatelse = true, bool samsvarKontrollVedFerdigattest = true, string vaarReferanse = null, bool? erklaeringAnsvarligUtfoerende = false, bool? erklaeringAnsvarligProsjekterende = false, bool? erklaeringAnsvarligKontrollerende = false)
        {
            AnsvarsomraadeType ansvarsomraade = new AnsvarsomraadeType();

            if (_form.ansvarsrett == null)
            {
                _form.ansvarsrett = new AnsvarsrettType();
            }

            ansvarsomraade.beskrivelseAvAnsvarsomraade = beskrivelseAvAnsvarsomraade;

            if (!string.IsNullOrEmpty(funksjonKodeverdi) || !string.IsNullOrEmpty(funksjonKodebeskrivelse))
            {
                ansvarsomraade.funksjon = new KodeType()
                {
                    kodeverdi = funksjonKodeverdi,
                    kodebeskrivelse = funksjonKodebeskrivelse
                };
            }

            if (!string.IsNullOrEmpty(tiltaksklasseKodeverdi) || !string.IsNullOrEmpty(tiltaksklasseKodebeskrivelse))
            {
                ansvarsomraade.tiltaksklasse = new KodeType()
                {
                    kodeverdi = tiltaksklasseKodeverdi,
                    kodebeskrivelse = tiltaksklasseKodebeskrivelse
                };
            }

            ansvarsomraade.samsvarKontrollVedRammetillatelse = samsvarKontrollVedRammetillatelse;
            ansvarsomraade.samsvarKontrollVedIgangsettingstillatelse = samsvarKontrollVedIgangsettingstillatelse;
            ansvarsomraade.samsvarKontrollVedMidlertidigBrukstillatelse = samsvarKontrollVedMidlertidigBrukstillatelse;
            ansvarsomraade.samsvarKontrollVedFerdigattest = samsvarKontrollVedFerdigattest;
            ansvarsomraade.vaarReferanse = vaarReferanse;
            ansvarsomraade.vaarReferanse = vaarReferanse;
            ansvarsomraade.vaarReferanse = vaarReferanse;

            List<AnsvarsomraadeType> ansvarsomraadeTypes = null;
            if (_form.ansvarsrett.ansvarsomraader == null)
            {
                ansvarsomraadeTypes = new List<AnsvarsomraadeType>() { ansvarsomraade };
            }
            else
            {
                ansvarsomraadeTypes = _form.ansvarsrett.ansvarsomraader.ToList();
                ansvarsomraadeTypes.Add(ansvarsomraade);
            }
            _form.ansvarsrett.ansvarsomraader = ansvarsomraadeTypes.ToArray();

            if (erklaeringAnsvarligUtfoerende.HasValue)
                _form.ansvarsrett.erklaeringAnsvarligUtfoerende = erklaeringAnsvarligUtfoerende.Value;
            if (erklaeringAnsvarligProsjekterende.HasValue)
                _form.ansvarsrett.erklaeringAnsvarligProsjekterende = erklaeringAnsvarligProsjekterende.Value;
            if (erklaeringAnsvarligKontrollerende.HasValue)
                _form.ansvarsrett.erklaeringAnsvarligKontrollerende = erklaeringAnsvarligKontrollerende.Value;

            return this;
        }

        public AnsvarsrettBuilder EiendomByggested()
        {
            _form.eiendomByggested = new[]
            {
                new EiendomType()
                {
                    eiendomsidentifikasjon = new MatrikkelnummerType()
                    {
                        kommunenummer = "123",
                        gaardsnummer = "123",
                        bruksnummer = "123"
                    }
                }
            };

            return this;
        }

        public AnsvarsrettBuilder Foretak(string orgNr)
        {
            _form.ansvarsrett.foretak = new ForetakType()
            {
                kontaktperson = new KontaktpersonType()
                {
                    navn = "Jens Jensen",
                    telefonnummer = "99994444",
                    mobilnummer = "44449999",
                    epost = "jens@kontaktperson.no"
                },
                harSentralGodkjenning = true,
                partstype = new KodeType()
                {
                    kodeverdi = "Foretak",
                    kodebeskrivelse = "Foretak"
                },
                organisasjonsnummer = orgNr,
                navn = "Nordmann Bygg og Anlegg AS",
                adresse = new EnkelAdresseType
                {
                    adresselinje1 = "Bøgata 16",
                    postnr = "3802",
                    poststed = "Bø i Telemark",
                    landkode = "NO"
                },
                telefonnummer = "12345678",
                mobilnummer = "98765432",
                epost = "ola@byggmestern-ola.no",
                signaturdato = new System.DateTime(2016, 12, 05),
                signaturdatoSpecified = true,
            };

            return this;
        }

        public AnsvarsrettBuilder ToAnsvarligeSoekere()
        {
            _form.ansvarsrett.ansvarsomraader = new[]
            {
                new AnsvarsomraadeType()
                {
                    funksjon = new KodeType()
                    {
                        kodeverdi = "SØK",
                        kodebeskrivelse = "Ansvarlig søker"
                    },
                    beskrivelseAvAnsvarsomraade = "Andre",
                    tiltaksklasse = new KodeType()
                    {
                        kodeverdi = "2",
                        kodebeskrivelse = "Tiltaksklasse 2"
                    },
                    samsvarKontrollVedRammetillatelse = false,
                    samsvarKontrollVedIgangsettingstillatelse = false,
                    samsvarKontrollVedMidlertidigBrukstillatelse = false,
                    samsvarKontrollVedFerdigattest = false
                },
                new AnsvarsomraadeType()
                {
                    funksjon = new KodeType()
                    {
                        kodeverdi = "SØK",
                        kodebeskrivelse = "Ansvarlig søker"
                    },
                    beskrivelseAvAnsvarsomraade = "Andre",
                    tiltaksklasse = new KodeType()
                    {
                        kodeverdi = "2",
                        kodebeskrivelse = "Tiltaksklasse 2"
                    },
                    samsvarKontrollVedRammetillatelse = false,
                    samsvarKontrollVedIgangsettingstillatelse = false,
                    samsvarKontrollVedMidlertidigBrukstillatelse = false,
                    samsvarKontrollVedFerdigattest = false
                }
            };

            return this;
        }


        public AnsvarsrettBuilder TiltaksklasseMedGodkjenteKodeverdier()
        {
            _form.ansvarsrett.ansvarsomraader = new[]
            {
                new AnsvarsomraadeType()
                {
                    funksjon = new KodeType()
                    {
                        kodeverdi = "KONTROLL",
                        kodebeskrivelse = "Ansvarlig kontroll"
                    },
                    beskrivelseAvAnsvarsomraade = "Overordnet ansvar for kontroll",
                    tiltaksklasse = new KodeType()
                    {
                        kodeverdi = "1",
                        kodebeskrivelse = "Tiltaksklasse 1"
                    },
                    samsvarKontrollVedRammetillatelse = false,
                    samsvarKontrollVedIgangsettingstillatelse = true,
                    samsvarKontrollVedMidlertidigBrukstillatelse = true,
                    samsvarKontrollVedFerdigattest = true,
                },
                new AnsvarsomraadeType()
                {
                    funksjon = new KodeType()
                    {
                        kodeverdi = "KONTROLL",
                        kodebeskrivelse = "Ansvarlig kontroll"
                    },
                    beskrivelseAvAnsvarsomraade = "Overordnet ansvar for kontroll",
                    tiltaksklasse = new KodeType()
                    {
                        kodeverdi = "2",
                        kodebeskrivelse = "Tiltaksklasse 2"
                    },
                    samsvarKontrollVedRammetillatelse = false,
                    samsvarKontrollVedIgangsettingstillatelse = true,
                    samsvarKontrollVedMidlertidigBrukstillatelse = true,
                    samsvarKontrollVedFerdigattest = true,
                },
                new AnsvarsomraadeType()
                {
                    funksjon = new KodeType()
                    {
                        kodeverdi = "KONTROLL",
                        kodebeskrivelse = "Ansvarlig kontroll"
                    },
                    beskrivelseAvAnsvarsomraade = "Overordnet ansvar for kontroll",
                    tiltaksklasse = new KodeType()
                    {
                        kodeverdi = "3",
                        kodebeskrivelse = "Tiltaksklasse 3"
                    },
                    samsvarKontrollVedRammetillatelse = false,
                    samsvarKontrollVedIgangsettingstillatelse = true,
                    samsvarKontrollVedMidlertidigBrukstillatelse = true,
                    samsvarKontrollVedFerdigattest = true,
                }
            };

            return this;
        }

        public AnsvarsrettBuilder TiltaksklasseMedIkkeGodkjenteKodeverdier()
        {
            _form.ansvarsrett.ansvarsomraader = new[]
            {
                new AnsvarsomraadeType()
                {
                    funksjon = new KodeType()
                    {
                        kodeverdi = "KONTROLL",
                        kodebeskrivelse = "Ansvarlig kontroll"
                    },
                    beskrivelseAvAnsvarsomraade = "Overordnet ansvar for kontroll",
                    tiltaksklasse = new KodeType()
                    {
                        kodeverdi = "a",
                        kodebeskrivelse = "Tiltaksklasse a"
                    },
                    samsvarKontrollVedRammetillatelse = false,
                    samsvarKontrollVedIgangsettingstillatelse = true,
                    samsvarKontrollVedMidlertidigBrukstillatelse = true,
                    samsvarKontrollVedFerdigattest = true,
                }
            };

            return this;
        }
    }
}