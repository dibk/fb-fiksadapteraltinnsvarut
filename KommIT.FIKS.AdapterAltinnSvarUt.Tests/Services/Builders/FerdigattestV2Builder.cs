﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using no.kxml.skjema.dibk.ferdigattestV2;


namespace KommIT.FIKS.AdapterAltinnSvarUt.Tests.Services.Builders
{
    class FerdigattestV2Builder
    {
        private readonly FerdigattestType _form;

        public FerdigattestV2Builder()
        {
            _form = new FerdigattestType();
        }

        public FerdigattestType Build()
        {
            return _form;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="kommunenummer">"2004"</param>
        /// <param name="kommunenavn">"Hammerfest"</param>
        /// <param name="bolignummer">"H0101"</param>
        /// <returns></returns>
        public FerdigattestV2Builder EiendomByggested(string kommunenummer = null, string kommunenavn = null, string bolignummer = null)
        {
            _form.eiendomByggested = new[]
            {
                new EiendomType()
                {
                    bolignummer = bolignummer,
                    kommunenavn = kommunenavn,
                    eiendomsidentifikasjon = new MatrikkelnummerType()
                    {
                        kommunenummer = kommunenummer,
                    }

                },
            };

            return this;
        }
        public FerdigattestV2Builder HarTilstrekkeligSikkerhet(bool tilfredstillerTiltaketKraveneFerdigattest, string typeArbeider, DateTime? utfoertInnen = null, DateTime? bekreftelseInnen = null)
        {
            _form.tilfredstillerTiltaketKraveneFerdigattestSpecified = true;
            _form.tilfredstillerTiltaketKraveneFerdigattest = tilfredstillerTiltaketKraveneFerdigattest;

            // Set Dato utfoertInnen
            if (utfoertInnen.HasValue)
            {
                _form.utfoertInnenSpecified = true;
                _form.utfoertInnen = utfoertInnen.Value;
            }
            else
            {
                _form.utfoertInnenSpecified = false;
            }

            // Set Dato tobekreftelseInnen

            if (bekreftelseInnen.HasValue)
            {
                _form.bekreftelseInnenSpecified = true;
                _form.bekreftelseInnen = bekreftelseInnen.Value;
            }
            else
            {
                _form.bekreftelseInnenSpecified = false;
            }

            _form.typeArbeider = typeArbeider;
            return this;
        }

        public FerdigattestV2Builder AnsvarligSoeker(string organisasjonsnummer, string foedselsnummer, string partsTypeKodeverdi = null, string kontaktperson = null, string navn = null, string adressLine = null, string postnr = null, string landkode = null
            , string poststed = null, string telefonnummer = null, string mobilnummer = null)
        {
            _form.ansvarligSoeker = new PartType();

            if (!string.IsNullOrEmpty(partsTypeKodeverdi))
            {
                _form.ansvarligSoeker.partstype = new KodeType
                {
                    kodeverdi = partsTypeKodeverdi,
                    kodebeskrivelse = ""
                };
            }

            _form.ansvarligSoeker.navn = navn;

            if (!string.IsNullOrEmpty(adressLine) || !string.IsNullOrEmpty(postnr) || !string.IsNullOrEmpty(landkode) || !string.IsNullOrEmpty(poststed))
            {
                _form.ansvarligSoeker.adresse = new EnkelAdresseType
                {
                    adresselinje1 = adressLine,
                    postnr = postnr,
                    landkode = landkode,
                    poststed = poststed
                };
            }

            _form.ansvarligSoeker.organisasjonsnummer = organisasjonsnummer; //"974760673" - 974760673 BRREG, Altinn brukerservice
            _form.ansvarligSoeker.kontaktperson = kontaktperson;
            _form.ansvarligSoeker.foedselsnummer = foedselsnummer;
            _form.ansvarligSoeker.telefonnummer = telefonnummer;
            _form.ansvarligSoeker.mobilnummer = mobilnummer;

            return this;
        }


        public FerdigattestV2Builder FraSluttbrukersystem(string fraSluttbrukersystem)
        {
            _form.fraSluttbrukersystem = fraSluttbrukersystem;
            return this;
        }

        public FerdigattestV2Builder KommunensSaksnummer(string sakssekvensnummer, string saksaar)
        {
            _form.kommunensSaksnummer = new SaksnummerType()
            {
                sakssekvensnummer = sakssekvensnummer,
                saksaar = saksaar
            };
            return this;
        }

        public FerdigattestV2Builder foretattIkkeSoeknadspliktigeJusteringer(bool value)
        {
            _form.foretattIkkeSoeknadspliktigeJusteringerSpecified = true;
            _form.foretattIkkeSoeknadspliktigeJusteringer = value;
            return this;
        }
    }
}
