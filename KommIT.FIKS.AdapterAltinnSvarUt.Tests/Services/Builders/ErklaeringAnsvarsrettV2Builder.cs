﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using no.kxml.skjema.dibk.ansvarsrettDirekteOpprettet;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Tests.Services.Builders
{
    class ErklaeringAnsvarsrettV2Builder
    {
        private readonly no.kxml.skjema.dibk.ansvarsrettDirekteOpprettet.ErklaeringAnsvarsrettType _form;

        public ErklaeringAnsvarsrettV2Builder()
        {
            _form = new ErklaeringAnsvarsrettType();
        }

        public ErklaeringAnsvarsrettType Build()
        {
            return _form;
        }


        public ErklaeringAnsvarsrettV2Builder AnsvarligSoeker(string organisasjonsnummer, string navn, string postnr, string poststed, string kontaktperson = null)
        {

            _form.ansvarligSoeker = new ForetakType()
            {
                organisasjonsnummer = organisasjonsnummer,
                navn = navn,
                partstype = new KodeType() { kodeverdi = "Foretak" },
                adresse = new EnkelAdresseType() { postnr = postnr, poststed = poststed },
            };

            if (!string.IsNullOrEmpty(kontaktperson))
            {
                _form.ansvarligSoeker.kontaktperson = new KontaktpersonType()
                {
                    navn = kontaktperson
                };
            }


            return this;
        }
        public ErklaeringAnsvarsrettV2Builder AnsvarligForetak(string ansvarsrettOrganisasjonsnummer, string navn, string postnr, string poststed)
        {
            if (_form.ansvarsrett == null)
                _form.ansvarsrett = new AnsvarsrettType();


            _form.ansvarsrett.foretak = new ForetakType()
            {
                organisasjonsnummer = ansvarsrettOrganisasjonsnummer,
                navn = navn,
                partstype = new KodeType() { kodeverdi = "Foretak" },
                adresse = new EnkelAdresseType() { postnr = postnr, poststed = poststed }
            };


            return this;
        }
    }
}
