﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KommIT.FIKS.AdapterAltinnSvarUt.Models.ApiModels;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Tests.Services.Builders
{
   public class ValidateAttachmentsBuilder
    {
        private readonly List<ValidateAttachment> _vedleggList;

        public ValidateAttachmentsBuilder()
        {
            _vedleggList = new List<ValidateAttachment>();
        }

        public List<ValidateAttachment> Build()
        {
            return _vedleggList;
        }

        public ValidateAttachmentsBuilder AddVedlegg(string vedleggName, string vedleggFileName, int vedleggFileSize,
            int vedleggCount)
        {
            for (int i = 0; i < vedleggCount; i++)
            {
                var vedlegg = new ValidateAttachment()
                {
                    Name = vedleggName,
                    Filename = vedleggFileName,
                    FileSize = vedleggFileSize
                };
                _vedleggList.Add(vedlegg);
            }
            return this;
        }
    }
}
