﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.SvarUt;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.SvarUtForm.SubForm;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Validations;
using KommIT.FIKS.AdapterAltinnSvarUt.Tests.Services.Builders;
using Moq;
using Xunit;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Tests.Services
{
    public class GjenpartnabovarselV2Tests
    {

        [Fact(DisplayName = "4816_1_6_EiendomByggestedValidationOkTest")]
        public void Gjenpartnabovarsel_4816_1_6_EiendomByggestedValidationOkTest()
        {
            // Hvis bruksenhetsnummer(bolignummer) er fylt ut må det følge riktig format (^[HKLU]\\d{4}$) (feks H0101) se https://www.kartverket.no/eiendom/adressering/bruksenhetsnummer/	Feil
            var val = new GjenpartnabovarselV2Validator();
            var municipalityValidator = new Mock<IMunicipalityValidator>();
            municipalityValidator.Setup(s => s.ValidateKommunenummerExists(It.IsAny<string>())).Returns(new GeneralValidationResult() { Status = ValidationStatus.Ok, Message = "UnitTest" });
            val.MunicipalityValidator = municipalityValidator.Object;

            var nabovarsel = new GjenpartnabovarselV2Builder()
                .EiendomType("2004", "1234", "4321", "0", "0", "Hammerfest", "H0101")
                .Build();
            val.EiendomByggestedValidation(nabovarsel);
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }

        [Fact(DisplayName = "4816_1_6_EiendomByggestedValidationErrorTest")]
        public void Gjenpartnabovarsel_4816_1_6_EiendomByggestedValidationErrorTest()
        {
            // Hvis bruksenhetsnummer(bolignummer) er fylt ut må det følge riktig format (^[HKLU]\\d{4}$) (feks H0101) se https://www.kartverket.no/eiendom/adressering/bruksenhetsnummer/	Feil
            var val = new GjenpartnabovarselV2Validator();
            var municipalityValidator = new Mock<IMunicipalityValidator>();
            municipalityValidator.Setup(s => s.ValidateKommunenummerExists(It.IsAny<string>())).Returns(new GeneralValidationResult() { Status = ValidationStatus.Ok, Message = "UnitTest" });
            val.MunicipalityValidator = municipalityValidator.Object;


            var nabovarsel = new GjenpartnabovarselV2Builder()
                .EiendomType("2004", "1234", "4321", "0", "0", "Hammerfest", "X0101")
                .Build();
            val.EiendomByggestedValidation(nabovarsel);
            var result = val.GetResult();

            result.messages.Where(m => m.reference.Contains("4816.1.6")).Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);

        }
        [Fact(DisplayName = "4816_1_1_EiendomByggestedValidationOkTest")]
        public void Gjenpartnabovarsel_4816_1_1_EiendomByggestedValidationOkTest()
        {
            // Hvis bruksenhetsnummer(bolignummer) er fylt ut må det følge riktig format (^[HKLU]\\d{4}$) (feks H0101) se https://www.kartverket.no/eiendom/adressering/bruksenhetsnummer/	Feil
            var val = new GjenpartnabovarselV2Validator();
            var municipalityValidator = new Mock<IMunicipalityValidator>();
            municipalityValidator.Setup(s => s.ValidateKommunenummerExists(It.IsAny<string>())).Returns(new GeneralValidationResult() { Status = ValidationStatus.Ok, Message = "UnitTest" });
            val.MunicipalityValidator = municipalityValidator.Object;

            var nabovarsel = new GjenpartnabovarselV2Builder()
                .EiendomType("2004", "1234", "4321", "0", "0", "Hammerfest", "H0101")
                .Build();
            val.EiendomByggestedValidation(nabovarsel);
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4816_1_1_EiendomByggestedValidationErrorTest")]
        public void Gjenpartnabovarsel_4816_1_1_EiendomByggestedValidationErrorTest()
        {
            // Hvis bruksenhetsnummer(bolignummer) er fylt ut må det følge riktig format (^[HKLU]\\d{4}$) (feks H0101) se https://www.kartverket.no/eiendom/adressering/bruksenhetsnummer/	Feil
            var val = new GjenpartnabovarselV2Validator();
            var municipalityValidator = new Mock<IMunicipalityValidator>();
            municipalityValidator.Setup(s => s.ValidateKommunenummerExists(It.IsAny<string>())).Returns(new GeneralValidationResult() { Status = ValidationStatus.Ok, Message = "UnitTest" });
            val.MunicipalityValidator = municipalityValidator.Object;

            var nabovarsel = new GjenpartnabovarselV2Builder()
                .EiendomType("2004", "1234", "4321", "0", "0", null, "H0101")
                .Build();
            val.EiendomByggestedValidation(nabovarsel);
            var result = val.GetResult();

            result.messages.Where(m => m.reference.Contains("4816.1.1")).Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4816_1_1_MinstEtTiltaOkTest")]
        public void Gjenpartnabovarsel_4816_1_1_MinstEtTiltaOkTest()
        {
            // Minst en tiltakstype må være registrert", "4816.1.1", "/Nabovarsel/beskrivelseAvTiltak[0]/type
            var val = new GjenpartnabovarselV2Validator();
            var tiltakformaalDictionary = new Dictionary<string, string>()
            {
                {"tiltaksformaalKode1","Fritidsbolig" },
                {"tiltaksformaalkodebeskrivelse1","Fritidsbolig" },
                {"tiltaksformaalKode2","Annet" },
                {"tiltaksformaalkodebeskrivelse2","Annet" },
            };
            var nabovarsel = new GjenpartnabovarselV2Builder()
                .BeskrivelseAvTiltak("nyttbyggboligformal", "Nytt bygg - Boligformål", "Annet", "Annet", "Fritidsbolig", "Fritidsbolig")
                .Build();
            val.MinstEtTiltak(nabovarsel);
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }

        [Fact(DisplayName = "4816_1_1_MinstEtTiltakErrorTest")]
        public void Gjenpartnabovarsel_4816_1_1_MinstEtTiltakErrorTest()
        {
            // Minst en tiltakstype må være registrert", "4816.1.1", "/Nabovarsel/beskrivelseAvTiltak[0]/type
            var val = new GjenpartnabovarselV2Validator();
            var tiltakformaalDictionary = new Dictionary<string, string>()
            {
                {"tiltaksformaalKode1","Fritidsbolig" },
                {"tiltaksformaalkodebeskrivelse1","Fritidsbolig" },
                {"tiltaksformaalKode2","Annet" },
                {"tiltaksformaalkodebeskrivelse2","Annet" },
            };
            //Tiltakstype + vedlegg
            var nabovarsel = new GjenpartnabovarselV2Builder()
                .BeskrivelseAvTiltak(null, null, "Annet", "Annet", "Fritidsbolig", "Fritidsbolig")
                .Build();
            val.MinstEtTiltak(nabovarsel);
            var result = val.GetResult();

            result.messages.Where(m => m.reference.Contains("4816.1.1")).Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);

        }

        [Fact(DisplayName = "4816_1_5_MinstEtTiltaOkTest")]
        public void Gjenpartnabovarsel_4816_1_5_MinstEtTiltaOkTest()
        {
            // Minst en tiltakstype må være registrert", "4816.1.1", "/Nabovarsel/beskrivelseAvTiltak[0]/type
            var val = new GjenpartnabovarselV2Validator();
            var tiltakformaalDictionary = new Dictionary<string, string>()
            {
                {"tiltaksformaalKode2","Annet" },
                {"tiltaksformaalkodebeskrivelse2","Annet" },
            };
            var nabovarsel = new GjenpartnabovarselV2Builder()
                .BeskrivelseAvTiltak("nyttbyggboligformal", "Nytt bygg - Boligformål", "Annet", "Annet", null,null)
                .Build();
            val.MinstEtTiltak(nabovarsel);
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4816_1_5_MinstEtTiltaErrorTest")]
        public void Gjenpartnabovarsel_4816_1_5_MinstEtTiltaErrorTest()
        {
            // Minst en tiltakstype må være registrert", "4816.1.1", "/Nabovarsel/beskrivelseAvTiltak[0]/type
            var val = new GjenpartnabovarselV2Validator();
            var tiltakformaalDictionary = new Dictionary<string, string>()
            {
                {"tiltaksformaalKode2","Annet" },
                {"tiltaksformaalkodebeskrivelse2","Annet" },
            };
            var nabovarsel = new GjenpartnabovarselV2Builder()
                .BeskrivelseAvTiltak("nyttbyggboligformal", "Nytt bygg - Boligformål","Annet","Annet",null,null, false)
                .Build();
            val.MinstEtTiltak(nabovarsel);
            var result = val.GetResult();

            result.messages.Where(m => m.reference.Contains("4816.1.5")).Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }


        [Fact(DisplayName = "4816_1_2_MinstEtTiltaOkTest")]
        public void Gjenpartnabovarsel_4816_1_2_MinstEtTiltaOkTest()
        {
            // Ugyldig kodeverdi i henhold til kodeliste for tiltakstype", "4816.1.2", "/Nabovarsel/beskrivelseAvTiltak[0]/type[0]/kodeverdi
            var val = new GjenpartnabovarselV2Validator();
            var tiltakformaalDictionary = new Dictionary<string, string>()
            {
                {"tiltaksformaalKode1","Fritidsbolig" },
                {"tiltaksformaalkodebeskrivelse1","Fritidsbolig" },
                {"tiltaksformaalKode2","Annet" },
                {"tiltaksformaalkodebeskrivelse2","Annet" },
            };

            var nabovarsel = new GjenpartnabovarselV2Builder()
                .BeskrivelseAvTiltak("nyttbyggboligformal", "Nytt bygg - Boligformål", "Annet", "Annet", "Fritidsbolig", "Fritidsbolig")
                .Build();
            val.MinstEtTiltak(nabovarsel);
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }

        [Fact(DisplayName = "4816_1_2_MinstEtTiltaErrorTest")]
        public void Gjenpartnabovarsel_4816_1_2_MinstEtTiltaErrorTest()
        {
            // Ugyldig kodeverdi i henhold til kodeliste for tiltakstype", "4816.1.2", "/Nabovarsel/beskrivelseAvTiltak[0]/type[0]/kodeverdi
            var val = new GjenpartnabovarselV2Validator();
            var tiltakformaalDictionary = new Dictionary<string, string>()
            {
                {"tiltaksformaalKode1","aaa" },
                {"tiltaksformaalkodebeskrivelse1","Fritidsbolig" },
            };

            var nabovarsel = new GjenpartnabovarselV2Builder()
                .BeskrivelseAvTiltak("ddd", "Nytt bygg - Boligformål", "aaa", "Fritidsbolig",null,null)
                .Build();
            val.MinstEtTiltak(nabovarsel);
            var result = val.GetResult();

            result.messages.Count(m => m.reference.Contains("4816.1.2")).Should().Be(2);
            result.messages.Count.Should().Be(2);
        }

        [Fact(DisplayName = "4816_1_4_DispensasjonValideringOKTest")]
        public void Rammetillatelse_4816_1_4_DispensasjonValideringOKTest()
        {
            // Når dispensasjon er angitt må dispensasjonstype og begrunnelse fylles ut. Feil
            var val = new GjenpartnabovarselV2Validator();
            var dispensasjonTypeDictionary = new Dictionary<string, string>()
            {
                {"dispensasjonTypeKode1","PLAN" },
                {"beskrivelse","Søknad om dispensasjon fra bestemmelsen NN i plan" }

            };
            var rammetillatelse = new GjenpartnabovarselV2Builder()
                .Dispensasjon("PLAN",null, "Søknad om dispensasjon fra bestemmelsen NN i plan", "Begrunnelse for dispensasjon")
                .Build();
            val.DispensasjonValidering(rammetillatelse);
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4816_1_4_DispensasjonValideringErrorTest")]
        public void Rammetillatelse_4816_1_4_DispensasjonValideringErrorTest()
        {
            // Når dispensasjon er angitt må dispensasjonstype og begrunnelse fylles ut. Feil
            var val = new GjenpartnabovarselV2Validator();
            var dispensasjonTypeDictionary = new Dictionary<string, string>()
            {
                {"beskrivelse","Søknad om dispensasjon fra bestemmelsen NN i plan"}
            };
            var rammetillatelse = new GjenpartnabovarselV2Builder()
                .Dispensasjon(null,null, "Søknad om dispensasjon fra bestemmelsen NN i plan", null)
                .Build();
            val.DispensasjonValidering(rammetillatelse);
            var result = val.GetResult();

            result.messages.Where(m => m.reference.Contains("4816.1.4")).Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4816_2_7_DispensasjonValideringOKTest")]
        public void Rammetillatelse_4816_2_7_DispensasjonValideringOKTest()
        {
            // Det er valgt at det skal søkes om dispensasjon, men det mangler informasjon om dispensasjonen med beskrivelse og begrunnelse.", "4816.2.10", "/Nabovarsel/....
            var val = new GjenpartnabovarselV2Validator();
            var dispensasjonTypeDictionary = new Dictionary<string, string>()
            {
                {"dispensasjonTypeKode1","PLAN" },
                //{"dispensasjonTypekodebeskrivelse1","Arealplan" },
                {"beskrivelse","Søknad om dispensasjon fra bestemmelsen NN i plan" }

            };
            var rammetillatelse = new GjenpartnabovarselV2Builder()
                .Dispensasjon("PLAN",null, "Søknad om dispensasjon fra bestemmelsen NN i plan", "Begrunnelse for dispensasjon")
                .Build();
            val.DispensasjonValidering(rammetillatelse);
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4816_2_7_DispensasjonValideringErrorTest")]
        public void Rammetillatelse_4816_2_7_DispensasjonValideringErrorTest()
        {
            // Det er valgt at det skal søkes om dispensasjon, men det mangler informasjon om dispensasjonen med beskrivelse og begrunnelse.", "4816.2.10", "/Nabovarsel/....
            var val = new GjenpartnabovarselV2Validator();
            var dispensasjonTypeDictionary = new Dictionary<string, string>()
            {
                {"dispensasjonTypeKode1","PLAN" },
            };
            var rammetillatelse = new GjenpartnabovarselV2Builder()
                .Dispensasjon("PLAN",null,null, "Begrunnelse for dispensasjon")
                .Build();
            val.DispensasjonValidering(rammetillatelse);
            var result = val.GetResult();

            result.messages.Where(m => m.reference.Contains("4816.2.7")).Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
    }
}
