﻿using System.Collections.Generic;
using System.IO;
using FluentAssertions;
using KommIT.FIKS.AdapterAltinnSvarUt.Services;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.SvarUt;
using Xunit;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Altinn;
using Moq;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Prefill;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Notification;
using Serilog;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Azure;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.WS.Altinn;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;
using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Encryption;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Svarut;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Svarut.StatusService;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.UserNotification;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.WS.AltinnCorrespondenceAgency;
using KommIT.FIKS.AdapterAltinnSvarUt.QueuePreparators;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.DecryptFnrForTh;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Tests.Services
{
    public class SkjemaSomFeilerTests
    {

        //*********************************************************************
        // Kan ikke kjøres da den er avhengig av Hangfire
        //*********************************************************************
        //[Fact(Skip = "Bugtest")]
        //public void SendRammesøknadFeilFraEbsTest()
        //{
        //    //(Skip = "bugfixtest")  
        //    var logService = new Mock<ILogEntryService>();
        //    var logger = new Mock<ILogger>();
        //    var formMetadataService = new Mock<IFormMetadataService>();
        //    var altinnService = new Mock<IAltinnService>();
        //    var altinnPrefillService = new Mock<IAltinnPrefillService>();
        //    var formShippingService = new Mock<IFormShippingService>();
        //    var CorrService = new Mock<IAltinnCorrespondenceService>();
        //    var decryptionFactory = new Mock<IDecryptionFactory>();

        //    decryptionFactory.Setup(f => f.GetDecryptor()).Returns(new Mock<IDecryption>().Object);

        //    var svarUtServicePrint = new Mock<ISvarUtService>();
        //    var svarUtServiceFactory = new Mock<ISvarUtServiceFactory>();
        //    svarUtServiceFactory.Setup(s => s.GetSvarUtServiceFor(It.IsAny<SvarUtServiceTypeEnum>()))
        //        .Returns(svarUtServicePrint.Object);

        //    var svarUtStatusService = new Mock<ISvarUtStatusService>();

        //    var appLogService = new Mock<IApplicationLogService>();
        //    var submittalQueueService = new Mock<ExportToSubmittalQueueService>();

        //    IFileStorage iFileStorage = new FileStorage(new BlobStorage());
        //    IFileDownloadStatusService fileDownloadStatusService = new FileDownloadStatusService(new ApplicationDbContext());
        //    //Sende ok på kvitteringsmelding
        //    CorrService.Setup(s => s.SendNotification(It.IsAny<string>(), It.IsAny<InsertCorrespondenceV2>())).Returns(new ReceiptExternal() { ReceiptStatusCode = ReceiptStatusEnum.OK });
        //    UserNotificationHandler userNotificationHandler = new UserNotificationHandler(new UserNotificationService(new ApplicationDbContext()));
        //    var syncService = new Mock<ISyncRecordsOfCombinedDistributions>();


        //    SendPrefillFormServiceV2 sendPrefillService = new SendPrefillFormServiceV2(CorrService.Object, altinnPrefillService.Object, logService.Object, formMetadataService.Object, decryptionFactory.Object, svarUtServiceFactory.Object, userNotificationHandler, syncService.Object, svarUtStatusService.Object);
        //    IAltinnFormDeserializer desrService = new AltinnFormDeserializer();
        //    FormDistributionServiceV2 formDistService = new FormDistributionServiceV2(CorrService.Object, logService.Object, formMetadataService.Object, sendPrefillService, decryptionFactory.Object, iFileStorage, fileDownloadStatusService);
        //    var filestorage = new Mock<IFileStorage>();
        //    var filedownloadStatus = new Mock<IFileDownloadStatusService>();
        //    NotificationServiceV2 formNotService = new NotificationServiceV2(CorrService.Object, logService.Object, formMetadataService.Object, filestorage.Object, filedownloadStatus.Object, syncService.Object, new Mock<ILogger>().Object);
        //    INotificationServiceResolver notificationServiceResolver = new NotificationServiceResolver(new List<INotificationServiceV2>() { formNotService });

        //    var SendCorrService = new SendCorrespondenceHelper(CorrService.Object, logService.Object, logger.Object);
        //    var sendNotification = new Mock<ISendSecondNotifications>();


        //    AltinnFormDownloadAndRoutingService routing = new AltinnFormDownloadAndRoutingService(altinnService.Object, desrService, formShippingService.Object, logService.Object, formMetadataService.Object, formDistService, notificationServiceResolver, SendCorrService, filestorage.Object, sendNotification.Object, appLogService.Object, submittalQueueService.Object);
        //    string ArchivedFormTaskAsXml = System.IO.File.ReadAllText(@"Services\Altinntestmeldinger\AR3826763_2017-11-24.xml");
        //    ArchivedFormTaskDQBE formTask = SerializeUtil.DeserializeFromString<ArchivedFormTaskDQBE>(ArchivedFormTaskAsXml);


        //    routing.ShipAltinnForm("AR3666383", formTask, false);

        //}

        //*********************************************************************
        // Kan ikke kjøres da den er avhengig av Hangfire
        //*********************************************************************
        //[Fact(Skip = "Bugtest")]
        //public void SendErklæringAnsvarsrettFeilFraFtbTest()
        //{
        //    //Ansvarsrett erklæring AR3775065_2017-11-24.xml AR3811434_2017-11-24.xml AR3822797_2017-11-24.xml
        //    //(Skip = "bugfixtest")
        //    var logService = new Mock<ILogEntryService>();
        //    var logger = new Mock<ILogger>();
        //    var formMetadataService = new Mock<IFormMetadataService>();
        //    var altinnService = new Mock<IAltinnService>();
        //    var altinnPrefillService = new Mock<IAltinnPrefillService>();
        //    var formShippingService = new Mock<IFormShippingService>();
        //    var CorrService = new Mock<IAltinnCorrespondenceService>();
        //    var decryptionFactory = new Mock<IDecryptionFactory>();
        //    decryptionFactory.Setup(f => f.GetDecryptor()).Returns(new Mock<IDecryption>().Object);

        //    var svarUtServicePrint = new Mock<ISvarUtService>();
        //    var svarUtServiceFactory = new Mock<ISvarUtServiceFactory>();
        //    svarUtServiceFactory.Setup(s => s.GetSvarUtServiceFor(It.IsAny<SvarUtServiceTypeEnum>()))
        //        .Returns(svarUtServicePrint.Object);

        //    var svarUtStatusService = new Mock<ISvarUtStatusService>();

        //    var appLogService = new Mock<IApplicationLogService>();
        //    var submittalQueueService = new Mock<ExportToSubmittalQueueService>();

        //    //var client = new Mock<IBackgroundJobClient>();

        //    //BackgroundJob.Enqueue(() => PurgeItem(archiveReference));


        //    //Sende ok på kvitteringsmelding
        //    CorrService.Setup(s => s.SendNotification(It.IsAny<string>(), It.IsAny<InsertCorrespondenceV2>())).Returns(new ReceiptExternal() { ReceiptStatusCode = ReceiptStatusEnum.OK });
        //    UserNotificationHandler userNotificationHandler = new UserNotificationHandler(new UserNotificationService(new ApplicationDbContext()));
        //    var syncService = new Mock<ISyncRecordsOfCombinedDistributions>();

        //    SendPrefillFormServiceV2 sendPrefillService = new SendPrefillFormServiceV2(CorrService.Object, altinnPrefillService.Object, logService.Object, formMetadataService.Object, decryptionFactory.Object, svarUtServiceFactory.Object, userNotificationHandler, syncService.Object, svarUtStatusService.Object);
        //    IAltinnFormDeserializer desrService = new AltinnFormDeserializer();
        //    IFileStorage iFileStorage = new FileStorage(new BlobStorage());
        //    IFileDownloadStatusService fileDownloadStatusService = new FileDownloadStatusService(new ApplicationDbContext());


        //    FormDistributionServiceV2 formDistService = new FormDistributionServiceV2(CorrService.Object, logService.Object, formMetadataService.Object, sendPrefillService, decryptionFactory.Object, iFileStorage, fileDownloadStatusService);
        //    var filestorage = new Mock<IFileStorage>();
        //    var filedownloadStatus = new Mock<IFileDownloadStatusService>();
        //    NotificationServiceV2 formNotService = new NotificationServiceV2(CorrService.Object, logService.Object, formMetadataService.Object, filestorage.Object, filedownloadStatus.Object, syncService.Object, new Mock<ILogger>().Object);
        //    INotificationServiceResolver notificationServiceResolver = new NotificationServiceResolver(new List<INotificationServiceV2>() { formNotService });

        //    var SendCorrService = new SendCorrespondenceHelper(CorrService.Object, logService.Object, logger.Object);
        //    var sendNotification = new Mock<ISendSecondNotifications>();


        //    AltinnFormDownloadAndRoutingService routing = new AltinnFormDownloadAndRoutingService(altinnService.Object, desrService, formShippingService.Object, logService.Object, formMetadataService.Object, formDistService, notificationServiceResolver, SendCorrService, filestorage.Object, sendNotification.Object, appLogService.Object, submittalQueueService.Object);

        //    string ArchivedFormTaskAsXml = System.IO.File.ReadAllText(@"Services\Altinntestmeldinger\AR3775065_2017-11-24.xml");
        //    ArchivedFormTaskDQBE formTask = SerializeUtil.DeserializeFromString<ArchivedFormTaskDQBE>(ArchivedFormTaskAsXml);


        //    routing.ShipAltinnForm("AR3775065", formTask, false);

        //    //Result - skjema støttes ikke i arbeidsflyt - får nå feil og blir ikke hengende i kø lenger

        //}

        //*********************************************************************
        // Kan ikke kjøres da den er avhengig av Hangfire
        //*********************************************************************
        //[Fact(Skip = "bugfixtest")]
        //public void SendGjennomføringsplanFeilFraFtbTest()
        //{
        //    //Gjennomføringsplan AR3826763_2017-11-24.xml
        //    //(Skip = "bugfixtest")
        //    var logService = new Mock<ILogEntryService>();
        //    var logger = new Mock<ILogger>();
        //    var formMetadataService = new Mock<IFormMetadataService>();
        //    var altinnService = new Mock<IAltinnService>();
        //    var altinnPrefillService = new Mock<IAltinnPrefillService>();
        //    var formShippingService = new Mock<IFormShippingService>();
        //    var CorrService = new Mock<IAltinnCorrespondenceService>();
        //    var decryptionFactory = new Mock<IDecryptionFactory>();
        //    decryptionFactory.Setup(f => f.GetDecryptor()).Returns(new Mock<IDecryption>().Object);

        //    var svarUtServicePrint = new Mock<ISvarUtService>();
        //    var svarUtServiceFactory = new Mock<ISvarUtServiceFactory>();
        //    svarUtServiceFactory.Setup(s => s.GetSvarUtServiceFor(It.IsAny<SvarUtServiceTypeEnum>()))
        //        .Returns(svarUtServicePrint.Object);

        //    var svarUtStatusService = new Mock<ISvarUtStatusService>();

        //    var appLogService = new Mock<IApplicationLogService>();
        //    var submittalQueueService = new Mock<ExportToSubmittalQueueService>();

        //    //var client = new Mock<IBackgroundJobClient>();

        //    //BackgroundJob.Enqueue(() => PurgeItem(archiveReference));


        //    //Sende ok på kvitteringsmelding
        //    CorrService.Setup(s => s.SendNotification(It.IsAny<string>(), It.IsAny<InsertCorrespondenceV2>())).Returns(new ReceiptExternal() { ReceiptStatusCode = ReceiptStatusEnum.OK });
        //    var syncService = new Mock<ISyncRecordsOfCombinedDistributions>();
        //    UserNotificationHandler userNotificationHandler = new UserNotificationHandler(new UserNotificationService(new ApplicationDbContext()));

        //    SendPrefillFormServiceV2 sendPrefillService = new SendPrefillFormServiceV2(CorrService.Object, altinnPrefillService.Object, logService.Object, formMetadataService.Object, decryptionFactory.Object, svarUtServiceFactory.Object, userNotificationHandler, syncService.Object, svarUtStatusService.Object);
        //    IAltinnFormDeserializer desrService = new AltinnFormDeserializer();
        //    IFileStorage iFileStorage = new FileStorage(new BlobStorage());
        //    IFileDownloadStatusService fileDownloadStatusService = new FileDownloadStatusService(new ApplicationDbContext());


        //    FormDistributionServiceV2 formDistService = new FormDistributionServiceV2(CorrService.Object, logService.Object, formMetadataService.Object, sendPrefillService, decryptionFactory.Object, iFileStorage, fileDownloadStatusService);
        //    var filestorage = new Mock<IFileStorage>();
        //    var filedownloadStatus = new Mock<IFileDownloadStatusService>();
        //    NotificationServiceV2 formNotService = new NotificationServiceV2(CorrService.Object, logService.Object, formMetadataService.Object, filestorage.Object, filedownloadStatus.Object, syncService.Object, new Mock<ILogger>().Object);
        //    INotificationServiceResolver notificationServiceResolver = new NotificationServiceResolver(new List<INotificationServiceV2>() { formNotService });

        //    var SendCorrService = new SendCorrespondenceHelper(CorrService.Object, logService.Object, logger.Object);
        //    var sendNotification = new Mock<ISendSecondNotifications>();


        //    AltinnFormDownloadAndRoutingService routing = new AltinnFormDownloadAndRoutingService(altinnService.Object, desrService, formShippingService.Object, logService.Object, formMetadataService.Object, formDistService, notificationServiceResolver, SendCorrService, filestorage.Object, sendNotification.Object, appLogService.Object, submittalQueueService.Object);

        //    string ArchivedFormTaskAsXml = System.IO.File.ReadAllText(@"Services\Altinntestmeldinger\AR3826763_2017-11-24.xml");
        //    ArchivedFormTaskDQBE formTask = SerializeUtil.DeserializeFromString<ArchivedFormTaskDQBE>(ArchivedFormTaskAsXml);


        //    routing.ShipAltinnForm("AR3826763", formTask, false);

        //    //Result - ok nå

        //}

        //*********************************************************************
        // Kan ikke kjøres da den er avhengig av Hangfire
        //*********************************************************************
        //[Fact(Skip = "bugfixtest")]
        //public void SendFeilFraFtbTest()
        //{
        //    //(Skip = "bugfixtest")
        //    var logService = new Mock<ILogEntryService>();
        //    var logger = new Mock<ILogger>();
        //    var formMetadataService = new Mock<IFormMetadataService>();
        //    var altinnService = new Mock<IAltinnService>();
        //    var altinnPrefillService = new Mock<IAltinnPrefillService>();
        //    var formShippingService = new Mock<IFormShippingService>();
        //    var CorrService = new Mock<IAltinnCorrespondenceService>();
        //    var decryptionFactory = new Mock<IDecryptionFactory>();
        //    decryptionFactory.Setup(f => f.GetDecryptor()).Returns(new Mock<IDecryption>().Object);

        //    var svarUtServicePrint = new Mock<ISvarUtService>();
        //    var svarUtServiceFactory = new Mock<ISvarUtServiceFactory>();
        //    svarUtServiceFactory.Setup(s => s.GetSvarUtServiceFor(It.IsAny<SvarUtServiceTypeEnum>()))
        //        .Returns(svarUtServicePrint.Object);

        //    var svarUtStatusService = new Mock<ISvarUtStatusService>();

        //    var appLogService = new Mock<IApplicationLogService>();
        //    var submittalQueueService = new Mock<ExportToSubmittalQueueService>();

        //    //var client = new Mock<IBackgroundJobClient>();

        //    //BackgroundJob.Enqueue(() => PurgeItem(archiveReference));
        //    //altinnService.Setup(s => s.GetPdfForms(It.IsAny<FormData>())).Returns(new Attachment() {  AttachmentType = "application_pdf", AttachmentData = System.IO.File.ReadAllBytes(@"Services\Altinntestmeldinger\5154-nabovarsel.pdf") });

        //    //Sende ok på kvitteringsmelding
        //    CorrService.Setup(s => s.SendNotification(It.IsAny<string>(), It.IsAny<InsertCorrespondenceV2>())).Returns(new ReceiptExternal() { ReceiptStatusCode = ReceiptStatusEnum.OK });
        //    var syncService = new Mock<ISyncRecordsOfCombinedDistributions>();
        //    UserNotificationHandler userNotificationHandler = new UserNotificationHandler(new UserNotificationService(new ApplicationDbContext()));

        //    SendPrefillFormServiceV2 sendPrefillService = new SendPrefillFormServiceV2(CorrService.Object, altinnPrefillService.Object, logService.Object, formMetadataService.Object, decryptionFactory.Object, svarUtServiceFactory.Object, userNotificationHandler, syncService.Object, svarUtStatusService.Object);
        //    IAltinnFormDeserializer desrService = new AltinnFormDeserializer();
        //    IFileStorage iFileStorage = new FileStorage(new BlobStorage());
        //    IFileDownloadStatusService fileDownloadStatusService = new FileDownloadStatusService(new ApplicationDbContext());


        //    FormDistributionServiceV2 formDistService = new FormDistributionServiceV2(CorrService.Object, logService.Object, formMetadataService.Object, sendPrefillService, decryptionFactory.Object, iFileStorage, fileDownloadStatusService);
        //    var filestorage = new Mock<IFileStorage>();
        //    var filedownloadStatus = new Mock<IFileDownloadStatusService>();
        //    NotificationServiceV2 formNotService = new NotificationServiceV2(CorrService.Object, logService.Object, formMetadataService.Object, filestorage.Object, filedownloadStatus.Object, syncService.Object, new Mock<ILogger>().Object);
        //    INotificationServiceResolver notificationServiceResolver = new NotificationServiceResolver(new List<INotificationServiceV2>() { formNotService });


        //    var SendCorrService = new SendCorrespondenceHelper(CorrService.Object, logService.Object, logger.Object);
        //    var sendNotification = new Mock<ISendSecondNotifications>();

        //    AltinnFormDownloadAndRoutingService routing = new AltinnFormDownloadAndRoutingService(altinnService.Object, desrService, formShippingService.Object, logService.Object, formMetadataService.Object, formDistService, notificationServiceResolver, SendCorrService, filestorage.Object, sendNotification.Object, appLogService.Object, submittalQueueService.Object);

        //    string ArchivedFormTaskAsXml = System.IO.File.ReadAllText(@"Services\Altinntestmeldinger\3816890.xml");
        //    ArchivedFormTaskDQBE formTask = SerializeUtil.DeserializeFromString<ArchivedFormTaskDQBE>(ArchivedFormTaskAsXml);

        //    routing.SendToNotification("AR3816890", formTask);

        //    //Result - Object reference not set to an instance of an object. - nå ok

        //}

        //*********************************************************************
        // Kan ikke kjøres da den er avhengig av Hangfire
        //*********************************************************************
        //[Fact(Skip = "bugfixtest")]
        //public void SendFeilNabovarselFraAmbitaTest()
        //{

        //    //(Skip = "bugfixtest")
        //    var logService = new Mock<ILogEntryService>();
        //    var logger = new Mock<ILogger>();
        //    var formMetadataService = new Mock<IFormMetadataService>();
        //    var altinnService = new Mock<IAltinnService>();
        //    var altinnPrefillService = new Mock<IAltinnPrefillService>();
        //    var formShippingService = new Mock<IFormShippingService>();
        //    var CorrService = new Mock<IAltinnCorrespondenceService>();
        //    var decryptionFactory = new Mock<IDecryptionFactory>();
        //    decryptionFactory.Setup(f => f.GetDecryptor()).Returns(new Mock<IDecryption>().Object);
        //    var svarUtServicePrint = new Mock<ISvarUtService>();
        //    var svarUtServiceFactory = new Mock<ISvarUtServiceFactory>();
        //    svarUtServiceFactory.Setup(s => s.GetSvarUtServiceFor(It.IsAny<SvarUtServiceTypeEnum>()))
        //        .Returns(svarUtServicePrint.Object);

        //    var svarUtStatusService = new Mock<ISvarUtStatusService>();
        //    var appLogService = new Mock<IApplicationLogService>();
        //    var submittalQueueService = new Mock<ExportToSubmittalQueueService>();

        //    //var client = new Mock<IBackgroundJobClient>();

        //    //BackgroundJob.Enqueue(() => PurgeItem(archiveReference));
        //    //altinnService.Setup(s => s.GetPdfMainForm(It.IsAny<string>())).Returns(new Attachment() { AttachmentType = "application_pdf", AttachmentData = System.IO.File.ReadAllBytes(@"Services\Altinntestmeldinger\5154-nabovarsel.pdf") });

        //    //Sende ok på kvitteringsmelding
        //    CorrService.Setup(s => s.SendNotification(It.IsAny<string>(), It.IsAny<InsertCorrespondenceV2>())).Returns(new ReceiptExternal() { ReceiptStatusCode = ReceiptStatusEnum.OK });
        //    var syncService = new Mock<ISyncRecordsOfCombinedDistributions>();
        //    UserNotificationHandler userNotificationHandler = new UserNotificationHandler(new UserNotificationService(new ApplicationDbContext()));

        //    SendPrefillFormServiceV2 sendPrefillService = new SendPrefillFormServiceV2(CorrService.Object, altinnPrefillService.Object, logService.Object, formMetadataService.Object, decryptionFactory.Object, svarUtServiceFactory.Object, userNotificationHandler, syncService.Object, svarUtStatusService.Object);
        //    IAltinnFormDeserializer desrService = new AltinnFormDeserializer();
        //    IFileStorage iFileStorage = new FileStorage(new BlobStorage());
        //    IFileDownloadStatusService fileDownloadStatusService = new FileDownloadStatusService(new ApplicationDbContext());


        //    FormDistributionServiceV2 formDistService = new FormDistributionServiceV2(CorrService.Object, logService.Object, formMetadataService.Object, sendPrefillService, decryptionFactory.Object, iFileStorage, fileDownloadStatusService);
        //    var filestorage = new Mock<IFileStorage>();
        //    var filedownloadStatus = new Mock<IFileDownloadStatusService>();
        //    NotificationServiceV2 formNotService = new NotificationServiceV2(CorrService.Object, logService.Object, formMetadataService.Object, filestorage.Object, filedownloadStatus.Object, syncService.Object, new Mock<ILogger>().Object);
        //    INotificationServiceResolver notificationServiceResolver = new NotificationServiceResolver(new List<INotificationServiceV2>() { formNotService });


        //    var SendCorrService = new SendCorrespondenceHelper(CorrService.Object, logService.Object, logger.Object);
        //    var sendNotification = new Mock<ISendSecondNotifications>();


        //    AltinnFormDownloadAndRoutingService routing = new AltinnFormDownloadAndRoutingService(altinnService.Object, desrService, formShippingService.Object, logService.Object, formMetadataService.Object, formDistService, notificationServiceResolver, SendCorrService, filestorage.Object, sendNotification.Object, appLogService.Object, submittalQueueService.Object);

        //    string ArchivedFormTaskAsXml = System.IO.File.ReadAllText(@"Services\Altinntestmeldinger\AR3710461_2017-11-24.xml");
        //    ArchivedFormTaskDQBE formTask = SerializeUtil.DeserializeFromString<ArchivedFormTaskDQBE>(ArchivedFormTaskAsXml);

        //    routing.SendToDistribution("AR3710461", formTask);

        //    //Result - nå ok - kan servicecode mangle i octopus liste?

        //}
        //DistribusjonAnsvarsrett AR3710461_2017-11-24.xml AR3819522_2017-11-24.xml



        //Samsvarserklæring AR3753631_2017-11-24.xml



        //Svar på nabovarsel AR3777849_2017-11-24.xml AR3816890_2017-11-24.xml

        //*********************************************************************
        // Kan ikke kjøres da den er avhengig av Hangfire
        //*********************************************************************
        //[Fact(Skip = "bugfixtest")]
        //public void SendRammesøknad2FeilFraEbsTest()
        //{

        //    //(Skip = "bugfixtest")
        //    var logService = new Mock<ILogEntryService>();
        //    var logger = new Mock<ILogger>();
        //    var formMetadataService = new Mock<IFormMetadataService>();
        //    var altinnService = new Mock<IAltinnService>();
        //    var altinnPrefillService = new Mock<IAltinnPrefillService>();
        //    var kommuneService = new Mock<IMunicipalityService>();
        //    var decryptionFactory = new Mock<IDecryptionFactory>();
        //    var decryptFnr = new Mock<IDecryptFnr>();
        //    var addSignatureDocument = new Mock<ITiltakshaversSignedApprovalDocument>();
        //    decryptionFactory.Setup(f => f.GetDecryptor()).Returns(new Mock<IDecryption>().Object);

        //    var svarUtServicePrint = new Mock<ISvarUtService>();
        //    svarUtServicePrint.Setup(c => c.IsServiceFor).Returns(SvarUtServiceTypeEnum.Print);

        //    var svarUtService = new Mock<ISvarUtService>();
        //    svarUtServicePrint.Setup(c => c.IsServiceFor).Returns(SvarUtServiceTypeEnum.Shipping);

        //    var svarUtServices = new List<ISvarUtService>();
        //    svarUtServices.Add(svarUtServicePrint.Object);
        //    svarUtServices.Add(svarUtService.Object);
        //    var svarUtServiceFactory = new SvarUtServiceFactory(svarUtServices);
        //    var ilogentry = new Mock<ILogEntryService>();


        //    var svarUtStatusService = new Mock<ISvarUtStatusService>();

        //    kommuneService.Setup(s => s.GetMunicipality(It.IsAny<string>())).Returns(new Municipality() { Code = "9999", Name = "Test", OrganizationNumber = "910297937" });

        //    IFormShippingService formShippingService = new FormShippingService(svarUtServiceFactory, formMetadataService.Object, kommuneService.Object, decryptFnr.Object, ilogentry.Object, addSignatureDocument.Object);
        //    var CorrService = new Mock<IAltinnCorrespondenceService>();
        //    var syncService = new Mock<ISyncRecordsOfCombinedDistributions>();
        //    var appLogService = new Mock<IApplicationLogService>();
        //    var submittalQueueService = new Mock<ExportToSubmittalQueueService>();

        //    //altinnService.Setup(s => s.GetPdfMainForm(It.IsAny<string>())).Returns(new Attachment() { AttachmentType = "application_pdf", AttachmentData = System.IO.File.ReadAllBytes(@"Services\Altinntestmeldinger\5154-nabovarsel.pdf"), FileName= "5154-nabovarsel.pdf" });



        //    //Sende ok på kvitteringsmelding
        //    CorrService.Setup(s => s.SendNotification(It.IsAny<string>(), It.IsAny<InsertCorrespondenceV2>())).Returns(new ReceiptExternal() { ReceiptStatusCode = ReceiptStatusEnum.OK });
        //    UserNotificationHandler userNotificationHandler = new UserNotificationHandler(new UserNotificationService(new ApplicationDbContext()));

        //    SendPrefillFormServiceV2 sendPrefillService = new SendPrefillFormServiceV2(CorrService.Object, altinnPrefillService.Object, logService.Object, formMetadataService.Object, decryptionFactory.Object, svarUtServiceFactory, userNotificationHandler, syncService.Object, svarUtStatusService.Object);
        //    IAltinnFormDeserializer desrService = new AltinnFormDeserializer();
        //    IFileStorage iFileStorage = new FileStorage(new BlobStorage());
        //    IFileDownloadStatusService fileDownloadStatusService = new FileDownloadStatusService(new ApplicationDbContext());


        //    FormDistributionServiceV2 formDistService = new FormDistributionServiceV2(CorrService.Object, logService.Object, formMetadataService.Object, sendPrefillService, decryptionFactory.Object, iFileStorage, fileDownloadStatusService);
        //    var filestorage = new Mock<IFileStorage>();
        //    var filedownloadStatus = new Mock<IFileDownloadStatusService>();
        //    NotificationServiceV2 formNotService = new NotificationServiceV2(CorrService.Object, logService.Object, formMetadataService.Object, filestorage.Object, filedownloadStatus.Object, syncService.Object, new Mock<ILogger>().Object);
        //    INotificationServiceResolver notificationServiceResolver = new NotificationServiceResolver(new List<INotificationServiceV2>() { formNotService });

        //    var SendCorrService = new SendCorrespondenceHelper(CorrService.Object, logService.Object, logger.Object);
        //    var sendNotification = new Mock<ISendSecondNotifications>();

        //    AltinnFormDownloadAndRoutingService routing = new AltinnFormDownloadAndRoutingService(altinnService.Object, desrService, formShippingService, logService.Object, formMetadataService.Object, formDistService, notificationServiceResolver, SendCorrService, filestorage.Object, sendNotification.Object, appLogService.Object, submittalQueueService.Object);

        //    string ArchivedFormTaskAsXml = System.IO.File.ReadAllText(@"Services\Altinntestmeldinger\AR3841690.xml");
        //    ArchivedFormTaskDQBE formTask = SerializeUtil.DeserializeFromString<ArchivedFormTaskDQBE>(ArchivedFormTaskAsXml);



        //    routing.ShipAltinnForm("AR3841690", formTask, false);

        //    //Result - En feil oppstod under sending til FIKS SvarUt:Sequence contains no matching element - ok nå - pga ingen egne innstillinger på svarut titler for kommunenr 99xx serien og ikke treff på subforms dokumenttype

        //}
    }
}