﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using KommIT.FIKS.AdapterAltinnSvarUt.Services;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.NotificationForm;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.SvarUt;
using KommIT.FIKS.AdapterAltinnSvarUt.Tests.Services.Builders;
using no.kxml.skjema.dibk.ansvarsrettDirekteOpprettet;
using Xunit;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Tests.Services
{
    public class ErklaeringAnsvarsrettV2Tests
    {
        [Fact(DisplayName = "4965_2_4_AnsvarligForetakGyldigOrganisasjonsNrOkTest")]
        public void ErklaeringAnsvarsrettV2_4965_2_4_AnsvarligForetakGyldigOrganisasjonsNrOKTest()
        {
            // Må sendes av ansvarlig foretak med gyldig organisasjonsnummer Feil
            var val = new ErklaeringAnsvarsrettV2Validator();
            var erklaeringAnsvarsrettV2 = new ErklaeringAnsvarsrettV2Builder()
                .AnsvarligForetak("974760223", "Byggmester Bob", "3800", "Bø")
                .Build();
            val.AnsvarligForetakGyldigOrganisasjonsNr(erklaeringAnsvarsrettV2);
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);

        }
        [Fact(DisplayName = "4965_2_4_AnsvarligForetakGyldigOrganisasjonsNrErrorTest")]
        public void ErklaeringAnsvarsrettV2_4965_2_4_AnsvarligForetakGyldigOrganisasjonsNrErrorTest()
        {
            // Må sendes av ansvarlig foretak med gyldig organisasjonsnummer Feil
            var val = new ErklaeringAnsvarsrettV2Validator();
            var erklaeringAnsvarsrettV2 = new ErklaeringAnsvarsrettV2Builder()

                .Build();
            val.AnsvarligForetakGyldigOrganisasjonsNr(erklaeringAnsvarsrettV2);
            var result = val.GetResult();

            result.messages.Where(m => m.reference.Contains("4965.2.4")).Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4965_2_5_AnsvarligSoekerGyldigOrganisasjonsNrOkTest")]
        public void ErklaeringAnsvarsrettV2_4965_2_5_AnsvarligSoekerGyldigOrganisasjonsNrOKTest()
        {
            // Må sendes til ansvarlig søker med gyldig organisasjonsnummer
            var val = new ErklaeringAnsvarsrettV2Validator();
            var erklaeringAnsvarsrettV2 = new ErklaeringAnsvarsrettV2Builder()
                .AnsvarligSoeker("974760223", "Arkitekt Flink", "3800", "Bø","Benjamin Fjell")
                .Build();
            val.AnsvarligSoekerGyldigOrganisasjonsNr(erklaeringAnsvarsrettV2);
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }

        [Fact(DisplayName = "4965_2_5_AnsvarligSoekerGyldigOrganisasjonsNrErrorTest")]
        public void ErklaeringAnsvarsrettV2_4965_2_5_AnsvarligSoekerGyldigOrganisasjonsNrErrorTest()
        {
            // Må sendes til ansvarlig søker med gyldig organisasjonsnummer
            var val = new ErklaeringAnsvarsrettV2Validator();
            var erklaeringAnsvarsrettV2 = new ErklaeringAnsvarsrettV2Builder()
                .AnsvarligSoeker("974s60223", "Arkitekt Flink", "3800", "Bø", "Benjamin Fjell")
                
                .Build();
            val.AnsvarligSoekerGyldigOrganisasjonsNr(erklaeringAnsvarsrettV2);
            var result = val.GetResult();

            result.messages.Where(m => m.reference.Contains("4965.2.5")).Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }


        [Fact(DisplayName = "4965.2.7 - Teste DirekteAnsvarsrett som underskjema til Gjennomforingsplan")]
        public void FailCheckDirekteAnsvarsrettWhenItIsSubForForGjennomforingsPLan()
        {
            var val = new ErklaeringAnsvarsrettV2Validator();
            var erklaeringAnsvarsrettV2 = new ErklaeringAnsvarsrettV2Builder()
                .AnsvarligSoeker("910297937", "FANA OG HAFSLO REVISJON", "3800", "Bø", "Benjamin Fjell")
                .AnsvarligForetak("910297938", "IKKE FANA OG HAFSLO REVISJON", "3800", "Bø")
                .Build();


            val.AnsvarligSokerOgForetakValidering(erklaeringAnsvarsrettV2, new List<string>(){"Gjennomføringsplan"});
            var result = val.GetResult();

            result.messages.Where(m => m.reference.Contains("4965.2.7")).Should().NotBeNullOrEmpty();
            result.Errors.Should().Be(1);
            result.messages.Count.Should().Be(1);


        }


    }
}
