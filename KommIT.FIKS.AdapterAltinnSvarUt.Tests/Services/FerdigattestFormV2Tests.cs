﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using FluentAssertions;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.FormsSamples;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.SvarUt;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.SvarUtForm;
using KommIT.FIKS.AdapterAltinnSvarUt.Tests.Services.Builders;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;
using no.kxml.skjema.dibk.ferdigattestV2;
using Xunit;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Tests.Services
{
    public class FerdigattestFormV2Tests
    {
        private Skjema _formSkjema;
        private List<string> _formSetElements;
        private FerdigattestType _form;
        private FerdigattestFormV2Validator _validator;

        public FerdigattestFormV2Tests()
        {
            _formSkjema = new FerdigattestV2Dfv43192().FerdigattestV2_02(true);
            _formSetElements = new List<string>() { "TegningNyttSnitt", "SluttrapportForBygningsavfall", "KvitteringForLevertAvfall" };
            _form = SerializeUtil.DeserializeFromString<FerdigattestType>(_formSkjema.Data);
            _validator = new FerdigattestFormV2Validator();
        }

        [Fact]
        public void FerdigattestValidateOkTest()
        {
            var formDataAsXml = File.ReadAllText(@"Services\testskjema\5855_43192_ferdigattestV2.xml");
            FerdigattestFormV2 form = new FerdigattestFormV2();
            var val = new FerdigattestFormV2Validator();
            val.MunicipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");

            form.Validator = val;

            form.InitiateForm(formDataAsXml);
            var result = form.ValidateData();
            result.rulesChecked.Should().NotBeNull();
        }

        [Fact(DisplayName = "4400_2_13_EiendomByggestedValidationOkTest")]
        public void Ferdigattest_4400_5_1_EiendomByggestedValidationOkTest()
        {
            // Hvis bruksenhetsnummer(bolignummer) er fylt ut må det følge riktig format (^[HKLU]\\d{4}$) (feks H0101) se https://www.kartverket.no/eiendom/adressering/bruksenhetsnummer/
            var val = new FerdigattestFormV2Validator();
            val.MunicipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");

            var ferdigattest = new FerdigattestV2Builder()
                .EiendomByggested("2004", "Hammerfest", "H0101")
                .Build();
            val.EiendomByggestedValidation(ferdigattest);
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }

        [Fact(DisplayName = "4400_2_13_EiendomByggestedValidationErrorTest")]
        public void Ferdigattest_4400_5_1_EiendomByggestedValidationErrorTest()
        {
            // Hvis bruksenhetsnummer(bolignummer) er fylt ut må det følge riktig format (^[HKLU]\\d{4}$) (feks H0101) se https://www.kartverket.no/eiendom/adressering/bruksenhetsnummer/
            var val = new FerdigattestFormV2Validator();
            val.MunicipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");

            var ferdigattest = new FerdigattestV2Builder()
                .EiendomByggested("2004", "Hammerfest", "X0101")
                .Build();
            val.EiendomByggestedValidation(ferdigattest);
            var result = val.GetResult();

            result.messages.Where(m => m.reference.Contains("4400.2.13")).Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4400_1_2_EiendomByggestedValidationOKTest")]
        public void Ferdigattest_4400_1_2_EiendomByggestedValidationOKTest()
        {
            // Kommunenavn bør fylles ut for eiendom/byggested
            var val = new FerdigattestFormV2Validator();
            val.MunicipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");

            var ferdigattest = new FerdigattestV2Builder()
                .EiendomByggested("2004", "Hammerfest", "H0101")
                .Build();
            val.EiendomByggestedValidation(ferdigattest);
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }

        [Fact(DisplayName = "4400_1_2_EiendomByggestedValidationErrorTest")]
        public void Ferdigattest_4400_1_2_EiendomByggestedValidationErrorTest()
        {
            // Kommunenavn bør fylles ut for eiendom/byggested
            var val = new FerdigattestFormV2Validator();
            val.MunicipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");

            var ferdigattest = new FerdigattestV2Builder()
                .EiendomByggested("2004", null, "H0101")
                .Build();
            val.EiendomByggestedValidation(ferdigattest);
            var result = val.GetResult();

            result.messages.Where(m => m.reference.Contains("4400.1.2.1")).Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4400_1_2_EiendomByggestedValidationKmnOKTest")]
        public void Ferdigattest_4400_1_2_EiendomByggestedValidationKmnOKTest()
        {
            // Ugyldig kommunenummer
            var val = new FerdigattestFormV2Validator();
            val.MunicipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");

            var ferdigattest = new FerdigattestV2Builder()
                .EiendomByggested("2004", "Hammerfest", "H0101")
                .Build();
            val.EiendomByggestedValidation(ferdigattest);
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }

        [Fact(DisplayName = "4400_1_2_EiendomByggestedValidationKmnErrorTest")]
        public void Ferdigattest_4400_1_2_EiendomByggestedValidationKmnErrorTest()
        {
            // Ugyldig kommunenummer
            var val = new FerdigattestFormV2Validator();
            val.MunicipalityValidator = TestHelper.MokMunicipalityValidatorResult("invalid");

            var ferdigattest = new FerdigattestV2Builder()
                .EiendomByggested("12004", "Hammerfest", "H0101")
                .Build();
            val.EiendomByggestedValidation(ferdigattest);
            var result = val.GetResult();

            result.messages.Where(m => m.reference.Contains("4400.1.2")).Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4400.2.5.5 SoekerValidationOrgErrorTest")]
        public void Ferdigattest_4400_2_5_SoekerValidationOrgErrorTest()
        {
            _validator.PostalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "OSLO");

            _form.ansvarligSoeker.partstype.kodeverdi = "Foretak";
            _form.ansvarligSoeker.organisasjonsnummer = "*974760673";
            _validator.SoekerValidation(_form);

            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4400.2.5.5").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4400.2.5.3 SoekerValidationFoedErrorTest")]
        public void Ferdigattest_4400_2_5_3_SoekerValidationFoedErrorTest()
        {
            _validator.PostalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "OSLO");

            _form.ansvarligSoeker.partstype.kodeverdi = "Privatperson";
            _form.ansvarligSoeker.foedselsnummer = "*25125401530";
            _validator.SoekerValidation(_form);

            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4400.2.5.3").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4400.2.14.1 SoekerValidation - Error")]
        public void Ferdigattest_4400_2_14_1_SoekerValidationFoedErrorTest()
        {
            _validator.PostalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "OSLO");

            _form.ansvarligSoeker.partstype.kodeverdi = "Foretak";
            _form.ansvarligSoeker.kontaktperson = null;
            _validator.SoekerValidation(_form);

            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4400.2.14.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4400.2.15 SoekerValidation - Error")]
        public void Ferdigattest_4400_2_15_SoekerValidationFoedErrorTest()
        {

            _validator.PostalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "OSLO");

            _form.ansvarligSoeker.partstype.kodeverdi = "Foretak";
            _form.ansvarligSoeker.telefonnummer = null;
            _form.ansvarligSoeker.mobilnummer = null;
            _validator.SoekerValidation(_form);

            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4400.2.15").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4400_1_6_fraSluttbrukersystemValidationOKTest")]
        public void Ferdigattest_4400_1_6_fraSluttbrukersystemValidationOKTest()
        {
            // Må fylle ut fraSluttbrukersystem med samme navn som brukt i registrering for Altinn API Feil
            var val = new FerdigattestFormV2Validator();

            var ferdigattest = new FerdigattestV2Builder()
                .FraSluttbrukersystem("Fellestjenester bygg testmotor")
                .Build();
            val.FraSluttbrukersystemValidation(ferdigattest);
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }

        [Fact(DisplayName = "4400_1_6_fraSluttbrukersystemValidationErrorTest")]
        public void Ferdigattest_4400_1_6_fraSluttbrukersystemValidationErrorTest()
        {
            // Må fylle ut fraSluttbrukersystem med samme navn som brukt i registrering for Altinn API Feil
            var val = new FerdigattestFormV2Validator();

            var ferdigattest = new FerdigattestV2Builder()
                .Build();
            val.FraSluttbrukersystemValidation(ferdigattest);
            var result = val.GetResult();

            result.messages.Where(m => m.reference.Contains("4400.1.6")).Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4400_2_7_SoknadsDelerAvTiltakenValideringOKTest")]
        public void Ferdigattest_4400_2_7_SoknadsDelerAvTiltakenValideringOKTest()
        {
            // Når tiltaket ikke tilfredsstiller kravene til ferdigattest må utført innen, type arbeider og bekreftelse innen fylles ut.  Feil
            var val = new FerdigattestFormV2Validator();

            var ferdigattest = new FerdigattestV2Builder()
                .HarTilstrekkeligSikkerhet(false, "Montert rekkverk på trapp", DateTime.Now.AddDays(10), DateTime.Now.AddDays(10))
                .Build();
            val.HartilfredstillerTiltaketKraveneFerdigattest(ferdigattest);
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }

        [Fact(DisplayName = "4400_2_7_SoknadsDelerAvTiltakenValideringErrorTest")]
        public void Ferdigattest_4400_2_7_SoknadsDelerAvTiltakenValideringErrorTest()
        {
            // Når tiltaket ikke tilfredsstiller kravene til ferdigattest må utført innen, type arbeider og bekreftelse innen fylles ut.  Feil
            var val = new FerdigattestFormV2Validator();

            var ferdigattest = new FerdigattestV2Builder()
                .HarTilstrekkeligSikkerhet(false, null, DateTime.Now.AddDays(10), DateTime.Now.AddDays(10))
                .Build();
            val.HartilfredstillerTiltaketKraveneFerdigattest(ferdigattest);
            var result = val.GetResult();

            result.messages.Where(m => m.reference.Contains("4400.2.7")).Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4400_2_8_SoknadsDelerAvTiltakenValideringOKTest")]
        public void Ferdigattest_4400_2_8_SoknadsDelerAvTiltakenValideringOKTest()
        {
            // Følgende arbeider vil bli utført innen kan være maks om 14 dager Feil
            var val = new FerdigattestFormV2Validator();

            var ferdigattest = new FerdigattestV2Builder()
                .HarTilstrekkeligSikkerhet(false, "Montert rekkverk på trapp", DateTime.Now.AddDays(10), DateTime.Now.AddDays(10))
                .Build();
            val.HartilfredstillerTiltaketKraveneFerdigattest(ferdigattest);
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }

        [Fact(DisplayName = "4400_2_8_SoknadsDelerAvTiltakenValideringErrorTest")]
        public void Ferdigattest_4400_2_8_SoknadsDelerAvTiltakenValideringErrorTest()
        {
            // Følgende arbeider vil bli utført innen kan være maks om 14 dager Feil
            var val = new FerdigattestFormV2Validator();

            var ferdigattest = new FerdigattestV2Builder()
                .HarTilstrekkeligSikkerhet(false, "Montert rekkverk på trapp", DateTime.Now.AddDays(15), DateTime.Now.AddDays(10))
                .Build();
            val.HartilfredstillerTiltaketKraveneFerdigattest(ferdigattest);
            var result = val.GetResult();

            result.messages.Where(m => m.reference.Contains("4400.2.8")).Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4400_2_9_SoknadsDelerAvTiltakenValideringOKTest")]
        public void Ferdigattest_4400_2_9_SoknadsDelerAvTiltakenValideringOKTest()
        {
            // Bekreftelse på at disse arbeidene er utført vil være kommunen i hende innen kan være maks om 14 dager	Feil
            var val = new FerdigattestFormV2Validator();

            var ferdigattest = new FerdigattestV2Builder()
                .HarTilstrekkeligSikkerhet(false, "Montert rekkverk på trapp", DateTime.Now.AddDays(10), DateTime.Now.AddDays(10))
                .Build();
            val.HartilfredstillerTiltaketKraveneFerdigattest(ferdigattest);
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }

        [Fact(DisplayName = "4400_2_9_SoknadsDelerAvTiltakenValideringErrorTest")]
        public void Ferdigattest_4400_2_9_SoknadsDelerAvTiltakenValideringErrorTest()
        {
            // Bekreftelse på at disse arbeidene er utført vil være kommunen i hende innen kan være maks om 14 dager	Feil
            var val = new FerdigattestFormV2Validator();

            var ferdigattest = new FerdigattestV2Builder()
                .HarTilstrekkeligSikkerhet(false, "Montert rekkverk på trapp", DateTime.Now.AddDays(10), DateTime.Now.AddDays(15))
                .Build();
            val.HartilfredstillerTiltaketKraveneFerdigattest(ferdigattest);
            var result = val.GetResult();

            result.messages.Where(m => m.reference.Contains("4400.2.9")).Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4400_2_11_KommunensSaksnummerValidatorOKTest")]
        public void Ferdigattest_4400_2_11_KommunensSaksnummerValidatorOKTest()
        {
            // Kommunens saksnummer må være fylt ut.	Feil
            var val = new FerdigattestFormV2Validator();

            var ferdigattest = new FerdigattestV2Builder()
                .KommunensSaksnummer("2015", "123")
                .Build();
            val.KommunensSaksnummerValidator(ferdigattest);
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }

        [Fact(DisplayName = "4400_2_11_KommunensSaksnummerValidatorErrorTest")]
        public void Ferdigattest_4400_2_11_KommunensSaksnummerValidatorErrorTest()
        {
            // Kommunens saksnummer må være fylt ut.	Feil
            var val = new FerdigattestFormV2Validator();

            var ferdigattest = new FerdigattestV2Builder()
                .KommunensSaksnummer(null, "123")
                .Build();
            val.KommunensSaksnummerValidator(ferdigattest);
            var result = val.GetResult();

            result.messages.Where(m => m.reference.Contains("4400.2.11")).Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4400_2_12_foretattIkkeSoeknadspliktTegningValideringOKTest")]
        public void Ferdigattest_4400_2_12_foretattIkkeSoeknadspliktigeJusteringerValifatoTegningValideringOKTest()
        {
            //Vedlegg for Tegning ny plan, Tegning ny fasade, Tegning nytt snitt skal bare sendes inn om de er knyttet til vilkår eller inneholder ikke søknadspliktige endringer
            var val = new FerdigattestFormV2Validator();

            var ferdigattest = new FerdigattestV2Builder()
                .KommunensSaksnummer(null, "123")
                .foretattIkkeSoeknadspliktigeJusteringer(true)
                .Build();
            val.ForetattIkkeSoeknadspliktigeJusteringerValifatoTegningValidering(ferdigattest, new List<string>() { "TegningNyPlan", "TegningNyFasade", "TegningNyttSnitt" });
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }

        [Fact(DisplayName = "4400_2_12_foretattIkkeSoeknadspliktTegningValideringErrorTest")]
        public void Ferdigattest_4400_2_12_foretattIkkeSoeknadspliktigeJusteringerValifatoTegningValideringErrorTest()
        {
            //Vedlegg for Tegning ny plan, Tegning ny fasade, Tegning nytt snitt skal bare sendes inn om de er knyttet til vilkår eller inneholder ikke søknadspliktige endringer
            var val = new FerdigattestFormV2Validator();

            var ferdigattest = new FerdigattestV2Builder()
                .KommunensSaksnummer(null, "123")
                .foretattIkkeSoeknadspliktigeJusteringer(true)
                .Build();
            val.ForetattIkkeSoeknadspliktigeJusteringerValifatoTegningValidering(ferdigattest, new List<string>() { "nnansd", "sdfasdf" });
            var result = val.GetResult();

            result.messages.Where(m => m.reference.Contains("4400.2.12")).Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4400_2_14_VeielappVedleggValideringErrorTest")]
        public void Ferdigattest_4400_2_12_VeielappVedleggValideringErrorTest()
        {
            //Vedlegg for Tegning ny plan, Tegning ny fasade, Tegning nytt snitt skal bare sendes inn om de er knyttet til vilkår eller inneholder ikke søknadspliktige endringer
            var val = new FerdigattestFormV2Validator();

            var ferdigattest = new FerdigattestV2Builder()
                .Build();

            val.KvitteringForLevertAvfallVedleggValidering(ferdigattest, new List<string>() { "SluttrapportForBygningsavfall", "*KvitteringForLevertAvfall" });

            var result = val.GetResult();

            result.messages.Where(m => m.reference.Contains("4400.2.14")).Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4400_2_16_AvfallVedleggValideringErrorTest")]
        public void Ferdigattest_4400_2_16_AvfallVedleggValideringErrorTest()
        {
            var val = new FerdigattestFormV2Validator();

            var ferdigattest = new FerdigattestV2Builder()
                .Build();
            val.AvfallVedleggValidering(ferdigattest, new List<string>() { "*SluttrapportForBygningsavfall", "TegningNyttSnitt" });
            var result = val.GetResult();

            result.messages.Where(m => m.reference.Contains("4400.2.16")).Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4400.1.1.4 SoekerValidation - Error")]
        public void Ferdigattest_4400_1_1_4_SoekerValidationFoedErrorTest()
        {
            _validator.PostalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "OSLO");

            _form.ansvarligSoeker = null;
            _form.tiltakshaver = null;

            _validator.SoekerValidation(_form);

            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4400.1.1.4").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4400.2.6 SoekerValidation - Error")]
        public void Ferdigattest_4400_2_6_SoekerValidationFoedErrorTest()
        {
            _validator.PostalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "OSLO");

            _form.ansvarligSoeker = null;
            _form.tiltakshaver.partstype = null;

            _validator.SoekerValidation(_form);

            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4400.2.6").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "Test data", Skip = "test Data")]
        public void FerdigattestV2Test()
        {
            _validator.PostalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "OSLO");
            _validator.MunicipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");

            _validator.Validate(_form, _formSetElements);

            var result = _validator.GetResult();

            var postmanJson = TestHelper.GetValidateFormv2FormSkjema(_formSkjema, _formSetElements);

            result.messages.Where(m => m.reference == "4400.2.6").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
    }
}
