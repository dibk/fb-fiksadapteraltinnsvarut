﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Altinn;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Encryption.TextEncyption;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Prefill;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.Builders;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.FormsSamples;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;
using Xunit;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Tests.Services
{
    public class GjenpartNabovarselDecoderTests
    {
        [Fact(DisplayName = "Get list of fødselnummer", Skip = "need test data")]
        public void Gjenpartnabovarsel()
        {

            List<Subform> subforms = new List<Subform>()
            {
                new Subform("5786", "42725", "xml"),
                new Subform("5785", "42725", "xml"),
                new Subform("5784", "42725", "xml"),
                new Subform("5783", "42725", "xml"),
                new Subform("5782", "42725", "xml"),
                new Subform("5782", "42725", "xml"),
            };

            var gjentaprtNaboeier = subforms.First(atc => atc.DataFormatId == "5826");

            var rammetillatelse = new GjenpartnabovarselV2Builder()
                .Dispensasjon("PLAN", null, "Søknad om dispensasjon fra bestemmelsen NN i plan", "Begrunnelse for dispensasjon")
                .Build();

            //Gjennomføringsplan (DataFormatId: 5786, DataFormatVersion: 42725)
            var subfom = new Subform("5786", "42725", "xml") { };

            subforms.Add(subfom);


            var listOfNeighborSsn = GjenpartNabovarselDecoder.GetListOfNeighborSsn(subfom);

            listOfNeighborSsn.Should().NotBeEmpty();
        }

        [Fact(DisplayName = "Get Naboeir model til Kommune")]
        public void GetNaboeierModelTilKommune()
        {

            var listOfNeighborSsn = new List<NaboGjenboer>()
            {
                new NaboGjenboer()
                {
                    encryptedFoedselsnummer = "ncX7R6YaiC"
                },
                new NaboGjenboer()
                {
                    foedselsnummer = "mmk"
                },
                new NaboGjenboer()
                {
                    encryptedFoedselsnummer = "GbC/iNFIGvKsXaie0eGmMzXZluVJpqeGSXmVvCreyfv6p9kNg40VcDi6HgwNDwRiEEyGiltFUqG24VKgy8zxpImTHz81xGgqIirlzvKXI8BAdxM6ZtswShQiaCqDNXCo="
                },
                new NaboGjenboer()
                {
                    encryptedFoedselsnummer = "Wsp5hffjLdaOwlfPBvmQF0Wkl/a07YsiVnKU3bHDoDpBsYdUH2bfFGDMFlC7z7G8rYNUDSqhEaSuf9EO2J+bJLS2SEVnXAlJIJo9wv5Ac+jgsl19S4n9APuR1BhcxKoANZQ9rFH2N825eTBesX1Qo3pH8c8E6hu9XSCvHI+PR7E="
                },
                new NaboGjenboer()
                {
                    encryptedFoedselsnummer = "l44h8ujJgFYtx1sO2rU+LbH73oHPTNaa0QQkiAU/pDOOe0h1a8PmdfTAZe3uItUDFlOsFz27y96WD5JmFai3X8M31CZx1y975Xv0UYS4C/b4m7oMEUPFdsMcg+Z/yEzx+Z6Wll/lNIcSSaz15u2YBvw3+HKBbal/OQf+R5CBUFo="
                },
                new NaboGjenboer()
                {
                    encryptedFoedselsnummer = "dWzBOH1/7L63fLiNXsHd8+2rIVmjm7jwDDkIc6WtgddJSM16Rw8HOi8b2RIq8VoJPFDnMY6D0t75GjZKBUtdG/KHgdiN6oyIcrMsvBpuQZyg6Pfof8fmRya8XoTqBu6EKbnhHEv2BktyZ4mJIJzimfbBwt6mre/k2WIjofNsIv0="
                }
            };

            var naboeierTilKommune = GjenpartNabovarselDecoder.GetNaboeier(listOfNeighborSsn);
            naboeierTilKommune.Should().NotBeNull();
        }

        [Fact(DisplayName = "get attachment", Skip = "integration test")]
        public void GetFoedselNummer()
        {
            Naboeier naboeier = new Naboeier()
            {
                naboeier = new[]
                {
                    new NaboGjenboer()
                    {
                        encryptedFoedselsnummer = "dWzBOH1/7L63fLiNXsHd8+2rIVmjm7jwDDkIc6WtgddJSM16Rw8HO",
                        foedselsnummer = "12345678901"
                    },new NaboGjenboer()
                    {
                        encryptedFoedselsnummer = "dWzBOH1/7L63fLiNXsHd8+2rIVmjm7jwDDkIc6WtgddJSM16Rw8HO",
                        foedselsnummer = "23456789012"
                    },new NaboGjenboer()
                    {
                        encryptedFoedselsnummer = "dWzBOH1/7L63fLiNXsHd8+2rIVmjm7jwDDkIc6WtgddJSM16Rw8HO",
                        foedselsnummer = "34567890123"
                    },new NaboGjenboer()
                    {
                        encryptedFoedselsnummer = "dWzBOH1/7L63fLiNXsHd8+2rIVmjm7jwDDkIc6WtgddJSM16Rw8HO",
                        foedselsnummer = "5678901234"
                    },
                }
            };


            var gjentaprtNaboeier = GjenpartNabovarselDecoder.GetGjenpartNabovarselAttachement("archivId", naboeier);

            var bytes = gjentaprtNaboeier.AttachmentData;
            string str = Encoding.ASCII.GetString(bytes);

            var deserializeFromString = SerializeUtil.DeserializeFromString<Naboeier>(str);

            deserializeFromString.Should().NotBeNull();
        }
    }
}
