﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.NotificationForm;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.SvarUtForm.SubForm;
using KommIT.FIKS.AdapterAltinnSvarUt.Tests.Services.Builders;
using Xunit;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Tests.Services
{
    public class MatrikkelopplysningerV1Tests
    {
        [Fact(DisplayName = "4920_1_4_BygningValideringOKTest")]
        public void  Matrikkelopplysninger_4920_1_4_BygningValideringOKTest()
        {
            // Bygningstype skal angis ved Søknad om rammetillatelse, Søknad om igangsettingstillatelse, Søknad om tillatelse i ett trinn og Søknad om tillatelse til tiltak uten ansvarsrett	Feil
            var val = new MatrikkelopplysningerFormV1Validator();

            var matrikkelopplysninger = new MatrikkelopplysningerV1Builder()
                .Bygning("161")
                .Build();
            val.BygningValidering(matrikkelopplysninger, new List<string>() { });
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4920_1_4_BygningValideringErrorTest")]
        public void  Matrikkelopplysninger_4920_1_4_BygningValideringErrorTest()
        {
            // Bygningstype skal angis ved Søknad om rammetillatelse, Søknad om igangsettingstillatelse, Søknad om tillatelse i ett trinn og Søknad om tillatelse til tiltak uten ansvarsrett	Feil
            var val = new MatrikkelopplysningerFormV1Validator();

            var matrikkelopplysninger = new MatrikkelopplysningerV1Builder()
                .Bygning(null)
                .Build();
            val.BygningValidering(matrikkelopplysninger, new List<string>() { });
            var result = val.GetResult();

            result.messages.Where(m => m.reference.Contains("4920.1.4")).Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4920_1_5_BygningValideringOKTest")]
        public void  Matrikkelopplysninger_4920_1_5_BygningValideringOKTest()
        {
            // Næringsgruppe skal angis ved Søknad om rammetillatelse, Søknad om igangsettingstillatelse, Søknad om tillatelse i ett trinn og Søknad om tillatelse til tiltak uten ansvarsrett  Feil
            var val = new MatrikkelopplysningerFormV1Validator();

            var matrikkelopplysninger = new MatrikkelopplysningerV1Builder()
                .Bygning("161")
                .Build();
            val.BygningValidering(matrikkelopplysninger, new List<string>() { });
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4920_1_5_BygningValideringErrorTest")]
        public void  Matrikkelopplysninger_4920_1_5_BygningValideringErrorTest()
        {
            // Næringsgruppe skal angis ved Søknad om rammetillatelse, Søknad om igangsettingstillatelse, Søknad om tillatelse i ett trinn og Søknad om tillatelse til tiltak uten ansvarsrett  Feil
            var val = new MatrikkelopplysningerFormV1Validator();

            var matrikkelopplysninger = new MatrikkelopplysningerV1Builder()
                .Bygning("161",null)
                .Build();
            val.BygningValidering(matrikkelopplysninger, new List<string>() { });
            var result = val.GetResult();

            result.messages.Where(m => m.reference.Contains("4920.1.5")).Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4920_1_6_BygningValideringOKTest")]
        public void  Matrikkelopplysninger_4920_1_6_BygningValideringOKTest()
        {
            // Bebygd areal skal angis ved Søknad om rammetillatelse, Søknad om igangsettingstillatelse, Søknad om tillatelse i ett trinn og Søknad om tillatelse til tiltak uten ansvarsrett
            var val = new MatrikkelopplysningerFormV1Validator();

            var matrikkelopplysninger = new MatrikkelopplysningerV1Builder()
                .Bygning("161","X",null)
                .Build();
            val.BygningValidering(matrikkelopplysninger, new List<string>() { });
            var result = val.GetResult();

            result.messages.Where(m => m.reference.Contains("4920.1.6")).Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4920_1_6_BygningValideringErrorTest")]
        public void  Matrikkelopplysninger_4920_1_6_BygningValideringErrorTest()
        {
            // Bebygd areal skal angis ved Søknad om rammetillatelse, Søknad om igangsettingstillatelse, Søknad om tillatelse i ett trinn og Søknad om tillatelse til tiltak uten ansvarsrett
            var val = new MatrikkelopplysningerFormV1Validator();

            var matrikkelopplysninger = new MatrikkelopplysningerV1Builder()
                .Bygning("161","X",null)
                .Build();
            val.BygningValidering(matrikkelopplysninger, new List<string>() { });
            var result = val.GetResult();

            result.messages.Where(m => m.reference.Contains("4920.1.6")).Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4920_1_7_BygningValideringOKTest")]
        public void  Matrikkelopplysninger_4920_1_7_BygningValideringOKTest()
        {
            // Minst en etasje bør angis ved Søknad om igangsettingstillatelse, Søknad om tillatelse i ett trinn og Søknad om tillatelse til tiltak uten ansvarsrett	Advarsel
            var val = new MatrikkelopplysningerFormV1Validator();

            var matrikkelopplysninger = new MatrikkelopplysningerV1Builder()
                .Bygning("161")
                .BygningEtasjer("H")
                .BygningBruksenheter()
                .Build();
            val.BygningValidering(matrikkelopplysninger, new List<string>() { "Søknad om tillatelse til tiltak uten ansvarsrett" });
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4920_1_7_BygningValideringErrorTest")]
        public void  Matrikkelopplysninger_4920_1_7_BygningValideringErrorTest()
        {
            // Minst en etasje bør angis ved Søknad om igangsettingstillatelse, Søknad om tillatelse i ett trinn og Søknad om tillatelse til tiltak uten ansvarsrett	Advarsel
                        var val = new MatrikkelopplysningerFormV1Validator();

            var matrikkelopplysninger = new MatrikkelopplysningerV1Builder()
                .Bygning("161")
                .BygningBruksenheter()
                .Build();
            val.BygningValidering(matrikkelopplysninger, new List<string>() { "Søknad om tillatelse til tiltak uten ansvarsrett" });
            var result = val.GetResult();

            result.messages.Where(m => m.reference.Contains("4920.1.7")).Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4920_1_8_BygningValideringOKTest")]
        public void  Matrikkelopplysninger_4920_1_8_BygningValideringOKTest()
        {
            // Minst en bruksenhet bør angis ved  Søknad om igangsettingstillatelse, Søknad om tillatelse i ett trinn og Søknad om tillatelse til tiltak uten ansvarsrett	Advarsel
                        var val = new MatrikkelopplysningerFormV1Validator();

            var matrikkelopplysninger = new MatrikkelopplysningerV1Builder()
                .Bygning("161")
                .BygningEtasjer("H")
                .BygningBruksenheter()
                .Build();
            val.BygningValidering(matrikkelopplysninger, new List<string>() { "Søknad om tillatelse til tiltak uten ansvarsrett" });
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4920_1_8_BygningValideringErrorTest")]
        public void  Matrikkelopplysninger_4920_1_8_BygningValideringErrorTest()
        {
            // Minst en bruksenhet bør angis ved  Søknad om igangsettingstillatelse, Søknad om tillatelse i ett trinn og Søknad om tillatelse til tiltak uten ansvarsrett	Advarsel
            var val = new MatrikkelopplysningerFormV1Validator();

            var matrikkelopplysninger = new MatrikkelopplysningerV1Builder()
                .Bygning("161")
                .BygningEtasjer("H")
                .Build();
            val.BygningValidering(matrikkelopplysninger, new List<string>() { "Søknad om tillatelse til tiltak uten ansvarsrett" });
            var result = val.GetResult();

            result.messages.Where(m => m.reference.Contains("4920.1.8")).Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4920_1_9_BygningValideringOKTest")]
        public void  Matrikkelopplysninger_4920_1_9_BygningValideringOKTest()
        {
            // Vannforsyning bør angis ved Søknad om igangsettingstillatelse, Søknad om tillatelse i ett trinn og Søknad om tillatelse til tiltak uten ansvarsrett	Advarsel
                        var val = new MatrikkelopplysningerFormV1Validator();

            var matrikkelopplysninger = new MatrikkelopplysningerV1Builder()
                .Bygning("161")
                .BygningEtasjer("H")
                .BygningBruksenheter()
                .Build();
            val.BygningValidering(matrikkelopplysninger, new List<string>() { "Søknad om tillatelse til tiltak uten ansvarsrett" });
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4920_1_9_BygningValideringErrorTest")]
        public void  Matrikkelopplysninger_4920_1_9_BygningValideringErrorTest()
        {
            // Vannforsyning bør angis ved Søknad om igangsettingstillatelse, Søknad om tillatelse i ett trinn og Søknad om tillatelse til tiltak uten ansvarsrett	Advarsel
                        var val = new MatrikkelopplysningerFormV1Validator();

            var matrikkelopplysninger = new MatrikkelopplysningerV1Builder()
                .Bygning("161","X",1020,null)
                .BygningEtasjer("H")
                .BygningBruksenheter()
                .Build();
            val.BygningValidering(matrikkelopplysninger, new List<string>() { "Søknad om tillatelse til tiltak uten ansvarsrett" });
            var result = val.GetResult();

            result.messages.Where(m => m.reference.Contains("4920.1.9")).Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4920_1_10_BygningValideringOKTest")]
        public void  Matrikkelopplysninger_4920_1_10_BygningValideringOKTest()
        {
            // Avløpstilknytning bør angis ved Søknad om igangsettingstillatelse, Søknad om tillatelse i ett trinn og Søknad om tillatelse til tiltak uten ansvarsrett Advarsel
            var val = new MatrikkelopplysningerFormV1Validator();

            var matrikkelopplysninger = new MatrikkelopplysningerV1Builder()
                .Bygning("161")
                .BygningEtasjer("H")
                .BygningBruksenheter()
                .Build();
            val.BygningValidering(matrikkelopplysninger, new List<string>() { "Søknad om tillatelse til tiltak uten ansvarsrett" });
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4920_1_10_BygningValideringErrorTest")]
        public void  Matrikkelopplysninger_4920_1_10_BygningValideringErrorTest()
        {
            // Avløpstilknytning bør angis ved Søknad om igangsettingstillatelse, Søknad om tillatelse i ett trinn og Søknad om tillatelse til tiltak uten ansvarsrett Advarsel
            var val = new MatrikkelopplysningerFormV1Validator();

            var matrikkelopplysninger = new MatrikkelopplysningerV1Builder()
                .Bygning("161","X",1020, "AnnenPrivatInnlagt", null)
                .BygningEtasjer("H")
                .BygningBruksenheter()
                .Build();
            val.BygningValidering(matrikkelopplysninger, new List<string>() { "Søknad om tillatelse til tiltak uten ansvarsrett" });
            var result = val.GetResult();

            result.messages.Where(m => m.reference.Contains("4920.1.10")).Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4920_1_11_BygningValideringOKTest")]
        public void  Matrikkelopplysninger_4920_1_11_BygningValideringOKTest()
        {
            // Heis bør angis ved Søknad om igangsettingstillatelse, Søknad om tillatelse i ett trinn og Søknad om tillatelse til tiltak uten ansvarsrett Advarsel
            var val = new MatrikkelopplysningerFormV1Validator();

            var matrikkelopplysninger = new MatrikkelopplysningerV1Builder()
                .Bygning("161")
                .BygningEtasjer("H")
                .BygningBruksenheter()
                .Build();
            val.BygningValidering(matrikkelopplysninger, new List<string>() { "Søknad om tillatelse til tiltak uten ansvarsrett" });
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4920_1_11_BygningValideringErrorTest")]
        public void  Matrikkelopplysninger_4920_1_11_BygningValideringErrorTest()
        {
            // Heis bør angis ved Søknad om igangsettingstillatelse, Søknad om tillatelse i ett trinn og Søknad om tillatelse til tiltak uten ansvarsrett Advarsel
            var val = new MatrikkelopplysningerFormV1Validator();

            var matrikkelopplysninger = new MatrikkelopplysningerV1Builder()
                .Bygning("161","X",1020, "AnnenPrivatInnlagt","PrivatKloakk",null)
                .BygningEtasjer("H")
                .BygningBruksenheter()
                .Build();
            val.BygningValidering(matrikkelopplysninger, new List<string>() { "Søknad om tillatelse til tiltak uten ansvarsrett" });
            var result = val.GetResult();

            result.messages.Where(m => m.reference.Contains("4920.1.11")).Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4920_1_12_BygningValideringOKTest")]
        public void  Matrikkelopplysninger_4920_1_12_BygningValideringOKTest()
        {
            // Energiforsyning/Energikilder bør angis ved Søknad om ferdigattest	Advarsel
                        var val = new MatrikkelopplysningerFormV1Validator();

            var matrikkelopplysninger = new MatrikkelopplysningerV1Builder()
                .Bygning("161")
                .BygningEtasjer("H")
                .BygningBruksenheter()
                .BygningEnergiforsyning()
                .Build();
            val.BygningValidering(matrikkelopplysninger, new List<string>() { "Søknad om ferdigattest" });
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4920_1_12_BygningValideringErrorTest")]
        public void  Matrikkelopplysninger_4920_1_12_BygningValideringErrorTest()
        {
            // Energiforsyning/Energikilder bør angis ved Søknad om ferdigattest	Advarsel
                        var val = new MatrikkelopplysningerFormV1Validator();

            var matrikkelopplysninger = new MatrikkelopplysningerV1Builder()
                .Bygning("161")
                .BygningEtasjer("H")
                .BygningBruksenheter()
                .BygningEnergiforsyning(null)
                .Build();
            val.BygningValidering(matrikkelopplysninger, new List<string>() { "Søknad om ferdigattest" });
            var result = val.GetResult();

            result.messages.Where(m => m.reference.Contains("4920.1.12")).Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4920_1_13_BygningValideringOKTest")]
        public void  Matrikkelopplysninger_4920_1_13_BygningValideringOKTest()
        {
            // Varmefordeling/Oppvarmingstyper bør angis ved Søknad om ferdigattest
            var val = new MatrikkelopplysningerFormV1Validator();

            var matrikkelopplysninger = new MatrikkelopplysningerV1Builder()
                .Bygning("161")
                .BygningEtasjer("H")
                .BygningBruksenheter()
                .BygningEnergiforsyning()
                .Build();
            val.BygningValidering(matrikkelopplysninger, new List<string>() { "Søknad om ferdigattest" });
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4920_1_13_BygningValideringErrorTest")]
        public void  Matrikkelopplysninger_4920_1_13_BygningValideringErrorTest()
        {
            // Varmefordeling/Oppvarmingstyper bør angis ved Søknad om ferdigattest
            var val = new MatrikkelopplysningerFormV1Validator();

            var matrikkelopplysninger = new MatrikkelopplysningerV1Builder()
                .Bygning("161")
                .BygningEtasjer("H")
                .BygningBruksenheter()
                .BygningEnergiforsyning("elektrisitet",null)
                .Build();
            val.BygningValidering(matrikkelopplysninger, new List<string>() { "Søknad om ferdigattest" });
            var result = val.GetResult();

            result.messages.Where(m => m.reference.Contains("4920.1.13")).Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        
    }
}
