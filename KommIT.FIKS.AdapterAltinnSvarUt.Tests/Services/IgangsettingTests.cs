﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using FluentAssertions;
using KommIT.FIKS.AdapterAltinnSvarUt.Services;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.SvarUt;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.SvarUtForm;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Validations;
using KommIT.FIKS.AdapterAltinnSvarUt.Tests.Services.Builders;
using Moq;
using Xunit;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Tests.Services
{
    public class IgangsettingTests
    {
        [Fact(DisplayName = "4401_2_8_AnsvarValideringOKTest")]
        public void Igangsettingstillatelse_4401_2_8_AnsvarValideringOKTest()
        {
            //Gjennomføringsplan (eller Søknad om ansvarsrett for selvbygger) skal følge med søknaden	Feil
            var val = new IgangsettingstillatelseFormV2Validator();

            var igangsettingstillatelse = new IgangsettingstillatelseV2Builder()
                .Build();
            val.AnsvarValidering(igangsettingstillatelse, new List<string>() { "Søknad om ansvarsrett for selvbygger", "Gjennomføringsplan", "Gjennomfoeringsplan", "GjennomføringsplanV4" });
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4401_2_8_AnsvarValideringErrorTest")]
        public void Igangsettingstillatelse_4401_2_8_AnsvarValideringErrorTest()
        {
            //Gjennomføringsplan (eller Søknad om ansvarsrett for selvbygger) skal følge med søknaden	Feil
            var val = new IgangsettingstillatelseFormV2Validator();

            var igangsettingstillatelse = new IgangsettingstillatelseV2Builder()
                .Build();
            val.AnsvarValidering(igangsettingstillatelse, new List<string>() { "qwerty" });
            var result = val.GetResult();

            result.messages.Where(m => m.reference.Contains("4401.2.8")).Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4401_2_10_SoknadsDelerAvTiltakenValideringOKTest")]
        public void Igangsettingstillatelse_4401_2_10_SoknadsDelerAvTiltakenValideringOKTest()
        {
            //Når det søkes om deler av tiltaket må beskrivelse av del fylles ut	/Igangsettingstillatelse/gjelderHeleTiltaket=false	Feil
            var val = new IgangsettingstillatelseFormV2Validator();

            var igangsettingstillatelse = new IgangsettingstillatelseV2Builder()
                .SoknadsDelerAvTiltaken(false, "qwerty")
                .Build();
            val.SoknadsDelerAvTiltakenValidering(igangsettingstillatelse);
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4401_2_10_SoknadsDelerAvTiltakenValideringErrorTest")]
        public void Igangsettingstillatelse_4401_2_10_SoknadsDelerAvTiltakenValideringErrorTest()
        {
            //Når det søkes om deler av tiltaket må beskrivelse av del fylles ut	/Igangsettingstillatelse/gjelderHeleTiltaket=false	Feil
            var val = new IgangsettingstillatelseFormV2Validator();

            var igangsettingstillatelse = new IgangsettingstillatelseV2Builder()
                .SoknadsDelerAvTiltaken(false)
                .Build();
            val.SoknadsDelerAvTiltakenValidering(igangsettingstillatelse);
            var result = val.GetResult();

            result.messages.Where(m => m.reference.Contains("4401.2.10")).Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4401_2_11_ArbeidsplasserValideringOKTest")]
        public void Igangsettingstillatelse_4401_2_11_ArbeidsplasserValideringOKTest()
        {
            //Når en velger at yrkesbygg berører arbeidsplasser må samtykke fra Arbeidstilsynet være på plass før igangsettingstillatelse kan gis. Vedlegg SamtykkeArbeidstilsynet  må legges ved denne søknaden.	/Igangsettingstillatelse/beroererArbeidsplasser=true	Feil
            var val = new IgangsettingstillatelseFormV2Validator();

            var igangsettingstillatelse = new IgangsettingstillatelseV2Builder()
                .Arbeidsplasser(true)
                .Build();
            val.ArbeidsplasserValidering(igangsettingstillatelse, new List<string>() { "Søknad om Arbeidstilsynets samtykke", "SamtykkeArbeidstilsynet", "Søknad om arbeidstilsynets samtykke" });
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4401_2_11_ArbeidsplasserValideringErrorTest")]
        public void Igangsettingstillatelse_4401_2_11_ArbeidsplasserValideringErrorTest()
        {
            //Når en velger at yrkesbygg berører arbeidsplasser må samtykke fra Arbeidstilsynet være på plass før igangsettingstillatelse kan gis. Vedlegg SamtykkeArbeidstilsynet  må legges ved denne søknaden.	/Igangsettingstillatelse/beroererArbeidsplasser=true	Feil
            var val = new IgangsettingstillatelseFormV2Validator();

            var igangsettingstillatelse = new IgangsettingstillatelseV2Builder()
                .Arbeidsplasser(true)
                .Build();
            val.ArbeidsplasserValidering(igangsettingstillatelse, new List<string>() { "" });
            var result = val.GetResult();

            result.messages.Where(m => m.reference.Contains("4401.2.11")).Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4401_2_12_kommunensSaksnummerValideringOKTest")]
        public void Igangsettingstillatelse_4401_2_12_kommunensSaksnummerValideringOKTest()
        {
            //Kommunens saksnummer må være fylt ut.	Feil
            var val = new IgangsettingstillatelseFormV2Validator();

            var igangsettingstillatelse = new IgangsettingstillatelseV2Builder()
                .KommunensSaksnummer("2015", "124340")
                .Arbeidsplasser(true)
                .Build();
            val.KommunensSaksnummerValidering(igangsettingstillatelse);
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4401_2_12_kommunensSaksnummerValideringErrorTest")]
        public void Igangsettingstillatelse_4401_2_12_kommunensSaksnummerValideringErrorTest()
        {
            //Kommunens saksnummer må være fylt ut.	Feil
            var val = new IgangsettingstillatelseFormV2Validator();

            var igangsettingstillatelse = new IgangsettingstillatelseV2Builder()
                .KommunensSaksnummer(null, "124340")
                .Arbeidsplasser(true)
                .Build();
            val.KommunensSaksnummerValidering(igangsettingstillatelse);
            var result = val.GetResult();

            result.messages.Where(m => m.reference.Contains("4401.2.12")).Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4401_2_13_EiendomByggestedValidationOKTest")]
        public void Igangsettingstillatelse_4401_2_13_EiendomByggestedValidationOKTest()
        {
            // Hvis bruksenhetsnummer(bolignummer) er fylt ut må det følge riktig format (^[HKLU]\\d{4}$) (feks H0101) se https://www.kartverket.no/eiendom/adressering/bruksenhetsnummer/
            var val = new IgangsettingstillatelseFormV2Validator();
            var municipalityValidator = new Mock<IMunicipalityValidator>();
            municipalityValidator.Setup(s => s.ValidateKommunenummer(It.IsAny<string>())).Returns(new GeneralValidationResult() { Status = ValidationStatus.Ok, Message = "UnitTest" });
            val.MunicipalityValidator = municipalityValidator.Object;

            var igangsettingstillatelse = new IgangsettingstillatelseV2Builder()
                .EiendomType("2004", "1234", "4321", "0", "0", "Hammerfest", "H0101")
                .Build();
            val.EiendomByggestedValidation(igangsettingstillatelse);
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4401_2_13_EiendomByggestedValidationErrorTest")]
        public void Igangsettingstillatelse_4401_2_13_EiendomByggestedValidationErrorTest()
        {
            // Hvis bruksenhetsnummer(bolignummer) er fylt ut må det følge riktig format (^[HKLU]\\d{4}$) (feks H0101) se https://www.kartverket.no/eiendom/adressering/bruksenhetsnummer/
            var val = new IgangsettingstillatelseFormV2Validator();
            var municipalityValidator = new Mock<IMunicipalityValidator>();
            municipalityValidator.Setup(s => s.ValidateKommunenummer(It.IsAny<string>())).Returns(new GeneralValidationResult() { Status = ValidationStatus.Ok, Message = "UnitTest" });
            val.MunicipalityValidator = municipalityValidator.Object;

            var igangsettingstillatelse = new IgangsettingstillatelseV2Builder()
                .EiendomType("2004", "1234", "4321", "0", "0", "Hammerfest", "x0101")
                .Build();
            val.EiendomByggestedValidation(igangsettingstillatelse);
            var result = val.GetResult();

            result.messages.Where(m => m.reference.Contains("4401.2.13")).Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4401_1_1a_EiendomByggestedValidationOKTest")]
        public void Igangsettingstillatelse_4401_1_1a_EiendomByggestedValidationOKTest()
        {
            // Innhold må være ihht informasjonsmodell/xsd for skjema
            var val = new IgangsettingstillatelseFormV2Validator();
            var municipalityValidator = new Mock<IMunicipalityValidator>();
            municipalityValidator.Setup(s => s.ValidateKommunenummer(It.IsAny<string>())).Returns(new GeneralValidationResult() { Status = ValidationStatus.Ok, Message = "UnitTest" });
            val.MunicipalityValidator = municipalityValidator.Object;

            var igangsettingstillatelse = new IgangsettingstillatelseV2Builder()
                .EiendomType("2004", "1234", "4321", "0", "0", "Hammerfest", "H0101")
                .Build();
            val.EiendomByggestedValidation(igangsettingstillatelse);
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4401_1_1a_EiendomByggestedValidationErrorTest")]
        public void Igangsettingstillatelse_4401_1_1a_EiendomByggestedValidationErrorTest()
        {
            // Innhold må være ihht informasjonsmodell/xsd for skjema
            var val = new IgangsettingstillatelseFormV2Validator();

            var igangsettingstillatelse = new IgangsettingstillatelseV2Builder()
            .Build();

            val.EiendomByggestedValidation(igangsettingstillatelse);
            var result = val.GetResult();

            result.messages.Where(m => m.reference.Contains("4401.1.1")).Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4401_1_1b_EiendomByggestedValidationKnrOKTest")]
        public void Igangsettingstillatelse_4401_1_1b_EiendomByggestedValidationKnrOKTest()
        {
            // kommunenummer bør fylles ut for eiendom/byggested", "4401.1.1", "/igangsettingstillatelse/eiendomByggested/eiendom[0]/eiendomsidentifikasjon/kommunenummer
            var val = new IgangsettingstillatelseFormV2Validator();
            var municipalityValidator = new Mock<IMunicipalityValidator>();
            municipalityValidator.Setup(s => s.ValidateKommunenummer(It.IsAny<string>())).Returns(new GeneralValidationResult() { Status = ValidationStatus.Ok, Message = "UnitTest" });
            val.MunicipalityValidator = municipalityValidator.Object;

            var igangsettingstillatelse = new IgangsettingstillatelseV2Builder()
                .EiendomType("2004", "1234", "4321", "0", "0", "Hammerfest", "H0101")
                .Build();
            val.EiendomByggestedValidation(igangsettingstillatelse);
            var result = val.GetResult();
            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4401_1_1b_EiendomByggestedValidationKnrErrorTest")]
        public void Igangsettingstillatelse_4401_1_1b_EiendomByggestedValidationKnrErrorTest()
        {
            // kommunenummer bør fylles ut for eiendom/byggested", "4401.1.1", "/igangsettingstillatelse/eiendomByggested/eiendom[0]/eiendomsidentifikasjon/kommunenummer
            var val = new IgangsettingstillatelseFormV2Validator();
            var municipalityValidator = new Mock<IMunicipalityValidator>();
            municipalityValidator.Setup(s => s.ValidateKommunenummer(It.IsAny<string>())).Returns(new GeneralValidationResult() { Status = ValidationStatus.Fail, Message = "UnitTest" });
            val.MunicipalityValidator = municipalityValidator.Object;

            var igangsettingstillatelse = new IgangsettingstillatelseV2Builder()
                .EiendomType("11111", "1234", "4321", "0", "0", "Hammerfest", "H0101")
                .Build();
            val.EiendomByggestedValidation(igangsettingstillatelse);
            var result = val.GetResult();
            result.messages.Where(m => m.reference.Contains("4401.1.1")).Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4401_1_1_AnsvarligSoekerValidationOKTest")]
        public void Igangsettingstillatelse_4401_1_1_AnsvarligSoekerValidationOkTest()
        {
            var val = new IgangsettingstillatelseFormV2Validator();
            var igangsettingstillatelse = new IgangsettingstillatelseV2Builder()
              .AnsvarligSoeker("974760673", null, "Foretak", "Benjamin Fjell", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "no", "Bø i Telemark", "11223344", "99887766")
                .Build();
            val.AnsvarligSoekerValidation(igangsettingstillatelse);
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4401_1_1_AnsvarligSoekerValidationErrorTest")]
        public void Igangsettingstillatelse_4401_1_1_AnsvarligSoekerValidationErrorTest()
        {
            var val = new IgangsettingstillatelseFormV2Validator();
            var igangsettingstillatelse = new IgangsettingstillatelseV2Builder()
                .AnsvarligSoeker("974760673", null, "Foretak", "Benjamin Fjell", null, "Lillegata 5", "7003", "no", "Bø i Telemark", "11223344", "99887766")
                .Build();
            val.AnsvarligSoekerValidation(igangsettingstillatelse);
            var result = val.GetResult();

            result.messages.Where(m => m.reference.Contains("4401.1.4")).Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4401_2_14_AnsvarligSoekerValidationErrorTest")]
        public void Igangsettingstillatelse_4401_2_14_AnsvarligSoekerValidationErrorTest()
        {
            var val = new IgangsettingstillatelseFormV2Validator();
            var igangsettingstillatelse = new IgangsettingstillatelseV2Builder()
                .AnsvarligSoeker("974760673", null, "Foretak",null, "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "no", "Bø i Telemark", "11223344", "99887766")
                .Build();
            val.AnsvarligSoekerValidation(igangsettingstillatelse);
            var result = val.GetResult();

            result.messages.Where(m => m.reference.Contains("4401.2.14")).Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4401_2_15_AnsvarligSoekerValidationErrorTest")]
        public void Igangsettingstillatelse_4401_2_15_AnsvarligSoekerValidationErrorTest()
        {
            var val = new IgangsettingstillatelseFormV2Validator();
            var igangsettingstillatelse = new IgangsettingstillatelseV2Builder()
                .AnsvarligSoeker("974760673", null, "Foretak", "Benjamin Fjell", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "no", "Bø i Telemark", null,null)
                .Build();
            val.AnsvarligSoekerValidation(igangsettingstillatelse);
            var result = val.GetResult();

            result.messages.Where(m => m.reference.Contains("4401.2.15")).Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
    }
}