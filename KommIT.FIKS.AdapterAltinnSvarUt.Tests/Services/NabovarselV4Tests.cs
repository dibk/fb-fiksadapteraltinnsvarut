using System.Collections.Generic;
using System.IO;
using System.Linq;
using FluentAssertions;
using KommIT.FIKS.AdapterAltinnSvarUt.Services;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.NotificationForm.Nabovarsel;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.Builders;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Validations;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;
using Moq;
using no.kxml.skjema.dibk.nabovarselV4;
using Xunit;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Tests.Services
{
    public class NabovarselV4Tests
    {
        private readonly bool _externalDependences = Feature.IsFeatureEnabled(FeatureFlags.UnitTestExternalDependence);
        private readonly bool _matrikkelFlag = Feature.IsFeatureEnabled(FeatureFlags.MatrikkelValidering);
        private readonly bool _bringFlag = Feature.IsFeatureEnabled(FeatureFlags.BringPostnummerValidering);
        private Skjema _skjema;
        private List<string> _formSetElements;
        private NabovarselType _form;

        public NabovarselV4Tests()
        {
            //var formDataAsXml = File.ReadAllText(@"Services\testskjema\6303_44820_distribusjonnabovarselv4.xml");

            _formSetElements = new List<string>() { "Situasjonsplan", "TegningNyPlan", "TegningNyttSnitt", "TegningNyFasade" };

            //_skjema = new Skjema()
            //{
            //    DataFormatID = "6303",
            //    DataFormatVersion = "44820",
            //    Data = formDataAsXml
            //};
            //_form = TestHelper.GetValue<NabovarselType>(_skjema);

        }

        [Fact(DisplayName = "4655_3_1_Nabovarsel full validering og prefill", Skip = "Fix Matrikelnummer and ansvarligSoeker info")]
        public void Nabovarselv4ValidateOkTest()
        {
            var formDataAsXml = File.ReadAllText(@"Services\testskjema\6303_44820_distribusjonnabovarselv4.xml");
            IAltinnForm nabovarselForm = new DistribusjonNabovarselFormV4();
            nabovarselForm.InitiateForm(formDataAsXml);
            nabovarselForm.SetFormSetElements(new List<string>() { "Situasjonsplan", "TegningNyPlan", "TegningNyttSnitt", "TegningNyFasade" });
            var result = nabovarselForm.ValidateData();
            result.IsOk().Should().BeTrue();

            var distributionForm = (IDistributionForm)nabovarselForm;
            List<IAltinnForm> forms = distributionForm.GetPrefillForms();
            foreach (var form in forms)
            {
                var prefillValidationResult = form.ValidateData();
                prefillValidationResult.IsOk().Should().BeTrue();
            }
        }
        [Fact(DisplayName = "4655.3.17 FraSluttbrukersystemErUtfylt - OK test")]
        public void NabovarselV4_4655_3_17_FraSluttbrukersystemErUtfylttOKTest()
        {

            //var formV2ValidationJson = TestHelper.GetValidateFormv2FormSkjema(_skjema,_formSetElements);
            var val = new NabovarselV4Validator();

            var nabovarselV4 = new NabovarselV4Builder()
                .FraSluttbrukersystem("Fellestjenester bygg testmotor")
                .Build();
            val.FraSluttbrukersystemErUtfylt(nabovarselV4);
            var result = val.GetResult();
            var noko = TestHelper.ConverRulesCheckedJson(result, false);
            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4655.3.17 FraSluttbrukersystemErUtfylt - Error test")]
        public void NabovarselV4_4655_3_17_FraSluttbrukersystemErUtfylttErrorTest()
        {
            var val = new NabovarselV4Validator();

            var nabovarselV4 = new NabovarselV4Builder()
                .FraSluttbrukersystem(null)
                .Build();
            val.FraSluttbrukersystemErUtfylt(nabovarselV4);
            var result = val.GetResult();

            AssertResult(result, "4655.3.17", 1);

        }
        [Fact(DisplayName = "4655 EiendomByggestedValidering - OK test")]
        public void NabovarselV4_4655_EiendomByggestedValideringOkTest()
        {
            var val = new NabovarselV4Validator();
            var nabovarselV4 = new NabovarselV4Builder()
                .EiendomByggested("2004", "1", "3", "0", "0", "Hammerfest", "H0101", "192423880", "Båtsfjorden 6", "Båtsfjorden", "6", "9664", "SANDØYBOTN")
                .Build();
            val.MunicipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");
            val._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, true);
            val.EiendomByggestedValidering(nabovarselV4);
            var result = val.GetResult();
            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4655.1.1 EiendomByggestedValidering - Error test")]
        public void NabovarselV4_4655_1_1_EiendomByggestedValideringErrorTest()
        {

            var val = new NabovarselV4Validator();

            var nabovarselV4 = new NabovarselV4Builder()
                .Build();
            val.EiendomByggestedValidering(nabovarselV4);
            var result = val.GetResult();

            AssertResult(result, "4655.1.1.15", 1);

        }
        [Fact(DisplayName = "4655.1.1.13 EiendomByggestedValidering - Error test")]
        public void NabovarselV4_4655_1_1_13_EiendomByggestedValideringErrorTest()
        {

            var val = new NabovarselV4Validator();

            var nabovarselV4 = new NabovarselV4Builder()
                .EiendomByggested(null, "1", "3", "0", "0", "Hammerfest", "H0101", "192423880", "Båtsfjorden 6", "Båtsfjorden", "6", "9664", "SANDØYBOTN")
                .Build();
            val.MunicipalityValidator = TestHelper.MokMunicipalityValidatorResult(null);
            val._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, true);
            val.EiendomByggestedValidering(nabovarselV4);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4655.1.1.13").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4655.1.1.13.1 EiendomByggestedValidering - Error test")]
        public void NabovarselV4_4655_1_1_13_1_EiendomByggestedValideringErrorTest()
        {

            var val = new NabovarselV4Validator();

            var nabovarselV4 = new NabovarselV4Builder()
                .EiendomByggested(null, "1", "3", "0", "0", "Hammerfest", "H0101", "192423880", "Båtsfjorden 6", "Båtsfjorden", "6", "9664", "SANDØYBOTN")
                .Build();
            val.MunicipalityValidator = TestHelper.MokMunicipalityValidatorResult("invalid");
            val._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, true);
            val.EiendomByggestedValidering(nabovarselV4);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4655.1.1.13.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4655.1.1.13.2 EiendomByggestedValidering - Error test")]
        public void NabovarselV4_4655_1_1_13_2_EiendomByggestedValideringErrorTest()
        {

            var val = new NabovarselV4Validator();

            var nabovarselV4 = new NabovarselV4Builder()
                .EiendomByggested(null, "1", "3", "0", "0", "Hammerfest", "H0101", "192423880", "Båtsfjorden 6", "Båtsfjorden", "6", "9664", "SANDØYBOTN")
                .Build();
            val.MunicipalityValidator = TestHelper.MokMunicipalityValidatorResult("expired");
            val._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, true);
            val.EiendomByggestedValidering(nabovarselV4);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4655.1.1.13.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4655.3.25.1 EiendomByggestedValidering - Error test")]
        public void nabovarselV4_4655_3_25_1_EiendomByggestedValideringErrorTest()
        {
            var val = new NabovarselV4Validator();
            var nabovarselV4 = new NabovarselV4Builder()
                .EiendomByggested("2004", null, "3", "0", "0", "Hammerfest", "H0101", "192423880", "Båtsfjorden 6", "Båtsfjorden", "6", "9664", "SANDØYBOTN")
                .Build();

            val.MunicipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");
            val._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, true);
            val.EiendomByggestedValidering(nabovarselV4);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4655.3.25.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4655.3.25.2 EiendomByggestedValidering - Error test")]
        public void nabovarselV4_4655_3_25_2_EiendomByggestedValideringErrorTest()
        {
            var val = new NabovarselV4Validator();
            var nabovarselV4 = new NabovarselV4Builder()
                .EiendomByggested("2004", "1", null, "0", "0", "Hammerfest", "H0101", "192423880", "Båtsfjorden 6", "Båtsfjorden", "6", "9664", "SANDØYBOTN")
                .Build();

            val.MunicipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");
            val._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, true);
            val.EiendomByggestedValidering(nabovarselV4);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4655.3.25.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4655.3.25.3 EiendomByggestedValidering - Error test")]
        public void nabovarselV4_4655_3_25_3_EiendomByggestedValideringErrorTest()
        {
            var val = new NabovarselV4Validator();
            var nabovarselV4 = new NabovarselV4Builder()
                .EiendomByggested("2004", "-1", "3", "0", "0", "Hammerfest", "H0101", "192423880", "Båtsfjorden 6", "Båtsfjorden", "6", "9664", "SANDØYBOTN")
                .Build();

            val.MunicipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");
            val._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, true);
            val.EiendomByggestedValidering(nabovarselV4);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4655.3.25.3").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4655.3.25.4 EiendomByggestedValidering - Error test")]
        public void nabovarselV4_4655_3_25_4_EiendomByggestedValideringErrorTest()
        {
            var val = new NabovarselV4Validator();
            var nabovarselV4 = new NabovarselV4Builder()
                .EiendomByggested("2004", "1", "-3", "0", "0", "Hammerfest", "H0101", "192423880", "Båtsfjorden 6", "Båtsfjorden", "6", "9664", "SANDØYBOTN")
                .Build();

            val.MunicipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");
            val._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, true);
            val.EiendomByggestedValidering(nabovarselV4);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4655.3.25.4").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4655.3.25.5 EiendomByggestedValidering - Error test")]
        public void nabovarselV4_4655_3_25_5_EiendomByggestedValideringErrorTest()
        {
            var val = new NabovarselV4Validator();
            var nabovarselV4 = new NabovarselV4Builder()
                .EiendomByggested("2004", "1", "3", "0", "0", "Hammerfest", "H0101", "192423880", "Båtsfjorden 6", "Båtsfjorden", "6", "9664", "SANDØYBOTN")
                .Build();

            val.MunicipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");
            val._matrikkelService = TestHelper.MokMatrikkelServiceResult(false, true, true);
            val.EiendomByggestedValidering(nabovarselV4);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4655.3.25.5").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4655.3.26 EiendomByggestedValidering - Error test")]
        public void nabovarselV4_4655_3_26_EiendomByggestedValideringErrorTest()
        {
            var val = new NabovarselV4Validator();
            var nabovarselV4 = new NabovarselV4Builder()
                .EiendomByggested("2004", "1", "3", "0", "0", "Hammerfest", "H0101", "*192423880", "Båtsfjorden 6", "Båtsfjorden", "6", "9664", "SANDØYBOTN")
                .Build();

            val.MunicipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");
            val._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, true);
            val.EiendomByggestedValidering(nabovarselV4);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4655.3.26").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4655.3.26.1 EiendomByggestedValidering - Error test")]
        public void nabovarselV4_4655_3_26_1_EiendomByggestedValideringErrorTest()
        {
            var val = new NabovarselV4Validator();
            var nabovarselV4 = new NabovarselV4Builder()
                .EiendomByggested("2004", "1", "3", "0", "0", "Hammerfest", "H0101", "192423880", "Båtsfjorden 6", "Båtsfjorden", "6", "9664", "SANDØYBOTN")
                .Build();

            val.MunicipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");
            val._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, false, true);
            val.EiendomByggestedValidering(nabovarselV4);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4655.3.26.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4655.3.12 EiendomByggestedValidering - Error test")]
        public void nabovarselV4_4655_3_12_EiendomByggestedValideringErrorTest()
        {
            var val = new NabovarselV4Validator();
            var nabovarselV4 = new NabovarselV4Builder()
                .EiendomByggested("2004", "1", "3", "0", "0", "Hammerfest", "H0101", "192423880", null, null, null, null, null)
                .Build();

            val.MunicipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");
            val._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, true);
            val.EiendomByggestedValidering(nabovarselV4);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4655.3.12").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4655.3.12.1 EiendomByggestedValidering - Error test")]
        public void nabovarselV4_4655_3_12_1_EiendomByggestedValideringErrorTest()
        {
            var val = new NabovarselV4Validator();
            var nabovarselV4 = new NabovarselV4Builder()
                .EiendomByggested("2004", "1", "3", "0", "0", "Hammerfest", "H0101", "192423880", null, "Båtsfjorden", "6", "9664", "SANDØYBOTN")
                .Build();

            val.MunicipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");
            val._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, true);
            val.EiendomByggestedValidering(nabovarselV4);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4655.3.12.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4655.3.28 EiendomByggestedValidering - Error test")]
        public void nabovarselV4_4655_3_28_EiendomByggestedValideringErrorTest()
        {
            var val = new NabovarselV4Validator();
            var nabovarselV4 = new NabovarselV4Builder()
                .EiendomByggested("2004", "1", "3", "0", "0", "Hammerfest", "H0101", "192423880", "Båtsfjorden 6", null, null, "9664", "SANDØYBOTN")
                .Build();

            val.MunicipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");
            val._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, true);
            val.EiendomByggestedValidering(nabovarselV4);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4655.3.28").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4655.3.27 EiendomByggestedValidering - Error test")]
        public void nabovarselV4_4655_3_27_EiendomByggestedValideringErrorTest()
        {
            var val = new NabovarselV4Validator();
            var nabovarselV4 = new NabovarselV4Builder()
                .EiendomByggested("2004", "1", "3", "0", "0", "Hammerfest", "H0101", "192423880", "Båtsfjorden 6", "Båtsfjorden", "6", "9664", "SANDØYBOTN")
                .Build();

            val.MunicipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");
            val._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, null);
            val.EiendomByggestedValidering(nabovarselV4);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4655.3.27").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4655.3.27.1 EiendomByggestedValidering - Error test")]
        public void nabovarselV4_4655_3_27_1_EiendomByggestedValideringErrorTest()
        {
            var val = new NabovarselV4Validator();
            var nabovarselV4 = new NabovarselV4Builder()
                .EiendomByggested("2004", "1", "3", "0", "0", "Hammerfest", "H0101", "192423880", "Båtsfjorden 6", "Båtsfjorden", "6", "9664", "SANDØYBOTN")
                .Build();

            val.MunicipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");
            val._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, false);
            val.EiendomByggestedValidering(nabovarselV4);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4655.3.27.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4655.3.29 EiendomByggestedValidering - Error test")]
        public void nabovarselV4_4655_3_29_EiendomByggestedValideringErrorTest()
        {
            var val = new NabovarselV4Validator();
            var nabovarselV4 = new NabovarselV4Builder()
                .EiendomByggested("2004", "1", "3", "0", "0", null, "H0101", "192423880", "Båtsfjorden 6", "Båtsfjorden", "6", "9664", "SANDØYBOTN")
                .Build();

            val.MunicipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");
            val._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, true);
            val.EiendomByggestedValidering(nabovarselV4);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4655.3.29").Should().NotBeNullOrEmpty();
        }

        [Fact(DisplayName = "4655.3.30 EiendomByggestedValidering - Error test")]
        public void nabovarselV4_4655_3_30_EiendomByggestedValideringErrorTest()
        {
            var val = new NabovarselV4Validator();
            var nabovarselV4 = new NabovarselV4Builder()
                .EiendomByggested("2004", "1", "3", "0", "0", "Hammerfest", "*H0101", "192423880", "Båtsfjorden 6", "Båtsfjorden", "6", "9664", "SANDØYBOTN")
                .Build();

            val.MunicipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");
            val._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, true);
            val.EiendomByggestedValidering(nabovarselV4);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4655.3.30").Should().NotBeNullOrEmpty();
        }

        [Fact(DisplayName = "4655.1.1.1 MinstEnNaboeier - ok test")]
        public void nabovarselV4_4655_1_1_1_MinstEnNaboeieOkTest()
        {

            var val = new NabovarselV4Validator();

            var nabovarselV4 = new NabovarselV4Builder()
                .NaboEiere("Hans Hansen", "privatperson", "yFNSdYXRoUXyobi8ousqk/tC6pOQ9beKApBzDPonOy1xkMNBLPqxpBXvd3a7XUsJTdJjluVyjAe1viEosL36QdsSzBJm7kMa7frpHfOVyyF6lfzUQSijBcTbY7DCm/Frc3hGM81BcmH6WX8e410oOuZh6lk1t7H93bmu8BknxXs=",
                    null)
                .AddGjelderNaboeiendom(null, null, "Benjamin")
                .AddEiendomsIdentifikasjonToGelderNaboeiendom("2004")
                .AddAddressToGjelderNaboEiendom("Storgata 3", "Storgata", "3", null, "3800", "Bø i Telemark", "no")
                .Build();
            val.MinstEnNaboeier(nabovarselV4);
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4655.1.1.1 MinstEnNaboeier - Error test")]
        public void nabovarselV4_4655_1_1_1_MinstEnNaboeieErrorTest()
        {

            var val = new NabovarselV4Validator();

            var nabovarselV4 = new NabovarselV4Builder()
                .NaboEiere()
                .Build();
            val.MinstEnNaboeier(nabovarselV4);
            var result = val.GetResult();

            result.messages.Count.Should().Be(1);

            var ruleId = "4655.1.1";
            result.messages.Where(m => m.reference.Contains(ruleId)).Should().NotBeNullOrEmpty();
        }
        [Fact(DisplayName = "4655.1. NaboeierValidering - Ok test")]
        public void nabovarselV4_4655_1_NaboeierValideringOKTest()
        {
            var val = new NabovarselV4Validator();

            var municipalityValidator = new Mock<IMunicipalityValidator>();
            municipalityValidator.Setup(s => s.ValidateKommunenummer(It.IsAny<string>())).Returns(new GeneralValidationResult() { Status = ValidationStatus.Ok, Message = "UnitTest" });
            val.MunicipalityValidator = municipalityValidator.Object;

            var nabovarselV4 = new NabovarselV4Builder()
                .NaboEiere("Hans Hansen", "privatperson", "yFNSdYXRoUXyobi8ousqk/tC6pOQ9beKApBzDPonOy1xkMNBLPqxpBXvd3a7XUsJTdJjluVyjAe1viEosL36QdsSzBJm7kMa7frpHfOVyyF6lfzUQSijBcTbY7DCm/Frc3hGM81BcmH6WX8e410oOuZh6lk1t7H93bmu8BknxXs=",
                    null, "storgate 1", "3800", "Bø i Telemark", "no")
                .AddGjelderNaboeiendom(null, null, "Benjamin")
                .AddEiendomsIdentifikasjonToGelderNaboeiendom("2004")
                .AddAddressToGjelderNaboEiendom("Storgata 3", "Storgata", "3", null, "3800", "Bø i Telemark", "no")
                .Build();
            val.MunicipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");
            val._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "", "Bø i Telemark");
            val.MunicipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");
            val.ParallelNaboeierValidering(nabovarselV4);

            var result = val.GetResult();

            TestHelper.AssertResult(result, null, 0, true);


        }
        [Fact(DisplayName = "4655.1.1 NaboeieNavn - error test")]
        public void nabovarselV4_4655_1_NaboeierNavnErrorTest()
        {

            var val = new NabovarselV4Validator();

            var nabovarselV4 = new NabovarselV4Builder()
                .NaboEiere(null, "privatperson", "yFNSdYXRoUXyobi8ousqk/tC6pOQ9beKApBzDPonOy1xkMNBLPqxpBXvd3a7XUsJTdJjluVyjAe1viEosL36QdsSzBJm7kMa7frpHfOVyyF6lfzUQSijBcTbY7DCm/Frc3hGM81BcmH6WX8e410oOuZh6lk1t7H93bmu8BknxXs=",
                    null, "storgate 1", "3800", "Bø i Telemark", "no")
                .Build();
            val.ParallelNaboeierValidering(nabovarselV4);
            var result = val.GetResult();
            result.messages.Count.Should().Be(1);

            var ruleId = "4655.1.1";
            result.messages.Where(m => m.reference.Contains(ruleId)).Should().NotBeNullOrEmpty();
        }
        [Fact(DisplayName = "4655.1.1.5 NaboeierValidering - Error test")]
        public void nabovarselV4_4655_1_1_5_NaboeierValideringErrorTest()
        {

            var val = new NabovarselV4Validator();

            var nabovarselV4 = new NabovarselV4Builder()
                .NaboEiere("Hans Hansen *beskyttet", "privatperson", "yFNSdYXRoUXyobi8ousqk/tC6pOQ9beKApBzDPonOy1xkMNBLPqxpBXvd3a7XUsJTdJjluVyjAe1viEosL36QdsSzBJm7kMa7frpHfOVyyF6lfzUQSijBcTbY7DCm/Frc3hGM81BcmH6WX8e410oOuZh6lk1t7H93bmu8BknxXs=",
                    null, "storgate 1", "3800", "Bø i Telemark", "no")
                .AddGjelderNaboeiendom(null, null, "Benjamin")
                .AddEiendomsIdentifikasjonToGelderNaboeiendom("2004")
                .AddAddressToGjelderNaboEiendom("Storgata 3", "Storgata", "3", null, "3800", "Bø i Telemark", "no")
                .Build();
            val.ParallelNaboeierValidering(nabovarselV4);
            var result = val.GetResult();

            result.messages.Count.Should().Be(1);

            var ruleId = "4655.1.1";
            result.messages.Where(m => m.reference.Contains(ruleId)).Should().NotBeNullOrEmpty();
        }
        [Fact(DisplayName = "4655.1.2.2 NaboeierValidering - Error test")]
        public void nabovarselV4_4655_1_2_2_NaboeierValideringErrorTest()
        {
            var val = new NabovarselV4Validator();
            var nabovarselV4 = new NabovarselV4Builder()
                 .NaboEiere("Hans Hansen", "*privatperson", "yFNSdYXRoUXyobi8ousqk/tC6pOQ9beKApBzDPonOy1xkMNBLPqxpBXvd3a7XUsJTdJjluVyjAe1viEosL36QdsSzBJm7kMa7frpHfOVyyF6lfzUQSijBcTbY7DCm/Frc3hGM81BcmH6WX8e410oOuZh6lk1t7H93bmu8BknxXs=",
                     null, "storgate 1", "3800", "Bø i Telemark", "no")
                 .AddGjelderNaboeiendom(null, null, "Benjamin")
                 .AddEiendomsIdentifikasjonToGelderNaboeiendom("2004")
                 .AddAddressToGjelderNaboEiendom("Storgata 3", "Storgata", "3", null, "3800", "Bø i Telemark", "no")
                 .Build();

            val.MunicipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");
            val.ParallelNaboeierValidering(nabovarselV4);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4655.1.2.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);


        }
        [Fact(DisplayName = "4655.1.1.16 NaboeierValidering - Error test")]
        public void nabovarselV4_4655_1_1_16_NaboeierValideringErrorTest()
        {
            var val = new NabovarselV4Validator();
            var nabovarselV4 = new NabovarselV4Builder()
                .NaboEiere("Hans Hansen", null, "yFNSdYXRoUXyobi8ousqk/tC6pOQ9beKApBzDPonOy1xkMNBLPqxpBXvd3a7XUsJTdJjluVyjAe1viEosL36QdsSzBJm7kMa7frpHfOVyyF6lfzUQSijBcTbY7DCm/Frc3hGM81BcmH6WX8e410oOuZh6lk1t7H93bmu8BknxXs=",
                    null, "storgate 1", "3800", "Bø i Telemark", "no")
                .AddGjelderNaboeiendom(null, null, "Benjamin")
                .AddEiendomsIdentifikasjonToGelderNaboeiendom("2004")
                .AddAddressToGjelderNaboEiendom("Storgata 3", "Storgata", "3", null, "3800", "Bø i Telemark", "no")
                .Build();
            val.MunicipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");
            val.ParallelNaboeierValidering(nabovarselV4);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4655.1.1.16").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4655.3.18 NaboeierValidering - Error test")]
        public void nabovarselV4_4655_3_18_NaboeierValideringErrorTest()
        {
            var val = new NabovarselV4Validator();
            var nabovarselV4 = new NabovarselV4Builder()
                .NaboEiere("Hans Hansen", "privatperson", "12345678901",
                    null, "storgate 1", "3800", "Bø i Telemark", "no")
                .AddGjelderNaboeiendom(null, null, "Benjamin")
                .AddEiendomsIdentifikasjonToGelderNaboeiendom("2004")
                .AddAddressToGjelderNaboEiendom("Storgata 3", "Storgata", "3", null, "3800", "Bø i Telemark", "no")
                .Build();

            val.MunicipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");
            val.ParallelNaboeierValidering(nabovarselV4);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4655.3.18").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4655.1.4 NaboeierValidering - Error test")]
        public void nabovarselV4_4655_1_4_NaboeierValideringErrorTest()
        {
            var val = new NabovarselV4Validator();
            var nabovarselV4 = new NabovarselV4Builder()
                //.NaboEiere("Hans Hansen", "privatperson", "*yFNSdYXRoUXyobi8ousqk/tC6pOQ9beKApBzDPonOy1xkMNBLPqxpBXvd3a7XUsJTdJjluVyjAe1viEosL36QdsSzBJm7kMa7frpHfOVyyF6lfzUQSijBcTbY7DCm/Frc3hGM81BcmH6WX8e410oOuZh6lk1t7H93bmu8BknxXs=",
                .NaboEiere("Hans Hansen", "privatperson", "",
                    null, "storgate 1", "3800", "Bø i Telemark", "no")
                .AddGjelderNaboeiendom(null, null, "Benjamin")
                .AddEiendomsIdentifikasjonToGelderNaboeiendom("2004")
                .AddAddressToGjelderNaboEiendom("Storgata 3", "Storgata", "3", null, "3800", "Bø i Telemark", "no").Build();

            val.MunicipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");
            val.ParallelNaboeierValidering(nabovarselV4);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4655.1.4").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4655.1.4.6 NaboeierValidering - Error test")]
        public void nabovarselV4_4655_1_4_6__NaboeierValideringErrorTest()
        {
            var val = new NabovarselV4Validator();
            var nabovarselV4 = new NabovarselV4Builder()
                .NaboEiere("Hans Hansen", "Foretak", null, "*974760673", "storgate 1", "3800", "Bø i Telemark", "no")
                .AddGjelderNaboeiendom(null, null, "Benjamin")
                .AddEiendomsIdentifikasjonToGelderNaboeiendom("2004")
                .AddAddressToGjelderNaboEiendom("Storgata 3", "Storgata", "3", null, "3800", "Bø i Telemark", "no")
                .Build();

            val.MunicipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");
            val.ParallelNaboeierValidering(nabovarselV4);
            var result = val.GetResult();
            result.messages.Where(m => m.reference == "4655.1.4.6").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);

        }
        [Fact(DisplayName = "4655.1.1.4 NaboeierValidering - Error test")]
        public void nabovarselV4_4655_1_1_4_NaboeierValideringErrorTest()
        {

            var val = new NabovarselV4Validator();

            var nabovarselV4 = new NabovarselV4Builder()
                .NaboEiere("Hans Hansen", "privatperson", "yFNSdYXRoUXyobi8ousqk/tC6pOQ9beKApBzDPonOy1xkMNBLPqxpBXvd3a7XUsJTdJjluVyjAe1viEosL36QdsSzBJm7kMa7frpHfOVyyF6lfzUQSijBcTbY7DCm/Frc3hGM81BcmH6WX8e410oOuZh6lk1t7H93bmu8BknxXs=",
                    null, "storgate 1", "3800", "Bø i Telemark", "no")
                .Build();

            val.MunicipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");
            val.ParallelNaboeierValidering(nabovarselV4);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4655.1.1.4").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);

        }
        [Fact(DisplayName = "4655.1.2.3 NaboeierValidering - Error test")]
        public void nabovarselV4_4655_1_2_3_NaboeierValideringErrorTest()
        {
            var val = new NabovarselV4Validator();
            var nabovarselV4 = new NabovarselV4Builder()
                .NaboEiere("Hans Hansen", "privatperson", "yFNSdYXRoUXyobi8ousqk/tC6pOQ9beKApBzDPonOy1xkMNBLPqxpBXvd3a7XUsJTdJjluVyjAe1viEosL36QdsSzBJm7kMa7frpHfOVyyF6lfzUQSijBcTbY7DCm/Frc3hGM81BcmH6WX8e410oOuZh6lk1t7H93bmu8BknxXs=",
                    null, "storgate 1", "3800", "Bø i Telemark", "no")
                .AddGjelderNaboeiendom(null, null, "Benjamin")
                .AddEiendomsIdentifikasjonToGelderNaboeiendom("*2004")
                .AddAddressToGjelderNaboEiendom("Storgata 3", "Storgata", "3", null, "3800", "Bø i Telemark", "no")
                .Build();

            val.MunicipalityValidator = TestHelper.MokMunicipalityValidatorResult("Invalid");
            val.ParallelNaboeierValidering(nabovarselV4);
            var result = val.GetResult();


            result.messages.Where(m => m.reference == "4655.1.2.3").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);

        }
        [Fact(DisplayName = "4655.3.13 NaboeierValidering - Error test")]
        public void nabovarselV4_4655_3_13_NaboeierValideringErrorTest()
        {
            var val = new NabovarselV4Validator();
            var nabovarselV4 = new NabovarselV4Builder()
                .NaboEiere("Hans Hansen", "privatperson", "yFNSdYXRoUXyobi8ousqk/tC6pOQ9beKApBzDPonOy1xkMNBLPqxpBXvd3a7XUsJTdJjluVyjAe1viEosL36QdsSzBJm7kMa7frpHfOVyyF6lfzUQSijBcTbY7DCm/Frc3hGM81BcmH6WX8e410oOuZh6lk1t7H93bmu8BknxXs=",
                    null, "storgate 1", "3800", "Bø i Telemark", "no")
                .AddGjelderNaboeiendom(null, null, "Benjamin")
                .AddEiendomsIdentifikasjonToGelderNaboeiendom("2004")
                .AddAddressToGjelderNaboEiendom()
                .Build();
            val.MunicipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");
            val.ParallelNaboeierValidering(nabovarselV4);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4655.3.13").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);

        }
        [Fact(DisplayName = "4655.3.13.1 NaboeierValidering - Error test")]
        public void nabovarselV4_4655_3_13_1_NaboeierValideringErrorTest()
        {
            var val = new NabovarselV4Validator();
            var nabovarselV4 = new NabovarselV4Builder()
                .NaboEiere("Hans Hansen", "privatperson", "yFNSdYXRoUXyobi8ousqk/tC6pOQ9beKApBzDPonOy1xkMNBLPqxpBXvd3a7XUsJTdJjluVyjAe1viEosL36QdsSzBJm7kMa7frpHfOVyyF6lfzUQSijBcTbY7DCm/Frc3hGM81BcmH6WX8e410oOuZh6lk1t7H93bmu8BknxXs=",
                    null, "storgate 1", "3800", "Bø i Telemark", "no")
                .AddGjelderNaboeiendom(null, null, "Benjamin")
                .AddEiendomsIdentifikasjonToGelderNaboeiendom("2004")
                .AddAddressToGjelderNaboEiendom(null, "Storgata", "3", null, "3800", "Bø i Telemark", "no").Build();

            val.MunicipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");
            val.ParallelNaboeierValidering(nabovarselV4);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4655.3.13.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4655.3.19.1 NaboeierValidering - Error test")]
        public void nabovarselV4_4655_1_2_19_1_NaboeierValideringErrorTest()
        {
            var val = new NabovarselV4Validator();
            var nabovarselV4 = new NabovarselV4Builder()
                .NaboEiere("Hans Hansen", "privatperson", "yFNSdYXRoUXyobi8ousqk/tC6pOQ9beKApBzDPonOy1xkMNBLPqxpBXvd3a7XUsJTdJjluVyjAe1viEosL36QdsSzBJm7kMa7frpHfOVyyF6lfzUQSijBcTbY7DCm/Frc3hGM81BcmH6WX8e410oOuZh6lk1t7H93bmu8BknxXs=",
                    null, "storgate 1", "3800", "Bø i Telemark", "no")
                .AddGjelderNaboeiendom(null, null, "Benjamin")
                .AddEiendomsIdentifikasjonToGelderNaboeiendom("2004")
                .AddAddressToGjelderNaboEiendom("Storgate 3", "Storgata", "3", null, "*3800", "Bø i Telemark", "NO").Build();
            val.MunicipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");
            val._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "", "Bø i Telemark");
            val.MunicipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");
            val.ParallelNaboeierValidering(nabovarselV4);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4655.3.19.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4655.3.19.3 NaboeierValidering - Error test")]
        public void nabovarselV4_4655_1_2_19_3_NaboeierValideringErrorTest()
        {
            var val = new NabovarselV4Validator();
            var nabovarselV4 = new NabovarselV4Builder()
                .NaboEiere("Hans Hansen", "privatperson", "yFNSdYXRoUXyobi8ousqk/tC6pOQ9beKApBzDPonOy1xkMNBLPqxpBXvd3a7XUsJTdJjluVyjAe1viEosL36QdsSzBJm7kMa7frpHfOVyyF6lfzUQSijBcTbY7DCm/Frc3hGM81BcmH6WX8e410oOuZh6lk1t7H93bmu8BknxXs=",
                    null, "storgate 1", "3800", "Bø i Telemark", "no")
                .AddGjelderNaboeiendom(null, null, "Benjamin")
                .AddEiendomsIdentifikasjonToGelderNaboeiendom("2004")
                .AddAddressToGjelderNaboEiendom("Storgate 3", "Storgata", "3", null, "3800", "*Bø i Telemark", "NO").Build();
            val.MunicipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");
            val._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "", "Bø i Telemark");
            val.MunicipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");
            val.ParallelNaboeierValidering(nabovarselV4);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4655.3.19.3").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4655.1.1.21 NaboeierValidering - Error test", Skip = "It is not relevant to the validation rules.")]
        public void nabovarselV4_4655_1_1_21_NaboeierValideringErrorTest()
        {

            var val = new NabovarselV4Validator();

            var nabovarselV4 = new NabovarselV4Builder()
                .NaboEiere("Hans Hansen", "privatperson", "yFNSdYXRoUXyobi8ousqk/tC6pOQ9beKApBzDPonOy1xkMNBLPqxpBXvd3a7XUsJTdJjluVyjAe1viEosL36QdsSzBJm7kMa7frpHfOVyyF6lfzUQSijBcTbY7DCm/Frc3hGM81BcmH6WX8e410oOuZh6lk1t7H93bmu8BknxXs=",
                    null, "storgate 1", "3800", "Bø i Telemark", "no")
                .AddGjelderNaboeiendom(null, null, "Benjamin")
                .AddEiendomsIdentifikasjonToGelderNaboeiendom(null)
                .AddAddressToGjelderNaboEiendom("Storgate 3", "Storgata", "3", null, "3800", "Bø i Telemark", "NO").Build();
            val.ParallelNaboeierValidering(nabovarselV4);
            var result = val.GetResult();

            int? messagesCount = _externalDependences ? 1 : (int?)null;
            AssertResult(result, "4655.1.1.21", messagesCount);
        }

        [Fact(DisplayName = "4655.1.1.3 NaboeierValidering - Error test")]
        public void nabovarselV4_4655_1_1_3_NaboeierValideringErrorTest()
        {
            var val = new NabovarselV4Validator();
            var municipalityValidator = new Mock<IMunicipalityValidator>();
            municipalityValidator.Setup(s => s.ValidateKommunenummer(It.IsAny<string>())).Returns(new GeneralValidationResult() { Status = ValidationStatus.Ok, Message = "UnitTest" });
            val.MunicipalityValidator = municipalityValidator.Object;

            var nabovarselV4 = new NabovarselV4Builder()
                .NaboEiere("Hans Hansen", "privatperson", "yFNSdYXRoUXyobi8ousqk/tC6pOQ9beKApBzDPonOy1xkMNBLPqxpBXvd3a7XUsJTdJjluVyjAe1viEosL36QdsSzBJm7kMa7frpHfOVyyF6lfzUQSijBcTbY7DCm/Frc3hGM81BcmH6WX8e410oOuZh6lk1t7H93bmu8BknxXs=",
                    null)
                .AddGjelderNaboeiendom(null, null, "Benjamin")
                .AddEiendomsIdentifikasjonToGelderNaboeiendom("2004")
                .AddAddressToGjelderNaboEiendom("Storgate 3", "Storgata", "3", null, "3800", "Bø i Telemark", "NO").Build();

            val.MunicipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");
            val.ParallelNaboeierValidering(nabovarselV4);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4655.1.1.3").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4655.1.1.3.1 NaboeierValidering - Error test")]
        public void nabovarselV4_4655_1_1_3_1_NaboeierValideringErrorTest()
        {
            var val = new NabovarselV4Validator();
            var municipalityValidator = new Mock<IMunicipalityValidator>();
            municipalityValidator.Setup(s => s.ValidateKommunenummer(It.IsAny<string>())).Returns(new GeneralValidationResult() { Status = ValidationStatus.Ok, Message = "UnitTest" });
            val.MunicipalityValidator = municipalityValidator.Object;

            var nabovarselV4 = new NabovarselV4Builder()
                .NaboEiere("Hans Hansen", "privatperson", "yFNSdYXRoUXyobi8ousqk/tC6pOQ9beKApBzDPonOy1xkMNBLPqxpBXvd3a7XUsJTdJjluVyjAe1viEosL36QdsSzBJm7kMa7frpHfOVyyF6lfzUQSijBcTbY7DCm/Frc3hGM81BcmH6WX8e410oOuZh6lk1t7H93bmu8BknxXs=",
                    null, null, "3800", "Bø i Telemark", "no")
                .AddGjelderNaboeiendom(null, null, "Benjamin")
                .AddEiendomsIdentifikasjonToGelderNaboeiendom("2004")
                .AddAddressToGjelderNaboEiendom("Storgate 3", "Storgata", "3", null, "3800", "Bø i Telemark", "NO").Build();
            val.MunicipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");
            val.ParallelNaboeierValidering(nabovarselV4);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4655.1.1.3.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);

        }
        [Fact(DisplayName = "4655.3.31 NaboeierValidering - Error test")]
        public void nabovarselV4_4655_3_31_NaboeierValideringErrorTest()
        {
            var val = new NabovarselV4Validator();
            var nabovarselV4 = new NabovarselV4Builder()
                .NaboEiere("Hans Hansen", "privatperson", "yFNSdYXRoUXyobi8ousqk/tC6pOQ9beKApBzDPonOy1xkMNBLPqxpBXvd3a7XUsJTdJjluVyjAe1viEosL36QdsSzBJm7kMa7frpHfOVyyF6lfzUQSijBcTbY7DCm/Frc3hGM81BcmH6WX8e410oOuZh6lk1t7H93bmu8BknxXs=",
                    null, "storgate 1", "3800", "Bø i Telemark", "*no")

                .AddGjelderNaboeiendom(null, null, "Benjamin")
                .AddEiendomsIdentifikasjonToGelderNaboeiendom("2004")
                .AddAddressToGjelderNaboEiendom("Storgate 3", "Storgata", "3", null, "3800", "Bø i Telemark", "NO").Build();
            val.MunicipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");
            val.ParallelNaboeierValidering(nabovarselV4);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4655.3.31").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4655.3.24.1 NaboeierValidering - Error test")]
        public void nabovarselV4_4655_3_24_1_NaboeierValideringErrorTest()
        {
            var val = new NabovarselV4Validator();
            var nabovarselV4 = new NabovarselV4Builder()
                .NaboEiere("Hans Hansen", "privatperson", "yFNSdYXRoUXyobi8ousqk/tC6pOQ9beKApBzDPonOy1xkMNBLPqxpBXvd3a7XUsJTdJjluVyjAe1viEosL36QdsSzBJm7kMa7frpHfOVyyF6lfzUQSijBcTbY7DCm/Frc3hGM81BcmH6WX8e410oOuZh6lk1t7H93bmu8BknxXs=",
                    null, "storgate 1", "*3800", "Bø i Telemark", "no")

                .AddGjelderNaboeiendom(null, null, "Benjamin")
                .AddEiendomsIdentifikasjonToGelderNaboeiendom("2004")
                .AddAddressToGjelderNaboEiendom("Storgate 3", "Storgata", "3", null, "3800", "Bø i Telemark", "NO").Build();
            val.MunicipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");
            val._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "", "Bø i Telemark");
            val.ParallelNaboeierValidering(nabovarselV4);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4655.3.24.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4655.3.24.3 NaboeierValidering - Error test")]
        public void nabovarselV4_4655_3_24_3_NaboeierValideringErrorTest()
        {
            var val = new NabovarselV4Validator();
            var nabovarselV4 = new NabovarselV4Builder()
                .NaboEiere("Hans Hansen", "privatperson", "yFNSdYXRoUXyobi8ousqk/tC6pOQ9beKApBzDPonOy1xkMNBLPqxpBXvd3a7XUsJTdJjluVyjAe1viEosL36QdsSzBJm7kMa7frpHfOVyyF6lfzUQSijBcTbY7DCm/Frc3hGM81BcmH6WX8e410oOuZh6lk1t7H93bmu8BknxXs=",
                    null, "storgate 1", "3800", "*Bø i Telemark", "no")

                .AddGjelderNaboeiendom(null, null, "Benjamin")
                .AddEiendomsIdentifikasjonToGelderNaboeiendom("2004")
                .AddAddressToGjelderNaboEiendom("Storgate 3", "Storgata", "3", null, "3800", "Bø i Telemark", "NO").Build();
            val.MunicipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");
            val._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "", "Bø i Telemark");
            val.ParallelNaboeierValidering(nabovarselV4);
            var result = val.GetResult();
            result.messages.Where(m => m.reference == "4655.3.24.3").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4655.3.20 NaboeierValidering - Error test")]
        public void nabovarselV4_4655_3_20_NaboeierValideringErrorTest()
        {
            var val = new NabovarselV4Validator();
            var nabovarselV4 = new NabovarselV4Builder()
                .NaboEiere("Hans Hansen", "privatperson", "yFNSdYXRoUXyobi8ousqk/tC6pOQ9beKApBzDPonOy1xkMNBLPqxpBXvd3a7XUsJTdJjluVyjAe1viEosL36QdsSzBJm7kMa7frpHfOVyyF6lfzUQSijBcTbY7DCm/Frc3hGM81BcmH6WX8e410oOuZh6lk1t7H93bmu8BknxXs=",
                    null, "storgate 1", "3800", "Bø i Telemark", "no")

                .AddGjelderNaboeiendom(null, null, "Benjamin")
                .AddEiendomsIdentifikasjonToGelderNaboeiendom("2004")
                .AddAddressToGjelderNaboEiendom("Storgate 3", "Storgata", "3", null, "3800", "Bø i Telemark", "*NO").Build();
            val.MunicipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");
            val.ParallelNaboeierValidering(nabovarselV4);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4655.3.20").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4655.1 SoekerValidation - OK test")]
        public void nabovarselV4_4655_1_SoekerValidationErrorTest()
        {
            var val = new NabovarselV4Validator();

            var nabovarselV4 = new NabovarselV4Builder()
                .AnsvarligSoeker("974760673", null, "Foretak", "Foretak", "Benjamin Fjell", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "no", "Trondheim", "11223344", "99887766", "post@test.no")
                //.AnsvarligSoeker(null, "25125401530", "Privatperson",null, "Hans Hansen", "Storgata 5","7003", "no", "Trondheim", "11223344", "99887766","post@test.no")
                .Build();
            val.SoekerValidation(nabovarselV4);
            var result = val.GetResult();

            TestHelper.AssertResult(result, null, 0, true);
        }
        [Fact(DisplayName = "4655.1.1.6 TiltakshaverHarLovligKodeverdi - Error test")]
        public void nabovarselV4_4655_1_1_6_TiltakshaverHarLovligKodeverdiErrorTest()
        {
            var val = new NabovarselV4Validator();

            var nabovarselV4 = new NabovarselV4Builder()
                .Tiltakshaver(null, "25125401530", null, null, null, "Hans Hansen", "Storgata 5", "7003", "no", "Trondheim", "11223344", "99887766")
                .Build();
            val.TiltakshaverHarLovligKodeverdi(nabovarselV4);
            var result = val.GetResult();

            result.messages.Count.Should().Be(1);

            var ruleId = "4655.1.1";
            result.messages.Where(m => m.reference.Contains(ruleId)).Should().NotBeNullOrEmpty();
        }
        [Fact(DisplayName = "4655.1.2.5 TiltakshaverHarLovligKodeverdi - Error test")]
        public void nabovarselV4_4655_1_2_11_TiltakshaverHarLovligKodeverdiErrorTest()
        {
            var val = new NabovarselV4Validator();

            var nabovarselV4 = new NabovarselV4Builder()
                .Tiltakshaver(null, "25125401530", "*Privatperson", null, null, "Hans Hansen", "Storgata 5", "7003", "no", "Trondheim", "11223344", "99887766")
                .Build();
            val.TiltakshaverHarLovligKodeverdi(nabovarselV4);
            var result = val.GetResult();

            result.messages.Count.Should().Be(1);

            var ruleId = "4655.1.2";
            result.messages.Where(m => m.reference.Contains(ruleId)).Should().NotBeNullOrEmpty();
        }
        [Fact(DisplayName = "4655.1.1.14 TiltakshaverHarLovligKodeverdi - Error test")]
        public void nabovarselV4_4655_1_1_14_TiltakshaverHarLovligKodeverdiErrorTest()
        {
            var val = new NabovarselV4Validator();

            var nabovarselV4 = new NabovarselV4Builder()
                .Tiltakshaver(null, "25125401530", "Privatperson", null, null, "", "Storgata 5", "7003", "no", "Trondheim", "11223344", "99887766")
                .Build();
            val.TiltakshaverHarLovligKodeverdi(nabovarselV4);
            var result = val.GetResult();

            result.messages.Count.Should().Be(1);

            var ruleId = "4655.1.1";
            result.messages.Where(m => m.reference.Contains(ruleId)).Should().NotBeNullOrEmpty();
        }

        [Fact(DisplayName = "4655.1.2.5 SoekerValidation - Error test")]
        public void nabovarselV4_4655_1_2_5_SoekerValidationErrorTest()
        {
            var val = new NabovarselV4Validator();

            var nabovarselV4 = new NabovarselV4Builder()
                .AnsvarligSoeker(null, "25125401530", "*Privatperson", null, "Hans Hansen", "Hans Hansen", "Storgata 5", "7003", "no", "Trondheim", "11223344", "99887766", "epost@test.no")
                .Build();
            val.SoekerValidation(nabovarselV4);
            var result = val.GetResult();


            result.messages.Where(m => m.reference == "4655.1.2.5").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);

        }
        [Fact(DisplayName = "4655.1.5.3 SoekerValidation - Error test")]
        public void nabovarselV4_4655_1_5_3_SoekerValidationErrorTest()
        {
            var val = new NabovarselV4Validator();
            var nabovarselV4 = new NabovarselV4Builder()
                .AnsvarligSoeker(null, "*25125401530", "Privatperson", null, "Hans Hansen", "Hans Hansen", "Storgata 5", "7003", "no", "Trondheim", "11223344", "99887766", "epost@test.no")
                .Build();
            val.SoekerValidation(nabovarselV4);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4655.1.5.3").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);

        }
        [Fact(DisplayName = "4655.1.5.4.2 SoekerValidation - Error test")]
        public void nabovarselV4_4655_1_5_4_2_SoekerValidationErrorTest()
        {
            var val = new NabovarselV4Validator();
            var nabovarselV4 = new NabovarselV4Builder()
                    .AnsvarligSoeker("*974760673", null, "Foretak", "Foretak", "Benjamin Fjell", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "no", "Trondheim", "11223344", "99887766", "epost@test.no")
                    .Build();
            val.SoekerValidation(nabovarselV4);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4655.1.5.4.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4655.3.21 SoekerValidation - Error test")]
        public void nabovarselV4_4655_3_21_SoekerValidationErrorTest()
        {
            var val = new NabovarselV4Validator();

            var nabovarselV4 = new NabovarselV4Builder()
                    .Tiltakshaver("974760673", null, "Foretak", "Foretak", null, "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "no", "Trondheim", "11223344", "99887766", "epost@test.no")
                .Build();
            val.KontaktPersonValidering(nabovarselV4);
            var result = val.GetResult();

            int? messagesCount = _externalDependences ? 1 : (int?)null;
            AssertResult(result, "4655.3.21", messagesCount);

        }
        [Fact(DisplayName = "4655.1.1.7 SoekerValidation - Error test")]
        public void nabovarselV4_4655_1_1_7_SoekerValidationErrorTest()
        {
            var val = new NabovarselV4Validator();

            var nabovarselV4 = new NabovarselV4Builder()
                .AnsvarligSoeker("974760673", null, "Foretak", "Foretak", "Benjamin Fjell", "Siv. Ark. Fjell og Sønner", null, "7003", "no", "Trondheim", "11223344", "99887766", "epost@test.no")
                .Build();
            val.SoekerValidation(nabovarselV4);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4655.1.1.7").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4655.3.23 SoekerValidation - Error test")]
        public void nabovarselV4_4655_3_23_SoekerValidationErrorTest()
        {
            var val = new NabovarselV4Validator();

            var nabovarselV4 = new NabovarselV4Builder()
                .AnsvarligSoeker("974760673", null, "Foretak", "Foretak", "Benjamin Fjell", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "*AR", "Trondheim", "11223344", "99887766", "epost@test.no")
                .Build();
            val.SoekerValidation(nabovarselV4);
            var result = val.GetResult();

            AssertResult(result, "4655.3.23", 1);

        }
        [Fact(DisplayName = "4655.3.22.3 SoekerValidation - Error test")]
        public void nabovarselV4_4655_3_22_3_SoekerValidationErrorTest()
        {
            var val = new NabovarselV4Validator();

            var nabovarselV4 = new NabovarselV4Builder()
                .AnsvarligSoeker("974760673", null, "Foretak", "Foretak", "Benjamin Fjell", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "no", "*Trondheim", "11223344", "99887766", "epost@test.no")
                .Build();
            val._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "", "Trondheim");
            val.SoekerValidation(nabovarselV4);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4655.3.22.3").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4655.3.15 SoekerValidation - Error test")]
        public void nabovarselV4_4655_3_15_SoekerValidationErrorTest()
        {
            var val = new NabovarselV4Validator();

            var nabovarselV4 = new NabovarselV4Builder()
                .AnsvarligSoeker("974760673", null, "Foretak", "Foretak", "Benjamin Fjell", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "no", "Trondheim", null, null, "epost@test.no")
                .Build();
            val.SoekerValidation(nabovarselV4);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "4655.3.15", 1, true);
        }
        [Fact(DisplayName = "4655.3.14 SoekerValidation - OK test")]
        public void nabovarselV4_4655_1_1_23_SoekerValidationErrorTest()
        {
            var val = new NabovarselV4Validator();

            var nabovarselV4 = new NabovarselV4Builder()
                .AnsvarligSoeker("974760673", null, "Foretak", "Foretak", "Benjamin Fjell", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "no", "Trondheim", "11223344", "99887766", null)
                .Build();
            val.SoekerValidation(nabovarselV4);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "4655.3.14", 1, true);

        }

        [Fact(DisplayName = "4655.1 GjeldendePlanHarLovligVerdi Ok Test")]
        public void nabovarselV4_4655_1_GjeldendePlanHarLovligVerdiOkTest()
        {
            // Koder må være ihht publiserte kodelister i XSD skjema
            var val = new NabovarselV4Validator();

            var nabovarselV4 = new NabovarselV4Builder()
                .GjeldendePlan("RP", "Reguleringsplan")
                .Build();
            val.GjeldendePlanHarLovligVerdi(nabovarselV4);
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4655.1.2.8 GjeldendePlanHarLovligVerdi Error Test")]
        public void nabovarselV4_4655_1_2_8_GjeldendePlanHarLovligVerdiErrorTest()
        {
            // Koder må være ihht publiserte kodelister i XSD skjema
            var val = new NabovarselV4Validator();

            var nabovarselV4 = new NabovarselV4Builder()
                .GjeldendePlan("123", null)
                .Build();
            val.GjeldendePlanHarLovligVerdi(nabovarselV4);
            var result = val.GetResult();

            result.messages.Count.Should().Be(1);

            var ruleId = "4655.1.2";
            result.messages.Where(m => m.reference.Contains(ruleId)).Should().NotBeNullOrEmpty();
        }
        [Fact(DisplayName = "4655.1.1.11 GjeldendePlanHarLovligVerdi Error Test")]
        public void nabovarselV4_4655_1_1_11_GjeldendePlanHarLovligVerdiErrorTest()
        {
            // Koder må være ihht publiserte kodelister i XSD skjema
            var val = new NabovarselV4Validator();

            var nabovarselV4 = new NabovarselV4Builder()
                .GjeldendePlan("RP", null)
                .Build();
            val.GjeldendePlanHarLovligVerdi(nabovarselV4);
            var result = val.GetResult();

            var ruleId = "4655.1.1";
            result.messages.Where(m => m.reference.Contains(ruleId)).Should().NotBeNullOrEmpty();
        }

        [Fact(DisplayName = "4655.3 DispensasjonstypeErLovlig OK Test")]
        public void nabovarselV4_4655_3_DispensasjonstypeErLovligOKTest()
        {
            var val = new NabovarselV4Validator();

            var nabovarselV4 = new NabovarselV4Builder()
                .Dispensasjon("PLAN", "Arealplan", "Søknad om dispensasjon fra bestemmelsen NN i plan", "Begrunnelse for dispensasjon")
                .Build();
            val.DispensasjonstypeErLovlig(nabovarselV4, _formSetElements);
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4655.3.10 DispensasjonstypeErLovlig Error Test")]
        public void nabovarselV4_4655_3_10_DispensasjonstypeErLovligErrorTest()
        {
            var val = new NabovarselV4Validator();

            var nabovarselV4 = new NabovarselV4Builder()
                .Dispensasjon("PLAN", "Arealplan", null, "Begrunnelse for dispensasjon")
                .Build();
            val.DispensasjonstypeErLovlig(nabovarselV4, _formSetElements);
            var result = val.GetResult();

            AssertResult(result, "4655.3.10", 1);

        }
        [Fact(DisplayName = "4655.3.11 DispensasjonstypeErLovlig Error Test")]
        public void nabovarselV4_4655_3_11_DispensasjonstypeErLovligErrorTest()
        {
            // Når dispensasjon er registrert bør beskrivelse fylles ut	Advarsel
            var val = new NabovarselV4Validator();

            var nabovarselV4 = new NabovarselV4Builder()
                .Dispensasjon("PLAN", "Arealplan", "Søknad om dispensasjon fra bestemmelsen NN i plan", null)
                .Build();
            val.DispensasjonstypeErLovlig(nabovarselV4, _formSetElements);
            var result = val.GetResult();

            AssertResult(result, "4655.3.11", 1);

        }
        [Fact(DisplayName = "4655.1.2.9_DispensasjonstypeErLovligErrorTest")]
        public void nabovarselV4_4655_1_2_9_DispensasjonstypeErLovligErrorTest()
        {
            // Når dispensasjon er registrert bør beskrivelse fylles ut	Advarsel
            var val = new NabovarselV4Validator();

            var nabovarselV4 = new NabovarselV4Builder()
                .Dispensasjon("*PLAN", "Arealplan", "Søknad om dispensasjon fra bestemmelsen NN i plan", "Begrunnelse for dispensasjon")
                .Build();
            val.DispensasjonstypeErLovlig(nabovarselV4, _formSetElements);
            var result = val.GetResult();

            result.messages.Count.Should().Be(1);

            var ruleId = "4655.1.2";
            result.messages.Where(m => m.reference.Contains(ruleId)).Should().NotBeNullOrEmpty();
        }

        [Fact(DisplayName = "4655.1. MinstEtTiltak OK Test")]
        public void nabovarselV4_4655_1_MinstEtTiltakOKTest()
        {
            // Vedlegg Situasjonsplan mangler for valgte tiltakstyper i varsel(jfr nasjonal sjekkliste pkt 1.28).	Se nasjonal sjekkliste pkt 1.28 for tiltakstyper	Feil
            var val = new NabovarselV4Validator();
            var tiltaksformaalKodeType = new Dictionary<string, string>()
            {
                {"Fritidsbolig","Fritidsbolig" },
                {"Annet","Annet" },
            };
            var tiltaksType = new Dictionary<string, string>()
            {
                {"rivinghelebygg","Riving av hele bygg" },
                {"nyttbyggboligformal","Nytt bygg - Boligformål" },
            };
            var nabovarselV4 = new NabovarselV4Builder()
                .BeskrivelseAvTiltak(tiltaksType, "Ny fritidsbolig med laftet anneks", tiltaksformaalKodeType)
                .Build();
            val.MinstEtTiltak(nabovarselV4);
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }

        [Fact(DisplayName = "4655.1.1.10 MinstEtTiltak Error Test")]
        public void nabovarselV4_4655_1_1_10_MinstEtTiltakErrorTest()
        {
            var val = new NabovarselV4Validator();


            var nabovarselV4 = new NabovarselV4Builder()
                .Build();
            val.MinstEtTiltak(nabovarselV4);

            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4655.1.1.10").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4655.1.1.9 MinstEtTiltak Error Test")]
        public void nabovarselV4_4655_1_1_9_MinstEtTiltakErrorTest()
        {
            var val = new NabovarselV4Validator();

            var tiltaksformaalKodeType = new Dictionary<string, string>()
            {
                {"Fritidsbolig","Fritidsbolig" },
                {"Annet","Annet" },
            };

            var nabovarselV4 = new NabovarselV4Builder()
                .BeskrivelseAvTiltak(null, "Ny fritidsbolig med laftet anneks", tiltaksformaalKodeType)
                .Build();
            val.MinstEtTiltak(nabovarselV4);

            var result = val.GetResult();
            result.messages.Count.Should().Be(1);

            var ruleId = "4655.1.1";
            result.messages.Where(m => m.reference.Contains(ruleId)).Should().NotBeNullOrEmpty();
        }
        [Fact(DisplayName = "4655.1.2.6 MinstEtTiltak Error Test")]
        public void nabovarselV4_4655_1_2_6_MinstEtTiltakErrorTest()
        {
            var val = new NabovarselV4Validator();

            var tiltaksformaalKodeType = new Dictionary<string, string>()
            {
                {"Fritidsbolig","Fritidsbolig" },
                {"Annet","Annet" },
            };
            var tiltaksType = new Dictionary<string, string>()
            {
                {"*riving","Riving" },
                {"nyttbyggboligformal","Nytt bygg - Boligformål" },
            };
            var nabovarselV4 = new NabovarselV4Builder()
                .BeskrivelseAvTiltak(tiltaksType, "Ny fritidsbolig med laftet anneks", tiltaksformaalKodeType)
                .Build();
            val.MinstEtTiltak(nabovarselV4);
            var result = val.GetResult();

            result.messages.Count.Should().Be(1);

            var ruleId = "4655.1.2";
            result.messages.Where(m => m.reference.Contains(ruleId)).Should().NotBeNullOrEmpty();
        }
        [Fact(DisplayName = "4655.1.1.10 MinstEtTiltak Error Test")]
        public void nabovarselV4_4655_1_1_10aa_MinstEtTiltakErrorTest()
        {
            var val = new NabovarselV4Validator();

            var tiltaksType = new Dictionary<string, string>()
            {
                {"rivinghelebygg","Riving av hele bygg" },
                {"nyttbyggboligformal","Nytt bygg - Boligformål" },
            };
            var nabovarselV4 = new NabovarselV4Builder()
                .BeskrivelseAvTiltak(tiltaksType, "Ny fritidsbolig med laftet anneks", null)
                .Build();
            val.MinstEtTiltak(nabovarselV4);
            var result = val.GetResult();

            result.messages.Count.Should().Be(1);

            var ruleId = "4655.1.1";
            result.messages.Where(m => m.reference.Contains(ruleId)).Should().NotBeNullOrEmpty();
        }
        [Fact(DisplayName = "4655.1.2.7 MinstEtTiltak Error Test")]
        public void nabovarselV4_4655_1_2_7_MinstEtTiltakErrorTest()
        {
            var val = new NabovarselV4Validator();

            var tiltaksformaalKodeType = new Dictionary<string, string>()
            {
                {"Fritidsbolig","Fritidsbolig" },
                {"*Annet","Annet" },
            };
            var tiltaksType = new Dictionary<string, string>()
            {
                                {"rivinghelebygg","Riving av hele bygg" },
                {"nyttbyggboligformal","Nytt bygg - Boligformål" },
            };
            var nabovarselV4 = new NabovarselV4Builder()
                .BeskrivelseAvTiltak(tiltaksType, "Ny fritidsbolig med laftet anneks", tiltaksformaalKodeType)
                .Build();
            val.MinstEtTiltak(nabovarselV4);
            var result = val.GetResult();

            result.messages.Count.Should().Be(1);

            var ruleId = "4655.1.2";
            result.messages.Where(m => m.reference.Contains(ruleId)).Should().NotBeNullOrEmpty();
        }

        [Fact(DisplayName = "4655.2 SituasjonsplanValidering OK Test")]
        public void nabovarselV4_4655_2_SituasjonsplanValideringOKTest()
        {
            // Vedlegg Situasjonsplan mangler for valgte tiltakstyper i varsel(jfr nasjonal sjekkliste pkt 1.28).	Se nasjonal sjekkliste pkt 1.28 for tiltakstyper	Feil
            var val = new NabovarselV4Validator();
            var tiltaksformaalKodeType = new Dictionary<string, string>()
            {
                {"Fritidsbolig","Fritidsbolig" },
                {"Annet","Annet" },
            };
            var tiltaksType = new Dictionary<string, string>()
            {
                                {"rivinghelebygg","Riving av hele bygg" },
                {"nyttbyggboligformal","Nytt bygg - Boligformål" },
            };
            var nabovarselV4 = new NabovarselV4Builder()
                .BeskrivelseAvTiltak(tiltaksType, "Ny fritidsbolig med laftet anneks", tiltaksformaalKodeType)
                .Build();
            val.MinstEtTiltak(nabovarselV4);
            val.SituasjonsplanValidering(nabovarselV4, new List<string>() { "Situasjonsplan" });
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4655.2.7 SituasjonsplanValidering Error Test")]
        public void nabovarselV4_4655_2_7_SituasjonsplanValideringErrorTest()
        {
            // Vedlegg Situasjonsplan mangler for valgte tiltakstyper i varsel(jfr nasjonal sjekkliste pkt 1.28).	Se nasjonal sjekkliste pkt 1.28 for tiltakstyper	Feil
            var val = new NabovarselV4Validator();
            var tiltaksformaalKodeType = new Dictionary<string, string>()
            {
                {"Fritidsbolig","Fritidsbolig" },
                {"Annet","Annet" },
            };
            var tiltaksType = new Dictionary<string, string>()
            {
                                {"rivinghelebygg","Riving av hele bygg" },
                {"nyttbyggboligformal","Nytt bygg - Boligformål" },
            };
            var nabovarselV4 = new NabovarselV4Builder()
                .BeskrivelseAvTiltak(tiltaksType, "Ny fritidsbolig med laftet anneks", tiltaksformaalKodeType)
                .Build();
            val.MinstEtTiltak(nabovarselV4);
            val.SituasjonsplanValidering(nabovarselV4, null);
            var result = val.GetResult();

            AssertResult(result, "4655.2.7", 1);

        }
        [Fact(DisplayName = "4655.2.7b SituasjonsplanValidering Error Test")]
        public void nabovarselV4_4655_2_7b_SituasjonsplanValideringErrorTest()
        {
            // Vedlegg Situasjonsplan mangler for valgte tiltakstyper i varsel(jfr nasjonal sjekkliste pkt 1.28).	Se nasjonal sjekkliste pkt 1.28 for tiltakstyper	Feil
            var val = new NabovarselV4Validator();
            var tiltaksformaalKodeType = new Dictionary<string, string>()
            {
                {"Fritidsbolig","Fritidsbolig" },
                {"Annet","Annet" },
            };
            var tiltaksType = new Dictionary<string, string>()
            {
                                {"rivinghelebygg","Riving av hele bygg" },
                {"nyttbyggboligformal","Nytt bygg - Boligformål" },
            };
            var nabovarselV4 = new NabovarselV4Builder()
                .BeskrivelseAvTiltak(tiltaksType, "Ny fritidsbolig med laftet anneks", tiltaksformaalKodeType)
                .Build();
            val.MinstEtTiltak(nabovarselV4);
            val.SituasjonsplanValidering(nabovarselV4, new List<string>() { "*Situasjonsplan" });
            var result = val.GetResult();

            AssertResult(result, "4655.2.7", 1);

        }
        [Fact(DisplayName = "4655.2.32 TegningValidering OK Test")]
        public void nabovarselV4_4655_2_32_TegningValideringOkTest()

        {
            // Vedlegg Snitttegninger mangler for valgte tiltakstyper i varsel(jfr nasjonal sjekkliste pkt 1.28).	Se nasjonal sjekkliste pkt 1.28 for tiltakstyper	Feil
            var val = new NabovarselV4Validator();
            var tiltaksformaalKodeType = new Dictionary<string, string>()
            {
                {"Fritidsbolig","Fritidsbolig" },
                {"Annet","Annet" },
            };
            var tiltaksType = new Dictionary<string, string>()
            {
                                {"rivinghelebygg","Riving av hele bygg" },
                {"nyttbyggboligformal","Nytt bygg - Boligformål" },
            };
            var nabovarselV4 = new NabovarselV4Builder()
                .BeskrivelseAvTiltak(tiltaksType, "Ny fritidsbolig med laftet anneks", tiltaksformaalKodeType)
                .Build();
            val.MinstEtTiltak(nabovarselV4);
            val.TegningValidering(new List<string>() { "TegningNyttSnitt", "TegningNyFasade" });
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4655.2.32 TegningValidering Error Test")]
        public void nabovarselV4_4655_2_8_TegningValideringErrorTest()
        {
            // Vedlegg Snitttegninger mangler for valgte tiltakstyper i varsel(jfr nasjonal sjekkliste pkt 1.28).	Se nasjonal sjekkliste pkt 1.28 for tiltakstyper	Feil
            var val = new NabovarselV4Validator();
            var tiltaksformaalKodeType = new Dictionary<string, string>()
            {
                {"Fritidsbolig","Fritidsbolig" },
                {"Annet","Annet" },
            };
            var tiltaksType = new Dictionary<string, string>()
            {
                                {"rivinghelebygg","Riving av hele bygg" },
                {"nyttbyggboligformal","Nytt bygg - Boligformål" },
            };
            var nabovarselV4 = new NabovarselV4Builder()
                .BeskrivelseAvTiltak(tiltaksType, "Ny fritidsbolig med laftet anneks", tiltaksformaalKodeType)
                .Build();
            val.MinstEtTiltak(nabovarselV4);
            val.TegningValidering(null);
            var result = val.GetResult();

            AssertResult(result, "4655.2.32", 1);

        }
        [Fact(DisplayName = "4655.2.8 SnitttegningerValidering Error Test")]
        public void nabovarselV4_4655_2_8_SnitttegningerValideringErrorTest()
        {
            // Vedlegg Snitttegninger mangler for valgte tiltakstyper i varsel(jfr nasjonal sjekkliste pkt 1.28).	Se nasjonal sjekkliste pkt 1.28 for tiltakstyper	Feil
            var val = new NabovarselV4Validator();
            var tiltaksformaalKodeType = new Dictionary<string, string>()
            {
                {"Fritidsbolig","Fritidsbolig" },
                {"Annet","Annet" },
            };
            var tiltaksType = new Dictionary<string, string>()
            {
                                {"rivinghelebygg","Riving av hele bygg" },
                {"nyttbyggboligformal","Nytt bygg - Boligformål" },
            };
            var nabovarselV4 = new NabovarselV4Builder()
                .BeskrivelseAvTiltak(tiltaksType, "Ny fritidsbolig med laftet anneks", tiltaksformaalKodeType)
                .Build();
            val.MinstEtTiltak(nabovarselV4);
            val.TegningValidering(new List<string>() { "*TegningNyttSnitt", "TegningNyFasade" });
            var result = val.GetResult();

            AssertResult(result, "4655.2.8", 1);
        }

        [Fact(DisplayName = "4655.2.9 FasadetegningerValidering Error Test")]
        public void nabovarselV4_4655_2_9_FasadetegningerValideringErrorTest()
        {
            // Vedlegg Fasadetegninger mangler for valgte tiltakstyper i varsel(jfr nasjonal sjekkliste pkt 1.28).	Se nasjonal sjekkliste pkt 1.28 for tiltakstyper	Feil
            var val = new NabovarselV4Validator();
            var tiltaksformaalKodeType = new Dictionary<string, string>()
            {
                {"Fritidsbolig","Fritidsbolig" },
                {"Annet","Annet" },
            };
            var tiltaksType = new Dictionary<string, string>()
            {
                                {"rivinghelebygg","Riving av hele bygg" },
                {"nyttbyggboligformal","Nytt bygg - Boligformål" },
            };
            var nabovarselV4 = new NabovarselV4Builder()
                .BeskrivelseAvTiltak(tiltaksType, "Ny fritidsbolig med laftet anneks", tiltaksformaalKodeType)
                .Build();
            val.MinstEtTiltak(nabovarselV4);
            val.TegningValidering(new List<string>() { "*TegningNyFasade", "TegningNyttSnitt" });
            var result = val.GetResult();

            AssertResult(result, "4655.2.9", 1);
        }
        // Common to all validations
        private static void AssertResult(ValidationResult result, string ruleId, int? messagesCount)
        {
            if (messagesCount != null)
            {
                result.messages.Count.Should().Be(messagesCount);
            }

            var ruleMessagetype = result.rulesChecked.First(r => r.id == ruleId);
            result.messages.Where(m => m.reference == ruleId).Should().NotBeNullOrEmpty();
            result.messages.Where(m => m.reference == ruleId).All(r => r.messagetype == ruleMessagetype.messagetype).Should().BeTrue();
        }

    }
}