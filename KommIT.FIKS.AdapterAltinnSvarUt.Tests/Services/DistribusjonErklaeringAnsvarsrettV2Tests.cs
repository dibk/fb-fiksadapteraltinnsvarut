﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using FluentAssertions;
using KommIT.FIKS.AdapterAltinnSvarUt.Services;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.NotificationForm;
using KommIT.FIKS.AdapterAltinnSvarUt.Tests.Services.Builders;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Tests.Services
{
    public class DistribusjonErklaeringAnsvarsrettV2Tests
    {

        [Fact(DisplayName = "4419.1 AnsvarligSokerValidering - Ok test")]
        public void DistribusjonErklaeringAnsvarsrett_4419_1_4_AnsvarligSokerValideringOkTest()

        {
            var val = new DistribusjonErklaeringAnsvarsrettV2Validator();
            var distribusjonErklaeringAnsvarsrett = new DistribusjonErklaeringAnsvarsrettV2Builder()
                .AnsvarligSoeker("Foretak","Foretak", "Tom Bola")
                .Build();
            val.AnsvarligSokerValidering(distribusjonErklaeringAnsvarsrett);

            var result = val.GetResult();
            result.messages.Count.Should().Be(0);
        }

        [Fact(DisplayName = "4419.1.6 AnsvarligSokerValidering - Error test")]
        public void DistribusjonErklaeringAnsvarsrett_4419_1_6_AnsvarligSokerValideringErrorTest()
        {
            var val = new DistribusjonErklaeringAnsvarsrettV2Validator();
            var distribusjonErklaeringAnsvarsrett = new DistribusjonErklaeringAnsvarsrettV2Builder()
                .AnsvarligSoeker(null,null, "Tom Bola")

                .Build();
            val.AnsvarligSokerValidering(distribusjonErklaeringAnsvarsrett);

            var result = val.GetResult();
            AssertResult(result, "4419.1.6", 1);

        }
        [Fact(DisplayName = "4419.1.7 AnsvarligSokerValidering - Error test")]
        public void DistribusjonErklaeringAnsvarsrett_4419_1_7_AnsvarligSokerValideringErrorTest()

        {
            var val = new DistribusjonErklaeringAnsvarsrettV2Validator();
            var distribusjonErklaeringAnsvarsrett = new DistribusjonErklaeringAnsvarsrettV2Builder()
                .AnsvarligSoeker("Foretak*", "Foretak", "Tom Bola")
                .Build();
            val.AnsvarligSokerValidering(distribusjonErklaeringAnsvarsrett);

            var result = val.GetResult();
            AssertResult(result, "4419.1.7", 1);

        }

        [Fact(DisplayName = "4419.1.8 AnsvarligSokerValidering - Error test")]
        public void DistribusjonErklaeringAnsvarsrett_4419_1_8_AnsvarligSokerValideringErrorTest()
        {
            var val = new DistribusjonErklaeringAnsvarsrettV2Validator();
            var distribusjonErklaeringAnsvarsrett = new DistribusjonErklaeringAnsvarsrettV2Builder()
                .AnsvarligSoeker("Foretak", "Foretak")
                .Build();
            val.AnsvarligSokerValidering(distribusjonErklaeringAnsvarsrett);

            var result = val.GetResult();
            AssertResult(result, "4419.1.8", 1);

        }
        private static void AssertResult(ValidationResult result, string ruleId, int? messagesCount)
        {
            if (messagesCount != null)
            {
                result.messages.Count.Should().Be(messagesCount);
            }


            var ruleMessagetype = result.rulesChecked.First(r => r.id == ruleId);
            result.messages.Where(m => m.reference == ruleId).Should().NotBeNullOrEmpty();
            result.messages.Where(m => m.reference == ruleId).All(r => r.messagetype == ruleMessagetype.messagetype).Should().BeTrue();
        }
    }
}
