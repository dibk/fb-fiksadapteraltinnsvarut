﻿using FluentAssertions;
using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.QueuePreparators;
using KommIT.FIKS.AdapterAltinnSvarUt.Services;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Altinn;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Altinn.Correspondence;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Altinn.Prefill;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Azure;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Encryption;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Notification;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Prefill;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Svarut;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Svarut.StatusService;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.UserNotification;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.WS.Altinn;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;
using Moq;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Tests.Services
{
    public class LoggAltinnMessageTest
    {
        //TODO: Check if this still is under development... :/
        //[Fact(Skip = "Under Development")]
        //public void LoggAltinnMessageOkTest()
        //{
        //    string ArchivedFormTaskAsXml = System.IO.File.ReadAllText(@"Services\Altinntestmeldinger\AR3666383_2017-10-04 -edited.xml");
        //    ArchivedFormTaskDQBE distributionService = SerializeUtil.DeserializeFromString<ArchivedFormTaskDQBE>(ArchivedFormTaskAsXml);
        //    FormData distributionServiceFormData = new AltinnFormDeserializer().DeserializeForm(distributionService);

        //    var logService = new Mock<ILogEntryService>();
        //    var decryptionFactory = new Mock<IDecryptionFactory>();
        //    decryptionFactory.Setup(f => f.GetDecryptor()).Returns(new Mock<IDecryption>().Object);

        //    var svarUtServicePrint = new Mock<ISvarUtService>();
        //    var svarUtServiceFactory = new Mock<ISvarUtServiceFactory>();
        //    svarUtServiceFactory.Setup(s => s.GetSvarUtServiceFor(It.IsAny<SvarUtServiceTypeEnum>()))
        //        .Returns(svarUtServicePrint.Object);
        //    var svarUtStatusService = new Mock<ISvarUtStatusService>();

        //    var formMetadataService = new Mock<IFormMetadataService>(); //returner guid
        //    formMetadataService.Setup(s => s.InsertDistributionForm(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(new DistributionForm() { Id = Guid.NewGuid() });

        //    IAltinnPrefillService prefillservice = new AltinnPrefillService(new Mock<ILogger>().Object, new PrefillClientFactory(), new PrefillPolicyProvider(new PrefillPolicyConfigurationProvider()));
        //    IAltinnCorrespondenceService corrService = new AltinnCorrespondenceService(new Mock<ILogger>().Object, new CorrespondenceClientFactory(), new CorrespondencePolicyProvider(new CorrespondencePolicyConfigurationProvider()));
        //    var altinnService = new Mock<IAltinnService>();
        //    var altinnFormDesService = new Mock<IAltinnFormDeserializer>();
        //    var formShippingsService = new Mock<IFormShippingService>();
        //    var statusdownload = new Mock<IFileDownloadStatusService>();
        //    var syncService = new Mock<ISyncRecordsOfCombinedDistributions>();
        //    var appLogService = new Mock<IApplicationLogService>();
        //    var submittalQueueService = new Mock<ExportToSubmittalQueueService>();

        //    BlobStorage blobStorage = new BlobStorage();
        //    IFileStorage filestorage = new FileStorage(blobStorage);
        //    IFileStorage iFileStorage = new FileStorage(new BlobStorage());
        //    IFileDownloadStatusService fileDownloadStatusService = new FileDownloadStatusService(new ApplicationDbContext());
        //    UserNotificationHandler userNotificationHandler = new UserNotificationHandler(new UserNotificationService(new ApplicationDbContext()));

        //    SendPrefillFormServiceV2 sendPrefillFormServiceV2 = new SendPrefillFormServiceV2(corrService, prefillservice, logService.Object, formMetadataService.Object, decryptionFactory.Object, svarUtServiceFactory.Object, userNotificationHandler, syncService.Object, svarUtStatusService.Object);
        //    FormDistributionServiceV2 formDistributionService = new FormDistributionServiceV2(corrService, logService.Object, formMetadataService.Object, sendPrefillFormServiceV2, decryptionFactory.Object, iFileStorage, fileDownloadStatusService);
        //    NotificationServiceV2 notificationService = new NotificationServiceV2(corrService, logService.Object, formMetadataService.Object, filestorage, statusdownload.Object, syncService.Object, new Mock<ILogger>().Object);
        //    INotificationServiceResolver notificationServiceResolver = new NotificationServiceResolver(new List<INotificationServiceV2>() { notificationService });

        //    var sendNotification = new Mock<ISendSecondNotifications>();

        //    AltinnFormDownloadAndRoutingService routing = new AltinnFormDownloadAndRoutingService(altinnService.Object, altinnFormDesService.Object, formShippingsService.Object, logService.Object, formMetadataService.Object, formDistributionService, notificationServiceResolver, null, filestorage, sendNotification.Object, appLogService.Object, submittalQueueService.Object);

        //    const string chars = "0123456789";
        //    Random random = new Random();
        //    string archiveRefDigits = new string(Enumerable.Repeat(chars, 9).Select(s => s[random.Next(s.Length)]).ToArray());


        //    string uri = routing.LoggAltinnMessage($"ar{archiveRefDigits}", distributionService);
        //    uri.Should().NotBeNullOrEmpty();
        //}
    }
}
