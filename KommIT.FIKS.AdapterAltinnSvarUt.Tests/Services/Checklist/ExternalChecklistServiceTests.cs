﻿using KommIT.FIKS.AdapterAltinnSvarUt.Services.Checklist;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Caching;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;



namespace KommIT.FIKS.AdapterAltinnSvarUt.Tests.Services.Checklist
{
    public class ExternalChecklistServiceTests
    {
        [Fact (Skip = "Integration Test")]
        void GetActivityEnterpriseTerms_NewCodelistServer()
        {
            // Test of specific checklist problem november 2020
            string[] terms = ExternalChecklistService.GetActivityEnterpriseTerms("14.17", "RS");
        }
    }
}
