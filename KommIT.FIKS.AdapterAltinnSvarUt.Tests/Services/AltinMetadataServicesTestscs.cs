﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Metadata;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xunit;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Tests.Services
{
    public class AltinMetadataServicesTestscs
    {
        [Fact]
        public void DibkServicesMetadataTest()
        {
            var dibkServicesMetadata = new AltinnMetadataServices().GetAllDibkServicesMetadata();
            dibkServicesMetadata.Should().NotBeNull();
        }
        [Fact]
        public void GETAltinMetadataTest()
        {
            var altinnMetadata = new AltinnMetadataServices().GetMatadataForAltinnServices("4397", "2");//.GetAltinMetadata("5512", "41809")
            altinnMetadata.Should().NotBeNull();
        }
    }
}
