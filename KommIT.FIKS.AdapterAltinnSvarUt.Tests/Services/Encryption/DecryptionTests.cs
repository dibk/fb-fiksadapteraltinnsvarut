﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using Xunit;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Encryption;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Tests.Services.Encryption
{
    public class DecryptionTests
    {

        [Fact(Skip = "Integration Test")]
        public void SendAnsvarsretterOKTest()
        {
            //Decryption decryption = new Decryption();

            string encryptIn = "2706201245686";
            string encrypted = Decryption.Instance.EncryptText(encryptIn);
            string decryptedOut = Decryption.Instance.DecryptText(encrypted);

            decryptedOut.Should().Be(encryptIn);
        }


        [Fact(Skip = "Integration Test")]
        public void TestDecryption()
        {
            //Decryption decryption = new Decryption();

            string cipherText = "V5csfQd9I83SLnbD7t9Ovkd5r2q3GnTaTgjVz9ELBFQlTaSRDqZ6YL7COprFYXLgtYySp7KVo+maX6pQGog8UeSn0Bg2P3YF1ndh3FOHj3+WSnxNhGdlj+EsIdFTdHE3qCMeR25ZGQba35l9P6KDUGdXabgd0r+jQPYbNuKJPOA=";
            string decryptedOut = Decryption.Instance.DecryptText(cipherText);

            decryptedOut.Should().Be("something");
        }
    }

}
