﻿using FluentAssertions;
using KommIT.FIKS.AdapterAltinnSvarUt.Resources;
using KommIT.FIKS.AdapterAltinnSvarUt.Services;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.NotificationForm;
using no.kxml.skjema.dibk.ansvarsrettVedDistribusjon;
using Xunit;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Tests.Services.NotificationForm
{
    public class DistribusjonErklaeringAnsvarsrettV2ValidatorTest
    {
        private readonly DistribusjonErklaeringAnsvarsrettV2Validator _validator;

        public DistribusjonErklaeringAnsvarsrettV2ValidatorTest()
        {
            _validator = new DistribusjonErklaeringAnsvarsrettV2Validator();
        }

        [Fact]
        public void AltOk()
        {
            var form = CreateForm();
            ValidationResult valResult = _validator.Validate(form);
            valResult.IsOk().Should().BeTrue();
        }

        [Fact]
        public void MinstEnEiendomFeilerNaarEiendomByggestedErNull()
        {
            var form = CreateForm();
            form.eiendomByggested = null;
            Validate(form, 1, ValidationError.MinstEnEiendom);
        }

        [Fact]
        public void MinstEnEiendom()
        {
            var form = CreateForm();
            form.eiendomByggested = new EiendomType[0];
            Validate(form, 1, ValidationError.MinstEnEiendom);
        }

        [Fact]
        public void MinstEtAnsvarsomraadeFeilerNaarAnsvarsrettErNull()
        {
            var form = CreateForm();
            form.ansvarsrett = null;
            Validate(form, 1, ValidationError.MinstEtAnsvarsomraade);
        }

        [Fact]
        public void MinstEtAnsvarsomraadeFeilerNaarAnsvarsomraaderErNull()
        {
            var form = CreateForm();
            form.ansvarsrett = new AnsvarsrettType();
            Validate(form, 1, ValidationError.MinstEtAnsvarsomraade);
        }

        [Fact]
        public void MinstEtAnsvarsomraadeFeilerNaarAnsvarsomraadeListeErTom()
        {
            var form = CreateForm();
            form.ansvarsrett = new AnsvarsrettType()
            {
                ansvarsomraader = new AnsvarsomraadeType[0]
            };

            Validate(form, 1, ValidationError.MinstEtAnsvarsomraade);
        }

        private void Validate(ErklaeringAnsvarsrettType form, int antallFeil, string feilmelding)
        {
            ValidationResult valResult = _validator.Validate(form);
            valResult.Errors.Should().Be(antallFeil);
            valResult.ToString().Should().Contain(feilmelding);
        }

        private static ErklaeringAnsvarsrettType CreateForm()
        {
            ErklaeringAnsvarsrettType form = new ErklaeringAnsvarsrettType();
            form.eiendomByggested = new[]
            {
                new  EiendomType(),
            };
            form.ansvarsrett = new AnsvarsrettType()
            {
                ansvarsomraader = new[]
                {
                    new  AnsvarsomraadeType(),
                }
            };
            form.ansvarligSoeker = new PartType()
            {
                partstype = new KodeType()
                {
                    kodeverdi = "Foretak"
                },
                kontaktperson = new KontaktpersonType()
                {
                    navn = "Benjamin"
                },
            };
            return form;
        }
    }
}