﻿using System;
using FluentAssertions;
using KommIT.FIKS.AdapterAltinnSvarUt.Services;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.Builders;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Validations;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;
using System.Collections.Generic;
using System.Linq;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.NotificationForm.KontrollOgSamsvarserklaering;
using Xunit;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Tests.Services.NotificationForm
{
    public class KontrollerklaeringVedDistribusjonFormValidatorTests
    {

        //[Fact(DisplayName = "5445.1.6 AnsvarsrettValidering - OK test")]
        //public void Kontrollerklaering_5445_1_6_AnsvarsrettValideringValideringOKTest()
        //{

        //    var val = new KontrollerklaeringVedDistribusjonFormValidator();

        //    var kontrollerklaeringType = new KontrollerklaeringVedDistribusjonV1Builder()
        //        .Ansvarsrett("KONTROLL", "Ansvarlig kontroll", true, false, false, DateTime.Now)
        //        .Build();
        //    val.AnsvarsrettValidering(kontrollerklaeringType, new List<string>() { "PlanForUavhengigKontroll"});
        //    var result = val.GetResult();

        //    TestHelper.AssertResult(result, null, 0);

        //}
        //[Fact(DisplayName = "5445.1.6 AnsvarsrettValidering - Error test", Skip = "Matias fix")]
        //public void Kontrollerklaering_5445_1_6_AnsvarsrettValideringValideringErrorTest()
        //{

        //    var val = new KontrollerklaeringVedDistribusjonFormValidator();

        //    var kontrollerklaeringType = new KontrollerklaeringVedDistribusjonV1Builder()
        //        .Ansvarsrett()
        //        .Build();
        //    val.AnsvarsrettValidering(kontrollerklaeringType, new List<string>());
        //    var result = val.GetResult();

        //    TestHelper.AssertResult(result, "5445.1.6", 1);
        //}
        //[Fact(DisplayName = "5445.1.6.1 AnsvarsrettValidering - Error test", Skip = "Matias fix")]
        //public void Kontrollerklaering_5445_1_6_1_AnsvarsrettValideringValideringErrorTest()
        //{

        //    var val = new KontrollerklaeringVedDistribusjonFormValidator();

        //    var kontrollerklaeringType = new KontrollerklaeringVedDistribusjonV1Builder()
        //        .Ansvarsrett(null, "Ansvarlig kontroll", true, false, false, DateTime.Now)
        //        .Build();
        //    val.AnsvarsrettValidering(kontrollerklaeringType, new List<string>());
        //    var result = val.GetResult();

        //    TestHelper.AssertResult(result, "5445.1.6.1", 1);
        //}

        //[Fact(DisplayName = "5445.1.1.2 AnsvarsrettValidering - Error test", Skip = "Matias fix")]
        //public void Kontrollerklaering_5445_1_1_2_AnsvarsrettValideringValideringErrorTest()
        //{

        //    var val = new KontrollerklaeringVedDistribusjonFormValidator();

        //    var kontrollerklaeringType = new KontrollerklaeringVedDistribusjonV1Builder()
        //        .Ansvarsrett("", "Ansvarlig kontroll", true, false, false, DateTime.Now)
        //        .Build();
        //    val.AnsvarsrettValidering(kontrollerklaeringType, new List<string>() { "PlanForUavhengigKontroll" });
        //    var result = val.GetResult();

        //    TestHelper.AssertResult(result, "5445.1.1.2", 1, false);
        //}
        //[Fact(DisplayName = "5445.1.2.2 AnsvarsrettValidering - Error test")]
        //public void Kontrollerklaering_5445_1_2_2_AnsvarsrettValideringValideringErrorTest()
        //{

        //    var val = new KontrollerklaeringVedDistribusjonFormValidator();

        //    var kontrollerklaeringType = new KontrollerklaeringVedDistribusjonV1Builder()
        //        .Ansvarsrett("*KONTROLL", "Ansvarlig kontroll", true, false, false, DateTime.Now)
        //        .Build();
        //    val.AnsvarsrettValidering(kontrollerklaeringType, new List<string>() { ""});
        //    var result = val.GetResult();

        //    TestHelper.AssertResult(result, "5445.1.2.2", 1);
        //}
        //[Fact(DisplayName = "5445.1.6.2 AnsvarsrettValidering - Error test")]
        //public void Kontrollerklaering_5445_1_6_2_AnsvarsrettValideringValideringErrorTest()
        //{

        //    var val = new KontrollerklaeringVedDistribusjonFormValidator();

        //    var kontrollerklaeringType = new KontrollerklaeringVedDistribusjonV1Builder()
        //        .Ansvarsrett("KONTROLL", "Ansvarlig kontroll", null, null, null, DateTime.Now)
        //        .Build();
        //    val.AnsvarsrettValidering(kontrollerklaeringType, new List<string>() { ""});
        //    var result = val.GetResult();

        //    TestHelper.AssertResult(result, "5445.1.6.2", 1);
        //}
        //[Fact(DisplayName = "5445.1.6.3 AnsvarsrettValidering - Error test")]
        //public void Kontrollerklaering_5445_1_6_3_AnsvarsrettValideringValideringErrorTest()
        //{

        //    var val = new KontrollerklaeringVedDistribusjonFormValidator();

        //    var kontrollerklaeringType = new KontrollerklaeringVedDistribusjonV1Builder()
        //        .Ansvarsrett("PRO", "Ansvarlig kontroll", null, false, false, DateTime.Now)
        //        .Build();
        //    val.AnsvarsrettValidering(kontrollerklaeringType, new List<string>() { ""});
        //    var result = val.GetResult();

        //    TestHelper.AssertResult(result, "5445.1.6.3", 1);
        //}
        //[Fact(DisplayName = "5445.1.6.4 AnsvarsrettValidering - Error test")]
        //public void Kontrollerklaering_5445_1_6_4_AnsvarsrettValideringValideringErrorTest()
        //{

        //    var val = new KontrollerklaeringVedDistribusjonFormValidator();

        //    var kontrollerklaeringType = new KontrollerklaeringVedDistribusjonV1Builder()
        //        .Ansvarsrett("KONTROLL", "Ansvarlig kontroll", null, false, false, DateTime.Now)
        //        .Build();
        //    val.AnsvarsrettValidering(kontrollerklaeringType, new List<string>() { ""});
        //    var result = val.GetResult();

        //    TestHelper.AssertResult(result, "5445.1.6.4", 1);
        //}
        //[Fact(DisplayName = "5445.1.6.5 AnsvarsrettValidering - Error test")]
        //public void Kontrollerklaering_5445_1_6_5_AnsvarsrettValideringValideringErrorTest()
        //{

        //    var val = new KontrollerklaeringVedDistribusjonFormValidator();

        //    var kontrollerklaeringType = new KontrollerklaeringVedDistribusjonV1Builder()
        //        .Ansvarsrett("KONTROLL", "Ansvarlig kontroll", null, true, true, DateTime.Now)
        //        .Build();
        //    val.AnsvarsrettValidering(kontrollerklaeringType, new List<string>() { ""});
        //    var result = val.GetResult();

        //    TestHelper.AssertResult(result, "5445.1.6.5", 1);
        //}
        //[Fact(DisplayName = "5445.1.6.6 AnsvarsrettValidering - Error test")]
        //public void Kontrollerklaering_5445_1_6_6_AnsvarsrettValideringValideringErrorTest()
        //{

        //    var val = new KontrollerklaeringVedDistribusjonFormValidator();

        //    var kontrollerklaeringType = new KontrollerklaeringVedDistribusjonV1Builder()
        //        .Ansvarsrett("KONTROLL", "Ansvarlig kontroll", null, true, false, DateTime.Now)
        //        .Build();
        //    val.AnsvarsrettValidering(kontrollerklaeringType, new List<string>() { "*PlanForUavhengigKontroll"});
        //    var result = val.GetResult();

        //    TestHelper.AssertResult(result, "5445.1.6.6", 1);
        //}  
        //[Fact(DisplayName = "5445.1.6.7 AnsvarsrettValidering - Error test")]
        //public void Kontrollerklaering_5445_1_6_7_AnsvarsrettValideringValideringErrorTest()
        //{

        //    var val = new KontrollerklaeringVedDistribusjonFormValidator();

        //    var kontrollerklaeringType = new KontrollerklaeringVedDistribusjonV1Builder()
        //        .Ansvarsrett("KONTROLL", "Ansvarlig kontroll", null, true, false,null)
        //        .Build();
        //    val.AnsvarsrettValidering(kontrollerklaeringType, new List<string>() { "PlanForUavhengigKontroll" });
        //    var result = val.GetResult();

        //    TestHelper.AssertResult(result, "5445.1.6.7", 1);
        //}

        //[Fact(DisplayName = "5445.1.9. BekreftelserValidation - OK test")]
        //public void Kontrollerklaering_5445_1_9_SoekerValidationErrorTest()
        //{
        //    var val = new KontrollerklaeringVedDistribusjonFormValidator();

        //    var kontrollerklaeringType = new KontrollerklaeringVedDistribusjonV1Builder()
        //        .Bekreftelser(null, DateTime.Now)
        //        .Build();
        //    val.BekreftelserValidation(kontrollerklaeringType);
        //    var result = val.GetResult();

        //    TestHelper.AssertResult(result, "5445.1.9", 1);
        //}
    }
}
