﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using FluentAssertions;
using KommIT.FIKS.AdapterAltinnSvarUt.Services;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.NotificationForm;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.Builders;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.FormsSamples;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Validations;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;
using Moq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using no.kxml.skjema.dibk.samsvarserklaeringDirekteOpprettet;
using Xunit;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Tests.Services.NotificationForm
{
    public class SamsvarDirekteFormValidatorTests
    {
        private static readonly bool _externalDependences = Feature.IsFeatureEnabled(FeatureFlags.UnitTestExternalDependence);
        private static readonly bool _matrikkelFlag = Feature.IsFeatureEnabled(FeatureFlags.MatrikkelValidering);
        private static readonly bool _bringFlag = Feature.IsFeatureEnabled(FeatureFlags.BringPostnummerValidering);

        [Fact(DisplayName = "5315.1.4 EiendomByggestedValidering - OK test")]
        public void SamsvarDirekte_5315_1_EiendomByggestedValideringOkTest()
        {
            var val = new SamsvarDirekteFormValidator();
            var municipalityValidator = new Mock<IMunicipalityValidator>();
            municipalityValidator.Setup(s => s.ValidateKommunenummer(It.IsAny<string>())).Returns(new GeneralValidationResult() { Status = ValidationStatus.Ok, Message = "UnitTest" });
            val.MunicipalityValidator = municipalityValidator.Object;

            var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                .EiendomByggested("2004", "1", "3", "0", "0", "Hammerfest", "H0101", "192423880", "Båtsfjorden 6", "Båtsfjorden", "6", "9664", "SANDØYBOTN")
                .Build();
            val.EiendomByggestedValidering(samsvarserklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "", 1, null, true);

        }

        [Fact(DisplayName = "5315.1.4 EiendomByggestedValidering - Error test")]
        public void SamsvarDirekte_5315_1_4_EiendomByggestedValideringErrorTest()
        {

            var val = new SamsvarDirekteFormValidator();

            var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                .EiendomByggested()
                .Build();
            val.EiendomByggestedValidering(samsvarserklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5315.1.4", 1);

        }
        [Fact(DisplayName = "5315.1.4.1 EiendomByggestedValidering - Error test")]
        public void SamsvarDirekte_5315_1_4_1_EiendomByggestedValideringErrorTest()
        {

            var val = new SamsvarDirekteFormValidator();

            var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                .EiendomByggested(null, "1", "3", "0", "0", "Hammerfest", "H0101", "192423880", "Båtsfjorden 6", "Båtsfjorden", "6", "9664", "SANDØYBOTN")
                .Build();
            val.EiendomByggestedValidering(samsvarserklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5315.1.4.1", 1);
        }

        [Fact(DisplayName = "5315.1.4.2 EiendomByggestedValidering - Error test")]
        public void SamsvarDirekte_5315_1_4_2_EiendomByggestedValideringErrorTest()
        {
            var val = new SamsvarDirekteFormValidator();
            var municipalityValidator = new Mock<IMunicipalityValidator>();
            municipalityValidator.Setup(s => s.ValidateKommunenummer(It.IsAny<string>())).Returns((GeneralValidationResult)null);
            val.MunicipalityValidator = municipalityValidator.Object;

            var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                .EiendomByggested("*2004", "1", "3", "0", "0", "Hammerfest", "H0101", "192423880", "Båtsfjorden 6", "Båtsfjorden", "6", "9664", "SANDØYBOTN")
                .Build();
            val.EiendomByggestedValidering(samsvarserklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5315.1.4.2", 1);

        }
        [Fact(DisplayName = "5315.1.4.3 EiendomByggestedValidering - Error test", Skip = "mangler info om en kommune som har ugyldig kode")]
        public void SamsvarDirekte_5315_1_4_3_EiendomByggestedValideringErrorTest()
        {
            var val = new SamsvarDirekteFormValidator();

            var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                .EiendomByggested("????", "1", "3", "0", "0", "Hammerfest", "H0101", "192423880", "Båtsfjorden 6", "Båtsfjorden", "6", "9664", "SANDØYBOTN")
                .Build();
            val.EiendomByggestedValidering(samsvarserklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5315.1.4.3", 1);

        }
        [Fact(DisplayName = "5315.1.4.4 EiendomByggestedValidering - Error test")]
        public void SamsvarDirekte_5315_1_4_4_EiendomByggestedValideringErrorTest()
        {
            var val = new SamsvarDirekteFormValidator();
            var municipalityValidator = new Mock<IMunicipalityValidator>();
            municipalityValidator.Setup(s => s.ValidateKommunenummer(It.IsAny<string>())).Returns(new GeneralValidationResult() { Status = ValidationStatus.Ok, Message = "UnitTest" });
            val.MunicipalityValidator = municipalityValidator.Object;

            var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                .EiendomByggested("2004", "1", null, "0", "0", "Hammerfest", "H0101", "192423880", "Båtsfjorden 6", "Båtsfjorden", "6", "9664", "SANDØYBOTN")
                .Build();
            val.EiendomByggestedValidering(samsvarserklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5315.1.4.4", 1);
        }
        [Fact(DisplayName = "5315.1.4.5 EiendomByggestedValidering - Error test", Skip = "gnr and bnr are Integer in the xsd")]
        public void SamsvarDirekte_5315_1_4_5_EiendomByggestedValideringErrorTest()
        {
            var val = new SamsvarDirekteFormValidator();
            var municipalityValidator = new Mock<IMunicipalityValidator>();
            municipalityValidator.Setup(s => s.ValidateKommunenummer(It.IsAny<string>())).Returns(new GeneralValidationResult() { Status = ValidationStatus.Ok, Message = "UnitTest" });
            val.MunicipalityValidator = municipalityValidator.Object;

            var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                .EiendomByggested("2004", "*1", "3", "0", "0", "Hammerfest", "H0101", "192423880", "Båtsfjorden 6", "Båtsfjorden", "6", "9664", "SANDØYBOTN")
                .Build();
            val.EiendomByggestedValidering(samsvarserklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5315.1.4.5", 1);
        }
        [Fact(DisplayName = "5315.1.4.6 EiendomByggestedValidering - Error test")]
        public void SamsvarDirekte_5315_1_4_6_EiendomByggestedValideringErrorTest()
        {
            var val = new SamsvarDirekteFormValidator();
            var municipalityValidator = new Mock<IMunicipalityValidator>();
            municipalityValidator.Setup(s => s.ValidateKommunenummer(It.IsAny<string>())).Returns(new GeneralValidationResult() { Status = ValidationStatus.Ok, Message = "UnitTest" });
            val.MunicipalityValidator = municipalityValidator.Object;

            var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                .EiendomByggested("2004", "-1", "0", "0", "0", "Hammerfest", "H0101", "192423880", "Båtsfjorden 6", "Båtsfjorden", "6", "9664", "SANDØYBOTN")
                .Build();
            val.EiendomByggestedValidering(samsvarserklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5315.1.4.6", 1);
        }
        [Fact(DisplayName = "5315.1.4.6.1 EiendomByggestedValidering - Error test")]
        public void SamsvarDirekte_5315_1_4_6_1_EiendomByggestedValideringErrorTest()
        {
            var val = new SamsvarDirekteFormValidator();
            var municipalityValidator = new Mock<IMunicipalityValidator>();
            municipalityValidator.Setup(s => s.ValidateKommunenummer(It.IsAny<string>())).Returns(new GeneralValidationResult() { Status = ValidationStatus.Ok, Message = "UnitTest" });
            val.MunicipalityValidator = municipalityValidator.Object;

            var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                .EiendomByggested("2004", "0", "-1", "0", "0", "Hammerfest", "H0101", "192423880", "Båtsfjorden 6", "Båtsfjorden", "6", "9664", "SANDØYBOTN")
                .Build();
            val.EiendomByggestedValidering(samsvarserklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5315.1.4.6.1", 1);
        }
        [Fact(DisplayName = "5315.1.4.6b EiendomByggestedValidering - Error test")]
        public void SamsvarDirekte_5315_1_4_6b_EiendomByggestedValideringErrorTest()
        {
            var val = new SamsvarDirekteFormValidator();
            var municipalityValidator = new Mock<IMunicipalityValidator>();
            municipalityValidator.Setup(s => s.ValidateKommunenummer(It.IsAny<string>())).Returns(new GeneralValidationResult() { Status = ValidationStatus.Ok, Message = "UnitTest" });
            val.MunicipalityValidator = municipalityValidator.Object;

            var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                .EiendomByggested("2004", "-1", "3", "0", "0", "Hammerfest", "H0101", "192423880", "Båtsfjorden 6", "Båtsfjorden", "6", "9664", "SANDØYBOTN")
                .Build();
            val.EiendomByggestedValidering(samsvarserklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5315.1.4.6", 1);
        }
        [Fact(DisplayName = "5315.1.4.7 EiendomByggestedValidering - Error test")]
        public void SamsvarDirekte_5315_1_4_7_EiendomByggestedValideringErrorTest()
        {
            var val = new SamsvarDirekteFormValidator();
            var municipalityValidator = new Mock<IMunicipalityValidator>();
            municipalityValidator.Setup(s => s.ValidateKommunenummer(It.IsAny<string>())).Returns(new GeneralValidationResult() { Status = ValidationStatus.Ok, Message = "UnitTest" });
            val.MunicipalityValidator = municipalityValidator.Object;

            var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                .EiendomByggested("2004", "1", "3", "0", "0", "Hammerfest", "H0101", "*192423880", "Båtsfjorden 6", "Båtsfjorden", "6", "9664", "SANDØYBOTN")
                .Build();
            val.EiendomByggestedValidering(samsvarserklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5315.1.4.7", 1, null, true);
        }
        [Fact(DisplayName = "5315.1.4.8 EiendomByggestedValidering - Error test")]
        public void SamsvarDirekte_5315_1_4_8_EiendomByggestedValideringErrorTest()
        {
            var val = new SamsvarDirekteFormValidator();
            var municipalityValidator = new Mock<IMunicipalityValidator>();
            municipalityValidator.Setup(s => s.ValidateKommunenummer(It.IsAny<string>())).Returns(new GeneralValidationResult() { Status = ValidationStatus.Ok, Message = "UnitTest" });
            val.MunicipalityValidator = municipalityValidator.Object;

            var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                .EiendomByggested("2004", "1", "3", "0", "0", "Hammerfest", "H0101", "0", "Båtsfjorden 6", "Båtsfjorden", "6", "9664", "SANDØYBOTN")
                .Build();
            val.EiendomByggestedValidering(samsvarserklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5315.1.4.8", 1, null, true);
        }
        [Fact(DisplayName = "5315.1.4.9 EiendomByggestedValidering - Error test")]
        public void SamsvarDirekte_5315_1_4_9_EiendomByggestedValideringErrorTest()
        {
            var val = new SamsvarDirekteFormValidator();
            var municipalityValidator = new Mock<IMunicipalityValidator>();
            municipalityValidator.Setup(s => s.ValidateKommunenummer(It.IsAny<string>())).Returns(new GeneralValidationResult() { Status = ValidationStatus.Ok, Message = "UnitTest" });
            val.MunicipalityValidator = municipalityValidator.Object;

            var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                .EiendomByggested("2004", "1", "3", "0", "0", "Hammerfest", "H0101", "192423880")
                .Build();
            val.EiendomByggestedValidering(samsvarserklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5315.1.4.9", 1, null, true);
        }
        [Fact(DisplayName = "5315.1.4.9.1 EiendomByggestedValidering - Error test")]
        public void SamsvarDirekte_5315_1_4_9_1_EiendomByggestedValideringErrorTest()
        {
            var val = new SamsvarDirekteFormValidator();
            var municipalityValidator = new Mock<IMunicipalityValidator>();
            municipalityValidator.Setup(s => s.ValidateKommunenummer(It.IsAny<string>())).Returns(new GeneralValidationResult() { Status = ValidationStatus.Ok, Message = "UnitTest" });
            val.MunicipalityValidator = municipalityValidator.Object;

            var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                .EiendomByggested("2004", "1", "3", "0", "0", "Hammerfest", "H0101", "192423880", null, "Båtsfjorden", "6", "9664", "SANDØYBOTN")
                .Build();
            val.EiendomByggestedValidering(samsvarserklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5315.1.4.9.1", 1, null, true);
        }
        [Fact(DisplayName = "5315.1.4.9.2 EiendomByggestedValidering - Error test")]
        public void SamsvarDirekte_5315_1_4_9_2_EiendomByggestedValideringErrorTest()
        {
            var val = new SamsvarDirekteFormValidator();
            var municipalityValidator = new Mock<IMunicipalityValidator>();
            municipalityValidator.Setup(s => s.ValidateKommunenummer(It.IsAny<string>())).Returns(new GeneralValidationResult() { Status = ValidationStatus.Ok, Message = "UnitTest" });
            val.MunicipalityValidator = municipalityValidator.Object;

            var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                .EiendomByggested("2004", "1", "3", "0", "0", "Hammerfest", "H0101", "192423880", "Båtsfjorden 6", "Båtsfjorden", null, "9664", "SANDØYBOTN")
                .Build();
            val.EiendomByggestedValidering(samsvarserklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5315.1.4.9.2", 1, null, true);
        }
        [Fact(DisplayName = "5315.1.4.9.2b EiendomByggestedValidering - Error test")]
        public void SamsvarDirekte_5315_1_4_9_2b_EiendomByggestedValideringErrorTest()
        {
            var val = new SamsvarDirekteFormValidator();
            var municipalityValidator = new Mock<IMunicipalityValidator>();
            municipalityValidator.Setup(s => s.ValidateKommunenummer(It.IsAny<string>())).Returns(new GeneralValidationResult() { Status = ValidationStatus.Ok, Message = "UnitTest" });
            val.MunicipalityValidator = municipalityValidator.Object;

            var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                .EiendomByggested("2004", "1", "3", "0", "0", "Hammerfest", "H0101", "192423880", "Båtsfjorden 6", null, "6", "9664", "SANDØYBOTN")

                .Build();
            val.EiendomByggestedValidering(samsvarserklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5315.1.4.9.2", 1, null, true);
        }
        [Fact(DisplayName = "5315.1.4.10 EiendomByggestedValidering - Error test")]
        public void SamsvarDirekte_5315_1_4_10_EiendomByggestedValideringErrorTest()
        {
            var val = new SamsvarDirekteFormValidator();
            var municipalityValidator = new Mock<IMunicipalityValidator>();
            municipalityValidator.Setup(s => s.ValidateKommunenummer(It.IsAny<string>())).Returns(new GeneralValidationResult() { Status = ValidationStatus.Ok, Message = "UnitTest" });
            val.MunicipalityValidator = municipalityValidator.Object;

            var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                .EiendomByggested("2004", "1", "3", "0", "0", null, "H0101", "192423880", "Båtsfjorden 6", "Båtsfjorden", "6", "9664", "SANDØYBOTN")
                .Build();
            val.EiendomByggestedValidering(samsvarserklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5315.1.4.10", 1, null, true);
        }

        [Fact(DisplayName = "5315.1.4.11 EiendomByggestedValidering - Error test")]
        public void SamsvarDirekte_5315_1_4_11_EiendomByggestedValideringErrorTest()
        {
            var val = new SamsvarDirekteFormValidator();
            var municipalityValidator = new Mock<IMunicipalityValidator>();
            municipalityValidator.Setup(s => s.ValidateKommunenummer(It.IsAny<string>())).Returns(new GeneralValidationResult() { Status = ValidationStatus.Ok, Message = "UnitTest" });
            val.MunicipalityValidator = municipalityValidator.Object;

            var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                .EiendomByggested("2004", "1", "3", "0", "0", "Hammerfest", "*H0101", "192423880", "Båtsfjorden 6", "Båtsfjorden", "6", "9664", "SANDØYBOTN")
                .Build();
            val.EiendomByggestedValidering(samsvarserklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5315.1.4.11", 1, null, true);
        }
        [Fact(DisplayName = "5315.1.4.12 EiendomByggestedValidering - Error test", Skip = "Matrikkel validering")]
        public void SamsvarDirekte_5315_1_4_12_EiendomByggestedValideringErrorTest()
        {
            var val = new SamsvarDirekteFormValidator();

            var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                .EiendomByggested("2004", "1", "3", "0", "0", "Hammerfest", "H0101", "192423880", "Båtsfjorden 6", "Båtsfjorden", "6", "9664", "SANDØYBOTN")
                .Build();
            val.EiendomByggestedValidering(samsvarserklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5315.1.4.12", 1);
        }
        [Fact(DisplayName = "5315.1.4.13 EiendomByggestedValidering - Error test", Skip = "Matrikkel validering")]
        public void SamsvarDirekte_5315_1_4_13_EiendomByggestedValideringErrorTest()
        {
            var val = new SamsvarDirekteFormValidator();

            var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                .EiendomByggested("2004", "1", "3", "0", "0", "Hammerfest", "H0101", "192423880", "Båtsfjorden 6", "Båtsfjorden", "6", "9664", "SANDØYBOTN")
                .Build();
            val.EiendomByggestedValidering(samsvarserklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5315.1.4.13", 1);
        }
        [Fact(DisplayName = "5315.1.4.14 EiendomByggestedValidering - Error test", Skip = "Matrikkel validering")]
        public void SamsvarDirekte_5315_1_4_14_EiendomByggestedValideringErrorTest()
        {
            var val = new SamsvarDirekteFormValidator();

            var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                .EiendomByggested("2004", "1", "3", "0", "0", "Hammerfest", "H0101", "666423880", "Båtsfjorden 6", "Båtsfjorden", "6", "9664", "SANDØYBOTN")
                .Build();
            val.EiendomByggestedValidering(samsvarserklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5315.1.4.14", 1);
        }
        [Fact(DisplayName = "5315.1.4.15 EiendomByggestedValidering - Error test", Skip = "Matrikkel validering")]
        public void SamsvarDirekte_5315_1_4_15_EiendomByggestedValideringErrorTest()
        {
            var val = new SamsvarDirekteFormValidator();

            var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                .EiendomByggested("2004", "1", "3", "0", "0", "Hammerfest", "H0101", "192423880", "Båtsfjorden 6", "*Båtsfjorden", "6", "9664", "SANDØYBOTN")
                .Build();
            val.EiendomByggestedValidering(samsvarserklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5315.1.4.11", 1);
        }



        [Fact(DisplayName = "5315.1.6 AnsvarsrettValidering PRO- OK test")]
        public void SamsvarDirekte_5315_1_6_AnsvarsrettValideringValideringPROOKTest()
        {

            var val = new SamsvarDirekteFormValidator();

            var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                .Ansvarsrett("PRO", "Ansvarlig prosjektering", null, null, true, null, null
                    , null, null, null, null, null, null, "Installasjon branninstallasjoner")

                .Ansvarsrett("PRO", "Ansvarlig prosjektering",
                    null, null, true, null,
                    null, null, null,
                    null, null, null, null,
                    "Installasjon branninstallasjoner")

                .Bekreftelser(true, null)
                .Build();
            val.AnsvarsrettValidering(samsvarserklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, null, 0);

        }
        [Fact(DisplayName = "5315.1.6 AnsvarsrettValidering UTF - OK test")]
        public void SamsvarDirekte_5315_1_6_AnsvarsrettValideringValideringUTFOKTest()
        {

            var val = new SamsvarDirekteFormValidator();

            var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                .Ansvarsrett("UTF", "Ansvarlig utførelse",
                    false, null, null, true,
                    false, true, false,
                    "Forskaling av mur.", "Trapp konstruksjon øst",
                    "Mangler rekkverk til trapp", DateTime.Now.AddDays(13),
                    "Installasjon branninstallasjoner")

                .Bekreftelser(null, true)
                .Build();
            val.AnsvarsrettValidering(samsvarserklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, null, 0);
        }
        [Fact(DisplayName = "5315.1.6 AnsvarsrettValidering - Error test")]
        public void SamsvarDirekte_5315_1_6_AnsvarsrettValideringValideringErrorTest()
        {

            var val = new SamsvarDirekteFormValidator();

            var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                .Ansvarsrett()
                .Build();
            val.AnsvarsrettValidering(samsvarserklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5315.1.6", 1);
        }
        [Fact(DisplayName = "5315.1.6.1 AnsvarsrettValidering - Error test")]
        public void SamsvarDirekte_5315_1_6_1_AnsvarsrettValideringValideringErrorTest()
        {

            var val = new SamsvarDirekteFormValidator();

            var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                .Ansvarsrett(null, null, true, false, false)
                .Build();
            val.AnsvarsrettValidering(samsvarserklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5315.1.6.1", 1);
        }
        [Fact(DisplayName = "5315.1.6.2 AnsvarsrettValidering - Error test")]
        public void SamsvarDirekte_5315_1_6_2_AnsvarsrettValideringValideringErrorTest()
        {

            var val = new SamsvarDirekteFormValidator();

            var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                .Ansvarsrett(
                    "SØK", "Ansvarlig søker",
                    true, null, null, null,
                    false, true, false,
                    "Trapp konstruksjon øst", "Forskaling av mur.", "Mangler rekkverk til trapp", DateTime.Now.AddDays(13),
                    "Installasjon branninstallasjoner")

                .Build();
            val.AnsvarsrettValidering(samsvarserklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5315.1.6.2", 1);
        }

        [Fact(DisplayName = "5315.1.1.2 AnsvarsrettValidering - Error test")]
        public void SamsvarDirekte_5315_1_1_2_AnsvarsrettValideringValideringErrorTest()
        {

            var val = new SamsvarDirekteFormValidator();

            var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                .Ansvarsrett("", "Ansvarlig prosjektering", true, false, false)
                .Build();
            val.AnsvarsrettValidering(samsvarserklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5315.1.1.2", 1, false);
        }
        [Fact(DisplayName = "5315.1.2.2 AnsvarsrettValidering - Error test")]
        public void SamsvarDirekte_5315_1_2_2_AnsvarsrettValideringValideringErrorTest()
        {

            var val = new SamsvarDirekteFormValidator();

            var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                .Ansvarsrett("*PRO", "Ansvarlig prosjektering", null, null, null, null, null, null, null, null,
                    null, null, null, "Installasjon branninstallasjoner")
                .Build();
            val.AnsvarsrettValidering(samsvarserklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5315.1.2.2", 1);
        }
        [Fact(DisplayName = "5315.1.6.6 AnsvarsrettValidering UTF - Error test")]
        public void SamsvarDirekte_5315_1_6_6_AnsvarsrettValideringValideringErrorTest()
        {

            var val = new SamsvarDirekteFormValidator();

            var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                .Ansvarsrett("UTF", "Ansvarlig utførelse", null, null, null,
                    null, null, null, null, null, null, null, null, "Installasjon branninstallasjoner")
                .Bekreftelser(null, true)
                .Build();
            val.AnsvarsrettValidering(samsvarserklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5315.1.6.6", 1);
        }
        [Fact(DisplayName = "5315.1.6.7 AnsvarsrettValidering - Error test")]
        public void SamsvarDirekte_5315_1_6_7_AnsvarsrettValideringValideringErrorTest()
        {

            var val = new SamsvarDirekteFormValidator();

            var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                .Ansvarsrett("UTF", "Ansvarlig utførelse", true, null, null, null, true, true, null, null, null, null, null, "Installasjon branninstallasjoner")
                .Bekreftelser(null, true)
                .Build();
            val.AnsvarsrettValidering(samsvarserklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5315.1.6.7", 1);
        }
        [Fact(DisplayName = "5315.1.6.8 AnsvarsrettValidering - Error test")]
        public void SamsvarDirekte_5315_1_6_8_AnsvarsrettValideringValideringErrorTest()
        {

            var val = new SamsvarDirekteFormValidator();

            var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                .Ansvarsrett("UTF", "Ansvarlig utførelse", true, null, null, null, false, true, null, null, null, null, null, "Installasjon branninstallasjoner")
                .Bekreftelser(null, true)
                .Build();
            val.AnsvarsrettValidering(samsvarserklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5315.1.6.8", 1);
        }
        [Fact(DisplayName = "5315.1.6.9 AnsvarsrettValidering - Error test")]
        public void SamsvarDirekte_5315_1_6_9_AnsvarsrettValideringValideringErrorTest()
        {

            var val = new SamsvarDirekteFormValidator();

            var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                .Ansvarsrett("UTF", "Ansvarlig utførelse",
                    true, null, null, null,
                    false, true, true,
                    null, null, "Mangler rekkverk til trapp", DateTime.Now.AddDays(13),
                    "Installasjon branninstallasjoner")
                .Bekreftelser(null, true)
                .Build();
            val.AnsvarsrettValidering(samsvarserklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5315.1.6.9", 1);
        }
        [Fact(DisplayName = "5315.1.6.10 AnsvarsrettValidering - Error test")]
        public void SamsvarDirekte_5315_1_6_10_AnsvarsrettValideringValideringErrorTest()
        {

            var val = new SamsvarDirekteFormValidator();

            var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                .Ansvarsrett("UTF", "Ansvarlig utførelse",
                    true, null, null, null,
                    false, true, true,
                    "Forskaling av mur.", null, "Mangler rekkverk til trapp", DateTime.Now.AddDays(13), "Installasjon branninstallasjoner")
                .Bekreftelser(null, true)
                .Build();
            val.AnsvarsrettValidering(samsvarserklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5315.1.6.10", 1);
        }
        [Fact(DisplayName = "5315.1.6.12 AnsvarsrettValidering - Error test")]
        public void SamsvarDirekte_5315_1_6_12_AnsvarsrettValideringValideringErrorTest()
        {

            var val = new SamsvarDirekteFormValidator();

            var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                .Ansvarsrett("UTF", "Ansvarlig utførelse",
                    true, null, null, null,
                    false, true, false, "Trapp konstruksjon øst",
                    "Forskaling av mur.", null, DateTime.Now.AddDays(13), "Installasjon branninstallasjoner")
                .Bekreftelser(null, true)
                .Build();
            val.AnsvarsrettValidering(samsvarserklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5315.1.6.12", 1);
        }
        [Fact(DisplayName = "5315.1.6.13 AnsvarsrettValidering - Error test")]
        public void SamsvarDirekte_5315_1_6_13_AnsvarsrettValideringValideringErrorTest()
        {

            var val = new SamsvarDirekteFormValidator();

            var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                .Ansvarsrett("UTF", "Ansvarlig utførelse",
                    true, null, null, null,
                    false, true, false, "Trapp konstruksjon øst",
                    "Forskaling av mur.", "Mangler rekkverk til trapp", null, "Installasjon branninstallasjoner")
                .Bekreftelser(null, true)
                .Build();
            val.AnsvarsrettValidering(samsvarserklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5315.1.6.13", 1);
        }
        [Fact(DisplayName = "5315.1.6.14 AnsvarsrettValidering - Error test")]
        public void SamsvarDirekte_5315_1_6_14_AnsvarsrettValideringValideringErrorTest()
        {

            var val = new SamsvarDirekteFormValidator();

            var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                .Ansvarsrett("UTF", "Ansvarlig utførelse",
                    true, null, null, null,
                    false, true, false, "Trapp konstruksjon øst",
                    "Forskaling av mur.", "Mangler rekkverk til trapp", DateTime.Now.AddDays(15), "Installasjon branninstallasjoner")
                .Bekreftelser(null, true)
                .Build();
            val.AnsvarsrettValidering(samsvarserklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5315.1.6.14", 1);
        } 
        [Fact(DisplayName = "5315.1.6.16 AnsvarsrettValidering - Error test")]
        public void SamsvarDirekte_5315_1_6_16_AnsvarsrettValideringValideringErrorTest()
        {

            var val = new SamsvarDirekteFormValidator();

            var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                    .Ansvarsrett("PRO", "Ansvarlig prosjektering", null, null, true, null, null
                        , null, null, null, null, null, null, "Installasjon branninstallasjoner")

                    .Ansvarsrett("PRO", "Ansvarlig prosjektering",
                        null, null, true, null,
                        null, null, null,
                        null, null, null, null,
                        "Installasjon branninstallasjoner")

                    .Bekreftelser(false, false)
                    .Build();
            val.AnsvarsrettValidering(samsvarserklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5315.1.6.16", 1);
        }

        [Fact(DisplayName = "5315.1.10 AnsvarsrettValidering - Error test")]
        public void SamsvarDirekte_5315_1_10_AnsvarsrettValideringValideringErrorTest()
        {

            var val = new SamsvarDirekteFormValidator();

            var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                .Ansvarsrett("UTF", "Ansvarlig utførelse",
                    true, null, null, null,
                    false, true, false, "Trapp konstruksjon øst",
                    "Forskaling av mur.", "Mangler rekkverk til trapp", DateTime.Now.AddDays(13), "Installasjon branninstallasjoner")
                .Bekreftelser(null, null)
                .Build();
            val.AnsvarsrettValidering(samsvarserklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5315.1.10", 1);
        }

        [Fact(DisplayName = "5315.1.11 AnsvarligSoeker - Error test")]
        public void SamsvarDirekte_5315_1_11_FraSluttbrukersystemErrorTest()
        {
            var val = new SamsvarDirekteFormValidator();

            var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                .FraSluttbrukersystem()
                .Build();
            val.FraSluttbrukersystem(samsvarserklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5315.1.3", 1);
        }

        [Fact(DisplayName = "5315.1.8 SoekerValidation - OK test")]
        public void SamsvarDirekte_5315_1_8_SoekerValideringOKTest()
        {
            var val = new SamsvarDirekteFormValidator();

            var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                .AnsvarligSoeker("974760673", "Foretak", "Foretak", "Benjamin Fjell", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "no", "Trondheim", "11223344", "99887766", "epost@test.no")
                .Build();
            val.SoekerValidation(samsvarserklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, null, 0, true);
        }
        [Fact(DisplayName = "5315.1.8 SoekerValidation - ERROR test")]
        public void SamsvarDirekte_5315_1_8_SoekerValideringERRORTest()
        {
            var val = new SamsvarDirekteFormValidator();

            var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                .Build();
            val.SoekerValidation(samsvarserklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5315.1.8", 1);
        }
        [Fact(DisplayName = "5315.1.8.1 SoekerValidation - ERROR test")]
        public void SamsvarDirekte_5315_1_8_1_SoekerValideringERRORTest()
        {
            var val = new SamsvarDirekteFormValidator();

            var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                .AnsvarligSoeker("974760673", null, null, "Benjamin Fjell", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "11223344", "NO")
                .Build();
            val.SoekerValidation(samsvarserklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5315.1.8.1", 1);
        }
        [Fact(DisplayName = "5315.1.8.1b SoekerValidation - ERROR test")]
        public void SamsvarDirekte_5315_1_8_1b_SoekerValideringERRORTest()
        {
            var val = new SamsvarDirekteFormValidator();

            var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                .AnsvarligSoeker("974760673", null, "Foretak", "Benjamin Fjell", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "11223344", "NO")
                .Build();
            val.SoekerValidation(samsvarserklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5315.1.8.1", 1);
        }
        [Fact(DisplayName = "5315.1.8.2 SoekerValidation - ERROR test")]
        public void SamsvarDirekte_5315_1_8_2_SoekerValideringERRORTest()
        {
            var val = new SamsvarDirekteFormValidator();

            var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                .AnsvarligSoeker("974760673", "*Foretak", "Foretak", "Benjamin Fjell", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "11223344", "NO")
                .Build();
            val.SoekerValidation(samsvarserklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5315.1.8.2", 1);
        }
        [Fact(DisplayName = "5315.1.8.3 SoekerValidation - ERROR test")]
        public void SamsvarDirekte_5315_1_8_3_SoekerValideringERRORTest()
        {
            var val = new SamsvarDirekteFormValidator();

            var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                .AnsvarligSoeker(null, "Privatperson", "Privatperson")
                .Build();
            val.SoekerValidation(samsvarserklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5315.1.8.3", 1);
        }
        [Fact(DisplayName = "5315.1.8.4 SoekerValidation - ERROR test")]
        public void SamsvarDirekte_5315_1_8_4_SoekerValideringERRORTest()
        {
            var val = new SamsvarDirekteFormValidator();

            var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                .AnsvarligSoeker(null, "Foretak", "Foretak", "Benjamin Fjell", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "no", "Trondheim", "11223344", "99887766", "epost@test.no")
                .Build();
            val.SoekerValidation(samsvarserklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5315.1.8.4", 1, true);
        }
        [Fact(DisplayName = "5315.1.8.5 SoekerValidation - ERROR test")]
        public void SamsvarDirekte_5315_1_8_5_SoekerValideringERRORTest()
        {
            var val = new SamsvarDirekteFormValidator();

            var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                .AnsvarligSoeker("974760000", "Foretak", "Foretak", "Benjamin Fjell", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "no", "Trondheim", "11223344", "99887766", "epost@test.no")
                .Build();
            val.SoekerValidation(samsvarserklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5315.1.8.5", 1, true);
        }
        [Fact(DisplayName = "5315.1.8.6 SoekerValidation - ERROR test")]
        public void SamsvarDirekte_5315_1_8_8_SoekerValideringERRORTest()
        {
            var val = new SamsvarDirekteFormValidator();

            var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                .AnsvarligSoeker("6974760673", "Foretak", "Foretak", "Benjamin Fjell", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "no", "Trondheim", "11223344", "99887766", "epost@test.no")
                .Build();
            val.SoekerValidation(samsvarserklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5315.1.8.6", 1, true);
        }
        [Fact(DisplayName = "5315.1.8.7 SoekerValidation - ERROR test")]
        public void SamsvarDirekte_5315_1_8_7_SoekerValideringERRORTest()
        {
            var val = new SamsvarDirekteFormValidator();

            var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                .AnsvarligSoeker("974760673", "Foretak", "Foretak", "Benjamin Fjell", "Siv. Ark. Fjell og Sønner", null, null, null, null, "11223344", "99887766", "epost@test.no")
                .Build();
            val.SoekerValidation(samsvarserklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5315.1.8.7", 1);
        }
        [Fact(DisplayName = "5315.1.8.8 SoekerValidation - ERROR test")]
        public void SamsvarDirekte_5315_1_8_81_SoekerValideringERRORTest()
        {
            var val = new SamsvarDirekteFormValidator();

            var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                .AnsvarligSoeker("974760673", "Foretak", "Foretak", "Benjamin Fjell", "Siv. Ark. Fjell og Sønner", null, "7003", "no", "Trondheim", "11223344", "99887766", "epost@test.no")
                .Build();
            val.SoekerValidation(samsvarserklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5315.1.8.8", 1);
        }
        [Fact(DisplayName = "5315.1.8.10 SoekerValidation - ERROR test")]
        public void SamsvarDirekte_5315_1_8_9_SoekerValideringERRORTest()
        {
            var val = new SamsvarDirekteFormValidator();

            var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                .AnsvarligSoeker("974760673", "Foretak", "Foretak", "Benjamin Fjell", "Siv. Ark. Fjell og Sønner", "Lillegata 5", null, "no", "Trondheim", "11223344", "99887766", "epost@test.no")
                .Build();
            val.SoekerValidation(samsvarserklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5315.1.8.10", 1);
        }
        [Fact(DisplayName = "5315.1.8.11 SoekerValidation - ERROR test")]
        public void SamsvarDirekte_5315_1_8_10_SoekerValideringERRORTest()
        {
            var val = new SamsvarDirekteFormValidator();

            var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                .AnsvarligSoeker("974760673", "Foretak", "Foretak", "Benjamin Fjell", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "T003", "no", "Trondheim", "11223344", "99887766", "epost@test.no")
                .Build();
            val.SoekerValidation(samsvarserklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5315.1.8.11", 1);
        }
        [Fact(DisplayName = "5315.1.8.12 SoekerValidation - ERROR test")]
        public void SamsvarDirekte_5315_1_8_11_SoekerValideringERRORTest()
        {
            var val = new SamsvarDirekteFormValidator();

            var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                .AnsvarligSoeker("974760673", "Foretak", "Foretak", "Benjamin Fjell", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "0000", "no", "Trondheim", "11223344", "99887766", "epost@test.no")
                .Build();
            val.SoekerValidation(samsvarserklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5315.1.8.12", 1, true);
        }
        [Fact(DisplayName = "5315.1.8.13 SoekerValidation - ERROR test")]
        public void SamsvarDirekte_5315_1_8_12_SoekerValideringERRORTest()
        {
            var val = new SamsvarDirekteFormValidator();

            var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                .AnsvarligSoeker("974760673", "Foretak", "Foretak", "Benjamin Fjell", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "no", "BØ", "11223344", "99887766", "epost@test.no")
                .Build();
            val.SoekerValidation(samsvarserklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5315.1.8.13", 1, true);
        }
        //TODO
        [Fact(DisplayName = "5315.1.7 ForetakValidation - OK test")]
        public void SamsvarDirekte_5315_1_7_ForetakValideringOKTest()
        {
            var val = new SamsvarDirekteFormValidator();

            var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                .Foretak("974760673", "Foretak", "Foretak", "Benjamin Fjell", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "no", "Trondheim", "11223344", "99887766", "epost@test.no")
                .Build();
            val.ForetakValidation(samsvarserklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, null, 0, true);
        }
        [Fact(DisplayName = "5315.1.7 ForetakValidation - ERROR test")]
        public void SamsvarDirekte_5315_1_7_ForetakValideringERRORTest()
        {
            var val = new SamsvarDirekteFormValidator();

            var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                .Build();
            val.ForetakValidation(samsvarserklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5315.1.7", 1);
        }
        [Fact(DisplayName = "5315.1.7.1 ForetakValidation - ERROR test")]
        public void SamsvarDirekte_5315_1_7_1_ForetakValideringERRORTest()
        {
            var val = new SamsvarDirekteFormValidator();

            var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                .Foretak("974760673", null, null, "Benjamin Fjell", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "11223344", "NO")
                .Build();
            val.ForetakValidation(samsvarserklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5315.1.7.1", 1);
        }
        [Fact(DisplayName = "5315.1.7.1b ForetakValidation - ERROR test")]
        public void SamsvarDirekte_5315_1_7_1b_ForetakValideringERRORTest()
        {
            var val = new SamsvarDirekteFormValidator();

            var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                .Foretak("974760673", null, "Foretak", "Benjamin Fjell", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "11223344", "NO")
                .Build();
            val.ForetakValidation(samsvarserklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5315.1.7.1", 1);
        }
        [Fact(DisplayName = "5315.1.7.2 ForetakValidation - ERROR test")]
        public void SamsvarDirekte_5315_1_7_2_ForetakValideringERRORTest()
        {
            var val = new SamsvarDirekteFormValidator();

            var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                .Foretak("974760673", "*Foretak", "Foretak", "Benjamin Fjell", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "11223344", "NO")
                .Build();
            val.ForetakValidation(samsvarserklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5315.1.7.2", 1);
        }
        [Fact(DisplayName = "5315.1.7.3 ForetakValidation - ERROR test")]
        public void SamsvarDirekte_5315_1_7_3_ForetakValideringERRORTest()
        {
            var val = new SamsvarDirekteFormValidator();

            var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                .Foretak(null, "Privatperson", "Privatperson")
                .Build();
            val.ForetakValidation(samsvarserklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5315.1.7.3", 1);
        }
        [Fact(DisplayName = "5315.1.7.4 ForetakValidation - ERROR test")]
        public void SamsvarDirekte_5315_1_7_4_ForetakValideringERRORTest()
        {
            var val = new SamsvarDirekteFormValidator();

            var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                .Foretak(null, "Foretak", "Foretak", "Benjamin Fjell", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "no", "Trondheim", "11223344", "99887766", "epost@test.no")
                .Build();
            val.ForetakValidation(samsvarserklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5315.1.7.4", 1, true);
        }
        [Fact(DisplayName = "5315.1.7.5 ForetakValidation - ERROR test")]
        public void SamsvarDirekte_5315_1_7_5_ForetakValideringERRORTest()
        {
            var val = new SamsvarDirekteFormValidator();

            var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                .Foretak("974760000", "Foretak", "Foretak", "Benjamin Fjell", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "no", "Trondheim", "11223344", "99887766", "epost@test.no")
                .Build();
            val.ForetakValidation(samsvarserklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5315.1.7.5", 1, true);
        }
        [Fact(DisplayName = "5315.1.7.6 ForetakValidation - ERROR test")]
        public void SamsvarDirekte_5315_1_7_6_ForetakValideringERRORTest()
        {
            var val = new SamsvarDirekteFormValidator();

            var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                .Foretak("6974760673", "Foretak", "Foretak", "Benjamin Fjell", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "no", "Trondheim", "11223344", "99887766", "epost@test.no")
                .Build();
            val.ForetakValidation(samsvarserklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5315.1.7.6", 1, true);
        }
        [Fact(DisplayName = "5315.1.7.7 ForetakValidation - ERROR test")]
        public void SamsvarDirekte_5315_1_7_7_ForetakValideringERRORTest()
        {
            var val = new SamsvarDirekteFormValidator();

            var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                .Foretak("974760673", "Foretak", "Foretak", "Benjamin Fjell", "Siv. Ark. Fjell og Sønner", null, null, null, null, "11223344", "99887766", "epost@test.no")
                .Build();
            val.ForetakValidation(samsvarserklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5315.1.7.7", 1);
        }
        [Fact(DisplayName = "5315.1.7.8 ForetakValidation - ERROR test")]
        public void SamsvarDirekte_5315_1_7_71_ForetakValideringERRORTest()
        {
            var val = new SamsvarDirekteFormValidator();

            var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                .Foretak("974760673", "Foretak", "Foretak", "Benjamin Fjell", "Siv. Ark. Fjell og Sønner", null, "7003", "no", "Trondheim", "11223344", "99887766", "epost@test.no")
                .Build();
            val.ForetakValidation(samsvarserklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5315.1.7.8", 1);
        }
        [Fact(DisplayName = "5315.1.7.10 ForetakValidation - ERROR test")]
        public void SamsvarDirekte_5315_1_7_9_ForetakValideringERRORTest()
        {
            var val = new SamsvarDirekteFormValidator();

            var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                .Foretak("974760673", "Foretak", "Foretak", "Benjamin Fjell", "Siv. Ark. Fjell og Sønner", "Lillegata 5", null, "no", "Trondheim", "11223344", "99887766", "epost@test.no")
                .Build();
            val.ForetakValidation(samsvarserklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5315.1.7.10", 1);
        }
        [Fact(DisplayName = "5315.1.7.11 ForetakValidation - ERROR test")]
        public void SamsvarDirekte_5315_1_7_10_ForetakValideringERRORTest()
        {
            var val = new SamsvarDirekteFormValidator();

            var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                .Foretak("974760673", "Foretak", "Foretak", "Benjamin Fjell", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "T003", "no", "Trondheim", "11223344", "99887766", "epost@test.no")
                .Build();
            val.ForetakValidation(samsvarserklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5315.1.7.11", 1);
        }
        [Fact(DisplayName = "5315.1.7.12 ForetakValidation - ERROR test")]
        public void SamsvarDirekte_5315_1_7_11_ForetakValideringERRORTest()
        {
            var val = new SamsvarDirekteFormValidator();

            var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                .Foretak("974760673", "Foretak", "Foretak", "Benjamin Fjell", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "0000", "no", "Trondheim", "11223344", "99887766", "epost@test.no")
                .Build();
            val.ForetakValidation(samsvarserklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5315.1.7.12", 1, true);
        }
        [Fact(DisplayName = "5315.1.7.13 ForetakValidation - ERROR test")]
        public void SamsvarDirekte_5315_1_7_13_ForetakValideringERRORTest()
        {
            var val = new SamsvarDirekteFormValidator();

            var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                .Foretak("974760673", "Foretak", "Foretak", "Benjamin Fjell", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "no", "BØ", "11223344", "99887766", "epost@test.no")
                .Build();
            val.ForetakValidation(samsvarserklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5315.1.7.13", 1, true);
        }

        // testModel Test
        [Fact(DisplayName = " Endring Av Tillatelse - Bruk")]
        public void EndringAvTillatelse_EndringAvTillatelseAvBrukTest()
        {
            var val = new SamsvarDirekteFormValidator();
            var municipalityValidator = new Mock<IMunicipalityValidator>();
            municipalityValidator.Setup(s => s.ValidateKommunenummer(It.IsAny<string>())).Returns(new GeneralValidationResult() { Status = ValidationStatus.Ok, Message = "UnitTest" });
            val.MunicipalityValidator = municipalityValidator.Object;

            var formSkjema = new SamsvarserklaeringDirekteOpprettetDfv44169()._11SamsvarserklaeringDirekteProOgMidlertidig(true);
            var endringAvTillatelse = TestHelper.GetValue<SamsvarserklaeringDirekteType>(formSkjema);

            val.Validate(endringAvTillatelse, new List<string>() { });
            var result = val.GetResult();

            var list = result.messages.Select(m => m.reference).ToList();
            var rulesChecked = TestHelper.ConverRulesCheckedJson(result);

            TestHelper.AssertResult(result, null, 0, null, true);

        }

        // Common to all validations
    }
}
