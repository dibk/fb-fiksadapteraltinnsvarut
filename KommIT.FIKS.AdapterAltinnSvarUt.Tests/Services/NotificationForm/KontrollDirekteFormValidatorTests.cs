﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.NotificationForm;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.Builders;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.FormsSamples;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Validations;
using Moq;
using Newtonsoft.Json;
using no.kxml.skjema.dibk.kontrollerklaeringDirekteOpprettet;
using no.kxml.skjema.dibk.samsvarserklaeringDirekteOpprettet;
using Xunit;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Tests.Services.NotificationForm
{
    public class KontrollDirekteFormValidatorTests
    {
        [Fact(DisplayName = "5444.1.4.1 EiendomByggestedValidering - Error test")]
        public void KontrollDirekte_5444_1_4_1_EiendomByggestedValideringErrorTest()
        {

            var val = new KontrollDirekteFormValidator();

            var kontrollerklaeringDirekteType = new KontrollerklaeringDirekteBuilder()
                .EiendomByggested(null, "1", "3", "0", "0", "Hammerfest", "H0101", "192423880", "Båtsfjorden 6", "Båtsfjorden", "6", "9664", "SANDØYBOTN")
                .Build();
            val.EiendomByggestedValidering(kontrollerklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5444.1.4.1", 1);
        }

        [Fact(DisplayName = "5444.1.4.2 EiendomByggestedValidering - Error test")]
        public void KontrollDirekte_5444_1_4_2_EiendomByggestedValideringErrorTest()
        {
           var val = new KontrollDirekteFormValidator();
            var municipalityValidator = new Mock<IMunicipalityValidator>();
            municipalityValidator.Setup(s => s.ValidateKommunenummer(It.IsAny<string>())).Returns((GeneralValidationResult)null);
            val.MunicipalityValidator = municipalityValidator.Object;

            var kontrollerklaeringDirekteType = new KontrollerklaeringDirekteBuilder()
                .EiendomByggested("*2004", "1", "3", "0", "0", "Hammerfest", "H0101", "192423880", "Båtsfjorden 6", "Båtsfjorden", "6", "9664", "SANDØYBOTN")
                .Build();
            val.EiendomByggestedValidering(kontrollerklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5444.1.4.2", 1);

        }
        [Fact(DisplayName = "5444.1.4.3 EiendomByggestedValidering - Error test", Skip = "mangler info om en kommune som har ugyldig kode")]
        public void KontrollDirekte_5444_1_4_3_EiendomByggestedValideringErrorTest()
        {
           var val = new KontrollDirekteFormValidator();

            var kontrollerklaeringDirekteType = new KontrollerklaeringDirekteBuilder()
                .EiendomByggested("????", "1", "3", "0", "0", "Hammerfest", "H0101", "192423880", "Båtsfjorden 6", "Båtsfjorden", "6", "9664", "SANDØYBOTN")
                .Build();
            val.EiendomByggestedValidering(kontrollerklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5444.1.4.3", 1);

        }
        [Fact(DisplayName = "5444.1.4.4 EiendomByggestedValidering - Error test")]
        public void KontrollDirekte_5444_1_4_4_EiendomByggestedValideringErrorTest()
        {
           var val = new KontrollDirekteFormValidator();
            var municipalityValidator = new Mock<IMunicipalityValidator>();
            municipalityValidator.Setup(s => s.ValidateKommunenummer(It.IsAny<string>())).Returns(new GeneralValidationResult() { Status = ValidationStatus.Ok, Message = "UnitTest" });
            val.MunicipalityValidator = municipalityValidator.Object;

            var kontrollerklaeringDirekteType = new KontrollerklaeringDirekteBuilder()
                .EiendomByggested("2004", "1", null, "0", "0", "Hammerfest", "H0101", "192423880", "Båtsfjorden 6", "Båtsfjorden", "6", "9664", "SANDØYBOTN")
                .Build();
            val.EiendomByggestedValidering(kontrollerklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5444.1.4.4", 1);
        }
        [Fact(DisplayName = "5444.1.4.6 EiendomByggestedValidering - Error test")]
        public void KontrollDirekte_5444_1_4_6_EiendomByggestedValideringErrorTest()
        {
           var val = new KontrollDirekteFormValidator();
            var municipalityValidator = new Mock<IMunicipalityValidator>();
            municipalityValidator.Setup(s => s.ValidateKommunenummer(It.IsAny<string>())).Returns(new GeneralValidationResult() { Status = ValidationStatus.Ok, Message = "UnitTest" });
            val.MunicipalityValidator = municipalityValidator.Object;

            var kontrollerklaeringDirekteType = new KontrollerklaeringDirekteBuilder()
                .EiendomByggested("2004", "-1", "0", "0", "0", "Hammerfest", "H0101", "192423880", "Båtsfjorden 6", "Båtsfjorden", "6", "9664", "SANDØYBOTN")
                .Build();
            val.EiendomByggestedValidering(kontrollerklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5444.1.4.6", 1);
        }
        [Fact(DisplayName = "5444.1.4.6.1 EiendomByggestedValidering - Error test")]
        public void KontrollDirekte_5444_1_4_6_1_EiendomByggestedValideringErrorTest()
        {
           var val = new KontrollDirekteFormValidator();
            var municipalityValidator = new Mock<IMunicipalityValidator>();
            municipalityValidator.Setup(s => s.ValidateKommunenummer(It.IsAny<string>())).Returns(new GeneralValidationResult() { Status = ValidationStatus.Ok, Message = "UnitTest" });
            val.MunicipalityValidator = municipalityValidator.Object;

            var kontrollerklaeringDirekteType = new KontrollerklaeringDirekteBuilder()
                .EiendomByggested("2004", "0", "-1", "0", "0", "Hammerfest", "H0101", "192423880", "Båtsfjorden 6", "Båtsfjorden", "6", "9664", "SANDØYBOTN")
                .Build();
            val.EiendomByggestedValidering(kontrollerklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5444.1.4.6.1", 1);
        }
        [Fact(DisplayName = "5444.1.4.7 EiendomByggestedValidering - Error test")]
        public void KontrollDirekte_5444_1_4_7_EiendomByggestedValideringErrorTest()
        {
           var val = new KontrollDirekteFormValidator();
            var municipalityValidator = new Mock<IMunicipalityValidator>();
            municipalityValidator.Setup(s => s.ValidateKommunenummer(It.IsAny<string>())).Returns(new GeneralValidationResult() { Status = ValidationStatus.Ok, Message = "UnitTest" });
            val.MunicipalityValidator = municipalityValidator.Object;

            var kontrollerklaeringDirekteType = new KontrollerklaeringDirekteBuilder()
                .EiendomByggested("2004", "1", "3", "0", "0", "Hammerfest", "H0101", "*192423880", "Båtsfjorden 6", "Båtsfjorden", "6", "9664", "SANDØYBOTN")
                .Build();
            val.EiendomByggestedValidering(kontrollerklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5444.1.4.7", 1, null, true);
        }
        [Fact(DisplayName = "5444.1.4.8 EiendomByggestedValidering - Error test")]
        public void KontrollDirekte_5444_1_4_8_EiendomByggestedValideringErrorTest()
        {
           var val = new KontrollDirekteFormValidator();
            var municipalityValidator = new Mock<IMunicipalityValidator>();
            municipalityValidator.Setup(s => s.ValidateKommunenummer(It.IsAny<string>())).Returns(new GeneralValidationResult() { Status = ValidationStatus.Ok, Message = "UnitTest" });
            val.MunicipalityValidator = municipalityValidator.Object;

            var kontrollerklaeringDirekteType = new KontrollerklaeringDirekteBuilder()
                .EiendomByggested("2004", "1", "3", "0", "0", "Hammerfest", "H0101", "0", "Båtsfjorden 6", "Båtsfjorden", "6", "9664", "SANDØYBOTN")
                .Build();
            val.EiendomByggestedValidering(kontrollerklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5444.1.4.8", 1, null, true);
        }
        [Fact(DisplayName = "5444.1.4.9 EiendomByggestedValidering - Error test")]
        public void KontrollDirekte_5444_1_4_9_EiendomByggestedValideringErrorTest()
        {
           var val = new KontrollDirekteFormValidator();
            var municipalityValidator = new Mock<IMunicipalityValidator>();
            municipalityValidator.Setup(s => s.ValidateKommunenummer(It.IsAny<string>())).Returns(new GeneralValidationResult() { Status = ValidationStatus.Ok, Message = "UnitTest" });
            val.MunicipalityValidator = municipalityValidator.Object;

            var kontrollerklaeringDirekteType = new KontrollerklaeringDirekteBuilder()
                .EiendomByggested("2004", "1", "3", "0", "0", "Hammerfest", "H0101", "192423880")
                .Build();
            val.EiendomByggestedValidering(kontrollerklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5444.1.4.9", 1, null, true);
        }
        [Fact(DisplayName = "5444.1.4.9.1 EiendomByggestedValidering - Error test")]
        public void KontrollDirekte_5444_1_4_9_1_EiendomByggestedValideringErrorTest()
        {
           var val = new KontrollDirekteFormValidator();
            var municipalityValidator = new Mock<IMunicipalityValidator>();
            municipalityValidator.Setup(s => s.ValidateKommunenummer(It.IsAny<string>())).Returns(new GeneralValidationResult() { Status = ValidationStatus.Ok, Message = "UnitTest" });
            val.MunicipalityValidator = municipalityValidator.Object;

            var kontrollerklaeringDirekteType = new KontrollerklaeringDirekteBuilder()
                .EiendomByggested("2004", "1", "3", "0", "0", "Hammerfest", "H0101", "192423880", null, "Båtsfjorden", "6", "9664", "SANDØYBOTN")
                .Build();
            val.EiendomByggestedValidering(kontrollerklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5444.1.4.9.1", 1, null, true);
        }
        [Fact(DisplayName = "5444.1.4.9.2 EiendomByggestedValidering - Error test")]
        public void KontrollDirekte_5444_1_4_9_2_EiendomByggestedValideringErrorTest()
        {
           var val = new KontrollDirekteFormValidator();
            var municipalityValidator = new Mock<IMunicipalityValidator>();
            municipalityValidator.Setup(s => s.ValidateKommunenummer(It.IsAny<string>())).Returns(new GeneralValidationResult() { Status = ValidationStatus.Ok, Message = "UnitTest" });
            val.MunicipalityValidator = municipalityValidator.Object;

            var kontrollerklaeringDirekteType = new KontrollerklaeringDirekteBuilder()
                .EiendomByggested("2004", "1", "3", "0", "0", "Hammerfest", "H0101", "192423880", "Båtsfjorden 6", "Båtsfjorden", null, "9664", "SANDØYBOTN")
                .Build();
            val.EiendomByggestedValidering(kontrollerklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5444.1.4.9.2", 1, null, true);
        }
        [Fact(DisplayName = "5444.1.4.9.2b EiendomByggestedValidering - Error test")]
        public void KontrollDirekte_5444_1_4_9_2b_EiendomByggestedValideringErrorTest()
        {
           var val = new KontrollDirekteFormValidator();
            var municipalityValidator = new Mock<IMunicipalityValidator>();
            municipalityValidator.Setup(s => s.ValidateKommunenummer(It.IsAny<string>())).Returns(new GeneralValidationResult() { Status = ValidationStatus.Ok, Message = "UnitTest" });
            val.MunicipalityValidator = municipalityValidator.Object;

            var kontrollerklaeringDirekteType = new KontrollerklaeringDirekteBuilder()
                .EiendomByggested("2004", "1", "3", "0", "0", "Hammerfest", "H0101", "192423880", "Båtsfjorden 6", null, "6", "9664", "SANDØYBOTN")

                .Build();
            val.EiendomByggestedValidering(kontrollerklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5444.1.4.9.2", 1, null, true);
        }
        [Fact(DisplayName = "5444.1.4.10 EiendomByggestedValidering - Error test")]
        public void KontrollDirekte_5444_1_4_10_EiendomByggestedValideringErrorTest()
        {
           var val = new KontrollDirekteFormValidator();
            var municipalityValidator = new Mock<IMunicipalityValidator>();
            municipalityValidator.Setup(s => s.ValidateKommunenummer(It.IsAny<string>())).Returns(new GeneralValidationResult() { Status = ValidationStatus.Ok, Message = "UnitTest" });
            val.MunicipalityValidator = municipalityValidator.Object;

            var kontrollerklaeringDirekteType = new KontrollerklaeringDirekteBuilder()
                .EiendomByggested("2004", "1", "3", "0", "0", null, "H0101", "192423880", "Båtsfjorden 6", "Båtsfjorden", "6", "9664", "SANDØYBOTN")
                .Build();
            val.EiendomByggestedValidering(kontrollerklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5444.1.4.10", 1, null, true);
        }

        [Fact(DisplayName = "5444.1.4.11 EiendomByggestedValidering - Error test")]
        public void KontrollDirekte_5444_1_4_11_EiendomByggestedValideringErrorTest()
        {
           var val = new KontrollDirekteFormValidator();
            var municipalityValidator = new Mock<IMunicipalityValidator>();
            municipalityValidator.Setup(s => s.ValidateKommunenummer(It.IsAny<string>())).Returns(new GeneralValidationResult() { Status = ValidationStatus.Ok, Message = "UnitTest" });
            val.MunicipalityValidator = municipalityValidator.Object;

            var kontrollerklaeringDirekteType = new KontrollerklaeringDirekteBuilder()
                .EiendomByggested("2004", "1", "3", "0", "0", "Hammerfest", "*H0101", "192423880", "Båtsfjorden 6", "Båtsfjorden", "6", "9664", "SANDØYBOTN")
                .Build();
            val.EiendomByggestedValidering(kontrollerklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5444.1.4.11", 1, null, true);
        }
        [Fact(DisplayName = "5444.1.4.12 EiendomByggestedValidering - Error test", Skip = "Matrikkel validering")]
        public void KontrollDirekte_5444_1_4_12_EiendomByggestedValideringErrorTest()
        {
           var val = new KontrollDirekteFormValidator();

            var kontrollerklaeringDirekteType = new KontrollerklaeringDirekteBuilder()
                .EiendomByggested("2004", "1", "3", "0", "0", "Hammerfest", "H0101", "192423880", "Båtsfjorden 6", "Båtsfjorden", "6", "9664", "SANDØYBOTN")
                .Build();
            val.EiendomByggestedValidering(kontrollerklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5444.1.4.12", 1);
        }
        [Fact(DisplayName = "5444.1.4.13 EiendomByggestedValidering - Error test", Skip = "Matrikkel validering")]
        public void KontrollDirekte_5444_1_4_13_EiendomByggestedValideringErrorTest()
        {
           var val = new KontrollDirekteFormValidator();

            var kontrollerklaeringDirekteType = new KontrollerklaeringDirekteBuilder()
                .EiendomByggested("2004", "1", "3", "0", "0", "Hammerfest", "H0101", "192423880", "Båtsfjorden 6", "Båtsfjorden", "6", "9664", "SANDØYBOTN")
                .Build();
            val.EiendomByggestedValidering(kontrollerklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5444.1.4.13", 1);
        }
        [Fact(DisplayName = "5444.1.4.14 EiendomByggestedValidering - Error test", Skip = "Matrikkel validering")]
        public void KontrollDirekte_5444_1_4_14_EiendomByggestedValideringErrorTest()
        {
           var val = new KontrollDirekteFormValidator();

            var kontrollerklaeringDirekteType = new KontrollerklaeringDirekteBuilder()
                .EiendomByggested("2004", "1", "3", "0", "0", "Hammerfest", "H0101", "666423880", "Båtsfjorden 6", "Båtsfjorden", "6", "9664", "SANDØYBOTN")
                .Build();
            val.EiendomByggestedValidering(kontrollerklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5444.1.4.14", 1);
        }
        [Fact(DisplayName = "5444.1.4.15 EiendomByggestedValidering - Error test", Skip = "Matrikkel validering")]
        public void KontrollDirekte_5444_1_4_15_EiendomByggestedValideringErrorTest()
        {
           var val = new KontrollDirekteFormValidator();

            var kontrollerklaeringDirekteType = new KontrollerklaeringDirekteBuilder()
                .EiendomByggested("2004", "1", "3", "0", "0", "Hammerfest", "H0101", "192423880", "Båtsfjorden 6", "*Båtsfjorden", "6", "9664", "SANDØYBOTN")
                .Build();
            val.EiendomByggestedValidering(kontrollerklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5444.1.4.11", 1);
        }
        //

        [Fact(DisplayName = "5444.1.6 AnsvarsrettValidering - OK test")]
        public void KontrollDirekte_5444_1_6_AnsvarsrettValideringValideringOKTest()
        {

           var val = new KontrollDirekteFormValidator();

            var kontrollerklaeringDirekteType = new KontrollerklaeringDirekteBuilder()
                .Ansvarsrett("KONTROLL", "Ansvarlig kontroll", false,false,true,true, DateTime.Now.AddDays(13),"Søknads System Referense Nr","Installasjon branninstallasjoner")
                .Build();
            val.AnsvarsrettValidering(kontrollerklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, null, 0);

        }
        [Fact(DisplayName = "5444.1.6 AnsvarsrettValidering - Error test")]
        public void KontrollDirekte_5444_1_6_AnsvarsrettValideringValideringErrorTest()
        {

           var val = new KontrollDirekteFormValidator();

            var kontrollerklaeringDirekteType = new KontrollerklaeringDirekteBuilder()
                .Ansvarsrett()
                .Build();
            val.AnsvarsrettValidering(kontrollerklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5444.1.6", 1);
        }
        [Fact(DisplayName = "5444.1.6.1 AnsvarsrettValidering - Error test")]
        public void KontrollDirekte_5444_1_6_1_AnsvarsrettValideringValideringErrorTest()
        {

           var val = new KontrollDirekteFormValidator();

            var kontrollerklaeringDirekteType = new KontrollerklaeringDirekteBuilder()
                .Ansvarsrett(null, null, false, false, true, true, DateTime.Now.AddDays(13), "Søknads System Referense Nr", "Installasjon branninstallasjoner")
                .Build();
            val.AnsvarsrettValidering(kontrollerklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5444.1.6.1", 1);
        }

        [Fact(DisplayName = "5444.1.1.2 AnsvarsrettValidering - Error test")]
        public void KontrollDirekte_5444_1_1_2_AnsvarsrettValideringValideringErrorTest()
        {

           var val = new KontrollDirekteFormValidator();

            var kontrollerklaeringDirekteType = new KontrollerklaeringDirekteBuilder()
                .Ansvarsrett("", "Ansvarlig kontroll", false, false, true, true, DateTime.Now.AddDays(1), "Søknads System Referense Nr", "Installasjon branninstallasjoner")
                .Build();
            val.AnsvarsrettValidering(kontrollerklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5444.1.1.2", 1, false);
        }
        [Fact(DisplayName = "5444.1.2.2 AnsvarsrettValidering - Error test")]
        public void KontrollDirekte_5444_1_2_2_AnsvarsrettValideringValideringErrorTest()
        {

           var val = new KontrollDirekteFormValidator();

            var kontrollerklaeringDirekteType = new KontrollerklaeringDirekteBuilder()
                .Ansvarsrett("*KONTROLL", "Ansvarlig kontroll", false, false, true, true, DateTime.Now.AddDays(13), "Søknads System Referense Nr", "Installasjon branninstallasjoner")

                .Build();
            val.AnsvarsrettValidering(kontrollerklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5444.1.2.2", 1);
        }
        
        [Fact(DisplayName = "5444.1.6.2 AnsvarsrettValidering - Error test")]
        public void KontrollDirekte_5444_1_6_2_AnsvarsrettValideringValideringErrorTest()
        {

            var val = new KontrollDirekteFormValidator();

            var kontrollerklaeringDirekteType = new KontrollerklaeringDirekteBuilder()
                .Ansvarsrett("PRO", "Ansvarlig Projektering", false, false, true, true, DateTime.Now.AddDays(1), "Søknads System Referense Nr", "Installasjon branninstallasjoner")
                .Build();
            val.AnsvarsrettValidering(kontrollerklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5444.1.6.2", 1);
        } 

        [Fact(DisplayName = "5444.1.6.3 AnsvarsrettValidering - Error test")]
        public void KontrollDirekte_5444_1_6_3_AnsvarsrettValideringErrorTest()
        {
            var val = new KontrollDirekteFormValidator();
            var kontrollerklaeringDirekteType = new KontrollerklaeringDirekteBuilder()
                .Ansvarsrett("KONTROLL", "Ansvarlig kontroll", null, null, null, true, DateTime.Now.AddDays(13), "Søknads System Referense Nr", "Installasjon branninstallasjoner")
                .Build();
            val.AnsvarsrettValidering(kontrollerklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5444.1.6.3", 1);
        }
        [Fact(DisplayName = "5444.1.6.4 AnsvarsrettValidering - Error test")]
        public void KontrollDirekte_5444_1_6_4_AnsvarsrettValideringErrorTest()
        {
            var val = new KontrollDirekteFormValidator();
            var kontrollerklaeringDirekteType = new KontrollerklaeringDirekteBuilder()
                .Ansvarsrett("KONTROLL", "Ansvarlig kontroll", false, false, false, true, DateTime.Now.AddDays(13), "Søknads System Referense Nr", "Installasjon branninstallasjoner")
                .Build();
            val.AnsvarsrettValidering(kontrollerklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5444.1.6.4", 1);
        }
        [Fact(DisplayName = "5444.1.6.5 AnsvarsrettValidering - Error test")]
        public void KontrollDirekte_5444_1_6_5_AnsvarsrettValideringErrorTest()
        {
            var val = new KontrollDirekteFormValidator();
            var kontrollerklaeringDirekteType = new KontrollerklaeringDirekteBuilder()
                .Ansvarsrett("KONTROLL", "Ansvarlig kontroll", true, false, true, true, DateTime.Now.AddDays(13), "Søknads System Referense Nr", "Installasjon branninstallasjoner")
                .Build();
            val.AnsvarsrettValidering(kontrollerklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5444.1.6.5", 1);
        }
        [Fact(DisplayName = "5444.1.6.6 AnsvarsrettValidering - Error test")]
        public void KontrollDirekte_5444_1_6_6_AnsvarsrettValideringErrorTest()
        {
            var val = new KontrollDirekteFormValidator();
            var kontrollerklaeringDirekteType = new KontrollerklaeringDirekteBuilder()
                .Ansvarsrett("KONTROLL", "Ansvarlig kontroll", false, false, true, true, DateTime.Now.AddDays(13), "Søknads System Referense Nr", null)
                .Build();
            val.AnsvarsrettValidering(kontrollerklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5444.1.6.6", 1);
        }  
        [Fact(DisplayName = "5444.1.6.8 AnsvarsrettValidering - Error test")]
        public void KontrollDirekte_5444_1_6_8_AnsvarsrettValideringErrorTest()
        {
            var val = new KontrollDirekteFormValidator();
            var kontrollerklaeringDirekteType = new KontrollerklaeringDirekteBuilder()
                .Ansvarsrett("KONTROLL", "Ansvarlig kontroll", false, false, true, true, null, "Søknads System Referense Nr", "Installasjon branninstallasjoner")
                .Build();
            val.AnsvarsrettValidering(kontrollerklaeringDirekteType);
            var result = val.GetResult();
            var rulesCheked = result.rulesChecked;
            var n = JsonConvert.SerializeObject(rulesCheked);

            TestHelper.AssertResult(result, "5444.1.6.8", 1);
        }

        [Fact(DisplayName = "5444.1.8 SoekerValidation - OK test")]
        public void KontrollDirekte_5444_1_8_SoekerValideringOKTest()
        {
           var val = new KontrollDirekteFormValidator();

            var kontrollerklaeringDirekteType = new KontrollerklaeringDirekteBuilder()
                .AnsvarligSoeker("974760673", "Foretak", "Foretak", "Benjamin Fjell", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "no", "Trondheim", "11223344", "99887766", "epost@test.no")
                .Build();
            val.SoekerValidation(kontrollerklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, null, 0, true);
        }
        [Fact(DisplayName = "5444.1.8 SoekerValidation - ERROR test")]
        public void KontrollDirekte_5444_1_8_SoekerValideringERRORTest()
        {
           var val = new KontrollDirekteFormValidator();

            var kontrollerklaeringDirekteType = new KontrollerklaeringDirekteBuilder()
                .Build();
            val.SoekerValidation(kontrollerklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5444.1.8", 1);
        }
        [Fact(DisplayName = "5444.1.8.1 SoekerValidation - ERROR test")]
        public void KontrollDirekte_5444_1_8_1_SoekerValideringERRORTest()
        {
           var val = new KontrollDirekteFormValidator();

            var kontrollerklaeringDirekteType = new KontrollerklaeringDirekteBuilder()
                .AnsvarligSoeker("974760673", null, null, "Benjamin Fjell", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "11223344", "NO")
                .Build();
            val.SoekerValidation(kontrollerklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5444.1.8.1", 1);
        }
        [Fact(DisplayName = "5444.1.8.1b SoekerValidation - ERROR test")]
        public void KontrollDirekte_5444_1_8_1b_SoekerValideringERRORTest()
        {
           var val = new KontrollDirekteFormValidator();

            var kontrollerklaeringDirekteType = new KontrollerklaeringDirekteBuilder()
                .AnsvarligSoeker("974760673", null, "Foretak", "Benjamin Fjell", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "11223344", "NO")
                .Build();
            val.SoekerValidation(kontrollerklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5444.1.8.1", 1);
        }
        [Fact(DisplayName = "5444.1.8.2 SoekerValidation - ERROR test")]
        public void KontrollDirekte_5444_1_8_2_SoekerValideringERRORTest()
        {
           var val = new KontrollDirekteFormValidator();

            var kontrollerklaeringDirekteType = new KontrollerklaeringDirekteBuilder()
                .AnsvarligSoeker("974760673", "*Foretak", "Foretak", "Benjamin Fjell", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "11223344", "NO")
                .Build();
            val.SoekerValidation(kontrollerklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5444.1.8.2", 1);
        }
        [Fact(DisplayName = "5444.1.8.3 SoekerValidation - ERROR test")]
        public void KontrollDirekte_5444_1_8_3_SoekerValideringERRORTest()
        {
           var val = new KontrollDirekteFormValidator();

            var kontrollerklaeringDirekteType = new KontrollerklaeringDirekteBuilder()
                .AnsvarligSoeker(null, "Privatperson", "Privatperson")
                .Build();
            val.SoekerValidation(kontrollerklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5444.1.8.3", 1);
        }
        [Fact(DisplayName = "5444.1.8.4 SoekerValidation - ERROR test")]
        public void KontrollDirekte_5444_1_8_4_SoekerValideringERRORTest()
        {
           var val = new KontrollDirekteFormValidator();

            var kontrollerklaeringDirekteType = new KontrollerklaeringDirekteBuilder()
                .AnsvarligSoeker(null, "Foretak", "Foretak", "Benjamin Fjell", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "no", "Trondheim", "11223344", "99887766", "epost@test.no")
                .Build();
            val.SoekerValidation(kontrollerklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5444.1.8.4", 1, true);
        }
        [Fact(DisplayName = "5444.1.8.5 SoekerValidation - ERROR test")]
        public void KontrollDirekte_5444_1_8_5_SoekerValideringERRORTest()
        {
           var val = new KontrollDirekteFormValidator();

            var kontrollerklaeringDirekteType = new KontrollerklaeringDirekteBuilder()
                .AnsvarligSoeker("974760000", "Foretak", "Foretak", "Benjamin Fjell", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "no", "Trondheim", "11223344", "99887766", "epost@test.no")
                .Build();
            val.SoekerValidation(kontrollerklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5444.1.8.5", 1, true);
        }
        [Fact(DisplayName = "5444.1.8.6 SoekerValidation - ERROR test")]
        public void KontrollDirekte_5444_1_8_6_SoekerValideringERRORTest()
        {
           var val = new KontrollDirekteFormValidator();

            var kontrollerklaeringDirekteType = new KontrollerklaeringDirekteBuilder()
                .AnsvarligSoeker("6974760673", "Foretak", "Foretak", "Benjamin Fjell", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "no", "Trondheim", "11223344", "99887766", "epost@test.no")
                .Build();
            val.SoekerValidation(kontrollerklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5444.1.8.6", 1, true);
        }
        [Fact(DisplayName = "5444.1.8.7 SoekerValidation - ERROR test")]
        public void KontrollDirekte_5444_1_8_7_SoekerValideringERRORTest()
        {
           var val = new KontrollDirekteFormValidator();

            var kontrollerklaeringDirekteType = new KontrollerklaeringDirekteBuilder()
                .AnsvarligSoeker("974760673", "Foretak", "Foretak", "Benjamin Fjell", "Siv. Ark. Fjell og Sønner", null, null, null, null, "11223344", "99887766", "epost@test.no")
                .Build();
            val.SoekerValidation(kontrollerklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5444.1.8.7", 1);
        }
        [Fact(DisplayName = "5444.1.8.8 SoekerValidation - ERROR test")]
        public void KontrollDirekte_5444_1_8_8_SoekerValideringERRORTest()
        {
           var val = new KontrollDirekteFormValidator();

            var kontrollerklaeringDirekteType = new KontrollerklaeringDirekteBuilder()
                .AnsvarligSoeker("974760673", "Foretak", "Foretak", "Benjamin Fjell", "Siv. Ark. Fjell og Sønner", null, "7003", "no", "Trondheim", "11223344", "99887766", "epost@test.no")
                .Build();
            val.SoekerValidation(kontrollerklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5444.1.8.8", 1);
        }
        [Fact(DisplayName = "5444.1.8.10 SoekerValidation - ERROR test")]
        public void KontrollDirekte_5444_1_8_10_SoekerValideringERRORTest()
        {
           var val = new KontrollDirekteFormValidator();

            var kontrollerklaeringDirekteType = new KontrollerklaeringDirekteBuilder()
                .AnsvarligSoeker("974760673", "Foretak", "Foretak", "Benjamin Fjell", "Siv. Ark. Fjell og Sønner", "Lillegata 5", null, "no", "Trondheim", "11223344", "99887766", "epost@test.no")
                .Build();
            val.SoekerValidation(kontrollerklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5444.1.8.10", 1);
        }
        [Fact(DisplayName = "5444.1.8.11 SoekerValidation - ERROR test")]
        public void KontrollDirekte_5444_1_8_11_SoekerValideringERRORTest()
        {
           var val = new KontrollDirekteFormValidator();

            var kontrollerklaeringDirekteType = new KontrollerklaeringDirekteBuilder()
                .AnsvarligSoeker("974760673", "Foretak", "Foretak", "Benjamin Fjell", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "T003", "no", "Trondheim", "11223344", "99887766", "epost@test.no")
                .Build();
            val.SoekerValidation(kontrollerklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5444.1.8.11", 1);
        }
        [Fact(DisplayName = "5444.1.8.12 SoekerValidation - ERROR test")]
        public void KontrollDirekte_5444_1_8_12_SoekerValideringERRORTest()
        {
           var val = new KontrollDirekteFormValidator();

            var kontrollerklaeringDirekteType = new KontrollerklaeringDirekteBuilder()
                .AnsvarligSoeker("974760673", "Foretak", "Foretak", "Benjamin Fjell", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "0000", "no", "Trondheim", "11223344", "99887766", "epost@test.no")
                .Build();
            val.SoekerValidation(kontrollerklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5444.1.8.12", 1, true);
        }
        [Fact(DisplayName = "5444.1.8.13 SoekerValidation - ERROR test")]
        public void KontrollDirekte_5444_1_8_13_SoekerValideringERRORTest()
        {
           var val = new KontrollDirekteFormValidator();

            var kontrollerklaeringDirekteType = new KontrollerklaeringDirekteBuilder()
                .AnsvarligSoeker("974760673", "Foretak", "Foretak", "Benjamin Fjell", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "no", "BØ", "11223344", "99887766", "epost@test.no")
                .Build();
            val.SoekerValidation(kontrollerklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5444.1.8.13", 1, true);
        }

        [Fact(DisplayName = "5444.1.7 ForetakValidation - OK test")]
        public void KontrollDirekte_5444_1_7_ForetakValideringOKTest()
        {
           var val = new KontrollDirekteFormValidator();

            var kontrollerklaeringDirekteType = new KontrollerklaeringDirekteBuilder()
                .Foretak("974760673", "Foretak", "Foretak", "Benjamin Fjell", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "no", "Trondheim", "11223344", "99887766", "epost@test.no")
                .Build();
            val.ForetakValidation(kontrollerklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, null, 0, true);
        }
        [Fact(DisplayName = "5444.1.7 ForetakValidation - ERROR test")]
        public void KontrollDirekte_5444_1_7_ForetakValideringERRORTest()
        {
           var val = new KontrollDirekteFormValidator();

            var kontrollerklaeringDirekteType = new KontrollerklaeringDirekteBuilder()
                .Build();
            val.ForetakValidation(kontrollerklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5444.1.7", 1);
        }
        [Fact(DisplayName = "5444.1.7.1 ForetakValidation - ERROR test")]
        public void KontrollDirekte_5444_1_7_1_ForetakValideringERRORTest()
        {
           var val = new KontrollDirekteFormValidator();

            var kontrollerklaeringDirekteType = new KontrollerklaeringDirekteBuilder()
                .Foretak("974760673", null, null, "Benjamin Fjell", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "11223344", "NO")
                .Build();
            val.ForetakValidation(kontrollerklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5444.1.7.1", 1);
        }
        [Fact(DisplayName = "5444.1.7.1b ForetakValidation - ERROR test")]
        public void KontrollDirekte_5444_1_7_1b_ForetakValideringERRORTest()
        {
           var val = new KontrollDirekteFormValidator();

            var kontrollerklaeringDirekteType = new KontrollerklaeringDirekteBuilder()
                .Foretak("974760673", null, "Foretak", "Benjamin Fjell", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "11223344", "NO")
                .Build();
            val.ForetakValidation(kontrollerklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5444.1.7.1", 1);
        }
        [Fact(DisplayName = "5444.1.7.2 ForetakValidation - ERROR test")]
        public void KontrollDirekte_5444_1_7_2_ForetakValideringERRORTest()
        {
           var val = new KontrollDirekteFormValidator();

            var kontrollerklaeringDirekteType = new KontrollerklaeringDirekteBuilder()
                .Foretak("974760673", "*Foretak", "Foretak", "Benjamin Fjell", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "11223344", "NO")
                .Build();
            val.ForetakValidation(kontrollerklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5444.1.7.2", 1);
        }
        [Fact(DisplayName = "5444.1.7.3 ForetakValidation - ERROR test")]
        public void KontrollDirekte_5444_1_7_3_ForetakValideringERRORTest()
        {
           var val = new KontrollDirekteFormValidator();

            var kontrollerklaeringDirekteType = new KontrollerklaeringDirekteBuilder()
                .Foretak(null, "Privatperson", "Privatperson")
                .Build();
            val.ForetakValidation(kontrollerklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5444.1.7.3", 1);
        }
        [Fact(DisplayName = "5444.1.7.4 ForetakValidation - ERROR test")]
        public void KontrollDirekte_5444_1_7_4_ForetakValideringERRORTest()
        {
           var val = new KontrollDirekteFormValidator();

            var kontrollerklaeringDirekteType = new KontrollerklaeringDirekteBuilder()
                .Foretak(null, "Foretak", "Foretak", "Benjamin Fjell", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "no", "Trondheim", "11223344", "99887766", "epost@test.no")
                .Build();
            val.ForetakValidation(kontrollerklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5444.1.7.4", 1, true);
        }
        [Fact(DisplayName = "5444.1.7.5 ForetakValidation - ERROR test")]
        public void KontrollDirekte_5444_1_7_5_ForetakValideringERRORTest()
        {
           var val = new KontrollDirekteFormValidator();

            var kontrollerklaeringDirekteType = new KontrollerklaeringDirekteBuilder()
                .Foretak("*974760000", "Foretak", "Foretak", "Benjamin Fjell", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "no", "Trondheim", "11223344", "99887766", "epost@test.no")
                .Build();
            val.ForetakValidation(kontrollerklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5444.1.7.5", 1, true);
        }
        [Fact(DisplayName = "5444.1.7.6 ForetakValidation - ERROR test")]
        public void KontrollDirekte_5444_1_7_6_ForetakValideringERRORTest()
        {
           var val = new KontrollDirekteFormValidator();

            var kontrollerklaeringDirekteType = new KontrollerklaeringDirekteBuilder()
                .Foretak("000060673", "Foretak", "Foretak", "Benjamin Fjell", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "no", "Trondheim", "11223344", "99887766", "epost@test.no")
                .Build();
            val.ForetakValidation(kontrollerklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5444.1.7.6", 1, true);
        }
        [Fact(DisplayName = "5444.1.7.7 ForetakValidation - ERROR test")]
        public void KontrollDirekte_5444_1_7_7_ForetakValideringERRORTest()
        {
           var val = new KontrollDirekteFormValidator();

            var kontrollerklaeringDirekteType = new KontrollerklaeringDirekteBuilder()
                .Foretak("974760673", "Foretak", "Foretak", "Benjamin Fjell", "Siv. Ark. Fjell og Sønner", null, null, null, null, "11223344", "99887766", "epost@test.no")
                .Build();
            val.ForetakValidation(kontrollerklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5444.1.7.7", 1);
        }
        [Fact(DisplayName = "5444.1.7.8 ForetakValidation - ERROR test")]
        public void KontrollDirekte_5444_1_7_71_ForetakValideringERRORTest()
        {
           var val = new KontrollDirekteFormValidator();

            var kontrollerklaeringDirekteType = new KontrollerklaeringDirekteBuilder()
                .Foretak("974760673", "Foretak", "Foretak", "Benjamin Fjell", "Siv. Ark. Fjell og Sønner", null, "7003", "no", "Trondheim", "11223344", "99887766", "epost@test.no")
                .Build();
            val.ForetakValidation(kontrollerklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5444.1.7.8", 1);
        }
        [Fact(DisplayName = "5444.1.7.10 ForetakValidation - ERROR test")]
        public void KontrollDirekte_5444_1_7_9_ForetakValideringERRORTest()
        {
           var val = new KontrollDirekteFormValidator();

            var kontrollerklaeringDirekteType = new KontrollerklaeringDirekteBuilder()
                .Foretak("974760673", "Foretak", "Foretak", "Benjamin Fjell", "Siv. Ark. Fjell og Sønner", "Lillegata 5", null, "no", "Trondheim", "11223344", "99887766", "epost@test.no")
                .Build();
            val.ForetakValidation(kontrollerklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5444.1.7.10", 1);
        }
        [Fact(DisplayName = "5444.1.7.11 ForetakValidation - ERROR test")]
        public void KontrollDirekte_5444_1_7_10_ForetakValideringERRORTest()
        {
           var val = new KontrollDirekteFormValidator();

            var kontrollerklaeringDirekteType = new KontrollerklaeringDirekteBuilder()
                .Foretak("974760673", "Foretak", "Foretak", "Benjamin Fjell", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "T003", "no", "Trondheim", "11223344", "99887766", "epost@test.no")
                .Build();
            val.ForetakValidation(kontrollerklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5444.1.7.11", 1);
        }
        [Fact(DisplayName = "5444.1.7.12 ForetakValidation - ERROR test")]
        public void KontrollDirekte_5444_1_7_11_ForetakValideringERRORTest()
        {
           var val = new KontrollDirekteFormValidator();

            var kontrollerklaeringDirekteType = new KontrollerklaeringDirekteBuilder()
                .Foretak("974760673", "Foretak", "Foretak", "Benjamin Fjell", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "0000", "no", "Trondheim", "11223344", "99887766", "epost@test.no")
                .Build();
            val.ForetakValidation(kontrollerklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5444.1.7.12", 1, true);
        }
        [Fact(DisplayName = "5444.1.7.13 ForetakValidation - ERROR test")]
        public void KontrollDirekte_5444_1_7_12_ForetakValideringERRORTest()
        {
           var val = new KontrollDirekteFormValidator();

            var kontrollerklaeringDirekteType = new KontrollerklaeringDirekteBuilder()
                .Foretak("974760673", "Foretak", "Foretak", "Benjamin Fjell", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "no", "BØ", "11223344", "99887766", "epost@test.no")
                .Build();
            val.ForetakValidation(kontrollerklaeringDirekteType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "5444.1.7.13", 1, true);
        }
        [Fact(DisplayName = "5444.1.10 VedleggValidering - ERROR test")]
        public void KontrollDirekte_5444_1_10_VedleggValideringERRORTest()
        {
           var val = new KontrollDirekteFormValidator();
            val.VedleggValidering(new List<string>() { "*PlanForUavhengigKontroll" });
            var result = val.GetResult();
        
            TestHelper.AssertResult(result, "5444.1.10", 1, true);
        }
        // testModel Test

        [Fact(DisplayName = " Samples Test", Skip = "Test Data")]
        public void EndringAvTillatelse_EndringAvTillatelseAvBrukTest()
        {
            var val = new KontrollDirekteFormValidator();
            var municipalityValidator = new Mock<IMunicipalityValidator>();
            municipalityValidator.Setup(s => s.ValidateKommunenummer(It.IsAny<string>())).Returns(new GeneralValidationResult() { Status = ValidationStatus.Ok, Message = "UnitTest" });
            val.MunicipalityValidator = municipalityValidator.Object;

            var formSkjema = new KontrollerklaeringDirekteOpprettetDfv45020()._03KontrollerklaeringObserverteOgAapneAvvik(true);
            var endringAvTillatelse = GetSamsvarserklaeringDirekteType(formSkjema);

            val.Validate(endringAvTillatelse, new List<string>(){ "PlanForUavhengigKontroll" });
            var result = val.GetResult();

            var list = result.messages.Select(m => m.reference).ToList();

            var rulescheked = TestHelper.ConverRulesCheckedJson(result);
            TestHelper.AssertResult(result, null, 0, null, true);

        }

        // Common to all validations

        private static KontrollerklaeringDirekteType GetSamsvarserklaeringDirekteType(Skjema formSkjema)
        {
            var xmlSkjema = formSkjema.Data;
            XmlSerializer serializer = new XmlSerializer(typeof(KontrollerklaeringDirekteType));
            var stringReader = new StringReader(xmlSkjema);
            var endringAvTillatelse = (KontrollerklaeringDirekteType)serializer.Deserialize(stringReader);

            //Save Xml Example
            //var path = Path.Combine(@"C:\Temp\", $"{formSkjema.VariantId}.xml");
            // File.WriteAllText(path, xmlSkjema);

            return endringAvTillatelse;
        }
    }
}
