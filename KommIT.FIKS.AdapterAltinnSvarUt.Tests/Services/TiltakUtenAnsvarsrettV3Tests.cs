﻿using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.Builders;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.FormsSamples;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.SvarUtForm;
using no.kxml.skjema.dibk.tiltakutenansvarsrettV3;
using Xunit;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Tests.Services
{
    public class TiltakUtenAnsvarsrettV3Tests
    {
        private readonly TiltakUtenAnsvarsrettType _form;
        private TiltakUtenAnsvarsrettFormV3Validator _validator;
        private List<string> _formSetElements;
        private Skjema _formSkjema;

        public TiltakUtenAnsvarsrettV3Tests()
        {
            _formSkjema = new TiltakUtenAnsvarsrettV3Dfv45753()._01TiltakUtenAnsvarsrett(true);
            _form = TestHelper.GetValue<TiltakUtenAnsvarsrettType>(_formSkjema);
            _validator = new TiltakUtenAnsvarsrettFormV3Validator();

            _formSetElements = new List<string>()
            {
                "Situasjonsplan", "TegningEksisterendePlan","TegningNyPlan", "Nabomerknader", "Gjenpart nabovarsel",
                "KvitteringNabovarsel", "TegningEksisterendeSnitt","TegningNyttSnitt", "TegningEksisterendeFasade", "TegningNyFasade"
            };
        }
        [Fact(DisplayName = "4373.2.3  MinstEtTiltak - OK")]
        public void form_3_MinstEtTiltakOKTest()
        {
            _validator.MinstEtTiltak(_form, _formSetElements);
            var result = _validator.GetResult();
            result.messages.Count.Should().Be(0);
        }

        [Fact(DisplayName = "4373.2.3  MinstEtTiltak - ERROR", Skip = "just if all properties are empty")]
        public void form_3_MinstEtTiltakERRORTest()
        {

            _form.beskrivelseAvTiltak = null;

            _validator.MinstEtTiltak(_form, _formSetElements);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4373.2.3").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4373.2.3.1  MinstEtTiltak - ERROR")]
        public void form_3_1_MinstEtTiltakERRORTest()
        {

            _form.beskrivelseAvTiltak.type = null;

            _validator.MinstEtTiltak(_form, _formSetElements);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4373.2.3.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4373.2.3.2  MinstEtTiltak - ERROR")]
        public void form_3_2_MinstEtTiltakERRORTest()
        {

            _form.beskrivelseAvTiltak.type.type.kodeverdi = null;
            _form.beskrivelseAvTiltak.type.type.kodebeskrivelse = "Nytt bygg - Under 70 m2 - ikke boligformål";

            _validator.MinstEtTiltak(_form, _formSetElements);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4373.2.3.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4373.2.3.2 (1) MinstEtTiltak - ERROR")]
        public void form_3_2_a_MinstEtTiltakERRORTest()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.beskrivelseAvTiltak.type.type.kodeverdi = "*nyttbyggunder70m2";
            _form.beskrivelseAvTiltak.type.type.kodebeskrivelse = "Nytt bygg - Under 70 m2 - ikke boligformål";

            _validator.MinstEtTiltak(_form, _formSetElements);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4373.2.3.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4373.2.3.3  MinstEtTiltak - ERROR")]
        public void form_3_3_MinstEtTiltakERRORTest()
        {

            _form.beskrivelseAvTiltak.type.type.kodeverdi = "nyttbyggunder70m2";
            _form.beskrivelseAvTiltak.type.type.kodebeskrivelse = null;

            var jsonFile = TestHelper.GetValidateFormv2FormSkjema(_formSkjema, _formSetElements, _form);
            _validator.MinstEtTiltak(_form, _formSetElements);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4373.2.3.3").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4373.2.3.3.1  MinstEtTiltak - ERROR")]
        public void form_3_3_1_MinstEtTiltakERRORTest()
        {

            _form.beskrivelseAvTiltak.type.type.kodeverdi = "nyttbyggunder70m2";
            _form.beskrivelseAvTiltak.type.type.kodebeskrivelse = "*Nytt bygg - Under 70 m2 - ikke boligformål";

            _validator.MinstEtTiltak(_form, _formSetElements);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4373.2.3.3.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4373.2.3.7  MinstEtTiltak - ERROR")]
        public void form_3_7_MinstEtTiltakERRORTest()
        {

            _form.beskrivelseAvTiltak.type.type.kodeverdi = "annet";
            _form.beskrivelseAvTiltak.type.type.kodebeskrivelse = "Annet - mindre tiltak på bebygd eiendom";

            _form.beskrivelseAvTiltak.type.beskrivelse = null;

            _validator.MinstEtTiltak(_form, _formSetElements);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4373.2.3.7").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4373.2.3.6  MinstEtTiltak - ERROR")]
        public void form_3_6_MinstEtTiltakERRORTest()
        {

            _form.beskrivelseAvTiltak.type.type.kodeverdi = "annet";
            _form.beskrivelseAvTiltak.type.type.kodebeskrivelse = "Annet - mindre tiltak på bebygd eiendom";

            _formSetElements = new List<string>() { "*Folgebrev" };

            _form.beskrivelseAvTiltak.foelgebrev = null;

            _validator.MinstEtTiltak(_form, _formSetElements);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4373.2.3.6").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4373.2.3.4  MinstEtTiltak - ERROR")]
        public void form_3_4_MinstEtTiltakERRORTest()
        {

            _form.beskrivelseAvTiltak.type.type.kodeverdi = "annet";
            _form.beskrivelseAvTiltak.type.type.kodebeskrivelse = "Annet - mindre tiltak på bebygd eiendom";

            _formSetElements = new List<string>() { "Folgebrev" };

            _form.beskrivelseAvTiltak.bruk.tiltaksformaal[0].kodeverdi = "*Bolig";
            _form.beskrivelseAvTiltak.bruk.tiltaksformaal[0].kodebeskrivelse = "Bolig";

            _validator.MinstEtTiltak(_form, _formSetElements);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4373.2.3.4").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4373.3.3.4.1  MinstEtTiltak - ERROR")]
        public void form_3_3_4_1_MinstEtTiltakERRORTest()
        {

            _form.beskrivelseAvTiltak.type.type.kodeverdi = "annet";
            _form.beskrivelseAvTiltak.type.type.kodebeskrivelse = "Annet - mindre tiltak på bebygd eiendom";

            _formSetElements = new List<string>() { "Folgebrev" };

            _form.beskrivelseAvTiltak.bruk.tiltaksformaal[0].kodeverdi = "Bolig";
            _form.beskrivelseAvTiltak.bruk.tiltaksformaal[0].kodebeskrivelse = null;

            _validator.MinstEtTiltak(_form, _formSetElements);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4373.3.3.4.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4373.3.3.4.2  MinstEtTiltak - ERROR")]
        public void form_3_3_4_2_MinstEtTiltakERRORTest()
        {

            _form.beskrivelseAvTiltak.type.type.kodeverdi = "annet";
            _form.beskrivelseAvTiltak.type.type.kodebeskrivelse = "Annet - mindre tiltak på bebygd eiendom";

            _formSetElements = new List<string>() { "Folgebrev" };

            _form.beskrivelseAvTiltak.bruk.tiltaksformaal[0].kodeverdi = "Bolig";
            _form.beskrivelseAvTiltak.bruk.tiltaksformaal[0].kodebeskrivelse = "*Bolig";

            _validator.MinstEtTiltak(_form, _formSetElements);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4373.3.3.4.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4373.1.3.5  MinstEtTiltak - ERROR")]
        public void form_3_5_MinstEtTiltakERRORTest()
        {

            _form.beskrivelseAvTiltak.bruk.tiltaksformaal[0].kodeverdi = "Annet";
            _form.beskrivelseAvTiltak.bruk.tiltaksformaal[0].kodebeskrivelse = "Annet";

            _form.beskrivelseAvTiltak.bruk.beskrivPlanlagtFormaal = null;


            _validator.MinstEtTiltak(_form, _formSetElements);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4373.2.3.5").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4373.2.4 EiendomByggestedValidation - OK")]
        public void form_4528_2_4_EiendomByggestedValidationOkTest()
        {
            _validator._municipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");
            _validator._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, true);

            _validator.EiendomByggestedValidation(_form);
            var result = _validator.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4373.2.4 EiendomByggestedValidation - Error")]
        public void form_4_EiendomByggestedValidationErrorTest()
        {
            _validator._municipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");
            _validator._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, true);

            _form.eiendomByggested = null;

            _validator.EiendomByggestedValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4373.2.4").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4373.2.4.1 EiendomByggestedValidation - Error")]
        public void form_4_1_EiendomByggestedValidationErrorTest()
        {
            _validator._municipalityValidator = TestHelper.MokMunicipalityValidatorResult("empty");
            _validator._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, true);

            _validator.EiendomByggestedValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4373.2.4.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4373.2.4.2 EiendomByggestedValidation - Error")]
        public void form_4_2_EiendomByggestedValidationErrorTest()
        {
            _validator._municipalityValidator = TestHelper.MokMunicipalityValidatorResult("Invalid");
            _validator._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, true);

            _validator.EiendomByggestedValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4373.2.4.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4373.2.4.3 EiendomByggestedValidation - Error")]
        public void form_4_3_EiendomByggestedValidationErrorTest()
        {
            _validator._municipalityValidator = TestHelper.MokMunicipalityValidatorResult("Expired");
            _validator._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, true);

            _validator.EiendomByggestedValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4373.2.4.3").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4373.2.4.4 EiendomByggestedValidation - Error")]
        public void form_4_4_EiendomByggestedValidationErrorTest()
        {
            _validator._municipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");
            _validator._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, true);

            _form.eiendomByggested[0].eiendomsidentifikasjon.gaardsnummer = null;

            _validator.EiendomByggestedValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4373.2.4.4").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4373.2.4.4.1 EiendomByggestedValidation - Error")]
        public void form_4_4_1_EiendomByggestedValidationErrorTest()
        {
            _validator._municipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");
            _validator._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, true);

            _form.eiendomByggested[0].eiendomsidentifikasjon.bruksnummer = null;

            _validator.EiendomByggestedValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4373.2.4.4.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4373.3.4.5 EiendomByggestedValidation - Error")]
        public void form_3_4_5_EiendomByggestedValidationErrorTest()
        {
            _validator._municipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");
            _validator._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, true);

            _form.eiendomByggested[0].eiendomsidentifikasjon.gaardsnummer = "-1";

            _validator.EiendomByggestedValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4373.3.4.5").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4373.3.4.6 EiendomByggestedValidation - Error")]
        public void form_3_4_6_EiendomByggestedValidationErrorTest()
        {
            _validator._municipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");
            _validator._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, true);

            _form.eiendomByggested[0].eiendomsidentifikasjon.bruksnummer = "-1";

            _validator.EiendomByggestedValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4373.3.4.6").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4373.2.4.12 EiendomByggestedValidation - Error")]
        public void form_4_12_EiendomByggestedValidationErrorTest()
        {
            _validator._municipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");
            _validator._matrikkelService = TestHelper.MokMatrikkelServiceResult(false, true, true);

            _validator.EiendomByggestedValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4373.2.4.12").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4373.2.4.13 EiendomByggestedValidation - Error")]
        public void form_4_13_EiendomByggestedValidationErrorTest()
        {
            _validator._municipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");
            _validator._matrikkelService = TestHelper.MokMatrikkelServiceResult(null, true, true);

            _validator.EiendomByggestedValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4373.2.4.13").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4373.2.4.7 EiendomByggestedValidation - Error")]
        public void form_4_7_EiendomByggestedValidationErrorTest()
        {
            _validator._municipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");
            _validator._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, true);

            _form.eiendomByggested[0].bygningsnummer = "12345678a";

            _validator.EiendomByggestedValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4373.2.4.7").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4373.2.4.8 EiendomByggestedValidation - Error")]
        public void form_4_8_EiendomByggestedValidationErrorTest()
        {
            _validator._municipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");
            _validator._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, true);

            _form.eiendomByggested[0].bygningsnummer = "0";

            _validator.EiendomByggestedValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4373.2.4.8").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4373.2.4.8 (1) EiendomByggestedValidation - Error")]
        public void form_4_8_a_EiendomByggestedValidationErrorTest()
        {
            _validator._municipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");
            _validator._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, true);

            _form.eiendomByggested[0].bygningsnummer = "-1";

            _validator.EiendomByggestedValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4373.2.4.8").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4373.2.4.14 EiendomByggestedValidation - Error")]
        public void form_4_14_EiendomByggestedValidationErrorTest()
        {
            _validator._municipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");
            _validator._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, false, true);

            _validator.EiendomByggestedValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4373.2.4.14").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4373.2.4.9 EiendomByggestedValidation - Error")]
        public void form_4_9_EiendomByggestedValidationErrorTest()
        {
            _validator._municipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");
            _validator._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, true);

            _form.eiendomByggested[0].adresse = null;

            _validator.EiendomByggestedValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4373.2.4.9").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4373.2.4.9.1 EiendomByggestedValidation - Error")]
        public void form_4_9_1_EiendomByggestedValidationErrorTest()
        {
            _validator._municipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");
            _validator._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, true);

            _form.eiendomByggested[0].adresse.adresselinje1 = null;

            _validator.EiendomByggestedValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4373.2.4.9.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4373.2.4.9.2 EiendomByggestedValidation - Error")]
        public void form_4_9_2_EiendomByggestedValidationErrorTest()
        {
            _validator._municipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");
            _validator._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, true);

            _form.eiendomByggested[0].adresse.gatenavn = null;

            _validator.EiendomByggestedValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4373.2.4.9.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4373.2.4.9.2 (1) EiendomByggestedValidation - Error")]
        public void form_4_9_2_a_EiendomByggestedValidationErrorTest()
        {
            _validator._municipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");
            _validator._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, true);

            _form.eiendomByggested[0].adresse.husnr = null;

            _validator.EiendomByggestedValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4373.2.4.9.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4373.2.4.15 EiendomByggestedValidation - Error")]
        public void form_4_15_EiendomByggestedValidationErrorTest()
        {
            _validator._municipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");
            _validator._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, null);

            _validator.EiendomByggestedValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4373.2.4.15").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4373.2.4.15.1 EiendomByggestedValidation - Error")]
        public void form_4_15_1_EiendomByggestedValidationErrorTest()
        {
            _validator._municipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");
            _validator._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, false);

            _validator.EiendomByggestedValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4373.2.4.15.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4373.2.4.10 EiendomByggestedValidation - Error")]
        public void form_4_10_EiendomByggestedValidationErrorTest()
        {
            _validator._municipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");
            _validator._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, true);

            _form.eiendomByggested[0].kommunenavn = null;

            _validator.EiendomByggestedValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4373.2.4.10").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4373.2.4.11 EiendomByggestedValidation - Error")]
        public void form_4_11_EiendomByggestedValidationErrorTest()
        {
            _validator._municipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");
            _validator._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, true);

            _form.eiendomByggested[0].bolignummer = "*H0101";

            _validator.EiendomByggestedValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4373.2.4.11").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4373.2.5.1 TiltakshaverValidation - ok")]
        public void form_5_1_TiltakshaverValidationOkTest()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "OSLO");

            _validator.TiltakshaverValidation(_form);
            var result = _validator.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4373.2.5.1 TiltakshaverValidation - Error")]
        public void form_5_1_TiltakshaverValidationErrorTest()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "OSLO");

            _form.tiltakshaver.partstype = null;

            _validator.TiltakshaverValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4373.2.5.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4373.2.5.2 TiltakshaverValidation - Error")]
        public void form_5_2_TiltakshaverValidationErrorTest()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "OSLO");

            _form.tiltakshaver.partstype.kodeverdi = null;
            _form.tiltakshaver.partstype.kodebeskrivelse = "Privatperson";

            _validator.TiltakshaverValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4373.2.5.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4373.2.5.3 TiltakshaverValidation - Error")]
        public void form_5_3_TiltakshaverValidationErrorTest()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "OSLO");

            _form.tiltakshaver.partstype.kodeverdi = "*Privatperson";
            _form.tiltakshaver.partstype.kodebeskrivelse = "Privatperson";

            _validator.TiltakshaverValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4373.2.5.3").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4373.2.5.4 TiltakshaverValidation - Error")]
        public void form_5_4_TiltakshaverValidationErrorTest()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "OSLO");

            _form.tiltakshaver.partstype.kodeverdi = "Privatperson";
            _form.tiltakshaver.partstype.kodebeskrivelse = "Privatperson";

            _form.tiltakshaver.foedselsnummer = null;

            _validator.TiltakshaverValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4373.2.5.4").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4373.2.5.4.1 TiltakshaverValidation - Error")]
        public void form_5_4_1_TiltakshaverValidationErrorTest()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "OSLO");

            _form.tiltakshaver.partstype.kodeverdi = "Privatperson";
            _form.tiltakshaver.partstype.kodebeskrivelse = "Privatperson";

            _form.tiltakshaver.foedselsnummer = "mas/bdjasbdhjabjhababsjhfbsjhfbsadjhfasjhfasdgasasasdbsdfSVBDFU";

            _validator.TiltakshaverValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4373.2.5.4.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4373.2.5.4.2 TiltakshaverValidation - Error")]
        public void form_5_4_2_TiltakshaverValidationErrorTest()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "OSLO");

            _form.tiltakshaver.partstype.kodeverdi = "Privatperson";
            _form.tiltakshaver.partstype.kodebeskrivelse = "Privatperson";

            _form.tiltakshaver.foedselsnummer = "25525401530";

            _validator.TiltakshaverValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4373.2.5.4.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4373.2.5.4.3 TiltakshaverValidation - Error")]
        public void form_5_4_3_TiltakshaverValidationErrorTest()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "OSLO");

            _form.tiltakshaver.partstype.kodeverdi = "Privatperson";
            _form.tiltakshaver.partstype.kodebeskrivelse = "Privatperson";

            _form.tiltakshaver.foedselsnummer = "5125401530";

            _validator.TiltakshaverValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4373.2.5.4.3").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4373.2.5.5 TiltakshaverValidation - Error")]
        public void form_5_5_TiltakshaverValidationErrorTest()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "OSLO");

            _form.tiltakshaver.partstype.kodeverdi = "Organisasjon";
            _form.tiltakshaver.partstype.kodebeskrivelse = "Organisasjon";

            _form.tiltakshaver.organisasjonsnummer = null;

            _validator.TiltakshaverValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4373.2.5.5").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4373.2.5.5.1 TiltakshaverValidation - Error")]
        public void form_5_5_1_TiltakshaverValidationErrorTest()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "OSLO");

            _form.tiltakshaver.partstype.kodeverdi = "Organisasjon";
            _form.tiltakshaver.partstype.kodebeskrivelse = "Organisasjon";

            _form.tiltakshaver.organisasjonsnummer = "55974760673";

            _validator.TiltakshaverValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4373.2.5.5.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4373.2.5.5.2 TiltakshaverValidation - Error")]
        public void form_5_5_2_TiltakshaverValidationErrorTest()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "OSLO");

            _form.tiltakshaver.partstype.kodeverdi = "Organisasjon";
            _form.tiltakshaver.partstype.kodebeskrivelse = "Organisasjon";

            _form.tiltakshaver.organisasjonsnummer = "557476068";

            _validator.TiltakshaverValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4373.2.5.5.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4373.3.5.12 TiltakshaverValidation - Error")]
        public void form_5_12_TiltakshaverValidationErrorTest()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "OSLO");

            _form.tiltakshaver.kontaktperson = null;

            _validator.TiltakshaverValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4373.3.5.12").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4373.3.5.12.1 TiltakshaverValidation - Error")]
        public void form_5_12_1_TiltakshaverValidationErrorTest()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "OSLO");

            _form.tiltakshaver.kontaktperson.navn = null;

            _validator.TiltakshaverValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4373.3.5.12.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4373.3.5.12.2 TiltakshaverValidation - Error")]
        public void form_5_12_2_TiltakshaverValidationErrorTest()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "OSLO");

            _form.tiltakshaver.kontaktperson.telefonnummer = null;
            _form.tiltakshaver.kontaktperson.mobilnummer = null;

            _validator.TiltakshaverValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4373.3.5.12.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4373.3.5.12.3 TiltakshaverValidation - Error")]
        public void form_5_12_3_TiltakshaverValidationErrorTest()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "OSLO");

            _form.tiltakshaver.kontaktperson.telefonnummer = "555 123456";

            _validator.TiltakshaverValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4373.3.5.12.3").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4373.3.5.12.4 TiltakshaverValidation - Error")]
        public void form_5_12_4_TiltakshaverValidationErrorTest()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "OSLO");

            _form.tiltakshaver.kontaktperson.mobilnummer = "555 123456";

            _validator.TiltakshaverValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4373.3.5.12.4").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4373.3.5.12.5 TiltakshaverValidation - Error")]
        public void form_5_12_5_TiltakshaverValidationErrorTest()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "OSLO");

            _form.tiltakshaver.kontaktperson.epost = null;

            _validator.TiltakshaverValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4373.3.5.12.5").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4373.2.5.6 TiltakshaverValidation - Error")]
        public void form_5_6_TiltakshaverValidationErrorTest()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "OSLO");

            _form.tiltakshaver.adresse = null;

            _validator.TiltakshaverValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4373.2.5.6").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4373.2.5.6.1 TiltakshaverValidation - Error")]
        public void form_5_6_1_TiltakshaverValidationErrorTest()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "OSLO");

            _form.tiltakshaver.adresse.adresselinje1 = null;

            _validator.TiltakshaverValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4373.2.5.6.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4373.2.5.6.2 TiltakshaverValidation - Error")]
        public void form_5_6_2_TiltakshaverValidationErrorTest()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "OSLO");

            _form.tiltakshaver.adresse.landkode = "*AR";

            _validator.TiltakshaverValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4373.2.5.6.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4373.2.5.6.3 TiltakshaverValidation - Error")]
        public void form_5_6_3_TiltakshaverValidationErrorTest()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "OSLO");

            _form.tiltakshaver.adresse.postnr = null;

            _validator.TiltakshaverValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4373.2.5.6.3").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4373.2.5.6.4 TiltakshaverValidation - Error")]
        public void form_5_6_4_TiltakshaverValidationErrorTest()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "OSLO");

            _form.tiltakshaver.adresse.postnr = "*3801";

            _validator.TiltakshaverValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4373.2.5.6.4").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4373.2.5.6.5 TiltakshaverValidation - Error")]
        public void form_5_6_5_TiltakshaverValidationErrorTest()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(false, "POBOX", "OSLO");

            _validator.TiltakshaverValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4373.2.5.6.5").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4373.2.5.6.6 TiltakshaverValidation - Error")]
        public void form_5_6_6_TiltakshaverValidationErrorTest()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "*OSLO");

            _validator.TiltakshaverValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4373.2.5.6.6").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4373.2.5.6.7 TiltakshaverValidation - Error")]
        public void form_5_6_7_TiltakshaverValidationErrorTest()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(null);

            _validator.TiltakshaverValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4373.2.5.6.7").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4373.2.5.7 TiltakshaverValidation - Error")]
        public void form_5_7_TiltakshaverValidationErrorTest()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "OSLO");

            _form.tiltakshaver.navn = null;

            _validator.TiltakshaverValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4373.2.5.7").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4373.2.5.8 TiltakshaverValidation - Error")]
        public void form_5_8_TiltakshaverValidationErrorTest()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "OSLO");

            _form.tiltakshaver.telefonnummer = null;
            _form.tiltakshaver.mobilnummer = null;

            _validator.TiltakshaverValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4373.2.5.8").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4373.3.5.9 TiltakshaverValidation - Error")]
        public void form_5_9_TiltakshaverValidationErrorTest()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "OSLO");

            _form.tiltakshaver.telefonnummer = "555 12345";

            _validator.TiltakshaverValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4373.3.5.9").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4373.3.5.10 TiltakshaverValidation - Error")]
        public void form_5_10_TiltakshaverValidationErrorTest()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "OSLO");

            _form.tiltakshaver.mobilnummer = "555 12345";

            _validator.TiltakshaverValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4373.3.5.10").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4373.2.5.11 TiltakshaverValidation - Error")]
        public void form_5_11_TiltakshaverValidationErrorTest()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "OSLO");

            _form.tiltakshaver.epost = null;

            _validator.TiltakshaverValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4373.2.5.11").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }


        [Fact(DisplayName = "4373.2.5.8 GjeldendePlanValidering - Ok")]
        public void form_2_8_4_GjeldendePlanValideringOkTest()
        {
            _validator.GjeldendePlanValidering(_form, _formSetElements);
            var result = _validator.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4373.2.8.4 GjeldendePlanValidering - Error")]
        public void form_2_8_4_GjeldendePlanValideringErrorTest()
        {
            _form.rammebetingelser.plan.gjeldendePlan.plantype = null;


            _validator.GjeldendePlanValidering(_form, _formSetElements);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4373.2.8.4").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4373.3.8.4.2.1 GjeldendePlanValidering - Error")]
        public void form_3_8_4_2_1_GjeldendePlanValideringErrorTest()
        {
            _form.rammebetingelser.plan.gjeldendePlan.plantype.kodebeskrivelse = "null";


            _validator.GjeldendePlanValidering(_form, _formSetElements);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4373.3.8.4.2.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4373.2.8.4.3.3 GjeldendePlanValidering - Error")]
        public void form_2_8_4_3_3_GjeldendePlanValideringErrorTest()
        {
            _form.rammebetingelser.plan.gjeldendePlan.beregningsregelGradAvUtnytting.kodeverdi = "annetUdef";

            _formSetElements = new List<string>() { "*UnderlagUtnytting" };

            _validator.GjeldendePlanValidering(_form, _formSetElements);
            var result = _validator.GetResult();

            var postmanJson = TestHelper.GetValidateFormv2FormSkjema(_formSkjema, _formSetElements, _form);

            result.messages.Where(m => m.reference == "4373.2.8.4.3.3").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4373.2.8.4.3.3 (1) GjeldendePlanValidering - OK")]
        public void form_2_8_4_3_3_a_GjeldendePlanValideringOKTest()
        {
            _form.rammebetingelser.plan.gjeldendePlan.beregningsregelGradAvUtnytting.kodeverdi = "annet";

            _formSetElements = new List<string>() { "*UnderlagUtnytting" };
            var postmanJson = TestHelper.GetValidateFormv2FormSkjema(_formSkjema, _formSetElements, _form);
            _validator.GjeldendePlanValidering(_form, _formSetElements);
            var result = _validator.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4373.3.18.1 ArealdisponeringValidering - OK")]
        public void form_3_6_5_ArealdisponeringValideringOKTest()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

           _form.beskrivelseAvTiltak.type.type.kodeverdi = tiltakstype;
           _form.beskrivelseAvTiltak.type.type.kodebeskrivelse = "Nytt bygg - Under 70 m2 - ikke boligformål";

            _validator.ArealdisponeringValidering(_form);
            var result = _validator.GetResult();

            result.messages.Count.Should().Be(0);
        }

        [Fact(DisplayName = "4373.3.18.1.1 ArealdisponeringValidering - error")]
        public void form_3_6_5_11_ArealdisponeringValideringErrorTest()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

           _form.beskrivelseAvTiltak.type.type.kodeverdi = tiltakstype;
           _form.beskrivelseAvTiltak.type.type.kodebeskrivelse = "Nytt bygg - Under 70 m2 - ikke boligformål";

            _form.rammebetingelser.plan.gjeldendePlan.beregningsregelGradAvUtnytting.kodeverdi = "BYA";
            _form.rammebetingelser.plan.gjeldendePlan.beregningsregelGradAvUtnytting.kodebeskrivelse = "Bebygd areal";

            _form.rammebetingelser.arealdisponering = null;

            _validator.ArealdisponeringValidering(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4373.3.18.1.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4373.3.18.1 ArealdisponeringValidering - error")]
        public void form_3_18_ArealdisponeringValideringErrorTest()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

           _form.beskrivelseAvTiltak.type.type.kodeverdi = tiltakstype;
           _form.beskrivelseAvTiltak.type.type.kodebeskrivelse = "Nytt bygg - Under 70 m2 - ikke boligformål";

            _form.rammebetingelser.arealdisponering.arealBebyggelseEksisterende = null;
            _form.rammebetingelser.arealdisponering.arealBebyggelseEksisterendeSpecified = false;

            _validator.ArealdisponeringValidering(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4373.3.18.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4373.3.18.2ArealdisponeringValidering - error")]
        public void form_3_18_2_ArealdisponeringValideringErrorTest()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

           _form.beskrivelseAvTiltak.type.type.kodeverdi = tiltakstype;
           _form.beskrivelseAvTiltak.type.type.kodebeskrivelse = "Nytt bygg - Under 70 m2 - ikke boligformål";

            _form.rammebetingelser.arealdisponering.arealBebyggelseEksisterende = -89.2;

            _validator.ArealdisponeringValidering(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4373.3.18.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4373.3.18.3 ArealdisponeringValidering - error")]
        public void form_3_18_3_ArealdisponeringValideringErrorTest()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

           _form.beskrivelseAvTiltak.type.type.kodeverdi = tiltakstype;
           _form.beskrivelseAvTiltak.type.type.kodebeskrivelse = "Nytt bygg - Under 70 m2 - ikke boligformål";

            _form.rammebetingelser.arealdisponering.arealBebyggelseNytt = -16.3;

            _validator.ArealdisponeringValidering(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4373.3.18.3").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4373.3.18.4 ArealdisponeringValidering - error")]
        public void form_3_18_4_ArealdisponeringValideringErrorTest()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

           _form.beskrivelseAvTiltak.type.type.kodeverdi = tiltakstype;
           _form.beskrivelseAvTiltak.type.type.kodebeskrivelse = "Nytt bygg - Under 70 m2 - ikke boligformål";

            _form.rammebetingelser.arealdisponering.arealBebyggelseSomSkalRives = -8.4;

            _validator.ArealdisponeringValidering(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4373.3.18.4").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4373.3.18.5 ArealdisponeringValidering - error")]
        public void form_3_18_5_ArealdisponeringValideringErrorTest()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

           _form.beskrivelseAvTiltak.type.type.kodeverdi = tiltakstype;
           _form.beskrivelseAvTiltak.type.type.kodebeskrivelse = "Nytt bygg - Under 70 m2 - ikke boligformål";

            _form.rammebetingelser.arealdisponering.parkeringsarealTerreng = -45.2;

            _validator.ArealdisponeringValidering(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4373.3.18.5").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4373.3.18.6 ArealdisponeringValidering - error")]
        public void form_3_18_6_ArealdisponeringValideringErrorTest()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

           _form.beskrivelseAvTiltak.type.type.kodeverdi = tiltakstype;
           _form.beskrivelseAvTiltak.type.type.kodebeskrivelse = "Nytt bygg - Under 70 m2 - ikke boligformål";

            _form.rammebetingelser.arealdisponering.beregnetGradAvUtnytting = -142.3;

            _validator.ArealdisponeringValidering(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4373.3.18.6").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4373.3.18.7 ArealdisponeringValidering - error")]
        public void form_3_18_7_ArealdisponeringValideringErrorTest()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

           _form.beskrivelseAvTiltak.type.type.kodeverdi = tiltakstype;
           _form.beskrivelseAvTiltak.type.type.kodebeskrivelse = "Nytt bygg - Under 70 m2 - ikke boligformål";
           
           _form.rammebetingelser.plan.gjeldendePlan.beregningsregelGradAvUtnytting.kodeverdi = "BYA";
           _form.rammebetingelser.plan.gjeldendePlan.beregningsregelGradAvUtnytting.kodebeskrivelse = "Bebygd areal";

            _form.rammebetingelser.arealdisponering.beregnetGradAvUtnytting = 142.4;

            _validator.ArealdisponeringValidering(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4373.3.18.7").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4373.3.18.8 ArealdisponeringValidering - error")]
        public void form_3_18_8_ArealdisponeringValideringErrorTest()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.beskrivelseAvTiltak.type.type.kodeverdi = tiltakstype;
            _form.beskrivelseAvTiltak.type.type.kodebeskrivelse = "Nytt bygg - Under 70 m2 - ikke boligformål";

            _form.rammebetingelser.plan.gjeldendePlan.beregningsregelGradAvUtnytting.kodeverdi = "%BYA";
            _form.rammebetingelser.plan.gjeldendePlan.beregningsregelGradAvUtnytting.kodebeskrivelse = "Prosent bebygd areal";
            _form.rammebetingelser.arealdisponering.tomtearealBeregnet = null;
            _form.rammebetingelser.arealdisponering.tomtearealBeregnetSpecified = false;

            _validator.ArealdisponeringValidering(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4373.3.18.8").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4373.3.18.9 ArealdisponeringValidering - error")]
        public void form_3_18_9_ArealdisponeringValideringErrorTest()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

           _form.beskrivelseAvTiltak.type.type.kodeverdi = tiltakstype;
           _form.beskrivelseAvTiltak.type.type.kodebeskrivelse = "Nytt bygg - Under 70 m2 - ikke boligformål";

            _form.rammebetingelser.plan.gjeldendePlan.beregningsregelGradAvUtnytting.kodeverdi = "%BYA";
            _form.rammebetingelser.plan.gjeldendePlan.beregningsregelGradAvUtnytting.kodebeskrivelse = "Prosent bebygd areal";
            _form.rammebetingelser.arealdisponering.tomtearealBeregnet = 0;

            _validator.ArealdisponeringValidering(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4373.3.18.9").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4373.3.18.10 ArealdisponeringValidering - error")]
        public void form_3_18_10_ArealdisponeringValideringErrorTest()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

           _form.beskrivelseAvTiltak.type.type.kodeverdi = tiltakstype;
           _form.beskrivelseAvTiltak.type.type.kodebeskrivelse = "Nytt bygg - Under 70 m2 - ikke boligformål";

            _form.rammebetingelser.plan.gjeldendePlan.beregningsregelGradAvUtnytting.kodeverdi = "%BYA";
            _form.rammebetingelser.plan.gjeldendePlan.beregningsregelGradAvUtnytting.kodebeskrivelse = "Prosent bebygd areal";
            _form.rammebetingelser.arealdisponering.tomtearealBeregnet = -24.1;

            _validator.ArealdisponeringValidering(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4373.3.18.10").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4373.3.18.11 ArealdisponeringValidering - error")]
        public void form_3_18_11_ArealdisponeringValideringErrorTest()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

           _form.beskrivelseAvTiltak.type.type.kodeverdi = tiltakstype;
           _form.beskrivelseAvTiltak.type.type.kodebeskrivelse = "Nytt bygg - Under 70 m2 - ikke boligformål";

            _form.rammebetingelser.plan.gjeldendePlan.beregningsregelGradAvUtnytting.kodeverdi = "%BYA";
            _form.rammebetingelser.plan.gjeldendePlan.beregningsregelGradAvUtnytting.kodebeskrivelse = "Prosent bebygd areal";
            _form.rammebetingelser.arealdisponering.beregnetGradAvUtnytting = 142.3; // '590.45643153527'

            _validator.ArealdisponeringValidering(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4373.3.18.11").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

    }
}
