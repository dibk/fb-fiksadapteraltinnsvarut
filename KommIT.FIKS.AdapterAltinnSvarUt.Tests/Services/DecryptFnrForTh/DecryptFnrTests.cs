﻿using FluentAssertions;
using KommIT.FIKS.AdapterAltinnSvarUt.Services;
using Moq;
using Xunit;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Tests.Services.DecryptFnrForTh
{
    public class DecryptFnrTests
    {
        [Fact]
        public void GenerateAttachment()
        {
            string ar = "AR123456789";
            
            var thDecryptMockService = new Mock<ITiltakshaverSamtykkeService>();

            var decryptor = new AdapterAltinnSvarUt.Services.DecryptFnrForTh.DecryptFnr(thDecryptMockService.Object);

            var attach = decryptor.GenerateAttachment("23456723456", ar);
            attach.Should().NotBe(null);
            attach.ArchiveReference.Should().Be(ar);
        }

        //[Fact]
        //public void DecryptFnrForTh()
        //{
        //    string ar = "AR7278613";
        //    Guid myGuid = Guid.Parse("C08B1E89-E9FA-413F-92C4-C5F4175FD199");

        //    var textEncrypt = new TextEncryptionService();
        //    var debCont = new ApplicationDbContext();

        //    var thDecryptMockService = new TiltakshaverSamtykkeService(debCont, textEncrypt);

        //    var decryptor = new AdapterAltinnSvarUt.Services.DecryptFnrForTh.DecryptFnr(thDecryptMockService);
        //    var attach = decryptor.DecryptFnrForTh(myGuid, ar);

        //    attach.Should().NotBe(null);
        //    attach.ArchiveReference.Should().Be(ar);
        //}



    }
}
