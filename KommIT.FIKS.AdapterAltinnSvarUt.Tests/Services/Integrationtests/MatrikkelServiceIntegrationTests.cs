﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using FluentAssertions;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Matrikkel;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Tests.Services.Integrationtests
{
    public class MatrikkelServiceIntegrationTests
    {
        [Fact(Skip = "Integrasjonstest")]
        public void MatrikkelnrOKTest()
        {
            var matrikkel = new MatrikkelService();
            matrikkel.MatrikkelnrExist("0101", 1, 2, 0, 0).Should().BeTrue();

        }

        [Fact(Skip = "Integrasjonstest")]
        public void MatrikkelnrFeilTest()
        {
            var matrikkel = new MatrikkelService();
            matrikkel.MatrikkelnrExist("0101", 1, 2, 0, 99).Should().BeFalse();

        }

        [Fact(Skip = "Integrasjonstest")]
        public void MatrikkelnrAdresseOkTest()
        {
            var matrikkel = new MatrikkelService();
            matrikkel.IsVegadresseOnMatrikkelnr("Svingen", "25", "", "0821", 49, 130, 0, 0).Should().BeTrue();
            // Matrikkelen finner ikke denne adressen, så vår funksjonen returnerer false, men vi får ikke exception fra matrikkelen.
        }

        [Fact(Skip = "Integrasjonstest")]
        public void MatrikkelnrAdresseFeilTest()
        {
            var matrikkel = new MatrikkelService();
            matrikkel.IsVegadresseOnMatrikkelnr("Svingen", "23", "", "0821", 49, 130, 0, 0).Should().BeFalse();

        }

        [Fact(Skip = "Integrasjonstest")]
        public void MatrikkelnrAdresse2FeilTest()
        {
            var matrikkel = new MatrikkelService();
            matrikkel.IsVegadresseOnMatrikkelnr("Svingingen", "1", "", "0821", 49, 130, 0, 0).Should().BeFalse();

        }

        [Fact(Skip = "Integrasjonstest")]
        public void MatrikkelnrAdresse3FeilTest()
        {
            var matrikkel = new MatrikkelService();
            matrikkel.IsVegadresseOnMatrikkelnr("Storgata", "", "", "0821", 49, 130, 0, 0).Should().BeFalse();
            // Should this BeNull() ? Test result in a exception

        }
        [Fact(Skip = "Integrasjonstest")]
        public void MatrikkelnrBygningOkTest()
        {
            var matrikkel = new MatrikkelService();
            matrikkel.IsBygningOnMatrikkelnr(165711807, "0821", 49, 130, 0, 0).Should().BeTrue();

        }
        [Fact(Skip = "Integrasjonstest")]
        public void MatrikkelnrBygningFeilTest()
        {
            var matrikkel = new MatrikkelService();
            matrikkel.IsBygningOnMatrikkelnr(123456, "0821", 49, 130, 0, 0).Should().BeFalse();

        }

    }
}