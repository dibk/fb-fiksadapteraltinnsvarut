using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.SearchEngine;
using Serilog;
using Serilog.Events;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Tests.Services.Integrationtests
{
    public class SearchEngineIntegrationTest
    {
        public SearchEngineIntegrationTest(ITestOutputHelper output)
        {
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Verbose()
                .Enrich.FromLogContext()
                .WriteTo.TestOutput(output, LogEventLevel.Debug,
                    "{Timestamp:HH:mm:ss.fff} {SourceContext} [{Level}] {ArchiveReference} {Message}{NewLine}{Exception}")
                .CreateLogger()
                .ForContext<SearchEngineIntegrationTest>();
        }

        [Fact(Skip = "integration test")]
        public async Task ShouldIndexLogEntriesInBatches()
        {
            var dbContext = new ApplicationDbContext();
            var elasticIndexer = new ElasticIndexer();
            await new BatchIndexer(dbContext, elasticIndexer).RunIndexingOfLogEntries();
        }

        [Fact(Skip = "integration test")]
        public async Task ShouldIndexMetadataInBatches()
        {
            var dbContext = new ApplicationDbContext();
            var elasticIndexer = new ElasticIndexer();
            await new BatchIndexer(dbContext, elasticIndexer).RunIndexingOfFormsMetadata();
        }
    }
}