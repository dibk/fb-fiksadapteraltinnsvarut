﻿using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Services;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Altinn;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Azure;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Notification;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Prefill;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.WS.AltinnPreFill;
using Moq;
using System;
using System.Collections.Generic;
using Xunit;
using Serilog;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Altinn.Correspondence;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Altinn.Prefill;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Tests.Services
{
    public class NotificationV2IntegrationTests
    {
        //[Fact(Skip = "Integrasjonstest")]
        //public void SendSignertAnsvarsrettOKTest()
        //{

        //    string formDataAsXml = System.IO.File.ReadAllText(@"Services\testskjema\5505_41797_ErklæringAnsvarsrett2.xml");
        //    FormData distributionServiceFormData = new FormData("AR123434", formDataAsXml);
        //    IAltinnForm nb = new DistribusjonErklaeringAnsvarsrettFormV2();
        //    nb.InitiateForm(formDataAsXml);
        //    distributionServiceFormData.Mainform = nb;
        //    distributionServiceFormData.MainFormPdf = CreateMainFormAttachment("AR123434");

        //    var logService = new Mock<ILogEntryService>();
        //    var formMetadataService = new Mock<IFormMetadataService>(); //returner guid
        //    formMetadataService.Setup(s => s.InsertDistributionForm(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(new DistributionForm() { Id = Guid.NewGuid() });
        //    var statusdownload = new Mock<IFileDownloadStatusService>();
        //    var filestorage = new Mock<IFileStorage>();
        //    var syncService = new Mock<ISyncRecordsOfCombinedDistributions>();

        //    IAltinnCorrespondenceService corrService = new AltinnCorrespondenceService(new Mock<ILogger>().Object, new CorrespondenceClientFactory(), new CorrespondencePolicyProvider(new CorrespondencePolicyConfigurationProvider()));

        //    NotificationServiceV2 notificationService = new NotificationServiceV2(corrService,logService.Object, formMetadataService.Object, filestorage.Object, statusdownload.Object, syncService.Object, new Mock<ILogger>().Object);

        //    notificationService.SendNotification(distributionServiceFormData);
                        
        //}
        
        //[Fact(Skip = "Integrasjonstest")]
        //public void SjekkLaasteFelterOKTest()
        //{
        //    //Fungerer ikke som tenkt
        //    IAltinnPrefillService _altinnPrefillService;
        //    _altinnPrefillService = new AltinnPrefillService(new Mock<ILogger>().Object, new PrefillClientFactory(), new PrefillPolicyProvider(new PrefillPolicyConfigurationProvider()));

        //    string formDataAsXml = System.IO.File.ReadAllText(@"Services\testskjema\5499_41789_svarpaanabovarsel1.xml");

        //    PrefillFormTaskBuilder _prefillFormTaskBuilder = new PrefillFormTaskBuilder();

        //    _prefillFormTaskBuilder.SetupPrefillFormTask("4699", 1, "10019000953", "sref", "sref", "Vigdis Vater", 14);
        //    _prefillFormTaskBuilder.AddPrefillForm("5499", 41789, formDataAsXml, "sref");
        //    _prefillFormTaskBuilder.AddPreFillIdentityField("/SvarPaaNabovarsel/nabo/adresse/adresselinje1", "Storgata 3");

        //    PrefillFormTask preFillData = _prefillFormTaskBuilder.Build();
        //    DateTime? dueDate = null;
            
        //    ReceiptExternal receiptExternal = _altinnPrefillService.SubmitAndInstantiatePrefilledForm(preFillData, dueDate);
            
        //}

        //TODO Test på for stort vedlegg

        private static List<Attachment> CreateAttachmentSample(string arkivreferanse)
        {
            var attachments = new List<Attachment>();
            attachments.Add(new Attachment(arkivreferanse, new byte[1], "application_pdf", "Nabomerknader", "Nabomerknader.pdf", false));
            return attachments;
        }
        private static Attachment CreateMainFormAttachment(string arkivreferanse)
        {
            return new Attachment(arkivreferanse, new byte[1], "application_pdf", "main form pdf", "Skjema.pdf", false);
        }
    }
}
