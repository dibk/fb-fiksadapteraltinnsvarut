﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.Builders;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.FormsSamples;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.SvarUtForm;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;
using no.kxml.skjema.dibk.etttrinnsoknadV3;
using Xunit;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Tests.Services
{
    public class EtttrinnsoknadFormV3ValidatorTests
    {
        private readonly EttTrinnType _form;
        private EtttrinnsoknadFormV3Validator _validator;
        private List<string> _formSetElements;
        private Skjema _formSkjema;

        public EtttrinnsoknadFormV3ValidatorTests()
        {
            _formSkjema = new EttTrinnV3Dfv45751()._01EttTrinnV3(true);
            _form = TestHelper.GetValue<EttTrinnType>(_formSkjema);
            _validator = new EtttrinnsoknadFormV3Validator();

            _formSetElements = new List<string>()
            {
                "Situasjonsplan", "TegningNyPlan", "TegningNyttSnitt", "TegningEksisterendeFasade",
                "Gjennomføringsplan", "Matrikkelopplysninger", "UttalelseKulturminnemyndighet", "Avkjoerselsplan", "ErklaeringAnsvarsrett","SoknadOmAnsvarsrettTiltaksklasse1"
            };
        }

        [Fact(DisplayName = "4528.2.3  MinstEtTiltak - OK")]
        public void form_4528_2_3_MinstEtTiltakOKTest()
        {
            _validator.MinstEtTiltak(_form);
            var result = _validator.GetResult();
            result.messages.Count.Should().Be(0);
        }

        [Fact(DisplayName = "4528.2.3  MinstEtTiltak - ERROR")]
        public void form_4528_2_3_MinstEtTiltakERRORTest()
        {

            _form.beskrivelseAvTiltak = null;

            _validator.MinstEtTiltak(_form);
            var result = _validator.GetResult();
            //var xmlwith = TestHelper.GetValidateFormv2FormSkjema(_formSkjema, _attachments, _form);
            result.messages.Where(m => m.reference == "4528.2.3").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.2.3.1  MinstEtTiltak - ERROR")]
        public void form_4528_2_3_1_MinstEtTiltakERRORTest()
        {

            _form.beskrivelseAvTiltak.type = null;

            _validator.MinstEtTiltak(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.3.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.2.3.2  MinstEtTiltak - ERROR")]
        public void form_4528_2_3_2_MinstEtTiltakERRORTest()
        {

            _form.beskrivelseAvTiltak.type[0].kodeverdi = null;

            _validator.MinstEtTiltak(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.3.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.2.3.2.1  MinstEtTiltak - ERROR")]
        public void form_4528_2_3_2_1_MinstEtTiltakERRORTest()
        {
            _form.beskrivelseAvTiltak.type[0].kodeverdi = "*tilbyggunder50m2";

            _validator.MinstEtTiltak(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.3.2.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.2.3.3  MinstEtTiltak - ERROR")]
        public void form_4528_2_3_3_MinstEtTiltakERRORTest()
        {

            _form.beskrivelseAvTiltak.type[0].kodebeskrivelse = null;

            _validator.MinstEtTiltak(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.3.3").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }


        [Fact(DisplayName = "4528.2.3.4  MinstEtTiltak - ERROR")]
        public void form_4528_2_3_4_MinstEtTiltakERRORTest()
        {

            _form.beskrivelseAvTiltak.type[0].kodebeskrivelse = "*Endring av bygg - utvendig - Tilbygg med samlet areal mindre enn 50 m2";

            _validator.MinstEtTiltak(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.3.4").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.2.3.5.1  MinstEtTiltak - ERROR")]
        public void form_4528_2_3_5_1_MinstEtTiltakERRORTest()
        {

            _form.beskrivelseAvTiltak.bruk.anleggstype.kodeverdi = "*andre";
            _validator.MinstEtTiltak(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.3.5.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4528.3.3.5.2  MinstEtTiltak - ERROR")]
        public void form_4528_3_3_5_2_MinstEtTiltakERRORTest()
        {

            _form.beskrivelseAvTiltak.bruk.anleggstype.kodebeskrivelse = null;
            _validator.MinstEtTiltak(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.3.5.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4528.3.3.5.3  MinstEtTiltak - ERROR")]
        public void form_4528_3_3_5_3_MinstEtTiltakERRORTest()
        {

            _form.beskrivelseAvTiltak.bruk.anleggstype.kodebeskrivelse = "*Andre";
            _validator.MinstEtTiltak(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.3.5.3").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }


        [Fact(DisplayName = "4528.2.3.6  MinstEtTiltak - ERROR")]
        public void form_4528_2_3_6_MinstEtTiltakERRORTest()
        {

            _form.beskrivelseAvTiltak.bruk.naeringsgruppe.kodeverdi = "*Y";

            _validator.MinstEtTiltak(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.3.6").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.2.3.7  MinstEtTiltak - ERROR")]
        public void form_4528_2_3_7_MinstEtTiltakERRORTest()
        {
            _form.beskrivelseAvTiltak.bruk.bygningstype.kodeverdi = "*161";

            _validator.MinstEtTiltak(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.3.7").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.2.3.8  MinstEtTiltak - ERROR")]
        public void form_4528_2_3_8_MinstEtTiltakERRORTest()
        {
            _form.beskrivelseAvTiltak.bruk.bygningstype.kodebeskrivelse = null;

            _validator.MinstEtTiltak(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.3.8").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.2.3.9  MinstEtTiltak - ERROR")]
        public void form_4528_2_3_9_MinstEtTiltakERRORTest()
        {
            _form.beskrivelseAvTiltak.bruk.bygningstype.kodebeskrivelse = "*Hytter, sommerhus og fritidsbygg";

            _validator.MinstEtTiltak(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.3.9").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.2.3.10  MinstEtTiltak - ERROR")]
        public void form_4528_2_3_10_MinstEtTiltakERRORTest()
        {
            _form.beskrivelseAvTiltak.bruk.tiltaksformaal[0].kodeverdi = "*Fritidsbolig";

            _validator.MinstEtTiltak(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.3.10").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4528.3.3.10.1  MinstEtTiltak - ERROR")]
        public void form_3_10_1_MinstEtTiltakERRORTest()
        {
            _form.beskrivelseAvTiltak.bruk.tiltaksformaal[0].kodeverdi = "Fritidsbolig";
            _form.beskrivelseAvTiltak.bruk.tiltaksformaal[0].kodebeskrivelse = null;

            _validator.MinstEtTiltak(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.3.10.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4528.3.3.10.2  MinstEtTiltak - ERROR")]
        public void form_3_10_2_MinstEtTiltakERRORTest()
        {
            _form.beskrivelseAvTiltak.bruk.tiltaksformaal[0].kodeverdi = "Fritidsbolig";
            _form.beskrivelseAvTiltak.bruk.tiltaksformaal[0].kodebeskrivelse = "*Fritidsbolig";

            _validator.MinstEtTiltak(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.3.10.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.2.3.11  MinstEtTiltak - ERROR")]
        public void form_4528_2_3_11_MinstEtTiltakERRORTest()
        {
            _form.beskrivelseAvTiltak.bruk.beskrivPlanlagtFormaal = null;

            _validator.MinstEtTiltak(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.3.11").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        //--

        [Fact(DisplayName = "4528.3.5 Foretak TiltakshaverValidation - Ok")]
        public void form_4528_3_5_Foretak_TiltakshaverValidationOkTest()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "OSLO");

            _validator.TiltakshaverValidation(_form);
            var result = _validator.GetResult();

            result.messages.Count.Should().Be(0);
        }

        [Fact(DisplayName = "4528.3.5 Foretak TiltakshaverValidation - Error")]
        public void form_4528_3_5_Foretak_TiltakshaverValidationErrorTest()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "OSLO");

            _form.tiltakshaver.partstype = null;

            _validator.TiltakshaverValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.5").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.1.5.1 Foretak TiltakshaverValidation - Error")]
        public void form_4528_1_5_1_Foretak_TiltakshaverValidationErrorTest()
        {

            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "OSLO");

            _form.tiltakshaver.partstype.kodeverdi = null;

            _validator.TiltakshaverValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.1.5.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.1.5.2 Foretak TiltakshaverValidation - Error")]
        public void form_4528_1_5_2_Foretak_TiltakshaverValidationErrorTest()
        {

            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "OSLO");

            _form.tiltakshaver.partstype.kodeverdi = "*Foretak";

            _validator.TiltakshaverValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.1.5.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.3.5.4 Foretak TiltakshaverValidation - Error")]
        public void form_4528_3_5_4_Foretak_TiltakshaverValidationErrorTest()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "OSLO");

            _form.tiltakshaver.organisasjonsnummer = null;

            _validator.TiltakshaverValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.5.4").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.3.5.4.1 Foretak TiltakshaverValidation - Error")]
        public void form_4528_3_5_4_1_Foretak_TiltakshaverValidationErrorTest()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "OSLO");

            _form.tiltakshaver.organisasjonsnummer = "557476068";

            _validator.TiltakshaverValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.5.4.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.3.5.4.2 Foretak TiltakshaverValidation - Error")]
        public void form_4528_3_5_4_2_Foretak_TiltakshaverValidationErrorTest()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "OSLO");

            _form.tiltakshaver.organisasjonsnummer = "55974760673";

            _validator.TiltakshaverValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.5.4.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.2.5.4.3 Foretak TiltakshaverValidation - Error")]
        public void form_4528_2_5_4_3_Foretak_TiltakshaverValidationErrorTest()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "OSLO");

            _form.tiltakshaver.kontaktperson = null;

            _validator.TiltakshaverValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.5.4.3").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.3.5.4.3.1 Foretak TiltakshaverValidation - Error")]
        public void form_4528_3_5_4_3_1_Foretak_TiltakshaverValidationErrorTest()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "OSLO");

            _form.tiltakshaver.kontaktperson.navn = null;

            _validator.TiltakshaverValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.5.4.3.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.2.5.4.3.2 Foretak TiltakshaverValidation - Error")]
        public void form_4528_2_5_4_3_2_Foretak_TiltakshaverValidationErrorTest()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "OSLO");

            _form.tiltakshaver.kontaktperson.mobilnummer = null;
            _form.tiltakshaver.kontaktperson.telefonnummer = null;

            _validator.TiltakshaverValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.5.4.3.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.3.5.4.3.3 Foretak TiltakshaverValidation - Error")]
        public void form_4528_3_5_4_3_3_Foretak_TiltakshaverValidationErrorTest()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "OSLO");

            _form.tiltakshaver.kontaktperson.telefonnummer = "555 12367";

            _validator.TiltakshaverValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.5.4.3.3").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.3.5.4.3.5 Foretak TiltakshaverValidation - Error")]
        public void form_4528_3_5_4_3_5_Foretak_TiltakshaverValidationErrorTest()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "OSLO");

            _form.tiltakshaver.kontaktperson.mobilnummer = "555 12367";

            _validator.TiltakshaverValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.5.4.3.4").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.3.5.5 Foretak TiltakshaverValidation - Error")]
        public void form_4528_3_5_5_Foretak_TiltakshaverValidationErrorTest()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "OSLO");

            _form.tiltakshaver.adresse = null;

            _validator.TiltakshaverValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.5.5").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.3.5.5.1 Foretak TiltakshaverValidation - Error")]
        public void form_4528_3_5_5_1_Foretak_TiltakshaverValidationErrorTest()
        {

            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "OSLO");

            _form.tiltakshaver.adresse.adresselinje1 = null;

            _validator.TiltakshaverValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.5.5.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.3.5.5.2 Foretak TiltakshaverValidation - Error")]
        public void form_4528_3_5_5_2_Foretak_TiltakshaverValidationErrorTest()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "OSLO");

            _form.tiltakshaver.adresse.landkode = "*no";

            _validator.TiltakshaverValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.5.5.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.3.5.5.3 Foretak TiltakshaverValidation - Error")]
        public void form_4528_3_5_5_3_Foretak_TiltakshaverValidationErrorTest()
        {

            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "OSLO");

            _form.tiltakshaver.adresse.postnr = null;

            _validator.TiltakshaverValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.5.5.3").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.3.5.5.3.1 Foretak TiltakshaverValidation - Error")]
        public void form_4528_3_5_5_3_1_Foretak_TiltakshaverValidationErrorTest()
        {

            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "OSLO");

            _form.tiltakshaver.adresse.postnr = "7003*";

            _validator.TiltakshaverValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.5.5.3.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.3.5.5.3.2 Foretak TiltakshaverValidation - Error")]
        public void form_4528_3_5_5_3_2_Foretak_TiltakshaverValidationErrorTest()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(false, "POBOX", "OSLO");

            _validator.TiltakshaverValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.5.5.3.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.3.5.5.3.3 Foretak TiltakshaverValidation - Error")]
        public void form_4528_3_5_5_3_3_Foretak_TiltakshaverValidationErrorTest()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "Bø i telemark");

            _validator.TiltakshaverValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.5.5.3.3").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.3.5.5.3.4 Foretak TiltakshaverValidation - Error")]
        public void form_4528_3_5_5_3_4_Foretak_TiltakshaverValidationErrorTest()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(null);

            _validator.TiltakshaverValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.5.5.3.4").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.3.5.6 Foretak TiltakshaverValidation - Error")]
        public void form_4528_3_5_6_Foretak_TiltakshaverValidationErrorTest()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "OSLO");

            _form.tiltakshaver.navn = null;

            _validator.TiltakshaverValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.5.6").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.3.5.7 Foretak TiltakshaverValidation - Error")]
        public void form_4528_3_5_7_Foretak_TiltakshaverValidationErrorTest()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "OSLO");

            _form.tiltakshaver.mobilnummer = null;
            _form.tiltakshaver.telefonnummer = null;

            _validator.TiltakshaverValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.5.7").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.3.5.8 Foretak TiltakshaverValidation - Error")]
        public void form_4528_3_5_8_Foretak_TiltakshaverValidationErrorTest()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "OSLO");

            _form.tiltakshaver.telefonnummer = "555 123456";

            _validator.TiltakshaverValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.5.8").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.3.5.9 Foretak TiltakshaverValidation - Error")]
        public void form_4528_3_5_9_Foretak_TiltakshaverValidationErrorTest()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "OSLO");

            _form.tiltakshaver.mobilnummer = "555 123456";

            _validator.TiltakshaverValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.5.9").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.3.5.10 Foretak TiltakshaverValidation - Error")]
        public void form_4528_3_5_10_Foretak_TiltakshaverValidationErrorTest()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "OSLO");

            _form.tiltakshaver.epost = null;

            _validator.TiltakshaverValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.5.10").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.2.4 EiendomByggestedValidation - OK")]
        public void form_4528_2_4_EiendomByggestedValidationOkTest()
        {
            _validator._municipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");
            _validator._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, true);

            _validator.EiendomByggestedValidation(_form);
            var result = _validator.GetResult();

            result.messages.Count.Should().Be(0);
        }


        [Fact(DisplayName = "4528.2.4 EiendomByggestedValidation - ERROR")]
        public void form_4528_2_4_EiendomByggestedValidationERRORTest()
        {
            _validator._municipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");
            _validator._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, true);

            _form.eiendomByggested[0] = null;

            _validator.EiendomByggestedValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.4").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.3.4.1 EiendomByggestedValidation - ERROR")]
        public void form_4528_3_4_1_EiendomByggestedValidationERRORTest()
        {
            _validator._municipalityValidator = TestHelper.MokMunicipalityValidatorResult("empty");
            _validator._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, true);

            _validator.EiendomByggestedValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.4.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.3.4.2 EiendomByggestedValidation - ERROR")]
        public void form_4528_3_4_2_EiendomByggestedValidationERRORTest()
        {
            _validator._municipalityValidator = TestHelper.MokMunicipalityValidatorResult("Invalid");
            _validator._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, true);

            _validator.EiendomByggestedValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.4.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.3.4.3 EiendomByggestedValidation - ERROR")]
        public void form_4528_3_4_3_EiendomByggestedValidationERRORTest()
        {
            _validator._municipalityValidator = TestHelper.MokMunicipalityValidatorResult("Expired");
            _validator._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, true);

            _validator.EiendomByggestedValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.4.3").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.3.4.4 EiendomByggestedValidation - ERROR")]
        public void form_4528_3_4_4_EiendomByggestedValidationERRORTest()
        {
            _validator._municipalityValidator = TestHelper.MokMunicipalityValidatorResult("Ok");
            _validator._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, true);

            _form.eiendomByggested[0].eiendomsidentifikasjon.gaardsnummer = null;

            _validator.EiendomByggestedValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.4.4").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.3.4.4.1 EiendomByggestedValidation - ERROR")]
        public void form_4528_3_4_4_1_EiendomByggestedValidationERRORTest()
        {
            _validator._municipalityValidator = TestHelper.MokMunicipalityValidatorResult("Ok");
            _validator._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, true);

            _form.eiendomByggested[0].eiendomsidentifikasjon.bruksnummer = null;

            _validator.EiendomByggestedValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.4.4.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.3.4.5 EiendomByggestedValidation - ERROR")]
        public void form_4528_3_4_5_EiendomByggestedValidationERRORTest()
        {
            _validator._municipalityValidator = TestHelper.MokMunicipalityValidatorResult("Ok");
            _validator._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, true);

            _form.eiendomByggested[0].eiendomsidentifikasjon.gaardsnummer = "-1";

            _validator.EiendomByggestedValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.4.5").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.3.4.6 EiendomByggestedValidation - ERROR")]
        public void form_4528_3_4_6_EiendomByggestedValidationERRORTest()
        {
            _validator._municipalityValidator = TestHelper.MokMunicipalityValidatorResult("Ok");
            _validator._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, true);

            _form.eiendomByggested[0].eiendomsidentifikasjon.bruksnummer = "-3";

            _validator.EiendomByggestedValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.4.6").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.3.4.12 EiendomByggestedValidation - ERROR")]
        public void form_4528_3_4_12_EiendomByggestedValidationERRORTest()
        {
            _validator._municipalityValidator = TestHelper.MokMunicipalityValidatorResult("Ok");
            _validator._matrikkelService = TestHelper.MokMatrikkelServiceResult(false, true, true);
            _validator.EiendomByggestedValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.4.12").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.3.4.13 EiendomByggestedValidation - ERROR")]
        public void form_4528_3_4_13_EiendomByggestedValidationERRORTest()
        {
            _validator._municipalityValidator = TestHelper.MokMunicipalityValidatorResult("Ok");
            _validator._matrikkelService = TestHelper.MokMatrikkelServiceResult(null, true, true);

            _validator.EiendomByggestedValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.4.13").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.3.4.7 EiendomByggestedValidation - ERROR")]
        public void form_4528_3_4_7_EiendomByggestedValidationERRORTest()
        {

            _validator._municipalityValidator = TestHelper.MokMunicipalityValidatorResult("Ok");
            _validator._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, true);

            _form.eiendomByggested[0].bygningsnummer = "12345678a";

            _validator.EiendomByggestedValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.4.7").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.3.4.8 EiendomByggestedValidation - ERROR")]
        public void form_4528_3_4_8_EiendomByggestedValidationERRORTest()
        {

            _validator._municipalityValidator = TestHelper.MokMunicipalityValidatorResult("Ok");
            _validator._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, true);

            _form.eiendomByggested[0].bygningsnummer = "0";

            _validator.EiendomByggestedValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.4.8").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.3.4.14 EiendomByggestedValidation - ERROR")]
        public void form_4528_3_4_14_EiendomByggestedValidationERRORTest()
        {
            _validator._municipalityValidator = TestHelper.MokMunicipalityValidatorResult("Ok");
            _validator._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, false, true);

            _validator.EiendomByggestedValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.4.14").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.3.4.9 EiendomByggestedValidation - ERROR")]
        public void form_4528_3_4_9_EiendomByggestedValidationERRORTest()
        {
            _validator._municipalityValidator = TestHelper.MokMunicipalityValidatorResult("Ok");
            _validator._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, true);

            _form.eiendomByggested[0].adresse = null;

            _validator.EiendomByggestedValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.4.9").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.3.4.9.1 EiendomByggestedValidation - ERROR")]
        public void form_4528_3_4_9_1_EiendomByggestedValidationERRORTest()
        {
            _validator._municipalityValidator = TestHelper.MokMunicipalityValidatorResult("Ok");
            _validator._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, true);

            _form.eiendomByggested[0].adresse.adresselinje1 = null;

            _validator.EiendomByggestedValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.4.9.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.3.4.9.2 EiendomByggestedValidation - ERROR")]
        public void form_4528_3_4_9_2_EiendomByggestedValidationERRORTest()
        {
            _validator._municipalityValidator = TestHelper.MokMunicipalityValidatorResult("Ok");
            _validator._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, true);

            _form.eiendomByggested[0].adresse.gatenavn = null;

            _validator.EiendomByggestedValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.4.9.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.3.4.9.2 (1) EiendomByggestedValidation - ERROR")]
        public void form_4528_3_4_9_2_a_EiendomByggestedValidationERRORTest()
        {
            _validator._municipalityValidator = TestHelper.MokMunicipalityValidatorResult("Ok");
            _validator._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, true);

            _form.eiendomByggested[0].adresse.husnr = null;

            _validator.EiendomByggestedValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.4.9.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.3.4.15 EiendomByggestedValidation - ERROR")]
        public void form_4528_3_4_15_EiendomByggestedValidationERRORTest()
        {
            _validator._municipalityValidator = TestHelper.MokMunicipalityValidatorResult("Ok");
            _validator._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, null);

            _validator.EiendomByggestedValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.4.15").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.3.4.15.1 EiendomByggestedValidation - ERROR")]
        public void form_4528_3_4_15_1_EiendomByggestedValidationERRORTest()
        {
            _validator._municipalityValidator = TestHelper.MokMunicipalityValidatorResult("Ok");
            _validator._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, false);

            _validator.EiendomByggestedValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.4.15.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.3.4.10 EiendomByggestedValidation - ERROR")]
        public void form_4528_3_4_10_EiendomByggestedValidationERRORTest()
        {
            _validator._municipalityValidator = TestHelper.MokMunicipalityValidatorResult("Ok");
            _validator._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, true);

            _form.eiendomByggested[0].kommunenavn = null;

            _validator.EiendomByggestedValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.4.10").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4528.2.4.11 EiendomByggestedValidation - ERROR")]
        public void form_4528_2_4_11_EiendomByggestedValidationERRORTest()
        {
            _validator._municipalityValidator = TestHelper.MokMunicipalityValidatorResult("Ok");
            _validator._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, true);

            _form.eiendomByggested[0].bolignummer = "0*101";

            _validator.EiendomByggestedValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.4.11").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
            result.messages.FirstOrDefault().messagetype.Should().Be("ERROR");
        }

        [Fact(DisplayName = "4528.2.6.1 AdkomstValidering - Ok")]
        public void form_4528_2_6_1_AdkomstValideringOkest()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.beskrivelseAvTiltak.type[0].kodeverdi = tiltakstype;
            _form.beskrivelseAvTiltak.type[0].kodebeskrivelse = "Nytt bygg - Under 70 m2 - ikke boligformål";

            _validator.AdkomstValidering(_form, new List<string>() { "Avkjoerselsplan" });

            var result = _validator.GetResult();
            result.messages.Count.Should().Be(0);
        }

        [Fact(DisplayName = "4528.2.6.1 AdkomstValidering - ERROR")]
        public void form_4528_2_6_1_AdkomstValideringERRORest()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.beskrivelseAvTiltak.type[0].kodeverdi = tiltakstype;
            _form.beskrivelseAvTiltak.type[0].kodebeskrivelse = "Nytt bygg - Under 70 m2 - ikke boligformål";

            _form.rammebetingelser.adkomst = null;
            _validator.AdkomstValidering(_form, new List<string>() { "Avkjoerselsplan" });
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.6.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.2.6.1.1 AdkomstValidering - ERROR")]
        public void form_4528_2_6_1_1_AdkomstValideringERRORest()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.beskrivelseAvTiltak.type[0].kodeverdi = tiltakstype;
            _form.beskrivelseAvTiltak.type[0].kodebeskrivelse = "Nytt bygg - Under 70 m2 - ikke boligformål";

            _form.rammebetingelser.adkomst.nyeEndretAdkomst = null;
            _form.rammebetingelser.adkomst.nyeEndretAdkomstSpecified = false;

            _validator.AdkomstValidering(_form, new List<string>() { "Avkjoerselsplan" });
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.6.1.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.2.6.2.1 AdkomstValidering - ERROR")]
        public void form_4528_2_6_2_1_AdkomstValideringERRORest()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.beskrivelseAvTiltak.type[0].kodeverdi = tiltakstype;
            _form.beskrivelseAvTiltak.type[0].kodebeskrivelse = "Nytt bygg - Under 70 m2 - ikke boligformål";

            _validator.AdkomstValidering(_form, new List<string>() { "*Avkjoerselsplan" });
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.6.2.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
            result.messages.FirstOrDefault().messagetype.Should().Be("WARNING");

        }

        [Fact(DisplayName = "4528.2.6.2.2 AdkomstValidering - ERROR")]
        public void form_4528_2_6_2_2_AdkomstValideringERRORest()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.beskrivelseAvTiltak.type[0].kodeverdi = tiltakstype;
            _form.beskrivelseAvTiltak.type[0].kodebeskrivelse = "Nytt bygg - Under 70 m2 - ikke boligformål";

            _form.rammebetingelser.adkomst.vegtype = null;

            _validator.AdkomstValidering(_form, new List<string>() { "Avkjoerselsplan" });
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.6.2.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.1.6.2.3 AdkomstValidering - ERROR")]
        public void form_4528_1_6_2_3_AdkomstValideringERRORest()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.beskrivelseAvTiltak.type[0].kodeverdi = tiltakstype;
            _form.beskrivelseAvTiltak.type[0].kodebeskrivelse = "Nytt bygg - Under 70 m2 - ikke boligformål";

            _form.rammebetingelser.adkomst.vegtype[0].kodeverdi = "*RiksFylkesveg";

            _validator.AdkomstValidering(_form, new List<string>() { "Avkjoerselsplan" });
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.1.6.2.3").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.2.6.2.4 AdkomstValidering - ERROR")]
        public void form_4528_2_6_2_4_AdkomstValideringERRORest()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.beskrivelseAvTiltak.type[0].kodeverdi = tiltakstype;
            _form.beskrivelseAvTiltak.type[0].kodebeskrivelse = "Nytt bygg - Under 70 m2 - ikke boligformål";

            _form.rammebetingelser.adkomst.vegtype[0].kodeverdi = "RiksFylkesveg";
            _form.rammebetingelser.adkomst.erTillatelseGittRiksFylkesveg = null;
            _form.rammebetingelser.adkomst.erTillatelseGittRiksFylkesvegSpecified = false;

            _validator.AdkomstValidering(_form, new List<string>() { "Avkjoerselsplan" });
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.6.2.4").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.2.6.2.5 AdkomstValidering - ERROR")]
        public void form_4528_2_6_2_5_AdkomstValideringERRORest()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.beskrivelseAvTiltak.type[0].kodeverdi = tiltakstype;
            _form.beskrivelseAvTiltak.type[0].kodebeskrivelse = "Nytt bygg - Under 70 m2 - ikke boligformål";

            _form.rammebetingelser.adkomst.vegtype[0].kodeverdi = "KommunalVeg";
            _form.rammebetingelser.adkomst.erTillatelseGittKommunalVeg = null;
            _form.rammebetingelser.adkomst.erTillatelseGittKommunalVegSpecified = false;

            _validator.AdkomstValidering(_form, new List<string>() { "Avkjoerselsplan" });
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.6.2.5").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.2.6.2.6 AdkomstValidering - ERROR")]
        public void form_4528_2_6_2_6_AdkomstValideringERRORest()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.beskrivelseAvTiltak.type[0].kodeverdi = tiltakstype;
            _form.beskrivelseAvTiltak.type[0].kodebeskrivelse = "Nytt bygg - Under 70 m2 - ikke boligformål";

            _form.rammebetingelser.adkomst.vegtype[0].kodeverdi = "PrivatVeg";
            _form.rammebetingelser.adkomst.erTillatelseGittPrivatVeg = null;
            _form.rammebetingelser.adkomst.erTillatelseGittPrivatVegSpecified = false;

            _validator.AdkomstValidering(_form, new List<string>() { "Avkjoerselsplan" });
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.6.2.6").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4528.2.6.4 GjeldendePlanValidering - OK")]
        public void form_4528_2_6_4_GjeldendePlanValideringOkRest()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _validator.GjeldendePlanValidering(_form, new List<string>() { "UnderlagUtnytting" });
            var result = _validator.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4528.2.6.4 GjeldendePlanValidering - Error")]
        public void form_4528_2_6_4_GjeldendePlanValideringErrorRest()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.rammebetingelser.plan.gjeldendePlan = null;

            _validator.GjeldendePlanValidering(_form, new List<string>() { "UnderlagUtnytting" });
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.6.4").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4528.2.6.4.1 GjeldendePlanValidering - Error")]
        public void form_4528_2_6_4_1_GjeldendePlanValideringErrorRest()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.rammebetingelser.plan.gjeldendePlan.plantype.kodeverdi = null;

            _validator.GjeldendePlanValidering(_form, new List<string>() { "UnderlagUtnytting" });
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.6.4.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4528.2.6.4.1.1 GjeldendePlanValidering - Error")]
        public void form_4528_2_6_4_1_1_GjeldendePlanValideringErrorRest()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.rammebetingelser.plan.gjeldendePlan.plantype.kodeverdi = "*RP";
            _form.rammebetingelser.plan.gjeldendePlan.plantype.kodebeskrivelse = "Reguleringsplan";

            _validator.GjeldendePlanValidering(_form, new List<string>() { "UnderlagUtnytting" });
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.6.4.1.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.2.6.4.2 GjeldendePlanValidering - Error")]
        public void form_4528_2_6_4_2_GjeldendePlanValideringErrorRest()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.rammebetingelser.plan.gjeldendePlan.plantype.kodeverdi = "RP";
            _form.rammebetingelser.plan.gjeldendePlan.plantype.kodebeskrivelse = null;

            _validator.GjeldendePlanValidering(_form, new List<string>() { "UnderlagUtnytting" });
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.6.4.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.2.6.4.2.1 GjeldendePlanValidering - Error")]
        public void form_4528_2_6_4_2_1_GjeldendePlanValideringErrorRest()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.rammebetingelser.plan.gjeldendePlan.plantype.kodeverdi = "RP";
            _form.rammebetingelser.plan.gjeldendePlan.plantype.kodebeskrivelse = "*Reguleringsplan";

            var jsonTestFile = TestHelper.GetValidateFormv2FormSkjema(_formSkjema, _formSetElements, _form);

            _validator.GjeldendePlanValidering(_form, new List<string>() { "UnderlagUtnytting" });
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.6.4.2.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4528.2.6.4.3 GjeldendePlanValidering - Error")]
        public void form_4528_2_6_4_3_GjeldendePlanValideringErrorRest()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.rammebetingelser.plan.gjeldendePlan.beregningsregelGradAvUtnytting.kodeverdi = null;

            _validator.GjeldendePlanValidering(_form, new List<string>() { "UnderlagUtnytting" });
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.6.4.3").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4528.2.6.4.4 GjeldendePlanValidering - Error")]
        public void form_4528_2_6_4_4_GjeldendePlanValideringErrorRest()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.rammebetingelser.plan.gjeldendePlan.beregningsregelGradAvUtnytting.kodeverdi = "*BYA";
            _form.rammebetingelser.plan.gjeldendePlan.beregningsregelGradAvUtnytting.kodebeskrivelse = "Bebygd areal";

            _validator.GjeldendePlanValidering(_form, new List<string>() { "UnderlagUtnytting" });
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.6.4.4").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4528.2.6.4.5 GjeldendePlanValidering - Error")]
        public void form_4528_2_6_4_5_GjeldendePlanValideringErrorRest()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.rammebetingelser.plan.gjeldendePlan.beregningsregelGradAvUtnytting.kodeverdi = "BYA";
            _form.rammebetingelser.plan.gjeldendePlan.beregningsregelGradAvUtnytting.kodebeskrivelse = null;

            _validator.GjeldendePlanValidering(_form, new List<string>() { "UnderlagUtnytting" });
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.6.4.5").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
            result.messages.FirstOrDefault().messagetype.Should().Be("WARNING");
        }
        [Fact(DisplayName = "4528.2.6.4.6 GjeldendePlanValidering - Error")]
        public void form_4528_2_6_4_6_GjeldendePlanValideringErrorRest()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.rammebetingelser.plan.gjeldendePlan.beregningsregelGradAvUtnytting.kodeverdi = "annetUdef";
            _form.rammebetingelser.plan.gjeldendePlan.beregningsregelGradAvUtnytting.kodebeskrivelse = "Annet";

            _validator.GjeldendePlanValidering(_form, new List<string>() { "*UnderlagUtnytting" });
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.6.4.6").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4528.2.6.4.7 GjeldendePlanValidering - Error")]
        public void form_4528_2_6_4_7_GjeldendePlanValideringErrorRest()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.rammebetingelser.plan.gjeldendePlan.formaal = null;

            _validator.GjeldendePlanValidering(_form, _formSetElements);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.6.4.7").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.3.6.5 ArealdisponeringValidering - OK")]
        public void form_4528_3_6_5_ArealdisponeringValideringOKTest()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.beskrivelseAvTiltak.type[0].kodeverdi = tiltakstype;
            _form.beskrivelseAvTiltak.type[0].kodebeskrivelse = "Nytt bygg - Under 70 m2 - ikke boligformål";

            _validator.ArealdisponeringValidering(_form);
            var result = _validator.GetResult();

            result.messages.Count.Should().Be(0);
        }

        [Fact(DisplayName = "4528.3.6.5.11 ArealdisponeringValidering - error")]
        public void form_4528_3_6_5_11_ArealdisponeringValideringErrorTest()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.beskrivelseAvTiltak.type[0].kodeverdi = tiltakstype;
            _form.beskrivelseAvTiltak.type[0].kodebeskrivelse = "Nytt bygg - Under 70 m2 - ikke boligformål";
            
            _form.rammebetingelser.plan.gjeldendePlan.beregningsregelGradAvUtnytting.kodeverdi = "BYA";
            _form.rammebetingelser.plan.gjeldendePlan.beregningsregelGradAvUtnytting.kodebeskrivelse = "Bebygd areal";
            
            _form.rammebetingelser.arealdisponering = null;

            _validator.ArealdisponeringValidering(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.6.5.11").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.2.6.5 ArealdisponeringValidering - error")]
        public void form_4528_2_6_5_ArealdisponeringValideringErrorTest()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.beskrivelseAvTiltak.type[0].kodeverdi = tiltakstype;
            _form.beskrivelseAvTiltak.type[0].kodebeskrivelse = "Nytt bygg - Under 70 m2 - ikke boligformål";

            _form.rammebetingelser.arealdisponering.arealBebyggelseEksisterende = null;
            _form.rammebetingelser.arealdisponering.arealBebyggelseEksisterendeSpecified = false;

            _validator.ArealdisponeringValidering(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.6.5").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.2.6.5.2 ArealdisponeringValidering - error")]
        public void form_4528_2_6_5_2_ArealdisponeringValideringErrorTest()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.beskrivelseAvTiltak.type[0].kodeverdi = tiltakstype;
            _form.beskrivelseAvTiltak.type[0].kodebeskrivelse = "Nytt bygg - Under 70 m2 - ikke boligformål";

            _form.rammebetingelser.arealdisponering.arealBebyggelseEksisterende = -89.2;

            _validator.ArealdisponeringValidering(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.6.5.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.2.6.5.3 ArealdisponeringValidering - error")]
        public void form_4528_2_6_5_3_ArealdisponeringValideringErrorTest()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.beskrivelseAvTiltak.type[0].kodeverdi = tiltakstype;
            _form.beskrivelseAvTiltak.type[0].kodebeskrivelse = "Nytt bygg - Under 70 m2 - ikke boligformål";

            _form.rammebetingelser.arealdisponering.arealBebyggelseNytt = -16.3;

            _validator.ArealdisponeringValidering(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.6.5.3").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.2.6.5.4 ArealdisponeringValidering - error")]
        public void form_4528_2_6_5_4_ArealdisponeringValideringErrorTest()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.beskrivelseAvTiltak.type[0].kodeverdi = tiltakstype;
            _form.beskrivelseAvTiltak.type[0].kodebeskrivelse = "Nytt bygg - Under 70 m2 - ikke boligformål";

            _form.rammebetingelser.arealdisponering.arealBebyggelseSomSkalRives = -8.4;

            _validator.ArealdisponeringValidering(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.6.5.4").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.2.6.5.5 ArealdisponeringValidering - error")]
        public void form_4528_2_6_5_5_ArealdisponeringValideringErrorTest()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.beskrivelseAvTiltak.type[0].kodeverdi = tiltakstype;
            _form.beskrivelseAvTiltak.type[0].kodebeskrivelse = "Nytt bygg - Under 70 m2 - ikke boligformål";

            _form.rammebetingelser.arealdisponering.parkeringsarealTerreng = -45.2;

            _validator.ArealdisponeringValidering(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.6.5.5").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.2.6.5.6 ArealdisponeringValidering - error")]
        public void form_4528_2_6_5_6_ArealdisponeringValideringErrorTest()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.beskrivelseAvTiltak.type[0].kodeverdi = tiltakstype;
            _form.beskrivelseAvTiltak.type[0].kodebeskrivelse = "Nytt bygg - Under 70 m2 - ikke boligformål";

            _form.rammebetingelser.arealdisponering.beregnetGradAvUtnytting = -142.3;

            _validator.ArealdisponeringValidering(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.6.5.6").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.2.6.5.9 ArealdisponeringValidering - error")]
        public void form_4528_2_6_5_9_ArealdisponeringValideringErrorTest()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.beskrivelseAvTiltak.type[0].kodeverdi = tiltakstype;
            _form.beskrivelseAvTiltak.type[0].kodebeskrivelse = "Nytt bygg - Under 70 m2 - ikke boligformål";

            _form.rammebetingelser.arealdisponering.beregnetGradAvUtnytting = 142.4;

            _validator.ArealdisponeringValidering(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.6.5.9").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.2.6.5.1 ArealdisponeringValidering - error")]
        public void form_4528_2_6_5_1_ArealdisponeringValideringErrorTest()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.beskrivelseAvTiltak.type[0].kodeverdi = tiltakstype;
            _form.beskrivelseAvTiltak.type[0].kodebeskrivelse = "Nytt bygg - Under 70 m2 - ikke boligformål";

            _form.rammebetingelser.plan.gjeldendePlan.beregningsregelGradAvUtnytting.kodeverdi = "%BYA";
            _form.rammebetingelser.plan.gjeldendePlan.beregningsregelGradAvUtnytting.kodebeskrivelse = "Prosent bebygd areal";
            _form.rammebetingelser.arealdisponering.tomtearealBeregnet = null;
            _form.rammebetingelser.arealdisponering.tomtearealBeregnetSpecified = false;

            _validator.ArealdisponeringValidering(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.6.5.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.2.6.5.8 ArealdisponeringValidering - error")]
        public void form_4528_2_6_5_8_ArealdisponeringValideringErrorTest()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.beskrivelseAvTiltak.type[0].kodeverdi = tiltakstype;
            _form.beskrivelseAvTiltak.type[0].kodebeskrivelse = "Nytt bygg - Under 70 m2 - ikke boligformål";

            _form.rammebetingelser.plan.gjeldendePlan.beregningsregelGradAvUtnytting.kodeverdi = "%BYA";
            _form.rammebetingelser.plan.gjeldendePlan.beregningsregelGradAvUtnytting.kodebeskrivelse = "Prosent bebygd areal";
            _form.rammebetingelser.arealdisponering.tomtearealBeregnet = 0;

            _validator.ArealdisponeringValidering(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.6.5.8").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.2.6.5.7 ArealdisponeringValidering - error")]
        public void form_4528_2_6_5_7_ArealdisponeringValideringErrorTest()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.beskrivelseAvTiltak.type[0].kodeverdi = tiltakstype;
            _form.beskrivelseAvTiltak.type[0].kodebeskrivelse = "Nytt bygg - Under 70 m2 - ikke boligformål";

            _form.rammebetingelser.plan.gjeldendePlan.beregningsregelGradAvUtnytting.kodeverdi = "%BYA";
            _form.rammebetingelser.plan.gjeldendePlan.beregningsregelGradAvUtnytting.kodebeskrivelse = "Prosent bebygd areal";
            _form.rammebetingelser.arealdisponering.tomtearealBeregnet = -24.1;

            _validator.ArealdisponeringValidering(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.6.5.7").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.2.6.5.10 ArealdisponeringValidering - error")]
        public void form_4528_2_6_5_10_ArealdisponeringValideringErrorTest()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.beskrivelseAvTiltak.type[0].kodeverdi = tiltakstype;
            _form.beskrivelseAvTiltak.type[0].kodebeskrivelse = "Nytt bygg - Under 70 m2 - ikke boligformål";

            _form.rammebetingelser.plan.gjeldendePlan.beregningsregelGradAvUtnytting.kodeverdi = "%BYA";
            _form.rammebetingelser.plan.gjeldendePlan.beregningsregelGradAvUtnytting.kodebeskrivelse = "Prosent bebygd areal";
            _form.rammebetingelser.arealdisponering.beregnetGradAvUtnytting = 142.3; // '590.45643153527'

            _validator.ArealdisponeringValidering(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.6.5.10").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.2.6.3.1 KulturminneValidering - Ok")]
        public void form_4528_2_6_3_1_KulturminneValideringOkTest()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.beskrivelseAvTiltak.type[0].kodeverdi = tiltakstype;
            _form.beskrivelseAvTiltak.type[0].kodebeskrivelse = "Nytt bygg - Under 70 m2 - ikke boligformål";

            _validator.KulturminneValidering(_form, new List<string>() { "UttalelseKulturminnemyndighet" });

            var result = _validator.GetResult();
            result.messages.Count.Should().Be(0);
        }

        [Fact(DisplayName = "4528.2.6.3.1 KulturminneValidering - Error")]
        public void form_4528_2_6_3_1_KulturminneValideringErrorTest()
        {
            var tiltakstype = "tilbyggover50m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.beskrivelseAvTiltak.type[0].kodeverdi = tiltakstype;
            _form.beskrivelseAvTiltak.type[0].kodebeskrivelse = "Endring av bygg - utvendig - Tilbygg med samlet areal større enn 50 m2";

            _form.rammebetingelser.generelleVilkaar = null;

            _validator.KulturminneValidering(_form, new List<string>() { "UttalelseKulturminnemyndighet" });

            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.6.3.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
            result.messages.FirstOrDefault().messagetype.Should().Be("WARNING");

        }

        [Fact(DisplayName = "4528.2.6.3.1 (1) KulturminneValidering - Error")]
        public void form_4528_2_6_3_1_a_KulturminneValideringErrorTest()
        {
            var tiltakstype = "tilbyggover50m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.beskrivelseAvTiltak.type[0].kodeverdi = tiltakstype;
            _form.beskrivelseAvTiltak.type[0].kodebeskrivelse = "Endring av bygg - utvendig - Tilbygg med samlet areal større enn 50 m2";

            _form.rammebetingelser.generelleVilkaar.beroererTidligere1850 = null;
            _form.rammebetingelser.generelleVilkaar.beroererTidligere1850Specified = false;

            _validator.KulturminneValidering(_form, new List<string>() { "UttalelseKulturminnemyndighet" });

            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.6.3.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
            result.messages.FirstOrDefault().messagetype.Should().Be("WARNING");

        }

        [Fact(DisplayName = "4528.2.6.3.1.1 KulturminneValidering - Error")]
        public void form_4528_2_6_3_1_1_KulturminneValideringErrorTest()
        {
            var tiltakstype = "tilbyggover50m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.beskrivelseAvTiltak.type[0].kodeverdi = tiltakstype;
            _form.beskrivelseAvTiltak.type[0].kodebeskrivelse = "Endring av bygg - utvendig - Tilbygg med samlet areal større enn 50 m2";

            _validator.KulturminneValidering(_form, new List<string>() { "*UttalelseKulturminnemyndighet" });

            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.6.3.1.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.2.6.3.2 ArbeidsplasserValidering - Ok")]
        public void form_4528_2_6_3_2_ArbeidsplasserValideringOkTest()
        {
            _validator.ArbeidsplasserValidering(_form, new List<string>() { "SamtykkeArbeidstilsynet" });

            var result = _validator.GetResult();
            result.messages.Count.Should().Be(0);
        }

        [Fact(DisplayName = "4528.2.6.3.2 ArbeidsplasserValidering - Error")]
        public void form_4528_2_6_3_2_ArbeidsplasserValideringErrorTest()
        {

            _form.rammebetingelser.generelleVilkaar.beroererArbeidsplasser = true;
            _form.rammebetingelser.generelleVilkaar.beroererArbeidsplasserSpecified = true;

            _validator.ArbeidsplasserValidering(_form, new List<string>() { "*SamtykkeArbeidstilsynet" });
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.6.3.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.2.6.3.2 (1) ArbeidsplasserValidering - Error")]
        public void form_4528_2_6_3_2_a_ArbeidsplasserValideringErrorTest()
        {

            _form.rammebetingelser.generelleVilkaar.beroererArbeidsplasser = true;
            _form.rammebetingelser.generelleVilkaar.beroererArbeidsplasserSpecified = true;

            _validator.ArbeidsplasserValidering(_form, null);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.6.3.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.2.6.3.3 GenerelleVilkaarValidering - Ok")]
        public void form_4528_2_6_3_3_GenerelleVilkaarValideringOkTest()
        {
            _validator.GenerelleVilkaarValidering(_form);

            var result = _validator.GetResult();
            result.messages.Count.Should().Be(0);
        }

        [Fact(DisplayName = "4528.2.6.3.3 GenerelleVilkaarValidering - Error")]
        public void form_4528_2_6_3_3_GenerelleVilkaarValideringErrorTest()
        {

            _form.rammebetingelser.generelleVilkaar.norskSvenskDansk = null;
            _form.rammebetingelser.generelleVilkaar.norskSvenskDanskSpecified = false;

            _validator.GenerelleVilkaarValidering(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.6.3.3").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.2.6.6 VannforsyningValidering - Ok")]
        public void form_4528_2_6_6_VannforsyningValideringOkTest()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.beskrivelseAvTiltak.type[0].kodeverdi = tiltakstype;
            _form.beskrivelseAvTiltak.type[0].kodebeskrivelse = "Nytt bygg - Under 70 m2 - ikke boligformål";

            _validator.VannforsyningValidering(_form);

            var result = _validator.GetResult();
            result.messages.Count.Should().Be(0);
        }

        [Fact(Skip = "Blocking deployment")]
        //[Fact(DisplayName = "4528.2.6.6 VannforsyningValidering - Error")]
        public void form_4528_2_6_6_VannforsyningValideringErrorTest()
        {
            var tiltakstype = "nyttbyggover70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.beskrivelseAvTiltak.type[0].kodeverdi = tiltakstype;
            _form.beskrivelseAvTiltak.type[0].kodebeskrivelse = "Alle typer bygg over 70 m2 unntatt boliger eller fritidsboliger";

            _form.rammebetingelser.vannforsyning = null;

            _validator.VannforsyningValidering(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.6.6").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(Skip ="Blocking deployment")]
        //[Fact(DisplayName = "4528.2.6.6.1 VannforsyningValidering - Error")]
        public void form_4528_2_6_6_1_VannforsyningValideringErrorTest()
        {
            var tiltakstype = "nyttbyggover70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.beskrivelseAvTiltak.type[0].kodeverdi = tiltakstype;
            _form.beskrivelseAvTiltak.type[0].kodebeskrivelse = "Alle typer bygg over 70 m2 unntatt boliger eller fritidsboliger";

            _form.rammebetingelser.vannforsyning.tilknytningstype.kodeverdi = "*PrivatKloakk";
            _form.rammebetingelser.vannforsyning.tilknytningstype.kodebeskrivelse = "Privat Kloakk";

            _validator.VannforsyningValidering(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.6.6.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.2.6.6.2 VannforsyningValidering - Error")]
        public void form_4528_2_6_6_2_VannforsyningValideringErrorTest()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.beskrivelseAvTiltak.type[0].kodeverdi = tiltakstype;
            _form.beskrivelseAvTiltak.type[0].kodebeskrivelse = "Nytt bygg - Under 70 m2 - ikke boligformål";

            _form.rammebetingelser.vannforsyning.tilknytningstype.kodeverdi = "AnnenPrivatInnlagt";
            _form.rammebetingelser.vannforsyning.tilknytningstype.kodebeskrivelse = "Annen privat vannforsyning, ikke innlagt vann";

            _form.rammebetingelser.vannforsyning.beskrivelse = null;

            _validator.VannforsyningValidering(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.6.6.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4528.2.6.7.8 VannforsyningValidering - Error")]
        public void form_4528_2_6_7_8_VannforsyningValideringErrorTest()
        {
            var tiltakstype = "nyttbyggover70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.beskrivelseAvTiltak.type[0].kodeverdi = tiltakstype;
            _form.beskrivelseAvTiltak.type[0].kodebeskrivelse = "Nytt bygg - Under 70 m2 - ikke boligformål";

            _form.rammebetingelser.vannforsyning.krysserVannforsyningAnnensGrunn = null;
            
            _form.rammebetingelser.vannforsyning.krysserVannforsyningAnnensGrunnSpecified = false;
            _validator.VannforsyningValidering(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.6.7.8").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.2.6.7.8.1 VannforsyningValidering - Error")]
        public void form_4528_2_6_7_8_1_VannforsyningValideringErrorTest()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.beskrivelseAvTiltak.type[0].kodeverdi = tiltakstype;
            _form.beskrivelseAvTiltak.type[0].kodebeskrivelse = "Nytt bygg - Under 70 m2 - ikke boligformål";

            _form.rammebetingelser.vannforsyning.krysserVannforsyningAnnensGrunn = true;
            _form.rammebetingelser.vannforsyning.krysserVannforsyningAnnensGrunnSpecified = true;

            _form.rammebetingelser.vannforsyning.tinglystErklaering = null;
            _form.rammebetingelser.vannforsyning.tinglystErklaeringSpecified = false;
            
            //_form.rammebetingelser.vannforsyning.tilknytningstype.kodeverdi = "AnnenPrivatIkkeInnlagt";
            
            _validator.VannforsyningValidering(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.6.7.8.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4528.2.6.7 AvloepValidering - Ok")]
        public void form_4528_2_6_7_AvloepValideringOkTest()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.beskrivelseAvTiltak.type[0].kodeverdi = tiltakstype;
            _form.beskrivelseAvTiltak.type[0].kodebeskrivelse = "Nytt bygg - Under 70 m2 - ikke boligformål";

            _validator.AvloepValidering(_form);

            var result = _validator.GetResult();
            result.messages.Count.Should().Be(0);
        }

        [Fact(DisplayName = "4528.2.6.7 AvloepValidering - Error")]
        public void form_4528_2_6_7_AvloepValideringErrorTest()
        {
            var tiltakstype = "boligoppdeling";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.beskrivelseAvTiltak.type[0].kodeverdi = tiltakstype;

            _form.beskrivelseAvTiltak.type[0].kodebeskrivelse = "Endring av boligenhet - Oppdeling av bolig";

            _form.rammebetingelser.avloep = null;

            _validator.AvloepValidering(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.6.7").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.2.6.7.1.1 AvloepValidering - Error")]
        public void form_4528_2_6_7_1_1_AvloepValideringErrorTest()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.beskrivelseAvTiltak.type[0].kodeverdi = tiltakstype;
            _form.beskrivelseAvTiltak.type[0].kodebeskrivelse = "Nytt bygg - Under 70 m2 - ikke boligformål";

            _form.rammebetingelser.avloep.tilknytningstype.kodeverdi = null;

            _validator.AvloepValidering(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.6.7.1.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.2.6.7.2 AvloepValidering - Error")]
        public void form_4528_2_6_7_2_AvloepValideringErrorTest()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.beskrivelseAvTiltak.type[0].kodeverdi = tiltakstype;
            _form.beskrivelseAvTiltak.type[0].kodebeskrivelse = "Nytt bygg - Under 70 m2 - ikke boligformål";

            _form.rammebetingelser.avloep.tilknytningstype.kodeverdi = "PrivatKloakk";
            _form.rammebetingelser.avloep.tilknytningstype.kodebeskrivelse = null;

            _validator.AvloepValidering(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.6.7.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.2.6.7.3 AvloepValidering - Error")]
        public void form_4528_2_6_7_3_AvloepValideringErrorTest()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.beskrivelseAvTiltak.type[0].kodeverdi = tiltakstype;
            _form.beskrivelseAvTiltak.type[0].kodebeskrivelse = "Nytt bygg - Under 70 m2 - ikke boligformål";

            _form.rammebetingelser.avloep.tilknytningstype.kodeverdi = "PrivatKloakk";
            _form.rammebetingelser.avloep.tilknytningstype.kodebeskrivelse = "Privat avløpsanlegg";

            _form.rammebetingelser.avloep.installereVannklosett = null;
            _form.rammebetingelser.avloep.installereVannklosettSpecified = false;

            _validator.AvloepValidering(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.6.7.3").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.2.6.7.4 AvloepValidering - Error")]
        public void form_4528_2_6_7_4_AvloepValideringErrorTest()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.beskrivelseAvTiltak.type[0].kodeverdi = tiltakstype;
            _form.beskrivelseAvTiltak.type[0].kodebeskrivelse = "Nytt bygg - Under 70 m2 - ikke boligformål";

            _form.rammebetingelser.avloep.tilknytningstype.kodeverdi = "PrivatKloakk";
            _form.rammebetingelser.avloep.tilknytningstype.kodebeskrivelse = "Privat avløpsanlegg";

            _form.rammebetingelser.avloep.utslippstillatelse = null;
            _form.rammebetingelser.avloep.utslippstillatelseSpecified = false;

            _validator.AvloepValidering(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.6.7.4").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.2.6.7.5 AvloepValidering - Error")]
        public void form_4528_2_6_7_5_AvloepValideringErrorTest()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.beskrivelseAvTiltak.type[0].kodeverdi = tiltakstype;
            _form.beskrivelseAvTiltak.type[0].kodebeskrivelse = "Nytt bygg - Under 70 m2 - ikke boligformål";

            _form.rammebetingelser.avloep.krysserAvloepAnnensGrunn = null;
            _form.rammebetingelser.avloep.krysserAvloepAnnensGrunnSpecified = false;

            _validator.AvloepValidering(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.6.7.5").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.2.6.7.5.1 AvloepValidering - Error")]
        public void form_4528_2_6_7_5_1_AvloepValideringErrorTest()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.beskrivelseAvTiltak.type[0].kodeverdi = tiltakstype;
            _form.beskrivelseAvTiltak.type[0].kodebeskrivelse = "Nytt bygg - Under 70 m2 - ikke boligformål";

            _form.rammebetingelser.avloep.krysserAvloepAnnensGrunn = true;
            _form.rammebetingelser.avloep.krysserAvloepAnnensGrunnSpecified = true;

            _form.rammebetingelser.avloep.tinglystErklaering = null;
            _form.rammebetingelser.avloep.tinglystErklaeringSpecified = false;

            _validator.AvloepValidering(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.6.7.5.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4528.2.6.7.6 AvloepValidering - Error")]
        public void form_4528_2_6_7_6_AvloepValideringErrorTest()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.beskrivelseAvTiltak.type[0].kodeverdi = tiltakstype;
            _form.beskrivelseAvTiltak.type[0].kodebeskrivelse = "Nytt bygg - Under 70 m2 - ikke boligformål";

            _form.rammebetingelser.avloep.overvannTerreng = null;
            _form.rammebetingelser.avloep.overvannTerrengSpecified = false;

            _form.rammebetingelser.avloep.overvannAvloepssystem = null;
            _form.rammebetingelser.avloep.overvannAvloepssystemSpecified = false;

            _validator.AvloepValidering(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.6.7.6").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }


        [Fact(DisplayName = "4528.2.6.8 KravTilByggegrunnValidering - Ok")]
        public void form_4528_2_6_8_KravTilByggegrunnValideringOkTest()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.beskrivelseAvTiltak.type[0].kodeverdi = tiltakstype;
            _form.beskrivelseAvTiltak.type[0].kodebeskrivelse = "Nytt bygg - Under 70 m2 - ikke boligformål";

            _validator.KravTilByggegrunnValidering(_form, new List<string>() { "RedegjoerelseSkredOgFlom" });
            var result = _validator.GetResult();

            result.messages.Count.Should().Be(0);
        }

        [Fact(DisplayName = "4528.2.6.8.1 KravTilByggegrunnValidering - Error")]
        public void form_4528_2_6_8_1_KravTilByggegrunnValideringErrorTest()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.beskrivelseAvTiltak.type[0].kodeverdi = tiltakstype;
            _form.beskrivelseAvTiltak.type[0].kodebeskrivelse = "Nytt bygg - Under 70 m2 - ikke boligformål";

            _form.rammebetingelser.kravTilByggegrunn.flomutsattOmraade = null;
            _form.rammebetingelser.kravTilByggegrunn.flomutsattOmraadeSpecified = false;

            _validator.KravTilByggegrunnValidering(_form, new List<string>() { "RedegjoerelseSkredOgFlom" });
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.6.8.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.2.6.8.1.1 KravTilByggegrunnValidering - ERROR")]
        public void form_4528_2_6_8_1_1_KravTilByggegrunnValideringErrorTest()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.beskrivelseAvTiltak.type[0].kodeverdi = tiltakstype;
            _form.beskrivelseAvTiltak.type[0].kodebeskrivelse = "Nytt bygg - Under 70 m2 - ikke boligformål";


            _form.rammebetingelser.kravTilByggegrunn.flomutsattOmraade = true;
            _form.rammebetingelser.kravTilByggegrunn.flomutsattOmraadeSpecified = true;


            _form.rammebetingelser.kravTilByggegrunn.f1 = null;
            _form.rammebetingelser.kravTilByggegrunn.f1Specified = false;

            _form.rammebetingelser.kravTilByggegrunn.f2 = null;
            _form.rammebetingelser.kravTilByggegrunn.f2Specified = false;

            _form.rammebetingelser.kravTilByggegrunn.f3 = null;
            _form.rammebetingelser.kravTilByggegrunn.f3Specified = false;

            _validator.KravTilByggegrunnValidering(_form, new List<string>() { "RedegjoerelseSkredOgFlom" });
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.6.8.1.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.2.6.8.1.2 KravTilByggegrunnValidering - ERROR")]
        public void form_4528_2_6_8_1_2_KravTilByggegrunnValideringErrorTest()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.beskrivelseAvTiltak.type[0].kodeverdi = tiltakstype;
            _form.beskrivelseAvTiltak.type[0].kodebeskrivelse = "Nytt bygg - Under 70 m2 - ikke boligformål";


            _form.rammebetingelser.kravTilByggegrunn.flomutsattOmraade = true;
            _form.rammebetingelser.kravTilByggegrunn.flomutsattOmraadeSpecified = true;

            _form.rammebetingelser.kravTilByggegrunn.f1 = true;
            _form.rammebetingelser.kravTilByggegrunn.f1Specified = true;

            _validator.KravTilByggegrunnValidering(_form, new List<string>() { "*RedegjoerelseSkredOgFlom" });
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.6.8.1.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.2.6.8.2 KravTilByggegrunnValidering - ERROR")]
        public void form_4528_2_6_8_2_KravTilByggegrunnValideringErrorTest()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.beskrivelseAvTiltak.type[0].kodeverdi = tiltakstype;
            _form.beskrivelseAvTiltak.type[0].kodebeskrivelse = "Nytt bygg - Under 70 m2 - ikke boligformål";

            _form.rammebetingelser.kravTilByggegrunn.skredutsattOmraade = null;
            _form.rammebetingelser.kravTilByggegrunn.skredutsattOmraadeSpecified = false;

            _validator.KravTilByggegrunnValidering(_form, new List<string>() { "RedegjoerelseSkredOgFlom" });
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.6.8.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.2.6.8.2.1 KravTilByggegrunnValidering - ERROR")]
        public void form_4528_2_6_8_2_1_KravTilByggegrunnValideringErrorTest()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.beskrivelseAvTiltak.type[0].kodeverdi = tiltakstype;
            _form.beskrivelseAvTiltak.type[0].kodebeskrivelse = "Nytt bygg - Under 70 m2 - ikke boligformål";

            _form.rammebetingelser.kravTilByggegrunn.skredutsattOmraade = true;
            _form.rammebetingelser.kravTilByggegrunn.skredutsattOmraadeSpecified = true;

            _form.rammebetingelser.kravTilByggegrunn.s1 = null;
            _form.rammebetingelser.kravTilByggegrunn.s1Specified = false;

            _form.rammebetingelser.kravTilByggegrunn.s2 = null;
            _form.rammebetingelser.kravTilByggegrunn.s2Specified = false;

            _form.rammebetingelser.kravTilByggegrunn.s3 = null;
            _form.rammebetingelser.kravTilByggegrunn.s3Specified = false;

            _validator.KravTilByggegrunnValidering(_form, new List<string>() { "RedegjoerelseSkredOgFlom" });
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.6.8.2.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.2.6.8.2.2 KravTilByggegrunnValidering - ERROR")]
        public void form_4528_2_6_8_2_2_KravTilByggegrunnValideringErrorTest()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.beskrivelseAvTiltak.type[0].kodeverdi = tiltakstype;
            _form.beskrivelseAvTiltak.type[0].kodebeskrivelse = "Nytt bygg - Under 70 m2 - ikke boligformål";

            _form.rammebetingelser.kravTilByggegrunn.skredutsattOmraade = true;
            _form.rammebetingelser.kravTilByggegrunn.skredutsattOmraadeSpecified = true;

            _form.rammebetingelser.kravTilByggegrunn.s1 = true;
            _form.rammebetingelser.kravTilByggegrunn.s1Specified = true;

            _validator.KravTilByggegrunnValidering(_form, new List<string>() { "*RedegjoerelseSkredOgFlom" });
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.6.8.2.2").Should().NotBeNullOrEmpty();
            //result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.2.6.8.3 KravTilByggegrunnValidering - ERROR")]
        public void form_4528_2_6_8_3_KravTilByggegrunnValideringErrorTest()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.beskrivelseAvTiltak.type[0].kodeverdi = tiltakstype;
            _form.beskrivelseAvTiltak.type[0].kodebeskrivelse = "Nytt bygg - Under 70 m2 - ikke boligformål";

            _form.rammebetingelser.kravTilByggegrunn.miljoeforhold = null;
            _form.rammebetingelser.kravTilByggegrunn.miljoeforholdSpecified = false;

            _validator.KravTilByggegrunnValidering(_form, new List<string>() { "RedegjoerelseAndreNaturMiljoeforhold" });
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.6.8.3").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.2.6.8.3.1 KravTilByggegrunnValidering - ERROR")]
        public void form_4528_2_6_8_3_1_KravTilByggegrunnValideringErrorTest()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.beskrivelseAvTiltak.type[0].kodeverdi = tiltakstype;
            _form.beskrivelseAvTiltak.type[0].kodebeskrivelse = "Nytt bygg - Under 70 m2 - ikke boligformål";

            _form.rammebetingelser.kravTilByggegrunn.miljoeforhold = true;
            _form.rammebetingelser.kravTilByggegrunn.miljoeforholdSpecified = true;

            _validator.KravTilByggegrunnValidering(_form, new List<string>() { "*RedegjoerelseAndreNaturMiljoeforhold" });
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.6.8.3.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.2.6.9 AndrePlanerValidering - OK")]
        public void form_4528_2_6_9_AndrePlanerValideringOkTest()
        {
            _form.rammebetingelser.plan.andrePlaner[0].plantype.kodeverdi = "34";
            _form.rammebetingelser.plan.andrePlaner[0].plantype.kodebeskrivelse = "Områderegulering";

            _validator.AndrePlanerValidering(_form);
            var result = _validator.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4528.2.6.9.2 AndrePlanerValidering - Error")]
        public void form_4528_2_6_9_2_AndrePlanerValideringErrorTest()
        {
            _form.rammebetingelser.plan.andrePlaner[0].plantype.kodeverdi = null;

            _validator.AndrePlanerValidering(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.6.9.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.2.6.9 AndrePlanerValidering - Error")]
        public void form_4528_2_6_9_AndrePlanerValideringErrorTest()
        {
            _form.rammebetingelser.plan.andrePlaner[0].plantype.kodeverdi = "*34";
            _form.rammebetingelser.plan.andrePlaner[0].plantype.kodebeskrivelse = "Områderegulering";

            _validator.AndrePlanerValidering(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.6.9").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.2.6.9.1 AndrePlanerValidering - Error")]
        public void form_4528_2_6_9_1_AndrePlanerValideringErrorTest()
        {
            _form.rammebetingelser.plan.andrePlaner[0].plantype.kodeverdi = "34";
            _form.rammebetingelser.plan.andrePlaner[0].plantype.kodebeskrivelse = "Områderegulering";

            _form.rammebetingelser.plan.andrePlaner[0].navn = null;

            _validator.AndrePlanerValidering(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.6.9.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.2.6.10 PlasseringValidering - Ok")]
        public void form_4528_2_6_10_PlasseringValideringOkTest()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.beskrivelseAvTiltak.type[0].kodeverdi = tiltakstype;
            _form.beskrivelseAvTiltak.type[0].kodebeskrivelse = "Nytt bygg - Under 70 m2 - ikke boligformål";

            _validator.PlasseringValidering(_form, new List<string>() { "AvklaringHoyspent", "AvklaringVA" });
            var result = _validator.GetResult();

            result.messages.Count.Should().Be(0);
        }

        [Fact(DisplayName = "4528.2.6.10 PlasseringValidering - Error")]
        public void form_4528_2_6_10_PlasseringValideringErrorTest()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.beskrivelseAvTiltak.type[0].kodeverdi = tiltakstype;
            _form.beskrivelseAvTiltak.type[0].kodebeskrivelse = "Nytt bygg - Under 70 m2 - ikke boligformål";

            _form.rammebetingelser.plassering.konfliktVannOgAvloep = null;
            _form.rammebetingelser.plassering.konfliktVannOgAvloepSpecified = false;

            _validator.PlasseringValidering(_form, new List<string>() { "AvklaringHoyspent", "AvklaringVA" });
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.6.10").Should().NotBeNullOrEmpty();
            //result.messages.Count.Should().Be(1); problem with checklist 14.18
        }

        [Fact(DisplayName = "4528.2.6.10.1 PlasseringValidering - Error")]
        public void form_4528_2_6_10_1_PlasseringValideringErrorTest()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.beskrivelseAvTiltak.type[0].kodeverdi = tiltakstype;
            _form.beskrivelseAvTiltak.type[0].kodebeskrivelse = "Nytt bygg - Under 70 m2 - ikke boligformål";

            _form.rammebetingelser.plassering.konfliktHoeyspentkraftlinje = null;
            _form.rammebetingelser.plassering.konfliktHoeyspentkraftlinjeSpecified = false;

            _validator.PlasseringValidering(_form, new List<string>() { "AvklaringHoyspent", "AvklaringVA" });
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.6.10.1").Should().NotBeNullOrEmpty();
            //result.messages.Count.Should().Be(1); problem with checklist 14.17
        }

        [Fact(DisplayName = "4528.2.6.10.2 PlasseringValidering - Error")]
        public void form_4528_2_6_10_2_PlasseringValideringErrorTest()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.beskrivelseAvTiltak.type[0].kodebeskrivelse = "Nytt bygg - Under 70 m2 - ikke boligformål";
            _form.beskrivelseAvTiltak.type[0].kodebeskrivelse = "Nytt bygg - Under 70 m2 - ikke boligformål";

            _form.rammebetingelser.plassering.konfliktHoeyspentkraftlinje = true;
            _form.rammebetingelser.plassering.konfliktHoeyspentkraftlinjeSpecified = true;

            _validator.PlasseringValidering(_form, new List<string>() { "*AvklaringHoyspent", "AvklaringVA" });
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.6.10.2").Should().NotBeNullOrEmpty();
            //result.messages.Count.Should().Be(1); problem with checklist 14.17
        }

        [Fact(DisplayName = "4528.2.6.10.3 PlasseringValidering - Error")]
        public void form_4528_2_6_10_3_PlasseringValideringErrorTest()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.beskrivelseAvTiltak.type[0].kodeverdi = tiltakstype;
            _form.beskrivelseAvTiltak.type[0].kodebeskrivelse = "Nytt bygg - Under 70 m2 - ikke boligformål";

            _form.rammebetingelser.plassering.konfliktVannOgAvloep = true;
            _form.rammebetingelser.plassering.konfliktVannOgAvloepSpecified = true;

            _validator.PlasseringValidering(_form, new List<string>() { "AvklaringHoyspent", "*AvklaringVA" });
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.6.10.3").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.2.7 DispensasjonValidering - OK")]
        public void form_4528_2_7_DispensasjonValideringOkTest()
        {

            _validator.DispensasjonValidering(_form, new List<string>() { "Dispensasjonssoeknad" });
            var result = _validator.GetResult();

            result.messages.Count.Should().Be(0);
        }

        [Fact(DisplayName = "4528.2.7 DispensasjonValidering - Error")]
        public void form_4528_2_7_DispensasjonValideringErrorTest()
        {
            _form.dispensasjon[0].dispensasjonstype = null;

            _validator.DispensasjonValidering(_form, new List<string>() { "Dispensasjonssoeknad" });
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.7").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.2.7.1 DispensasjonValidering - Error")]
        public void form_4528_2_7_1_DispensasjonValideringErrorTest()
        {
            _form.dispensasjon[0].dispensasjonstype.kodeverdi = "*TEK";
            _form.dispensasjon[0].dispensasjonstype.kodebeskrivelse = "Byggteknisk forskrift med veiledning";

            _validator.DispensasjonValidering(_form, new List<string>() { "Dispensasjonssoeknad" });
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.7.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.2.7.2 DispensasjonValidering - Error")]
        public void form_4528_2_7_2_DispensasjonValideringErrorTest()
        {
            _form.dispensasjon[0].dispensasjonstype.kodeverdi = "TEK";
            _form.dispensasjon[0].dispensasjonstype.kodebeskrivelse = "Byggteknisk forskrift med veiledning";

            _form.dispensasjon[0].beskrivelse = null;


            _validator.DispensasjonValidering(_form, new List<string>() { "*Dispensasjonssoeknad" });
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.7.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.2.7.3 DispensasjonValidering - Error")]
        public void form_4528_2_7_3_DispensasjonValideringErrorTest()
        {
            _form.dispensasjon[0].dispensasjonstype.kodeverdi = "TEK";
            _form.dispensasjon[0].dispensasjonstype.kodebeskrivelse = "Byggteknisk forskrift med veiledning";

            _form.dispensasjon[0].begrunnelse = null;


            _validator.DispensasjonValidering(_form, new List<string>() { "*Dispensasjonssoeknad" });
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.7.3").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.2.8 MetadataValidering - Ok")]
        public void form_4528_2_8_MetadataValideringOkTest()
        {

            _validator.MetadataValidering(_form);
            var result = _validator.GetResult();

            result.messages.Count.Should().Be(0);
        }

        [Fact(DisplayName = "4528.2.8 MetadataValidering - Error")]
        public void form_4528_2_8_MetadataValideringErrorTest()
        {
            _form.metadata.fraSluttbrukersystem = null;

            _validator.MetadataValidering(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.8").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.2.9 VarslingValidering - Ok")]
        public void form_4528_2_9_VarslingValideringOkTest()
        {
            _formSetElements = new List<string>() { "GjenpartNabovarsel", "KvitteringNabovarsel", "KommentarNabomerknader" };
            _validator.VarslingValidering(_form, _formSetElements);

            var result = _validator.GetResult();

            result.messages.Count.Should().Be(0);
        }

        [Fact(DisplayName = "4528.2.9 VarslingValidering - Error")]
        public void form_4528_2_9_VarslingValideringErrorTest()
        {
            _form.varsling = null;

            _formSetElements = new List<string>() { "GjenpartNabovarsel", "KvitteringNabovarsel", "KommentarNabomerknader" };
            _validator.VarslingValidering(_form, _formSetElements);

            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.9").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.3.9.4 VarslingValidering - Error")]
        public void form_4528_3_9_4_VarslingValideringErrorTest()
        {
            _form.varsling.fritattFraNabovarsling = null;
            _form.varsling.fritattFraNabovarslingSpecified = false;

            _formSetElements = new List<string>() { "GjenpartNabovarsel", "KvitteringNabovarsel", "KommentarNabomerknader" };
            _validator.VarslingValidering(_form, _formSetElements);


            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.9.4").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.2.9.1 VarslingValidering - Error")]
        public void form_4528_2_9_1_VarslingValideringErrorTest()
        {
            _form.varsling.fritattFraNabovarsling = false;
            _form.varsling.fritattFraNabovarslingSpecified = true;

            _formSetElements = new List<string>() { "*GjenpartNabovarsel", "KvitteringNabovarsel", "KommentarNabomerknader", "Nabomerknader" };
            _validator.VarslingValidering(_form, _formSetElements);

            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.9.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.2.9.2 VarslingValidering - Error")]
        public void form_4528_2_9_2_VarslingValideringErrorTest()
        {
            _form.varsling.fritattFraNabovarsling = false;
            _form.varsling.fritattFraNabovarslingSpecified = true;

            _formSetElements = new List<string>() { "GjenpartNabovarsel", "*KvitteringNabovarsel", "KommentarNabomerknader", "Nabomerknader" };
            _validator.VarslingValidering(_form, _formSetElements);

            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.9.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.3.9.3 VarslingValidering - Error")]
        public void form_4528_3_9_3_VarslingValideringErrorTest()
        {
            _form.varsling.fritattFraNabovarsling = false;
            _form.varsling.fritattFraNabovarslingSpecified = true;

            _form.varsling.foreliggerMerknader = null;
            _form.varsling.foreliggerMerknaderSpecified = false;

            _formSetElements = new List<string>() { "GjenpartNabovarsel", "KvitteringNabovarsel", "KommentarNabomerknader", "Nabomerknader" };
            _validator.VarslingValidering(_form, _formSetElements);

            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.9.3").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.2.9.3.1 VarslingValidering - Error")]
        public void form_4528_2_9_3_1_VarslingValideringErrorTest()
        {
            _form.varsling.fritattFraNabovarsling = false;
            _form.varsling.fritattFraNabovarslingSpecified = true;

            _form.varsling.foreliggerMerknader = true;
            _form.varsling.foreliggerMerknaderSpecified = true;

            _form.varsling.antallMerknader = null;

            _formSetElements = new List<string>() { "GjenpartNabovarsel", "KvitteringNabovarsel", "KommentarNabomerknader", "Nabomerknader" };
            _validator.VarslingValidering(_form, _formSetElements);

            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.9.3.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.2.9.3.2 VarslingValidering - Error")]
        public void form_4528_2_9_3_2_VarslingValideringErrorTest()
        {
            _form.varsling.fritattFraNabovarsling = false;
            _form.varsling.fritattFraNabovarslingSpecified = true;

            _form.varsling.foreliggerMerknader = true;
            _form.varsling.foreliggerMerknaderSpecified = true;

            _form.varsling.vurderingAvMerknader = null;

            _formSetElements = new List<string>() { "GjenpartNabovarsel", "KvitteringNabovarsel", "*KommentarNabomerknader", "Nabomerknader" };
            _validator.VarslingValidering(_form, _formSetElements);

            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.9.3.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.3.10 FakturamottakerValidation - Ok")]
        public void form_4528_3_10_FakturamottakerValidationTest()
        {
            _validator.FakturamottakerValidation(_form);
            var result = _validator.GetResult();

            result.messages.Count.Should().Be(0);
        }

        [Fact(DisplayName = "4528.3.10 FakturamottakerValidation - Error")]
        public void form_4528_3_10_FakturamottakerValidationErrorTest()
        {
            _form.fakturamottaker = null;

            _form.tiltakshaver.partstype.kodeverdi = "Foretak";
            _form.tiltakshaver.partstype.kodebeskrivelse = "Foretak";

            _validator.FakturamottakerValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.10").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.3.10.4 FakturamottakerValidation - Error")]
        public void form_4528_3_10_4_FakturamottakerValidationErrorTest()
        {

            _form.tiltakshaver.partstype.kodeverdi = "Foretak";
            _form.tiltakshaver.partstype.kodebeskrivelse = "Foretak";

            _form.fakturamottaker.ehfFaktura = true;
            _form.fakturamottaker.ehfFakturaSpecified = true;

            _form.fakturamottaker.fakturaPapir = true;
            _form.fakturamottaker.fakturaPapirSpecified = true;

            _validator.FakturamottakerValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.10.4").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.3.10.1 FakturamottakerValidation - Error")]
        public void form_4528_3_10_1_FakturamottakerValidationErrorTest()
        {

            _form.tiltakshaver.partstype.kodeverdi = "Foretak";
            _form.tiltakshaver.partstype.kodebeskrivelse = "Foretak";

            _form.fakturamottaker.ehfFaktura = true;
            _form.fakturamottaker.ehfFakturaSpecified = true;

            _form.fakturamottaker.fakturaPapir = false;
            _form.fakturamottaker.fakturaPapirSpecified = true;

            _form.fakturamottaker.organisasjonsnummer = null;

            _validator.FakturamottakerValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.10.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.3.10.2 FakturamottakerValidation - Error")]
        public void form_4528_3_10_2_FakturamottakerValidationErrorTest()
        {

            _form.tiltakshaver.partstype.kodeverdi = "Foretak";
            _form.tiltakshaver.partstype.kodebeskrivelse = "Foretak";

            _form.fakturamottaker.ehfFaktura = true;
            _form.fakturamottaker.ehfFakturaSpecified = true;

            _form.fakturamottaker.fakturaPapir = false;
            _form.fakturamottaker.fakturaPapirSpecified = true;

            _form.fakturamottaker.navn = null;

            _validator.FakturamottakerValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.10.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.3.10.3 FakturamottakerValidation - Error")]
        public void form_4528_3_10_3_FakturamottakerValidationErrorTest()
        {

            _form.tiltakshaver.partstype.kodeverdi = "Foretak";
            _form.tiltakshaver.partstype.kodebeskrivelse = "Foretak";

            _form.fakturamottaker.ehfFaktura = false;
            _form.fakturamottaker.ehfFakturaSpecified = true;

            _form.fakturamottaker.fakturaPapir = true;
            _form.fakturamottaker.fakturaPapirSpecified = true;

            _form.fakturamottaker.adresse = null;

            _validator.FakturamottakerValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.10.3").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.3.10.3.1 FakturamottakerValidation - Error")]
        public void form_4528_3_10_3_1_FakturamottakerValidationErrorTest()
        {

            _form.tiltakshaver.partstype.kodeverdi = "Foretak";
            _form.tiltakshaver.partstype.kodebeskrivelse = "Foretak";

            _form.fakturamottaker.ehfFaktura = false;
            _form.fakturamottaker.ehfFakturaSpecified = true;

            _form.fakturamottaker.fakturaPapir = true;
            _form.fakturamottaker.fakturaPapirSpecified = true;

            _form.fakturamottaker.adresse.adresselinje1 = null;

            _validator.FakturamottakerValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.10.3.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.3.10.3.2 FakturamottakerValidation - Error")]
        public void form_4528_3_10_3_2_FakturamottakerValidationErrorTest()
        {

            _form.tiltakshaver.partstype.kodeverdi = "Foretak";
            _form.tiltakshaver.partstype.kodebeskrivelse = "Foretak";

            _form.fakturamottaker.ehfFaktura = false;
            _form.fakturamottaker.ehfFakturaSpecified = true;

            _form.fakturamottaker.fakturaPapir = true;
            _form.fakturamottaker.fakturaPapirSpecified = true;

            _form.fakturamottaker.adresse.postnr = null;

            _validator.FakturamottakerValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.10.3.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.3.10.3.3 FakturamottakerValidation - Error")]
        public void form_4528_3_10_3_3_FakturamottakerValidationErrorTest()
        {

            _form.tiltakshaver.partstype.kodeverdi = "Foretak";
            _form.tiltakshaver.partstype.kodebeskrivelse = "Foretak";

            _form.fakturamottaker.ehfFaktura = false;
            _form.fakturamottaker.ehfFakturaSpecified = true;

            _form.fakturamottaker.fakturaPapir = true;
            _form.fakturamottaker.fakturaPapirSpecified = true;

            _form.fakturamottaker.adresse.poststed = null;

            _validator.FakturamottakerValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.10.3.3").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.3.11 AnsvarsrettSoekerValidation - OK")]
        public void form_4528_3_11_AnsvarsrettSoekerValidation_Ok_Test()
        {
            _validator.AnsvarsrettSoekerValidation(_form, _formSetElements);
            var result = _validator.GetResult();

            result.messages.Count.Should().Be(0);
        }

        [Fact(DisplayName = "4528.3.11 AnsvarsrettSoekerValidation - Error")]
        public void form_4528_3_11_AnsvarsrettSoekerValidation_Error_Test()
        {
            _form.ansvarsrettSoeker = null;

            _validator.AnsvarsrettSoekerValidation(_form, _formSetElements);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.11").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.3.11.4 AnsvarsrettSoekerValidation - Error")]
        public void form_4528_3_11_4_AnsvarsrettSoekerValidation_Error_Test()
        {
            _form.ansvarsrettSoeker.erklaeringAnsvar = null;
            _form.ansvarsrettSoeker.erklaeringAnsvarSpecified = false;

            _validator.AnsvarsrettSoekerValidation(_form, _formSetElements);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.11.4").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.3.11.4 (1) AnsvarsrettSoekerValidation - Error")]
        public void form_4528_3_11_4_a_AnsvarsrettSoekerValidation_Error_Test()
        {
            _form.ansvarsrettSoeker.erklaeringAnsvar = false;
            _form.ansvarsrettSoeker.erklaeringAnsvarSpecified = true;

            _validator.AnsvarsrettSoekerValidation(_form, _formSetElements);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.11.4").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.3.11.1 AnsvarsrettSoekerValidation - Error")]
        public void form_4528_3_11_1_AnsvarsrettSoekerValidation_Error_Test()
        {
            _form.ansvarsrettSoeker.tiltaksklasse = null;

            _validator.AnsvarsrettSoekerValidation(_form, _formSetElements);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.11.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.3.11.2 AnsvarsrettSoekerValidation - Error")]
        public void form_4528_3_11_2_AnsvarsrettSoekerValidation_Error_Test()
        {
            _form.ansvarsrettSoeker.tiltaksklasse.kodeverdi = "*2";
            _form.ansvarsrettSoeker.tiltaksklasse.kodebeskrivelse = "2";

            _validator.AnsvarsrettSoekerValidation(_form, _formSetElements);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.11.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.3.11.5 AnsvarsrettSoekerValidation - Error")]
        public void form_4528_3_11_5_AnsvarsrettSoekerValidation_Error_Test()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.beskrivelseAvTiltak.type[0].kodeverdi = tiltakstype;
            _form.beskrivelseAvTiltak.type[0].kodebeskrivelse = "Nytt bygg - Under 70 m2 - ikke boligformål";

            _form.ansvarligSoeker.partstype.kodeverdi = "Foretak";
            _form.ansvarligSoeker.partstype.kodebeskrivelse = "Foretak";

            _form.ansvarsrettSoeker.tiltaksklasse.kodeverdi = "2";
            _form.ansvarsrettSoeker.tiltaksklasse.kodebeskrivelse = "2";


            _formSetElements = new List<string>() { "*ErklaeringAnsvarsrett" };

            _validator.AnsvarsrettSoekerValidation(_form, _formSetElements);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.11.5").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.3.11.5 (1) AnsvarsrettSoekerValidation - Error")]
        public void form_4528_3_11_5_a_AnsvarsrettSoekerValidation_Error_Test()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.beskrivelseAvTiltak.type[0].kodeverdi = tiltakstype;
            _form.beskrivelseAvTiltak.type[0].kodebeskrivelse = "Nytt bygg - Under 70 m2 - ikke boligformål";

            _form.ansvarligSoeker.partstype.kodeverdi = "Foretak";
            _form.ansvarligSoeker.partstype.kodebeskrivelse = "Foretak";

            _form.ansvarsrettSoeker.tiltaksklasse.kodeverdi = "3";
            _form.ansvarsrettSoeker.tiltaksklasse.kodebeskrivelse = "3";


            _formSetElements = new List<string>() { "*ErklaeringAnsvarsrett" };

            _validator.AnsvarsrettSoekerValidation(_form, _formSetElements);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.11.5").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.3.11.5.1 AnsvarsrettSoekerValidation - Error")]
        public void form_4528_3_11_5_1_AnsvarsrettSoekerValidation_Error_Test()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.beskrivelseAvTiltak.type[0].kodeverdi = tiltakstype;
            _form.beskrivelseAvTiltak.type[0].kodebeskrivelse = "Nytt bygg - Under 70 m2 - ikke boligformål";

            _form.ansvarligSoeker.partstype.kodeverdi = "Foretak";
            _form.ansvarligSoeker.partstype.kodebeskrivelse = "Foretak";

            _form.ansvarsrettSoeker.tiltaksklasse.kodeverdi = "1";
            _form.ansvarsrettSoeker.tiltaksklasse.kodebeskrivelse = "1";

            _formSetElements.Remove("ErklaeringAnsvarsrett");
            _formSetElements.Remove("SoknadOmAnsvarsrettTiltaksklasse1");

            _validator.AnsvarsrettSoekerValidation(_form, _formSetElements);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.11.5.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.3.11.3 AnsvarsrettSoekerValidation - Error")]
        public void form_4528_3_11_3_AnsvarsrettSoekerValidation_Error_Test()
        {
            _form.ansvarsrettSoeker.harSentralGodkjenning = null;
            _form.ansvarsrettSoeker.harSentralGodkjenningSpecified = false;

            _validator.AnsvarsrettSoekerValidation(_form, _formSetElements);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.11.3").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.1.12 AnsvarligSokerValidation - Ok")]
        public void form_4528_1_12_AnsvarligSokerValidation_Ok_Test()
        {
            _validator.AnsvarligSokerValidation(_form);
            var result = _validator.GetResult();

            result.messages.Count.Should().Be(0);
        }

        [Fact(DisplayName = "4528.1.12 AnsvarligSokerValidation - Error")]
        public void form_4528_1_12_AnsvarligSokerValidation_Error_Test()
        {
            _form.ansvarsrettSoeker = null;

            _validator.AnsvarsrettSoekerValidation(_form, _formSetElements);
            var result = _validator.GetResult();

            _validator.AnsvarligSokerValidation(_form);
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.1.12.1 AnsvarligSokerValidation - Error")]
        public void form_4528_1_12_1_AnsvarligSokerValidation_Error_Test()
        {
            _form.ansvarligSoeker.partstype = null;

            _validator.AnsvarligSokerValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.1.12.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.1.12.2 AnsvarligSokerValidation - Error")]
        public void form_4528_1_12_2_AnsvarligSokerValidation_Error_Test()
        {
            _form.ansvarligSoeker.partstype.kodeverdi = "*Foretak";
            _form.ansvarligSoeker.partstype.kodebeskrivelse = "Foretak";

            _validator.AnsvarligSokerValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.1.12.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.3.12.3 AnsvarligSokerValidation - Error")]
        public void form_4528_3_12_3_AnsvarligSokerValidation_Error_Test()
        {
            _form.ansvarligSoeker.partstype.kodeverdi = "Privatperson";
            _form.ansvarligSoeker.partstype.kodebeskrivelse = "Privatperson";

            _form.ansvarligSoeker.foedselsnummer = null;


            _validator.AnsvarligSokerValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.12.3").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.3.12.3.1 AnsvarligSokerValidation - Error")]
        public void form_4528_3_12_3_1_AnsvarligSokerValidation_Error_Test()
        {
            _form.ansvarligSoeker.partstype.kodeverdi = "Privatperson";
            _form.ansvarligSoeker.partstype.kodebeskrivelse = "Privatperson";

            _form.ansvarligSoeker.foedselsnummer = "25525401530";

            _validator.AnsvarligSokerValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.12.3.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.3.12.3.2 AnsvarligSokerValidation - Error")]
        public void form_4528_3_12_3_2_AnsvarligSokerValidation_Error_Test()
        {
            _form.ansvarligSoeker.partstype.kodeverdi = "Privatperson";
            _form.ansvarligSoeker.partstype.kodebeskrivelse = "Privatperson";

            _form.ansvarligSoeker.foedselsnummer = "5125401530";

            _validator.AnsvarligSokerValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.12.3.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.3.12.3.3 AnsvarligSokerValidation - Error")]
        public void form_4528_3_12_3_3_AnsvarligSokerValidation_Error_Test()
        {
            _form.ansvarligSoeker.partstype.kodeverdi = "Privatperson";
            _form.ansvarligSoeker.partstype.kodebeskrivelse = "Privatperson";

            _form.ansvarligSoeker.foedselsnummer = "vcsdvsavnaskjvnaksjvnakjsvniwagfbnwajgfbnijfgnbeiwrgfbiwebifabuiwbgfiuwf";

            _validator.AnsvarligSokerValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.12.3.3").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.3.12.3.3 AnsvarligSokerValidation - Ok")]
        public void form_4528_3_12_3_3_AnsvarligSokerValidation_ok_Test()
        {
            _form.ansvarligSoeker.partstype.kodeverdi = "Privatperson";
            _form.ansvarligSoeker.partstype.kodebeskrivelse = "Privatperson";

            _form.ansvarligSoeker.foedselsnummer = "yFNSdYXRoUXyobi8ousqk/tC6pOQ9beKApBzDPonOy1xkMNBLPqxpBXvd3a7XUsJTdJjluVyjAe1viEosL36QdsSzBJm7kMa7frpHfOVyyF6lfzUQSijBcTbY7DCm/Frc3hGM81BcmH6WX8e410oOuZh6lk1t7H93bmu8BknxXs=";

            _validator.AnsvarligSokerValidation(_form);
            var result = _validator.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4528.2.12.4 AnsvarligSokerValidation - Error")]
        public void form_4528_2_12_4_AnsvarligSokerValidation_Error_Test()
        {
            _form.ansvarligSoeker.partstype.kodeverdi = "Foretak";
            _form.ansvarligSoeker.partstype.kodebeskrivelse = "Foretak";

            _form.ansvarligSoeker.organisasjonsnummer = null;

            _validator.AnsvarligSokerValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.12.4").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4528.2.12.4.1 AnsvarligSokerValidation - Error")]
        public void form_4528_2_12_4_1_AnsvarligSokerValidation_Error_Test()
        {
            _form.ansvarligSoeker.partstype.kodeverdi = "Foretak";
            _form.ansvarligSoeker.partstype.kodebeskrivelse = "Foretak";

            _form.ansvarligSoeker.organisasjonsnummer = "557476068";

            _validator.AnsvarligSokerValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.12.4.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4528.3.12.4.2 AnsvarligSokerValidation - Error")]
        public void form_4528_3_12_4_2_AnsvarligSokerValidation_Error_Test()
        {
            _form.ansvarligSoeker.partstype.kodeverdi = "Foretak";
            _form.ansvarligSoeker.partstype.kodebeskrivelse = "Foretak";

            _form.ansvarligSoeker.organisasjonsnummer = "55974760673";

            _validator.AnsvarligSokerValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.12.4.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4528.2.12.4.3 AnsvarligSokerValidation - Error")]
        public void form_4528_2_12_4_3_AnsvarligSokerValidation_Error_Test()
        {
            _form.ansvarligSoeker.partstype.kodeverdi = "Foretak";
            _form.ansvarligSoeker.partstype.kodebeskrivelse = "Foretak";

            _form.ansvarligSoeker.kontaktperson = null;

            _validator.AnsvarligSokerValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.12.4.3").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4528.3.12.4.3.1 AnsvarligSokerValidation - Error")]
        public void form_4528_3_12_4_3_1_AnsvarligSokerValidation_Error_Test()
        {
            _form.ansvarligSoeker.partstype.kodeverdi = "Foretak";
            _form.ansvarligSoeker.partstype.kodebeskrivelse = "Foretak";

            _form.ansvarligSoeker.kontaktperson.navn = null;

            _validator.AnsvarligSokerValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.12.4.3.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4528.3.12.4.3.2 AnsvarligSokerValidation - Error")]
        public void form_4528_3_12_4_3_2_AnsvarligSokerValidation_Error_Test()
        {
            _form.ansvarligSoeker.partstype.kodeverdi = "Foretak";
            _form.ansvarligSoeker.partstype.kodebeskrivelse = "Foretak";

            _form.ansvarligSoeker.kontaktperson.telefonnummer = null;
            _form.ansvarligSoeker.kontaktperson.mobilnummer = null;

            _validator.AnsvarligSokerValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.12.4.3.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4528.3.12.4.3.3 AnsvarligSokerValidation - Error")]
        public void form_4528_3_12_4_3_3_AnsvarligSokerValidation_Error_Test()
        {
            _form.ansvarligSoeker.partstype.kodeverdi = "Foretak";
            _form.ansvarligSoeker.partstype.kodebeskrivelse = "Foretak";

            _form.ansvarligSoeker.kontaktperson.telefonnummer = "555 123456";

            _validator.AnsvarligSokerValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.12.4.3.3").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4528.3.12.4.3.4 AnsvarligSokerValidation - Error")]
        public void form_4528_3_12_4_3_4_AnsvarligSokerValidation_Error_Test()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "Oslo");

            _form.ansvarligSoeker.partstype.kodeverdi = "Foretak";
            _form.ansvarligSoeker.partstype.kodebeskrivelse = "Foretak";

            _form.ansvarligSoeker.kontaktperson.mobilnummer = "555 123456";

            _validator.AnsvarligSokerValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.12.4.3.4").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4528.3.12.4.3.5 AnsvarligSokerValidation - Error")]
        public void form_4528_3_12_4_3_5_AnsvarligSokerValidation_Error_Test()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "Oslo");

            _form.ansvarligSoeker.partstype.kodeverdi = "Foretak";
            _form.ansvarligSoeker.partstype.kodebeskrivelse = "Foretak";

            _form.ansvarligSoeker.kontaktperson.epost = null;

            _validator.AnsvarligSokerValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.12.4.3.5").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4528.1.12.5 AnsvarligSokerValidation - Error")]
        public void form_4528_1_12_5_AnsvarligSokerValidation_Error_Test()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "Oslo");

            _form.ansvarligSoeker.adresse = null;

            _validator.AnsvarligSokerValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.1.12.5").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4528.3.12.5.1 AnsvarligSokerValidation - Error")]
        public void form_4528_3_12_5_1_AnsvarligSokerValidation_Error_Test()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "Oslo");

            _form.ansvarligSoeker.adresse.adresselinje1 = null;

            _validator.AnsvarligSokerValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.12.5.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4528.3.12.5.2 AnsvarligSokerValidation - Error")]
        public void form_4528_3_12_5_2_AnsvarligSokerValidation_Error_Test()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "Oslo");

            _form.ansvarligSoeker.adresse.landkode = "*AR";

            _validator.AnsvarligSokerValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.12.5.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4528.1.12.5.3 AnsvarligSokerValidation - Error")]
        public void form_4528_1_12_5_3_AnsvarligSokerValidation_Error_Test()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "Oslo");

            _form.ansvarligSoeker.adresse.postnr = null;

            _validator.AnsvarligSokerValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.1.12.5.3").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4528.3.12.5.3.1 AnsvarligSokerValidation - Error")]
        public void form_4528_3_12_5_3_1_AnsvarligSokerValidation_Error_Test()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "Oslo");

            _form.ansvarligSoeker.adresse.postnr = "2008a";

            _validator.AnsvarligSokerValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.12.5.3.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4528.3.12.5.3.2 AnsvarligSokerValidation - Error")]
        public void form_4528_3_12_5_3_2_AnsvarligSokerValidation_Error_Test()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(false, "POBOX", "Oslo");

            _validator.AnsvarligSokerValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.12.5.3.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4528.3.12.5.3.3 AnsvarligSokerValidation - Error")]
        public void form_4528_3_12_5_3_3_AnsvarligSokerValidation_Error_Test()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "*Oslo");

            _validator.AnsvarligSokerValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.12.5.3.3").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4528.3.12.5.3.4 AnsvarligSokerValidation - Error")]
        public void form_4528_3_12_5_3_4_AnsvarligSokerValidation_Error_Test()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult();

            _validator.AnsvarligSokerValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.12.5.3.4").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4528.3.12.6 AnsvarligSokerValidation - Error")]
        public void form_4528_3_12_6_AnsvarligSokerValidation_Error_Test()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "Oslo");

            _form.ansvarligSoeker.navn = null;

            _validator.AnsvarligSokerValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.12.6").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4528.3.12.7 AnsvarligSokerValidation - Error")]
        public void form_4528_3_12_7_AnsvarligSokerValidation_Error_Test()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "Oslo");

            _form.ansvarligSoeker.telefonnummer = null;
            _form.ansvarligSoeker.mobilnummer = null;

            _validator.AnsvarligSokerValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.12.7").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4528.3.12.8 AnsvarligSokerValidation - Error")]
        public void form_4528_3_12_8_AnsvarligSokerValidation_Error_Test()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "Oslo");

            _form.ansvarligSoeker.telefonnummer = "555 012345";

            _validator.AnsvarligSokerValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.12.8").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4528.3.12.9 AnsvarligSokerValidation - Error")]
        public void form_4528_3_12_9_AnsvarligSokerValidation_Error_Test()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "Oslo");

            _form.ansvarligSoeker.mobilnummer = "555 012345";

            _validator.AnsvarligSokerValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.12.9").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4528.3.12.10 AnsvarligSokerValidation - Error")]
        public void form_4528_3_12_10_AnsvarligSokerValidation_Error_Test()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "Oslo");

            _form.ansvarligSoeker.epost = null;

            _validator.AnsvarligSokerValidation(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.3.12.10").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4528.2.13.1 SituasjonsplanValidering - Ok")]
        public void form_4528_2_13_1_SituasjonsplanValidering_Ok_Test()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.beskrivelseAvTiltak.type[0].kodeverdi = tiltakstype;
            _form.beskrivelseAvTiltak.type[0].kodebeskrivelse = "Nytt bygg - Under 70 m2 - ikke boligformål";

            _formSetElements = new List<string>() { "Situasjonsplan" };

            _validator.SituasjonsplanValidering(_formSetElements);
            var result = _validator.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4528.2.13.1 SituasjonsplanValidering - Error")]
        public void form_4528_2_13_1_SituasjonsplanValidering_Error_Test()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.beskrivelseAvTiltak.type[0].kodeverdi = tiltakstype;
            _form.beskrivelseAvTiltak.type[0].kodebeskrivelse = "Nytt bygg - Under 70 m2 - ikke boligformål";

            _formSetElements = new List<string>() { "*Situasjonsplan" };

            _validator.SituasjonsplanValidering(_formSetElements);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.13.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4528.2.13.2 TegningValidering - Ok")]
        public void form_4528_2_13_2_TegningValidering_Ok_Test()
        {
            _formSetElements = new List<string>()
            {
                "TegningEksisterendeSnitt",
                "TegningNyttSnitt",
                "TegningEksisterendePlan",
                "TegningNyPlan",
                "TegningEksisterendeFasade",
                "TegningNyFasade"
            };

            _validator.TegningValidering(_formSetElements);
            var result = _validator.GetResult();
            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4528.2.13.2 TegningValidering - Error")]
        public void form_4528_2_13_2_TegningValidering_Error_Test()
        {
            _formSetElements = new List<string>()
            {
                "*TegningEksisterendeSnitt",
                "*TegningNyttSnitt",
                "*TegningEksisterendePlan",
                "*TegningNyPlan",
                "*TegningEksisterendeFasade",
                "*TegningNyFasade"
            };

            _validator.TegningValidering(_formSetElements);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.13.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4528.2.13.3 PlantegningValidering - Ok")]
        public void form_4528_2_13_3_PlantegningValidering_Ok_Test()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.beskrivelseAvTiltak.type[0].kodeverdi = tiltakstype;
            _form.beskrivelseAvTiltak.type[0].kodebeskrivelse = "Nytt bygg - Under 70 m2 - ikke boligformål";

            _formSetElements = new List<string>()
            {
                "TegningEksisterendePlan",
                "TegningNyPlan"
            };

            _validator.PlantegningValidering(_formSetElements);
            var result = _validator.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4528.2.13.3 PlantegningValidering - Error")]
        public void form_4528_2_13_3_PlantegningValidering_Error_Test()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.beskrivelseAvTiltak.type[0].kodeverdi = tiltakstype;
            _form.beskrivelseAvTiltak.type[0].kodebeskrivelse = "Nytt bygg - Under 70 m2 - ikke boligformål";

            _formSetElements = new List<string>()
            {
                "*TegningEksisterendePlan",
                "*TegningNyPlan"
            };

            _validator.PlantegningValidering(_formSetElements);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.13.3").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4528.2.13.4 SnitttegningerValidering - Ok")]
        public void form_4528_2_13_4_SnitttegningerValidering_Ok_Test()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.beskrivelseAvTiltak.type[0].kodeverdi = tiltakstype;
            _form.beskrivelseAvTiltak.type[0].kodebeskrivelse = "Nytt bygg - Under 70 m2 - ikke boligformål";

            _formSetElements = new List<string>()
            {
                "TegningEksisterendeSnitt",
                "TegningNyttSnitt"
            };

            _validator.SnitttegningerValidering(_formSetElements);
            var result = _validator.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4528.2.13.4 SnitttegningerValidering - Error")]
        public void form_4528_2_13_4_SnitttegningerValidering_Error_Test()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.beskrivelseAvTiltak.type[0].kodeverdi = tiltakstype;
            _form.beskrivelseAvTiltak.type[0].kodebeskrivelse = "Nytt bygg - Under 70 m2 - ikke boligformål";

            _formSetElements = new List<string>()
            {
                "*TegningEksisterendeSnitt",
                "*TegningNyttSnitt"
            };

            _validator.SnitttegningerValidering(_formSetElements);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.13.4").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4528.2.13.5 FasadetegningerValidering - Ok")]
        public void form_4528_2_13_5_FasadetegningerValidering_Ok_Test()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.beskrivelseAvTiltak.type[0].kodeverdi = tiltakstype;
            _form.beskrivelseAvTiltak.type[0].kodebeskrivelse = "Nytt bygg - Under 70 m2 - ikke boligformål";

            _formSetElements = new List<string>()
            {
                "TegningEksisterendeFasade",
                "TegningNyFasade"
            };

            _validator.FasadetegningerValidering(_formSetElements);
            var result = _validator.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4528.2.13.5 FasadetegningerValidering - Error")]
        public void form_4528_2_13_5_FasadetegningerValidering_Error_Test()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.beskrivelseAvTiltak.type[0].kodeverdi = tiltakstype;
            _form.beskrivelseAvTiltak.type[0].kodebeskrivelse = "Nytt bygg - Under 70 m2 - ikke boligformål";

            _formSetElements = new List<string>()
            {
                "*TegningEksisterendeFasade",
                "*TegningNyFasade"
            };

            _validator.FasadetegningerValidering(_formSetElements);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.13.5").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4528.2.13.7 AnsvarValidering - Ok")]
        public void form_4528_2_13_7_AnsvarValidering_Ok_Test()
        {

            _form.ansvarligSoeker.partstype.kodeverdi = "Privatperson";
            _form.ansvarligSoeker.partstype.kodebeskrivelse = "Privatperson";

            _formSetElements = new List<string>()
            {
                "Gjennomføringsplan",
                "Gjennomfoeringsplan",
                "GjennomføringsplanV4",
                "AnsvarsrettSelvbygger",
                "Søknad om ansvarsrett for selvbygger",
            };
            _validator.AnsvarValidering(_form, _formSetElements);
            var result = _validator.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4528.2.13.7 AnsvarValidering - Error")]
        public void form_4528_2_13_7_AnsvarValidering_Error_Test()
        {

            _form.ansvarligSoeker.partstype.kodeverdi = "Privatperson";
            _form.ansvarligSoeker.partstype.kodebeskrivelse = "Privatperson";

            _formSetElements = new List<string>()
            {
                "AnsvarsrettSelvbygger",
                "Søknad om ansvarsrett for selvbygger",
            };
            _validator.AnsvarValidering(_form, _formSetElements);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.13.7").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4528.2.13.7.1 AnsvarValidering - Error")]
        public void form_4528_2_13_7_1_AnsvarValidering_Error_Test()
        {

            _form.ansvarligSoeker.partstype.kodeverdi = "Privatperson";
            _form.ansvarligSoeker.partstype.kodebeskrivelse = "Privatperson";

            _formSetElements = new List<string>()
            {
                "Gjennomføringsplan",
                "Gjennomfoeringsplan",
                "GjennomføringsplanV4",
            };
            _validator.AnsvarValidering(_form, _formSetElements);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.13.7.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4528.2.13.6 AnsvarValidering - Error")]
        public void form_4528_2_13_6_AnsvarValidering_Error_Test()
        {

            _form.ansvarligSoeker.partstype.kodeverdi = "Foretak";
            _form.ansvarligSoeker.partstype.kodebeskrivelse = "Foretak";

            _formSetElements = null;
            _validator.AnsvarValidering(_form, _formSetElements);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.13.6").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4528.2.13.8 MatrikkelopplysningValidering - Ok")]
        public void form_4528_2_13_8_MatrikkelopplysningValidering_Ok_Test()
        {
            var tiltakstype = "nyttbyggunder70m2";
            _validator._tiltakstyperISoeknad = new List<string>() { tiltakstype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltakstype };

            _form.beskrivelseAvTiltak.type[0].kodeverdi = tiltakstype;
            _form.beskrivelseAvTiltak.type[0].kodebeskrivelse = "Nytt bygg - Under 70 m2 - ikke boligformål";

            _formSetElements = new List<string>()
            {
                "Matrikkelopplysninger",
            };

            _validator.MatrikkelopplysningValidering(_formSetElements);
            var result = _validator.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4528.2.13.8 MatrikkelopplysningValidering - Error")]
        public void form_4528_2_13_8_MatrikkelopplysningValidering_Error_Test()
        {
            _formSetElements = null;
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { "nyttbyggboligformal" };

            _validator.MatrikkelopplysningValidering(_formSetElements);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.13.8").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4528.2.13.9 ByggesaksBIMValidering - Ok")]
        public void form_4528_2_13_9_ByggesaksBIMValidering_Ok_Test()
        {

            _formSetElements = new List<string>()
            {
                "ByggesaksBIM",
                "Vedleggsopplysninger",
            };

            _validator.ByggesaksBIMValidering(_formSetElements);
            var result = _validator.GetResult();
            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4528.2.13.9 ByggesaksBIMValidering - Error")]
        public void form_4528_2_13_9_ByggesaksBIMValidering_Error_Test()
        {

            _formSetElements = new List<string>()
            {
                "ByggesaksBIM",
            };

            _validator.ByggesaksBIMValidering(_formSetElements);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.13.9").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4528.2.15.1 OppfyllesVilkaarFor3UkersfristValidering - Ok")]
        public void form_4528_2_15_1_OppfyllesVilkaarFor3UkersfristValidering_Ok_Test()
        {
            _form.rammebetingelser.generelleVilkaar.oppfyllesVilkaarFor3Ukersfrist = true;
            _form.rammebetingelser.generelleVilkaar.oppfyllesVilkaarFor3UkersfristSpecified = true;

            _form.rammebetingelser.generelleVilkaar.beroererTidligere1850 = true;
            _form.rammebetingelser.generelleVilkaar.beroererTidligere1850Specified = true;

            _form.rammebetingelser.generelleVilkaar.beroererArbeidsplasser = true;
            _form.rammebetingelser.generelleVilkaar.beroererArbeidsplasserSpecified = true;

            _form.varsling.foreliggerMerknader = false;

            _form.dispensasjon = null;

            _formSetElements = new List<string>()
            {
                "UttalelseKulturminnemyndighet",
                "SamtykkeArbeidstilsynet",
            };

            _validator.OppfyllesVilkaarFor3UkersfristValidering(_form, _formSetElements);
            var result = _validator.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4528.2.15.1 OppfyllesVilkaarFor3UkersfristValidering - Error")]
        public void form_4528_2_15_1_OppfyllesVilkaarFor3UkersfristValidering_Error_Test()
        {
            _form.rammebetingelser.generelleVilkaar.oppfyllesVilkaarFor3Ukersfrist = true;
            _form.rammebetingelser.generelleVilkaar.oppfyllesVilkaarFor3UkersfristSpecified = true;

            _form.rammebetingelser.generelleVilkaar.beroererTidligere1850 = true;
            _form.rammebetingelser.generelleVilkaar.beroererTidligere1850Specified = true;

            _form.rammebetingelser.generelleVilkaar.beroererArbeidsplasser = true;
            _form.rammebetingelser.generelleVilkaar.beroererArbeidsplasserSpecified = true;

            _form.varsling.foreliggerMerknader = false;


            _form.dispensasjon = null;

            _formSetElements = new List<string>()
            {
                "SamtykkeArbeidstilsynet",
            };

            _validator.OppfyllesVilkaarFor3UkersfristValidering(_form, _formSetElements);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.15.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4528.2.15.2 OppfyllesVilkaarFor3UkersfristValidering - Error")]
        public void form_4528_2_15_2_OppfyllesVilkaarFor3UkersfristValidering_Error_Test()
        {
            _form.rammebetingelser.generelleVilkaar.oppfyllesVilkaarFor3Ukersfrist = true;
            _form.rammebetingelser.generelleVilkaar.oppfyllesVilkaarFor3UkersfristSpecified = true;

            _form.rammebetingelser.generelleVilkaar.beroererTidligere1850 = true;
            _form.rammebetingelser.generelleVilkaar.beroererTidligere1850Specified = true;

            _form.rammebetingelser.generelleVilkaar.beroererArbeidsplasser = true;
            _form.rammebetingelser.generelleVilkaar.beroererArbeidsplasserSpecified = true;

            _form.varsling.foreliggerMerknader = false;


            _form.dispensasjon = null;

            _formSetElements = new List<string>()
            {
                "UttalelseKulturminnemyndighet",
            };

            _validator.OppfyllesVilkaarFor3UkersfristValidering(_form, _formSetElements);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.15.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4528.2.15.3 OppfyllesVilkaarFor3UkersfristValidering - Error")]
        public void form_4528_2_15_3_OppfyllesVilkaarFor3UkersfristValidering_Error_Test()
        {
            _form.rammebetingelser.generelleVilkaar.oppfyllesVilkaarFor3Ukersfrist = true;
            _form.rammebetingelser.generelleVilkaar.oppfyllesVilkaarFor3UkersfristSpecified = true;

            _form.rammebetingelser.generelleVilkaar.beroererTidligere1850 = true;
            _form.rammebetingelser.generelleVilkaar.beroererTidligere1850Specified = true;

            _form.rammebetingelser.generelleVilkaar.beroererArbeidsplasser = true;
            _form.rammebetingelser.generelleVilkaar.beroererArbeidsplasserSpecified = true;

            _form.varsling.foreliggerMerknader = false;


            _formSetElements = new List<string>()
            {
                "UttalelseKulturminnemyndighet",
                "SamtykkeArbeidstilsynet",
            };

            _validator.OppfyllesVilkaarFor3UkersfristValidering(_form, _formSetElements);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.15.3").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4528.2.15.4 OppfyllesVilkaarFor3UkersfristValidering - Error")]
        public void form_4528_2_15_4_OppfyllesVilkaarFor3UkersfristValidering_Error_Test()
        {
            _form.rammebetingelser.generelleVilkaar.oppfyllesVilkaarFor3Ukersfrist = true;
            _form.rammebetingelser.generelleVilkaar.oppfyllesVilkaarFor3UkersfristSpecified = true;

            _form.rammebetingelser.generelleVilkaar.beroererTidligere1850 = true;
            _form.rammebetingelser.generelleVilkaar.beroererTidligere1850Specified = true;

            _form.rammebetingelser.generelleVilkaar.beroererArbeidsplasser = true;
            _form.rammebetingelser.generelleVilkaar.beroererArbeidsplasserSpecified = true;

            _form.varsling.foreliggerMerknader = true;

            _form.dispensasjon = null;

            _formSetElements = new List<string>()
            {
                "UttalelseKulturminnemyndighet",
                "SamtykkeArbeidstilsynet",
            };

            _validator.OppfyllesVilkaarFor3UkersfristValidering(_form, _formSetElements);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4528.2.15.4").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

    }
}
