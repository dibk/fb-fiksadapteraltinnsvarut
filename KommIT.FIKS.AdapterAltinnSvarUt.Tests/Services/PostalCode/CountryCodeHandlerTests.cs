using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;
using Xunit;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Tests.Services.PostalCode
{
    public class CountryCodeHandlerTests
    {
        [Fact]
        public void VerifyCountryCodeTest()
        {
            
            bool isWrongcch = CountryCodeHandler.VerifyCountryCode("TB");
            bool isRightcch = CountryCodeHandler.VerifyCountryCode("DK");

            bool isWrongSsb = CountryCodeHandler.VerifyCountryCode("888");
            bool isRightSsb = CountryCodeHandler.VerifyCountryCode("123");

            isWrongcch.Should().BeFalse();
            isRightcch.Should().BeTrue();
            isWrongSsb.Should().BeFalse();
            isRightSsb.Should().BeTrue();

        }

        [Fact]
        public void ReturnCountryNameTest()
        {

            string nameSverige = CountryCodeHandler.ReturnCountryName("SE");
            string name141 = CountryCodeHandler.ReturnCountryName("141");
            string nameDK = CountryCodeHandler.ReturnCountryName("DK");
            string name357 = CountryCodeHandler.ReturnCountryName("357");
            string nameNotExists = CountryCodeHandler.ReturnCountryName("995");
            string nameNoCountryCode = CountryCodeHandler.ReturnCountryName("");

            nameSverige.Should().Be("SVERIGE");
            name141.Should().Be("SVEITS");
            nameDK.Should().Be("DANMARK");
            name357.Should().Be("SWAZILAND");
            nameNotExists.Should().BeEmpty();
            nameNoCountryCode.Should().BeEmpty();
        }
        [Fact]
        public void ReturnCountryIsoCodeTest()
        {
            string isoCodeNO = CountryCodeHandler.ReturnCountryIsoCode("0");
            string isoCodeDK = CountryCodeHandler.ReturnCountryIsoCode("101");
            string isoCodeNotExists = CountryCodeHandler.ReturnCountryIsoCode("995");
            string isoCodeNoCountryCode = CountryCodeHandler.ReturnCountryIsoCode("");
            string isoCodeNoCountryCode1 = CountryCodeHandler.ReturnCountryIsoCode(" ");

            isoCodeNO.Should().Be("NO");
            isoCodeDK.Should().Be("DK");
            isoCodeNotExists.Should().BeEmpty();
            isoCodeNoCountryCode.Should().BeEmpty();

        }
        [Fact]
        public void ReturnAdjustedPostalcodeTest()
        {
            CountryCodeHandler cch = new CountryCodeHandler();
            string postalCodeDK4000 = CountryCodeHandler.ReturnAdjustedPostalcode("4000", CountryCodeHandler.ReturnCountryIsoCode("101"));
            string postalCodeDE64296 = CountryCodeHandler.ReturnAdjustedPostalcode("64296", CountryCodeHandler.ReturnCountryIsoCode("144"));
            string postalCodeJPxPrefix = CountryCodeHandler.ReturnAdjustedPostalcode("100-36", CountryCodeHandler.ReturnCountryIsoCode("464"));
            string postalCodeAdjustmentException = CountryCodeHandler.ReturnAdjustedPostalcode("", CountryCodeHandler.ReturnCountryIsoCode(""));

            postalCodeDK4000.Should().Be("DK-4000");
            postalCodeDE64296.Should().Be("DE-64296");
            postalCodeJPxPrefix.Should().Be("100-36");
            postalCodeAdjustmentException.Should().BeEmpty();

        }
    }

}
