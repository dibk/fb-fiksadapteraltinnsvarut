﻿using FluentAssertions;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Validations;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;
using Xunit;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Tests.Services.PostalCode
{
    public class PostalCodeTests
    {
        private readonly bool _externalDependences = Feature.IsFeatureEnabled(FeatureFlags.UnitTestExternalDependence);
        private readonly bool _bringPostnummer = Feature.IsFeatureEnabled(FeatureFlags.BringPostnummerValidering);

        [Fact(DisplayName = "Postalcode - OK test")]
        public void PostalCodeOkTest()
        {

            var postalcodeResponse = new AdapterAltinnSvarUt.Services.PostalCode.BringPostalCodeProvider().ValidatePostnr("3800", "no");

            if (_externalDependences && _bringPostnummer)
                postalcodeResponse.Valid.Should().BeTrue();
        }
        [Fact(DisplayName = "Postalcode - Error test")]
        public void PostalCodeErrorTest()
        {

            var postalcodeResponse = new AdapterAltinnSvarUt.Services.PostalCode.BringPostalCodeProvider().ValidatePostnr("38002", "no");

            if (_externalDependences && _bringPostnummer)
                postalcodeResponse.Valid.Should().BeFalse();
        }

        [Fact]
        public void NorwegianAddress_SSB_countryCode_validPostalcode_namedCity_returns_true()
        {
            Models.Address address = new Models.Address()
            {
                CountryCode = "0", //SSB value for Norway
                PostalCode = "3800",
                City = "Bø"
            };

            var isValid = GeneralValidations.IsAddressValidForPrint(address);

            if (_externalDependences && _bringPostnummer)
                isValid.Should().BeTrue();
        }

        [Fact]
        public void NorwegianAddress_ISO_countryCode_validPostalcode_namedCity_returns_true()
        {
            Models.Address address = new Models.Address()
            {
                CountryCode = "NO", //ISO value for Norway
                PostalCode = "3800",
                City = "Bø"
            };

            var isValid = GeneralValidations.IsAddressValidForPrint(address);

            if (_externalDependences && _bringPostnummer)
                isValid.Should().BeTrue();
        }

        [Fact]
        public void NorwegianAddress_SSB_countryCode_invalidPostalcode_namedCity_returns_false()
        {
            Models.Address address = new Models.Address()
            {
                CountryCode = "0", //SSB value for Norway
                PostalCode = "12",
                City = "Bø"
            };

            var isValid = GeneralValidations.IsAddressValidForPrint(address);

            if (_externalDependences && _bringPostnummer)
                isValid.Should().BeFalse();
        }

        [Fact]
        public void NorwegianAddress_ISO_countryCode_validPostalcode_noCity_returns_false()
        {
            Models.Address address = new Models.Address()
            {
                CountryCode = "NO", //ISO value for Norway
                PostalCode = "3800",
            };

            var isValid = GeneralValidations.IsAddressValidForPrint(address);

            if (_externalDependences && _bringPostnummer)
                isValid.Should().BeFalse();
        }

        [Fact]
        public void ForgeignAddress_SSB_countryCode_namedCity_returns_true()
        {
            Models.Address address = new Models.Address()
            {
                CountryCode = "106", //SSB value for Sweden
                City = "Lönneberget"
            };

            var isValid = GeneralValidations.IsAddressValidForPrint(address);

            isValid.Should().BeTrue();
        }
        [Fact]
        public void ForgeignAddress_ISO_countryCode_namedCity_returns_true()
        {
            Models.Address address = new Models.Address()
            {
                CountryCode = "SE", //ISO value for Sweden
                City = "Lönneberget"
            };

            var isValid = GeneralValidations.IsAddressValidForPrint(address);

            isValid.Should().BeTrue();
        }

        [Fact]
        public void ForgeignAddress_ISO_countryCode_noCity_returns_false()
        {
            Models.Address address = new Models.Address()
            {
                CountryCode = "106", //SSB value for Sweden
            };

            var isValid = GeneralValidations.IsAddressValidForPrint(address);

            isValid.Should().BeFalse();
        }
    }
}
