using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using FluentAssertions;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.SvarUtForm;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.Builders;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Validations;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;
using Moq;
using Xunit;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.FormsSamples;
using no.kxml.skjema.dibk.arbeidstilsynetsSamtykke;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Tests.Services
{
    public class ArbeidstilsynetsSamtykkeFormV1ValidationTests
    {

        [Fact]
        public void ValidateTest()
        {
            var validateRuels = new ArbeidstilsynetsSamtykkeFormV1Validation();
            validateRuels.Should().NotBeNull();
        }
        [Fact(DisplayName = "4845.1.3 FraSluttbrukersystemErUtfylt - OK test")]
        public void Arbeidstilsynets_4845_1_3_FraSluttbrukersystemErUtfyltOkTest()
        {
            var val = new ArbeidstilsynetsSamtykkeFormV1Validation();

            var arbeidstilsynetsType = new ArbeidstilsynetsSamtykkeBuilder()
                .FraSluttbrukersystemErUtfylt("Fellestjenester bygg testmotor")
                .Build();
            val.FraSluttbrukersystemErUtfylt(arbeidstilsynetsType);
            var result = val.GetResult();

            var rulescheked = TestHelper.ConverRulesCheckedJson(result);
 
            TestHelper.AssertResult(result, "", 0, null, true);
        }
        [Fact(DisplayName = "4845.1.3 FraSluttbrukersystemErUtfylt - Error test")]
        public void Arbeidstilsynets_4845_1_3_FraSluttbrukersystemErUtfyltErrorTest()
        {
            var val = new ArbeidstilsynetsSamtykkeFormV1Validation();

            var arbeidstilsynetsType = new ArbeidstilsynetsSamtykkeBuilder()
                .FraSluttbrukersystemErUtfylt("Fellestjenester bygg testmotor")
                .Build();
            val.FraSluttbrukersystemErUtfylt(arbeidstilsynetsType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "4845.1.3", 1, null, true);
        }
        //Eiendommens
        [Fact(DisplayName = "4845.1.4 EiendomByggestedValidering - OK test")]
        public void Arbeidstilsynets_4845_1_4_EiendomByggestedValideringOKTest()
        {

            var val = new ArbeidstilsynetsSamtykkeFormV1Validation();
            var municipalityValidator = new Mock<IMunicipalityValidator>();
            municipalityValidator.Setup(s => s.ValidateKommunenummer(It.IsAny<string>())).Returns(new GeneralValidationResult() { Status = ValidationStatus.Ok, Message = "UnitTest" });

            val.MunicipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");


            var arbeidstilsynetsType = new ArbeidstilsynetsSamtykkeBuilder()
                .EiendomByggested("9999", "1", "3", "0", "0", "Hammerfest", "H0101", "192423880", "Båtsfjorden 6", "Båtsfjorden", "6", "9664", "SANDØYBOTN")
                .Build();
            val.EiendomByggestedValidering(arbeidstilsynetsType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "", 0, null, true);
        }
        [Fact(DisplayName = "4845.1.4 EiendomByggestedValidering - Error test")]
        public void Arbeidstilsynets_4845_1_4_EiendomByggestedValideringErrorTest()
        {

            var val = new ArbeidstilsynetsSamtykkeFormV1Validation();

            var arbeidstilsynetsType = new ArbeidstilsynetsSamtykkeBuilder()
                .EiendomByggested()
                .Build();
            val.EiendomByggestedValidering(arbeidstilsynetsType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "4845.1.4", 1, null, true);
        }
        [Fact(DisplayName = "4845.1.4.1 EiendomByggestedValidering - ERROR test")]
        public void Arbeidstilsynets_4845_1_4_1_EiendomByggestedValideringErrorTest()
        {

            var val = new ArbeidstilsynetsSamtykkeFormV1Validation();

            var arbeidstilsynetsType = new ArbeidstilsynetsSamtykkeBuilder()
                .EiendomByggested(null, "1", "3", "0", "0", "Hammerfest", "H0101", "192423880", "Båtsfjorden 6", "Båtsfjorden", "6", "9664", "SANDØYBOTN")
                .Build();
            val.EiendomByggestedValidering(arbeidstilsynetsType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "4845.1.4.1", 1, null, true);
        }
        [Fact(DisplayName = "4845.1.4.2 EiendomByggestedValidering - ERROR test")]
        public void Arbeidstilsynets_4845_1_4_2_EiendomByggestedValideringErrorTest()
        {

            var val = new ArbeidstilsynetsSamtykkeFormV1Validation();

            val.MunicipalityValidator = TestHelper.MokMunicipalityValidatorResult("invalid");

            var arbeidstilsynetsType = new ArbeidstilsynetsSamtykkeBuilder()
                .EiendomByggested("*9999", "1", "3", "0", "0", "Hammerfest", "H0101", "192423880", "Båtsfjorden 6", "Båtsfjorden", "6", "9664", "SANDØYBOTN")
                .Build();
            val.EiendomByggestedValidering(arbeidstilsynetsType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "4845.1.4.2", 1, null, true);
        }


        [Fact(DisplayName = "4845.1.4.3 EiendomByggestedValidering - ERROR test")]
        public void Arbeidstilsynets_4845_1_4_3_EiendomByggestedValideringErrorTest()
        {

            var val = new ArbeidstilsynetsSamtykkeFormV1Validation();
            val.MunicipalityValidator = TestHelper.MokMunicipalityValidatorResult("expired");


            var arbeidstilsynetsType = new ArbeidstilsynetsSamtykkeBuilder()
                .EiendomByggested("9999", "1", "3", "0", "0", "Hammerfest", "H0101", "192423880", "Båtsfjorden 6", "Båtsfjorden", "6", "9664", "SANDØYBOTN")
                .Build();
            val.EiendomByggestedValidering(arbeidstilsynetsType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "4845.1.4.3", 1, null, true);
        }
        [Fact(DisplayName = "4845.1.4.4 EiendomByggestedValidering - ERROR test")]
        public void Arbeidstilsynets_4845_1_4_4_EiendomByggestedValideringErrorTest()
        {

            var val = new ArbeidstilsynetsSamtykkeFormV1Validation();
            val.MunicipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");


            var arbeidstilsynetsType = new ArbeidstilsynetsSamtykkeBuilder()
                .EiendomByggested("9999", null, "3", "0", "0", "Hammerfest", "H0101", "192423880", "Båtsfjorden 6", "Båtsfjorden", "6", "9664", "SANDØYBOTN")
                .Build();
            val.EiendomByggestedValidering(arbeidstilsynetsType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "4845.1.4.4", 1, null, true);
        }
        [Fact(DisplayName = "4845.1.4.5 EiendomByggestedValidering - ERROR test")]
        public void Arbeidstilsynets_4845_1_4_5_EiendomByggestedValideringErrorTest()
        {

            var val = new ArbeidstilsynetsSamtykkeFormV1Validation();
            val.MunicipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");


            var arbeidstilsynetsType = new ArbeidstilsynetsSamtykkeBuilder()
                .EiendomByggested("9999", "1", "*3", "0", "0", "Hammerfest", "H0101", "192423880", "Båtsfjorden 6", "Båtsfjorden", "6", "9664", "SANDØYBOTN")
                .Build();
            val.EiendomByggestedValidering(arbeidstilsynetsType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "4845.1.4.5", 1, null, true);
        }
        [Fact(DisplayName = "4845.1.4.6 EiendomByggestedValidering - ERROR test")]
        public void Arbeidstilsynets_4845_1_4_6_EiendomByggestedValideringErrorTest()
        {

            var val = new ArbeidstilsynetsSamtykkeFormV1Validation();
            val.MunicipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");


            var arbeidstilsynetsType = new ArbeidstilsynetsSamtykkeBuilder()
                .EiendomByggested("9999", "-1", "0", "0", "0", "Hammerfest", "H0101", "192423880", "Båtsfjorden 6", "Båtsfjorden", "6", "9664", "SANDØYBOTN")
                .Build();
            val.EiendomByggestedValidering(arbeidstilsynetsType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "4845.1.4.6", 1, null, true);
        }
        [Fact(DisplayName = "4845.1.4.6.1 EiendomByggestedValidering - ERROR test")]
        public void Arbeidstilsynets_4845_1_4_6_1_EiendomByggestedValideringErrorTest()
        {

            var val = new ArbeidstilsynetsSamtykkeFormV1Validation();
            val.MunicipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");


            var arbeidstilsynetsType = new ArbeidstilsynetsSamtykkeBuilder()
                .EiendomByggested("9999", "0", "-1", "0", "0", "Hammerfest", "H0101", "192423880", "Båtsfjorden 6", "Båtsfjorden", "6", "9664", "SANDØYBOTN")
                .Build();
            val.EiendomByggestedValidering(arbeidstilsynetsType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "4845.1.4.6.1", 1, null, true);
        }

        //Matrikkel

        [Fact(DisplayName = "4845.1.4.7 EiendomByggestedValidering - ERROR test")]
        public void Arbeidstilsynets_4845_1_4_7_EiendomByggestedValideringErrorTest()
        {

            var val = new ArbeidstilsynetsSamtykkeFormV1Validation();
            val.MunicipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");


            var arbeidstilsynetsType = new ArbeidstilsynetsSamtykkeBuilder()
                .EiendomByggested("9999", "1", "3", "0", "0", "Hammerfest", "H0101", "192423880r", "Båtsfjorden 6", "Båtsfjorden", "6", "9664", "SANDØYBOTN")
                .Build();
            val.EiendomByggestedValidering(arbeidstilsynetsType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "4845.1.7.4", 1, null, true);
        }

        [Fact(DisplayName = "4845.1.4.8 EiendomByggestedValidering - ERROR test")]
        public void Arbeidstilsynets_4845_1_4_8_EiendomByggestedValideringErrorTest()
        {

            var val = new ArbeidstilsynetsSamtykkeFormV1Validation();
            val.MunicipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");


            var arbeidstilsynetsType = new ArbeidstilsynetsSamtykkeBuilder()
                .EiendomByggested("9999", "1", "3", "0", "0", "Hammerfest", "H0101", "0", "Båtsfjorden 6", "Båtsfjorden", "6", "9664", "SANDØYBOTN")
                .Build();
            val.EiendomByggestedValidering(arbeidstilsynetsType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "4845.1.4.8", 1, null, true);
        }

        [Fact(DisplayName = "4845.1.4.9 EiendomByggestedValidering - ERROR test")]
        public void Arbeidstilsynets_4845_1_4_9_EiendomByggestedValideringErrorTest()
        {

            var val = new ArbeidstilsynetsSamtykkeFormV1Validation();
            val.MunicipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");


            var arbeidstilsynetsType = new ArbeidstilsynetsSamtykkeBuilder()
                .EiendomByggested("9999", "1", "3", "0", "0", "Hammerfest", "H0101", "192423880")
                .Build();
            val.EiendomByggestedValidering(arbeidstilsynetsType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "4845.1.4.9", 1, null, true);
        }

        [Fact(DisplayName = "4845.1.4.9.1 EiendomByggestedValidering - ERROR test")]
        public void Arbeidstilsynets_4845_1_4_9_1_EiendomByggestedValideringErrorTest()
        {

            var val = new ArbeidstilsynetsSamtykkeFormV1Validation();
            val.MunicipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");


            var arbeidstilsynetsType = new ArbeidstilsynetsSamtykkeBuilder()
                .EiendomByggested("9999", "1", "3", "0", "0", "Hammerfest", "H0101", "192423880", null, "Båtsfjorden", "6", "9664", "SANDØYBOTN")
                .Build();
            val.EiendomByggestedValidering(arbeidstilsynetsType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "4845.1.4.9.1", 1, null, true);
        }
        [Fact(DisplayName = "4845.1.4.9.2 EiendomByggestedValidering - ERROR test")]
        public void Arbeidstilsynets_4845_1_4_9_2_EiendomByggestedValideringErrorTest()
        {

            var val = new ArbeidstilsynetsSamtykkeFormV1Validation();
            val.MunicipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");


            var arbeidstilsynetsType = new ArbeidstilsynetsSamtykkeBuilder()
                .EiendomByggested("9999", "1", "3", "0", "0", "Hammerfest", "H0101", "192423880", "Båtsfjorden 6", null, "6", "9664", "SANDØYBOTN")
                .Build();
            val.EiendomByggestedValidering(arbeidstilsynetsType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "4845.1.4.9.2", 1, null, true);
        }

        [Fact(DisplayName = "4845.1.4.10 EiendomByggestedValidering - ERROR test")]
        public void Arbeidstilsynets_4845_1_4_10_EiendomByggestedValideringErrorTest()
        {

            var val = new ArbeidstilsynetsSamtykkeFormV1Validation();
            val.MunicipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");


            var arbeidstilsynetsType = new ArbeidstilsynetsSamtykkeBuilder()
                .EiendomByggested("9999", "1", "3", "0", "0", null, "H0101", "192423880", "Båtsfjorden 6", "Båtsfjorden", "6", "9664", "SANDØYBOTN")
                .Build();
            val.EiendomByggestedValidering(arbeidstilsynetsType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "4845.1.4.10", 1, null, true);
        }
        [Fact(DisplayName = "4845.1.4.11 EiendomByggestedValidering - ERROR test")]
        public void Arbeidstilsynets_4845_1_4_11_EiendomByggestedValideringErrorTest()
        {

            var val = new ArbeidstilsynetsSamtykkeFormV1Validation();
            val.MunicipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");


            var arbeidstilsynetsType = new ArbeidstilsynetsSamtykkeBuilder()
                .EiendomByggested("9999", "1", "3", "0", "0", "Hammerfest", "*H0101", "192423880", "Båtsfjorden 6", "Båtsfjorden", "6", "9664", "SANDØYBOTN")
                .Build();
            val.EiendomByggestedValidering(arbeidstilsynetsType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "4845.1.4.11", 1, null, true);
        }

        [Fact(DisplayName = "4845.1.5 ArbeidsplasserValidering - OK test")]
        public void Arbeidstilsynets_4845_1_5_ArbeidsplasserValideringOkTest()
        {

            var val = new ArbeidstilsynetsSamtykkeFormV1Validation();

            var arbeidstilsynetsType = new ArbeidstilsynetsSamtykkeBuilder()
                .Arbeidsplasser(true, null, null, true, true, "2", "16", "En finn Firma")
                .Build();
            val.ArbeidsplasserValidering(arbeidstilsynetsType, new List<string>() { "asasa", "Noko2" });
            var result = val.GetResult();

            TestHelper.AssertResult(result, null, 0, null, true);
        }
        [Fact(DisplayName = "4845.1.5 ArbeidsplasserValidering - Error test")]
        public void Arbeidstilsynets_4845_1_5_ArbeidsplasserValideringErrorTest()
        {

            var val = new ArbeidstilsynetsSamtykkeFormV1Validation();

            var arbeidstilsynetsType = new ArbeidstilsynetsSamtykkeBuilder()
                .Build();
            val.ArbeidsplasserValidering(arbeidstilsynetsType, new List<string>());
            var result = val.GetResult();

            TestHelper.AssertResult(result, "4845.1.5", 1, null, true);
        }
        [Fact(DisplayName = "4845.1.5.1 ArbeidsplasserValidering - Error test")]
        public void Arbeidstilsynets_4845_1_5_1_ArbeidsplasserValideringErrorTest()
        {

            var val = new ArbeidstilsynetsSamtykkeFormV1Validation();

            var arbeidstilsynetsType = new ArbeidstilsynetsSamtykkeBuilder()
                .Arbeidsplasser(false, true, null, true, true, "2", "16", "En finn Firma")
                .Build();
            val.ArbeidsplasserValidering(arbeidstilsynetsType, new List<string>());
            var result = val.GetResult();

            TestHelper.AssertResult(result, "4845.1.5.1", 1, null, true);
        }
        [Fact(DisplayName = "4845.1.5.2 ArbeidsplasserValidering - Error test")]
        public void Arbeidstilsynets_4845_1_5_2_ArbeidsplasserValideringErrorTest()
        {

            var val = new ArbeidstilsynetsSamtykkeFormV1Validation();

            var arbeidstilsynetsType = new ArbeidstilsynetsSamtykkeBuilder()
                .Arbeidsplasser(false, false, null, true, true, "2", "16", "En finn Firma")
                .Build();
            val.ArbeidsplasserValidering(arbeidstilsynetsType, new List<string>());
            var result = val.GetResult();

            TestHelper.AssertResult(result, "4845.1.5.2", 1, null, true);
        }



        [Fact(DisplayName = "4845.1.6 Tiltakshaver - OK test")]
        public void arbeidstilsynetsSamtykke_4845_1_6_TiltakshaverValidationOkTest()
        {
            var val = new ArbeidstilsynetsSamtykkeFormV1Validation();

            var arbeidstilsynetsType = new ArbeidstilsynetsSamtykkeBuilder()
                .Tiltakshaver("974760673", null, "Foretak", "Foretak", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "no", "Trondheim", "11223344", "99887766", "post@test.no", "Benjamin Fjell")
                .Build();
            val.TiltakshaverValidation(arbeidstilsynetsType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "", 0, null, true);

        }

        [Fact(DisplayName = "4845.1.6 Tiltakshaver - Error test")]
        public void arbeidstilsynetsSamtykke_4845_1_6_TiltakshaverValidationErrorTest()
        {
            var val = new ArbeidstilsynetsSamtykkeFormV1Validation();

            var arbeidstilsynetsType = new ArbeidstilsynetsSamtykkeBuilder()
               .Build();
            val.TiltakshaverValidation(arbeidstilsynetsType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "4845.1.6", 1, true);
        }

        [Fact(DisplayName = "4845.1.6.1 Tiltakshaver - Error test")]
        public void arbeidstilsynetsSamtykke_4845_1_6_1_TiltakshaverValidationErrorTest()
        {
            var val = new ArbeidstilsynetsSamtykkeFormV1Validation();

            var arbeidstilsynetsType = new ArbeidstilsynetsSamtykkeBuilder()
                .Tiltakshaver("974760673", null, null, "Foretak", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "no", "Trondheim", "11223344", "99887766", "post@test.no", "Benjamin Fjell")
                .Build();
            val.TiltakshaverValidation(arbeidstilsynetsType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "4845.1.6.1", 1, true);
        }
        [Fact(DisplayName = "4845.1.6.2 Tiltakshaver - Error test")]
        public void arbeidstilsynetsSamtykke_4845_1_6_2_TiltakshaverValidationErrorTest()
        {
            var val = new ArbeidstilsynetsSamtykkeFormV1Validation();

            var arbeidstilsynetsType = new ArbeidstilsynetsSamtykkeBuilder()
                .Tiltakshaver("974760673", null, "*Foretak", "Foretak", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "no", "Trondheim", "11223344", "99887766", "post@test.no", "Benjamin Fjell")
                .Build();
            val.TiltakshaverValidation(arbeidstilsynetsType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "4845.1.6.2", 1, true);
        }
        [Fact(DisplayName = "4845.1.6.3 Tiltakshaver - Error test")]
        public void arbeidstilsynetsSamtykke_4845_1_6_3_TiltakshaverValidationErrorTest()
        {
            var val = new ArbeidstilsynetsSamtykkeFormV1Validation();

            var arbeidstilsynetsType = new ArbeidstilsynetsSamtykkeBuilder()
                .Tiltakshaver("974760673", null, "Privatperson", "Foretak", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "no", "Trondheim", "11223344", "99887766", "post@test.no", "Benjamin Fjell")
                .Build();
            val.TiltakshaverValidation(arbeidstilsynetsType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "4845.1.6.3", 1, true);
        }
        [Fact(DisplayName = "4845.1.6.3.1 Tiltakshaver - Error test")]
        public void arbeidstilsynetsSamtykke_4845_1_6_3_1_TiltakshaverValidationErrorTest()
        {
            var val = new ArbeidstilsynetsSamtykkeFormV1Validation();

            var arbeidstilsynetsType = new ArbeidstilsynetsSamtykkeBuilder()
                // "ncX7R6YaiCKMNxmEku/UkLCpivaC140mq4hyGXP3q+1SqDHmh1z9x1+rO0KVcq1o5Si+Q1ISqYhokevrlYXTcJMHQhdGk+zWNlyBN1UM+zPyhVKpFdlMr6ooEj1Ar297/sjvJunTXo4/6qemwDk3h815RKhsDduAMPQEAJgj6n0=" 
                .Tiltakshaver(null, "ncaiCKMNxmEku/UkLCpivaC140mq4hyGXP3q+1SqDHmh1z9x1+rO0KVcq1o5Si+Q1ISqYhokevrlYXTcJMHQhdGk+zWNlyBN1UM+zPyhVKpFdlMr6ooEj1Ar297/sjvJunTXo4/6qemwDk3h815RKhsDduAMPQEAJgj6n0=",
                    "Privatperson", "Privatperson", null, "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "no", "Trondheim", "11223344", "99887766", "post@test.no")
                .Build();
            val.TiltakshaverValidation(arbeidstilsynetsType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "4845.1.6.3.1", 1, true);
        }
        [Fact(DisplayName = "4845.1.6.3.2 Tiltakshaver - Error test")]
        public void arbeidstilsynetsSamtykke_4845_1_6_3_2_TiltakshaverValidationErrorTest()
        {
            var val = new ArbeidstilsynetsSamtykkeFormV1Validation();

            var arbeidstilsynetsType = new ArbeidstilsynetsSamtykkeBuilder()
                .Tiltakshaver(null, "25525401530", "Privatperson", "Privatperson", null, "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "no", "Trondheim", "11223344", "99887766", "post@test.no")
                .Build();
            val.TiltakshaverValidation(arbeidstilsynetsType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "4845.1.6.3.2", 1, true);
        }
        [Fact(DisplayName = "4845.1.6.3.3 Tiltakshaver - Error test")]
        public void arbeidstilsynetsSamtykke_4845_1_6_3_3_TiltakshaverValidationErrorTest()
        {
            var val = new ArbeidstilsynetsSamtykkeFormV1Validation();

            var arbeidstilsynetsType = new ArbeidstilsynetsSamtykkeBuilder()
                .Tiltakshaver(null, "*106500248", "Privatperson", "Privatperson", null, "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "no", "Trondheim", "11223344", "99887766", "post@test.no")
                .Build();
            val.TiltakshaverValidation(arbeidstilsynetsType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "4845.1.6.3.3", 1, true);
        }
        [Fact(DisplayName = "4845.1.6.4 Tiltakshaver - Error test")]
        public void arbeidstilsynetsSamtykke_4845_1_6_4_TiltakshaverValidationErrorTest()
        {
            var val = new ArbeidstilsynetsSamtykkeFormV1Validation();

            var arbeidstilsynetsType = new ArbeidstilsynetsSamtykkeBuilder()
                .Tiltakshaver(null, null, "Foretak", "Foretak", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "no", "Trondheim", "11223344", "99887766", "post@test.no", "Benjamin Fjell")
                .Build();
            val.TiltakshaverValidation(arbeidstilsynetsType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "4845.1.6.4", 1, true);
        }
        [Fact(DisplayName = "4845.1.6.5 Tiltakshaver - Error test")]
        public void arbeidstilsynetsSamtykke_4845_1_6_5_TiltakshaverValidationErrorTest()
        {
            var val = new ArbeidstilsynetsSamtykkeFormV1Validation();

            var arbeidstilsynetsType = new ArbeidstilsynetsSamtykkeBuilder()
                .Tiltakshaver("*74760673", null, "Foretak", "Foretak", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "no", "Trondheim", "11223344", "99887766", "post@test.no", "Benjamin Fjell")
                .Build();
            val.TiltakshaverValidation(arbeidstilsynetsType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "4845.1.6.5", 1, true);
        }
        [Fact(DisplayName = "4845.1.6.6 Tiltakshaver - Error test")]
        public void arbeidstilsynetsSamtykke_4845_1_6_6_TiltakshaverValidationErrorTest()
        {
            var val = new ArbeidstilsynetsSamtykkeFormV1Validation();

            var arbeidstilsynetsType = new ArbeidstilsynetsSamtykkeBuilder()
                .Tiltakshaver("254760673", null, "Foretak", "Foretak", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "no", "Trondheim", "11223344", "99887766", "post@test.no", "Benjamin Fjell")
                .Build();
            val.TiltakshaverValidation(arbeidstilsynetsType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "4845.1.6.6", 1, true);
        }
        [Fact(DisplayName = "4845.1.6.7 Tiltakshaver - Error test")]
        public void arbeidstilsynetsSamtykke_4845_1_6_7_TiltakshaverValidationErrorTest()
        {
            var val = new ArbeidstilsynetsSamtykkeFormV1Validation();

            var arbeidstilsynetsType = new ArbeidstilsynetsSamtykkeBuilder()
                .Tiltakshaver("974760673", null, "Foretak", "Foretak", "Siv. Ark. Fjell og Sønner", null, null, null, null, "11223344", "99887766", "post@test.no", "Benjamin Fjell")
                .Build();
            val.TiltakshaverValidation(arbeidstilsynetsType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "4845.1.6.7", 1, true);
        }

        [Fact(DisplayName = "4845.1.6.8 Tiltakshaver - Error test")]
        public void arbeidstilsynetsSamtykke_4845_1_6_8_TiltakshaverValidationErrorTest()
        {
            var val = new ArbeidstilsynetsSamtykkeFormV1Validation();

            var arbeidstilsynetsType = new ArbeidstilsynetsSamtykkeBuilder()
                .Tiltakshaver("974760673", null, "Foretak", "Foretak", "Siv. Ark. Fjell og Sønner", null, "7003", "no", "Trondheim", "11223344", "99887766", "post@test.no", "Benjamin Fjell")
                .Build();
            val.TiltakshaverValidation(arbeidstilsynetsType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "4845.1.6.8", 1, true);
        }

        [Fact(DisplayName = "4845.1.6.9 Tiltakshaver - Error test")]
        public void arbeidstilsynetsSamtykke_4845_1_6_9_TiltakshaverValidationErrorTest()
        {
            var val = new ArbeidstilsynetsSamtykkeFormV1Validation();

            var arbeidstilsynetsType = new ArbeidstilsynetsSamtykkeBuilder()
                .Tiltakshaver("974760673", null, "Foretak", "Foretak", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "*AR", "Trondheim", "11223344", "99887766", "post@test.no", "Benjamin Fjell")
                .Build();
            val.TiltakshaverValidation(arbeidstilsynetsType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "4845.1.6.9", 1, true);
        }
        [Fact(DisplayName = "4845.1.6.10 Tiltakshaver - Error test")]
        public void arbeidstilsynetsSamtykke_4845_1_6_10_TiltakshaverValidationErrorTest()
        {
            var val = new ArbeidstilsynetsSamtykkeFormV1Validation();

            var arbeidstilsynetsType = new ArbeidstilsynetsSamtykkeBuilder()
                .Tiltakshaver("974760673", null, "Foretak", "Foretak", "Siv. Ark. Fjell og Sønner", "Lillegata 5", null, "no", "Trondheim", "11223344", "99887766", "post@test.no", "Benjamin Fjell")
                .Build();
            val.TiltakshaverValidation(arbeidstilsynetsType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "4845.1.6.10", 1, true);
        }

        [Fact(DisplayName = "4845.1.6.11 Tiltakshaver - Error test")]
        public void arbeidstilsynetsSamtykke_4845_1_6_11_TiltakshaverValidationErrorTest()
        {
            var val = new ArbeidstilsynetsSamtykkeFormV1Validation();

            var arbeidstilsynetsType = new ArbeidstilsynetsSamtykkeBuilder()
                .Tiltakshaver("974760673", null, "Foretak", "Foretak", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "000*", "no", "Trondheim", "11223344", "99887766", "post@test.no", "Benjamin Fjell")
                .Build();
            val.TiltakshaverValidation(arbeidstilsynetsType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "4845.1.6.11", 1, true);
        }
        //BRING
        [Fact(DisplayName = "4845.1.6.15 Tiltakshaver - Error test")]
        public void arbeidstilsynetsSamtykke_4845_1_6_15_TiltakshaverValidationErrorTest()
        {
            var val = new ArbeidstilsynetsSamtykkeFormV1Validation();

            var arbeidstilsynetsType = new ArbeidstilsynetsSamtykkeBuilder()
                .Tiltakshaver("974760673", null, "Foretak", "Foretak", null, "Lillegata 5", "7003", "no", "Trondheim", "11223344", "99887766", "post@test.no", "Benjamin Fjell")
                .Build();
            val.TiltakshaverValidation(arbeidstilsynetsType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "4845.1.6.15", 1, true);
        }
        [Fact(DisplayName = "4845.1.6.16 Tiltakshaver - Error test")]
        public void arbeidstilsynetsSamtykke_4845_1_6_16_TiltakshaverValidationErrorTest()
        {
            var val = new ArbeidstilsynetsSamtykkeFormV1Validation();

            var arbeidstilsynetsType = new ArbeidstilsynetsSamtykkeBuilder()
                .Tiltakshaver("974760673", null, "Foretak", "Foretak", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "no", "Trondheim", "11223344", "99887766", "post@test.no", null)
                .Build();
            val.TiltakshaverValidation(arbeidstilsynetsType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "4845.1.6.16", 1, true);
        }
        [Fact(DisplayName = "4845.1.6.17 Tiltakshaver - Error test")]
        public void arbeidstilsynetsSamtykke_4845_1_6_17_TiltakshaverValidationErrorTest()
        {
            var val = new ArbeidstilsynetsSamtykkeFormV1Validation();

            var arbeidstilsynetsType = new ArbeidstilsynetsSamtykkeBuilder()
                .Tiltakshaver("974760673", null, "Foretak", "Foretak", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "no", "Trondheim", null, null, "post@test.no", "Benjamin Fjell")
                .Build();
            val.TiltakshaverValidation(arbeidstilsynetsType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "4845.1.6.17", 1, true);
        }
        [Fact(DisplayName = "4845.1.6.18 Tiltakshaver - Error test")]
        public void arbeidstilsynetsSamtykke_4845_1_6_18_TiltakshaverValidationErrorTest()
        {
            var val = new ArbeidstilsynetsSamtykkeFormV1Validation();

            var arbeidstilsynetsType = new ArbeidstilsynetsSamtykkeBuilder()
                .Tiltakshaver("974760673", null, "Foretak", "Foretak", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "no", "Trondheim", "11223344", "99887766", null, "Benjamin Fjell")
                .Build();
            val.TiltakshaverValidation(arbeidstilsynetsType);
            var result = val.GetResult();
            


            TestHelper.AssertResult(result, "4845.1.6.18", 1, true);
        }
        [Fact(DisplayName = "4845.1.7 Fakturamottaker - OK test")]
        public void arbeidstilsynetsSamtykke_4845_1_7FakturamottakerValidationOKTest()
        {
            var val = new ArbeidstilsynetsSamtykkeFormV1Validation();

            var arbeidstilsynetsType = new ArbeidstilsynetsSamtykkeBuilder()
                .Fakturamottaker(true, false, "123456789", "BARB_126", "FaRef_1258", "NoenSaamTrengeFakture AS", "202012345", "StorGate 126", "3800", "Bø i Telemark", "NO") // Mangler
                .Build();
            val.FakturamottakerValidation(arbeidstilsynetsType);
            var result = val.GetResult();
            
            TestHelper.AssertResult(result, "", null);
        }
        [Fact(DisplayName = "4845.1.7 Fakturamottaker - ERROR test")]
        public void arbeidstilsynetsSamtykke_4845_1_7FakturamottakerValidationErrorTest()
        {
            var val = new ArbeidstilsynetsSamtykkeFormV1Validation();

            var arbeidstilsynetsType = new ArbeidstilsynetsSamtykkeBuilder()
                .Build();
            val.FakturamottakerValidation(arbeidstilsynetsType);
            var result = val.GetResult();
            
            TestHelper.AssertResult(result, "4845.1.7", 1, true);
        }
        [Fact(DisplayName = "4845.1.7.1 Fakturamottaker - ERROR test")]
        public void arbeidstilsynetsSamtykke_4845_1_7_1_FakturamottakerValidationErrorTest()
        {
            var val = new ArbeidstilsynetsSamtykkeFormV1Validation();

            var arbeidstilsynetsType = new ArbeidstilsynetsSamtykkeBuilder()
                .Fakturamottaker(false, false)

                .Build();
            val.FakturamottakerValidation(arbeidstilsynetsType);
            var result = val.GetResult();
            
            TestHelper.AssertResult(result, "4845.1.7.1", 1, true);
        }
        [Fact(DisplayName = "4845.1.7.1a Fakturamottaker - ERROR test")]
        public void arbeidstilsynetsSamtykke_4845_1_7_1a_FakturamottakerValidationErrorTest()
        {
            var val = new ArbeidstilsynetsSamtykkeFormV1Validation();

            var arbeidstilsynetsType = new ArbeidstilsynetsSamtykkeBuilder()
                .Fakturamottaker(true, true)
                .Build();
            val.FakturamottakerValidation(arbeidstilsynetsType);
            var result = val.GetResult();
            TestHelper.AssertResult(result, "4845.1.7.1", 1, true);
        }
        [Fact(DisplayName = "4845.1.7.2 Fakturamottaker - ERROR test")]
        public void arbeidstilsynetsSamtykke_4845_1_7_2_FakturamottakerValidationErrorTest()
        {
            var val = new ArbeidstilsynetsSamtykkeFormV1Validation();

            var arbeidstilsynetsType = new ArbeidstilsynetsSamtykkeBuilder()
                .Fakturamottaker(false, true, null, "BARB_126", "FaRef_1258", "NoenSaamTrengeFakture AS", "202012345", "StorGate 126", "3800", "Bø i Telemark", "NO") // Mangler
                .Build();
            val.FakturamottakerValidation(arbeidstilsynetsType);
            var result = val.GetResult();
            TestHelper.AssertResult(result, "4845.1.7.2", 1, true);
        }
        [Fact(DisplayName = "4845.1.7.2.1 Fakturamottaker - ERROR test")]
        public void arbeidstilsynetsSamtykke_4845_1_7_2_1_FakturamottakerValidationErrorTest()
        {
            var val = new ArbeidstilsynetsSamtykkeFormV1Validation();

            var arbeidstilsynetsType = new ArbeidstilsynetsSamtykkeBuilder()
                .Fakturamottaker(false, true, "254760673", "BARB_126", "FaRef_1258", "NoenSaamTrengeFakture AS", "202012345", "StorGate 126", "3800", "Bø i Telemark", "NO") // Mangler
                .Build();
            val.FakturamottakerValidation(arbeidstilsynetsType);
            var result = val.GetResult();
            TestHelper.AssertResult(result, "4845.1.7.2.1", 1, true);
        }
        [Fact(DisplayName = "4845.1.7.2.2 Fakturamottaker - ERROR test")]
        public void arbeidstilsynetsSamtykke_4845_1_7_2_2_FakturamottakerValidationErrorTest()
        {
            var val = new ArbeidstilsynetsSamtykkeFormV1Validation();

            var arbeidstilsynetsType = new ArbeidstilsynetsSamtykkeBuilder()
                .Fakturamottaker(false, true, "*254760673", "BARB_126", "FaRef_1258", "NoenSaamTrengeFakture AS", "202012345", "StorGate 126", "3800", "Bø i Telemark", "NO") // Mangler
                .Build();
            val.FakturamottakerValidation(arbeidstilsynetsType);
            var result = val.GetResult();
            TestHelper.AssertResult(result, "4845.1.7.2.2", 1, true);
        }
        [Fact(DisplayName = "4845.1.7.3 Fakturamottaker - ERROR test")]
        public void arbeidstilsynetsSamtykke_4845_1_7_3_FakturamottakerValidationErrorTest()
        {
            var val = new ArbeidstilsynetsSamtykkeFormV1Validation();

            var arbeidstilsynetsType = new ArbeidstilsynetsSamtykkeBuilder()
                .Fakturamottaker(true, false, "123456789", "BARB_126", "FaRef_1258", null, "202012345", "StorGate 126", "3800", "Bø i Telemark", "NO") // Mangler
                .Build();
            val.FakturamottakerValidation(arbeidstilsynetsType);
            var result = val.GetResult();
            TestHelper.AssertResult(result, "4845.1.7.3", 1, true);
        }
        [Fact(DisplayName = "4845.1.7.4 Fakturamottaker - ERROR test")]
        public void arbeidstilsynetsSamtykke_4845_1_7_4_FakturamottakerValidationErrorTest()
        {
            var val = new ArbeidstilsynetsSamtykkeFormV1Validation();

            var arbeidstilsynetsType = new ArbeidstilsynetsSamtykkeBuilder()
                .Fakturamottaker(true, false, "123456789", "BARB_126", "FaRef_1258", "NoenSaamTrengeFakture AS", "202012345") // Mangler
                .Build();
            val.FakturamottakerValidation(arbeidstilsynetsType);
            var result = val.GetResult();
            
            TestHelper.AssertResult(result, "4845.1.7.4", 1, true);
        }
        [Fact(DisplayName = "4845.1.7.4.1 Fakturamottaker - ERROR test")]
        public void arbeidstilsynetsSamtykke_4845_1_7_4_1_FakturamottakerValidationErrorTest()
        {
            var val = new ArbeidstilsynetsSamtykkeFormV1Validation();

            var arbeidstilsynetsType = new ArbeidstilsynetsSamtykkeBuilder()
                .Fakturamottaker(true, false, "123456789", "BARB_126", "FaRef_1258", "NoenSaamTrengeFakture AS", "202012345", null, "3800", "Bø i Telemark", "NO")
                .Build();
            val.FakturamottakerValidation(arbeidstilsynetsType);
            var result = val.GetResult();
            
            TestHelper.AssertResult(result, "4845.1.7.4.1", 1, true);
        }
        [Fact(DisplayName = "4845.1.7.4.2 Fakturamottaker - ERROR test")]
        public void arbeidstilsynetsSamtykke_4845_1_7_4_2_FakturamottakerValidationErrorTest()
        {
            var val = new ArbeidstilsynetsSamtykkeFormV1Validation();

            var arbeidstilsynetsType = new ArbeidstilsynetsSamtykkeBuilder()
                .Fakturamottaker(true, false, "123456789", "BARB_126", "FaRef_1258", "NoenSaamTrengeFakture AS", "202012345", "StorGate 126", null, "Bø i Telemark", "NO")
                .Build();
            val.FakturamottakerValidation(arbeidstilsynetsType);
            var result = val.GetResult();
            
            TestHelper.AssertResult(result, "4845.1.7.4.2", 1, true);
        }
        [Fact(DisplayName = "4845.1.7.4.3 Fakturamottaker - ERROR test")]
        public void arbeidstilsynetsSamtykke_4845_1_7_4_3_FakturamottakerValidationErrorTest()
        {
            var val = new ArbeidstilsynetsSamtykkeFormV1Validation();

            var arbeidstilsynetsType = new ArbeidstilsynetsSamtykkeBuilder()
                .Fakturamottaker(true, false, "123456789", "BARB_126", "FaRef_1258", "NoenSaamTrengeFakture AS", "202012345", "StorGate 126", "3800", null, "NO")
                .Build();
            val.FakturamottakerValidation(arbeidstilsynetsType);
            var result = val.GetResult();
            
            TestHelper.AssertResult(result, "4845.1.7.4.3", 1, true);
        }
        [Fact(DisplayName = "4845.1.7.4.4 Fakturamottaker - ERROR test")]
        public void arbeidstilsynetsSamtykke_4845_1_7_4_4_FakturamottakerValidationErrorTest()
        {
            var val = new ArbeidstilsynetsSamtykkeFormV1Validation();

            var arbeidstilsynetsType = new ArbeidstilsynetsSamtykkeBuilder()
                .Fakturamottaker(true, false, "123456789", "BARB_126", "FaRef_1258", "NoenSaamTrengeFakture AS", "202012345", "StorGate 126", "3800", "Bø i Telemark", null)
                .Build();
            val.FakturamottakerValidation(arbeidstilsynetsType);
            var result = val.GetResult();
            
            TestHelper.AssertResult(result, "4845.1.7.4.4", 1, true);
        }
        [Fact(DisplayName = "4845.1.7.5 Fakturamottaker - ERROR test")]
        public void arbeidstilsynetsSamtykke_4845_1_7_5_FakturamottakerValidationErrorTest()
        {
            var val = new ArbeidstilsynetsSamtykkeFormV1Validation();

            var arbeidstilsynetsType = new ArbeidstilsynetsSamtykkeBuilder()
                .Fakturamottaker(true, false, "123456789", null, "FaRef_1258", "NoenSaamTrengeFakture AS", "202012345", "StorGate 126", "3800", "Bø i Telemark", "no")
                .Build();
            val.FakturamottakerValidation(arbeidstilsynetsType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "4845.1.7.5", 1, true);
        }
        [Fact(DisplayName = "4845.1.7.6 Fakturamottaker - ERROR test")]
        public void arbeidstilsynetsSamtykke_4845_1_7_6_FakturamottakerValidationErrorTest()
        {
            var val = new ArbeidstilsynetsSamtykkeFormV1Validation();

            var arbeidstilsynetsType = new ArbeidstilsynetsSamtykkeBuilder()
                .Fakturamottaker(true, false, "123456789", "BARB_126", null, "NoenSaamTrengeFakture AS", "202012345", "StorGate 126", "3800", "Bø i Telemark", "no")
                .Build();
            val.FakturamottakerValidation(arbeidstilsynetsType);
            var result = val.GetResult();
            
            TestHelper.AssertResult(result, "4845.1.7.6", 1, true);
        }

        [Fact(DisplayName = "4845.1.8  Bygningstype - OK test")]
        public void arbeidstilsynetsSamtykke_4845_1_8_BygningstypeValidationOkTest()
        {
            var val = new ArbeidstilsynetsSamtykkeFormV1Validation();

            var arbeidstilsynetsType = new ArbeidstilsynetsSamtykkeBuilder()
                .BeskrivelseAvTilta("211", "Fabrikkbygning")
                .Build();
            val.BygningstypeValidation(arbeidstilsynetsType);
            var result = val.GetResult();
            TestHelper.AssertResult(result, "", 0);
        }

        [Fact(DisplayName = "4845.1.8  Bygningstype - Error test")]
        public void arbeidstilsynetsSamtykke_4845_1_8_BygningstypeValidationErrorTest()
        {
            var val = new ArbeidstilsynetsSamtykkeFormV1Validation();

            var arbeidstilsynetsType = new ArbeidstilsynetsSamtykkeBuilder()
                .BeskrivelseAvTilta(null, null)
                .Build();
            val.BygningstypeValidation(arbeidstilsynetsType);
            var result = val.GetResult();
            TestHelper.AssertResult(result, "4845.1.8", 1);
        }
        [Fact(DisplayName = "4845.1.8.1 Bygningstype - Error test")]
        public void arbeidstilsynetsSamtykke_4845_1_8_1_BygningstypeValidationErrorTest()
        {
            var val = new ArbeidstilsynetsSamtykkeFormV1Validation();

            var arbeidstilsynetsType = new ArbeidstilsynetsSamtykkeBuilder()
                .BeskrivelseAvTilta(null, "Fabrikkbygning")
                .Build();
            val.BygningstypeValidation(arbeidstilsynetsType);
            var result = val.GetResult();
            TestHelper.AssertResult(result, "4845.1.8.1", 1);
        }
        [Fact(DisplayName = "4845.1.8.2 Bygningstype - Error test")]
        public void arbeidstilsynetsSamtykke_4845_1_8_2_BygningstypeValidationErrorTest()
        {
            var val = new ArbeidstilsynetsSamtykkeFormV1Validation();

            var arbeidstilsynetsType = new ArbeidstilsynetsSamtykkeBuilder()
                .BeskrivelseAvTilta("199", "Annen beredskapsbygning")
                .Build();
            val.BygningstypeValidation(arbeidstilsynetsType);
            var result = val.GetResult();
            TestHelper.AssertResult(result, "4845.1.8.2", 1);
        }
        [Fact(DisplayName = "4845.1.8.3 Bygningstype - Error test")]
        public void arbeidstilsynetsSamtykke_4845_1_8_3_BygningstypeValidationErrorTest()
        {
            var val = new ArbeidstilsynetsSamtykkeFormV1Validation();

            var arbeidstilsynetsType = new ArbeidstilsynetsSamtykkeBuilder()
                .BeskrivelseAvTilta("829", "*Annen beredskapsbygning")
                .Build();
            val.BygningstypeValidation(arbeidstilsynetsType);
            var result = val.GetResult();
            var rulesChecked = TestHelper.ConverRulesCheckedJson(result);

            TestHelper.AssertResult(result, "4845.1.8.3", 1);
        }
        [Fact(DisplayName = "4845.1.9 ArbeidstilsynetsSaksnummerValidering - OK test")]
        public void arbeidstilsynetsSamtykke_4845_1_9_ArbeidstilsynetsSaksnummerValideringOKTest()
        {
            var val = new ArbeidstilsynetsSamtykkeFormV1Validation();

            var arbeidstilsynetsType = new ArbeidstilsynetsSamtykkeBuilder()
                .ProsjektInfo(null, null, "1234", "2020")
                .Build();
            val.ArbeidstilsynetsSaksnummerValidering(arbeidstilsynetsType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "", 0);
        }
        [Fact(DisplayName = "4845.1.9 ArbeidstilsynetsSaksnummerValidering - ERROR test")]
        public void arbeidstilsynetsSamtykke_4845_1_9_ArbeidstilsynetsSaksnummerValideringErrorTest()
        {
            var val = new ArbeidstilsynetsSamtykkeFormV1Validation();

            var arbeidstilsynetsType = new ArbeidstilsynetsSamtykkeBuilder()
                .ProsjektInfo(null, null, null, "2020")
                .Build();
            val.ArbeidstilsynetsSaksnummerValidering(arbeidstilsynetsType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "4845.1.9", 1);
        }
        [Fact(DisplayName = "4845.1.9.1 ArbeidstilsynetsSaksnummerValidering - ERROR test")]
        public void arbeidstilsynetsSamtykke_4845_1_9_1_ArbeidstilsynetsSaksnummerValideringErrorTest()
        {
            var val = new ArbeidstilsynetsSamtykkeFormV1Validation();

            var arbeidstilsynetsType = new ArbeidstilsynetsSamtykkeBuilder()
                .ProsjektInfo(null, null, "1234", null)
                .Build();
            val.ArbeidstilsynetsSaksnummerValidering(arbeidstilsynetsType);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "4845.1.9.1", 1);
        }

        // testModel Test
        [Fact(DisplayName = " ArbeidstilsynetsSamtykke Test Motor", Skip = "test data")]
        public void ArbeidstilsynetsSamtykkeTest()
        {
            var val = new ArbeidstilsynetsSamtykkeFormV1Validation();

            var municipalityValidator = new Mock<IMunicipalityValidator>();
            municipalityValidator.Setup(s => s.Validate_kommunenummerStatus(It.IsAny<string>())).Returns(new MunicipalityValidationResult() { Status = MunicipalityValidation.Ok, Message = "Unitests" });
            val.MunicipalityValidator = municipalityValidator.Object;

            var formSkjema = new ArbeidstilsynetsSamtykkeDfv41999()._04ArbeidstilsynetsSamtykke(true);
            var stringXml = TestHelper.GetXmlWithoutSpaces(formSkjema.Data);

            var arbeidstilsynets = TestHelper.GetValue<ArbeidstilsynetsSamtykkeType>(formSkjema);

            val.Validate(arbeidstilsynets, new List<string>() { "BeskrivelseTypeArbeidProsess" });
            var result = val.GetResult();

            var list = result.messages.Select(m => m.reference).ToList();
            var rulesChecked = TestHelper.ConverRulesCheckedJson(result);

            TestHelper.AssertResult(result, null, 0, null, true);
        }
    }
}
