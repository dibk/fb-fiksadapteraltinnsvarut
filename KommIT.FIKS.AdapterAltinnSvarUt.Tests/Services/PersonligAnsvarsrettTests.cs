﻿using System.Collections.Generic;
using System.IO;
using FluentAssertions;
using KommIT.FIKS.AdapterAltinnSvarUt.Services;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.SvarUt;
using Xunit;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Altinn;
using Moq;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Prefill;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Notification;
using Serilog;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Azure;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.WS.Altinn;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;
using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Encryption;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Svarut;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Svarut.StatusService;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.UserNotification;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.WS.AltinnCorrespondenceAgency;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Validations;
using KommIT.FIKS.AdapterAltinnSvarUt.QueuePreparators;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.DecryptFnrForTh;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Tests.Services
{
    public class PersonligAnsvarsrettTests
    {
        [Fact]
        public void PersonligAnsvarsrettValidateOkTest()
        {
            var formDataAsXml = File.ReadAllText(@"Services\testskjema\5500_41791_SøknadAnsvarsrettSelvbygger1.xml");
            SoeknadPersonligAnsvarsrettFormV1 form = new SoeknadPersonligAnsvarsrettFormV1();
            var municipalityValidator = new Mock<IMunicipalityValidator>();
            municipalityValidator.Setup(s => s.ValidateKommunenummer(It.IsAny<string>())).Returns(new GeneralValidationResult() { Status = ValidationStatus.Ok, Message = "UnitTest" });
            form.MunicipalityValidator = municipalityValidator.Object;

            form.InitiateForm(formDataAsXml);
            var result = form.ValidateData();
            result.IsOk().Should().BeTrue();
        }

        [Fact]
        public void PersonligAnsvarsrett2ValidateOkTest()
        {
            var formDataAsXml = File.ReadAllText(@"Services\testskjema\5500_41791_selvbygger2.xml");
            SoeknadPersonligAnsvarsrettFormV1 form = new SoeknadPersonligAnsvarsrettFormV1();
            var municipalityValidator = new Mock<IMunicipalityValidator>();
            municipalityValidator.Setup(s => s.ValidateKommunenummer(It.IsAny<string>())).Returns(new GeneralValidationResult() { Status = ValidationStatus.Ok, Message = "UnitTest" });
            form.MunicipalityValidator = municipalityValidator.Object;

            form.InitiateForm(formDataAsXml);
            var result = form.ValidateData();
            result.IsOk().Should().BeTrue();
        }

        //*********************************************************************
        // Kan ikke kjøres da den er avhengig av Hangfire
        //*********************************************************************
        //[Fact(Skip = "bugfixtest")]
        //public void PersonligAnsvarsrettShippingValidateOkTest()
        //{
        //    var logService = new Mock<ILogEntryService>();
        //    var logger = new Mock<ILogger>();
        //    var formMetadataService = new Mock<IFormMetadataService>();
        //    var altinnService = new Mock<IAltinnService>();
        //    var altinnPrefillService = new Mock<IAltinnPrefillService>();
        //    var kommuneService = new Mock<IMunicipalityService>();
        //    var decryptionFactory = new Mock<IDecryptionFactory>();
        //    decryptionFactory.Setup(f => f.GetDecryptor()).Returns(new Mock<IDecryption>().Object);
        //    var syncService = new Mock<ISyncRecordsOfCombinedDistributions>();
        //    var appLogService = new Mock<IApplicationLogService>();
        //    var decryptFnr = new Mock<IDecryptFnr>();
        //    var addSignatureDocument = new Mock<ITiltakshaversSignedApprovalDocument>();
        //    var submittalQueueService = new Mock<ExportToSubmittalQueueService>();

        //    var svarUtServicePrint = new Mock<ISvarUtService>();
        //    svarUtServicePrint.Setup(c => c.IsServiceFor).Returns(SvarUtServiceTypeEnum.Print);
            
        //    var svarUtService = new Mock<ISvarUtService>();
        //    svarUtServicePrint.Setup(c => c.IsServiceFor).Returns(SvarUtServiceTypeEnum.Shipping);
            
        //    var svarUtServices = new List<ISvarUtService>();
        //    svarUtServices.Add(svarUtServicePrint.Object);
        //    svarUtServices.Add(svarUtService.Object);


        //    var svarUtServiceFactory = new SvarUtServiceFactory(svarUtServices);

        //    var svarUtStatusService = new Mock<ISvarUtStatusService>();
        //    var ilogService = new Mock<ILogEntryService>();
            
        //    kommuneService.Setup(s => s.GetMunicipality(It.IsAny<string>())).Returns(new Municipality() { Code="9999", Name = "Test", OrganizationNumber = "910297937" });
            
        //    IFormShippingService formShippingService = new FormShippingService(svarUtServiceFactory, formMetadataService.Object, kommuneService.Object, decryptFnr.Object, ilogService.Object, addSignatureDocument.Object);
        //    var CorrService = new Mock<IAltinnCorrespondenceService>();
        //    IFileStorage iFileStorage = new FileStorage(new BlobStorage());
        //    IFileDownloadStatusService fileDownloadStatusService = new FileDownloadStatusService(new ApplicationDbContext());
        //    UserNotificationHandler userNotificationHandler = new UserNotificationHandler(new UserNotificationService(new ApplicationDbContext()));

        //    //Sende ok på kvitteringsmelding
        //    CorrService.Setup(s => s.SendNotification(It.IsAny<string>(), It.IsAny<InsertCorrespondenceV2>())).Returns(new ReceiptExternal() { ReceiptStatusCode = ReceiptStatusEnum.OK });

        //    SendPrefillFormServiceV2 sendPrefillService = new SendPrefillFormServiceV2(CorrService.Object, altinnPrefillService.Object, logService.Object, formMetadataService.Object, decryptionFactory.Object, svarUtServiceFactory, userNotificationHandler, syncService.Object, svarUtStatusService.Object);
        //    IAltinnFormDeserializer desrService = new AltinnFormDeserializer();
        //    FormDistributionServiceV2 formDistService = new FormDistributionServiceV2(CorrService.Object, logService.Object, formMetadataService.Object, sendPrefillService, decryptionFactory.Object, iFileStorage, fileDownloadStatusService);
        //    var filestorage = new Mock<IFileStorage>();
        //    var filedownloadStatus = new Mock<IFileDownloadStatusService>();
        //    NotificationServiceV2 formNotService = new NotificationServiceV2(CorrService.Object, logService.Object, formMetadataService.Object, filestorage.Object, filedownloadStatus.Object, syncService.Object, new Mock<ILogger>().Object);
        //    INotificationServiceResolver notificationServiceResolver = new NotificationServiceResolver(new List<INotificationServiceV2>() { formNotService });
        //    var SendCorrService = new SendCorrespondenceHelper(CorrService.Object, logService.Object, logger.Object);
        //    var sendNotification = new Mock<ISendSecondNotifications>();


        //    AltinnFormDownloadAndRoutingService routing = new AltinnFormDownloadAndRoutingService(altinnService.Object, desrService, formShippingService, logService.Object, formMetadataService.Object, formDistService, notificationServiceResolver, SendCorrService, filestorage.Object, sendNotification.Object, appLogService.Object, submittalQueueService.Object);

        //    string ArchivedFormTaskAsXml = System.IO.File.ReadAllText(@"Services\Altinntestmeldinger\AR4371578.xml");
        //    ArchivedFormTaskDQBE formTask = SerializeUtil.DeserializeFromString<ArchivedFormTaskDQBE>(ArchivedFormTaskAsXml);

        //    routing.ShipAltinnForm("AR4371578", formTask, false);
        //}
    }
}