﻿using System.Collections.Generic;
using FluentAssertions;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.NotificationForm;
using KommIT.FIKS.AdapterAltinnSvarUt.Tests.Services.Builders;
using Xunit;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Tests.Services
{
    public class ErklaeringAnsvarsrettTests
    {
        private readonly ErklaeringAnsvarsrettV2Validator _validator;

        public ErklaeringAnsvarsrettTests()
        {
            _validator = new ErklaeringAnsvarsrettV2Validator();
        }

        [Fact]
        public void ErklaeringAnsvarsrettValidateOkTest()
        {
        }

        [Fact]
        public void MinstEnEiendomFeiler()
        {
            _validator.MinstEnEiendom(new AnsvarsrettBuilder().Build());
            NumberOfErrorsShouldBe(1);
        }

        [Fact]
        public void MinstEnEiendom()
        {
            _validator.MinstEnEiendom(new AnsvarsrettBuilder().EiendomByggested().Build());
            NumberOfErrorsShouldBe(0);
        }

        [Fact]
        public void MinstEtAnsvarsomraadeFeiler()
        {
            _validator.MinstEtAnsvarsomraade(new AnsvarsrettBuilder().Build());
            NumberOfErrorsShouldBe(1);
        }

        [Fact]
        public void MinstEtAnsvarsomraade()
        {
            _validator.MinstEtAnsvarsomraade(new AnsvarsrettBuilder()
                .Ansvarsrett("KONTROLL", "Ansvarlig kontroll", "Overordnet ansvar for kontroll", "2", "Tiltaksklasse 2",
                    false, true, true, true, "123456",
                    null, null, true)
                .Build());
            NumberOfErrorsShouldBe(0);
        }

        //[Fact]
        //public void KontaktpersonForForetak()
        //{
        //    _validator.ForetaketsKontaktpersonErPaakrevd(new AnsvarsrettBuilder().Foretak("914994780").Build());
        //    NumberOfErrorsShouldBe(0);
        //}

        //[Fact(Skip="")]
        //public void KontaktpersonForForetakMangler()
        //{
        //    _validator.ForetaketsKontaktpersonErPaakrevd(new AnsvarsrettBuilder().Build());
        //    NumberOfErrorsShouldBe(1);
        //}

        //[Fact]
        //public void OrganisasjonsNrForForetak()
        //{
        //    _validator.OrganisasjonsNrErPaakrevd(new AnsvarsrettBuilder().Foretak("914994780").Build());
        //    NumberOfErrorsShouldBe(0);
        //}

        [Fact]
        public void FeilIOrganisasjonsNrForForetak()
        {

            _validator.AnsvarligForetakGyldigOrganisasjonsNr(new AnsvarsrettBuilder().Foretak("914994783").Build());
            NumberOfErrorsShouldBe(1);
        }

        [Fact]
        public void RiktigOrganisasjonsNrForForetak()
        {

            _validator.AnsvarligForetakGyldigOrganisasjonsNr(new AnsvarsrettBuilder().Foretak("914994780").Build());
            NumberOfErrorsShouldBe(0);
        }
        
        [Fact(DisplayName = "4965.2.7 AnsvarligSokerOgForetakValidering ok Test")]
        public void AnsvarligSokerOgForetakValidering()
        {
            var validator = new ErklaeringAnsvarsrettV2Validator();
            var erklearingAnsvarsrett = new AnsvarsrettBuilder()
                    .Foretak("123456789")
                    //.AnsvarligSoeker("123456789")
                    .Build()
                ;
            validator.AnsvarligSokerOgForetakValidering(erklearingAnsvarsrett,new List<string>(){"noko", "Søknad om tillatelse i ett trinn" });

            var result = validator.GetResult();
            result.messages.Count.Should().Be(0);
        } 
        [Fact(DisplayName = "4965.2.8 ValidereAnsvarsomraader ok Test")]
        public void ValidereAnsvarsomraader()
        {
            var validator = new ErklaeringAnsvarsrettV2Validator();
            var erklearingAnsvarsrett = new AnsvarsrettBuilder()
                    .Ansvarsrett("KONTROLL", "Ansvarlig kontroll", "Overordnet ansvar for kontroll","2", "Tiltaksklasse 2",
                        false,true,true,true,"123456",
                        null, null, true)
                    .Ansvarsrett("PRO", "Ansvarlig prosjektering", "Overordnet ansvar for kontroll","2", "Tiltaksklasse 2",
                        false,true,true,true,"123456",
                        null,true,null)
                    .Ansvarsrett("UTF", "Ansvarlig utførelse", "Overordnet ansvar for kontroll","2", "Tiltaksklasse 2",
                        false,true,true,true,"123456",
                        true,null,null)
                    .Build()
                ;
            validator.ValidereAnsvarsomraader(erklearingAnsvarsrett);

            var result = validator.GetResult();
            result.messages.Count.Should().Be(0);
        }
        //
        private void NumberOfErrorsShouldBe(int expectedNumberOfErrors)
        {
            var validationResult = _validator.GetResult();

            if (expectedNumberOfErrors == 0)
                validationResult.IsOk().Should().BeTrue();
            else
                validationResult.HasErrors().Should().BeTrue();

            validationResult.Errors.Should().Be(expectedNumberOfErrors);
        }
    }
}