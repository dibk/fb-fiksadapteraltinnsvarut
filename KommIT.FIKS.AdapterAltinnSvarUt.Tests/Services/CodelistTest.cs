﻿using System.Collections.Generic;
using DIBK.FBygg.Altinn.MapperCodelist;
using FluentAssertions;
using Xunit;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Tests.Services
{
    public class CodelistTest
    {
        [Fact]
        public void GetCodelistTest()
        {
            bool goodCodelist = new CodeListService().IsCodelistValid(CodelistNames.Dispensasjonstype.ToString(), "PLAN");
            bool badCodelist = new CodeListService().IsCodelistValid("dispensasjonstype", "PLANING");
            goodCodelist.Should().BeTrue();
            badCodelist.Should().BeFalse();
        }


        [Fact]
        public void ValidateGoodCodelistLabelTest()
        {
            bool goodCodelistLabel = new CodeListService().IsCodelistLabelValid(
                "bygningstype",
                "145",
                "Store sammenbygde boligbygg på 3 og 4 etasjer");
            goodCodelistLabel.Should().BeTrue();
        }

        [Fact]
        public void ValidateIncorrectCodelistLabelTest()
        {
            bool goodCodelistLabel = new CodeListService().IsCodelistLabelValid(
                "bygningstype",
                "313",
                "Medianerrbygning");
            goodCodelistLabel.Should().BeFalse();
        }

        [Fact]
        public void ValidetKommuneNrTest()
        {
            string kommuneNameOk  = new CodeListService().GetValidKommuneName("0301");
            string kommuneNameBad = new CodeListService().GetValidKommuneName("2222");

            kommuneNameOk.Should().Be("Oslo");
            kommuneNameBad.Should().BeNull();


        }



        [Fact]
        public void GetKommuneNrTest()
        {
            Dictionary<string, CodelistFormat> codeData = new CodeListService().GetCodelist("kommunenummer", RegistryType.Kommunenummer);
            codeData.Should().NotBeEmpty();
        }


        [Fact]
        public void GetFunctionTest()
        {
            Dictionary<string, CodelistFormat> codeData = new CodeListService().GetCodelist("Funksjon", RegistryType.KodelisteByggesoknad);
            codeData.Should().HaveCount(4); 
        }
    }
}