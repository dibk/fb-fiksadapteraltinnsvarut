using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Xml;
using FluentAssertions;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.FormsSamples;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.SampleData;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.SvarUtForm;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Validations;
using KommIT.FIKS.AdapterAltinnSvarUt.Tests.Services.Builders;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;
using Moq;
using no.kxml.skjema.dibk.endringavtillatelse;
using Xunit;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Tests.Services
{
    public class EndringAvTillatelseTests
    {

        private EndringAvTillatelseType _form;
        private EndringAvTillatelseFormValidator _validator;
        private List<string> _formSetElements;
        private Skjema _formSkjema;

        public EndringAvTillatelseTests()
        {
            _validator = new EndringAvTillatelseFormValidator();
        }
        internal void EndringAvAreal()
        {
            _formSkjema = new EndringAvTillatelseDfV42350().EndringAvTillatelseAvAreal(true);
            _form = TestHelper.GetValue<EndringAvTillatelseType>(_formSkjema);
            _formSetElements = new List<string>()
            {
                "UnderlagUtnytting"
            };
        }
        internal void EndringAvPlasering()
        {
            _formSkjema = new EndringAvTillatelseDfV42350().EndringAvTillatelseAvPlassering(true);
            _form = TestHelper.GetValue<EndringAvTillatelseType>(_formSkjema);
            _formSetElements = new List<string>()
            {
                "UnderlagUtnytting","RedegjoerelseSkredOgFlom"
            };
        }

        [Fact(DisplayName = "4402.1.6 Minst en endring")]
        public void EndringAvTillatelse_4402_1_6_MinstEnEndringValideringOkTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .EndringAvTillatelse(true, null, null, null, null)
                .Build();

            val.MinstEnEndringValidering(endringAvTillatelse);
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4402.1.6 Minst en endring")]
        public void EndringAvTillatelse_4402_1_6_MinstEnEndringValideringErrorTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .EndringAvTillatelse(null, null, null, null, null)
                .Build();

            val.MinstEnEndringValidering(endringAvTillatelse);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.6").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4402.1.3 MinstEnEiendom - OK")]
        public void EndringAvTillatelse_4402_1_3_MinstEnEiendomOKTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .EiendomByggested("2004", "1", "3", "0", "0", "Hammerfest", "H0101")
                .Build();
            val.MinstEnEiendom(endringAvTillatelse);
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);

        }
        [Fact(DisplayName = "4402.1.3 MinstEnEiendom - Error")]
        public void EndringAvTillatelse_4402_1_3_MinstEnEiendomErrorTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .Build();
            val.MinstEnEiendom(endringAvTillatelse);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.3").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);

        }

        [Fact(DisplayName = "4402.1.7 EiendomByggestedValidation - OK", Skip = "disable_not working in teamcity")]
        public void EndringAvTillatelse_4402_1_7_EiendomByggestedValidationOkTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .EiendomByggested("2004", "1", "3", "0", "0", "Hammerfest", "H0101", "192423880", "Båtsfjorden 6", "Båtsfjorden", "6", "9664", "SANDØYBOTN")
                .Build();

            val.EiendomByggestedValidation(endringAvTillatelse);
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4402.1.7 EiendomByggestedValidation - Error")]
        public void EndringAvTillatelse_4402_1_7_EiendomByggestedValidationErrorTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .EiendomByggested(null, "1", "3", "0", "0", "Hammerfest", "H0101")
                .Build();

            val.EiendomByggestedValidation(endringAvTillatelse);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.7").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4402.1.2 EiendomByggestedValidation - Error", Skip = "disable_not working in teamcity")]
        public void EndringAvTillatelse_4402_1_2_EiendomByggestedValidationErrorTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .EiendomByggested("8514", "1", "3", "0", "0", "Hammerfest", "H0101")
                .Build();
            val.EiendomByggestedValidation(endringAvTillatelse);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(2);
        }
        [Fact(DisplayName = "4402.1.7.1 EiendomByggestedValidation - Error", Skip = "disable_not working in teamcity")]
        public void EndringAvTillatelse_4402_1_7_1_EiendomByggestedValidationErrorTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .EiendomByggested("2004", "1", "3", "0", "0", null, "H0101")
                .Build();
            val.EiendomByggestedValidation(endringAvTillatelse);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.7.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4402.1.8 EiendomByggestedValidation - Error", Skip = "disable_not working in teamcity")]
        public void EndringAvTillatelse_4402_1_8_EiendomByggestedValidationErrorTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .EiendomByggested("2004", "1", "3", "0", "0", "Hammerfest", "H0101x")
                .Build();
            val.EiendomByggestedValidation(endringAvTillatelse);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.8").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4402.1.14 EiendomByggestedValidation - Error", Skip = "disable_not working in teamcity")]
        public void EndringAvTillatelse_4402_1_14_EiendomByggestedValidationErrorTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .EiendomByggested("2004", "1", "88", "0", "0", "Hammerfest", "H0101")
                .Build();

            val.EiendomByggestedValidation(endringAvTillatelse);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.14").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4402.1.15 EiendomByggestedValidation - Error", Skip = "Error in MatrikkelServices 'IsBygningOnMatrikkelnr'")]
        public void EndringAvTillatelse_4402_1_15_EiendomByggestedValidationErrorTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .EiendomByggested("2004", "1", "3", "0", "0", "Hammerfest", "H0101", "*192423880")
                .Build();

            val.EiendomByggestedValidation(endringAvTillatelse);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.15").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4402.1.18 EiendomByggestedValidation - Error", Skip = "disable_not working in teamcity")]
        public void EndringAvTillatelse_4402_1_18_EiendomByggestedValidationErrorTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .EiendomByggested("2004", "1", "3", "0", "0", "Hammerfest", "H0101", "192423880", "Båtsfjorden 6", "*Båtsfjorden", "6", "9664", "SANDØYBOTN")
                .Build();

            val.EiendomByggestedValidation(endringAvTillatelse);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.18").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4402.1.65 EiendomByggestedValidation - Error", Skip = "disable_not working in teamcity")]
        public void EndringAvTillatelse_4402_1_65_EiendomByggestedValidationErrorTest()
        {
            var ruleId = "4402.1.65";

            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .EiendomByggested("2004", "1", "3", "0", "0", "Hammerfest", "H0101", "192423880", "Båtsfjorden 6", null, "6", "9664", "SANDØYBOTN")
                .Build();

            val.EiendomByggestedValidation(endringAvTillatelse);
            var result = val.GetResult();
            var ruleMessagetype = result.rulesChecked.First(r => r.id == ruleId);

            result.messages.Where(m => m.reference == ruleId).Should().NotBeNullOrEmpty();
            result.messages.Where(m => m.reference == ruleId).All(r => r.messagetype == ruleMessagetype.messagetype).Should().BeTrue();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4402.1.9 VarslingValidering - Ok")]
        public void EndringAvTillatelse_4402_1_9_VarslingValideringOkTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .Varsling(false, true, "Merknaden er tatt til følge", "1")
                .Build();
            val.VarslingValidering(endringAvTillatelse, new List<string>() { "KvitteringNabovarsel", "Gjenpart nabovarsel", "Opplysninger gitt i nabovarsel", "Nabomerknader" });
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4402.1.9 VarslingValidering - Error")]
        public void EndringAvTillatelse_4402_1_9_VarslingValideringErrorTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .Varsling(false, true, "Merknaden er tatt til følge", "1")
                .Build();
            val.VarslingValidering(endringAvTillatelse, new List<string>() { "KvitteringNabovarsel", "Nabomerknader" });
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.9").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4402.1.10 VarslingValidering - OK")]
        public void EndringAvTillatelse_4402_1_10_VarslingValideringOkTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .Varsling(false, null, "Merknaden er tatt til følge", "1")
                .Build();
            val.VarslingValidering(endringAvTillatelse, new List<string>() { "Opplysninger gitt i nabovarsel ", "Gjenpart nabovarsel", "KvitteringNabovarsel", "Nabomerknader" });
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4402.1.10 VarslingValidering - Error")]
        public void EndringAvTillatelse_4402_1_10_VarslingValideringErrorTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .Varsling(false, null, "Merknaden er tatt til følge", "1")
                .Build();
            val.VarslingValidering(endringAvTillatelse, new List<string>() { "Opplysninger gitt i nabovarsel ", "Gjenpart nabovarsel", "Nabomerknader" });
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.10").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4402.1.11 VarslingValidering - OK")]
        public void EndringAvTillatelse_4402_1_11_VarslingValideringOkTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .Varsling(false, true, "Merknaden er tatt til følge", "1")
                .Build();
            val.VarslingValidering(endringAvTillatelse, new List<string>() { "Opplysninger gitt i nabovarsel ", "Gjenpart nabovarsel", "KvitteringNabovarsel", "Nabomerknader" });
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4402.1.11 VarslingValidering - Error")]
        public void EndringAvTillatelse_4402_1_11_VarslingValideringErrorTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .Varsling(false, true, "Merknaden er tatt til følge", null)
                .Build();
            val.VarslingValidering(endringAvTillatelse, new List<string>() { "Opplysninger gitt i nabovarsel ", "Gjenpart nabovarsel", "KvitteringNabovarsel", "Nabomerknader" });
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.11").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4402.1.12 VarslingValidering - OK")]
        public void EndringAvTillatelse_4402_1_12_VarslingValideringOkTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .Varsling(false, true, "Merknaden er tatt til følge", "1")
                .Build();
            val.VarslingValidering(endringAvTillatelse, new List<string>() { "Opplysninger gitt i nabovarsel ", "Gjenpart nabovarsel", "KvitteringNabovarsel", "Nabomerknader" });
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4402.1.12 VarslingValidering - Error")]
        public void EndringAvTillatelse_4402_1_12_VarslingValideringErrorTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .Varsling(false, true, null, "1")
                .Build();
            val.VarslingValidering(endringAvTillatelse, new List<string>() { "Opplysninger gitt i nabovarsel ", "Gjenpart nabovarsel", "KvitteringNabovarsel", "Nabomerknader" });
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.12").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4402.1.13 VarslingValidering - Ok")]
        public void EndringAvTillatelse_4402_1_13_VarslingValideringOkTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .Varsling(false, true, "Merknaden er tatt til følge", "1")
                .Build();
            val.VarslingValidering(endringAvTillatelse, new List<string>() { "KvitteringNabovarsel", "Gjenpart nabovarsel", "Opplysninger gitt i nabovarsel", "Nabomerknader" });
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4402.1.13 VarslingValidering - Error")]
        public void EndringAvTillatelse_4402_1_13_VarslingValideringErrorTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .Varsling(false, true, "Merknaden er tatt til følge", "1")
                .Build();
            val.VarslingValidering(endringAvTillatelse, new List<string>() { "KvitteringNabovarsel", "Gjenpart nabovarsel", "Opplysninger gitt i nabovarsel" });
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.13").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4402.1.17 søkes om dispensasjon Validering - OK")]
        public void EndringAvTillatelse_4402_1_17_DispensasjonValideringOkTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .Dispensasjon("Søknad om dispensasjon fra bestemmelsen NN i plan", "Begrunnelse for dispensasjon", "PLAN", "Arealplan")
                .Build();

            val.DispensasjonValidering(endringAvTillatelse, new List<string>(){ "Folgebrev", "Dispensasjonssoeknad" });
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4402.1.17 søkes om dispensasjon Validering - Error")]
        public void EndringAvTillatelse_4402_1_17_DispensasjonValideringErrorTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .Build();

            val.DispensasjonValidering(endringAvTillatelse, new List<string>() { "Folgebrev" });
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.17").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4402.1.16 søkes om dispensasjon Validering - OK")]
        public void EndringAvTillatelse_4402_1_16_DispensasjonValideringOKTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
               .Dispensasjon("Søknad om dispensasjon fra bestemmelsen NN i plan", "Begrunnelse for dispensasjon", "PLAN", "Arealplan")
                .Build();

            val.DispensasjonValidering(endringAvTillatelse, new List<string>() { "Folgebrev", "Dispensasjonssoeknad" });
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4402.1.16 søkes om dispensasjon Validering - Error")]
        public void EndringAvTillatelse_4402_1_16_DispensasjonValideringErrorTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
               .Dispensasjon(null, "Begrunnelse for dispensasjon", "PLAN", "Arealplan")
                .Build();

            val.DispensasjonValidering(endringAvTillatelse, new List<string>() { "Folgebrev" });
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.16").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);

        }

        [Fact(DisplayName = "4402.1.16.1 søkes om dispensasjon Validering - Error")]
        public void EndringAvTillatelse_4402_1_16_1_DispensasjonValideringErrorTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
               .Dispensasjon("Søknad om dispensasjon fra bestemmelsen NN i plan", null, "PLAN", "Arealplan")
                .Build();

            val.DispensasjonValidering(endringAvTillatelse, new List<string>() { "Folgebrev" });
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.16.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4402.1.2.1 søkes om dispensasjon Validering - Error")]
        public void EndringAvTillatelse_4402_1_2_1_DispensasjonValideringErrorTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .Dispensasjon("Søknad om dispensasjon fra bestemmelsen NN i plan", "Begrunnelse for dispensasjon", "noko", "Arealplan")
                .Build();

            val.DispensasjonValidering(endringAvTillatelse, new List<string>() { "Folgebrev", "Dispensasjonssoeknad" });
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.2.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        //[Fact(DisplayName = "4402.1.2.18 søkes om dispensasjon Validering - Error")]
        //public void EndringAvTillatelse_4402_1_2_18_DispensasjonValideringErrorTest()
        //{
        //    var val = new EndringAvTillatelseFormValidator();

        //    var endringAvTillatelse = new EndringAvTillatelseBuilder()
        //        .endringSomKreverDispensasjon("Søknad om dispensasjon fra bestemmelsen NN i plan", "Begrunnelse for dispensasjon", null, "Arealplan")
        //        .Build();

        //    val.DispensasjonValidering(endringAvTillatelse);
        //    var result = val.GetResult();

        //    result.messages.Where(m => m.reference == "4402.1.2.18").Should().NotBeNullOrEmpty();
        //    result.messages.Count.Should().Be(1);
        //}

        [Fact(DisplayName = "4402.1.2.2 MinstEtTiltak - Ok")]
        public void EndringAvTillatelse_4402_1_2_2_MinstEtTiltakOkTest()
        {
            var val = new EndringAvTillatelseFormValidator();
            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .BeskrivelseAvTiltak("nyttbyggboligformal", "Nytt bygg - Boligformål", "Ny fritidsbolig med laftet anneks", "Fritidsbolig", "Fritidsbolig")
                .Build();
            val.MinstEtTiltak(endringAvTillatelse);
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4402.1.1 MinstEtTiltak - Error")]
        public void EndringAvTillatelse_4402_1_1_MinstEtTiltakErrorTest()
        {
            var val = new EndringAvTillatelseFormValidator();
            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .BeskrivelseAvTiltak(null, null, "Ny fritidsbolig med laftet anneks", "Fritidsbolig", "Fritidsbolig")
                .Build();
            val.MinstEtTiltak(endringAvTillatelse);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4402.1.2.2 MinstEtTiltak - Error")]
        public void EndringAvTillatelse_4402_1_2_2_MinstEtTiltakErrorTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .BeskrivelseAvTiltak("*nyttbyggboligformal", "Nytt bygg - Boligformål", "Ny fritidsbolig med laftet anneks", "Fritidsbolig", "Fritidsbolig")
                .Build();
            val.MinstEtTiltak(endringAvTillatelse);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.2.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        //[Fact(DisplayName = "4402.1.2.17 MinstEtTiltak - Error")]
        //public void EndringAvTillatelse_4402_1_2_17_MinstEtTiltakErrorTest()
        //{
        //    var val = new EndringAvTillatelseFormValidator();

        //    var endringAvTillatelse = new EndringAvTillatelseBuilder()
        //        .BeskrivelseAvTiltak(null, "Nytt bygg - Boligformål", "Ny fritidsbolig med laftet anneks", "Fritidsbolig", "Fritidsbolig")
        //        .Build();
        //    val.MinstEtTiltak(endringAvTillatelse);
        //    var result = val.GetResult();

        //    result.messages.Where(m => m.reference == "4402.1.2.17").Should().NotBeNullOrEmpty();
        //    result.messages.Count.Should().Be(1);
        //}
        [Fact(DisplayName = "4402.1.2.3 MinstEtTiltak - Error")]
        public void EndringAvTillatelse_4402_1_2_3_MinstEtTiltakErrorTest()
        {
            var val = new EndringAvTillatelseFormValidator();
            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .BeskrivelseAvTiltak("nyttbyggboligformal", "*Nytt bygg - Boligformål", "Ny fritidsbolig med laftet anneks", "Fritidsbolig", "Fritidsbolig", "161", "Hytter, sommerhus og fritidsbygg", "y", "Næringsgruppe for annet som ikke er næring", "andre", "andre")
                .Build();
            val.MinstEtTiltak(endringAvTillatelse);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.2.3").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4402.1.2.15 MinstEtTiltak - Error")]
        public void EndringAvTillatelse_4402_1_2_15_MinstEtTiltakErrorTest()
        {
            var val = new EndringAvTillatelseFormValidator();
            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .BeskrivelseAvTiltak("nyttbyggboligformal", null, "Ny fritidsbolig med laftet anneks", "Fritidsbolig", "Fritidsbolig", "161", "Hytter, sommerhus og fritidsbygg", "y", "Næringsgruppe for annet som ikke er næring", "andre", "andre")
                .Build();
            val.MinstEtTiltak(endringAvTillatelse);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.2.15").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }


        //MatrikkelInformasjonValidering

        [Fact(DisplayName = "4402.1.2.16 MatrikkelInformasjonValidering - Error")]
        public void EndringAvTillatelse_4402_1_2_16_MinstEtTiltakErrorTest()
        {
            var val = new EndringAvTillatelseFormValidator();
            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .BeskrivelseAvTiltak("nyttbyggboligformal", "Nytt bygg - Boligformål", "Ny fritidsbolig med laftet anneks", "Fritidsbolig", "Fritidsbolig", "161", null, "y", "Næringsgruppe for annet som ikke er næring")
                .Build();
            val.MinstEtTiltak(endringAvTillatelse);
            val.MatrikkelInformasjonValidering(endringAvTillatelse);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.2.16").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4402.1.2.4 MinstEtTiltak - Error")]
        public void EndringAvTillatelse_4402_1_2_4_MinstEtTiltakErrorTest()
        {
            var val = new EndringAvTillatelseFormValidator();
            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .Build();
            val.MinstEtTiltak(endringAvTillatelse);
            val.MatrikkelInformasjonValidering(endringAvTillatelse);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.2.4").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4402.1.2.5 MinstEtTiltak - Error")]
        public void EndringAvTillatelse_4402_1_2_5_MinstEtTiltakErrorTest()
        {
            var val = new EndringAvTillatelseFormValidator();
            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .BeskrivelseAvTiltak("nyttbyggboligformal", "Nytt bygg - Boligformål", "Ny fritidsbolig med laftet anneks", "Fritidsbolig", "Fritidsbolig", "161", "Hytter, sommerhus og fritidsbygg", "*y", "Næringsgruppe for annet som ikke er næring")
                .Build();
            val.MinstEtTiltak(endringAvTillatelse);
            val.MatrikkelInformasjonValidering(endringAvTillatelse);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.2.5").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4402.1.2.6 MinstEtTiltak - Error")]
        public void EndringAvTillatelse_4402_1_2_6_MinstEtTiltakErrorTest()
        {
            var val = new EndringAvTillatelseFormValidator();
            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .BeskrivelseAvTiltak("nyttbyggboligformal", "Nytt bygg - Boligformål", "Ny fritidsbolig med laftet anneks", "Fritidsbolig", "Fritidsbolig", "*161", "Hytter, sommerhus og fritidsbygg",
                    "y", "Næringsgruppe for annet som ikke er næring", "andre", "andre")
                .Build();
            val.MinstEtTiltak(endringAvTillatelse);
            val.MatrikkelInformasjonValidering(endringAvTillatelse);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.2.6").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4402.1.2.7 MinstEtTiltak - Error")]
        public void EndringAvTillatelse_4402_1_2_7_MinstEtTiltakErrorTest()
        {
            var val = new EndringAvTillatelseFormValidator();
            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .BeskrivelseAvTiltak("nyttbyggboligformal", "Nytt bygg - Boligformål", "Ny fritidsbolig med laftet anneks", "Fritidsbolig", "Fritidsbolig", "161", "*Hytter, sommerhus og fritidsbygg", null)
                .Build();
            val.MinstEtTiltak(endringAvTillatelse);
            val.MatrikkelInformasjonValidering(endringAvTillatelse);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.2.7").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4402.1.2.8 MinstEtTiltak - Error")]
        public void EndringAvTillatelse_4402_1_2_8_MinstEtTiltakErrorTest()
        {
            var val = new EndringAvTillatelseFormValidator();
            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .BeskrivelseAvTiltak("nyttbyggboligformal", "Nytt bygg - Boligformål", "Ny fritidsbolig med laftet anneks", "*Fritidsbolig", "Fritidsbolig")
                .Build();
            val.MinstEtTiltak(endringAvTillatelse);
            val.MatrikkelInformasjonValidering(endringAvTillatelse);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.2.8").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }



        [Fact(DisplayName = "4402.1.46 ArbeidsplasserValidering - Ok")]
        public void EndringAvTillatelse_4402_1_46_ArbeidsplasserValideringOkTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .EndringAvTillatelse(false, false, false, true, null)
                .RammebetingerlserGenerelleVilkaar(null, true, null, false)
                .Build();
            val.ArbeidsplasserValidering(endringAvTillatelse, new List<string>() { "Søknad om Arbeidstilsynets samtykk", "SamtykkeArbeidstilsynet", "Søknad om arbeidstilsynets samtykke" });
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4402.1.46 ArbeidsplasserValidering - Error")]
        public void EndringAvTillatelse_4402_1_46_ArbeidsplasserValideringErrorTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .RammebetingerlserGenerelleVilkaar(null, true, null, false)
                .Build();
            val.ArbeidsplasserValidering(endringAvTillatelse, new List<string>() { "Søknad om Arbeidstilsynets samtykk" });
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.46").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4402.1.30 AdkomstValideringg - ok")]
        public void EndringAvTillatelse_4402_1_30_AdkomstValideringgOkTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .EndringAvTillatelse(false, true, false, false, null)
                .BeskrivelseAvTiltak("nyttbyggboligformal", "Nytt bygg - Boligformål", "Ny fritidsbolig med laftet anneks", "Fritidsbolig", "Fritidsbolig")
                .RammebetingelserAdkomst("RiksFylkesveg", "Riksvei/fylkesvei", true, null, null, true)
                .Build();
            val.MinstEtTiltak(endringAvTillatelse);
            val.AdkomstValidering(endringAvTillatelse, new List<string>() { "TegningNyPlan", "Avkjoerselsplan" });
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4402.1.30 AdkomstValideringg - Error")]
        public void EndringAvTillatelse_4402_1_30_AdkomstValideringgErrorTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .EndringAvTillatelse(false, true, false, false, null)
                .BeskrivelseAvTiltak("nyttbyggboligformal", "Nytt bygg - Boligformål", "Ny fritidsbolig med laftet anneks", "Fritidsbolig", "Fritidsbolig")
                .RammebetingelserAdkomst("1234", null, null, null, null, null)
                .Build();
            val.MinstEtTiltak(endringAvTillatelse);
            val.AdkomstValidering(endringAvTillatelse, new List<string>() { "TegningNyPlan" });
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.30").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4402.1.31 Adkomst Avkjorsels plan Til taker Ny Eller Endre - Ok")]
        public void EndringAvTillatelse_4402_1_31_AdkomstAvkjorselsplanTiltakerNyEllerEndreOkTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .EndringAvTillatelse(false, true, false, false, null)
                .RammebetingelserAdkomst("RiksFylkesveg", "Riksvei/fylkesvei", true, true, null, true)
                .Build();
            val.AdkomstValidering(endringAvTillatelse, new List<string>() { "Avkjoerselsplan" });
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4402.1.31 Adkomst Avkjorsels plan Til taker Ny Eller Endre - Error")]
        public void EndringAvTillatelse_4402_1_31_AdkomstAvkjorselsplanTiltakerNyEllerEndreErrorTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .EndringAvTillatelse(false, true, false, false, null)
                .RammebetingelserAdkomst("RiksFylkesveg", "Riksvei/fylkesvei", true, null, null, true)
                .Build();
            val.AdkomstValidering(endringAvTillatelse, new List<string>() { "" });
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.31").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4402.1.32 Adkomst Riksveg/fylkesveg avkjøringstillatelse - Ok")]
        public void EndringAvTillatelse_4402_1_32_AdkomstValideringOkTest()
        {
            var val = new EndringAvTillatelseFormValidator();
            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .EndringAvTillatelse(false, true, false, false, null)
                .RammebetingelserAdkomst("RiksFylkesveg", "Riksvei/fylkesvei", true, null, null, true)
                .Build();
            val.AdkomstValidering(endringAvTillatelse, new List<string>() { "Avkjoerselsplan" });
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }

        [Fact(DisplayName = "4402.1.32 Adkomst Riksveg/fylkesveg avkjøringstillatelse - Error")]
        public void EndringAvTillatelse_4402_1_32_AdkomstValideringErrorTest()
        {
            var val = new EndringAvTillatelseFormValidator();
            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .EndringAvTillatelse(false, true, false, false, null)
                .RammebetingelserAdkomst("RiksFylkesveg", "Riksvei/fylkesvei", true, null, null, null)
                .Build();
            val.AdkomstValidering(endringAvTillatelse, new List<string>() { "Avkjoerselsplan" });
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.32").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4402.1.33 AdkomstValidering - OK")]
        public void EndringAvTillatelse_4402_2_33_AdkomstValideringOKTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .EndringAvTillatelse(false, true, false, false, null)
                .RammebetingelserAdkomst("KommunalVeg", "KommunalVeg", true, true, true, null)
                .Build();
            val.AdkomstValidering(endringAvTillatelse, new List<string>() { "Avkjoerselsplan" });
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }

        [Fact(DisplayName = "4402.1.33 AdkomstValidering - Error")]
        public void EndringAvTillatelse_4402_1_33_AdkomstValideringErrorTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .EndringAvTillatelse(false, true, false, false, null)
                .RammebetingelserAdkomst("KommunalVeg", "KommunalVeg", true, null, null, null)
                .Build();
            val.AdkomstValidering(endringAvTillatelse, new List<string>() { "Avkjoerselsplan" });
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.33").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4402.1.34 Adkomst tinglyst erklæring fylles - OK")]
        public void EndringAvTillatelse_4402_1_34_AdkomstValideringOKTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .EndringAvTillatelse(false, true, false, false, null)
                .RammebetingelserAdkomst("PrivatVeg", "PrivatVeg", true, true, null, null)
                .Build();
            val.AdkomstValidering(endringAvTillatelse, new List<string>() { "Avkjoerselsplan" });
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }

        [Fact(DisplayName = "4402.1.34 Adkomst tinglyst erklæring fylles - Error")]
        public void EndringAvTillatelse_4402_1_34_AdkomstValideringErrorTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .EndringAvTillatelse(false, true, false, false, null)
                .RammebetingelserAdkomst("PrivatVeg", "PrivatVeg", true, null, null, null)
                .Build();
            val.AdkomstValidering(endringAvTillatelse, new List<string>() { "Avkjoerselsplan" });
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.34").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4402.1.2.9 Adkomst tinglyst erklæring fylles - Error")]
        public void EndringAvTillatelse_4402_1_2_9_AdkomstValideringErrorTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .EndringAvTillatelse(false, true, false, false, null)
                .RammebetingelserAdkomst("PrivatVeg*", "PrivatVeg", true, true, null, null)
                .Build();
            val.AdkomstValidering(endringAvTillatelse, new List<string>() { "Avkjoerselsplan" });
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.2.9").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4402.1.19 ArealdisponeringValidering - Ok ")]
        public void EndringAvTillatelse_4402_1_19_ArealdisponeringValideringgOKTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .RammebetingelserArealdisponering(200.4, 30.5, 10.6, 18.0, 23.36, 1020, 240.5, 1000.5,
                    30.2, 50.3, 300.2)
                .RammebetingelserGjeldendePlan(null, null, "%BYA", "Prosent bebygd areal")
                .Build();
            val.ArealdisponeringValidering(endringAvTillatelse);
            var result = val.GetResult();
            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4402.1.19 ArealdisponeringValidering - Ok ")]
        public void EndringAvTillatelse_4402_1_19_a_ArealdisponeringValideringgOKTest()
        {
            EndringAvAreal();
            _form.rammebetingelser.gjeldendePlan[0].beregningsregelGradAvUtnytting.kodeverdi = "ingenKrav";
            _form.rammebetingelser.arealdisponering = null;

            _validator.ArealdisponeringValidering(_form);

            var result = _validator.GetResult();
            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4402.1.19 ArealdisponeringValidering - Error")]
        public void EndringAvTillatelse_4402_1_19_ArealdisponeringValideringErrorTest()
        {
            EndringAvAreal();
            _form.rammebetingelser.gjeldendePlan[0].beregningsregelGradAvUtnytting.kodeverdi = "%BYA";
            _form.rammebetingelser.gjeldendePlan[0].beregningsregelGradAvUtnytting.kodebeskrivelse = "Prosent bebygd areal";

            _form.rammebetingelser.arealdisponering = null;

            _validator.ArealdisponeringValidering(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4402.1.19").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4402.1.20 ArealdisponeringValidering - OK")]
        public void EndringAvTillatelse_4402_1_20_ArealdisponeringValideringOKTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .RammebetingelserArealdisponering(200.4, 30.5, 10.6, 18.0, 23.36, 1020, 240.5, 1000.5,
                    30.2, 50.3, 300.2)
                .RammebetingelserGjeldendePlan(null, null, "%BYA", "Prosent bebygd areal")

               .Build();
            val.ArealdisponeringValidering(endringAvTillatelse);
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4402.1.21 ArealdisponeringValidering - Ok")]
        public void EndringAvTillatelse_4402_1_21_ArealdisponeringValideringOkTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .RammebetingelserArealdisponering(200.4, 30.5, 10.6, 18.0, 23.36, 1020, 240.5, 1000.5,
                    30.2, 50.3, 300.2)

                .RammebetingelserGjeldendePlan(null, null, "%BYA", "Prosent bebygd areal")

               .Build();
            val.ArealdisponeringValidering(endringAvTillatelse);
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4402.1.21 ArealdisponeringValidering - Error")]
        public void EndringAvTillatelse_4402_1_21_ArealdisponeringValideringErrorTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .RammebetingelserArealdisponering(null, 30.5, 10.6, 18.0, 23.36, 1020, 240.5, 1000.5,
                    30.2, 50.3, 300.2)
                .RammebetingelserGjeldendePlan(null, null, "BYA", "Bebygd area")

               .Build();
            val.ArealdisponeringValidering(endringAvTillatelse);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.21").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4402.1.22 ArealdisponeringValidering - Ok")]
        public void EndringAvTillatelse_4402_1_22_ArealdisponeringValideringOkTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .RammebetingelserArealdisponering(200.4, 30.5, 10.6, 18.0, 238.3, 1020, 240.5, 1000.5,
                    30.2, 50.3, 300.2)
                .RammebetingelserGjeldendePlan(null, null, "BYA", "Bebygd area")
               .Build();
            val.ArealdisponeringValidering(endringAvTillatelse);
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4402.1.22 ArealdisponeringValidering - Error")]
        public void EndringAvTillatelse_4402_1_22_ArealdisponeringValideringErrorTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .RammebetingelserArealdisponering(200.4, 30.5, 10.6, 18.0, 23.37, 1020, 240.5, 1000.5,
                    30.2, 50.3, 300.2)
                .RammebetingelserGjeldendePlan(null, null, "BYA", "Bebygd area")
               .Build();
            val.ArealdisponeringValidering(endringAvTillatelse);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.22").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4402.1.23 ArealdisponeringValidering - Ok")]
        public void EndringAvTillatelse_4402_1_23_ArealdisponeringValideringOkTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .RammebetingelserArealdisponering(200.4, 30.5, 10.6, 18.0, 23.36, 1020, 240.5, 1000.5,
                    30.2, 50.3, 300.2)
                .RammebetingelserGjeldendePlan(null, null, "%BYA", "Prosent bebygd areal")
               .Build();
            val.ArealdisponeringValidering(endringAvTillatelse);
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4402.1.23 ArealdisponeringValidering - Error")]
        public void EndringAvTillatelse_4402_1_23_ArealdisponeringValideringErrorTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .RammebetingelserArealdisponering(null, 30.5, 10.6, 18.0, 23.37, 1020, 240.5, 1000.5,
                    30.2, 50.3, 300.2)
                .RammebetingelserGjeldendePlan(null, null, "%BYA", "Prosent bebygd areal")
               .Build();
            val.ArealdisponeringValidering(endringAvTillatelse);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.23").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4402.1.24 ArealdisponeringValidering - Ok")]
        public void EndringAvTillatelse_4402_1_24_GjeldendePlanValideringOkTest()
        {
            EndringAvAreal();
            _form.rammebetingelser.gjeldendePlan[0].beregningsregelGradAvUtnytting.kodeverdi = "%BYA";
            _form.rammebetingelser.gjeldendePlan[0].beregningsregelGradAvUtnytting.kodebeskrivelse = "Prosent bebygd areal";


            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .RammebetingelserArealdisponering(200.4, 30.5, 10.6, 18.0, 23.36, 1020, 240.5, 1000.5,
                    30.2, 50.3, 300.2)
                .RammebetingelserGjeldendePlan(null, null, "%annet", "Prosent annet")
               .Build();
            _validator.GjeldendePlanValidering(_form, new List<string>() { "UnderlagUtnytting" });
            var result = _validator.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4402.1.24 ArealdisponeringValidering - Error")]
        public void EndringAvTillatelse_4402_1_24_ArealdisponeringValideringErrorTest()
        {
            EndringAvAreal();
            _form.rammebetingelser.gjeldendePlan[0].beregningsregelGradAvUtnytting.kodeverdi = "annetUdef";
            _form.rammebetingelser.gjeldendePlan[0].beregningsregelGradAvUtnytting.kodebeskrivelse = "Annet";
            _form.rammebetingelser.gjeldendePlan[0].navn = "PlanNavn";
            _formSetElements.Remove("UnderlagUtnytting");

            _validator.MinstEtTiltak(_form);
            _validator.GjeldendePlanValidering(_form, _formSetElements);

            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4402.1.24").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4402.1.20 ArealdisponeringValidering - Error")]
        public void EndringAvTillatelse_4402_1_20_ArealdisponeringValideringErrorTest()
        {
            EndringAvAreal();
            string tiltaktype;

            //Ikke krav
            //tiltaktype = "antenneover5m";
            //_form.beskrivelseAvTiltak.type[0].kodeverdi = "antenneover5m";
            //_form.beskrivelseAvTiltak.type[0].kodebeskrivelse = "Antennesystem med høyde over 5m";

            // Bare i 3.1
            //tiltaktype = "parkeringsplass";
            //_form.beskrivelseAvTiltak.type[0].kodeverdi = tiltaktype;
            //_form.beskrivelseAvTiltak.type[0].kodebeskrivelse = "Parkeringsplass";

            //Begge
            tiltaktype = "nyttbyggover70m2";
            _form.beskrivelseAvTiltak.type[0].kodeverdi = "nyttbyggover70m2";
            _form.beskrivelseAvTiltak.type[0].kodebeskrivelse = "Nytt bygg - Over 70 m2 -Ikke boligformål";

            _form.rammebetingelser.gjeldendePlan[0].beregningsregelGradAvUtnytting = null;
            _form.rammebetingelser.gjeldendePlan[0].navn = "pLANnAVN";

            _validator._tiltakstyperISoeknad = new List<string>() { tiltaktype };
            _validator._validationResult.tiltakstyperISoeknad = new List<string>() { tiltaktype };

            _validator.GjeldendePlanValidering(_form, _formSetElements);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "4402.1.20").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4402.1.25 ArealdisponeringValidering - Ok")]
        public void EndringAvTillatelse_4402_1_25_ArealdisponeringValideringOkTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .RammebetingelserArealdisponering(200.4, 30.5, 10.6, 18.0, 23.36, 1020, 240.5, 1000.5,
                    30.2, 50.3, 300.2)
                .RammebetingelserGjeldendePlan(null, null, "%BYA", "Prosent bebygd areal")
               .Build();
            val.ArealdisponeringValidering(endringAvTillatelse);
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4402.1.25.1 ArealdisponeringValidering - error")]
        public void EndringAvTillatelse_4402_1_25_1_ArealdisponeringValideringErrorTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .RammebetingelserArealdisponering(-200.4, 30.5, 10.6, 18.0, 23.36, 1020, 240.5, 1000.5,
                    30.2, 50.3, 300.2)
                .RammebetingelserGjeldendePlan(null, null, "%BYA", "Prosent bebygd areal")
                .Build();
            val.ArealdisponeringValidering(endringAvTillatelse);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.25.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4402.1.25.2 ArealdisponeringValidering - error")]
        public void EndringAvTillatelse_4402_1_25_2_ArealdisponeringValideringErrorTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .RammebetingelserArealdisponering(200.4, -30.5, 10.6, 18.0, 23.36, 1020, 240.5, 1000.5,
                    30.2, 50.3, 300.2)
                .RammebetingelserGjeldendePlan(null, null, "%BYA", "Prosent bebygd areal")
                .Build();
            val.ArealdisponeringValidering(endringAvTillatelse);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.25.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4402.1.25.3 ArealdisponeringValidering - error")]
        public void EndringAvTillatelse_4402_1_25_3_ArealdisponeringValideringErrorTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .RammebetingelserArealdisponering(200.4, 30.5, -10.6, 18.0, 23.36, 1020, 240.5, 1000.5,
                    30.2, 50.3, 300.2)
                .RammebetingelserGjeldendePlan(null, null, "%BYA", "Prosent bebygd areal")
                .Build();
            val.ArealdisponeringValidering(endringAvTillatelse);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.25.3").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4402.1.25.4 ArealdisponeringValidering - Error")]
        public void EndringAvTillatelse_4402_1_25_4_ArealdisponeringValideringErrorTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .RammebetingelserArealdisponering(200.4, 30.5, 10.6, -18.0, 23.36, 1020, 240.5, 1000.5,
                    30.2, 50.3, 300.2)
                .RammebetingelserGjeldendePlan(null, null, "%BYA", "Prosent bebygd areal")
               .Build();
            val.ArealdisponeringValidering(endringAvTillatelse);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.25.4").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4402.1.25.5 ArealdisponeringValidering - Error")]
        public void EndringAvTillatelse_4402_1_25_5_ArealdisponeringValideringErrorTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .RammebetingelserArealdisponering(200.4, 30.5, 10.6, 18.0, -23.36, 1020, 240.5, 1000.5,
                    30.2, 50.3, 300.2)
                .RammebetingelserGjeldendePlan(null, null, "%BYA", "Prosent bebygd areal")
               .Build();
            val.ArealdisponeringValidering(endringAvTillatelse);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.25.5").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4402.1.25.6 ArealdisponeringValidering - Error")]
        public void EndringAvTillatelse_4402_1_25_6_ArealdisponeringValideringErrorTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .RammebetingelserArealdisponering(200.4, 30.5, 10.6, 18.0, 23.36, -1020, 240.5, 1000.5,
                    30.2, 50.3, 300.2)
                .RammebetingelserGjeldendePlan(null, null, "%BYA", "Prosent bebygd areal")
               .Build();
            val.ArealdisponeringValidering(endringAvTillatelse);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.25.6").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4402.1.26 ArealdisponeringValidering - Ok")]
        public void EndringAvTillatelse_4402_1_26_ArealdisponeringValideringOkTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .RammebetingelserArealdisponering(200.4, 30.5, 10.6, 18.0, 23.36, 1020, 240.5, 1000.5,
                    30.2, 50.3, 300.2)
                .RammebetingelserGjeldendePlan(null, null, "%BYA", "Prosent bebygd areal")
               .Build();
            val.ArealdisponeringValidering(endringAvTillatelse);
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4402.1.26 ArealdisponeringValidering - Error")]
        public void EndringAvTillatelse_4402_1_26_ArealdisponeringValideringErrorTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .RammebetingelserArealdisponering(200.4, 30.5, 10.6, 18.0, 23.36, 0, 240.5, 1000.5,
                    30.2, 50.3, 300.2)
                .RammebetingelserGjeldendePlan(null, null, "%BYA", "Prosent bebygd areal")
               .Build();
            val.ArealdisponeringValidering(endringAvTillatelse);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.26").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4402.1.27 ArealdisponeringValidering - Ok")]
        public void EndringAvTillatelse_4402_1_27_ArealdisponeringValideringOkTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .RammebetingelserArealdisponering(200.4, 30.5, 10.6, 18.0, 23.36, 1020, 240.5, 1000.5,
                    30.2, 50.3, 300.2)
                .RammebetingelserGjeldendePlan(null, null, "%BYA", "Prosent bebygd areal")
                .Build();
            val.ArealdisponeringValidering(endringAvTillatelse);
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4402.1.27 ArealdisponeringValidering - Error")]
        public void EndringAvTillatelse_4402_1_27_ArealdisponeringValideringErrorTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .RammebetingelserArealdisponering(200.4, 30.5, 10.6, 18.0, 23, 1020, 240.5, 1000.5,
                    30.2, 50.3, 300.2)
                .RammebetingelserGjeldendePlan(null, null, "%BYA", "Prosent bebygd areal")
                .Build();
            val.ArealdisponeringValidering(endringAvTillatelse);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.27").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4402.1.2.13 ArealdisponeringValidering - Ok ")]
        public void EndringAvTillatelse_4402_1_2_13_ArealdisponeringValideringgOKTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .RammebetingelserArealdisponering(200.4, 30.5, 10.6, 18.0, 23.36, 1020, 240.5, 1000.5,
                    30.2, 50.3, 300.2)
                .RammebetingelserGjeldendePlan(null, null, "*%BYA", "Prosent bebygd areal")
                .Build();
            val.ArealdisponeringValidering(endringAvTillatelse);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.2.13").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4402.1.66 ArealdisponeringValidering - Ok ")]
        public void EndringAvTillatelse_4402_1_66_ArealdisponeringValideringgOKTest()
        {


            var val = new EndringAvTillatelseFormValidator();
            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .RammebetingelserArealdisponering(200.4, 30.5, 10.6, 18.0, 23.36, 1020, 240.5, 1000.5,
                    30.2, 50.3, 300.2)
                .RammebetingelserGjeldendePlan(null, null, "%TU", "Tomteutnyttelse")
                .Build();
            val.ArealdisponeringValidering(endringAvTillatelse);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "", 0);

        }
        [Fact(DisplayName = "4402.1.66 ArealdisponeringValidering - ERROR ", Skip = "There is no invalid status code")]
        public void EndringAvTillatelse_4402_1_66_ArealdisponeringValideringgERRORTest()
        {


            var val = new EndringAvTillatelseFormValidator();
            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .RammebetingelserArealdisponering(200.4, 30.5, 10.6, 18.0, 23.36, 1020, 240.5, 1000.5,
                    30.2, 50.3, 300.2)
                .RammebetingelserGjeldendePlan(null, null, "TU", "Tomteutnyttelse")
                .Build();
            val.ArealdisponeringValidering(endringAvTillatelse);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "4402.1.66", 1);

        }
        [Fact(DisplayName = "4402.1.1.3 ArealdisponeringValidering - Ok ")]
        public void EndringAvTillatelse_4402_1_1_3_ArealdisponeringValideringgOKTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .RammebetingelserArealdisponering(200.4, 30.5, 10.6, 18.0, 23.36, 1020, 240.5, 1000.5,
                    30.2, 50.3, 300.2)
                .RammebetingelserGjeldendePlan(null, null, "%BYA", null)
                .Build();
            val.ArealdisponeringValidering(endringAvTillatelse);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "4402.1.1.3", 1);
        }

        [Fact(DisplayName = "4402.1.45 MatrikkelopplysningValidering - Ok")]
        public void EndringAvTillatelse_4402_1_45_MatrikkelopplysningValideringOkTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .EndringAvTillatelse(true, true, false, false)
                .EndringAvTillatelseRammerType()
                .Build();
            val.MatrikkelopplysningValidering(endringAvTillatelse, new List<string>() { "Matrikkelopplysninger" });
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4402.1.45 MatrikkelopplysningValidering - Error")]
        public void EndringAvTillatelse_4402_1_45_MatrikkelopplysningValideringErrorTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .EndringAvTillatelseRammerType()
                .Build();
            val.MatrikkelopplysningValidering(endringAvTillatelse, new List<string>() { "Avkjoerselsplan" });
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.45").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }


        [Fact(DisplayName = "4402.1.36 KravTilByggegrunnValidering - OK")]
        public void EndringAvTillatelse_4402_1_36_KravTilByggegrunnValideringOkTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .EndringAvTillatelse(false, true, false, false)
                .RammebetingelserKravTilByggegrunn(true, false, false, true, false, false, true, false, false)
                .BeskrivelseAvTiltak("nyttbyggboligformal", "Nytt bygg - Boligformål", "Ny fritidsbolig med laftet anneks", "Fritidsbolig", "Fritidsbolig")
                .Build();
            val.MinstEtTiltak(endringAvTillatelse);
            val.KravTilByggegrunnValidering(endringAvTillatelse, new List<string>() { "byggegrunn", "RedegjoerelseSkredOgFlom" });
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4402.1.36 KravTilByggegrunnValidering - Error")]
        public void EndringAvTillatelse_4402_1_36_KravTilByggegrunnValideringErrorTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .EndringAvTillatelse(false, true, false, false)
                .RammebetingelserKravTilByggegrunn(null, false, false, true, false, false, true, false, false)
                .EndringAvTillatelseRammerType()
                .BeskrivelseAvTiltak("nyttbyggdriftsbygningover1000m2", "Nytt bygg - Driftsbygning i landbruket med samlet areal over 1000 m2", "Ny fritidsbolig med laftet anneks", "Fritidsbolig", "Fritidsbolig")
                .Build();
            val.MinstEtTiltak(endringAvTillatelse);

            val.KravTilByggegrunnValidering(endringAvTillatelse, new List<string>() { "byggegrunn" });
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.36").Should().NotBeNullOrEmpty();

        }
        [Fact(DisplayName = "4402.1.37 KravTilByggegrunnValidering - Ok")]
        public void EndringAvTillatelse_4402_1_37_KravTilByggegrunnValideringOkTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .EndringAvTillatelse(false, true, false, false)
                .RammebetingelserKravTilByggegrunn(true, true, false, true, false, false, true, false, false)
                .Build();
            val.KravTilByggegrunnValidering(endringAvTillatelse, new List<string>() { "RedegjoerelseSkredOgFlom" });
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4402.1.37 KravTilByggegrunnValidering - Error")]
        public void EndringAvTillatelse_4402_1_37_KravTilByggegrunnValideringErrorTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .EndringAvTillatelse(false, true, false, false)
                .RammebetingelserKravTilByggegrunn(true, false, false, true, false, false, true, false, false)
                .BeskrivelseAvTiltak("nyttbyggdriftsbygningover1000m2", "Nytt bygg - Driftsbygning i landbruket med samlet areal over 1000 m2", "Ny fritidsbolig med laftet anneks", "Fritidsbolig", "Fritidsbolig")
                .Build();
            val.MinstEtTiltak(endringAvTillatelse);

            val.KravTilByggegrunnValidering(endringAvTillatelse, new List<string>() { "byggegrunn" });
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.37").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4402.1.38 KravTilByggegrunnValidering - Ok")]
        public void EndringAvTillatelse_4402_1_38_KravTilByggegrunnValideringOkTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .EndringAvTillatelse(false, true, false, false)
                .RammebetingelserKravTilByggegrunn(true, true, true, true, false, false, true, false, false)
                .Build();
            val.KravTilByggegrunnValidering(endringAvTillatelse, new List<string>() { "RedegjoerelseSkredOgFlom", "RedegjoerelseAndreNaturMiljoeforhold" });
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4402.1.38 KravTilByggegrunnValidering - Error")]
        public void EndringAvTillatelse_4402_1_38_KravTilByggegrunnValideringErrorTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .EndringAvTillatelse(false, true, false, false)
                .RammebetingelserKravTilByggegrunn(true, true, true, true, false, false, true, false, false)
                .Build();
            val.KravTilByggegrunnValidering(endringAvTillatelse, new List<string>() { "RedegjoerelseSkredOgFlom" });
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.38.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4402.1.1.1 KravTilByggegrunnValidering - Ok")]
        public void EndringAvTillatelse_4402_1_1_1_KravTilByggegrunnValideringOkTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .EndringAvTillatelse(false, true, false, false)
                .RammebetingelserKravTilByggegrunn(true, true, false, null, null, null, true, false, false)
                .BeskrivelseAvTiltak("nyttbyggdriftsbygningover1000m2", "Nytt bygg - Driftsbygning i landbruket med samlet areal over 1000 m2", "Ny fritidsbolig med laftet anneks", "Fritidsbolig", "Fritidsbolig")
                .Build();
            val.MinstEtTiltak(endringAvTillatelse);
            val.KravTilByggegrunnValidering(endringAvTillatelse, new List<string>() { "RedegjoerelseSkredOgFlom" });
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.1.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4402.1.1.2 KravTilByggegrunnValidering - Ok")]
        public void EndringAvTillatelse_4402_1_1_2_KravTilByggegrunnValideringOkTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .EndringAvTillatelse(false, true, false, false)
                .RammebetingelserKravTilByggegrunn(true, true, false, true, false, false, null, false, false)
                .BeskrivelseAvTiltak("nyttbyggdriftsbygningover1000m2", "Nytt bygg - Driftsbygning i landbruket med samlet areal over 1000 m2", "Ny fritidsbolig med laftet anneks", "Fritidsbolig", "Fritidsbolig")
                .Build();
            val.MinstEtTiltak(endringAvTillatelse);

            val.KravTilByggegrunnValidering(endringAvTillatelse, new List<string>() { "RedegjoerelseSkredOgFlom" });
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.1.2.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4402.1.1.2.2 KravTilByggegrunnValidering - Ok")]
        public void form_1_1_2_2_KravTilByggegrunnValideringOkTest()
        {
            EndringAvPlasering();
            _form.rammebetingelser.kravTilByggegrunn.miljoeforholdSpecified = true;
            _form.rammebetingelser.kravTilByggegrunn.miljoeforhold = false;
            _form.rammebetingelser.kravTilByggegrunn.skredutsattOmraadeSpecified = true;
            _form.rammebetingelser.kravTilByggegrunn.skredutsattOmraade = true;

            _form.rammebetingelser.kravTilByggegrunn.s1Specified = true;
            _form.rammebetingelser.kravTilByggegrunn.s1 = true;

            var postmanXML = TestHelper.GetValidateFormv2FormSkjema(_formSkjema, _formSetElements, _form);

            _formSetElements.Remove("RedegjoerelseSkredOgFlom");

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .EndringAvTillatelse(false, true, false, false)
                .RammebetingelserKravTilByggegrunn(true, true, false, true, false, false, null, false, false)
                .BeskrivelseAvTiltak("nyttbyggdriftsbygningover1000m2", "Nytt bygg - Driftsbygning i landbruket med samlet areal over 1000 m2", "Ny fritidsbolig med laftet anneks", "Fritidsbolig", "Fritidsbolig")
                .Build();

            _validator.MinstEtTiltak(_form);
            var postmanXML1 = TestHelper.GetValidateFormv2FormSkjema(_formSkjema, _formSetElements, _form);

            _validator.KravTilByggegrunnValidering(_form, _formSetElements);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4402.1.1.2.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }


        [Fact(DisplayName = "4402.1.40 PlasseringValidering - Ok")]
        public void EndringAvTillatelse_4402_1_40_PlasseringValideringOkTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .EndringAvTillatelse(false, true, false, false)
                .BeskrivelseAvTiltak("nyttbyggdriftsbygningover1000m2", "Nytt bygg - Driftsbygning i landbruket med samlet areal over 1000 m2", "Ny fritidsbolig med laftet anneks", "Fritidsbolig", "Fritidsbolig")
                .rammebetingelserPlassering(false, true)
                .Build();
            val.PlasseringValidering(endringAvTillatelse, new List<string>() { "AvklaringVA" });
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }

        [Fact(DisplayName = "4402.1.40 PlasseringValidering - Error")]
        public void EndringAvTillatelse_4402_1_40_PlasseringValideringErrorTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .EndringAvTillatelse(false, true, false, false)
                .BeskrivelseAvTiltak("nyttbyggdriftsbygningover1000m2", "Nytt bygg - Driftsbygning i landbruket med samlet areal over 1000 m2", "Ny fritidsbolig med laftet anneks", "Fritidsbolig", "Fritidsbolig")
                .rammebetingelserPlassering(false, null)
                .Build();
            val.MinstEtTiltak(endringAvTillatelse);
            val.PlasseringValidering(endringAvTillatelse, new List<string>() { "RedegjoerelseSkredOgFlom" });
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.40").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4402.1.41 PlasseringValidering - Ok")]
        public void EndringAvTillatelse_4402_1_41_PlasseringValideringOkTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .EndringAvTillatelse(false, true, false, false)
                .BeskrivelseAvTiltak("nyttbyggdriftsbygningover1000m2", "Nytt bygg - Driftsbygning i landbruket med samlet areal over 1000 m2", "Ny fritidsbolig med laftet anneks", "Fritidsbolig", "Fritidsbolig")
                .rammebetingelserPlassering(null, false)
                .Build();
            val.MinstEtTiltak(endringAvTillatelse);
            val.PlasseringValidering(endringAvTillatelse, new List<string>() { "AvklaringHoyspent" });
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.41").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4402.1.41 PlasseringValidering - Error")]
        public void EndringAvTillatelse_4402_1_41_PlasseringValideringErrorTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .EndringAvTillatelse(false, true, false, false)
                .BeskrivelseAvTiltak("nyttbyggdriftsbygningover1000m2", "Nytt bygg - Driftsbygning i landbruket med samlet areal over 1000 m2", "Ny fritidsbolig med laftet anneks", "Fritidsbolig", "Fritidsbolig")
                .rammebetingelserPlassering(null, false)
                .Build();
            val.MinstEtTiltak(endringAvTillatelse);
            val.PlasseringValidering(endringAvTillatelse, new List<string>() { "" });
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.41").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4402.1.42 PlasseringValidering - Ok")]
        public void EndringAvTillatelse_4402_1_42_PlasseringValideringOkTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .EndringAvTillatelse(false, true, false, false)
                .rammebetingelserPlassering(true, false)
                .Build();
            val.PlasseringValidering(endringAvTillatelse, new List<string>() { "AvklaringHoyspent" });
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4402.1.42 PlasseringValidering - Error")]
        public void EndringAvTillatelse_4402_1_42_PlasseringValideringErrorTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .EndringAvTillatelse(false, true, false, false)
                .rammebetingelserPlassering(true, false)
                .Build();
            val.PlasseringValidering(endringAvTillatelse, new List<string>() { "" });
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.42").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4402.1.43 PlasseringValidering - Ok")]
        public void EndringAvTillatelse_4402_1_43_PlasseringValideringOkTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .EndringAvTillatelse(false, true, false, false)
                .rammebetingelserPlassering(false, true)
                .Build();
            val.PlasseringValidering(endringAvTillatelse, new List<string>() { "AvklaringVA" });
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4402.1.43 PlasseringValidering - Error")]
        public void EndringAvTillatelse_4402_1_43_PlasseringValideringErrorTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .EndringAvTillatelse(false, true, false, false)
                .rammebetingelserPlassering(false, true)
                .Build();
            val.PlasseringValidering(endringAvTillatelse, new List<string>() { "" });
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.43").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4402.1.44 PlasseringValidering - Error")]
        public void EndringAvTillatelse_4402_1_44_PlasseringValideringErrorTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .Rammebetingelser()
                .Build();
            val.PlasseringValidering(endringAvTillatelse, new List<string>() { "AvklaringVA" });
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.44").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4402.1.30 AdkomstValideringg formål - Error")]
        public void EndringAvTillatelse_4402_1_30_AdkomstValideringErrorTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .EndringAvTillatelse(false, false, true, false)
                .BeskrivelseAvTiltak("nyttbyggboligformal", "Nytt bygg - Boligformål", "Ny fritidsbolig med laftet anneks", "Fritidsbolig", "Fritidsbolig")
                .RammebetingelserAdkomst("1234", null, null, null, null, null)
                .Build();
            val.MinstEtTiltak(endringAvTillatelse);
            val.AdkomstValidering(endringAvTillatelse, new List<string>() { "TegningNyPlan" });
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.30").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4402.2.35 MinstEtTiltak beskrivPlanlagtFormaal - OK")]
        public void EndringAvTillatelse_4402_2_35_MinstEtTiltakBeskrivPlanlagtFormaalOKTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .BeskrivelseAvTiltak("nyttbyggboligformal", "Nytt bygg - Boligformål", "Ny fritidsbolig med laftet anneks", "Fritidsbolig", "Fritidsbolig")
                .Build();
            val.MinstEtTiltak(endringAvTillatelse);
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4402.1.35 MinstEtTiltak beskrivPlanlagtFormaal - Error")]
        public void EndringAvTillatelse_4402_1_35_MinstEtTiltakBeskrivPlanlagtFormaalErrorTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .BeskrivelseAvTiltak("nyttbyggboligformal", "Nytt bygg - Boligformål", null, "Annet")
                .Build();
            val.MinstEtTiltak(endringAvTillatelse);
            val.MatrikkelInformasjonValidering(endringAvTillatelse);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.35").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4402.1.39 MinstEtTiltak beskrivPlanlagtFormaal - Error")]
        public void EndringAvTillatelse_4402_1_39_MatrikkelInformasjonValideringErrorTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .BeskrivelseAvTiltak("nyttbyggboligformal", "Nytt bygg - Boligformål", null, "Fritidsbolig", "Fritidsbolig", "161", "Hytter, sommerhus og fritidsbygg", "y", "Næringsgruppe for annet som ikke er næring")
                .Build();
            val.MinstEtTiltak(endringAvTillatelse);
            val.MatrikkelInformasjonValidering(endringAvTillatelse);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.39").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4402.1.53 SoekerValidation Tiltakshaver- OK")]
        public void EndringAvTillatelse_4402_1_53_SoekerValidationOKTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                //.Tiltakshaver("974760673",null, "Foretak", "Foretak","Benjamin Fjell", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "no", "Bø i Telemark", "11223344", "99887766")
                .Tiltakshaver(null, "25125401530", "Privatperson", "Privatperson", null, "Hans Hansen", "Storgata 5", "7003", "no", "Bø i Telemark", "11223344", "99887766")
                //.AnsvarligSoeker("974760673", null, "Foretak", "Foretak","Benjamin Fjell", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "no", "Bø i Telemark", "11223344", "99887766")
                //.AnsvarligSoeker(null, "25125401530", "Privatperson",, "Privatperson"null, "Hans Hansen", "Storgata 5", "7003", "no", "Bø i Telemark", "11223344", "99887766")
                .Build();
            val.SoekerValidation(endringAvTillatelse);
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4402.1.53 SoekerValidation Tiltakshaver- Error")]
        public void EndringAvTillatelse_4402_1_53_SoekerValidationErrorTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .Tiltakshaver(null, "*25125401530", "Privatperson", "Privatperson", null, "Hans Hansen", "Storgata 5", "7003", "no", "Bø i Telemark", "11223344", "99887766")
                .Build();
            val.SoekerValidation(endringAvTillatelse);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.53").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4402.1.61 SoekerValidation - OK")]
        public void EndringAvTillatelse_4402_1_61_SoekerValidationOKTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                //.Tiltakshaver("974760673",null, "Foretak", "Foretak","Benjamin Fjell", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "no", "Bø i Telemark", "11223344", "99887766")
                .Tiltakshaver(null, "25125401530", "Privatperson", "Privatperson", null, "Hans Hansen", "Storgata 5", "7003", "no", "Bø i Telemark", "11223344", "99887766")
                .AnsvarligSoeker("974760673", null, "Foretak", "Foretak", "Benjamin Fjell", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "no", "Bø i Telemark", "11223344", "99887766")
                //.AnsvarligSoeker(null, "25125401530", "Privatperson",null, "Hans Hansen", "Storgata 5", "7003", "no", "Bø i Telemark", "11223344", "99887766")
                .Build();
            val.SoekerValidation(endringAvTillatelse);
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4402.1.61 SoekerValidation - Error")]
        public void EndringAvTillatelse_4402_1_61_SoekerValidationErrorTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .Tiltakshaver(null, "25125401530", null, null, "Hans Hansen", "Storgata 5", "7003", "no", "Bø i Telemark", "11223344", "99887766")
                .AnsvarligSoeker("974760673", null, null, "Benjamin Fjell", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "no", "Bø i Telemark", "11223344", "99887766")
                .Build();
            val.SoekerValidation(endringAvTillatelse);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.61").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4402.1.2.12 SoekerValidation Tiltakshaver- Error")]
        public void EndringAvTillatelse_4402_1_2_12_SoekerValidationErrorTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .Tiltakshaver(null, "25125401530", "*Privatperson", null, "Hans Hansen", "Storgata 5", "7003", "no", "Bø i Telemark", "11223344", "99887766")
                .Build();
            val.SoekerValidation(endringAvTillatelse);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.2.12").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4402.1.2.14 SoekerValidation AnsvarligSoeker- Error")]
        public void EndringAvTillatelse_4402_1_2_14_SoekerValidationErrorTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .AnsvarligSoeker("974760673", null, "Foretakdd", "Foretak", "Benjamin Fjell", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "no", "Bø i Telemark", "11223344", "99887766").Build();
            val.SoekerValidation(endringAvTillatelse);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.2.14").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }



        [Fact(DisplayName = "4402.1.54 SoekerValidation Tiltakshaver- Error")]
        public void EndringAvTillatelse_4402_1_54_SoekerValidationErrorTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .Tiltakshaver("*974760673", null, "Foretak", "Foretak", "Benjamin Fjell", "Siv. Ark. Fjell og Sønner",
                    "Lillegata 5", "7003", "no", "Bø i Telemark", "11223344", "99887766")
                .Build();
            val.SoekerValidation(endringAvTillatelse);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.54").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4402.1.62 SoekerValidation AnsvarligSoeker- Error")]
        public void EndringAvTillatelse_4402_1_62_SoekerValidationErrorTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .AnsvarligSoeker("*974760673", null, "Foretak", "Foretak", "Benjamin Fjell", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "no", "Bø i Telemark", "11223344", "99887766").Build();
            val.SoekerValidation(endringAvTillatelse);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.62").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4402.1.55 SoekerValidation Tiltakshaver- Error")]
        public void EndringAvTillatelse_4402_1_55_SoekerValidationErrorTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .Tiltakshaver("974760673", null, "Foretak", "Foretak", null, "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "no", "Bø i Telemark", "11223344", "99887766")
                .Build();
            val.SoekerValidation(endringAvTillatelse);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.55").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4402.1.63 SoekerValidation AnsvarligSoeker - Error")]
        public void EndringAvTillatelse_4402_1_63_SoekerValidationErrorTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .AnsvarligSoeker("974760673", null, "Foretak", "Foretak", null, "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "no", "Bø i Telemark", "11223344", "99887766").Build();
            val.SoekerValidation(endringAvTillatelse);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.63").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4402.1.56 SoekerValidation Tiltakshaver- Error")]
        public void EndringAvTillatelse_4402_1_56_SoekerValidationErrorTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .Tiltakshaver(null, "25125401530", "Privatperson", "Privatperson", null, "Hans Hansen", "Storgata 5", "7003", "no",
                    null, "11223344", "99887766")
                .Build();
            val.SoekerValidation(endringAvTillatelse);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.56").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4402.1.64 SoekerValidation Tiltakshaver- Error")]
        public void EndringAvTillatelse_4402_1_64_SoekerValidationErrorTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .AnsvarligSoeker("974760673", null, "Foretak", "Foretak", "Benjamin Fjell", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "no", null, "11223344", "99887766").Build();
            val.SoekerValidation(endringAvTillatelse);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.64").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4402.1.57 SoekerValidation Tiltakshaver- Error")]
        public void EndringAvTillatelse_4402_1_57_SoekerValidationErrorTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .Tiltakshaver("974760673", null, "Foretak", "Foretak", "Benjamin Fjell", "Siv. Ark. Fjell og Sønner", "Lillegata 5", null, "no", "Bø i Telemark", "11223344", "99887766").Build();
            val.SoekerValidation(endringAvTillatelse);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.57").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4402.1.52 SoekerValidation AnsvarligSoeker- Error")]
        public void EndringAvTillatelse_4402_1_52_SoekerValidationErrorTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .AnsvarligSoeker(null, "25125401530", "Privatperson", "Privatperson", null, "Hans Hansen", "Storgata 5", "*7003", "no",
                    "Bø i Telemark", "11223344", "99887766")
                .Build();
            val.SoekerValidation(endringAvTillatelse);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.52").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4402.1.60 SoekerValidation Tiltakshaver- Error")]
        public void EndringAvTillatelse_4402_1_60_SoekerValidationErrorTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .Tiltakshaver("974760673", null, "Foretak", "Foretak", "Benjamin Fjell", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "3800", "no", "Bø i Telemark", null, null)
                .Build();
            val.SoekerValidation(endringAvTillatelse);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.60").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4402.1.59 SoekerValidation AnsvarligSoeker- Error")]
        public void EndringAvTillatelse_4402_1_59_SoekerValidationErrorTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .AnsvarligSoeker(null, "25125401530", "Privatperson", "Privatperson", null, "Hans Hansen", "Storgata 5", "7003", "no", "Bø i Telemark", null, null)
                .Build();
            val.SoekerValidation(endringAvTillatelse);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.59").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4402.1.47 GjeldendePlanValidering - OK")]
        public void EndringAvTillatelse_4402_1_47_GjeldendePlanValideringOKTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .BeskrivelseAvTiltak("nyttbyggboligformal", "Nytt bygg - Boligformål", "Ny fritidsbolig med laftet anneks", "Fritidsbolig", "Fritidsbolig")
                .RammebetingelserGjeldendePlan("KP", "Arealdel av kommuneplan", "%BYA", "Porsent bebygs areal", "gjeldendePlan.navn")
                .Build();
            val.MinstEtTiltak(endringAvTillatelse);
            val.GjeldendePlanValidering(endringAvTillatelse, _formSetElements);
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4402.1.47 GjeldendePlanValidering - Error")]
        public void EndringAvTillatelse_4402_1_47_GjeldendePlanValideringErrorTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .BeskrivelseAvTiltak("nyttbyggboligformal", "Nytt bygg - Boligformål", "Ny fritidsbolig med laftet anneks", "Fritidsbolig", "Fritidsbolig")
                .Build();
            val.MinstEtTiltak(endringAvTillatelse);
            val.GjeldendePlanValidering(endringAvTillatelse, _formSetElements);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.47").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4402.1.2.10 GjeldendePlanValidering - Error")]
        public void EndringAvTillatelse_4402_1_2_10_GjeldendePlanValideringErrorTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .BeskrivelseAvTiltak("nyttbyggboligformal", "Nytt bygg - Boligformål", "Ny fritidsbolig med laftet anneks", "Fritidsbolig", "Fritidsbolig")
                .RammebetingelserGjeldendePlan("*KP", "Arealdel av kommuneplan", null, null, "gjeldendePlan.navn")
                .Build();
            val.MinstEtTiltak(endringAvTillatelse);
            val.GjeldendePlanValidering(endringAvTillatelse, _formSetElements);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.2.10").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4402.1.2.11 GjeldendePlanValidering - Error")]
        public void EndringAvTillatelse_4402_1_2_11_GjeldendePlanValideringErrorTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .BeskrivelseAvTiltak("nyttbyggboligformal", "Nytt bygg - Boligformål", "Ny fritidsbolig med laftet anneks", "Fritidsbolig", "Fritidsbolig")
                .RammebetingelserGjeldendePlan("KP", null, null, null, "gjeldendePlan.navn")
                .Build();
            val.MinstEtTiltak(endringAvTillatelse);
            val.GjeldendePlanValidering(endringAvTillatelse, _formSetElements);
            var result = val.GetResult();

            result.messages.Where(m => m.reference == "4402.1.2.11").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4402.1.2.17 GjeldendePlanValidering - Error")]
        public void EndringAvTillatelse_4402_1_2_17_GjeldendePlanValideringErrorTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .EndringAvTillatelse(false, false, false, true, false)
                .BeskrivelseAvTiltak("nyttbyggboligformal", "Nytt bygg - Boligformål", "Ny fritidsbolig med laftet anneks", "Fritidsbolig", "Fritidsbolig")
                .RammebetingelserGjeldendePlan("KP", "Arealdel av kommuneplan", "%BYA", "Porsent bebygs areal", null)
                .Build();
            val.MinstEtTiltak(endringAvTillatelse);
            val.GjeldendePlanValidering(endringAvTillatelse, _formSetElements);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "4402.1.2.17", 1);
        }
        [Fact(DisplayName = "4402.1.67 KommunensSaksnummerValidering - OK")]
        public void EndringAvTillatelse_4402_1_67_KommunensSaksnummerValideringOkTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .kommunensSaksnummer("sakssekvensnummer", "saksaar")
                .Build();
            val.MinstEtTiltak(endringAvTillatelse);
            val.GjeldendePlanValidering(endringAvTillatelse, _formSetElements);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "", null);
        }
        [Fact(DisplayName = "4402.1.67 KommunensSaksnummerValidering - Error")]
        public void EndringAvTillatelse_4402_1_67_KommunensSaksnummerValideringERRORTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .Build();
            val.KommunensSaksnummerValidering(endringAvTillatelse);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "4402.1.67", 1);
        }
        [Fact(DisplayName = "4402.1.68 KommunensSaksnummerValidering - Error")]
        public void EndringAvTillatelse_4402_1_68_KommunensSaksnummerValideringERRORTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .kommunensSaksnummer("sakssekvensnummer")
                .Build();
            val.KommunensSaksnummerValidering(endringAvTillatelse);
            var result = val.GetResult();

            TestHelper.AssertResult(result, "4402.1.68", 1);
        }
        [Fact(DisplayName = "4402.1.69 KommunensSaksnummerValidering - Error")]
        public void EndringAvTillatelse_4402_1_69_KommunensSaksnummerValideringERRORTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .kommunensSaksnummer(null, "saksaar")
                .Build();
            val.KommunensSaksnummerValidering(endringAvTillatelse);
            var result = val.GetResult();

            var rulesChecked = TestHelper.ConverRulesCheckedJson(result);
            TestHelper.AssertResult(result, "4402.1.69", 1);
        }
        [Fact(DisplayName = "4402.1.70 VedleggValidering - OK")]
        public void EndringAvTillatelse_4402_1_70_VedleggValideringOKTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .EndringAvTillatelse(true, null, null, null)
                .AnsvarligSoeker("974760673", null, "Foretak", "Foretak", "Benjamin Fjell", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "no", "Bø i Telemark", "11223344", "99887766")
                .Build();
            val.VedleggValidering(endringAvTillatelse, new List<string>() { "TegningNyFasade", "TegningNyPlan", "TegningNyttSnitt", "Situasjonsplan", "*Gjennomføringsplan", "Gjennomfoeringsplan", "*GjennomføringsplanV4" });
            var result = val.GetResult();

            TestHelper.AssertResult(result, "", 0);
        }
        [Fact(DisplayName = "4402.1.70 VedleggValidering - Error")]
        public void EndringAvTillatelse_4402_1_70_VedleggValideringERRORTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .EndringAvTillatelse(true, null, null, null)
                .AnsvarligSoeker("974760673", null, "Foretak", "Foretak", "Benjamin Fjell", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "no", "Bø i Telemark", "11223344", "99887766")
                .Build();
            val.VedleggValidering(endringAvTillatelse, new List<string>() { "TegningNyFasade", "TegningNyPlan", "TegningNyttSnitt", "Situasjonsplan", "*Gjennomføringsplan", "*Gjennomfoeringsplan", "*GjennomføringsplanV4" });
            var result = val.GetResult();

            TestHelper.AssertResult(result, "4402.1.70", 1);
        }
        [Fact(DisplayName = "4402.1.70.1 VedleggValidering - Error")]
        public void EndringAvTillatelse_4402_1_70_1_VedleggValideringERRORTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .EndringAvTillatelse(true, null, null, null)
                .Build();
            val.VedleggValidering(endringAvTillatelse, new List<string>() { "TegningNyFasade", "TegningNyPlan", "TegningNyttSnitt", "Situasjonsplan", "AnsvarsrettSelvbygger", "*Gjennomføringsplan", "*Gjennomfoeringsplan", "*GjennomføringsplanV4" });
            var result = val.GetResult();

            TestHelper.AssertResult(result, "4402.1.70.1", 1);
        }
        [Fact(DisplayName = "4402.1.70.2 VedleggValidering - Error")]
        public void EndringAvTillatelse_4402_1_70_2_VedleggValideringERRORTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .AnsvarligSoeker(null, "170995", "Privatperson", "Privatperson", null, "Ola Nordmann", "Olavsgata 7", "3803", "no", "Midt-Telemark", "44556677", "99337744")
                //.Tiltakshaver(null, "170999", "Privatperson", "Privatperson", null, "Ola Nordmann", "Olavsgata 7", "3803", "no", "Midt-Telemark", "44556677", "99337744")
                .Build();
            val.VedleggValidering(endringAvTillatelse, new List<string>() { "TegningNyFasade", "TegningNyPlan", "TegningNyttSnitt", "Situasjonsplan", "*AnsvarsrettSelvbygger", "Gjennomføringsplan", "Gjennomfoeringsplan", "GjennomføringsplanV4" });
            var result = val.GetResult();

            TestHelper.AssertResult(result, "4402.1.70.2", 1);
        }
        [Fact(DisplayName = "4402.1.71 VedleggValidering - Error")]
        public void EndringAvTillatelse_4402_1_71_VedleggValideringERRORTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .EndringAvTillatelse(true, null, null, null, null, true)
                .Build();
            val.VedleggValidering(endringAvTillatelse, new List<string>() { "Gjennomfoeringsplan", "TegningNyFasade", "TegningNyPlan", "TegningNyttSnitt" });
            var result = val.GetResult();

            TestHelper.AssertResult(result, "4402.1.71", 1);
        }

        [Fact(DisplayName = "4402.1.73 VedleggValidering - Error")]
        public void EndringAvTillatelse_4402_1_73_VedleggValideringERRORTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .EndringAvTillatelse(true, null, null, null)
                .Build();
            val.VedleggValidering(endringAvTillatelse, new List<string>() { "Gjennomfoeringsplan", "TegningNyFasade", "TegningNyttSnitt", "Situasjonsplan" });
            var result = val.GetResult();

            TestHelper.AssertResult(result, "4402.1.73", 1);
        }
        [Fact(DisplayName = "4402.1.73a VedleggValidering - Error")]
        public void EndringAvTillatelse_4402_1_73a_VedleggValideringERRORTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .EndringAvTillatelse(false, null, null, null, null, true)
                .Build();
            val.VedleggValidering(endringAvTillatelse, new List<string>() { "Gjennomfoeringsplan", "TegningNyFasade", "TegningNyttSnitt", "Situasjonsplan" });
            var result = val.GetResult();

            TestHelper.AssertResult(result, "4402.1.73", 1);
        }
        [Fact(DisplayName = "4402.1.74 VedleggValidering - Error")]
        public void EndringAvTillatelse_4402_1_74_VedleggValideringERRORTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .EndringAvTillatelse(false, true, null, null)
                .Build();
            val.VedleggValidering(endringAvTillatelse, new List<string>() { "Gjennomfoeringsplan", "TegningNyFasade", "TegningNyPlan", "TegningNyttSnitt" });
            var result = val.GetResult();

            TestHelper.AssertResult(result, "4402.1.74", 1);
        }

        [Fact(DisplayName = "4402.1.77 VedleggValidering - OK")]
        public void EndringAvTillatelse_4402_1_77_VedleggValideringOkTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .Foelgebrev("foelgebrev")
                .Build();
            val.FoelgebrevValidering(endringAvTillatelse, new List<string>() { "Folgebrev" });
            var result = val.GetResult();

            var list = result.rulesChecked;
            var rulesChecked = TestHelper.ConverRulesCheckedJson(result);
            //var rulesCheckedSortes = TestHelper.ConverRulesCheckedJson(listLines1);

            TestHelper.AssertResult(result, "", 0);
        }
        [Fact(DisplayName = "4402.1.77 VedleggValidering - ERROR")]
        public void EndringAvTillatelse_4402_1_77_VedleggValideringErrrorTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var endringAvTillatelse = new EndringAvTillatelseBuilder()
                .Build();
            val.FoelgebrevValidering(endringAvTillatelse, new List<string>() { "*Folgebrev" });
            var result = val.GetResult();

            var list = result.rulesChecked;
            var rulesChecked = TestHelper.ConverRulesCheckedJson(result, false);
            //var rulesCheckedSortes = TestHelper.ConverRulesCheckedJson(listLines1);

            TestHelper.AssertResult(result, "4402.1.77", 1);
        }

        // testModel Test
        [Fact(DisplayName = " Endring Av Tillatelse - Bruk", Skip = "feil i db")]
        public void EndringAvTillatelse_EndringAvTillatelseAvBrukTest()
        {
            var val = new EndringAvTillatelseFormValidator();

            var formSkjema = new EndringAvTillatelseDfV42350()._09EndringAnnetLangeTekststrengerOgLinjeskift(true);
            var xmlForApiV2 = TestHelper.GetXmlWithoutSpaces(formSkjema.Data);

            var endringAvTillatelse = TestHelper.GetValue<EndringAvTillatelseType>(formSkjema);

            val.Validate(endringAvTillatelse, new List<string>() { "TegningNyFasade", "TegningNyPlan", "TegningNyttSnitt", "Gjennomføringsplan" });
            var result = val.GetResult();

            var list = result.messages.Select(m => m.reference).ToList();
            var rulesChecked = TestHelper.ConverRulesCheckedJson(result);
        }
    }
}
