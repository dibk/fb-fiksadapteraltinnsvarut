﻿using System;
using System.Diagnostics;
using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.FileDownload;
using Moq;
using Xunit;
using Xunit.Abstractions;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Tests.Services
{
    public class FileDownloadUrlProviderTests
    {
        private readonly ITestOutputHelper output;
        public FileDownloadUrlProviderTests(ITestOutputHelper output)
        {
            this.output = output;
        }
        [Fact]
        public void FileDownloadUrlProvider_hostNameWithoutTrailingSlash()
        {
            var hostName = "http://www.testserver2000.com";
            var fileName = "test.pdf";
            var fileGuid = Guid.NewGuid();
            var fileDownloadStatus = new FileDownloadStatus() { Filename = fileName, Guid = fileGuid };

            var urlProvider = new FileDownloadApiUrlProvider(hostName);

            var result = urlProvider.GenerateDownloadUri(fileDownloadStatus);
            output.WriteLine($"Result {result}");

            var expectedResult = $"http://www.testserver2000.com/api/download/{fileGuid.ToString()}/{fileName}";

            Assert.Equal(expectedResult, result);
        }

        [Fact]
        public void FileDownloadUrlProvider_serverNameContainsTralingSlash()
        {
            var hostName = "http://www.testserver2000.com/";
            var fileName = "test.pdf";
            var fileGuid = Guid.NewGuid();
            var fileDownloadStatus = new FileDownloadStatus() { Filename = fileName, Guid = fileGuid };

            var urlProvider = new FileDownloadApiUrlProvider(hostName);

            var result = urlProvider.GenerateDownloadUri(fileDownloadStatus);
            output.WriteLine($"Result {result}");

            var expectedResult = $"http://www.testserver2000.com/api/download/{fileGuid.ToString()}/{fileName}";

            Assert.Equal(expectedResult, result);
        }

        [Fact]
        public void FileDownloadUrlProvider_oneSpecialCharacterInFileName()
        {
            var hostName = "http://www.testserver2000.com/";
            var fileName = "vedlegg.#test.pdf";
            var fileGuid = Guid.NewGuid();
            var fileDownloadStatus = new FileDownloadStatus() { Filename = fileName, Guid = fileGuid };

            var urlProvider = new FileDownloadApiUrlProvider(hostName);

            var result = urlProvider.GenerateDownloadUri(fileDownloadStatus);
            output.WriteLine($"Result {result}");

            var expectedResult = $"http://www.testserver2000.com/api/download/{fileGuid.ToString()}/vedlegg.%23test.pdf";

            Assert.Equal(expectedResult, result);
        }

        [Fact]
        public void FileDownloadUrlProvider_multipleSpecialCharactersAndWhitespacesInFileName()
        {
            var hostName = "http://www.testserver2000.com/";
            var fileName = "og så, har vi et vedlegg.#test.pdf";
            var fileGuid = Guid.NewGuid();
            var fileDownloadStatus = new FileDownloadStatus() { Filename = fileName, Guid = fileGuid };

            var urlProvider = new FileDownloadApiUrlProvider(hostName);

            var result = urlProvider.GenerateDownloadUri(fileDownloadStatus);
            output.WriteLine($"Result {result}");

            var expectedResult = $"http://www.testserver2000.com/api/download/{fileGuid.ToString()}/og%20s%C3%A5%2C%20har%20vi%20et%20vedlegg.%23test.pdf";

            Assert.Equal(expectedResult.ToUpper(), result.ToUpper());
        }

        [Fact]
        public void FileDownloadUrlProvider_multipleNorwegianLettersAndWhitespacesInFileName()
        {
            var hostName = "http://www.testserver2000.com/";
            var fileName = "Vedlegg2.TRÆRNE STÅR PÅ EN KNAUS.pdf";
            var fileGuid = Guid.NewGuid();
            var fileDownloadStatus = new FileDownloadStatus() { Filename = fileName, Guid = fileGuid };

            var urlProvider = new FileDownloadApiUrlProvider(hostName);

            var result = urlProvider.GenerateDownloadUri(fileDownloadStatus);
            output.WriteLine($"Result {result}");

            var expectedResult = $"http://www.testserver2000.com/api/download/{fileGuid.ToString()}/Vedlegg2.TR%C3%86RNE%20ST%C3%85R%20P%C3%85%20EN%20KNAUS.pdf";

            Assert.Equal(expectedResult.ToUpper(), result.ToUpper());
        }

        [Fact]
        public void FileDownloadUrlProvider_parentesesAndWhitespacesInFileName()
        {
            var hostName = "http://www.testserver2000.com/";
            var fileName = "Vedlegg1.dummy (1) (1) (2) (2) (1) (1).png";
            var fileGuid = Guid.NewGuid();
            var fileDownloadStatus = new FileDownloadStatus() { Filename = fileName, Guid = fileGuid };

            var urlProvider = new FileDownloadApiUrlProvider(hostName);

            var result = urlProvider.GenerateDownloadUri(fileDownloadStatus);
            output.WriteLine($"Result {result}");

            var expectedResult = $"http://www.testserver2000.com/api/download/{fileGuid.ToString()}/Vedlegg1.dummy%20%281%29%20%281%29%20%282%29%20%282%29%20%281%29%20%281%29.png";

            Assert.Equal(expectedResult.ToUpper(), result.ToUpper());
        }

        [Fact]
        public void FileDownloadUrlProvider_longComplexFilename()
        {
            var hostName = "http://www.testserver2000.com/";
            var fileName = "Vedlegg1.HK-#274449-v1-NETBAS_Plot_-_Vedlegg_til_søknad_om_forelegging_-_Prosjekt_40166_Leirvik_Sentrum – Kopi.pdf";
            var fileGuid = Guid.NewGuid();
            var fileDownloadStatus = new FileDownloadStatus() { Filename = fileName, Guid = fileGuid };

            var urlProvider = new FileDownloadApiUrlProvider(hostName);

            var result = urlProvider.GenerateDownloadUri(fileDownloadStatus);
            output.WriteLine($"Result {result}");

            var expectedResult = $"http://www.testserver2000.com/api/download/{fileGuid.ToString()}/Vedlegg1.HK-%23274449-v1-NETBAS_Plot_-_Vedlegg_til_s%C3%B8knad_om_forelegging_-_Prosjekt_40166_Leirvik_Sentrum%20%E2%80%93%20Kopi.pdf";

            Assert.Equal(expectedResult.ToUpper(), result.ToUpper());
        }
    }
}