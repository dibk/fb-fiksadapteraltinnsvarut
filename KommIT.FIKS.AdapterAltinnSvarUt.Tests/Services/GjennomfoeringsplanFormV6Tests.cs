﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.Builders;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.SvarUtForm;

using Xunit;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Tests.Services
{
    public class GjennomfoeringsplanFormV6Tests
    {
        [Fact(DisplayName = "4398.1.4 EiendomByggestedValidering - OK test")]
        public void Gjennomforingsplan_4398_1_4_EiendomByggestedValideringOKTest()
        {

            var val = new GjennomforingsplanFormV6Validator();
            val._municipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");
            val._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, true);



            var form = new GjennomfoeringsplanV6Builder()
                .EiendomByggested("9999", "1", "3", "0", "0", "Hammerfest", "H0101", "192423880", "Båtsfjorden 6", "Båtsfjorden", "6", "9664", "SANDØYBOTN")
                .Build();
            val.EiendomByggestedValidation(form);
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }

        [Fact(DisplayName = "4398.1.4 EiendomByggestedValidering - Error test")]
        public void Gjennomforingsplan_4398_1_4_EiendomByggestedValideringErrorTest()
        {

            var val = new GjennomforingsplanFormV6Validator();

            var form = new GjennomfoeringsplanV6Builder()
                .EiendomByggested()
                .Build();
            val.EiendomByggestedValidation(form);
            var result = val.GetResult();

            result.messages.Where(m => m.reference.Equals("4398.1.4")).Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4398.1.4.1 EiendomByggestedValidering - ERROR test")]
        public void Gjennomforingsplan_4398_1_4_1_EiendomByggestedValideringErrorTest()
        {

            var val = new GjennomforingsplanFormV6Validator();
            val._municipalityValidator = TestHelper.MokMunicipalityValidatorResult("empty");
            val._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, true);

            var form = new GjennomfoeringsplanV6Builder()
                .EiendomByggested(null, "1", "3", "0", "0", "Hammerfest", "H0101", "192423880", "Båtsfjorden 6", "Båtsfjorden", "6", "9664", "SANDØYBOTN")
                .Build();
            val.EiendomByggestedValidation(form);
            var result = val.GetResult();

            result.messages.Where(m => m.reference.Equals("4398.1.4.1")).Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4398.1.4.2 EiendomByggestedValidering - ERROR test")]
        public void Gjennomforingsplan_4398_1_4_2_EiendomByggestedValideringErrorTest()
        {

            var val = new GjennomforingsplanFormV6Validator();

            val._municipalityValidator = TestHelper.MokMunicipalityValidatorResult("invalid");
            val._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, true);

            var form = new GjennomfoeringsplanV6Builder()
                .EiendomByggested("*9999", "1", "3", "0", "0", "Hammerfest", "H0101", "192423880", "Båtsfjorden 6", "Båtsfjorden", "6", "9664", "SANDØYBOTN")
                .Build();
            val.EiendomByggestedValidation(form);
            var result = val.GetResult();

            result.messages.Where(m => m.reference.Equals("4398.1.4.2")).Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }


        [Fact(DisplayName = "4398.1.4.3 EiendomByggestedValidering - ERROR test")]
        public void Gjennomforingsplan_4398_1_4_3_EiendomByggestedValideringErrorTest()
        {

            var val = new GjennomforingsplanFormV6Validator();
            val._municipalityValidator = TestHelper.MokMunicipalityValidatorResult("expired");
            val._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, true);


            var form = new GjennomfoeringsplanV6Builder()
                .EiendomByggested("9999", "1", "3", "0", "0", "Hammerfest", "H0101", "192423880", "Båtsfjorden 6", "Båtsfjorden", "6", "9664", "SANDØYBOTN")
                .Build();
            val.EiendomByggestedValidation(form);
            var result = val.GetResult();

            result.messages.Where(m => m.reference.Equals("4398.1.4.3")).Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);

        }

        //[Fact(DisplayName = "4398.1.4.4 EiendomByggestedValidering - ERROR test")]
        //public void Gjennomforingsplan_4398_1_4_4_EiendomByggestedValideringErrorTest()
        //{

        //    var val = new GjennomforingsplanFormV6Validator();
        //    val._municipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");
        //    val._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, true);


        //    var form = new GjennomfoeringsplanV6Builder()
        //        .EiendomByggested("9999", null, "3", "0", "0", "Hammerfest", "H0101", "192423880", "Båtsfjorden 6", "Båtsfjorden", "6", "9664", "SANDØYBOTN")
        //        .Build();
        //    val.EiendomByggestedValidation(form);
        //    var result = val.GetResult();

        //    result.messages.Where(m => m.reference.Equals("4398.1.4.4")).Should().NotBeNullOrEmpty();
        //    result.messages.Count.Should().Be(1);

        //}

        //[Fact(DisplayName = "4398.1.4.5 EiendomByggestedValidering - ERROR test")]
        //public void Gjennomforingsplan_4398_1_4_5_EiendomByggestedValideringErrorTest()
        //{

        //    var val = new GjennomforingsplanFormV6Validator();
        //    val._municipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");
        //    val._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, true);


        //    var form = new GjennomfoeringsplanV6Builder()
        //        .EiendomByggested("9999", "1", "*3", "0", "0", "Hammerfest", "H0101", "192423880", "Båtsfjorden 6", "Båtsfjorden", "6", "9664", "SANDØYBOTN")
        //        .Build();
        //    val.EiendomByggestedValidation(form);
        //    var result = val.GetResult();

        //    result.messages.Where(m => m.reference.Equals("4398.1.4.5")).Should().NotBeNullOrEmpty();
        //    result.messages.Count.Should().Be(1);

        //}

        //[Fact(DisplayName = "4398.1.4.6 EiendomByggestedValidering - ERROR test")]
        //public void Gjennomforingsplan_4398_1_4_6_EiendomByggestedValideringErrorTest()
        //{

        //    var val = new GjennomforingsplanFormV6Validator();
        //    val._municipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");
        //    val._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, true);


        //    var form = new GjennomfoeringsplanV6Builder()
        //        .EiendomByggested("9999", "1", "0", "0", "0", "Hammerfest", "H0101", "192423880", "Båtsfjorden 6", "Båtsfjorden", "6", "9664", "SANDØYBOTN")
        //        .Build();
        //    val.EiendomByggestedValidation(form);
        //    var result = val.GetResult();

        //    result.messages.Where(m => m.reference.Equals("4398.1.4.6")).Should().NotBeNullOrEmpty();
        //    result.messages.Count.Should().Be(1);

        //}

        //Matrikkel

        [Fact(DisplayName = "4398.1.4.8 EiendomByggestedValidering - ERROR test")]
        public void Gjennomforingsplan_4398_1_4_8_EiendomByggestedValideringErrorTest()
        {

            var val = new GjennomforingsplanFormV6Validator();
            val._municipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");
            val._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, true);


            var form = new GjennomfoeringsplanV6Builder()
                .EiendomByggested("9999", "1", "3", "0", "0", "Hammerfest", "H0101", "0", "Båtsfjorden 6", "Båtsfjorden", "6", "9664", "SANDØYBOTN")
                .Build();
            val.EiendomByggestedValidation(form);
            var result = val.GetResult();

            result.messages.Where(m => m.reference.Equals("4398.1.4.8")).Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4398.1.4.9 EiendomByggestedValidering - ERROR test")]
        public void Gjennomforingsplan_4398_1_4_9_EiendomByggestedValideringErrorTest()
        {

            var val = new GjennomforingsplanFormV6Validator();
            val._municipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");
            val._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, true);


            var form = new GjennomfoeringsplanV6Builder()
                .EiendomByggested("9999", "1", "3", "0", "0", "Hammerfest", "H0101", "192423880")
                .Build();
            val.EiendomByggestedValidation(form);
            var result = val.GetResult();

            result.messages.Where(m => m.reference.Equals("4398.1.4.9")).Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4398.1.4.9.1 EiendomByggestedValidering - ERROR test")]
        public void Gjennomforingsplan_4398_1_4_9_1_EiendomByggestedValideringErrorTest()
        {

            var val = new GjennomforingsplanFormV6Validator();
            val._municipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");
            val._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, true);


            var form = new GjennomfoeringsplanV6Builder()
                .EiendomByggested("9999", "1", "3", "0", "0", "Hammerfest", "H0101", "192423880", null, "Båtsfjorden", "6", "9664", "SANDØYBOTN")
                .Build();
            val.EiendomByggestedValidation(form);
            var result = val.GetResult();

            result.messages.Where(m => m.reference.Equals("4398.1.4.9.1")).Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4398.1.4.9.2 EiendomByggestedValidering - ERROR test")]
        public void Gjennomforingsplan_4398_1_4_9_2_EiendomByggestedValideringErrorTest()
        {

            var val = new GjennomforingsplanFormV6Validator();
            val._municipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");
            val._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, true);


            var form = new GjennomfoeringsplanV6Builder()
                .EiendomByggested("9999", "1", "3", "0", "0", "Hammerfest", "H0101", "192423880", "Båtsfjorden 6", null, "6", "9664", "SANDØYBOTN")
                .Build();
            val.EiendomByggestedValidation(form);
            var result = val.GetResult();


            result.messages.Where(m => m.reference.Equals("4398.1.4.9.2")).Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4398.1.4.10 EiendomByggestedValidering - ERROR test")]
        public void Gjennomforingsplan_4398_1_4_10_EiendomByggestedValideringErrorTest()
        {

            var val = new GjennomforingsplanFormV6Validator();
            val._municipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");
            val._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, true);


            var form = new GjennomfoeringsplanV6Builder()
                .EiendomByggested("9999", "1", "3", "0", "0", null, "H0101", "192423880", "Båtsfjorden 6", "Båtsfjorden", "6", "9664", "SANDØYBOTN")
                .Build();
            val.EiendomByggestedValidation(form);
            var result = val.GetResult();


            result.messages.Where(m => m.reference.Equals("4398.1.4.10")).Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4398.1.4.11 EiendomByggestedValidering - ERROR test")]
        public void Gjennomforingsplan_4398_1_4_11_EiendomByggestedValideringErrorTest()
        {

            var val = new GjennomforingsplanFormV6Validator();
            val._municipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");
            val._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, true);


            var form = new GjennomfoeringsplanV6Builder()
                .EiendomByggested("9999", "1", "3", "0", "0", "Hammerfest", "*H0101", "192423880", "Båtsfjorden 6", "Båtsfjorden", "6", "9664", "SANDØYBOTN")
                .Build();
            val.EiendomByggestedValidation(form);
            var result = val.GetResult();


            result.messages.Where(m => m.reference.Equals("4398.1.4.11")).Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4398.1.4.12 EiendomByggestedValidering - Error test")]
        public void Gjennomforingsplan_4398_1_4_12_EiendomByggestedValideringErrorTest()
        {

            var val = new GjennomforingsplanFormV6Validator();
            val._municipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");
            val._matrikkelService = TestHelper.MokMatrikkelServiceResult(false, true, true);



            var form = new GjennomfoeringsplanV6Builder()
                .EiendomByggested("9999", "1", "3", "0", "0", "Hammerfest", "H0101", "192423880", "Båtsfjorden 6", "Båtsfjorden", "6", "9664", "SANDØYBOTN")
                .Build();
            val.EiendomByggestedValidation(form);
            var result = val.GetResult();

            result.messages.Where(m => m.reference.Equals("4398.1.4.12")).Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4398.1.4.13 EiendomByggestedValidering - Error test")]
        public void Gjennomforingsplan_4398_1_4_13_EiendomByggestedValideringErrorTest()
        {

            var val = new GjennomforingsplanFormV6Validator();
            val._municipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");
            val._matrikkelService = TestHelper.MokMatrikkelServiceResult(null, true, true);



            var form = new GjennomfoeringsplanV6Builder()
                .EiendomByggested("9999", "1", "3", "0", "0", "Hammerfest", "H0101", "192423880", "Båtsfjorden 6", "Båtsfjorden", "6", "9664", "SANDØYBOTN")
                .Build();
            val.EiendomByggestedValidation(form);
            var result = val.GetResult();

            result.messages.Where(m => m.reference.Equals("4398.1.4.13")).Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4398.1.4.15 EiendomByggestedValidering - Error test")]
        public void Gjennomforingsplan_4398_1_4_15_EiendomByggestedValideringErrorTest()
        {

            var val = new GjennomforingsplanFormV6Validator();
            val._municipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");
            val._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, null);



            var form = new GjennomfoeringsplanV6Builder()
                .EiendomByggested("9999", "1", "3", "0", "0", "Hammerfest", "H0101", "192423880", "Båtsfjorden 6", "Båtsfjorden", "6", "9664", "SANDØYBOTN")
                .Build();
            val.EiendomByggestedValidation(form);
            var result = val.GetResult();

            result.messages.Where(m => m.reference.Equals("4398.1.4.15")).Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4398.1.4.15.1 EiendomByggestedValidering - Error test")]
        public void Gjennomforingsplan_4398_1_4_15_1_EiendomByggestedValideringErrorTest()
        {

            var val = new GjennomforingsplanFormV6Validator();
            val._municipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");
            val._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, false);



            var form = new GjennomfoeringsplanV6Builder()
                .EiendomByggested("9999", "1", "3", "0", "0", "Hammerfest", "H0101", "192423880", "Båtsfjorden 6", "Båtsfjorden", "6", "9664", "SANDØYBOTN")
                .Build();
            val.EiendomByggestedValidation(form);
            var result = val.GetResult();

            result.messages.Where(m => m.reference.Equals("4398.1.4.15.1")).Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4398.4.1 SaksnummerErUtfyltVedHovedsoeknad - OK test")]
        public void Gjennomforingsplan_4398_4_1_SaksnummerErUtfyltVedHovedsoeknadOKTest()
        {

            var val = new GjennomforingsplanFormV6Validator();
            var form = new GjennomfoeringsplanV6Builder()
                .KommunensSaksnummer("2020", "123456789")
                .Build();
            val.SaksnummerErUtfyltVedHovedsoeknad(form, new List<string>() { });
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }

        [Fact(DisplayName = "4398.4.1 a SaksnummerErUtfyltVedHovedsoeknad - OK test")]
        public void Gjennomforingsplan_4398_4_1_a_SaksnummerErUtfyltVedHovedsoeknadOKTest()
        {

            var val = new GjennomforingsplanFormV6Validator();
            var form = new GjennomfoeringsplanV6Builder()
                .Build();
            val.SaksnummerErUtfyltVedHovedsoeknad(form, new List<string>() {"Søknad om midlertidig brukstillatelse"});
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }

        //[Fact(DisplayName = "4398.4.1 SaksnummerErUtfyltVedHovedsoeknad - Error test")]
        //public void Gjennomforingsplan_4398_4_1_SaksnummerErUtfyltVedHovedsoeknadErrorTest()
        //{

        //    var val = new GjennomforingsplanFormV6Validator();
        //    var form = new GjennomfoeringsplanV6Builder()

        //        .Build();
        //    val.SaksnummerErUtfyltVedHovedsoeknad(form, new List<string>() {"*Søknad om midlertidig brukstillatelse"});
        //    var result = val.GetResult();

        //    result.messages.Where(m => m.reference.Equals("4398.4.1")).Should().NotBeNullOrEmpty();
        //    result.messages.Count.Should().Be(1);
        //}

        //[Fact(DisplayName = "4398.4.1.1 SaksnummerErUtfyltVedHovedsoeknad - Error test")]
        //public void Gjennomforingsplan_4398_4_1_1_SaksnummerErUtfyltVedHovedsoeknadErrorTest()
        //{

        //    var val = new GjennomforingsplanFormV6Validator();
        //    var form = new GjennomfoeringsplanV6Builder()
        //        .KommunensSaksnummer(null, "12345678")
        //        .Build();
        //    val.SaksnummerErUtfyltVedHovedsoeknad(form, new List<string>() { });
        //    var result = val.GetResult();

        //    result.messages.Where(m => m.reference.Equals("4398.4.1.1")).Should().NotBeNullOrEmpty();
        //    result.messages.Count.Should().Be(1);
        //}

        //[Fact(DisplayName = "4398.4.1.2 SaksnummerErUtfyltVedHovedsoeknad - Error test")]
        //public void Gjennomforingsplan_4398_4_1_2_SaksnummerErUtfyltVedHovedsoeknadErrorTest()
        //{

        //    var val = new GjennomforingsplanFormV6Validator();
        //    var form = new GjennomfoeringsplanV6Builder()
        //        .KommunensSaksnummer("2020", null)
        //        .Build();
        //    val.SaksnummerErUtfyltVedHovedsoeknad(form, new List<string>() { });
        //    var result = val.GetResult();

        //    result.messages.Where(m => m.reference.Equals("4398.4.1.2")).Should().NotBeNullOrEmpty();
        //    result.messages.Count.Should().Be(1);
        //}

        [Fact(DisplayName = "4398.1.6 AnsvarsomraadeValidation - Ok test")]
        public void Gjennomforingsplan_4398_1_6_AnsvarsomraadeValidationOkTest()
        {

            var val = new GjennomforingsplanFormV6Validator();
            var form = new GjennomfoeringsplanV6Builder()
                .Ansvarsomraade("PRO", "Ansvarlig prosjektering", "Arkitektur prosjektering av bygg", "1", "1", true, false,
                    false, false, new DateTime(2020, 07, 06), new DateTime(2020, 07, 06), new DateTime(2020, 07, 06), new DateTime(2020, 07, 06)
                    , true, "AR132456", true, "NN123548", "status", new DateTime(2020, 07, 06), "vedlegg navn")
                .Build();
            val.AnsvarsomraadeValidation(form);
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }

        //[Fact(DisplayName = "4398.1.6 AnsvarsomraadeValidation - Error test")]
        //public void Gjennomforingsplan_4398_1_6_AnsvarsomraadeValidationErrorTest()
        //{

        //    var val = new GjennomforingsplanFormV6Validator();
        //    var form = new GjennomfoeringsplanV6Builder()

        //        .Build();
        //    val.AnsvarsomraadeValidation(form);
        //    var result = val.GetResult();

        //    result.messages.Where(m => m.reference.Equals("4398.1.6")).Should().NotBeNullOrEmpty();
        //    result.messages.Count.Should().Be(1);
        //}

        //[Fact(DisplayName = "4398.1.6.1 AnsvarsomraadeValidation - Error test")]
        //public void Gjennomforingsplan_4398_1_6_1_AnsvarsomraadeValidationErrorTest()
        //{

        //    var val = new GjennomforingsplanFormV6Validator();
        //    var form = new GjennomfoeringsplanV6Builder()
        //        .Ansvarsomraade("*PRO", "Ansvarlig prosjektering", "Arkitektur prosjektering av bygg", "1", "1", true, false,
        //            false, false, new DateTime(2020, 07, 06), new DateTime(2020, 07, 06), new DateTime(2020, 07, 06), new DateTime(2020, 07, 06)
        //            , true, "AR132456", true, "NN123548", "status", new DateTime(2020, 07, 06), "vedlegg navn")
        //        .Build();
        //    val.AnsvarsomraadeValidation(form);
        //    var result = val.GetResult();

        //    result.messages.Where(m => m.reference.Equals("4398.1.6.1")).Should().NotBeNullOrEmpty();
        //    result.messages.Count.Should().Be(1);
        //}

        //[Fact(DisplayName = "4398.1.6.2 AnsvarsomraadeValidation - Error test")]
        //public void Gjennomforingsplan_4398_1_6_2_AnsvarsomraadeValidationErrorTest()
        //{

        //    var val = new GjennomforingsplanFormV6Validator();
        //    var form = new GjennomfoeringsplanV6Builder()
        //        .Ansvarsomraade("SØK", "Ansvarlig søker", "Arkitektur prosjektering av bygg", "1", "1",
        //            true, null, null, null,
        //            null, null, null, null
        //            , true, "AR132456", true, "NN123548", "status", new DateTime(2020, 07, 06), "vedlegg navn")
        //        .Build();
        //    val.AnsvarsomraadeValidation(form);
        //    var result = val.GetResult();

        //    result.messages.Where(m => m.reference.Equals("4398.1.6.2")).Should().NotBeNullOrEmpty();
        //    result.messages.Count.Should().Be(1);
        //}

        //[Fact(DisplayName = "4398.1.6.3 AnsvarsomraadeValidation - Error test")]
        //public void Gjennomforingsplan_4398_1_6_3_AnsvarsomraadeValidationErrorTest()
        //{

        //    var val = new GjennomforingsplanFormV6Validator();
        //    var form = new GjennomfoeringsplanV6Builder()
        //        .Ansvarsomraade("SØK", "Ansvarlig søker", "Arkitektur prosjektering av bygg", "1", "1",
        //            null, null, null, null,
        //            new DateTime(2020, 07, 06), null, null, null
        //            , true, "AR132456", true, "NN123548", "status", new DateTime(2020, 07, 06), "vedlegg navn")
        //        .Build();
        //    val.AnsvarsomraadeValidation(form);
        //    var result = val.GetResult();

        //    result.messages.Where(m => m.reference.Equals("4398.1.6.3")).Should().NotBeNullOrEmpty();
        //    result.messages.Count.Should().Be(1);
        //}

        [Fact(DisplayName = "4398.1.5 AnsvarligSokerValidation - ok test")]
        public void Gjennomforingsplan_4398_1_5_AnsvarligSokerValidationOkTest()
        {

            var val = new GjennomforingsplanFormV6Validator();
            var postal = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "TRONDHEIM");
            val._postalCodeProvider = postal;
            var form = new GjennomfoeringsplanV6Builder()
                .AnsvarligSoeker("974760673", null, "Foretak", "Foretak", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "no", "Trondheim", "11223344", "99887766", "post@test.no", "Benjamin Fjell", "55512345", "kontakt@epost.no")
                .Build();
            val.AnsvarligSokerValidation(form);
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }

        //[Fact(DisplayName = "4398.1.5 AnsvarligSokerValidation - Error test")]
        //public void Gjennomforingsplan_4398_1_5_AnsvarligSokerValidationErrorTest()
        //{

        //    var val = new GjennomforingsplanFormV6Validator();
        //    var postal = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "TRONDHEIM");
        //    val._postalCodeProvider = postal;
        //    var form = new GjennomfoeringsplanV6Builder()
        //        .AnsvarligSoeker()
        //        .Build();
        //    val.AnsvarligSokerValidation(form);
        //    var result = val.GetResult();

        //    result.messages.Where(m => m.reference.Equals("4398.1.5")).Should().NotBeNullOrEmpty();
        //    result.messages.Count.Should().Be(1);
        //}

        //[Fact(DisplayName = "4398.1.5.1 AnsvarligSokerValidation - Error test")]
        //public void Gjennomforingsplan_4398_1_5_1_AnsvarligSokerValidationErrorTest()
        //{

        //    var val = new GjennomforingsplanFormV6Validator();
        //    var postal = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "TRONDHEIM");
        //    val._postalCodeProvider = postal;
        //    var form = new GjennomfoeringsplanV6Builder()
        //        .AnsvarligSoeker("974760673", null, null, "Foretak", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "no", "Trondheim", "11223344", "99887766", "post@test.no", "Benjamin Fjell", "55512345", "kontakt@epost.no")
        //        .Build();
        //    val.AnsvarligSokerValidation(form);
        //    var result = val.GetResult();

        //    result.messages.Where(m => m.reference.Equals("4398.1.5.1")).Should().NotBeNullOrEmpty();
        //    result.messages.Count.Should().Be(1);
        //}

        //[Fact(DisplayName = "4398.1.5.2 AnsvarligSokerValidation - Error test")]
        //public void Gjennomforingsplan_4398_1_5_2_AnsvarligSokerValidationErrorTest()
        //{

        //    var val = new GjennomforingsplanFormV6Validator();
        //    var postal = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "TRONDHEIM");
        //    val._postalCodeProvider = postal;
        //    var form = new GjennomfoeringsplanV6Builder()
        //        .AnsvarligSoeker("974760673", null, "*Foretak", "Foretak", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "no", "Trondheim", "11223344", "99887766", "post@test.no", "Benjamin Fjell", "55512345", "kontakt@epost.no")
        //        .Build();
        //    val.AnsvarligSokerValidation(form);
        //    var result = val.GetResult();

        //    result.messages.Where(m => m.reference.Equals("4398.1.5.2")).Should().NotBeNullOrEmpty();
        //    result.messages.Count.Should().Be(1);
        //}

        //[Fact(DisplayName = "4398.1.5.3 AnsvarligSokerValidation - Error test")]
        //public void Gjennomforingsplan_4398_1_5_3_AnsvarligSokerValidationErrorTest()
        //{

        //    var val = new GjennomforingsplanFormV6Validator();
        //    var postal = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "TRONDHEIM");
        //    val._postalCodeProvider = postal;
        //    var form = new GjennomfoeringsplanV6Builder()
        //        .AnsvarligSoeker("974760673", null, "Privatperson", "Privatperson", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "no", "Trondheim", "11223344", "99887766", "post@test.no", "Benjamin Fjell", "55512345", "kontakt@epost.no")
        //        .Build();
        //    val.AnsvarligSokerValidation(form);
        //    var result = val.GetResult();

        //    result.messages.Where(m => m.reference.Equals("4398.1.5.3")).Should().NotBeNullOrEmpty();
        //    result.messages.Count.Should().Be(1);
        //}

        //[Fact(DisplayName = "4398.1.5.4 AnsvarligSokerValidation - Error test")]
        //public void Gjennomforingsplan_4398_1_5_4_AnsvarligSokerValidationErrorTest()
        //{

        //    var val = new GjennomforingsplanFormV6Validator();
        //    var postal = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "TRONDHEIM");
        //    val._postalCodeProvider = postal;
        //    var form = new GjennomfoeringsplanV6Builder()
        //        .AnsvarligSoeker(null, null, "Foretak", "Foretak", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "no", "Trondheim", "11223344", "99887766", "post@test.no", "Benjamin Fjell", "55512345", "kontakt@epost.no")
        //        .Build();
        //    val.AnsvarligSokerValidation(form);
        //    var result = val.GetResult();

        //    result.messages.Where(m => m.reference.Equals("4398.1.5.4")).Should().NotBeNullOrEmpty();
        //    result.messages.Count.Should().Be(1);
        //}

        //[Fact(DisplayName = "4398.1.5.5 AnsvarligSokerValidation - Error test")]
        //public void Gjennomforingsplan_4398_1_5_5_AnsvarligSokerValidationErrorTest()
        //{

        //    var val = new GjennomforingsplanFormV6Validator();
        //    var postal = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "TRONDHEIM");
        //    val._postalCodeProvider = postal;
        //    var form = new GjennomfoeringsplanV6Builder()
        //        .AnsvarligSoeker("000000673", null, "Foretak", "Foretak", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "no", "Trondheim", "11223344", "99887766", "post@test.no", "Benjamin Fjell", "55512345", "kontakt@epost.no")
        //        .Build();
        //    val.AnsvarligSokerValidation(form);
        //    var result = val.GetResult();

        //    result.messages.Where(m => m.reference.Equals("4398.1.5.5")).Should().NotBeNullOrEmpty();
        //    result.messages.Count.Should().Be(1);
        //}

        //[Fact(DisplayName = "4398.1.5.6 AnsvarligSokerValidation - Error test")]
        //public void Gjennomforingsplan_4398_1_5_6_AnsvarligSokerValidationErrorTest()
        //{

        //    var val = new GjennomforingsplanFormV6Validator();
        //    var postal = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "TRONDHEIM");
        //    val._postalCodeProvider = postal;
        //    var form = new GjennomfoeringsplanV6Builder()
        //        .AnsvarligSoeker("74760673", null, "Foretak", "Foretak", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "no", "Trondheim", "11223344", "99887766", "post@test.no", "Benjamin Fjell", "55512345", "kontakt@epost.no")
        //        .Build();
        //    val.AnsvarligSokerValidation(form);
        //    var result = val.GetResult();

        //    result.messages.Where(m => m.reference.Equals("4398.1.5.6")).Should().NotBeNullOrEmpty();
        //    result.messages.Count.Should().Be(1);
        //}

        //[Fact(DisplayName = "4398.4.3.6 AnsvarligSokerValidation - Error test")]
        //public void Gjennomforingsplan_4398_4_3_6_AnsvarligSokerValidationErrorTest()
        //{

        //    var val = new GjennomforingsplanFormV6Validator();
        //    var postal = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "TRONDHEIM");
        //    val._postalCodeProvider = postal;
        //    var form = new GjennomfoeringsplanV6Builder()
        //        .AnsvarligSoeker("974760673", null, "Foretak", "Foretak", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "no", "Trondheim", "11223344", "99887766", "post@test.no", null, null, null)
        //        .Build();
        //    val.AnsvarligSokerValidation(form);
        //    var result = val.GetResult();

        //    result.messages.Where(m => m.reference.Equals("4398.4.3.6")).Should().NotBeNullOrEmpty();
        //    result.messages.Count.Should().Be(1);
        //}

        //[Fact(DisplayName = "4398.4.3.6.1 AnsvarligSokerValidation - Error test")]
        //public void Gjennomforingsplan_4398_4_3_6_1_AnsvarligSokerValidationErrorTest()
        //{

        //    var val = new GjennomforingsplanFormV6Validator();
        //    var postal = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "TRONDHEIM");
        //    val._postalCodeProvider = postal;
        //    var form = new GjennomfoeringsplanV6Builder()
        //        .AnsvarligSoeker("974760673", null, "Foretak", "Foretak", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "no", "Trondheim", "11223344", "99887766", "post@test.no", null, "55512345", "kontakt@epost.no")
        //        .Build();
        //    val.AnsvarligSokerValidation(form);
        //    var result = val.GetResult();

        //    result.messages.Where(m => m.reference.Equals("4398.4.3.6.1")).Should().NotBeNullOrEmpty();
        //    result.messages.Count.Should().Be(1);
        //}

        //[Fact(DisplayName = "4398.4.3.6.2 AnsvarligSokerValidation - Error test")]
        //public void Gjennomforingsplan_4398_4_3_6_2_AnsvarligSokerValidationErrorTest()
        //{

        //    var val = new GjennomforingsplanFormV6Validator();
        //    var postal = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "TRONDHEIM");
        //    val._postalCodeProvider = postal;
        //    var form = new GjennomfoeringsplanV6Builder()
        //        .AnsvarligSoeker("974760673", null, "Foretak", "Foretak", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "no", "Trondheim", "11223344", "99887766", "post@test.no", "Benjamin Fjell", null, "kontakt@epost.no")
        //        .Build();
        //    val.AnsvarligSokerValidation(form);
        //    var result = val.GetResult();

        //    result.messages.Where(m => m.reference.Equals("4398.4.3.6.2")).Should().NotBeNullOrEmpty();
        //    result.messages.Count.Should().Be(1);
        //}

        //[Fact(DisplayName = "4398.4.3.6.5 AnsvarligSokerValidation - Error test")]
        //public void Gjennomforingsplan_4398_4_3_6_5_AnsvarligSokerValidationErrorTest()
        //{

        //    var val = new GjennomforingsplanFormV6Validator(); 
        //    var postal = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "TRONDHEIM");
        //    val._postalCodeProvider = postal;
        //    var form = new GjennomfoeringsplanV6Builder()
        //        .AnsvarligSoeker("974760673", null, "Foretak", "Foretak", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "no", "Trondheim", "11223344", "99887766", "post@test.no", "Benjamin Fjell", "55512345", null)
        //        .Build();
        //    val.AnsvarligSokerValidation(form);
        //    var result = val.GetResult();

        //    result.messages.Where(m => m.reference.Equals("4398.4.3.6.5")).Should().NotBeNullOrEmpty();
        //    result.messages.Count.Should().Be(1);

        //}
        //[Fact(DisplayName = "4398.1.5.7 AnsvarligSokerValidation - Error test")]
        //public void Gjennomforingsplan_4398_1_5_7_AnsvarligSokerValidationErrorTest()
        //{

        //    var val = new GjennomforingsplanFormV6Validator();
        //    var postal = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "TRONDHEIM");
        //    val._postalCodeProvider = postal;
        //    var form = new GjennomfoeringsplanV6Builder()
        //        .AnsvarligSoeker("974760673", null, "Foretak", "Foretak", "Siv. Ark. Fjell og Sønner", null, null, null, null, "11223344", "99887766", "post@test.no", "Benjamin Fjell", "55512345", "kontakt@epost.no")
        //        .Build();
        //    val.AnsvarligSokerValidation(form);
        //    var result = val.GetResult();

        //    result.messages.Where(m => m.reference.Equals("4398.1.5.7")).Should().NotBeNullOrEmpty();
        //    result.messages.Count.Should().Be(1);
        //}
        //[Fact(DisplayName = "4398.1.5.7.1 AnsvarligSokerValidation - Error test")]
        //public void Gjennomforingsplan_4398_1_5_7_1_AnsvarligSokerValidationErrorTest()
        //{

        //    var val = new GjennomforingsplanFormV6Validator();
        //    var postal = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "TRONDHEIM");
        //    val._postalCodeProvider = postal;
        //    var form = new GjennomfoeringsplanV6Builder()
        //        .AnsvarligSoeker("974760673", null, "Foretak", "Foretak", "Siv. Ark. Fjell og Sønner", null, "7003", "no", "Trondheim", "11223344", "99887766", "post@test.no", "Benjamin Fjell", "55512345", "kontakt@epost.no")
        //        .Build();
        //    val.AnsvarligSokerValidation(form);
        //    var result = val.GetResult();

        //    result.messages.Where(m => m.reference.Equals("4398.4.3.7.1")).Should().NotBeNullOrEmpty();
        //    result.messages.Count.Should().Be(1);
        //}
        //[Fact(DisplayName = "4398.1.5.7.2 AnsvarligSokerValidation - Error test")]
        //public void Gjennomforingsplan_4398_1_5_7_2_AnsvarligSokerValidationErrorTest()
        //{

        //    var val = new GjennomforingsplanFormV6Validator();
        //    var postal = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "TRONDHEIM");
        //    val._postalCodeProvider = postal;
        //    var form = new GjennomfoeringsplanV6Builder()
        //        .AnsvarligSoeker("974760673", null, "Foretak", "Foretak", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "*no", "Trondheim", "11223344", "99887766", "post@test.no", "Benjamin Fjell", "55512345", "kontakt@epost.no")
        //        .Build();
        //    val.AnsvarligSokerValidation(form);
        //    var result = val.GetResult();

        //    result.messages.Where(m => m.reference.Equals("4398.4.3.7.2")).Should().NotBeNullOrEmpty();
        //    result.messages.Count.Should().Be(1);
        //}
        //[Fact(DisplayName = "4398.1.5.8 AnsvarligSokerValidation - Error test")]
        //public void Gjennomforingsplan_4398_1_5_8_AnsvarligSokerValidationErrorTest()
        //{

        //    var val = new GjennomforingsplanFormV6Validator();
        //    var postal = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "TRONDHEIM");
        //    val._postalCodeProvider = postal;
        //    var form = new GjennomfoeringsplanV6Builder()
        //        .AnsvarligSoeker("974760673", null, "Foretak", "Foretak", "Siv. Ark. Fjell og Sønner", "Lillegata 5", null, "no", "Trondheim", "11223344", "99887766", "post@test.no", "Benjamin Fjell", "55512345", "kontakt@epost.no")
        //        .Build();
        //    val.AnsvarligSokerValidation(form);
        //    var result = val.GetResult();

        //    result.messages.Where(m => m.reference.Equals("4398.1.5.8")).Should().NotBeNullOrEmpty();
        //    result.messages.Count.Should().Be(1);
        //}
        //[Fact(DisplayName = "4398.4.3.7.3 AnsvarligSokerValidation - Error test")]
        //public void Gjennomforingsplan_4398_4_3_7_3_AnsvarligSokerValidationErrorTest()
        //{

        //    var val = new GjennomforingsplanFormV6Validator();
        //    var postal = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "TRONDHEIM");
        //    val._postalCodeProvider = postal;
        //    var form = new GjennomfoeringsplanV6Builder()
        //        .AnsvarligSoeker("974760673", null, "Foretak", "Foretak", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "*003", "no", "Trondheim", "11223344", "99887766", "post@test.no", "Benjamin Fjell", "55512345", "kontakt@epost.no")
        //        .Build();
        //    val.AnsvarligSokerValidation(form);
        //    var result = val.GetResult();

        //    result.messages.Where(m => m.reference.Equals("4398.4.3.7.3")).Should().NotBeNullOrEmpty();
        //    result.messages.Count.Should().Be(1);
        //}
        //[Fact(DisplayName = "4398.4.3.7.4 AnsvarligSokerValidation - Error test")]
        //public void Gjennomforingsplan_4398_4_3_7_4_AnsvarligSokerValidationErrorTest()
        //{

        //    var val = new GjennomforingsplanFormV6Validator();
        //    var postal = TestHelper.MokPostalCodeProviderResult(false, "POBOX", "TRONDHEIM");
        //    val._postalCodeProvider = postal;
            
        //    var form = new GjennomfoeringsplanV6Builder()
        //        .AnsvarligSoeker("974760673", null, "Foretak", "Foretak", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "no", "Trondheim", "11223344", "99887766", "post@test.no", "Benjamin Fjell", "55512345", "kontakt@epost.no")
        //        .Build();
        //    val.AnsvarligSokerValidation(form);
        //    var result = val.GetResult();

        //    result.messages.Where(m => m.reference.Equals("4398.4.3.7.4")).Should().NotBeNullOrEmpty();
        //    result.messages.Count.Should().Be(1);
        //}
        //[Fact(DisplayName = "4398.4.3.7.5 AnsvarligSokerValidation - Error test")]
        //public void Gjennomforingsplan_4398_4_3_7_5_AnsvarligSokerValidationErrorTest()
        //{

        //    var val = new GjennomforingsplanFormV6Validator();
        //    var postal = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "*TRONDHEIM");
        //    val._postalCodeProvider = postal;
            
        //    var form = new GjennomfoeringsplanV6Builder()
        //        .AnsvarligSoeker("974760673", null, "Foretak", "Foretak", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "no", "Trondheim", "11223344", "99887766", "post@test.no", "Benjamin Fjell", "55512345", "kontakt@epost.no")
        //        .Build();
        //    val.AnsvarligSokerValidation(form);
        //    var result = val.GetResult();

        //    result.messages.Where(m => m.reference.Equals("4398.4.3.7.5")).Should().NotBeNullOrEmpty();
        //    result.messages.Count.Should().Be(1);
        //}
        //[Fact(DisplayName = "4398.4.3.7.6 AnsvarligSokerValidation - Error test")]
        //public void Gjennomforingsplan_4398_4_3_7_6_AnsvarligSokerValidationErrorTest()
        //{

        //    var val = new GjennomforingsplanFormV6Validator();
        //    var postal = TestHelper.MokPostalCodeProviderResult(null,null,null);
        //    val._postalCodeProvider = postal;
            
        //    var form = new GjennomfoeringsplanV6Builder()
        //        .AnsvarligSoeker("974760673", null, "Foretak", "Foretak", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "no", "Trondheim", "11223344", "99887766", "post@test.no", "Benjamin Fjell", "55512345", "kontakt@epost.no")
        //        .Build();
        //    val.AnsvarligSokerValidation(form);
        //    var result = val.GetResult();

        //    result.messages.Where(m => m.reference.Equals("4398.4.3.7.6")).Should().NotBeNullOrEmpty();
        //    result.messages.Count.Should().Be(1);
        //}
        //[Fact(DisplayName = "4398.1.5.9 AnsvarligSokerValidation - Error test")]
        //public void Gjennomforingsplan_4398_1_5_9_AnsvarligSokerValidationErrorTest()
        //{

        //    var val = new GjennomforingsplanFormV6Validator();
        //    var postal = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "TRONDHEIM");
        //    val._postalCodeProvider = postal;
            
        //    var form = new GjennomfoeringsplanV6Builder()
        //        .AnsvarligSoeker("974760673", null, "Foretak", "Foretak", null, "Lillegata 5", "7003", "no", "Trondheim", "11223344", "99887766", "post@test.no", "Benjamin Fjell", "55512345", "kontakt@epost.no")
        //        .Build();
        //    val.AnsvarligSokerValidation(form);
        //    var result = val.GetResult();

        //    result.messages.Where(m => m.reference.Equals("4398.1.5.9")).Should().NotBeNullOrEmpty();
        //    result.messages.Count.Should().Be(1);
        //}
        //[Fact(DisplayName = "4398.4.3.8 AnsvarligSokerValidation - Error test")]
        //public void Gjennomforingsplan_4398_4_3_8_AnsvarligSokerValidationErrorTest()
        //{

        //    var val = new GjennomforingsplanFormV6Validator();
        //    var postal = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "TRONDHEIM");

        //    val._postalCodeProvider = postal;
        //    var form = new GjennomfoeringsplanV6Builder()
        //        .AnsvarligSoeker("974760673", null, "Foretak", "Foretak", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "no", "Trondheim", null, null, "post@test.no", "Benjamin Fjell", "55512345", "kontakt@epost.no")
        //        .Build();
        //    val.AnsvarligSokerValidation(form);
        //    var result = val.GetResult();

        //    result.messages.Where(m => m.reference.Equals("4398.4.3.8")).Should().NotBeNullOrEmpty();
        //    result.messages.Count.Should().Be(1);
        //}
        //[Fact(DisplayName = "4398.4.3.11 AnsvarligSokerValidation - Error test")]
        //public void Gjennomforingsplan_4398_4_3_11_AnsvarligSokerValidationErrorTest()
        //{

        //    var val = new GjennomforingsplanFormV6Validator();
        //    var postal = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "TRONDHEIM");
        //    val._postalCodeProvider = postal;
        //    var form = new GjennomfoeringsplanV6Builder()
        //        .AnsvarligSoeker("974760673", null, "Foretak", "Foretak", "Siv. Ark. Fjell og Sønner", "Lillegata 5", "7003", "no", "Trondheim", "11223344", "99887766", null, "Benjamin Fjell", "55512345", "kontakt@epost.no")
        //        .Build();
        //    val.AnsvarligSokerValidation(form);
        //    var result = val.GetResult();

        //    result.messages.Where(m => m.reference.Equals("4398.4.3.11")).Should().NotBeNullOrEmpty();
        //    result.messages.Count.Should().Be(1);
        //}
    }
}