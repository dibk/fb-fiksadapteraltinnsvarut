﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.SearchEngine;
using Xunit;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Tests.Services.SearchEngine
{
    public class FormMetadataDocumentTests
    {
        [Fact()]
        public void MunisipalityNameTest()
        {
            FormMetadata result = new FormMetadata()
            {
                Municipality = new Municipality("2004", "964830533", "Hammerfest"),
            };

            FormMetadataDocument formMetadataDocument= new FormMetadataDocument(result);

            formMetadataDocument.MunicipalityName.Should().NotBeEmpty();
        }
        [Fact()]
        public void MunisipalityNameErrorTest()
        {
            FormMetadata result = new FormMetadata()
            {
                Municipality = new Municipality("2004", "964830533", null),
            };

            FormMetadataDocument formMetadataDocument= new FormMetadataDocument(result);

            formMetadataDocument.MunicipalityName.Should().BeNullOrEmpty();
        }
        [Fact()]
        public void MunisipalityNameErrorTest02()
        {
            FormMetadata result = new FormMetadata()
            {
                //Municipality = new Municipality("2004", "964830533", null),
            };

            FormMetadataDocument formMetadataDocument= new FormMetadataDocument(result);

            formMetadataDocument.MunicipalityName.Should().BeNullOrEmpty();
        }
    }
}