﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.Builders;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.FormsSamples;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.SvarUtForm;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using no.kxml.skjema.dibk.midlertidigbrukstillatelseV2;
using Xunit;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Tests.Services
{
    public class MidlertidigBrukstillatelseTests
    {
        private Skjema _formSkjema;
        private List<string> _formSetElements;
        private MidlertidigBrukstillatelseType _form;


        private MidlertidigBrukstillatelseFormV2Validator _validator;

        public MidlertidigBrukstillatelseTests()
        {
            _formSkjema = new MidlertidigBrukstillatelseV2Dfv43006().MidlertidigBrukstillatelseV2_02(true);
            _formSetElements = new List<string>() { "Søknad om ansvarsrett for selvbygger", "Gjennomføringsplan", "Gjennomfoeringsplan", "GjennomføringsplanV4" };
            _form = SerializeUtil.DeserializeFromString<MidlertidigBrukstillatelseType>(_formSkjema.Data);
            _validator = new MidlertidigBrukstillatelseFormV2Validator();
        }
        [Fact(DisplayName = "4399_2_8_AnsvarValideringOKTest")]
        public void MidlertidigBrukstillatelse_4399_2_8_AnsvarValideringOKTest()
        {
            // Gjennomføringsplan (eller Søknad om ansvarsrett for selvbygger) skal følge med søknaden	Feil
            var val = new MidlertidigBrukstillatelseFormV2Validator();

            var midlertidigBrukstillatelse = new MidlertidigBrukstillatelseV2Builder()
                .Build();
            val.AnsvarValidering(_form,_formSetElements);
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4399_2_8_AnsvarValideringErrorTest")]
        public void MidlertidigBrukstillatelse_4399_2_8_AnsvarValideringErrorTest()
        {
            // Gjennomføringsplan (eller Søknad om ansvarsrett for selvbygger) skal følge med søknaden	Feil
            var val = new MidlertidigBrukstillatelseFormV2Validator();

            var midlertidigBrukstillatelse = new MidlertidigBrukstillatelseV2Builder()
                .Build();
            val.AnsvarValidering(_form, new List<string>() { "" });
            var result = val.GetResult();

            result.messages.Where(m => m.reference.Contains("4399.2.8")).Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4399_2_10_DatoferdigattestValideringErrorTest")]
        public void MidlertidigBrukstillatelse_4399_2_10_DatoferdigattestValideringOKTest()
        {
            // Gjennomføringsplan (eller Søknad om ansvarsrett for selvbygger) skal følge med søknaden	Feil
            var val = new MidlertidigBrukstillatelseFormV2Validator();

            var midlertidigBrukstillatelse = new MidlertidigBrukstillatelseV2Builder()
                .Datoferdigattest(DateTime.Now.AddDays(20))
                .Build();
            val.DatoferdigattestValidering(midlertidigBrukstillatelse);
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4399_2_10_DatoferdigattestValideringErrorTest")]
        public void MidlertidigBrukstillatelse_4399_2_10_DatoferdigattestValideringErrorTest()
        {
            // Gjennomføringsplan (eller Søknad om ansvarsrett for selvbygger) skal følge med søknaden	Feil
            var val = new MidlertidigBrukstillatelseFormV2Validator();

            var midlertidigBrukstillatelse = new MidlertidigBrukstillatelseV2Builder()
                .Datoferdigattest(null)
                .Build();
            val.DatoferdigattestValidering(midlertidigBrukstillatelse);
            var result = val.GetResult();

            result.messages.Where(m => m.reference.Contains("4399.2.10")).Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4399_2_11_SoknadsDelerAvTiltakenValideringOKTest")]
        public void MidlertidigBrukstillatelse_4399_2_11_SoknadsDelerAvTiltakenValideringOKTest()
        {
            // Når det søkes om deler av tiltaket må beskrivelse av del fylles ut	/MidlertidigBrukstillatelse/gjelderHeleTiltaket=false	Feil
            var val = new MidlertidigBrukstillatelseFormV2Validator();

            var midlertidigBrukstillatelse = new MidlertidigBrukstillatelseV2Builder()
                .SoknadsDelerAvTiltaken(false, "Første byggetrinn", "Overflatebehandling gulv", "Montert rekkverk på trapp")
                .Build();
            val.SoknadsDelerAvTiltakenValidering(midlertidigBrukstillatelse);
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4399_2_11_SoknadsDelerAvTiltakenValideringErrorTest")]
        public void MidlertidigBrukstillatelse_4399_2_11_SoknadsDelerAvTiltakenValideringErrorTest()
        {
            // Når det søkes om deler av tiltaket må beskrivelse av del fylles ut	/MidlertidigBrukstillatelse/gjelderHeleTiltaket=false	Feil
            var val = new MidlertidigBrukstillatelseFormV2Validator();

            var midlertidigBrukstillatelse = new MidlertidigBrukstillatelseV2Builder()
                .SoknadsDelerAvTiltaken(false, null, "Overflatebehandling gulv", "Montert rekkverk på trapp")
                .Build();
            val.SoknadsDelerAvTiltakenValidering(midlertidigBrukstillatelse);
            var result = val.GetResult();

            result.messages.Where(m => m.reference.Contains("4399.2.11")).Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4399_2_12_SoknadsDelerAvTiltakenValideringOKTest")]
        public void MidlertidigBrukstillatelse_4399_2_12_SoknadsDelerAvTiltakenValideringOKTest()
        {
            // Når det søkes om deler av tiltaket må resterende del fylles ut	/MidlertidigBrukstillatelse/gjelderHeleTiltaket=false	Feil
            var val = new MidlertidigBrukstillatelseFormV2Validator();

            var midlertidigBrukstillatelse = new MidlertidigBrukstillatelseV2Builder()
                .SoknadsDelerAvTiltaken(false, "Første byggetrinn", "Overflatebehandling gulv", "Montert rekkverk på trapp")
                .Build();
            val.SoknadsDelerAvTiltakenValidering(midlertidigBrukstillatelse);
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4399_2_12_SoknadsDelerAvTiltakenValideringErrorTest")]
        public void MidlertidigBrukstillatelse_4399_2_12_SoknadsDelerAvTiltakenValideringErrorTest()
        {
            // Når det søkes om deler av tiltaket må resterende del fylles ut	/MidlertidigBrukstillatelse/gjelderHeleTiltaket=false	Feil
            var val = new MidlertidigBrukstillatelseFormV2Validator();

            var midlertidigBrukstillatelse = new MidlertidigBrukstillatelseV2Builder()
                .SoknadsDelerAvTiltaken(false, "Første byggetrinn", null, "Montert rekkverk på trapp")
                .Build();
            val.SoknadsDelerAvTiltakenValidering(midlertidigBrukstillatelse);
            var result = val.GetResult();

            result.messages.Where(m => m.reference.Contains("4399.2.12")).Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4399_2_13_SoknadsDelerAvTiltakenValideringOKTest")]
        public void MidlertidigBrukstillatelse_4399_2_13_SoknadsDelerAvTiltakenValideringOKTest()
        {
            // Når tiltaket ikke har tilstrekkelig sikkerhet for brukstillatelse må utført innen, type arbeider og bekreftelse innen fylles ut	/MidlertidigBrukstillatelse/delsoeknad/harTilstrekkeligSikkerhet=false  Feil
            var val = new MidlertidigBrukstillatelseFormV2Validator();

            var midlertidigBrukstillatelse = new MidlertidigBrukstillatelseV2Builder()
                .HarTilstrekkeligSikkerhet(false, "Montert rekkverk på trapp", DateTime.Now.AddDays(10), DateTime.Now.AddDays(10))
                .Build();
            val.HarTilstrekkeligSikkerhetValidering(midlertidigBrukstillatelse);
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4399_2_13_SoknadsDelerAvTiltakenValideringErrorTest")]
        public void MidlertidigBrukstillatelse_4399_2_13_SoknadsDelerAvTiltakenValideringErrorTest()
        {
            // Når tiltaket ikke har tilstrekkelig sikkerhet for brukstillatelse må utført innen, type arbeider og bekreftelse innen fylles ut	/MidlertidigBrukstillatelse/delsoeknad/harTilstrekkeligSikkerhet=false  Feil
            var val = new MidlertidigBrukstillatelseFormV2Validator();

            var midlertidigBrukstillatelse = new MidlertidigBrukstillatelseV2Builder()
                .HarTilstrekkeligSikkerhet(false, null, DateTime.Now.AddDays(10), DateTime.Now.AddDays(10))
                .Build();
            val.HarTilstrekkeligSikkerhetValidering(midlertidigBrukstillatelse);
            var result = val.GetResult();

            result.messages.Where(m => m.reference.Contains("4399.2.13")).Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4399_2_14_SoknadsDelerAvTiltakenValideringOKTest")]
        public void MidlertidigBrukstillatelse_4399_2_14_SoknadsDelerAvTiltakenValideringOKTest()
        {
            // Følgende arbeider vil bli utført innen kan være maks om 14 dager	/MidlertidigBrukstillatelse/delsoeknad/harTilstrekkeligSikkerhet=false	Feil
            var val = new MidlertidigBrukstillatelseFormV2Validator();

            var midlertidigBrukstillatelse = new MidlertidigBrukstillatelseV2Builder()
                .HarTilstrekkeligSikkerhet(false, "Montert rekkverk på trapp", DateTime.Now.AddDays(10), DateTime.Now.AddDays(10))
                .Build();
            val.HarTilstrekkeligSikkerhetValidering(midlertidigBrukstillatelse);
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4399_2_14_SoknadsDelerAvTiltakenValideringErrorTest")]
        public void MidlertidigBrukstillatelse_4399_2_14_SoknadsDelerAvTiltakenValideringErrorTest()
        {
            // Følgende arbeider vil bli utført innen kan være maks om 14 dager	/MidlertidigBrukstillatelse/delsoeknad/harTilstrekkeligSikkerhet=false	Feil
            var val = new MidlertidigBrukstillatelseFormV2Validator();
            var midlertidigBrukstillatelse = new MidlertidigBrukstillatelseV2Builder()
                .HarTilstrekkeligSikkerhet(false, "Montert rekkverk på trapp", DateTime.Now.AddDays(15), DateTime.Now.AddDays(10))
                .Build();
            val.HarTilstrekkeligSikkerhetValidering(midlertidigBrukstillatelse);
            var result = val.GetResult();

            result.messages.Where(m => m.reference.Contains("4399.2.14")).Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4399_2_15_SoknadsDelerAvTiltakenValideringOKTest")]
        public void MidlertidigBrukstillatelse_4399_2_15_SoknadsDelerAvTiltakenValideringOKTest()
        {
            // Bekreftelse på at disse arbeidene er utført vil være kommunen i hende innen kan være maks om 14 dager	/MidlertidigBrukstillatelse/delsoeknad/harTilstrekkeligSikkerhet=false	Feil
            var val = new MidlertidigBrukstillatelseFormV2Validator();

            var midlertidigBrukstillatelse = new MidlertidigBrukstillatelseV2Builder()
                .HarTilstrekkeligSikkerhet(false, "Montert rekkverk på trapp", DateTime.Now.AddDays(10), DateTime.Now.AddDays(10))
                .Build();
            val.HarTilstrekkeligSikkerhetValidering(midlertidigBrukstillatelse);
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4399_2_15_SoknadsDelerAvTiltakenValideringErrorTest")]
        public void MidlertidigBrukstillatelse_4399_2_15_SoknadsDelerAvTiltakenValideringErrorTest()
        {
            // Bekreftelse på at disse arbeidene er utført vil være kommunen i hende innen kan være maks om 14 dager	/MidlertidigBrukstillatelse/delsoeknad/harTilstrekkeligSikkerhet=false	Feil
            var val = new MidlertidigBrukstillatelseFormV2Validator();

            var midlertidigBrukstillatelse = new MidlertidigBrukstillatelseV2Builder()
                .HarTilstrekkeligSikkerhet(false, "Montert rekkverk på trapp", DateTime.Now.AddDays(10), DateTime.Now.AddDays(15))
                .Build();
            val.HarTilstrekkeligSikkerhetValidering(midlertidigBrukstillatelse);
            var result = val.GetResult();

            result.messages.Where(m => m.reference.Contains("4399.2.15")).Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4399_2_17_EiendomByggestedValidationOKTest")]
        public void MidlertidigBrukstillatelse_4399_2_17_EiendomByggestedValidationOKTest()
        {
            // Hvis bruksenhetsnummer(bolignummer) er fylt ut må det følge riktig format (^[HKLU]\\d{4}$) (feks H0101) se https://www.kartverket.no/eiendom/adressering/bruksenhetsnummer/
            var val = new MidlertidigBrukstillatelseFormV2Validator();

            val.MunicipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");

            var midlertidigBrukstillatelse = new MidlertidigBrukstillatelseV2Builder()
                .EiendomType("2004", "1234", "4321", "0", "0", "Hammerfest", "H0101")
                .Build();
            val.EiendomByggestedValidation(midlertidigBrukstillatelse);
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4399_2_17_EiendomByggestedValidationErrorTest")]
        public void MidlertidigBrukstillatelse_4399_2_17_EiendomByggestedValidationErrorTest()
        {
            // Hvis bruksenhetsnummer(bolignummer) er fylt ut må det følge riktig format (^[HKLU]\\d{4}$) (feks H0101) se https://www.kartverket.no/eiendom/adressering/bruksenhetsnummer/
            var val = new MidlertidigBrukstillatelseFormV2Validator();
            val.MunicipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");

            var midlertidigBrukstillatelse = new MidlertidigBrukstillatelseV2Builder()
                .EiendomType("2004", "1234", "4321", "0", "0", "Hammerfest", "x0101")
                .Build();
            val.EiendomByggestedValidation(midlertidigBrukstillatelse);
            var result = val.GetResult();

            result.messages.Where(m => m.reference.Contains("4399.2.17")).Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4399_1_1EiendomByggestedValidationKnrOKTest")]
        public void MidlertidigBrukstillatelse_4399_1_1_EiendomByggestedValidationKnrOKTest()
        {
            var val = new MidlertidigBrukstillatelseFormV2Validator();
            val.MunicipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");

            var midlertidigBrukstillatelse = new MidlertidigBrukstillatelseV2Builder()
                .EiendomType("2004", "1234", "4321", "0", "0", "Hammerfest", "H0101")
                .Build();
            val.EiendomByggestedValidation(midlertidigBrukstillatelse);
            var result = val.GetResult();
            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4399_1_1_EiendomByggestedValidationKnrErrorTest")]
        public void MidlertidigBrukstillatelse_4399_1_1_EiendomByggestedValidationKnrErrorTest()
        {
            // kommunenummer bør fylles ut for eiendom/byggested", "4399.1.1", "/midlertidigBrukstillatelse/eiendomByggested/eiendom[0]/eiendomsidentifikasjon/kommunenummer
            var val = new MidlertidigBrukstillatelseFormV2Validator();
            //var municipalityValidator = new Mock<IMunicipalityValidator>();
            //municipalityValidator.Setup(s => s.Validate_kommunenummer(It.IsAny<string>())).Returns(new GeneralValidationResult() { Status = ValidationStatus.Fail, Message = "UnitTest" });
            //val.MunicipalityValidator = municipalityValidator.Object;
            val.MunicipalityValidator = TestHelper.MokMunicipalityValidatorResult("invalid");

            var midlertidigBrukstillatelse = new MidlertidigBrukstillatelseV2Builder()
                .EiendomType("11111", "1234", "4321", "0", "0", "Hammerfest", "H0101")
                .Build();
            val.EiendomByggestedValidation(midlertidigBrukstillatelse);
            var result = val.GetResult();
            result.messages.Where(m => m.reference.Contains("4399.1.1.1")).Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4399_2_16_kommunensSaksnummerValideringOKrTest")]
        public void MidlertidigBrukstillatelse_4399_2_16_kommunensSaksnummerValideringOKTest()
        {
            //Kommunens saksnummer må være fylt ut.	Feil
            var val = new MidlertidigBrukstillatelseFormV2Validator();

            var midlertidigBrukstillatelse = new MidlertidigBrukstillatelseV2Builder()
                .KommunensSaksnummer("2015", "124340")
                .Build();
            val.KommunensSaksnummerValidator(midlertidigBrukstillatelse);
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4399_2_16_kommunensSaksnummerValideringErrorTest")]
        public void MidlertidigBrukstillatelse_4399_2_16_kommunensSaksnummerValideringErrorTest()
        {
            //Kommunens saksnummer må være fylt ut.	Feil
            var val = new MidlertidigBrukstillatelseFormV2Validator();

            var midlertidigBrukstillatelse = new MidlertidigBrukstillatelseV2Builder()
                .KommunensSaksnummer(null, "124340")
                .Build();
            val.KommunensSaksnummerValidator(midlertidigBrukstillatelse);
            var result = val.GetResult();

            result.messages.Where(m => m.reference.Contains("4399.2.16")).Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "4399.1.1.4 SoekerValidering Ok")]
        public void SoekerValidering_1_1_4()
        {
            _validator.SoekerValidering(_form);
            var result = _validator.GetResult();
            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "4399.1.1.4 SoekerValidering Error")]
        public void Form_1_1_4_SoekerValidering_Error()
        {
            _form.ansvarligSoeker = null;
            _form.tiltakshaver = null;

            _validator.SoekerValidering(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4399.1.1.4").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        //Tiltakshaver
        [Fact(DisplayName = "4399.2.5.1 SoekerValidering Error")]
        public void Form_2_5_1_SoekerValidering_Error()
        {
            _form.ansvarligSoeker = null;
            _form.tiltakshaver.partstype =null;

            _validator.SoekerValidering(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4399.2.5.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4399.2.5.1.1 SoekerValidering Error")]
        public void Form_2_5_1_1_SoekerValidering_Error()
        {
            _form.ansvarligSoeker = null;
            _form.tiltakshaver.partstype.kodeverdi = "nokorar";

            _validator.SoekerValidering(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4399.2.5.1.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4399.2.5.2 SoekerValidering Error")]
        public void Form_2_5_2_SoekerValidering_Error()
        {
            _form.ansvarligSoeker = null;

            _form.tiltakshaver.partstype.kodeverdi = "Privatperson";
            _form.tiltakshaver.partstype.kodebeskrivelse = "Privatperson";

            _form.tiltakshaver.foedselsnummer = null;

            _validator.SoekerValidering(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4399.2.5.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4399.2.5.2.1 SoekerValidering Error")]
        public void Form_2_5_2_1_SoekerValidering_Error()
        {
            _form.ansvarligSoeker = null;
            _form.tiltakshaver.partstype.kodeverdi = "Privatperson";
            _form.tiltakshaver.partstype.kodebeskrivelse = "Privatperson";

            _form.tiltakshaver.foedselsnummer = "25525401530";

            _validator.SoekerValidering(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4399.2.5.2.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4399.2.5.2.2 SoekerValidering Error")]
        public void Form_2_5_2_2_SoekerValidering_Error()
        {
            _form.ansvarligSoeker = null;
            _form.tiltakshaver.partstype.kodeverdi = "Privatperson";
            _form.tiltakshaver.partstype.kodebeskrivelse = "Privatperson";

            _form.tiltakshaver.foedselsnummer = "5525401530";

            _validator.SoekerValidering(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4399.2.5.2.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4399.2.5.2.3 SoekerValidering Error")]
        public void Form_2_5_2_3_SoekerValidering_Error()
        {
            _form.ansvarligSoeker = null;
            _form.tiltakshaver.partstype.kodeverdi = "Privatperson";
            _form.tiltakshaver.partstype.kodebeskrivelse = "Privatperson";

            _form.tiltakshaver.foedselsnummer = "55254015301215jhjhguyguytyttrdtrt";

            _validator.SoekerValidering(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4399.2.5.2.3").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4399.2.5.3 SoekerValidering Error")]
        public void Form_2_5_3_SoekerValidering_Error()
        {
            _form.ansvarligSoeker = null;
            _form.tiltakshaver.partstype.kodeverdi = "Foretak";
            _form.tiltakshaver.partstype.kodebeskrivelse = "Foretak";

            _form.tiltakshaver.organisasjonsnummer = null;

            _validator.SoekerValidering(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4399.2.5.3").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4399.2.5.3.1 SoekerValidering Error")]
        public void Form_2_5_3_1_SoekerValidering_Error()
        {
            _form.ansvarligSoeker = null;
            _form.tiltakshaver.partstype.kodeverdi = "Foretak";
            _form.tiltakshaver.partstype.kodebeskrivelse = "Foretak";

            _form.tiltakshaver.organisasjonsnummer = "557476068";

            _validator.SoekerValidering(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4399.2.5.3.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4399.2.5.3.2 SoekerValidering Error")]
        public void Form_2_5_3_2_SoekerValidering_Error()
        {
            _form.ansvarligSoeker = null;
            _form.tiltakshaver.partstype.kodeverdi = "Foretak";
            _form.tiltakshaver.partstype.kodebeskrivelse = "Foretak";

            _form.tiltakshaver.organisasjonsnummer = "55974760673";

            _validator.SoekerValidering(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4399.2.5.3.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4399.2.5.3.3 SoekerValidering Error")]
        public void Form_2_5_3_3_SoekerValidering_Error()
        {
            _form.ansvarligSoeker = null;
            _form.tiltakshaver.partstype.kodeverdi = "Foretak";
            _form.tiltakshaver.partstype.kodebeskrivelse = "Foretak";

            _form.tiltakshaver.organisasjonsnummer = "910297937";
            _form.tiltakshaver.kontaktperson = null;

            _validator.SoekerValidering(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4399.2.5.3.3").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4399.2.5.4 SoekerValidering Error")]
        public void Form_2_5_4_SoekerValidering_Error()
        {
            _form.ansvarligSoeker = null;
            _form.tiltakshaver.adresse = null;

            _validator.SoekerValidering(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4399.2.5.4").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4399.2.5.5 SoekerValidering Error")]
        public void Form_2_5_5_SoekerValidering_Error()
        {
            _form.ansvarligSoeker = null;
            _form.tiltakshaver.telefonnummer = null;
            _form.tiltakshaver.mobilnummer = null;

            _validator.SoekerValidering(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4399.2.5.5").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        //Ansvarligsøker
        [Fact(DisplayName = "4399.1.1.5 SoekerValidering Error")]
        public void Form_1_1_5_SoekerValidering_Error()
        {
            _validator.PostalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "OSLO");

            _form.tiltakshaver = null;
            _form.ansvarligSoeker.partstype = null;

            _validator.SoekerValidering(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4399.1.1.5").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4399.1.2 SoekerValidering Error")]
        public void Form_1_2_SoekerValidering_Error()
        {
            _validator.PostalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "OSLO");

            _form.tiltakshaver = null;
            _form.ansvarligSoeker.partstype.kodeverdi = "*Foretak";
            _form.ansvarligSoeker.partstype.kodebeskrivelse = "Foretak";

            _validator.SoekerValidering(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4399.1.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        //Adresse
        [Fact(DisplayName = "4399.2.4.8 SoekerValidering Error")]
        public void Form_2_4_8_SoekerValidering_Error()
        {
            _validator.PostalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "OSLO");

            _form.tiltakshaver = null;
            _form.ansvarligSoeker.adresse = null;

            _validator.SoekerValidering(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4399.2.4.8").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4399.2.4.8.1 SoekerValidering Error")]
        public void Form_2_4_8_1_SoekerValidering_Error()
        {
            _validator.PostalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "OSLO");

            _form.tiltakshaver = null;
            _form.ansvarligSoeker.adresse.adresselinje1 = null;

            _validator.SoekerValidering(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4399.2.4.8.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4399.2.4.8.2 SoekerValidering Error")]
        public void Form_2_4_8_2_SoekerValidering_Error()
        {
            _validator.PostalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "OSLO");

            _form.tiltakshaver = null;
            _form.ansvarligSoeker.adresse.landkode = "*AR";

            _validator.SoekerValidering(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4399.2.4.8.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4399.2.4.11 SoekerValidering Error")]
        public void Form_2_4_11_SoekerValidering_Error()
        {
            _validator.PostalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "OSLO");

            _form.tiltakshaver = null;
            _form.ansvarligSoeker.adresse.postnr = "nokorar";

            _validator.SoekerValidering(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4399.2.4.11").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4399.2.4.10.1 SoekerValidering Error")]
        public void Form_2_4_10_1_SoekerValidering_Error()
        {
            _validator.PostalCodeProvider = TestHelper.MokPostalCodeProviderResult(false, "POBOX", "OSLO");

            _form.tiltakshaver = null;


            _validator.SoekerValidering(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4399.2.4.10.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }


        [Fact(DisplayName = "4399.2.4.5 SoekerValidering Error")]
        public void Form_2_4_5_SoekerValidering_Error()
        {
            _validator.PostalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "OSLO");

            _form.tiltakshaver = null;
            _form.ansvarligSoeker.partstype.kodeverdi = "Foretak";
            _form.ansvarligSoeker.partstype.kodebeskrivelse = "Foretak";

            _form.ansvarligSoeker.organisasjonsnummer = null;

            _validator.SoekerValidering(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4399.2.4.5").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4399.2.4.6 SoekerValidering Error")]
        public void Form_2_4_6_SoekerValidering_Error()
        {
            _validator.PostalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "OSLO");

            _form.tiltakshaver = null;
            _form.ansvarligSoeker.partstype.kodeverdi = "Foretak";
            _form.ansvarligSoeker.partstype.kodebeskrivelse = "Foretak";

            _form.ansvarligSoeker.organisasjonsnummer = "55974760673";

            _validator.SoekerValidering(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4399.2.4.6").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "4399.2.4.7 SoekerValidering Error")]
        public void Form_2_4_7_SoekerValidering_Error()
        {
            _validator.PostalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "OSLO");

            _form.tiltakshaver = null;
            _form.ansvarligSoeker.partstype.kodeverdi = "Foretak";
            _form.ansvarligSoeker.partstype.kodebeskrivelse = "Foretak";

            _form.ansvarligSoeker.organisasjonsnummer = "557476068";

            _validator.SoekerValidering(_form);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "4399.2.4.7").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

    }
}