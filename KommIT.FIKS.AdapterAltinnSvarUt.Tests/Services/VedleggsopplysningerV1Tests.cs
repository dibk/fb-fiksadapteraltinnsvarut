﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Castle.Components.DictionaryAdapter;
using FluentAssertions;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.SvarUtForm;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.SvarUtForm.SubForm;
using KommIT.FIKS.AdapterAltinnSvarUt.Tests.Services.Builders;
using Moq;
using no.kxml.skjema.dibk.vedlegg;
using Xunit;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Tests.Services
{
    public class VedleggsopplysningerV1Tests
    {
        [Fact(DisplayName = "5058_1_5_VedleggstypeErTegningOkTest")]
        public void Vedleggsopplysninger_5058_1_5_VedleggstypeErTegningOkTest()
        {
            // Når vedleggstype er tegninger bør tegningsnr og tegningsdato fylles ut.	/vedleggsopplysninger/vedlegg/vedleggstype = TegningEksisterendePlan, TegningNyPlan, TegningEksisterendeSnitt, TegningNyttSnitt, TegningEksisterendeFasade, TegningNyFasade	Advarsel
            var val = new VedleggsopplysningerV1Validator();

            var vedlegg = new VedleggsopplysningerV1Builder()
                .Vedlegg(null,"TegningEksisterendePlan", DateTime.Today, "1234")
                .Build();
            val.Vedleggstype(vedlegg);
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "5058_1_5_VedleggstypeErTegningErrorTest")]
        public void Vedleggsopplysninger_5058_1_5_VedleggstypeErTegningErrorTest()
        {
            // Når vedleggstype er tegninger bør tegningsnr og tegningsdato fylles ut.	/vedleggsopplysninger/vedlegg/vedleggstype = TegningEksisterendePlan, TegningNyPlan, TegningEksisterendeSnitt, TegningNyttSnitt, TegningEksisterendeFasade, TegningNyFasade	Advarsel
            var val = new VedleggsopplysningerV1Validator();

            var vedlegg = new VedleggsopplysningerV1Builder()
                .Vedlegg(null,"TegningEksisterendePlan", DateTime.Today)
                .Build();
            val.Vedleggstype(vedlegg);
            var result = val.GetResult();

            result.messages.Where(m => m.reference.Contains("5058.1.5")).Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "5058_1_4_ByggesaksBimValideringOkTest")]
        public void Rammetillatelse_5058_1_4_ByggesaksBimValideringOkTest()
        {
            // Når vedleggstype er ByggesaksBIM bør ifcValidationId fylles ut.	/vedleggsopplysninger/vedlegg/vedleggstype = ByggesaksBIM	Advarsel
            var val = new VedleggsopplysningerV1Validator();

            var vedlegg = new VedleggsopplysningerV1Builder()
                .Vedlegg("F9C118E6-6F19-45A8-995B-07F6413A037C", "ByggesaksBIM")
                .Build();
            val.Vedleggstype(vedlegg);
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = "5058_1_4_ByggesaksBimValideringErrorTest")]
        public void Rammetillatelse_5058_1_4_ByggesaksBimValideringErrorTest()
        {
            // Når vedleggstype er ByggesaksBIM bør ifcValidationId fylles ut.	/vedleggsopplysninger/vedlegg/vedleggstype = ByggesaksBIM	Advarsel
            var val = new VedleggsopplysningerV1Validator();

            var vedlegg = new VedleggsopplysningerV1Builder()
                .Vedlegg(null,"ByggesaksBIM")
                .Build();
            val.Vedleggstype(vedlegg);
            var result = val.GetResult();

            result.messages.Where(m => m.reference.Contains("5058.1.4")).Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
    }
}
