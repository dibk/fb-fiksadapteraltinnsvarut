using EntityFramework.MoqHelper;
using FluentAssertions;
using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Models.UserAccess;
using KommIT.FIKS.AdapterAltinnSvarUt.Services;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Altinn;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Authentication;
using Moq;
using System.Collections.Generic;
using Xunit;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Tests.Services
{
    public class MunicipalityServiceTest
    {
        private List<Municipality> GetData()
        {
            return new List<Municipality>
            {
                new Municipality { Name = "B� i Telemark", Code = "0821", AdministratedByUsers = new List<ApplicationUser>() {new ApplicationUser() {Id = "2"} } },
                new Municipality { Name = "Sauherad", Code = "0822", AdministratedByUsers = new List<ApplicationUser>() {new ApplicationUser() {Id = "2"} } },
                new Municipality { Name = "Nome", Code = "0819", AdministratedByUsers = new List<ApplicationUser>() {new ApplicationUser() {Id = "3"} } },
            };
        }

        [Fact]
        public void ShouldReturnFalseWhenMunicipalityIsNotAdministratedByUser()
        {
            var userId = "3";

            var municipalityService = new MunicipalityService(SetupDbContext(), SetupAuthorization(userId, false), null);

            bool result = municipalityService.UserHasAccessToMunicipality("0822", userId);
            result.Should().BeFalse();
        }

        [Fact]
        public void ShouldReturnTrueWhenMunicipalityIsAdministratedByUser()
        {
            var userId = "2";

            var municipalityService = new MunicipalityService(SetupDbContext(), SetupAuthorization(userId, false), null);

            bool result = municipalityService.UserHasAccessToMunicipality("0822", userId);
            result.Should().BeTrue();
        }

        [Fact]
        public void AdministratorShouldHaveAccessToAllMunicipalities()
        {
            var userId = "1";

            var municipalityService = new MunicipalityService(SetupDbContext(), SetupAuthorization(userId, true), null);

            bool result = municipalityService.UserHasAccessToMunicipality("0822", userId);
            result.Should().BeTrue();
        }

        //[Fact]
        //public void AttachmentsSorted()
        //{
        //    var formData = new FormData("AR77777", "<ns4:EttTrinn xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:sawsdl=\"http://www.w3.org/ns/sawsdl\" xmlns:fiks=\"http://skjema.kxml.no/metadata\" xmlns:seres=\"http://seres.no/xsd/forvaltningsdata\" xmlns:dibk=\"http://skjema.kxml.no/dibk/igangsettingstillatelse/0.9\" xmlns:ns1=\"http://skjema.kxml.no/dibk/gjennomforingsplan/0.9\" xmlns:tns=\"http://tempuri.org/\" xmlns:q1=\"http://schemas.datacontract.org/2004/07/DIBK.FBygg.Altinn.MapperCodelist\" xmlns:ns2=\"http://schemas.microsoft.com/2003/10/Serialization/\" xmlns:ns3=\"http://skjema.kxml.no/dibk/rammesoknad/0.9\" xmlns:ns4=\"http://skjema.kxml.no/dibk/etttrinn/0.9\" dataFormatProvider=\"SERES\" dataFormatId=\"5501\" dataFormatVersion=\"41792\"><ns4:fraSluttbrukersystem>FTB konsesjon #234</ns4:fraSluttbrukersystem><ns4:tiltakshaver><ns4:partstype><ns4:kodeverdi>Privatperson</ns4:kodeverdi><ns4:kodebeskrivelse>Privatperson</ns4:kodebeskrivelse></ns4:partstype><ns4:foedselsnummer>08021612345</ns4:foedselsnummer><ns4:navn>Hans Hansen</ns4:navn><ns4:adresse><ns4:adresselinje1>Storgata 5</ns4:adresselinje1><ns4:postnr>7003</ns4:postnr><ns4:poststed>Trondheim</ns4:poststed></ns4:adresse><ns4:telefonnummer>11223344</ns4:telefonnummer><ns4:mobilnummer>99887766</ns4:mobilnummer><ns4:epost>hanshansen@bmail.no</ns4:epost><ns4:signaturdato>2016-07-28</ns4:signaturdato></ns4:tiltakshaver><ns4:eiendomByggested><ns4:eiendom><ns4:eiendomsidentifikasjon><ns4:kommunenummer>1516</ns4:kommunenummer><ns4:gaardsnummer>187</ns4:gaardsnummer><ns4:bruksnummer>6</ns4:bruksnummer><ns4:festenummer>0</ns4:festenummer><ns4:seksjonsnummer>0</ns4:seksjonsnummer></ns4:eiendomsidentifikasjon><ns4:adresse><ns4:adresselinje1>Nygate 33</ns4:adresselinje1><ns4:postnr>3825</ns4:postnr><ns4:poststed>Lunde</ns4:poststed></ns4:adresse><ns4:bygningsnummer>1234567896</ns4:bygningsnummer><ns4:bolignummer>H0101</ns4:bolignummer></ns4:eiendom></ns4:eiendomByggested><ns4:rammebetingelser><ns4:adkomst><ns4:nyeEndretAdkomst>true</ns4:nyeEndretAdkomst><ns4:vegtype><ns4:kodeverdi>PrivatVeg</ns4:kodeverdi><ns4:kodebeskrivelse>PrivatVeg</ns4:kodebeskrivelse></ns4:vegtype><ns4:erTillatelseGittRiksFylkesveg>true</ns4:erTillatelseGittRiksFylkesveg><ns4:erTillatelseGittKommunalVeg>false</ns4:erTillatelseGittKommunalVeg><ns4:erTillatelseGittPrivatVeg>false</ns4:erTillatelseGittPrivatVeg></ns4:adkomst><ns4:arealdisponering><ns4:tomtearealByggeomraade>1000.5</ns4:tomtearealByggeomraade><ns4:gjeldendePlan><ns4:utnyttingsgrad>23</ns4:utnyttingsgrad><ns4:plantype><ns4:kodeverdi>RP</ns4:kodeverdi><ns4:kodebeskrivelse>Reguleringsplan</ns4:kodebeskrivelse></ns4:plantype><ns4:navn>Naustgrendanabbevannet</ns4:navn><ns4:formaal>Fritidsbegyggelse</ns4:formaal><ns4:andreRelevanteKrav>andre relevante krav her</ns4:andreRelevanteKrav><ns4:beregningsregelGradAvUtnytting><ns4:kodeverdi>%BYA</ns4:kodeverdi><ns4:kodebeskrivelse>Prosent bebygd areal</ns4:kodebeskrivelse></ns4:beregningsregelGradAvUtnytting></ns4:gjeldendePlan><ns4:tomtearealSomTrekkesFra>30.2</ns4:tomtearealSomTrekkesFra><ns4:tomtearealSomLeggesTil>50.3</ns4:tomtearealSomLeggesTil><ns4:tomtearealBeregnet>1020</ns4:tomtearealBeregnet><ns4:beregnetMaksByggeareal>300.2</ns4:beregnetMaksByggeareal><ns4:arealBebyggelseEksisterende>200.4</ns4:arealBebyggelseEksisterende><ns4:arealBebyggelseSomSkalRives>10.6</ns4:arealBebyggelseSomSkalRives><ns4:arealBebyggelseNytt>30.5</ns4:arealBebyggelseNytt><ns4:parkeringsarealTerreng>18</ns4:parkeringsarealTerreng><ns4:arealSumByggesak>240.5</ns4:arealSumByggesak><ns4:beregnetGradAvUtnytting>23</ns4:beregnetGradAvUtnytting></ns4:arealdisponering><ns4:generelleVilkaar><ns4:oppfyllesVilkaarFor3Ukersfrist>true</ns4:oppfyllesVilkaarFor3Ukersfrist><ns4:beroererTidligere1850>false</ns4:beroererTidligere1850><ns4:forhaandskonferanseAvholdt>true</ns4:forhaandskonferanseAvholdt><ns4:paalagtUavhengigKontroll>true</ns4:paalagtUavhengigKontroll><ns4:beroererArbeidsplasser>true</ns4:beroererArbeidsplasser><ns4:utarbeideAvfallsplan>false</ns4:utarbeideAvfallsplan><ns4:behovForTillatelse>true</ns4:behovForTillatelse></ns4:generelleVilkaar><ns4:bygningsopplysninger><ns4:arealverdier><ns4:arealBYA><ns4:arealEksisterendebebyggelse>200.3</ns4:arealEksisterendebebyggelse><ns4:arealNyBebyggelse>30.2</ns4:arealNyBebyggelse><ns4:aapneArealer>60.4</ns4:aapneArealer><ns4:sumAreal>290.7</ns4:sumAreal></ns4:arealBYA><ns4:arealBoligBRA><ns4:arealEksisterendebebyggelse>300.3</ns4:arealEksisterendebebyggelse><ns4:arealNyBebyggelse>30.2</ns4:arealNyBebyggelse><ns4:aapneArealer>60.4</ns4:aapneArealer><ns4:sumAreal>390.7</ns4:sumAreal></ns4:arealBoligBRA><ns4:arealAnnetBRA><ns4:arealEksisterendebebyggelse>400.3</ns4:arealEksisterendebebyggelse><ns4:arealNyBebyggelse>30.2</ns4:arealNyBebyggelse><ns4:aapneArealer>60.4</ns4:aapneArealer><ns4:sumAreal>490.7</ns4:sumAreal></ns4:arealAnnetBRA><ns4:arealIaltBRA><ns4:arealEksisterendebebyggelse>500.3</ns4:arealEksisterendebebyggelse><ns4:arealNyBebyggelse>30.2</ns4:arealNyBebyggelse><ns4:aapneArealer>60.4</ns4:aapneArealer><ns4:sumAreal>590.7</ns4:sumAreal></ns4:arealIaltBRA></ns4:arealverdier><ns4:antallEtasjer>2</ns4:antallEtasjer><ns4:antallBruksenheter>8</ns4:antallBruksenheter><ns4:antallBruksenheterEksisterende>2</ns4:antallBruksenheterEksisterende><ns4:antallBruksenheterNye>1</ns4:antallBruksenheterNye><ns4:antallBruksenheterBolig>4</ns4:antallBruksenheterBolig><ns4:antallBruksenheterBoligEksisterende>3</ns4:antallBruksenheterBoligEksisterende><ns4:antallBruksenheterBoligNye>1</ns4:antallBruksenheterBoligNye><ns4:antallBruksenheterAnnet>2</ns4:antallBruksenheterAnnet><ns4:antallBruksenheterAnnetEksisterende>1</ns4:antallBruksenheterAnnetEksisterende><ns4:antallBruksenheterAnnetNye>1</ns4:antallBruksenheterAnnetNye></ns4:bygningsopplysninger><ns4:vannforsyning><ns4:tilknytningstype><ns4:kodeverdi>Annen privat vannforsyning, innlagt vann</ns4:kodeverdi><ns4:kodebeskrivelse>Annen privat vannforsyning, innlagt vann</ns4:kodebeskrivelse></ns4:tilknytningstype><ns4:beskrivelse>Felles privat vannforsyning i hele reguleringsomr�det</ns4:beskrivelse><ns4:krysserVannforsyningAnnensGrunn>true</ns4:krysserVannforsyningAnnensGrunn><ns4:tinglystErklaering>true</ns4:tinglystErklaering></ns4:vannforsyning><ns4:avloep><ns4:tilknytningstype><ns4:kodeverdi>Offentlig avl�psanlegg</ns4:kodeverdi><ns4:kodebeskrivelse>Offentlig avl�psanlegg</ns4:kodebeskrivelse></ns4:tilknytningstype><ns4:installereVannklosett>true</ns4:installereVannklosett><ns4:utslippstillatelse>true</ns4:utslippstillatelse><ns4:krysserAvloepAnnensGrunn>true</ns4:krysserAvloepAnnensGrunn><ns4:tinglystErklaering>true</ns4:tinglystErklaering><ns4:overvannTerreng>false</ns4:overvannTerreng><ns4:overvannAvloepssystem>true</ns4:overvannAvloepssystem></ns4:avloep><ns4:kravTilByggegrunn><ns4:flomutsattOmraade>true</ns4:flomutsattOmraade><ns4:f1>false</ns4:f1><ns4:f2>false</ns4:f2><ns4:f3>false</ns4:f3><ns4:skredutsattOmraade>false</ns4:skredutsattOmraade><ns4:s1>false</ns4:s1><ns4:s2>false</ns4:s2><ns4:s3>false</ns4:s3><ns4:miljoeforhold>false</ns4:miljoeforhold></ns4:kravTilByggegrunn><ns4:loefteinnretninger><ns4:erLoefteinnretningIBygning>true</ns4:erLoefteinnretningIBygning><ns4:planleggesLoefteinnretningIBygning>true</ns4:planleggesLoefteinnretningIBygning><ns4:planleggesHeis>true</ns4:planleggesHeis><ns4:planleggesTrappeheis>false</ns4:planleggesTrappeheis><ns4:planleggesRulletrapp>false</ns4:planleggesRulletrapp><ns4:planleggesLoefteplattform>false</ns4:planleggesLoefteplattform></ns4:loefteinnretninger><ns4:plassering><ns4:konfliktHoeyspentkraftlinje>false</ns4:konfliktHoeyspentkraftlinje><ns4:konfliktVannOgAvloep>false</ns4:konfliktVannOgAvloep><ns4:minsteAvstandTilAnnenBygning>100.2</ns4:minsteAvstandTilAnnenBygning><ns4:minsteAvstandNabogrense>200.4</ns4:minsteAvstandNabogrense><ns4:minsteAvstandTilMidtenAvVei>40.2</ns4:minsteAvstandTilMidtenAvVei></ns4:plassering></ns4:rammebetingelser><ns4:beskrivelseAvTiltak><ns4:tiltak><ns4:bruk><ns4:anleggstype><ns4:kodeverdi>andre</ns4:kodeverdi><ns4:kodebeskrivelse>andre</ns4:kodebeskrivelse></ns4:anleggstype><ns4:naeringsgruppe><ns4:kodeverdi>Y</ns4:kodeverdi><ns4:kodebeskrivelse>N�ringsgruppe for annet som ikke er n�ring</ns4:kodebeskrivelse></ns4:naeringsgruppe><ns4:bygningstype><ns4:kodeverdi>161</ns4:kodeverdi><ns4:kodebeskrivelse>Hytter, sommerhus ol fritidsbygning</ns4:kodebeskrivelse></ns4:bygningstype><ns4:tiltaksformaal><ns4:kodeverdi>Fritidsbolig</ns4:kodeverdi><ns4:kodebeskrivelse>Fritidsbolig</ns4:kodebeskrivelse></ns4:tiltaksformaal><ns4:tiltaksformaal><ns4:kodeverdi>Annet</ns4:kodeverdi><ns4:kodebeskrivelse>Annet</ns4:kodebeskrivelse></ns4:tiltaksformaal><ns4:beskrivPlanlagtFormaal>Ny fritidsbolig med laftet anneks</ns4:beskrivPlanlagtFormaal></ns4:bruk><ns4:type><ns4:kodeverdi>nyttbyggboligformal</ns4:kodeverdi><ns4:kodebeskrivelse>Nytt bygg - Boligform�l</ns4:kodebeskrivelse></ns4:type><ns4:foelgebrev>Det s�kes om bygging av en ny fritidsbolig og et laftet lite anneks. Annekset skal bygges f�rst og benyttes som \"arbeidsbrakke\" mens hovedhytta bygges.</ns4:foelgebrev></ns4:tiltak></ns4:beskrivelseAvTiltak><ns4:dispensasjon><ns4:dispensasjonstype><ns4:kodeverdi>TEK</ns4:kodeverdi><ns4:kodebeskrivelse>Byggteknisk forskrift med veiledning</ns4:kodebeskrivelse></ns4:dispensasjonstype><ns4:begrunnelse>Begrunnelse for dispensasjon</ns4:begrunnelse></ns4:dispensasjon><ns4:signatur><ns4:signaturdato>2017-03-27T08:49:59</ns4:signaturdato><ns4:signertAv>FILIP  MOHAMED p� vegne av Siv. Ark. Fjell og S�nner</ns4:signertAv></ns4:signatur><ns4:soekesOmDispensasjon>true</ns4:soekesOmDispensasjon><ns4:ansvarligSoeker><ns4:partstype><ns4:kodeverdi>Foretak</ns4:kodeverdi><ns4:kodebeskrivelse>Foretak</ns4:kodebeskrivelse></ns4:partstype><ns4:organisasjonsnummer>974760673</ns4:organisasjonsnummer><ns4:navn>Siv. Ark. Fjell og S�nner</ns4:navn><ns4:adresse><ns4:adresselinje1>Lillegata 5</ns4:adresselinje1><ns4:postnr>7003</ns4:postnr><ns4:poststed>Trondheim</ns4:poststed></ns4:adresse><ns4:telefonnummer>11223344</ns4:telefonnummer><ns4:mobilnummer>99887766</ns4:mobilnummer><ns4:epost>post@fjell.no</ns4:epost><ns4:kontaktperson>Benjamin Fjell</ns4:kontaktperson><ns4:signaturdato>2016-07-26</ns4:signaturdato></ns4:ansvarligSoeker></ns4:EttTrinn>");
        //    formData.MainFormPdf = CreateMainFormAttachment();
        //    formData.Attachments = CreateAttachmentSample();
        //    formData.Subforms = CreateSubFormList();
        //    formData.SvarUtAttachmentSettings = CreateAttachmentSettingsSample();
        //    var sort = new SvarUtAttachmentSorter8().GenerateOrderedArrayOfDocsAndAttachments(formData);
        //    foreach (var item in sort)
        //        System.Diagnostics.Debug.WriteLine(item.SvarUtDokument.filnavn);
        //}

        //private List<SvarUtAttachmentSetting> CreateAttachmentSettingsSample()
        //{
        //    List<SvarUtAttachmentSetting> attSettings = new List<SvarUtAttachmentSetting>();
        //    attSettings.Add(new SvarUtAttachmentSetting { AttatchmentTypeName = "Avkjoerselsplan", DocumentNumber = 1, DocumentType = "ANKO", DocumentStatus = "F", TilknyttetRegistreringSom = "V" });
        //    attSettings.Add(new SvarUtAttachmentSetting { AttatchmentTypeName = "Situasjonsplan", DocumentNumber = 2, DocumentType = "ANKO", DocumentStatus = "F", TilknyttetRegistreringSom = "V" });
        //    return attSettings;
        //}

        private static IAuthorizationService SetupAuthorization(string userId, bool isAdmin)
        {
            var authorizationService = new Mock<IAuthorizationService>();
            authorizationService.Setup(a => a.UserHasAdministratorRole(userId)).Returns(isAdmin);
            return authorizationService.Object;
        }

        private ApplicationDbContext SetupDbContext()
        {
            var mockSet = EntityFrameworkMoqHelper.CreateMockForDbSet<Municipality>().SetupForQueryOn(GetData());
            var mockContext = EntityFrameworkMoqHelper.CreateMockForDbContext<ApplicationDbContext, Municipality>(mockSet);
            return mockContext.Object;
        }

        private static List<Attachment> CreateAttachmentSample()
        {
            var attachments = new List<Attachment>();
            attachments.Add(new Attachment("AR45345", new byte[1], "application_xml", "Avkjoerselsplan", "filnavn.xml", false));
            attachments.Add(new Attachment("AR45345", new byte[1], "application_xml", "Avkjoerselsplan", "filnavn2.xml", false));
            attachments.Add(new Attachment("AR45345", new byte[1], "application_xml", "Situasjonsplan", "filnavn2.pdf", false));
            return attachments;
        }

        private static Attachment CreateMainFormAttachment()
        {
            return new Attachment("AR45345", new byte[1], "application_pdf", "main form pdf", "Skjema.pdf", false);
        }

        private static List<Subform> CreateSubFormList()
        {
            return new List<Subform>();
        }
    }
}