using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.FormsSamples;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.SvarUtForm;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Validations;
using Moq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using no.kxml.skjema.dibk.etttrinnsoknadV3;
using Xunit;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Tests.Services.SamplesTests
{
    public class EttTrinnV3Dfv45751Tests
    {
        [Fact(DisplayName = "EttTrinn v3 01", Skip = "test data")]
        public void EttTrinnV3Dfv45751_01_test()
        {


            var formSkjema = new EttTrinnV3Dfv45751()._01EttTrinnV3(true);
            var xmlWithoutSpaces = TestHelper.GetXmlWithoutSpaces(formSkjema.Data);


            var attachments = new List<string>() { "Situasjonsplan", "TegningNyPlan", "TegningNyttSnitt", "TegningEksisterendeFasade",
                "Gjennomføringsplan", "Matrikkelopplysninger", "UttalelseKulturminnemyndighet", "Avkjoerselsplan","ErklaeringAnsvarsrett" };

            var form = TestHelper.GetValue<EttTrinnType>(formSkjema);
            var saveXmlFile = TestHelper.SaveSchema2XmlFile(formSkjema, $"Ett Trinn V3 - {form.beskrivelseAvTiltak.type[0].kodeverdi}");

            //Get json for Validation API v2
            var validateFormv2 = TestHelper.GetValidateFormv2FormSkjema(formSkjema, attachments);

            var val = new EtttrinnsoknadFormV3Validator();
            val._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "Oslo");

            val._municipalityValidator = TestHelper.MokMunicipalityValidatorResult("OK");
            val._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, true);



            val.Validate(form, attachments);
            var result = val.GetResult();

            var list = result.messages.Select(m => m.reference).ToList();
            var noko = JArray.Parse(JsonConvert.SerializeObject(list));

            var rulesChecked = TestHelper.ConverRulesCheckedJson(result, false);

            TestHelper.AssertResult(result, null, 0, null, true);
        }
        [Fact(DisplayName = "EttTrinn v3 01", Skip = "test data")]
        public void EttTrinnV3Dfv45751test()
        {
            var formSkjema = new EttTrinnV3Dfv45751()._01EttTrinnV3(true);
            var form = TestHelper.GetValue<EttTrinnType>(formSkjema);

            var attachments = new List<string>() { "Situasjonsplan", "TegningNyPlan", "TegningNyttSnitt", "TegningEksisterendeFasade",
                "Gjennomføringsplan", "Matrikkelopplysninger", "UttalelseKulturminnemyndighet", "Avkjoerselsplan","ErklaeringAnsvarsrett" };
            SortedDictionary<string, string> tiltakstypes = new SortedDictionary<string, string>()
            {
                {"antenneover5m","Antennesystem med høyde over 5m"},
                {"fasade","Endring av bygg - utvendig - Fasade"},
                {"skiltreklamefasadestor","Skilt/reklame større enn 6,5 m2 - fasade"},
                {"tilbyggover50m2","Endring av bygg - utvendig - Tilbygg med samlet areal større enn 50 m2"},
                {"pabygg","Endring av bygg - utvendig - Påbygg"},
                {"nyttbyggboligformal","Nytt bygg - Boligformål"},
                {"endringanlegg","Endring av anlegg"},
                {"rivinghelebygg","Riving av hele bygg"}
            };

            var allsaved = new List<bool>();
            foreach (var tiltakstype in tiltakstypes)
            {
                form.beskrivelseAvTiltak.type[0].kodeverdi = tiltakstype.Key;
                form.beskrivelseAvTiltak.type[0].kodebeskrivelse = tiltakstype.Value;
                var saveXmlFile = TestHelper.SaveSchema2XmlFile(formSkjema, $"Ett trinn V3 - {form.beskrivelseAvTiltak.type[0].kodeverdi}");
                var xmlWithoutSpaces = TestHelper.GetXmlWithoutSpaces(formSkjema.Data);
                var validationV2ApiJson = TestHelper.GetValidateFormv2FormSkjema(formSkjema, attachments, form);
                allsaved.Add(saveXmlFile);
            }

            allsaved.Any(s => !s).Should().Be(false);
        }
        [Fact(DisplayName = "EttTrinn v3 01",Skip = "test data")]
        public void EttTrinnV3Dfv45751_03_test()
        {
            var formSkjema = new EttTrinnV3Dfv45751()._03EttTrinnV3(true);
            var xmlWithoutSpaces = TestHelper.GetXmlWithoutSpaces(formSkjema.Data);

            var attachments = new List<string>() { "Gjennomføringsplan", "Avkjoerselsplan", "TegningNyttSnitt", "Matrikkelopplysninger", "Situasjonsplan", "TegningEksisterendePlan", "TegningEksisterendeFasade", "ErklaeringAnsvarsrett" };

            var form = TestHelper.GetValue<EttTrinnType>(formSkjema);
            var saveXmlFile = TestHelper.SaveSchema2XmlFile(formSkjema, $"Ett Trinn V3 - {form.beskrivelseAvTiltak.type[0].kodeverdi}");

            //Get json for Validation API v2
            var validateFormv2 = TestHelper.GetValidateFormv2FormSkjema(formSkjema, attachments);

            var val = new EtttrinnsoknadFormV3Validator();
            val._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "Oslo");

            val._municipalityValidator = TestHelper.MokMunicipalityValidatorResult("OK");
            val._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, true);



            val.Validate(form, attachments);
            var result = val.GetResult();

            var list = result.messages.Select(m => m.reference).ToList();
            var noko = JArray.Parse(JsonConvert.SerializeObject(list));

            var rulesChecked = TestHelper.ConverRulesCheckedJson(result, false);

            TestHelper.AssertResult(result, null, 0, null, true);
        }
    }
}
