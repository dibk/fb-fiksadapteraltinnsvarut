﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.Serialization;
using FluentAssertions;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.FormsSamples;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.SampleData;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.SvarUtForm;
using KommIT.FIKS.AdapterAltinnSvarUt.Tests.Services.Builders;
using no.kxml.skjema.dibk.endringavtillatelse;
using Xunit;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Tests.Services.SamplesTests
{
    public class EndrindAvTillatelseDfv42350Tests
    {
        [Fact(DisplayName = " Endring Av Tillatelse - Plassering", Skip = "disable_not working in teamcity")]
        public void EndringAvTillatelse_EndringAvTillatelseAvPlasseringTest()
        {
            var val = new EndringAvTillatelseFormValidator();
            var formSkjema = new EndringAvTillatelseDfV42350().EndringAvTillatelseAvPlassering(true);
            var endringAvTillatelse = GetEndringAvTillatelseType(formSkjema);

            val.Validate(endringAvTillatelse, new List<string>() { "Nabovarsel", "KvitteringNabovarsel", "Matrikkelopplysninger" });
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = " Endring Av Tillatelse - Areal", Skip = "disable_not working in teamcity")]
        public void EndringAvTillatelse_EndringAvTillatelseAvArealTest()
        {
            var val = new EndringAvTillatelseFormValidator();
            var formSkjema = new EndringAvTillatelseDfV42350().EndringAvTillatelseAvAreal( true);
            var endringAvTillatelse = GetEndringAvTillatelseType(formSkjema);

            val.Validate(endringAvTillatelse, new List<string>() { "Nabovarsel", "Matrikkelopplysninger", "KvitteringNabovarsel", "TegningNyPlan", "Folgebrev" });
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = " Endring Av Tillatelse - Bruk", Skip = "disable_not working in teamcity")]
        public void EndringAvTillatelse_EndringAvTillatelseAvBrukTest()
        {
            var val = new EndringAvTillatelseFormValidator();
            var formSkjema = new EndringAvTillatelseDfV42350().EndringAvTillatelseAvBruk( true);
            var endringAvTillatelse = GetEndringAvTillatelseType(formSkjema);

            val.Validate(endringAvTillatelse, new List<string>() { "Nabovarsel", "KvitteringNabovarsel", "Situasjonsplan", "TegningNyPlan", "Folgebrev", "Matrikkelopplysninger" });
            var result = val.GetResult();

            result.messages.Count.Should().Be(0);
        }
        [Fact(DisplayName = " Endring Av Tillatelse - Som kreves Dispensasjon", Skip = "disable_not working in teamcity")]
        public void EndringAvTillatelse_EndringAvTillatelseSomKrevesDispensasjonTest()
        {
            var val = new EndringAvTillatelseFormValidator();
            var formSkjema = new EndringAvTillatelseDfV42350().EndringAvTillatelseSomKreverDispensasjon(true);
            var endringAvTillatelse = GetEndringAvTillatelseType(formSkjema);

            val.Validate(endringAvTillatelse, new List<string>() { "Nabovarsel", "Situasjonsplan", "TegningNyPlan", "Folgebrev", "Matrikkelopplysninger" });
            var result = val.GetResult();

            result.messages.Count.Should().Be(1);
        }

        private static EndringAvTillatelseType GetEndringAvTillatelseType(Skjema formSkjema)
        {
            var xmlSkjema = formSkjema.Data;
            XmlSerializer serializer = new XmlSerializer(typeof(EndringAvTillatelseType));
            var stringReader = new StringReader(xmlSkjema);
            var endringAvTillatelse = (EndringAvTillatelseType) serializer.Deserialize(stringReader);
            
            // Save Xml Example
            //var path = Path.Combine(@"C:\Temp\", $"{formSkjema.VariantId}.xml");
            //File.WriteAllText(path,xmlSkjema);

            return endringAvTillatelse;
        }
    }
}
