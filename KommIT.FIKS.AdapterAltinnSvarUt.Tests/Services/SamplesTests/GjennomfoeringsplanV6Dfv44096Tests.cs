using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.FormsSamples;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.SvarUtForm;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Validations;
using Moq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using no.kxml.skjema.dibk.gjennomforingsplanV6;
using Xunit;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Tests.Services.SamplesTests
{
    public class GjennomfoeringsplanV6Dfv44096Tests
    {
        [Fact(DisplayName = "Test Data", Skip = "test data")]
        public void GjennomfoeringsplanV6Dfv44096_01_Test()
        {



            var formSkjema = new GjennomfoeringsplanV6Dfv44096()._01Gjennomfoeringsplan(true);

            var xmlWithoutSpaces = TestHelper.GetXmlWithoutSpaces(formSkjema.Data);

            var attachments = new List<string>() { "Annet" };

            //Get json for Validation API v2
            var validateFormv2 = TestHelper.GetValidateFormv2FormSkjema(formSkjema, attachments);
            var jObjectValidateFormv2 = JObject.FromObject(validateFormv2);

            var val = new GjennomforingsplanFormV6Validator();
            var form = TestHelper.GetValue<GjennomfoeringsplanType>(formSkjema);

            val._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "FJERDINGBY");
            val._municipalityValidator = TestHelper.MokMunicipalityValidatorResult("OK");
            val._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, true);

            val.Validate(form, attachments);
            var result = val.GetResult();

            var list = result.messages.Select(m => m.reference).ToList();
            var noko = JArray.Parse(JsonConvert.SerializeObject(list));

            var rulesChecked = TestHelper.ConverRulesCheckedJson(result, false);

            TestHelper.AssertResult(result, null, 0, null, true);

        }
    }
}