using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Linq;
using FluentAssertions;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.FormsSamples;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.SvarUtForm;
using Xunit;
using no.kxml.skjema.dibk.tiltakutenansvarsrettV3;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Tests.Services.SamplesTests
{
    public class TiltakUtenAnsvarsrettV3Dfv45753Test
    {
        [Fact(DisplayName = "Test Data", Skip = "test data")]
        public void TiltakUtenAnsvarsrettV3Dfv45753_01_Test()
        {
          
            var val = new TiltakUtenAnsvarsrettFormV3Validator();
            val._municipalityValidator = TestHelper.MokMunicipalityValidatorResult("OK");
            val._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, true);
            var formSkjema = new TiltakUtenAnsvarsrettV3Dfv45753()._01TiltakUtenAnsvarsrett(true);
            
            //var savexml = TestHelper.SaveSchema2XmlFile(formSkjema);
            var xmlWithoutSpaces = TestHelper.GetXmlWithoutSpaces(formSkjema.Data);

            var attachments = new List<string>()
            {
                "Situasjonsplan", "TegningEksisterendePlan","TegningNyPlan", "Nabomerknader", "Gjenpart nabovarsel", 
                "KvitteringNabovarsel", "TegningEksisterendeSnitt","TegningNyttSnitt", "TegningEksisterendeFasade", "TegningNyFasade"
            };

            //Get json for Validation API v2
            var validateFormv2 = TestHelper.GetValidateFormv2FormSkjema(formSkjema, attachments);

            var tiltakUtenAnsvarsrettType = TestHelper.GetValue<TiltakUtenAnsvarsrettType>(formSkjema);

            val.Validate(tiltakUtenAnsvarsrettType, attachments);
            var result = val.GetResult();


            var list = result.messages.Select(m => m.reference).ToList();
            var noko = JArray.Parse(JsonConvert.SerializeObject(list));

            var rulesChecked = TestHelper.ConverRulesCheckedJson(result);

            TestHelper.AssertResult(result, null, 0, null, true);

        }

        [Fact(DisplayName = "Test Data", Skip = "test data")]
        public void TiltakUtenAnsvarsrettV3Dfv45753()
        {
            var formSkjema = new TiltakUtenAnsvarsrettV3Dfv45753()._01TiltakUtenAnsvarsrett(true);
            var attachments = new List<string>()
            {
                "Situasjonsplan", "TegningEksisterendePlan","TegningNyPlan", "Nabomerknader", "Gjenpart nabovarsel",
                "KvitteringNabovarsel", "TegningEksisterendeSnitt","TegningNyttSnitt", "TegningEksisterendeFasade", "TegningNyFasade"
            };

            var form = TestHelper.GetValue<TiltakUtenAnsvarsrettType>(formSkjema);
            SortedDictionary<string, string> tiltakstypes = new SortedDictionary<string, string>()
            {
                    {"antenneover5m","Antennesystem med h�yde over 5m"},
                    {"fasade","Endring av bygg - utvendig - Fasade"},
                    {"skiltreklamefasadestor","Skilt/reklame st�rre enn 6,5 m2 - fasade"},
                    {"tilbyggover50m2","Endring av bygg - utvendig - Tilbygg med samlet areal st�rre enn 50 m2"},
                    {"pabygg","Endring av bygg - utvendig - P�bygg"},
                    {"nyttbyggboligformal","Nytt bygg - Boligform�l"},
                    {"endringanlegg","Endring av anlegg"},
                    {"rivinghelebygg","Riving av hele bygg"}
                };

            var allsaved = new List<bool>();
            foreach (var tiltakstype in tiltakstypes)
            {
                form.beskrivelseAvTiltak.type.type.kodeverdi = tiltakstype.Key;
                form.beskrivelseAvTiltak.type.type.kodebeskrivelse = tiltakstype.Value;
                var saveXmlFile = TestHelper.SaveSchema2XmlFile(formSkjema, $"Tiltak uten ansvar V3 - {form.beskrivelseAvTiltak.type.type.kodeverdi}");
                var xmlWithoutSpaces = TestHelper.GetXmlWithoutSpaces(formSkjema.Data);
                var validationV2ApiJson = TestHelper.GetValidateFormv2FormSkjema(formSkjema, attachments, form);
                allsaved.Add(saveXmlFile);
            }

            allsaved.Any(s => !s).Should().Be(false);
        }
    }
}
