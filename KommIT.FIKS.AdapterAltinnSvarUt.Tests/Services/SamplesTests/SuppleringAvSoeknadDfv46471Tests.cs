using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.FormsSamples;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.SvarUtForm;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using no.kxml.skjema.dibk.suppleringAvSoknad;
using Xunit;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Tests.Services.SamplesTests
{
    public class SuppleringAvSoeknadDfv46471Tests
    {
        private SuppleringFormValidator _validator;
        private List<string> _formSetElements;


        public SuppleringAvSoeknadDfv46471Tests()
        {
            _validator = new SuppleringFormValidator();
            _validator._municipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");
            _validator._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, true);
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "T", "Trondheim");

            _formSetElements = new List<string>() { "Gjennomføringsplan", "Matrikkelopplysninger" };
        }
        [Fact(Skip = "sample data")]
        public void TestSampledata()
        {
            var skjema = new SuppleringAvSoeknadDfv46471()._01SuppleringAvSoknad(true);
            var form = TestHelper.GetValue<SuppleringAvSoeknadType>(skjema);

            _validator.Validate(form, _formSetElements);
            var result = _validator.GetResult();

            var list = result.messages.Select(m => m.reference).ToList();

            var noko = JArray.Parse(JsonConvert.SerializeObject(list));

        }
        [Fact(Skip = "sample data")]
        public void TestXmlSampledata()
        {
            var suppleringSoknadXmlSkjema = new SuppleringAvSoeknadDfv46471().SuppleringSoknadXmlFiles(true);

            var fileName = "SuppleringAvSoeknad_Dfv46471_EttersendingUlikeVarianter";

            var skjema = suppleringSoknadXmlSkjema.FirstOrDefault(e => e.VariantId.Equals(fileName, StringComparison.CurrentCultureIgnoreCase));

            var form = TestHelper.GetValue<SuppleringAvSoeknadType>(skjema);

            _validator.Validate(form, _formSetElements);
            
            var result = _validator.GetResult();

            var list = result.messages.Select(m => m.reference).ToList();

            var listOfMessageErrors = result.messages.Select(m => string.Concat(m.reference," - ",m.message)).ToArray();
            var noko = JArray.Parse(JsonConvert.SerializeObject(list));
            var messageErrors = JArray.Parse(JsonConvert.SerializeObject(listOfMessageErrors));

            var rulesChecked = TestHelper.ConverRulesCheckedJson(result, false);

        }
        [Fact()]
        public void TestXmlSampledata_AllWorck()
        {
            var suppleringSoknadXmlSkjema = new SuppleringAvSoeknadDfv46471().SuppleringSoknadXmlFiles(true);

            List<SuppleringAvSoeknadType> suppleringAvSoeknadTypes = new List<SuppleringAvSoeknadType>();

            Dictionary<string, JArray> skjemaValidationResults = new Dictionary<string, JArray>();
            
            foreach (var skjema1 in suppleringSoknadXmlSkjema)
            {
                var form = TestHelper.GetValue<SuppleringAvSoeknadType>(skjema1);
                suppleringAvSoeknadTypes.Add(form);

                _validator.Validate(form, _formSetElements);
                var result = _validator.GetResult();

                var listOfMessageErrors = result.messages.Select(m => string.Concat(m.reference, " - ",m.messagetype," - ", m.message)).ToArray();
                var messageErrors = JArray.Parse(JsonConvert.SerializeObject(listOfMessageErrors));
                
                skjemaValidationResults.Add(skjema1.VariantId, messageErrors);
            }
            var jsonObjectValidationResults = JObject.FromObject(skjemaValidationResults);

            if (suppleringAvSoeknadTypes.Count == suppleringSoknadXmlSkjema.Count)
            {
                
            }

            suppleringAvSoeknadTypes.Count.Should().Be(suppleringSoknadXmlSkjema.Count);
        }
    }
}
