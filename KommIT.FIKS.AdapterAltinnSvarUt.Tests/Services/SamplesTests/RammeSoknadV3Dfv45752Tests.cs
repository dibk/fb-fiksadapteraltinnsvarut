using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.SvarUtForm;
using Xunit;
using no.kxml.skjema.dibk.rammesoknadV3;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.FormsSamples;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Tests.Services.SamplesTests
{
    public class RammeSoknadV3Dfv45752Tests
    {
        [Fact(DisplayName = "Test Data", Skip = "Test data")]
        public void RammeSoknadV3Dfv45752_01_Test()
        {
           
            
            var formSkjema = new RammeSoknadV3Dfv45752()._01RammeSoknadV3Test(true);
            var xmlWithoutSpaces = TestHelper.GetXmlWithoutSpaces(formSkjema.Data);


            var attachments = new List<string>() { "Situasjonsplan", "TegningNyPlan", "TegningNyttSnitt", "TegningEksisterendeFasade",
                "Gjennomføringsplan", "Matrikkelopplysninger", "UttalelseKulturminnemyndighet", "Avkjoerselsplan","ErklaeringAnsvarsrett" };

            //Get json for Validation API v2
            var validateFormv2 = TestHelper.GetValidateFormv2FormSkjema(formSkjema, attachments);
            var jObjectValidateFormv2 = JObject.FromObject(validateFormv2);


            var val = new RammesoknadFormV3Validator();

            val._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "Oslo");

            val._municipalityValidator = TestHelper.MokMunicipalityValidatorResult("OK");
            val._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, true);

            var rammetillatelseType = TestHelper.GetValue<RammetillatelseType>(formSkjema);
            val.Validate(rammetillatelseType, attachments);
            var result = val.GetResult();

            var list = result.messages.Select(m => m.reference).ToList();
            var noko = JArray.Parse(JsonConvert.SerializeObject(list));

            var rulesChecked = TestHelper.ConverRulesCheckedJson(result, false);

            TestHelper.AssertResult(result, null, 0, null, true);

        }
        
        [Fact(DisplayName = "Test Data", Skip = "test data")]
        public void RammeSoknadV3Dfv45752()
        {
            var formSkjema = new RammeSoknadV3Dfv45752()._01RammeSoknadV3Test(true);
            var form = TestHelper.GetValue<RammetillatelseType>(formSkjema);

            var attachments = new List<string>() { "Situasjonsplan", "TegningNyPlan", "TegningNyttSnitt", "TegningEksisterendeFasade",
                "Gjennomføringsplan", "Matrikkelopplysninger", "UttalelseKulturminnemyndighet", "Avkjoerselsplan" };

            SortedDictionary<string, string> tiltakstypes = new SortedDictionary<string, string>()
            {
                {"antenneover5m","Antennesystem med høyde over 5m"},
                {"fasade","Endring av bygg - utvendig - Fasade"},
                {"skiltreklamefasadestor","Skilt/reklame større enn 6,5 m2 - fasade"},
                {"tilbyggover50m2","Endring av bygg - utvendig - Tilbygg med samlet areal større enn 50 m2"},
                {"pabygg","Endring av bygg - utvendig - Påbygg"},
                {"nyttbyggboligformal","Nytt bygg - Boligformål"},
                {"endringanlegg","Endring av anlegg"},
                {"rivinghelebygg","Riving av hele bygg"}
            };

            var allsaved = new List<bool>();
            foreach (var tiltakstype in tiltakstypes)
            {
                form.beskrivelseAvTiltak.type[0].kodeverdi = tiltakstype.Key;
                form.beskrivelseAvTiltak.type[0].kodebeskrivelse = tiltakstype.Value;
                var saveXmlFile = TestHelper.SaveSchema2XmlFile(formSkjema, $"Ramme soknad V3 - {form.beskrivelseAvTiltak.type[0].kodeverdi}");
                var xmlWithoutSpaces = TestHelper.GetXmlWithoutSpaces(formSkjema.Data);
                var validationV2ApiJson = TestHelper.GetValidateFormv2FormSkjema(formSkjema, attachments, form);
                allsaved.Add(saveXmlFile);
            }

            allsaved.Any(s => !s).Should().Be(false);

        }

        [Fact(DisplayName = "Test Data", Skip = "test data")]
        public void RammeSoknadV3Dfv45752_02_Test()
        {

            var classSkjemaMethods = SampleService.GetClassSkjemaMethods(new RammeSoknadV3Dfv45752(), true);

            var skjemasFromXmlFiles = new RammeSoknadV3Dfv45752().RammeSoknadXmlFiles(true);
            var formSkjema = skjemasFromXmlFiles.FirstOrDefault();

           
            var xmlWithoutSpaces = TestHelper.GetXmlWithoutSpaces(formSkjema?.Data);

            var attachments = new List<string>() { "Situasjonsplan", "TegningNyPlan", "TegningNyttSnitt", "TegningEksisterendeFasade",
                "Gjennomføringsplan", "Matrikkelopplysninger", "UttalelseKulturminnemyndighet", "Avkjoerselsplan","ErklaeringAnsvarsrett" };

            //Get json for Validation API v2
            var validateFormv2 = TestHelper.GetValidateFormv2FormSkjema(formSkjema, attachments);
            var jObjectValidateFormv2 = JObject.FromObject(validateFormv2);


            var val = new RammesoknadFormV3Validator();

            val._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "POBOX", "Oslo");

            val._municipalityValidator = TestHelper.MokMunicipalityValidatorResult("OK");
            val._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, true);

            var rammetillatelseType = TestHelper.GetValue<RammetillatelseType>(formSkjema);
            val.Validate(rammetillatelseType, attachments);
            var result = val.GetResult();

            var list = result.messages.Select(m => m.reference).ToList();
            var noko = JArray.Parse(JsonConvert.SerializeObject(list));

            var rulesChecked = TestHelper.ConverRulesCheckedJson(result, false);

            TestHelper.AssertResult(result, null, 0, null, true);

        }

    }

}
