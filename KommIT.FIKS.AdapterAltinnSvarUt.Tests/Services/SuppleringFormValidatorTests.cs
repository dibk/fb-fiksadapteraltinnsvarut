using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.FormsSamples;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.SvarUtForm;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;
using no.kxml.skjema.dibk.suppleringAvSoknad;
using Xunit;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Tests.Services
{
    public class SuppleringFormValidatorTests
    {
        private SuppleringAvSoeknadType _form;
        private SuppleringFormValidator _validator;
        private List<string> _formSetElements;
        private Skjema _skjema;
        public SuppleringFormValidatorTests()
        {
            _validator = new SuppleringFormValidator();
            _validator._municipalityValidator = TestHelper.MokMunicipalityValidatorResult("ok");
            _validator._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, true);
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "T", "Oslo");

            //var skjemasFromXmlFiles = new SuppleringAvSoeknadDfv46471().SuppleringSoknadXmlFiles(true);
            //var formSkjema = skjemasFromXmlFiles.FirstOrDefault();
            //_form = SerializeUtil.DeserializeFromString<SuppleringAvSoeknadType>(formSkjema.Data);

            _skjema = new SuppleringAvSoeknadDfv46471()._01SuppleringAvSoknad(true);
            _form = SerializeUtil.DeserializeFromString<SuppleringAvSoeknadType>(_skjema.Data);
            _formSetElements = new List<string>() { "Gjennomføringsplan", "Matrikkelopplysninger" };


        }
        [Fact(DisplayName = "Test Without Mock", Skip = "uMock")]
        public void TestWithoutMock()
        {
            var validator = new SuppleringFormValidator();
            validator.EiendomByggestedValidation(_form);

            var result = validator.GetResult();

            result.messages.Count.Should().Be(0);
        }

        [Fact(DisplayName = "5721.1.4 EiendomByggestedValidation Error")]
        public void EiendomByggestedValidation_1_4()
        {
            var postmanTestData = TestHelper.GetValidateFormv2FormSkjema(_skjema, _formSetElements);

            _form.eiendomByggested = null;
            _validator.EiendomByggestedValidation(_form);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.4").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.4.1 EiendomByggestedValidation Error")]
        public void EiendomByggestedValidation_1_4_1()
        {
            _validator._municipalityValidator = TestHelper.MokMunicipalityValidatorResult("empty");
            _form.eiendomByggested[0].eiendomsidentifikasjon.kommunenummer = null;
            _validator.EiendomByggestedValidation(_form);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.4.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.4.2 EiendomByggestedValidation Error")]
        public void EiendomByggestedValidation_1_4_2()
        {
            _validator._municipalityValidator = TestHelper.MokMunicipalityValidatorResult("Invalid");
            _validator.EiendomByggestedValidation(_form);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.4.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.4.3 EiendomByggestedValidation Error")]
        public void EiendomByggestedValidation_1_4_3()
        {
            _validator._municipalityValidator = TestHelper.MokMunicipalityValidatorResult("Expired");
            _validator.EiendomByggestedValidation(_form);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.4.3").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.4.4 EiendomByggestedValidation Error")]
        public void EiendomByggestedValidation_1_4_4()
        {

            _form.eiendomByggested[0].eiendomsidentifikasjon.gaardsnummer = null;
            _validator.EiendomByggestedValidation(_form);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.4.4").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.4.4.1 EiendomByggestedValidation Error")]
        public void EiendomByggestedValidation_1_4_4_1()
        {

            _form.eiendomByggested[0].eiendomsidentifikasjon.bruksnummer = null;
            _validator.EiendomByggestedValidation(_form);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.4.4.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.4.5 EiendomByggestedValidation Error")]
        public void EiendomByggestedValidation_1_4_5()
        {

            _form.eiendomByggested[0].eiendomsidentifikasjon.gaardsnummer = "-1";
            _validator.EiendomByggestedValidation(_form);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.4.5").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.4.6 EiendomByggestedValidation Error")]
        public void EiendomByggestedValidation_1_4_6()
        {

            _form.eiendomByggested[0].eiendomsidentifikasjon.bruksnummer = "-1";
            _validator.EiendomByggestedValidation(_form);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.4.6").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.4.12 EiendomByggestedValidation Error")]
        public void EiendomByggestedValidation_1_4_12()
        {

            _validator._matrikkelService = TestHelper.MokMatrikkelServiceResult(false, true, true);
            _validator.EiendomByggestedValidation(_form);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.4.12").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.4.13 EiendomByggestedValidation Error")]
        public void EiendomByggestedValidation_1_4_13()
        {

            _validator._matrikkelService = TestHelper.MokMatrikkelServiceResult(null, true, true);
            _validator.EiendomByggestedValidation(_form);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.4.13").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.4.7 EiendomByggestedValidation Error")]
        public void EiendomByggestedValidation_1_4_7()
        {
            _form.eiendomByggested[0].bygningsnummer = "noko";
            _validator.EiendomByggestedValidation(_form);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.4.7").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.4.8 EiendomByggestedValidation Error")]
        public void EiendomByggestedValidation_1_4_8()
        {
            _form.eiendomByggested[0].bygningsnummer = "-1";
            _validator.EiendomByggestedValidation(_form);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.4.8").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.4.14 EiendomByggestedValidation Error")]
        public void EiendomByggestedValidation_1_4_14()
        {

            _validator._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, false, true);
            _form.eiendomByggested[0].bygningsnummer = "12358";
            _validator.EiendomByggestedValidation(_form);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.4.14").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.4.9 EiendomByggestedValidation Error")]
        public void EiendomByggestedValidation_1_4_9()
        {
            _form.eiendomByggested[0].adresse = null;
            _validator.EiendomByggestedValidation(_form);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.4.9").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.4.9.1 EiendomByggestedValidation Error")]
        public void EiendomByggestedValidation_1_4_9_1()
        {
            _form.eiendomByggested[0].adresse.adresselinje1 = null;
            _validator.EiendomByggestedValidation(_form);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.4.9.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.4.9.2 EiendomByggestedValidation Error")]
        public void EiendomByggestedValidation_1_4_9_2()
        {
            _form.eiendomByggested[0].adresse.gatenavn = null;
            _validator.EiendomByggestedValidation(_form);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.4.9.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.4.15 EiendomByggestedValidation Error")]
        public void EiendomByggestedValidation_1_4_15()
        {

            _validator._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, null);
            _validator.EiendomByggestedValidation(_form);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.4.15").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.4.15.1 EiendomByggestedValidation Error")]
        public void EiendomByggestedValidation_1_4_15_1()
        {

            _validator._matrikkelService = TestHelper.MokMatrikkelServiceResult(true, true, false);
            _validator.EiendomByggestedValidation(_form);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.4.15.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.4.10 EiendomByggestedValidation Error")]
        public void EiendomByggestedValidation_1_4_10()
        {
            _form.eiendomByggested[0].kommunenavn = null;
            _validator.EiendomByggestedValidation(_form);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.4.10").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.4.11 EiendomByggestedValidation Error")]
        public void EiendomByggestedValidation_1_4_11()
        {
            _form.eiendomByggested[0].bolignummer = "w7652";
            _validator.EiendomByggestedValidation(_form);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.4.11").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "5721.1.5 SoekerValidering Error")]
        public void SoekerValidering_1_5()
        {
            _form.ansvarligSoeker = null;
            _form.tiltakshaver = null;
            _validator.SoekerValidering(_form);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.5").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.5.1 SoekerValidering Error")]
        public void SoekerValidering_1_5_1()
        {
            _form.tiltakshaver = null;

            _form.ansvarligSoeker.partstype = null;

            _validator.SoekerValidering(_form);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.5.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.5.2 SoekerValidering Error")]
        public void SoekerValidering_1_5_2()
        {
            _form.tiltakshaver = null;

            _form.ansvarligSoeker.partstype.kodeverdi = "*Privatperson";
            _form.ansvarligSoeker.partstype.kodebeskrivelse = "Privatperson";

            _validator.SoekerValidering(_form);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.5.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.5.3 SoekerValidering Error")]
        public void SoekerValidering_1_5_3()
        {
            _form.tiltakshaver = null;

            _form.ansvarligSoeker.partstype.kodeverdi = "Privatperson";
            _form.ansvarligSoeker.partstype.kodebeskrivelse = "Privatperson";

            _form.ansvarligSoeker.foedselsnummer = null;

            _validator.SoekerValidering(_form);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.5.3").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.5.3.1 SoekerValidering Error")]
        public void SoekerValidering_1_5_3_1()
        {
            _form.tiltakshaver = null;

            _form.ansvarligSoeker.partstype.kodeverdi = "Privatperson";
            _form.ansvarligSoeker.partstype.kodebeskrivelse = "Privatperson";

            _form.ansvarligSoeker.foedselsnummer = "25525401530";

            _validator.SoekerValidering(_form);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.5.3.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.5.3.2 SoekerValidering Error")]
        public void SoekerValidering_1_5_3_2()
        {
            _form.tiltakshaver = null;

            _form.ansvarligSoeker.partstype.kodeverdi = "Privatperson";
            _form.ansvarligSoeker.partstype.kodebeskrivelse = "Privatperson";

            _form.ansvarligSoeker.foedselsnummer = "5125401530";

            _validator.SoekerValidering(_form);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.5.3.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.5.3.3 SoekerValidering Error")]
        public void SoekerValidering_1_5_3_3()
        {
            _form.tiltakshaver = null;

            _form.ansvarligSoeker.partstype.kodeverdi = "Privatperson";
            _form.ansvarligSoeker.partstype.kodebeskrivelse = "Privatperson";

            _form.ansvarligSoeker.foedselsnummer = "asdfasdgbkjnq234235dfbdf6er566c3553tbz13dfbtyt0890+";

            _validator.SoekerValidering(_form);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.5.3.3").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.5.4 SoekerValidering Error")]
        public void SoekerValidering_1_5_4()
        {
            _form.tiltakshaver = null;

            _form.ansvarligSoeker.partstype.kodeverdi = "Foretak";
            _form.ansvarligSoeker.partstype.kodebeskrivelse = "Foretak";

            _form.ansvarligSoeker.organisasjonsnummer = null;

            _validator.SoekerValidering(_form);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.5.4").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.5.4.1 SoekerValidering Error")]
        public void SoekerValidering_1_5_4_1()
        {
            _form.tiltakshaver = null;

            _form.ansvarligSoeker.partstype.kodeverdi = "Foretak";
            _form.ansvarligSoeker.partstype.kodebeskrivelse = "Foretak";

            _form.ansvarligSoeker.organisasjonsnummer = "55974760673";


            _validator.SoekerValidering(_form);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.5.4.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.5.4.2 SoekerValidering Error")]
        public void SoekerValidering_1_5_4_2()
        {
            _form.tiltakshaver = null;

            _form.ansvarligSoeker.partstype.kodeverdi = "Foretak";
            _form.ansvarligSoeker.partstype.kodebeskrivelse = "Foretak";

            _form.ansvarligSoeker.organisasjonsnummer = "557476068";

            _validator.SoekerValidering(_form);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.5.4.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.5.5.9 SoekerValidering Error")]
        public void SoekerValidering_1_5_5_9()
        {
            _form.tiltakshaver = null;

            _form.ansvarligSoeker.partstype.kodeverdi = "Foretak";
            _form.ansvarligSoeker.partstype.kodebeskrivelse = "Foretak";

            _form.ansvarligSoeker.navn = null;

            _validator.SoekerValidering(_form);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.5.5.9").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.5.5.10 SoekerValidering Error")]
        public void SoekerValidering_1_5_5_10()
        {
            _form.tiltakshaver = null;

            _form.ansvarligSoeker.partstype.kodeverdi = "Foretak";
            _form.ansvarligSoeker.partstype.kodebeskrivelse = "Foretak";

            _form.ansvarligSoeker.epost = null;

            _validator.SoekerValidering(_form);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.5.5.10").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.5.4.3 SoekerValidering Error")]
        public void SoekerValidering_1_5_4_3()
        {
            _form.tiltakshaver = null;

            _form.ansvarligSoeker.partstype.kodeverdi = "Foretak";
            _form.ansvarligSoeker.partstype.kodebeskrivelse = "Foretak";

            _form.ansvarligSoeker.kontaktperson = null;

            _validator.SoekerValidering(_form);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.5.4.3").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.5.4.3.1 SoekerValidering Error")]
        public void SoekerValidering_1_5_4_3_1()
        {
            _form.tiltakshaver = null;

            _form.ansvarligSoeker.partstype.kodeverdi = "Foretak";
            _form.ansvarligSoeker.partstype.kodebeskrivelse = "Foretak";

            _form.ansvarligSoeker.kontaktperson.navn = null;

            _validator.SoekerValidering(_form);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.5.4.3.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.5.4.3.2 SoekerValidering Error")]
        public void SoekerValidering_1_5_4_3_2()
        {
            _form.tiltakshaver = null;

            _form.ansvarligSoeker.partstype.kodeverdi = "Foretak";
            _form.ansvarligSoeker.partstype.kodebeskrivelse = "Foretak";

            _form.ansvarligSoeker.kontaktperson.telefonnummer = null;
            _form.ansvarligSoeker.kontaktperson.mobilnummer = null;

            _validator.SoekerValidering(_form);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.5.4.3.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.5.4.3.3 SoekerValidering Error")]
        public void SoekerValidering_1_5_4_3_3()
        {
            _form.tiltakshaver = null;

            _form.ansvarligSoeker.partstype.kodeverdi = "Foretak";
            _form.ansvarligSoeker.partstype.kodebeskrivelse = "Foretak";

            _form.ansvarligSoeker.kontaktperson.telefonnummer = "-555";

            _validator.SoekerValidering(_form);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.5.4.3.3").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.5.4.3.4 SoekerValidering Error")]
        public void SoekerValidering_1_5_4_3_4()
        {
            _form.tiltakshaver = null;

            _form.ansvarligSoeker.partstype.kodeverdi = "Foretak";
            _form.ansvarligSoeker.partstype.kodebeskrivelse = "Foretak";

            _form.ansvarligSoeker.kontaktperson.mobilnummer = "-555";

            _validator.SoekerValidering(_form);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.5.4.3.4").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.5.4.3.5 SoekerValidering Error")]
        public void SoekerValidering_1_5_4_3_5()
        {
            _form.tiltakshaver = null;

            _form.ansvarligSoeker.partstype.kodeverdi = "Foretak";
            _form.ansvarligSoeker.partstype.kodebeskrivelse = "Foretak";

            _form.ansvarligSoeker.kontaktperson.epost = null;

            _validator.SoekerValidering(_form);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.5.4.3.5").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.5.5 SoekerValidering Error")]
        public void SoekerValidering_1_5_5()
        {
            _form.tiltakshaver = null;

            _form.ansvarligSoeker.adresse = null;

            _validator.SoekerValidering(_form);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.5.5").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.5.5.1 SoekerValidering Error")]
        public void SoekerValidering_1_5_5_1()
        {
            _form.tiltakshaver = null;

            _form.ansvarligSoeker.adresse.adresselinje1 = null;

            _validator.SoekerValidering(_form);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.5.5.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.5.5.2 SoekerValidering Error")]
        public void SoekerValidering_1_5_5_2()
        {
            _form.tiltakshaver = null;

            _form.ansvarligSoeker.adresse.landkode = "*AR";

            _validator.SoekerValidering(_form);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.5.5.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.5.5.3 SoekerValidering Error")]
        public void SoekerValidering_1_5_5_3()
        {
            _form.tiltakshaver = null;

            _form.ansvarligSoeker.adresse.postnr = null;

            _validator.SoekerValidering(_form);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.5.5.3").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.5.5.4 SoekerValidering Error")]
        public void SoekerValidering_1_5_5_4()
        {
            _form.tiltakshaver = null;

            _form.ansvarligSoeker.adresse.postnr = "000";

            _validator.SoekerValidering(_form);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.5.5.4").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.5.5.5 SoekerValidering Error")]
        public void SoekerValidering_1_5_5_5()
        {
            _form.tiltakshaver = null;

            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(false, "T", "Frong");

            _validator.SoekerValidering(_form);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.5.5.5").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.5.5.6 SoekerValidering Error")]
        public void SoekerValidering_1_5_5_6()
        {
            _form.tiltakshaver = null;

            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "T", "Bø");

            _validator.SoekerValidering(_form);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.5.5.6").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.5.5.7 SoekerValidering Error")]
        public void SoekerValidering_1_5_5_7()
        {
            _form.tiltakshaver = null;

            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(null, null, null);

            _validator.SoekerValidering(_form);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.5.5.7").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.5.5.8 SoekerValidering Error")]
        public void SoekerValidering_1_5_5_8()
        {
            _form.tiltakshaver = null;
            _form.ansvarligSoeker.telefonnummer = null;
            _form.ansvarligSoeker.mobilnummer = null;
            _validator.SoekerValidering(_form);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.5.5.8").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.5.5.8.1 SoekerValidering Error")]
        public void SoekerValidering_1_5_5_8_1()
        {
            _form.tiltakshaver = null;
            _form.ansvarligSoeker.telefonnummer = "-555";

            _validator.SoekerValidering(_form);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.5.5.8.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.5.5.8.2 SoekerValidering Error")]
        public void SoekerValidering_1_5_5_8_2()
        {
            _form.tiltakshaver = null;
            _form.ansvarligSoeker.mobilnummer = "-555";

            _validator.SoekerValidering(_form);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.5.5.8.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.5.5.8.10 SoekerValidering Error")]
        public void SoekerValidering_1_5_5_8_10()
        {
            _form.tiltakshaver = null;
            _form.ansvarligSoeker.epost = null;

            _validator.SoekerValidering(_form);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.5.5.10").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.5.6 SoekerValidering Error")]
        public void SoekerValidering_1_5_6()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "T", "Bø i Telemark");
            _form.ansvarligSoeker = null;

            _form.tiltakshaver.partstype = null;

            _validator.SoekerValidering(_form);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.5.6").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.5.6.1 SoekerValidering Error")]
        public void SoekerValidering_1_5_2_1()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "T", "Bø i Telemark");
            _form.ansvarligSoeker = null;

            _form.tiltakshaver.partstype.kodeverdi = "*Privatperson";
            _form.tiltakshaver.partstype.kodebeskrivelse = "Privatperson";

            _validator.SoekerValidering(_form);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.5.6.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.5.6.2 SoekerValidering Error")]
        public void SoekerValidering_1_5_6_2()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "T", "Bø i Telemark");
            _form.ansvarligSoeker = null;

            _form.tiltakshaver.partstype.kodeverdi = "Privatperson";
            _form.tiltakshaver.partstype.kodebeskrivelse = "Privatperson";

            _form.tiltakshaver.foedselsnummer = null;

            _validator.SoekerValidering(_form);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.5.6.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.5.6.2.1 SoekerValidering Error")]
        public void SoekerValidering_1_5_6_2_1()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "T", "Bø i Telemark");
            _form.ansvarligSoeker = null;

            _form.tiltakshaver.partstype.kodeverdi = "Privatperson";
            _form.tiltakshaver.partstype.kodebeskrivelse = "Privatperson";

            _form.tiltakshaver.foedselsnummer = "25525401530";

            _validator.SoekerValidering(_form);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.5.6.2.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.5.6.2.2 SoekerValidering Error")]
        public void SoekerValidering_1_5_2_2_2()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "T", "Bø i Telemark");
            _form.ansvarligSoeker = null;

            _form.tiltakshaver.partstype.kodeverdi = "Privatperson";
            _form.tiltakshaver.partstype.kodebeskrivelse = "Privatperson";

            _form.tiltakshaver.foedselsnummer = "5125401530";

            _validator.SoekerValidering(_form);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.5.6.2.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.5.6.2.3 SoekerValidering Error")]
        public void SoekerValidering_1_5_6_2_3()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "T", "Bø i Telemark");
            _form.ansvarligSoeker = null;

            _form.tiltakshaver.partstype.kodeverdi = "Privatperson";
            _form.tiltakshaver.partstype.kodebeskrivelse = "Privatperson";

            _form.tiltakshaver.foedselsnummer = "asdfasdgbkjnq234235dfbdf6er566c3553tbz13dfbtyt0890+";

            _validator.SoekerValidering(_form);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.5.6.2.3").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.5.6.3 SoekerValidering Error")]
        public void SoekerValidering_1_5_6_3()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "T", "Bø i Telemark");
            _form.ansvarligSoeker = null;

            _form.tiltakshaver.partstype.kodeverdi = "Foretak";
            _form.tiltakshaver.partstype.kodebeskrivelse = "Foretak";

            _form.tiltakshaver.organisasjonsnummer = null;

            _validator.SoekerValidering(_form);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.5.6.3").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.5.6.3.1 SoekerValidering Error")]
        public void SoekerValidering_1_5_6_3_1()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "T", "Bø i Telemark");
            _form.ansvarligSoeker = null;

            _form.tiltakshaver.partstype.kodeverdi = "Foretak";
            _form.tiltakshaver.partstype.kodebeskrivelse = "Foretak";

            _form.tiltakshaver.organisasjonsnummer = "557476068";

            _validator.SoekerValidering(_form);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.5.6.3.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.5.6.3.2 SoekerValidering Error")]
        public void SoekerValidering_1_5_6_3_2()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "T", "Bø i Telemark");
            _form.ansvarligSoeker = null;

            _form.tiltakshaver.partstype.kodeverdi = "Foretak";
            _form.tiltakshaver.partstype.kodebeskrivelse = "Foretak";
            _form.tiltakshaver.organisasjonsnummer = "55974760673";


            _validator.SoekerValidering(_form);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.5.6.3.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.5.6.3.3 SoekerValidering Error")]
        public void SoekerValidering_1_5_6_3_3()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "T", "Bø i Telemark");
            _form.ansvarligSoeker = null;

            _form.tiltakshaver.partstype.kodeverdi = "Foretak";
            _form.tiltakshaver.partstype.kodebeskrivelse = "Foretak";
            _form.tiltakshaver.organisasjonsnummer = "910748548";

            _form.tiltakshaver.kontaktperson = null;

            _validator.SoekerValidering(_form);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.5.6.3.3").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.5.6.3.3.1 SoekerValidering Error")]
        public void SoekerValidering_1_5_6_3_3_1()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "T", "Bø i Telemark");
            _form.ansvarligSoeker = null;

            _form.tiltakshaver.partstype.kodeverdi = "Foretak";
            _form.tiltakshaver.partstype.kodebeskrivelse = "Foretak";
            _form.tiltakshaver.organisasjonsnummer = "910748548";

            _form.tiltakshaver.kontaktperson.navn = null;

            _validator.SoekerValidering(_form);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.5.6.3.3.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.5.6.3.3.2 SoekerValidering Error")]
        public void SoekerValidering_1_5_6_3_3_2()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "T", "Bø i Telemark");
            _form.ansvarligSoeker = null;

            _form.tiltakshaver.partstype.kodeverdi = "Foretak";
            _form.tiltakshaver.partstype.kodebeskrivelse = "Foretak";
            _form.tiltakshaver.organisasjonsnummer = "910748548";

            _form.tiltakshaver.kontaktperson.mobilnummer = null;
            _form.tiltakshaver.kontaktperson.telefonnummer = null;

            _validator.SoekerValidering(_form);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.5.6.3.3.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.5.6.3.3.3 SoekerValidering Error")]
        public void SoekerValidering_1_5_6_3_3_3()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "T", "Bø i Telemark");
            _form.ansvarligSoeker = null;

            _form.tiltakshaver.partstype.kodeverdi = "Foretak";
            _form.tiltakshaver.partstype.kodebeskrivelse = "Foretak";
            _form.tiltakshaver.organisasjonsnummer = "910748548";

            _form.tiltakshaver.kontaktperson.telefonnummer = "555 55688";

            _validator.SoekerValidering(_form);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.5.6.3.3.3").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.5.6.3.3.4 SoekerValidering Error")]
        public void SoekerValidering_1_5_6_3_3_4()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "T", "Bø i Telemark");
            _form.ansvarligSoeker = null;

            _form.tiltakshaver.partstype.kodeverdi = "Foretak";
            _form.tiltakshaver.partstype.kodebeskrivelse = "Foretak";
            _form.tiltakshaver.organisasjonsnummer = "910748548";

            _form.tiltakshaver.kontaktperson.mobilnummer = "555 55688";

            _validator.SoekerValidering(_form);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.5.6.3.3.4").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.5.6.4 SoekerValidering Error")]
        public void SoekerValidering_1_5_6_4()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "T", "Bø i Telemark");
            _form.ansvarligSoeker = null;

            _form.tiltakshaver.adresse = null;

            _validator.SoekerValidering(_form);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.5.6.4").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "5721.1.5.6.4.1 SoekerValidering Error")]
        public void SoekerValidering_1_5_6_4_1()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "T", "Bø i Telemark");
            _form.ansvarligSoeker = null;

            _form.tiltakshaver.adresse.adresselinje1 = null;

            _validator.SoekerValidering(_form);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.5.6.4.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.5.6.4.2 SoekerValidering Error")]
        public void SoekerValidering_1_5_6_4_2()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "T", "Bø i Telemark");
            _form.ansvarligSoeker = null;

            _form.tiltakshaver.adresse.landkode = "*AR";

            _validator.SoekerValidering(_form);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.5.6.4.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.5.6.4.3 SoekerValidering Error")]
        public void SoekerValidering_1_5_6_4_3()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "T", "Bø i Telemark");
            _form.ansvarligSoeker = null;

            _form.tiltakshaver.adresse.postnr = null;

            _validator.SoekerValidering(_form);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.5.6.4.3").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.5.6.4.4 SoekerValidering Error")]
        public void SoekerValidering_1_5_6_4_4()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "T", "Bø i Telemark");

            _form.ansvarligSoeker = null;

            _form.tiltakshaver.adresse.postnr = "000";

            _validator.SoekerValidering(_form);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.5.6.4.4").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.5.6.4.5 SoekerValidering Error")]
        public void SoekerValidering_1_5_6_4_5()
        {
            _form.ansvarligSoeker = null;

            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(false, "T", "Frong");

            _validator.SoekerValidering(_form);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.5.6.4.5").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.5.6.4.6 SoekerValidering Error")]
        public void SoekerValidering_1_5_6_4_6()
        {
            _form.ansvarligSoeker = null;

            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "T", "Bø");

            _validator.SoekerValidering(_form);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.5.6.4.6").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.5.6.4.7 SoekerValidering Error")]
        public void SoekerValidering_1_5_6_4_7()
        {
            _form.ansvarligSoeker = null;

            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(null, null, null);

            _validator.SoekerValidering(_form);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.5.6.4.7").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.5.6.5 SoekerValidering Error")]
        public void SoekerValidering_1_5_6_5()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "T", "Bø i Telemark");

            _form.ansvarligSoeker = null;
            _form.tiltakshaver.telefonnummer = null;
            _form.tiltakshaver.mobilnummer = null;
            _validator.SoekerValidering(_form);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.5.6.5").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.5.6.6 SoekerValidering Error")]
        public void SoekerValidering_1_5_6_6()
        {
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "T", "Bø i Telemark");

            _form.ansvarligSoeker = null;
            _form.tiltakshaver.navn = null;
            _validator._postalCodeProvider = TestHelper.MokPostalCodeProviderResult(true, "T", "Bø i Telemark");
            _validator.SoekerValidering(_form);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.5.6.6").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.6 KommunensSaksnummerValidering Error")]
        public void KommunensSaksnummerValidering_1_6()
        {
            _form.kommunensSaksnummer = null;
            _validator.KommunensSaksnummerValidering(_form);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.6").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.6.1 KommunensSaksnummerValidering Error")]
        public void KommunensSaksnummerValidering_1_6_1()
        {
            _form.kommunensSaksnummer.saksaar = null;
            _validator.KommunensSaksnummerValidering(_form);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.6.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.6.2 KommunensSaksnummerValidering Error")]
        public void KommunensSaksnummerValidering_1_6_2()
        {
            _form.kommunensSaksnummer.sakssekvensnummer = null;
            _validator.KommunensSaksnummerValidering(_form);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.6.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "5721.1.8.1 EttersendingValidering Error")]
        public void EttersendingValidering_1_8_1()
        {
            _form.ettersending[0].vedlegg = new[]{new VedleggType()
            {
                filnavn = null
            }};
            _form.ettersending[0].underskjema = new[]{new KodeType()
            {
                kodeverdi = null
            }};
            _form.ettersending[0].kommentar = null;
            _validator.EttersendingValidering(_form, _formSetElements);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.8.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.8.8 EttersendingValidering Error")]
        public void EttersendingValidering_1_8_8()
        {
            _formSetElements = null;
            _validator.EttersendingValidering(_form, _formSetElements);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.8.8").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.8 EttersendingValidering Error")]
        public void EttersendingValidering_1_8()
        {
            _form.ettersending[0].tittel = null;
            _validator.EttersendingValidering(_form, _formSetElements);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.8").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.8.2 EttersendingValidering Error")]
        public void EttersendingValidering_1_8_2()
        {
            _form.ettersending[0].tema.kodeverdi = "*byggetomt";
            _validator.EttersendingValidering(_form, _formSetElements);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.8.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.8.3 EttersendingValidering Error")]
        public void EttersendingValidering_1_8_3()
        {
            _form.ettersending[0].tema.kodebeskrivelse = null;
            _validator.EttersendingValidering(_form, _formSetElements);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.8.3").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.8.4 EttersendingValidering Error")]
        public void EttersendingValidering_1_8_4()
        {
            _form.ettersending[0].tema.kodeverdi = "kravTilOpparbeidelse";
            _form.ettersending[0].tema.kodebeskrivelse = "**Krav til opparbeidelse av infrastruktur for regulert eiendom";
            _validator.EttersendingValidering(_form, _formSetElements);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.8.4").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.8.5 EttersendingValidering Error")]
        public void EttersendingValidering_1_8_5()
        {
            _form.ettersending[0].vedlegg[0].filnavn = "filnavnNoko.PDF";
            _form.ettersending[0].vedlegg[0].vedleggstype = null;

            _validator.EttersendingValidering(_form, _formSetElements);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.8.5").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.8.6 EttersendingValidering Error")]
        public void EttersendingValidering_1_8_6()
        {
            _form.ettersending[0].vedlegg[0].filnavn = "filnavnNoko.PDF";
            _form.ettersending[0].vedlegg[0].vedleggstype.kodeverdi = null;

            _validator.EttersendingValidering(_form, _formSetElements);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.8.6").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.8.6.1 EttersendingValidering Error")]
        public void EttersendingValidering_1_8_6_1()
        {
            _form.ettersending[0].vedlegg[0].filnavn = "filnavnNoko.PDF";
            _form.ettersending[0].vedlegg[0].vedleggstype.kodeverdi = "**Avkjoerselsplan";

            _validator.EttersendingValidering(_form, _formSetElements);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.8.6.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.8.6.2 EttersendingValidering Error")]
        public void EttersendingValidering_1_8_6_2()
        {
            _form.ettersending[0].vedlegg[0].filnavn = "filnavnNoko.PDF";
            _form.ettersending[0].vedlegg[0].vedleggstype.kodebeskrivelse = null;

            _validator.EttersendingValidering(_form, _formSetElements);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.8.6.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.8.6.3 EttersendingValidering Error")]
        public void EttersendingValidering_1_8_6_3()
        {
            _form.ettersending[0].vedlegg[0].filnavn = "filnavnNoko.PDF";
            _form.ettersending[0].vedlegg[0].vedleggstype.kodeverdi = "AvklaringHoyspent";
            _form.ettersending[0].vedlegg[0].vedleggstype.kodebeskrivelse = "**Avklaring av plassering nær høyspentledning";

            _validator.EttersendingValidering(_form, _formSetElements);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.8.6.3").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.8.6.4 EttersendingValidering Error")]
        public void EttersendingValidering_1_8_6_4()
        {
            _form.ettersending[0].vedlegg[0].vedleggstype.kodeverdi = "TegningNyPlan";
            _form.ettersending[0].vedlegg[0].vedleggstype.kodebeskrivelse = "Tegning ny plan";

            //_form.ettersending[0].vedlegg[0].vedleggstype.kodeverdi = "Utomhusplan";
            //_form.ettersending[0].vedlegg[0].vedleggstype.kodebeskrivelse = "Utomhusplan";

            _form.ettersending[0].vedlegg[0].versjonsdatoSpecified = false;
            _form.ettersending[0].vedlegg[0].versjonsdato = null;

            _validator.EttersendingValidering(_form, _formSetElements);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.8.6.4").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.8.6.5 EttersendingValidering Error")]
        public void EttersendingValidering_1_8_6_5()
        {
            _form.ettersending[0].vedlegg[0].vedleggstype.kodeverdi = "TegningNyPlan";
            _form.ettersending[0].vedlegg[0].vedleggstype.kodebeskrivelse = "Tegning ny plan";

            _form.ettersending[0].vedlegg[0].versjonsnummer = null;

            _validator.EttersendingValidering(_form, _formSetElements);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.8.6.5").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.8.7 EttersendingValidering Error")]
        public void EttersendingValidering_1_8_7()
        {
            _form.ettersending[0].underskjema[0].kodeverdi = null;

            _validator.EttersendingValidering(_form, _formSetElements);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.8.7").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.8.7.1 EttersendingValidering Error")]
        public void EttersendingValidering_1_8_7_1()
        {
            _form.ettersending[0].underskjema[0].kodeverdi = "*AnsvarsrettSelvbygger";

            _validator.EttersendingValidering(_form, _formSetElements);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.8.7.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.8.7.2 EttersendingValidering Error")]
        public void EttersendingValidering_1_8_7_2()
        {
            _form.ettersending[0].underskjema[0].kodebeskrivelse = null;

            _validator.EttersendingValidering(_form, _formSetElements);

            var result = _validator.GetResult();
            result.messages.Where(m => m.reference == "5721.1.8.7.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.8.7.3 EttersendingValidering Error")]
        public void EttersendingValidering_1_8_7_3()
        {
            _form.ettersending[0].underskjema[0].kodeverdi = "SoeknadArbeidstilsynet";
            _form.ettersending[0].underskjema[0].kodebeskrivelse = "**Søknad om Arbeidstilsynets samtykke";

            _validator.EttersendingValidering(_form, _formSetElements);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "5721.1.8.7.3").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.9 MangelbesvarelseValidering Error")]
        public void MangelbesvarelseValidering_1_9()
        {

            _form.mangelbesvarelse[0].tittel = null;

            _validator.MangelbesvarelseValidering(_form, _formSetElements);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "5721.1.9").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.9.1 MangelbesvarelseValidering Error")]
        public void MangelbesvarelseValidering_1_9_1()
        {

            _form.mangelbesvarelse[0].erMangelBesvaresSenere = null;
            _form.mangelbesvarelse[0].erMangelBesvaresSenereSpecified = false;

            _validator.MangelbesvarelseValidering(_form, _formSetElements);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "5721.1.9.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.9.2 MangelbesvarelseValidering Error")]
        public void MangelbesvarelseValidering_1_9_2()
        {

            _form.mangelbesvarelse[0].erMangelBesvaresSenere = true;
            _form.mangelbesvarelse[0].erMangelBesvaresSenereSpecified = true;
            _form.mangelbesvarelse[0].kommentar = null;

            _validator.MangelbesvarelseValidering(_form, _formSetElements);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "5721.1.9.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.9.2.1 MangelbesvarelseValidering Error")]
        public void MangelbesvarelseValidering_1_9_2_1()
        {

            _form.mangelbesvarelse[0].erMangelBesvaresSenere = false;
            _form.mangelbesvarelse[0].erMangelBesvaresSenereSpecified = true;

            //_form.mangelbesvarelse[0].mangel.mangelId = null;
            _form.mangelbesvarelse[0].vedlegg = null;
            _form.mangelbesvarelse[0].underskjema = null;


            _form.mangelbesvarelse[0].kommentar = null;

            _validator.MangelbesvarelseValidering(_form, _formSetElements);
            var result = _validator.GetResult();


            result.messages.Where(m => m.reference == "5721.1.9.2.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.9.4 MangelbesvarelseValidering Error")]
        public void MangelbesvarelseValidering_1_9_4()
        {

            _form.mangelbesvarelse[0].mangel.mangelId = "12332354asd5a4f8asfsdsdaf";
            _form.mangelbesvarelse[0].tema.kodeverdi = "*andreMyndigheter";
            _form.mangelbesvarelse[0].tema.kodebeskrivelse = "Andre myndigheter";


            _validator.MangelbesvarelseValidering(_form, _formSetElements);
            var result = _validator.GetResult();
            var rulesJson = TestHelper.ConverRulesCheckedJson(result);
            result.messages.Where(m => m.reference == "5721.1.9.4").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.9.4.1 MangelbesvarelseValidering Error")]
        public void MangelbesvarelseValidering_1_9_4_1()
        {

            _form.mangelbesvarelse[0].mangel.mangelId = "12332354asd5a4f8asfsdsdaf";
            _form.mangelbesvarelse[0].tema.kodeverdi = "andreMyndigheter";
            _form.mangelbesvarelse[0].tema.kodebeskrivelse = null;


            _validator.MangelbesvarelseValidering(_form, _formSetElements);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "5721.1.9.4.1").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.9.4.2 MangelbesvarelseValidering Error")]
        public void MangelbesvarelseValidering_1_9_4_2()
        {

            _form.mangelbesvarelse[0].mangel.mangelId = "12332354asd5a4f8asfsdsdaf";
            _form.mangelbesvarelse[0].tema.kodeverdi = "andreMyndigheter";
            _form.mangelbesvarelse[0].tema.kodebeskrivelse = "***Andre myndigheter";


            _validator.MangelbesvarelseValidering(_form, _formSetElements);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "5721.1.9.4.2").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.9.8 MangelbesvarelseValidering Error")]
        public void MangelbesvarelseValidering_1_9_8()
        {

            _validator.MangelbesvarelseValidering(_form, new List<string>());
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "5721.1.9.8").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }
        [Fact(DisplayName = "5721.1.9.6.5 MangelbesvarelseValidering Error")]
        public void MangelbesvarelseValidering_1_9_6_5()
        {
            _form.mangelbesvarelse[0].vedlegg[0].vedleggstype.kodeverdi = "TegningNyPlan";
            _form.mangelbesvarelse[0].vedlegg[0].vedleggstype.kodebeskrivelse = "Tegning ny plan";
            
            _form.mangelbesvarelse[0].vedlegg[0].versjonsnummer = null;
            _validator.MangelbesvarelseValidering(_form, _formSetElements);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "5721.1.9.6.5").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }

        [Fact(DisplayName = "5721.1.9.6.4 MangelbesvarelseValidering Error")]
        public void MangelbesvarelseValidering_1_9_6_4()
        {
            _form.mangelbesvarelse[0].vedlegg[0].vedleggstype.kodeverdi = "TegningNyPlan";
            _form.mangelbesvarelse[0].vedlegg[0].vedleggstype.kodebeskrivelse = "Tegning ny plan"; 
            
            _form.mangelbesvarelse[0].vedlegg[0].versjonsdatoSpecified = false;
            _form.mangelbesvarelse[0].vedlegg[0].versjonsdato = null;
            _validator.MangelbesvarelseValidering(_form, _formSetElements);
            var result = _validator.GetResult();

            result.messages.Where(m => m.reference == "5721.1.9.6.4").Should().NotBeNullOrEmpty();
            result.messages.Count.Should().Be(1);
        }


    }
}
