﻿using FluentAssertions;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Altinn;
using Xunit;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Tests.Services
{
    public class FormValidationTests
    {
        [Fact]
        public void FormValidateIsKnownRammetillatelseV2Test()
        {
            var result = AltinnFormTypeResolver.IsKnownForm("6741", "45752");
            result.Should().BeTrue();
        }

        [Fact]
        public void FormValidateIsKnownFalseTest()
        {
            var result = AltinnFormTypeResolver.IsKnownForm("9999", "41793");
            result.Should().BeFalse();
        }
    }
}