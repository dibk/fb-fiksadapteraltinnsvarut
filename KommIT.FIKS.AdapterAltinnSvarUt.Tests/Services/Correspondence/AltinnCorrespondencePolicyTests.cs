﻿using KommIT.FIKS.AdapterAltinnSvarUt.Services.Altinn;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Altinn.Correspondence;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Notification;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.WS.AltinnCorrespondenceAgency;
using Moq;
using Serilog;
using Serilog.Events;
using System;
using System.Collections.Generic;
using System.ServiceModel;
using Xunit;
using Xunit.Abstractions;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Tests.Services.Correspondence
{
    //public class AltinnCorrespondencePolicyTests
    //{
    //    private readonly ITestOutputHelper output;
    //    public AltinnCorrespondencePolicyTests(ITestOutputHelper output)
    //    {
    //        this.output = output;
    //    }

    //    [Fact]
    //    public void SendNotification_Ok()
    //    {
    //        var correspondence = BuildTestCorrespondence();
    //        var expectedResult = BuildTestReceipt();
    //        var logger = GetConsoleLogger(output);
    //        var client = new Mock<ICorrespondenceClient>();
    //        client.Setup(r => r.SendCorrespondence(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<InsertCorrespondenceV2>()))
    //            .Returns(expectedResult);

    //        var factory = new Mock<ICorrespondenceClientFactory>();
    //        factory.Setup(s => s.GetClient()).Returns(client.Object);

    //        var policyProvider = GetTestPolicyProvider();

    //        var altinnCorrespondenceService = new AltinnCorrespondenceService(logger, factory.Object, policyProvider);

    //        var result = altinnCorrespondenceService.SendNotification("UnitTest", correspondence);

    //        Assert.Equal(expectedResult, result);
    //    }

    //    [Fact]
    //    public void SendNotification_ThrowsTimeoutExceptionRetries1Time_thenSuccess()
    //    {
    //        var correspondence = BuildTestCorrespondence();
    //        var expectedResult = BuildTestReceipt();

    //        var exception = new System.TimeoutException("This takes too long time");

    //        var logger = GetConsoleLogger(output);
    //        var client = new Mock<ICorrespondenceClient>();
    //        client.SetupSequence(r => r.SendCorrespondence(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<InsertCorrespondenceV2>()))
    //            .Throws(exception).Returns(expectedResult);

    //        var factory = new Mock<ICorrespondenceClientFactory>();
    //        factory.Setup(s => s.GetClient()).Returns(client.Object);

    //        var policyProvider = GetTestPolicyProvider();

    //        var altinnCorrespondenceService = new AltinnCorrespondenceService(logger, factory.Object, policyProvider);

    //        var result = altinnCorrespondenceService.SendNotification("UnitTest", correspondence);

    //        client.Verify(s => s.SendCorrespondence(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<InsertCorrespondenceV2>())
    //            , Times.Exactly(2));
    //        Assert.Equal(expectedResult, result);
    //    }

    //    [Fact]
    //    public void SendNotification_ThrowsServerTooBusyExceptionRetries2Times_thenSuccess()
    //    {
    //        var correspondence = BuildTestCorrespondence();
    //        var expectedResult = BuildTestReceipt();

    //        var exception = new System.ServiceModel.ServerTooBusyException("There is currently too much for me to handle!");


    //        var logger = GetConsoleLogger(output);
    //        var client = new Mock<ICorrespondenceClient>();
    //        client.SetupSequence(r => r.SendCorrespondence(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<InsertCorrespondenceV2>()))
    //            .Throws(exception).Throws(exception).Returns(expectedResult);

    //        var factory = new Mock<ICorrespondenceClientFactory>();
    //        factory.Setup(s => s.GetClient()).Returns(client.Object);

    //        var policyConfiguration = new Mock<ICorrespondencePolicyConfigurationProvider>();
    //        policyConfiguration.Setup(s => s.RetryAttemptWaitSecondsStructure).Returns(new List<int>() { 1, 1 });
    //        var policyProvider = new CorrespondencePolicyProvider(policyConfiguration.Object);

    //        var altinnCorrespondenceService = new AltinnCorrespondenceService(logger, factory.Object, policyProvider);

    //        var result = altinnCorrespondenceService.SendNotification("UnitTest", correspondence);

    //        client.Verify(s => s.SendCorrespondence(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<InsertCorrespondenceV2>())
    //            , Times.Exactly(3));
    //        Assert.Equal(expectedResult, result);
    //    }

    //    [Fact]
    //    public void SendNotification_ThrowsServerTooBusyExceptionRetries1TimeMoreThanPolicy_thenFails()
    //    {
    //        var correspondence = BuildTestCorrespondence();
    //        var expectedResult = BuildTestReceipt();

    //        var exception = new System.ServiceModel.ServerTooBusyException("There is currently too much for me to handle!");

    //        var logger = GetConsoleLogger(output);
    //        var client = new Mock<ICorrespondenceClient>();
    //        client.SetupSequence(r => r.SendCorrespondence(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<InsertCorrespondenceV2>()))
    //            .Throws(exception).Throws(exception).Throws(exception).Returns(expectedResult);

    //        var factory = new Mock<ICorrespondenceClientFactory>();
    //        factory.Setup(s => s.GetClient()).Returns(client.Object);

    //        var policyConfiguration = new Mock<ICorrespondencePolicyConfigurationProvider>();
    //        policyConfiguration.Setup(s => s.RetryAttemptWaitSecondsStructure).Returns(new List<int>() { 1, 1 });
    //        var policyProvider = new CorrespondencePolicyProvider(policyConfiguration.Object);

    //        var altinnCorrespondenceService = new AltinnCorrespondenceService(logger, factory.Object, policyProvider);

    //        Assert.Throws<System.ServiceModel.ServerTooBusyException>(() => altinnCorrespondenceService.SendNotification("UnitTest", correspondence));

    //        client.Verify(s => s.SendCorrespondence(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<InsertCorrespondenceV2>())
    //            , Times.Exactly(3));
    //    }

    //    [Fact]
    //    public void SendNotification_ThrowsFaultException()
    //    {
    //        var correspondence = BuildTestCorrespondence();
    //        var expectedResult = BuildTestReceipt();

    //        var exception = new System.ServiceModel.FaultException("Something went wrong!");

    //        var logger = GetConsoleLogger(output);
    //        var client = new Mock<ICorrespondenceClient>();
    //        client.Setup(r => r.SendCorrespondence(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<InsertCorrespondenceV2>()))
    //            .Throws(exception);

    //        var factory = new Mock<ICorrespondenceClientFactory>();
    //        factory.Setup(s => s.GetClient()).Returns(client.Object);

    //        var policyProvider = GetTestPolicyProvider();

    //        var altinnCorrespondenceService = new AltinnCorrespondenceService(logger, factory.Object, policyProvider);


    //        Assert.Throws<System.ServiceModel.FaultException>(() => altinnCorrespondenceService.SendNotification("UnitTest", correspondence));

    //        client.Verify(s => s.SendCorrespondence(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<InsertCorrespondenceV2>())
    //            , Times.Exactly(1));
    //    }

    //    [Fact]
    //    public void SendNotification_ThrowsAltinFaultException()
    //    {
    //        var correspondence = BuildTestCorrespondence();
    //        var expectedResult = BuildTestReceipt();

    //        var fault = new AltinnFault();
    //        fault.AltinnErrorMessage = "I am Altinn error message";
    //        fault.AltinnExtendedErrorMessage = "I am Altinn extended error message";
    //        fault.ErrorGuid = Guid.NewGuid().ToString();

    //        var faultReason = new FaultReason("Super important error");
    //        var faultCode = new FaultCode("Ich bin Fault");

    //        var exception = new FaultException<AltinnFault>(fault, faultReason, faultCode, "SOAP action ftw");
    //        var logger = GetConsoleLogger(output);
    //        var client = new Mock<ICorrespondenceClient>();
    //        client.Setup(r => r.SendCorrespondence(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<InsertCorrespondenceV2>()))
    //            .Throws(exception);

    //        var factory = new Mock<ICorrespondenceClientFactory>();
    //        factory.Setup(s => s.GetClient()).Returns(client.Object);

    //        var policyProvider = GetTestPolicyProvider();

    //        var altinnCorrespondenceService = new AltinnCorrespondenceService(logger, factory.Object, policyProvider);


    //        Assert.Throws<System.ServiceModel.FaultException<AltinnFault>>(() => altinnCorrespondenceService.SendNotification("UnitTest", correspondence));

    //        client.Verify(s => s.SendCorrespondence(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<InsertCorrespondenceV2>())
    //            , Times.Exactly(1));
    //    }

    //    private ICorrespondencePolicyProvider GetTestPolicyProvider()
    //    {
    //        var policyConfiguration = new Mock<ICorrespondencePolicyConfigurationProvider>();
    //        policyConfiguration.Setup(s => s.RetryAttemptWaitSecondsStructure).Returns(new List<int>() { 1 });

    //        return new CorrespondencePolicyProvider(policyConfiguration.Object);
    //    }

    //    private InsertCorrespondenceV2 BuildTestCorrespondence()
    //    {
    //        CorrespondanceBuilder correspondanceBuilder = new CorrespondanceBuilder();
    //        correspondanceBuilder.SetUpCorrespondence("1234", "1", "Ich bin reportee", "AR987654321", false);
    //        correspondanceBuilder.AddContent("I'm the title!", "I'm the summary!", "And I am the body!");

    //        return correspondanceBuilder.Build();
    //    }

    //    private ReceiptExternal BuildTestReceipt()
    //    {
    //        var retVal = new ReceiptExternal();

    //        retVal.ReceiptStatusCode = ReceiptStatusEnum.OK;
    //        retVal.ReceiptId = 1;
    //        retVal.ReceiptText = "OK";

    //        return retVal;
    //    }

    //    private ILogger GetConsoleLogger(ITestOutputHelper output)
    //    {
    //        ILogger logger = new LoggerConfiguration()
    //            .MinimumLevel.Verbose()
    //            .Enrich.FromLogContext()
    //            .WriteTo.TestOutput(output, LogEventLevel.Debug)
    //            .CreateLogger()
    //            .ForContext<AltinnCorrespondencePolicyTests>();
    //        Log.Logger = logger;

    //        return logger;

    //    }
    //}
}
