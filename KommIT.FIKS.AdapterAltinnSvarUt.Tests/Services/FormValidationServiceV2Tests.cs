﻿using FluentAssertions;
using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Services;
using KommIT.FIKS.AdapterAltinnSvarUt.Tests.Services.Builders;
using Newtonsoft.Json;
using System.IO;
using System.Linq;
using System.Reflection;
using Xunit;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Tests.Services
{
    public class FormValidationServiceV2Tests
    {
        [Fact(DisplayName = "Vedlegg count - Ok Test")]
        public void VedleggCountOkTest()
        {
            var val = new FormValidationServiceV2();
            var vedlleggs = new ValidateAttachmentsBuilder()
                .AddVedlegg("ErklaeringAnsvarsrett", "ErklaeringAnsvarsrett.xls", 100, 1)
                .AddVedlegg("Kart", "Kart.tiff", 200, 1)
                .AddVedlegg("Avkjoerselsplan", "Avkjoerselsplan.xls", 100, 90)
                .Build();
            var altinnMetadata = GetAltinnMetadataFromJson();

            var result = val.ValidateFormAttachmentsAndSubForms(altinnMetadata, vedlleggs);
            result.messages.Count.Should().Be(0);
        }

        [Fact(DisplayName = "Vedlegg count - Error Test")]
        public void VedleggCountErrorTest()
        {
            var val = new FormValidationServiceV2();
            var vedlleggs = new ValidateAttachmentsBuilder()
                .AddVedlegg("ErklaeringAnsvarsrett", "ErklaeringAnsvarsrett.xls", 100, 1)
                .AddVedlegg("Kart", "Kart.tiff", 200, 1)
                .AddVedlegg("Avkjoerselsplan", "Avkjoerselsplan.xls", 100, 101)
                .Build();
            var altinnMetadata = GetAltinnMetadataFromJson();

            var result = val.ValidateFormAttachmentsAndSubForms(altinnMetadata, vedlleggs);
            result.messages.Count.Should().Be(1);
            result.messages.Where(m => m.reference.Contains("4397.1.3")).Should().NotBeNullOrEmpty();
        }

        [Fact(DisplayName = "Vedlegg Type - Ok Test")]
        public void VedleggTypeOkTest()
        {
            var val = new FormValidationServiceV2();
            var vedlleggs = new ValidateAttachmentsBuilder()
                .AddVedlegg("ErklaeringAnsvarsrett", "ErklaeringAnsvarsrett.xls", 100, 1)
                .AddVedlegg("Kart", "Kart.tiff", 200, 1)
                .Build();
            var altinnMetadata = GetAltinnMetadataFromJson();

            var result = val.ValidateFormAttachmentsAndSubForms(altinnMetadata, vedlleggs);
            result.messages.Count.Should().Be(0);
        }

        [Fact(DisplayName = "Vedlegg Type - Error Test")]
        public void VedleggTypeErrorTest()
        {
            var val = new FormValidationServiceV2();
            var vedlleggs = new ValidateAttachmentsBuilder()
                .AddVedlegg("ErklaeringAnsvarsrett", "ErklaeringAnsvarsrett.xls", 100, 1)
                .AddVedlegg("Folgebrev", "Folgebrev.exe", 199, 1)
                .AddVedlegg("Kart", "Kart.tiff", 200, 1)
                .Build();
            var altinnMetadata = GetAltinnMetadataFromJson();

            var result = val.ValidateFormAttachmentsAndSubForms(altinnMetadata, vedlleggs);
            result.messages.Count.Should().Be(1);
            result.messages.Where(m => m.reference.Contains("4397.1.3")).Should().NotBeNullOrEmpty();
        }

        [Fact(DisplayName = "Vedlegg File Type - OK Test")]
        public void VedleggFileTypeOkTest()
        {
            var val = new FormValidationServiceV2();
            var vedlleggs = new ValidateAttachmentsBuilder()
                .AddVedlegg("ErklaeringAnsvarsrett", "ErklaeringAnsvarsrett.xls", 100, 1)
                .AddVedlegg("Kart", "Kart.tiff", 200, 1)
                .Build();
            var altinnMetadata = GetAltinnMetadataFromJson();

            var result = val.ValidateFormAttachmentsAndSubForms(altinnMetadata, vedlleggs);
            result.messages.Count.Should().Be(0);
        }

        [Fact(DisplayName = "Vedlegg File Type - Error Test")]
        public void VedleggFileTypeErrorTest()
        {
            var val = new FormValidationServiceV2();
            var vedlleggs = new ValidateAttachmentsBuilder()
                .AddVedlegg("ErklaeringAnsvarsrett", "ErklaeringAnsvarsrett.xls", 100, 1)
                .AddVedlegg("Kart", "Kart.exe", 200, 1)
                .Build();
            var altinnMetadata = GetAltinnMetadataFromJson();

            var result = val.ValidateFormAttachmentsAndSubForms(altinnMetadata, vedlleggs);
            result.messages.Count.Should().Be(1);
            result.messages.Where(m => m.reference.Contains("4397.1.3")).Should().NotBeNullOrEmpty();
        }

        [Fact(DisplayName = "Vedlegg File Name - Error Test")]
        public void VedleggFileNameErrorTest()
        {
            var val = new FormValidationServiceV2();
            var vedlleggs = new ValidateAttachmentsBuilder()
                .AddVedlegg("Kart", "Kart< > ? * | :.tiff", 200, 1)
                .AddVedlegg("Kart", "Kart?.tiff", 200, 1)
                .AddVedlegg("Kart", ".tiff", 200, 1)
                .AddVedlegg("Kart", "Kart.as.as.as..tiff", 200, 1)
                .AddVedlegg("Kart", "Kart-.tiff", 200, 1)
                .AddVedlegg("Kart", "Kar_t.tiff", 200, 1)
                .Build();
            var altinnMetadata = GetAltinnMetadataFromJson();

            var result = val.ValidateFormAttachmentsAndSubForms(altinnMetadata, vedlleggs);
            result.messages.Count.Should().Be(3);
            result.messages.Where(m => m.reference.Contains("4397.1.3")).Should().NotBeNullOrEmpty();
        }

        private static AltinnMetadata GetAltinnMetadataFromJson()
        {
            var resourcePath = "KommIT.FIKS.AdapterAltinnSvarUt.Tests.TestData.AltinnMetadata.json";
            var assembly = Assembly.GetExecutingAssembly();
            AltinnMetadata altinnMetadata;

            using (Stream fileStream = assembly.GetManifestResourceStream(resourcePath))
            using (StreamReader streamReader = new StreamReader(fileStream))
            {
                var json = streamReader.ReadToEnd();
                altinnMetadata = JsonConvert.DeserializeObject<AltinnMetadata>(json);
            }
            return altinnMetadata;
        }
    }
}