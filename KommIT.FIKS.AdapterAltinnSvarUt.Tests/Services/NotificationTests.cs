﻿using FluentAssertions;
using Xunit;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Altinn.Tests
{
    public class NotificationTests
    {
        [Fact]
        public void CountNotificationImplTest()
        {
            var instances = AltinnFormDeserializer.GetForms();
            int notificationCount = 0;
            foreach (var item in instances)
            {
                if (item is INotificationForm)
                {
                    notificationCount++;
                }
            }
            notificationCount.Should().Be(9);
        }
    }
}