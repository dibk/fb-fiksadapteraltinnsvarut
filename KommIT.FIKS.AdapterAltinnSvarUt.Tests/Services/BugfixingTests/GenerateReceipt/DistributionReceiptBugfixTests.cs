using CsvHelper;
using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Services;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.NotificationForm.Nabovarsel;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Prefill;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.WS.Altinn;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Linq;
using Xunit;
using CsvHelper.Configuration;
using System.Globalization;
using CsvHelper.TypeConversion;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Altinn;
using KommIT.FIKS.AdapterAltinnSvarUt.Models.ApiModels;
using Moq;
using Serilog;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Tests.Services
{
    public class DistributionReceiptBugfixTests
    {

        //[Fact]
        [Fact (Skip = "Only ued to run receipt if something goes wrong")]
        public void TheGenerateDistributionReceiptNewWrapper()
        {
            GenerateDistributionReceiptNew("AR456020646");
        }



        private void GenerateDistributionReceiptNew(string archiveReference)
        {

            var loggerMock = new Mock<ILogger>();
            var appLogMock = new Mock<IApplicationLogService>();

            AltinnDownloadStreamAttachment stream = new AltinnDownloadStreamAttachment(loggerMock.Object);
            AltinnService_DIBK altinnService = new AltinnService_DIBK(stream, appLogMock.Object, loggerMock.Object);


            // *************** Important ****************
            // Download the xml data of the form from altinn and put it somewhere...
            //
            var arcivedFormTask = altinnService.GetItemByArchiveReference(archiveReference);

            //Only used when we are unable to download from DLQ
            //var arcivedFormTask = BuildArchivedFormTaskDQBE(archiveReference, 
            //                                                DateTime.Parse(archiveTimeStamp), 
            //                                                $@"C:\temp\GenereringAvKvitteringer\{archiveReference}\{archiveReference}.xml",
            //                                                $@"C:\temp\GenereringAvKvitteringer\{archiveReference}\AttachmentResults.csv");

            //Create formData
            var formData = new AltinnFormDeserializer().DeserializeForm(arcivedFormTask);




            // *************** Important ****************
            // Download the pdf-version of the form from altinn and put it somewhere...
            //
            var attachment = new Attachment
            {
                AttachmentData = altinnService.GetPdfForm(formData.ArchiveReference, formData.MainformDataFormatID, formData.MainformDataFormatVersionID),
                ArchiveReference = formData.ArchiveReference,
                AttachmentType = "application/pdf",
                AttachmentTypeName = AttachmentSetting.GetAttachmentTypeName("4655"),
                FileName = AttachmentSetting.GetMainFormPDFAttachmentFileName("4655"),
                IsEncrypted = false
            };
            formData.MainFormPdf = attachment;


            // *************** Important ****************
            // Get the distributionForm rows in CSV related to the distribtion and add them to the "DistributionForms.csv" file...
            //
            //Create distributionForms data
            var distributionForms = GetDistributionForms($@"C:\temp\GenereringAvKvitteringer\{archiveReference}\DistributionForms.csv");

            //Get prefills
            var prefills = ((IDistributionForm)formData.Mainform).GetPrefillForms();
            var kvitteringer = new List<KvitteringForNabovarsel>();
            string kontaktperson = "kontaktperson";

            foreach (var item in prefills)
            {
                var svar = item as INabovarselSvar;
                var kvittering = svar.GetkvitteringForNabovarsel();

                kvittering.Hovedinnsendingsnummer = distributionForms.Where(d => d.ExternalSystemReference == svar.GetSluttbrukersystemVaarReferanse().FirstOrDefault())?.FirstOrDefault()?.Id.ToString();

                kvitteringer.Add(kvittering);
            }

            //Create attachments
            var attachments = formData.Attachments;

            var receiptService = new DistributionReceiptService();
            var receipt = receiptService.MakeReceipt(kvitteringer, distributionForms, ((INabovarselDistribution)formData.Mainform).GetProjectName(), formData.Mainform.GetPropertyIdentifiers().AnsvarligSokerNavn, kontaktperson, attachments);

            WriteFile(receipt, $@"C:\temp\GenereringAvKvitteringer\{archiveReference}\{Guid.NewGuid()}-{Resources.TextStrings.KvitteringNabovarselFilename}");
        }





        [Fact(Skip = "Only ued to run receipt if something goes wrong")]
        public void GenerateDistributionReceipt()
        {
            var archiveReference = "AR456515715";
            var archiveTimeStamp = "2021-11-30 11:33:47.183";

            // *************** Important ****************
            // Download the xml data of the form from altinn and put it somewhere...
            //
            var arcivedFormTask = LoadXmlFromFile($@"C:\temp\GenereringAvKvitteringer\{archiveReference}\{archiveReference}.xml");

            //Only used when we are unable to download from DLQ
            //var arcivedFormTask = BuildArchivedFormTaskDQBE(archiveReference, 
            //                                                DateTime.Parse(archiveTimeStamp), 
            //                                                $@"C:\temp\GenereringAvKvitteringer\{archiveReference}\{archiveReference}.xml",
            //                                                $@"C:\temp\GenereringAvKvitteringer\{archiveReference}\AttachmentResults.csv");

            //Create formData
            var formData = new AltinnFormDeserializer().DeserializeForm(arcivedFormTask);

            // *************** Important ****************
            // Download the pdf-version of the form from altinn and put it somewhere...
            //
            var attachment = new Attachment
            {
                AttachmentData = GetFileBytes($@"C:\temp\GenereringAvKvitteringer\{archiveReference}\{archiveReference}.pdf"),
                ArchiveReference = "The archiveReference",
                AttachmentType = "application/pdf",
                AttachmentTypeName = AttachmentSetting.GetAttachmentTypeName("4655"),
                FileName = Attachment.FilenameMainFormPdf,
                IsEncrypted = false
            };

            formData.MainFormPdf = attachment;

            // *************** Important ****************
            // Get the distributionForm rows in CSV related to the distribtion and add them to the "DistributionForms.csv" file...
            //
            //Create distributionForms data
            var distributionForms = GetDistributionForms($@"C:\temp\GenereringAvKvitteringer\{archiveReference}\DistributionForms.csv");

            //Get prefills
            var prefills = ((IDistributionForm)formData.Mainform).GetPrefillForms();
            var kvitteringer = new List<KvitteringForNabovarsel>();
            string kontaktperson = "kontaktperson";

            foreach (var item in prefills)
            {
                var svar = item as INabovarselSvar;
                var kvittering = svar.GetkvitteringForNabovarsel();

                kvittering.Hovedinnsendingsnummer = distributionForms.Where(d => d.ExternalSystemReference == svar.GetSluttbrukersystemVaarReferanse().FirstOrDefault())?.FirstOrDefault()?.Id.ToString();

                kvitteringer.Add(kvittering);
            }

            //Create attachments
            var attachments = formData.Attachments;

            var receiptService = new DistributionReceiptService();
            var receipt = receiptService.MakeReceipt(kvitteringer, distributionForms, ((INabovarselDistribution)formData.Mainform).GetProjectName(), formData.Mainform.GetPropertyIdentifiers().AnsvarligSokerNavn, kontaktperson, attachments);

            WriteFile(receipt, $@"C:\temp\GenereringAvKvitteringer\{archiveReference}\{Guid.NewGuid()}-{Resources.TextStrings.KvitteringNabovarselFilename}");
        }

        [Fact(Skip = "Only used for generating filedownloadstatus if something has gone wrong")]
        public void GenerateFiledownloadStatusJsons()
        {
            var archivereference = "ARxxxxxxxx";
            var guid = Guid.Parse("7918dc1f-ac46-48d3-af97-63015e691c0e"); //The container name used in BlobStorage
            var kvitteringBlobUri = ""; //Url to the uploaded receipt

            var fdsKvittering = new FileDownloadStatus(archivereference, guid, Resources.TextStrings.FormNameNabovarsel,
                    FilTyperForNedlasting.KvitteringNabovarsel, Resources.TextStrings.KvitteringNabovarselFilename, kvitteringBlobUri, Resources.TextStrings.MimeAppPdf);

            var nabovarselBlobUri = ""; //Url to the uploaded nabovarsel
            var filename = Resources.TextStrings.NabovarselMainformFilename;

            var fdsNabovarsel = new FileDownloadStatus(archivereference, guid, Resources.TextStrings.FormNameNabovarsel,
                    FilTyperForNedlasting.Nabovarsel, filename, nabovarselBlobUri, Resources.TextStrings.MimeAppPdf);

            var fds = new List<FileDownloadStatus>() { fdsKvittering, fdsNabovarsel };

            var serializerSettings = new Newtonsoft.Json.JsonSerializerSettings() { NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore, };
            var filecontent = Newtonsoft.Json.JsonConvert.SerializeObject(fds, serializerSettings);

            File.WriteAllText($@"{Environment.CurrentDirectory}\Services\BugfixingTests\GenerateReceipt\{guid}-FiledownloadStatus.txt", filecontent);
        }

        private void WriteFile(Attachment receipt, string path)
        {
            File.WriteAllBytes(path, receipt.AttachmentData);
        }
        private byte[] GetFileBytes(string filename)
        {
            return File.ReadAllBytes(filename);
        }

        private ArchivedFormTaskDQBE LoadXmlFromFile(string fileName)
        {
            var fileContent = File.ReadAllText(fileName, System.Text.Encoding.UTF8);

            return SerializeUtil.DeserializeFromString<ArchivedFormTaskDQBE>(fileContent);
        }

        private ArchivedFormTaskDQBE BuildArchivedFormTaskDQBE(string archiveReference, DateTime archiveTimestamp, string xmlFileName, string attachmentCsvFileName)
        {
            var fileContent = File.ReadAllText(xmlFileName, System.Text.Encoding.UTF8);

            var attachmentInfo = GetFormAttachmentMetadatas(attachmentCsvFileName);

            var task = new ArchivedFormTaskDQBE();
            task.ArchiveReference = archiveReference;
            task.ArchiveTimeStamp = archiveTimestamp;
            task.Forms = new ArchivedFormListDQBE();
            task.Forms.Add(new ArchivedFormDQBE() { FormData = fileContent, DataFormatID = "6303", DataFormatVersionID = 44820 });
            task.Attachments = new ArchivedAttachmentExternalListDQBE();

            var attachments = attachmentInfo.Select(a => new ArchivedAttachmentDQBE() { ArchiveReference = archiveReference, AttachmentType = a.AttachmentType, FileName = a.FileName, AttachmentTypeName = a.AttachmentType }).ToList();
            attachments.ForEach(p => task.Attachments.Add(p));
            task.Reportee = "AdHocJob";

            return task;
        }

        private List<DistributionForm> GetDistributionForms(string filename)
        {
            using (StreamReader reader = new StreamReader(filename, Encoding.UTF8))
            {
                var csvReader = new CsvReader(reader, GetCsvConfiguration());
                
                csvReader.Context.RegisterClassMap<DistributionFormMap>();

                var distributionForms = csvReader.GetRecords<DistributionForm>().ToArray();
                return distributionForms.ToList();
            }
        }

        private static CsvConfiguration GetCsvConfiguration()
        {
            return new CsvConfiguration(CultureInfo.GetCultureInfo("nb-NO"))
            {
                HeaderValidated = null,
                Delimiter = ",",
                HasHeaderRecord = true,
                MissingFieldFound = null,
            };
        }

        private List<FormAttachmentMetadata> GetFormAttachmentMetadatas(string filename)
        {
            using (StreamReader reader = new StreamReader(filename, Encoding.UTF8))
            {
                var csvReader = new CsvReader(reader, GetCsvConfiguration());
                csvReader.Context.RegisterClassMap<FormAttachmentMetadataFormMap>();

                var attachmentInfo = csvReader.GetRecords<FormAttachmentMetadata>().ToArray();
                return attachmentInfo.ToList();
            }
        }
    }

    public sealed class FormAttachmentMetadataFormMap : ClassMap<FormAttachmentMetadata>
    {
        public FormAttachmentMetadataFormMap()
        {
            Map(m => m.Id).Name("Id");
            Map(m => m.ArchiveReference).Name("ArchiveReference");
            Map(m => m.AttachmentType).Name("AttachmentType");
            Map(m => m.FileName).Name("FileName");
            Map(m => m.Size).Name("Size");
            Map(m => m.FormMetadata).Ignore();
        }
    }
    public sealed class DistributionFormMap : ClassMap<DistributionForm>
    {
        public DistributionFormMap()
        {
            Map(m => m.Id).Name("Id");
            Map(m => m.InitialArchiveReference).Name("InitialArchiveReference");
            Map(m => m.DistributionType).Name("DistributionType");
            Map(m => m.ExternalSystemReference).Name("ExternalSystemReference");
            Map(m => m.DistributionStatus).Name("DistributionStatus");
            Map(m => m.SubmitAndInstantiatePrefilledFormTaskReceiptId).Name("SubmitAndInstantiatePrefilledFormTaskReceiptId");
            Map(m => m.ErrorMessage).Name("ErrorMessage");
            Map(m => m.SubmitAndInstantiatePrefilled).Name("SubmitAndInstantiatePrefilled").TypeConverter<DateConverter>();
            Map(m => m.SignedArchiveReference).Name("SignedArchiveReference");
            Map(m => m.Signed).Name("Signed").TypeConverter<DateConverter>();
            Map(m => m.RecieptSentArchiveReference).Name("RecieptSentArchiveReference");
            Map(m => m.RecieptSent).Name("RecieptSent").TypeConverter<DateConverter>();
            Map(m => m.InitialExternalSystemReference).Name("InitialExternalSystemReference");
            Map(m => m.AnSaKoStatus).Ignore();
        }
    }

    public class DateConverter : ITypeConverter
    {
        private readonly string _dateFormat;

        public DateConverter()
        {
            //_dateFormat = "yyyy-MM-dd HH:mm:ss.fff";
            _dateFormat = "yyyy-MM-dd HH:mm:ss.ffffff";
        }
        public DateConverter(string dateFormat)
        {
            _dateFormat = dateFormat;
        }

        public object ConvertFromString(string text, IReaderRow row, MemberMapData memberMapData)
        {
            if (!string.IsNullOrEmpty(text))
            {
                DateTime dt;
                DateTime.TryParseExact(text, _dateFormat,
                                       CultureInfo.InvariantCulture,
                                       DateTimeStyles.None,
                                       out dt);
                return dt;

            }

            return null;
        }
        public string ConvertToString(object value, IWriterRow row, MemberMapData memberMapData)
        {
            return ObjectToDateString(value, _dateFormat);
        }

        public string ObjectToDateString(object o, string dateFormat)
        {
            if (o == null) return string.Empty;

            DateTime dt;
            if (DateTime.TryParse(o.ToString(), out dt))
                return dt.ToString(dateFormat);
            else
                return string.Empty;
        }
        public bool IsValidSqlDateTime(DateTime? dateTime)
        {
            if (dateTime == null) return true;

            DateTime minValue = DateTime.Parse(System.Data.SqlTypes.SqlDateTime.MinValue.ToString());
            DateTime maxValue = DateTime.Parse(System.Data.SqlTypes.SqlDateTime.MaxValue.ToString());

            if (minValue > dateTime.Value || maxValue < dateTime.Value)
                return false;

            return true;
        }

    }
}
