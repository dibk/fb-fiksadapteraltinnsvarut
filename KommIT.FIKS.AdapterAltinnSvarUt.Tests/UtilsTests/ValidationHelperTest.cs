﻿using FluentAssertions;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;
using Xunit;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Tests.UtilTests
{
    public class ValidationHelperTest
    {
        [Fact]
        public void BeregnetGradAvUtnyttingArealProsent()
        {
            double eksisterende = 10.5;
            double skalRives = 2.5;
            double nytt = 2;
            double parkeringsarealTerreng = 0.5;

            var result = ValidationHelper.BeregnetGradAvUtnyttingAreal(eksisterende, skalRives, nytt, parkeringsarealTerreng);

            result.Should().Be(10.5);
        }

        [Fact]
        public void BeregnetGradAvUtnyttingAreal()
        {
            double eksisterende = 10.5;
            double skalRives = 2.5;
            double nytt = 2;
            double parkeringsarealTerreng = 0.5;
            double tomteareal = 10.5;

            var result = ValidationHelper.BeregnetGradAvUtnyttingArealProsent(eksisterende, skalRives, nytt, parkeringsarealTerreng, tomteareal);

            result.Should().Be(100);
        }


        [Fact]
        public void UtfyltOgBeregnetArealErDetSamme()
        {
            double utfyltareal = 2.05;
            double beregnetareal = 2.05;

            var result = ValidationHelper.UtfyltOgBeregnetArealErDetSamme(utfyltareal, beregnetareal);

            result.Should().Be(true);
        }

        [Fact]
        public void UtfyltOgBeregnetArealErDetSammeNårManRunderAvDesimaler()
        {
            double utfyltareal = 224.7;
            double beregnetareal = 224.70000000002;

            var result = ValidationHelper.UtfyltOgBeregnetArealErDetSamme(utfyltareal, beregnetareal);

            result.Should().Be(true);
        }
        [Fact]
        public void Servicedesk()
        {
            //https://dibk.atlassian.net/browse/FBHS-1524
            double utfyltareal = 22.13;
            double beregnetareal = 22.125;

            var result = ValidationHelper.UtfyltOgBeregnetArealErDetSamme(utfyltareal, beregnetareal);

            result.Should().Be(true);
        }
        [Fact]
        public void TestData()
        {
            //https://dibk.atlassian.net/browse/FBHS-1524
            double utfyltareal = 23.36;
            double beregnetareal = 23.362745098039216;

            var result = ValidationHelper.UtfyltOgBeregnetArealErDetSamme(utfyltareal, beregnetareal);

            result.Should().Be(true);
        }




    }
}
