﻿using System.Collections.Generic;
using FluentAssertions;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Checklist;
using Xunit;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Tests.UtilsTests
{
  public  class CheckListUtilitiesTests
    {
        [Fact]
        public void BeregnetGradAvUtnyttingArealProsent()
        {
            string rulechecklistReferenceNumber = "2.1";
            string soknadtype = "RS";
            List<string> tiltakstyperISoeknad = new List<string>()
            {
                "nyttbyggover70m2",
                "rivinghelebygg",
                "antenneover5m"
            };

            var result = new ChecklistUtilities().IsRuleRelevantForApplication(rulechecklistReferenceNumber, soknadtype, tiltakstyperISoeknad);

            result.Should().NotBeNull();
        }
    }
}
