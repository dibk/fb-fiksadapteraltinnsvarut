﻿using Xunit;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Tests.UtilsTests
{
    public class EmailValidatorTests
    {
        [Fact]
        public void IsValidEmailAddress_validInput_returnTrue()
        {
            var result = Utils.Helpers.IsValidEmailAddress("el@kanon.com");
            Assert.True(result);
        }

        [Fact]
        public void IsValidEmailAddress_trailing_dot_returnFalse()
        {
            var result = Utils.Helpers.IsValidEmailAddress("el@kanon.com.");
            Assert.False(result);
        }

        [Fact]
        public void IsValidEmailAddress_missingRoot_returnFalse()
        {
            var result = Utils.Helpers.IsValidEmailAddress("el@kanon.");
            Assert.False(result);
        }

        [Fact]
        public void IsValidEmailAddress_containingWhitespace_returnFalse()
        {
            var result = Utils.Helpers.IsValidEmailAddress("el @kanon.com");
            Assert.False(result);
        }

        [Fact]
        public void IsValidEmailAddress_containingDoubleAt_returnFalse()
        {
            var result = Utils.Helpers.IsValidEmailAddress("el@@kanon.com");
            Assert.False(result);
        }

        [Fact]
        public void IsValidEmailAddress_missingHost_returnFalse()
        {
            var result = Utils.Helpers.IsValidEmailAddress("el@");
            Assert.False(result);
        }

        [Fact]
        public void IsValidEmailAddress_emptyInput_returnFalse()
        {
            var result = Utils.Helpers.IsValidEmailAddress("");
            Assert.False(result);
        }
    }
}
