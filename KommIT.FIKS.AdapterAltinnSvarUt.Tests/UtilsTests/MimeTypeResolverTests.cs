﻿using KommIT.FIKS.AdapterAltinnSvarUt.Utils;
using Xunit;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Tests.UtilsTests
{
    public class MimeTypeResolverTests
    {
        [Fact]
        public void MimeTypes_xml()
        {
            var mime = MimeTypeResolver.GetMimeType("I am a file.xml");
            Assert.Equal("application/xml", mime);
        }

        [Fact]
        public void MimeTypes_json()
        {
            var mime = MimeTypeResolver.GetMimeType("I am a file.JSON");
            Assert.Equal("application/json", mime);
        }

        [Fact]
        public void MimeTypes_pdf()
        {
            var mime = MimeTypeResolver.GetMimeType("I am a file.pdf");
            Assert.Equal("application/pdf", mime);
        }
    }
}
