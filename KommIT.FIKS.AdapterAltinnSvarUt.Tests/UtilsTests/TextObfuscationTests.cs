﻿using FluentAssertions;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;
using Xunit;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Tests.UtilTests
{
    public class TextObfuscationTests
    {
        [Fact]
        public void ObfuscateSSN_containingSSN_returnsObfuscatedString()
        {
            var stringToObfuscate = "Invalid reportee: 12345678912";
            
            var result = TextObfuscation.ObfuscateSSN(stringToObfuscate);

            result.Should().Be("Invalid reportee: ANONYMISERT");
        }

        [Fact]
        public void ObfuscateSSN_containingMultipleOccurancesOfSSN_returnsObfuscatedString()
        {
            var stringToObfuscate = "Dist id 18deb8bb-0031-422a-b4e9-18856e102c17 - Feil ved Altinn prefill til >>>30115935379<<<: ValidationFailed Invalid Reportee  : 30115935379";

            var result = TextObfuscation.ObfuscateSSN(stringToObfuscate);

            result.Should().Be("Dist id 18deb8bb-0031-422a-b4e9-18856e102c17 - Feil ved Altinn prefill til >>>ANONYMISERT<<<: ValidationFailed Invalid Reportee  : ANONYMISERT");
        }

        [Fact]
        public void ObfuscateSSN_notContainingSSN_returnsUnchangedString()
        {
            var stringForPossibleObfuscation = "I am the string 1234567891";

            var result = TextObfuscation.ObfuscateSSN(stringForPossibleObfuscation);

            result.Should().Be(stringForPossibleObfuscation);
        }

        [Fact]
        public void ObfuscateSSN_containing10Digits_returnsUnchangedString()
        {
            var stringForPossibleObfuscation = "1234567891";

            var result = TextObfuscation.ObfuscateSSN(stringForPossibleObfuscation);

            result.Should().Be(stringForPossibleObfuscation);
        }

        [Fact]
        public void ObfuscateSSN_containing12Digits_returnsUnchangedString()
        {
            var stringForPossibleObfuscation = "123456789123";

            var result = TextObfuscation.ObfuscateSSN(stringForPossibleObfuscation);

            result.Should().Be(stringForPossibleObfuscation);
        }

        [Fact]
        public void ObfuscateSSN_containingOnlyText_returnsUnchangedString()
        {
            var stringForPossibleObfuscation = "Once upon a time there was a string named Bob Einar";

            var result = TextObfuscation.ObfuscateSSN(stringForPossibleObfuscation);

            result.Should().Be(stringForPossibleObfuscation);
        }

        [Fact]
        public void ObfuscateSSN_nullInput_returnsNull()
        {
            string stringForPossibleObfuscation = null;

            var result = TextObfuscation.ObfuscateSSN(stringForPossibleObfuscation);

            result.Should().Be(stringForPossibleObfuscation);
        }

        [Fact]
        public void ObfuscateSSN_emptyString_returnsEmptyString()
        {
            var stringForPossibleObfuscation = string.Empty;

            var result = TextObfuscation.ObfuscateSSN(stringForPossibleObfuscation);

            result.Should().Be(stringForPossibleObfuscation);
        }        
    }
}
