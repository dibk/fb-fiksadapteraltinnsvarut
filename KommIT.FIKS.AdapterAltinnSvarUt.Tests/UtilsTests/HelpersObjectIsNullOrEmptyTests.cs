﻿using System;
using Xunit;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;
using FluentAssertions;
using System.Collections.Generic;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Tests.UtilsTests
{    
    public class HelpersObjectIsNullOrEmptyTests
    {
        [Fact]
        public void HelpersObjectIsNullOrEmpty_mainPropIsNull_returnsTrue()
        {
            var testClass1 = new MyClass()
            {
                MyStringProperty = "TestingTesting",
                MyNullableIntProperty = 123,
                MyNullableBoolProperty = false
            };
            testClass1.ChildClassProperty = new MyChildClass() { ChildMainBool = null, ChildMainBoolSpecified = false };
            var result1 = Helpers.ObjectIsNullOrEmpty(testClass1.ChildClassProperty);

            var testClass2 = new MyClass()
            {
                MyStringProperty = "TestingTesting",
                MyNullableIntProperty = 666,
                MyNullableBoolProperty = false
            };
            testClass2.ChildClassProperty = new MyChildClass() { ChildMainBool = null, ChildMainBoolSpecified = true };
            var result2 = Helpers.ObjectIsNullOrEmpty(testClass2.ChildClassProperty);

            Assert.True(result1);
            result1.Should().Be(result2);
        }

        [Fact]
        public void HelpersObjectIsNullOrEmpty_Hierarchy_returnsTrue()
        {
            var testClass1 = new MyClass();
            testClass1.ChildClassProperty = new MyChildClass() { ChildMainBool = null, ChildMainBoolSpecified = false };
            var result1 = Helpers.ObjectIsNullOrEmpty(testClass1);

            var testClass2 = new MyClass();
            testClass2.ChildClassProperty = new MyChildClass() { ChildMainBool = null, ChildMainBoolSpecified = true };
            var result2 = Helpers.ObjectIsNullOrEmpty(testClass2);

            Assert.True(result1);
            result1.Should().Be(result2);
        }
      
        [Fact]
        public void HelpersObjectIsNullOrEmpty_ArrayWithNoElements_returnsTrue()
        {
            var testClass1 = new MyClass();
            testClass1.ChildClassProperty = new MyChildClass() { ChildMainBool = null, ChildMainBoolSpecified = false };
            testClass1.ChildClassProperty.ChildArray = new List<MyCollectionItem>().ToArray();
            var result1 = Helpers.ObjectIsNullOrEmpty(testClass1.ChildClassProperty);

            result1.Should().Be(true);
        }

        [Fact]
        public void HelpersObjectIsNullOrEmpty_ArrayWithOneNullElement_returnsTrue()
        {
            var testClass1 = new MyClass();
            testClass1.ChildClassProperty = new MyChildClass() { ChildMainBool = null, ChildMainBoolSpecified = false };
            testClass1.ChildClassProperty.ChildArray = new List<MyCollectionItem>() { null }.ToArray();
            var result1 = Helpers.ObjectIsNullOrEmpty(testClass1.ChildClassProperty);

            result1.Should().Be(true);
        }

        [Fact]
        public void HelpersObjectIsNullOrEmpty_ArrayWithElements_returnsFalse()
        {
            var testClass1 = new MyClass();
            testClass1.ChildClassProperty = new MyChildClass() { ChildMainBool = null, ChildMainBoolSpecified = false };
            testClass1.ChildClassProperty.ChildArray = new List<MyCollectionItem>() { null, new MyCollectionItem() { CollectionBool = true, CollectionString ="Eg"} }.ToArray();
            var result1 = Helpers.ObjectIsNullOrEmpty(testClass1.ChildClassProperty);

            result1.Should().Be(false);
        }

        [Fact]
        public void HelpersObjectIsNullOrEmpty_mainPropHasValue_returnsFalse()
        {
            var testClass1 = new MyClass()
            {
                MyStringProperty = "TestingTesting",
                MyNullableIntProperty = 123,
                MyNullableBoolProperty = false
            };
            testClass1.ChildClassProperty = new MyChildClass() { ChildMainBool = true, ChildMainBoolSpecified = true };

            var result1 = Helpers.ObjectIsNullOrEmpty(testClass1.ChildClassProperty);

            result1.Should().Be(false);
        }

        [Fact]
        public void HelpersObjectIsNullOrEmpty_parentClassHasValues_returnsFalse()
        {
            var testClass1 = new MyClass()
            {
                MyStringProperty = "TestingTesting",
                MyNullableIntProperty = 123,
                MyNullableBoolProperty = false
            };
            testClass1.ChildClassProperty = new MyChildClass() { ChildMainBool = true, ChildMainBoolSpecified = true };

            var result1 = Helpers.ObjectIsNullOrEmpty(testClass1.MyNullableIntProperty);

            result1.Should().Be(false);
        }

        [Fact]
        public void HelpersObjectIsNullOrEmpty_nullablePropertyIsNull_returnsTrue()
        {
            int? value = null;
            var result1 = Helpers.ObjectIsNullOrEmpty(value);
            result1.Should().Be(true);
        }

        [Fact]
        public void HelpersObjectIsNullOrEmpty_nullablePropertyHasValue_returnsFalse()
        {
            int? value = 1;
            var result1 = Helpers.ObjectIsNullOrEmpty(value);
            result1.Should().Be(false);
        }
    }

    public class MyClass
    {
        public string MyStringProperty { get; set; }        
        public int? MyNullableIntProperty { get; set; }
        public bool? MyNullableBoolProperty { get; set; }

        public MyChildClass ChildClassProperty { get; set; }
    }

    public class MyChildClass
    {
        public bool? ChildMainBool { get; set; }
        public bool ChildMainBoolSpecified { get; set; }

        public MyCollectionItem[] ChildArray { get; set; }
    }

    public class MyCollectionItem
    {
        public bool? CollectionBool { get; set; }
        public string CollectionString { get; set; }
    }
}
