﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Serialization;
using FluentAssertions;
using KommIT.FIKS.AdapterAltinnSvarUt.Models.ApiModels;
using KommIT.FIKS.AdapterAltinnSvarUt.Services;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Matrikkel;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.PostalCode;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Validations;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Validations;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;
using Moq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Tests
{
    public class TestHelper
    {
        private static readonly bool _externalDependences = Feature.IsFeatureEnabled(FeatureFlags.UnitTestExternalDependence);
        private static readonly bool _matrikkelFlag = Feature.IsFeatureEnabled(FeatureFlags.MatrikkelValidering);
        private static readonly bool _bringFlag = Feature.IsFeatureEnabled(FeatureFlags.BringPostnummerValidering);
        public static void AssertResult(ValidationResult result, string ruleId, int? messagesCount, bool? useBring = null, bool? useMatrikkel = null)
        {

            if (messagesCount != null)
            {



                if (useBring.HasValue || useMatrikkel.HasValue)
                {
                    //Check if feature flags are used in Unit tests 
                    if (_externalDependences)
                    {
                        if ((useBring.GetValueOrDefault(false) && _bringFlag) && (useMatrikkel.GetValueOrDefault(false) && _matrikkelFlag))
                        {
                            result.messages.Count.Should().Be(messagesCount);
                        }
                        else
                        {
                            if ((useBring.GetValueOrDefault(false) && _bringFlag))
                            {
                                result.messages.Count.Should().Be(messagesCount);
                            }
                            else if (useMatrikkel.GetValueOrDefault(false) && _matrikkelFlag)
                            {
                                result.messages.Count.Should().Be(messagesCount);
                            }
                            else
                            {
                                // feature is not active can't validete ruleId
                                return;
                            }
                        }
                    }
                    else
                    {
                        // feature is not active can't validete ruleId
                        return;
                    }
                }
                else
                {
                    result.messages.Count.Should().Be(messagesCount);
                }
            }

            if (!string.IsNullOrWhiteSpace(ruleId))
            {
                var ruleMessagetype = result.rulesChecked.First(r => r.id == ruleId);
                result.messages.Where(m => m.reference == ruleId).Should().NotBeNullOrEmpty();

                //This check doesn't work with all the validation because not all have unique rules id and text

                //result.messages.Where(m => m.reference == ruleId).All(r => r.messagetype == ruleMessagetype.messagetype).Should().BeTrue();
            }

            result.Should().NotBeNull();
        }

        public static IPostalCodeProvider MokPostalCodeProviderResult(bool? valid = null, string postalCodeType = null, string result = null)
        {
            var postalCode = new Mock<IPostalCodeProvider>();

            if (!valid.HasValue && string.IsNullOrEmpty(postalCodeType) && string.IsNullOrEmpty(result))
            {
                //Nothing to set up, returns null by default
            }
            else
            {
                postalCode.Setup(s => s.ValidatePostnr(It.IsAny<string>(), It.IsAny<string>())).Returns(new PostalCodeValidationResult() { Valid = valid.GetValueOrDefault(), PostalCodeType = postalCodeType, Result = result });
            }
            return postalCode.Object;
        }


        public static IMunicipalityValidator MokMunicipalityValidatorResult(string result = null)
        {
            var municipalityValidator = new Mock<IMunicipalityValidator>();
            result = result ?? "empty";

            MunicipalityValidation statusValidation;

            switch (result.ToLower())
            {
                case "invalid":
                    statusValidation = MunicipalityValidation.Invalid;
                    break;
                case "empty":
                    statusValidation = MunicipalityValidation.Empty;
                    break;
                case "toosoon":
                    statusValidation = MunicipalityValidation.TooSoon;
                    break;
                case "expired":
                    statusValidation = MunicipalityValidation.Expired;
                    break;
                default:
                    statusValidation = MunicipalityValidation.Ok;
                    break;
            }

            municipalityValidator.Setup(s => s.Validate_kommunenummerStatus(It.IsAny<string>())).Returns(new MunicipalityValidationResult() { Status = statusValidation, Message = $"kommunenumer er : '{statusValidation.ToString()}'." });

            return municipalityValidator.Object;
        }
        public static IMatrikkelService MokMatrikkelServiceResult(bool? matrikkelnrExist, bool? isBygningOnMatrikkelnr, bool? isVegadresseOnMatrikkelnr)
        {
            var matrikkelServiceMok = new Mock<IMatrikkelService>();

            matrikkelServiceMok.Setup(m => m.MatrikkelnrExist(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>())).Returns(matrikkelnrExist);
            matrikkelServiceMok.Setup(m => m.IsBygningOnMatrikkelnr(It.IsAny<long>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>())).Returns(isBygningOnMatrikkelnr);
            matrikkelServiceMok.Setup(m => m.IsVegadresseOnMatrikkelnr(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>())).Returns(isVegadresseOnMatrikkelnr);

            return matrikkelServiceMok.Object;
        }

        public static JArray ConverRulesCheckedJson(ValidationResult validationRule, bool? sort = false)
        {
            JArray rulesChecked = null;
            var rules = validationRule.rulesChecked;

            var rulesInClassOrder = sort.HasValue ? rules?.AsEnumerable().Reverse() : rules;

            if (rulesInClassOrder != null)
            {
                var rulesSorted = sort.GetValueOrDefault() ? rules.OrderBy(r => r.id).ToList() : rulesInClassOrder;
                rulesChecked = JArray.Parse(JsonConvert.SerializeObject(rulesSorted));
            }

            return rulesChecked;
        }

        //https://stackoverflow.com/a/40129137

        public static T GetValue<T>(Skjema formSkjema)
        {
            return SerializeUtil.DeserializeFromString<T>(formSkjema.Data);
        }

        public static bool SaveSchema2XmlFile(Skjema formSkjema, string fileName = null, string path = null, object form = null)
        {
            try
            {
                fileName = fileName ?? formSkjema.VariantId;
                path = path ?? Path.Combine(@"C:\Temp\", $"{fileName}.xml");

                string xmlData = form != null ? SerializeUtil.Serialize(form) : formSkjema.Data;

                File.WriteAllText(path, xmlData);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public static string GetXmlWithoutSpaces(string formAsXml)
        {
            Regex Parser = new Regex(@">\s*<");
            var xml = Parser.Replace(formAsXml, "><");
            xml.Trim();
            return xml;
        }

        public static JObject GetValidateFormv2FormSkjema(Skjema formSkjema, List<string> attachemnts, object form = null)
        {
            if (Helpers.ObjectIsNullOrEmpty(formSkjema))
                return null;

            string formSkjemaData = null;
            if (form != null)
                formSkjemaData = SerializeUtil.Serialize(form);

            formSkjemaData = formSkjemaData ?? formSkjema.Data;


            ValidateFormv2 validateFormv2 = new ValidateFormv2()
            {
                DataFormatVersion = formSkjema.DataFormatVersion,
                DataFormatId = formSkjema.DataFormatID,
                FormData = GetXmlWithoutSpaces(formSkjemaData)
            };

            if (attachemnts != null && attachemnts.Any())
            {
                var validateAttachments = new List<ValidateAttachment>();

                foreach (var attachemnt in attachemnts)
                {
                    ValidateAttachment validateAttachment = new ValidateAttachment()
                    {
                        Name = attachemnt,
                        Filename = $"{attachemnt}.pdf",
                        FileSize = 15
                    };
                    validateAttachments.Add(validateAttachment);
                }
                validateFormv2.AttachmentTypesAndForms = validateAttachments.ToArray();
            }
            else
            {
                validateFormv2.AttachmentTypesAndForms = new ValidateAttachment[] { };
            }

            var validateFormv2JObject = JObject.FromObject(validateFormv2);
            return validateFormv2JObject;
        }
    }

}
