using Hangfire;
using Hangfire.Client;
using Hangfire.Common;
using Hangfire.Server;
using Hangfire.States;
using Hangfire.Storage;
using Serilog;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace KommIT.FIKS.AdapterAltinnSvarUt.HangfireConfiguration
{

    //This attribute was found here: https://gist.github.com/odinserj/a8332a3f486773baa009
    public class HangfireDisableMultipleQueuedItemsAttribute : JobFilterAttribute, IClientFilter, IServerFilter, IElectStateFilter
    {
        private static readonly TimeSpan FingerprintTimeout = TimeSpan.FromHours(1);
        private static readonly TimeSpan LockTimeout = TimeSpan.FromSeconds(10);
        private ILogger _logger = Serilog.Log.ForContext<HangfireDisableMultipleQueuedItemsAttribute>();

        public void OnCreating(CreatingContext filterContext)
        {
            if (!AddFingerprintIfNotExists(filterContext.Connection, filterContext.Job))
            {                
                _logger.Information("Background job for {backgroundJobMethodName} already enqueued, cancels creation", filterContext.Job.Method.Name);
                filterContext.Canceled = true;
            }
        }

        public void OnPerformed(PerformedContext filterContext)
        {
            RemoveFingerprint(filterContext.Connection, filterContext.BackgroundJob);
        }

        public void OnStateElection(ElectStateContext context)
        {
            // If for any reason we delete a job from queue we release the lock.
            if (context.CandidateState.Name == "Deleted")
            {
                RemoveFingerprint(context.Connection, context.BackgroundJob);
            }
        }

        private static bool AddFingerprintIfNotExists(IStorageConnection connection, Job job)
        {
            using (connection.AcquireDistributedLock(GetFingerprintLockKey(job), LockTimeout))
            {
                var fingerprint = connection.GetAllEntriesFromHash(GetFingerprintKey(job));

                DateTimeOffset timestamp;

                if (fingerprint != null &&
                    fingerprint.ContainsKey("Timestamp") &&
                    DateTimeOffset.TryParse(fingerprint["Timestamp"], null, DateTimeStyles.RoundtripKind, out timestamp) &&
                    DateTimeOffset.UtcNow <= timestamp.Add(FingerprintTimeout))
                {
                    // Actual fingerprint found, returning.
                    return false;
                }

                // Fingerprint does not exist, it is invalid (no `Timestamp` key),
                // or it is not actual (timeout expired).
                connection.SetRangeInHash(
                    GetFingerprintKey(job),
                    new Dictionary<string, string>
                {
                { "Timestamp", DateTimeOffset.UtcNow.ToString("o") }
                });

                return true;
            }
        }

        private static string GetFingerprint(Job job)
        {
            string parameters = string.Empty;
            if (job.Args != null)
            {
                parameters = string.Join(".", job.Args);
            }

            if (job.Type == null || job.Method == null)
            {
                return string.Empty;
            }

            var fingerprint = $"{job.Type.Name}.{job.Method.Name}.{parameters}";

            return fingerprint;
        }

        private static string GetFingerprintKey(Job job)
        {
            return $"Fingerprint:{GetFingerprint(job)}";
        }

        private static string GetFingerprintLockKey(Job job)
        {
            return $"{GetFingerprintKey(job)}:lock";
        }

        private static void RemoveFingerprint(IStorageConnection connection, BackgroundJob job)
        {
            RemoveFingerprint(connection, job.Job);
        }

        private static void RemoveFingerprint(IStorageConnection connection, Job job)
        {
            using (connection.AcquireDistributedLock(GetFingerprintLockKey(job), LockTimeout))
            {
                using (var transaction = connection.CreateWriteTransaction())
                {
                    transaction.RemoveHash(GetFingerprintKey(job));
                    transaction.Commit();
                }
            }
        }

        void IClientFilter.OnCreated(CreatedContext filterContext)
        {
        }

        void IServerFilter.OnPerforming(PerformingContext filterContext)
        {
        }

    }
}