﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KommIT.FIKS.AdapterAltinnSvarUt.HangfireConfiguration
{
    public static class HangfireQueueNames
    {
        public const string Default = "default";
        public const string Purge = "purge";
    }
}