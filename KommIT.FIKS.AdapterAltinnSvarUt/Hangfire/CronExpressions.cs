﻿namespace KommIT.FIKS.AdapterAltinnSvarUt.HangfireConfiguration
{
    public static class CronExpressions
    {
        public static string HourInterval(int interval)
        {
            return $"0 */{interval} * * *";
        }

        public static string DailyAt(string hour = "*", string minute = "*" )
        {
            return $"{minute} {hour} * * *";
        }
    }
}