﻿using Elastic.Apm.AspNetFullFramework;
using Elastic.Apm.Logging;
using KommIT.FIKS.AdapterAltinnSvarUt.Logger;
using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using Serilog;
using System.Data.Entity;
using System.Globalization;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace KommIT.FIKS.AdapterAltinnSvarUt
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            Log.Logger = Logger.LogConfig.GetLoggerConfiguration().CreateLogger();

            Log.Logger.ForContext<MvcApplication>().Debug("Application_Start triggered");

            Database.SetInitializer(new MigrateDatabaseToLatestVersion<ApplicationDbContext, Migrations.Configuration>());

            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            var culture = new CultureInfo("nb-NO");
            Thread.CurrentThread.CurrentCulture = culture;
            Thread.CurrentThread.CurrentUICulture = culture;

            // Logging av hendelser i Apm-modulen
            //AgentDependencies.Logger = new ApmLoggerToSerilog(LogLevel.Warning);
        }

        protected void Application_End()
        {
            Log.Logger.ForContext<MvcApplication>().Debug("Application_End triggered");
            ApplicationShutdownReason shutdownReason = System.Web.Hosting.HostingEnvironment.ShutdownReason;
            Log.Logger.ForContext<MvcApplication>().Information("App is shutting down (reason = {ShutdownReason})", shutdownReason);
        }

        protected void Application_Error()
        {
            var ex = Server.GetLastError();
            Log.Logger.ForContext<MvcApplication>().Error(ex, "Error occurred");
        }
    }
}