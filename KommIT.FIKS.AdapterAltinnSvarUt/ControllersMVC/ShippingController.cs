﻿using System.Web.Mvc;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Altinn;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Controllers
{
    public class ShippingController : Controller
    {
        private readonly AltinnFormDownloadAndRoutingService _dibk_altinnFormDownloadAndRoutingService;
        private readonly AltinnFormDownloadAndRoutingService_ATIL _atil_altinnFormDownloadAndRoutingService;

        public ShippingController(AltinnFormDownloadAndRoutingService_DIBK dibk_altinnFormDownloadAndRoutingService, AltinnFormDownloadAndRoutingService_ATIL atil_AltinnFormDownloadAndRoutingService)
        {
            _dibk_altinnFormDownloadAndRoutingService = dibk_altinnFormDownloadAndRoutingService;
            _atil_altinnFormDownloadAndRoutingService = atil_AltinnFormDownloadAndRoutingService;
        }

        public ActionResult Index()
        {
            _dibk_altinnFormDownloadAndRoutingService.StartReccuringHangfireJobForForms();
            _atil_altinnFormDownloadAndRoutingService.StartReccuringHangfireJobForForms();

            return View();
        }
    }
}