﻿using KommIT.FIKS.AdapterAltinnSvarUt.Models.UserAccess;
using KommIT.FIKS.AdapterAltinnSvarUt.Models.ViewModels;
using KommIT.FIKS.AdapterAltinnSvarUt.Services;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Controllers
{
    [Authorize]
    public class ClientSystemController : Controller
    {
        private readonly ClientSystemService _clientSystemService;

        public ClientSystemController(ClientSystemService clientSystemService)
        {
            _clientSystemService = clientSystemService;
        }

        // GET: /Manage/ManageClientSystems
        public async Task<ActionResult> ManageClientSystems()
        {
            var clientSystems = _clientSystemService.GetClientSystems();
            return View(new ClientSystemAdministrationViewModel
            {
                ClientSystems = clientSystems.Select(p => new ClientSystemViewModel
                {
                    SystemKey = p.SystemKey,
                    DisplayName = p.DisplayName,
                    Description = p.Description,
                    Identifiers = p.Identifiers
                }).ToList()
            });
        }

        [HttpGet]
        public async Task<ActionResult> RegisterSystem()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> PostSystem(ClientSystemViewModel clientSystemViewModel)
        {
            try
            {
                var dbModel = new ClientSystem()
                {
                    SystemKey = clientSystemViewModel.SystemKey,
                    DisplayName = clientSystemViewModel.DisplayName,
                    Description = clientSystemViewModel.Description,
                    Identifiers = clientSystemViewModel.Identifiers
                };
                _clientSystemService.AddClientSystem(dbModel);

                return RedirectToAction("ManageClientSystems", "ClientSystem");
            }
            catch (System.Exception)
            {
                throw;
            }

            return View();
        }

        public async Task<ActionResult> EditSystem(string systemKey)
        {
            var clientSystems = _clientSystemService.GetClientSystems();

            var clientSystem = clientSystems.FirstOrDefault(p => p.SystemKey == systemKey);

            return View(new ClientSystemViewModel
            {
                SystemKey = clientSystem.SystemKey,
                DisplayName = clientSystem.DisplayName,
                Description = clientSystem.Description,
                Identifiers = clientSystem.Identifiers
            });
        }

        [HttpPost]
        public async Task<ActionResult> UpdateClientSystem(ClientSystemViewModel clientSystemViewModel)
        {
            try
            {
                var dbModel = new ClientSystem()
                {
                    SystemKey = clientSystemViewModel.SystemKey,
                    DisplayName = clientSystemViewModel.DisplayName,
                    Description = clientSystemViewModel.Description,
                    Identifiers = clientSystemViewModel.Identifiers
                };
                _clientSystemService.UpdateClientSystem(dbModel);

                return RedirectToAction("ManageClientSystems", "ClientSystem");
            }
            catch (System.Exception)
            {
                throw;
            }

            return View();
        }
    }
}