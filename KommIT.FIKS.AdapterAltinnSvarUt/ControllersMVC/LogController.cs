﻿using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Models.ApiModels;
using KommIT.FIKS.AdapterAltinnSvarUt.Models.UserAccess;
using KommIT.FIKS.AdapterAltinnSvarUt.Models.ViewModels;
using KommIT.FIKS.AdapterAltinnSvarUt.Services;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.FileDownload;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Svarut.StatusService;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Controllers
{
    [Authorize]
    public class LogController : Controller
    {
        private readonly ILogEntryService _logEntryService;
        private readonly ApplicationUserManager _userManager;
        private readonly ISvarUtStatusService _svarUtStatusService;
        private readonly IFileDownloadStatusService _fileDownloadStatusService;
        private readonly IApplicationUserService _applicationUserService;
        private readonly IFormMetadataService _formMetadataService;
        private readonly string _serverURL;

        public LogController(ILogEntryService logEntryService, IApplicationUserService applicationUserService, IFormMetadataService formMetadataService, ApplicationUserManager userManager, ISvarUtStatusService svarUtStatusService, IFileDownloadStatusService fileDownloadStatusService)
        {
            _logEntryService = logEntryService;
            _applicationUserService = applicationUserService;
            _formMetadataService = formMetadataService;
            _userManager = userManager;
            _svarUtStatusService = svarUtStatusService;
            _fileDownloadStatusService = fileDownloadStatusService;
            _serverURL = ConfigurationManager.AppSettings["ServerURL"];
        }

        // GET: Log
        public ActionResult Index(string filter, string currentFilter, int? page)
        {
            if (filter != null)
            {
                page = 1;
            }
            else
            {
                filter = currentFilter;
            }
            ViewBag.CurrentFilter = filter;
            ViewBag.isMunicipalityShown = IsUserAdminOrUserWithMultipleMunicipalities();

            int pageSize = 20;
            int pageNumber = (page ?? 1);
            int skip = 0;
            if (page.HasValue) skip = (page.Value - 1) * pageSize;

            var res = _formMetadataService.GetFormMetadataList(skip, pageSize, filter);

            List<LogByArchiveReferenceViewModel> logByArchiveReference = new List<LogByArchiveReferenceViewModel>();
            foreach (var item in res)
            {
                logByArchiveReference.Add(new LogByArchiveReferenceViewModel(item));
            }
            LogViewModel viewModel = new LogViewModel();
            viewModel.log = logByArchiveReference;

            return View(viewModel);
        }

        // GET: Details
        public ActionResult Details(string archivereference)
        {
            LogDetailsViewModel logDetailsViewModel = GetLogDetailsForArchiveReference(archivereference);
            if (HasAccessToDetailPage(logDetailsViewModel))
            {
                return View(logDetailsViewModel);
            }
            return HttpNotFound("Access denied");
        }

        private LogDetailsViewModel GetLogDetailsForArchiveReference(string archivereference)
        {
            List<LogEntry> logEntries = _logEntryService.GetLogEntriesByArchiveReference(archivereference);
            FormMetadata formMetadata = _formMetadataService.GetFormMetadata(archivereference);
            List<FileDownloadStatus> fileDownloadStatuses = _fileDownloadStatusService.GetFileDownloadStatusesForArchiveReference(archivereference);

            // Alle filer burde være tilgjengelige i dette viewet...
            if (fileDownloadStatuses.Any())
            {
                var receiptFile = fileDownloadStatuses.FirstOrDefault(f => FileDownloadApiUrlProvider.ReceiptTypes.Contains(f.FileType));

                if (receiptFile != null)
                    formMetadata.DistributionRecieptLink = new FileDownloadApiUrlProvider(_serverURL).GenerateDownloadUri(receiptFile);
            }

            var logDetailsForArchiveReference = new LogDetailsViewModel(logEntries, formMetadata)
            {
                SvarUtStatus = GetSvarUtShipmentStatus(archivereference),
            };

            var decision = _formMetadataService.GetDecision(archivereference);
            if (decision != null)
            {
                List<Vilkaar> listvilkaar = new List<Vilkaar>();
                foreach (var term in decision.Terms)
                {
                    listvilkaar.Add(new Vilkaar(term.TermsId, term.TermForProcess, term.TermDescription));
                }
                logDetailsForArchiveReference.Vilkaar = listvilkaar;
            }
            var listDistribusjon = _formMetadataService.GetDistributionForm(archivereference);
            logDetailsForArchiveReference.Distribusjoner = new List<Distribusjon>();
            foreach (var distributionItem in listDistribusjon)
            {
                var filInformasionForNedlastings = new List<FilInformasjonForNedlasting>();

                if (!string.IsNullOrEmpty(distributionItem.SignedArchiveReference))
                {
                    foreach (var fileDownload in _fileDownloadStatusService.GetFileDownloadStatusesForArchiveReference(distributionItem.SignedArchiveReference)
                )
                    {
                        filInformasionForNedlastings.Add(
                            new FilInformasjonForNedlasting()
                            {
                                FilURL = FileDownloadApiUrlProvider.GenerateDownloadApiUri(fileDownload),
                                Filbeskrivelse = fileDownload.FileType.ToString(),
                                Mimetype = fileDownload.MimeType,
                                Filnavn = fileDownload.Filename
                            });
                    }
                }
                logDetailsForArchiveReference.Distribusjoner.Add(new Distribusjon()
                {
                    Distribusjonstype = distributionItem.DistributionType,
                    ReferanseSluttbrukersystem = distributionItem.ExternalSystemReference,
                    Status = distributionItem.DistributionStatus.ToString(),
                    ReferanseIdSendt = distributionItem.SubmitAndInstantiatePrefilledFormTaskReceiptId,
                    Feilmelding = distributionItem.ErrorMessage,
                    Sendt = distributionItem.SubmitAndInstantiatePrefilled,
                    ReferanseIdSignert = distributionItem.SignedArchiveReference,
                    Signert = distributionItem.Signed,
                    ReferanseIdKvittering = distributionItem.RecieptSentArchiveReference,
                    SendtKvittering = distributionItem.RecieptSent,
                    Filnedlastinger = filInformasionForNedlastings,
                    ReferanseOpprinneligDistribusjonSluttbrukersystem = distributionItem.InitialExternalSystemReference
                });
            }

            return logDetailsForArchiveReference;
        }

        private string GetSvarUtShipmentStatus(string archivereference)
        {
            string forsendelsesId = _formMetadataService.GetForsendelsesIdByArchiveReference(archivereference);

            if (string.IsNullOrEmpty(forsendelsesId))
            {
                return "Ikke satt";
            }
            else
            {
                return _svarUtStatusService.GetShipmentStatus(forsendelsesId);
            }
        }

        private bool HasAccessToDetailPage(LogDetailsViewModel logDetailsViewModel)
        {
            return _applicationUserService.AccessDetailsPage(_userManager.FindById(User.Identity.GetUserId()), logDetailsViewModel.Municipality);
        }

        private bool IsUserAdminOrUserWithMultipleMunicipalities()
        {
            bool isUserAdminOrUserWithMultipleMunicipalities;

            ApplicationUser user = _userManager.FindById(User.Identity.GetUserId());
            if (_applicationUserService.IsAdmin())
            {
                isUserAdminOrUserWithMultipleMunicipalities = true;
            }
            else if (user.MunicipalitiesToAdministrate.Count > 1)
            {
                isUserAdminOrUserWithMultipleMunicipalities = true;
            }
            else
            {
                isUserAdminOrUserWithMultipleMunicipalities = false;
            }

            return isUserAdminOrUserWithMultipleMunicipalities;
        }
    }
}