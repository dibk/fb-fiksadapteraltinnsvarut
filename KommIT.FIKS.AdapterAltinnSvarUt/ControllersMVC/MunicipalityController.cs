﻿using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Services;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Views
{
    public class MunicipalityController : Controller
    {
        public MunicipalityController(IMunicipalityService municipalityService)
        {
            _municipalityService = municipalityService;
        }

        private readonly IMunicipalityService _municipalityService;
        public Serilog.ILogger Logger { private get; set; }

        // GET: Municipality/Edit/5
        public ActionResult Edit(string municipalityCode)
        {
            if (String.IsNullOrEmpty(municipalityCode))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Municipality municipality = _municipalityService.GetMunicipalityWithAccessControl(municipalityCode, User.Identity.GetUserId());
            if (municipality == null)
            {
                Logger.Information("Access denied for User {user}[{userId}] at /Municipality/Edit/{municipality}", User.Identity.Name, User.Identity.GetUserId(), municipalityCode);
                return new HttpUnauthorizedResult();
            }

            //ViewBag.MunicipalityCode = municipalityClassification.MunicipalityCode;
            return View(municipality);
        }

        // POST: Municipality/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Code, Name, OrganizationNumber, PlanningDepartmentSpecificOrganizationNumber")] Municipality municipalityForm)
        {
            Municipality municipality = null;
            if (ModelState.IsValid)
            {
                municipality = _municipalityService.GetMunicipalityWithAccessControl(municipalityForm.Code, User.Identity.GetUserId());
                if (municipality == null)
                {
                    Logger.Information("Access denied for User {user}[{userId}] at /Municipality/Edit/{municipality}", User.Identity.Name, User.Identity.GetUserId(), municipalityForm.Code);
                    return new HttpUnauthorizedResult();
                }
                municipality.OrganizationNumber = municipalityForm.OrganizationNumber;
                municipality.PlanningDepartmentSpecificOrganizationNumber = municipalityForm.PlanningDepartmentSpecificOrganizationNumber;
                _municipalityService.UpdateMunicipality(municipality);
                return RedirectToAction("Forms", "FormSettings", new { MunicipalityCode = municipality.Code });
            }
            ViewBag.MunicipalityCode = municipality.Code;
            return View(municipality);
        }
    }
}