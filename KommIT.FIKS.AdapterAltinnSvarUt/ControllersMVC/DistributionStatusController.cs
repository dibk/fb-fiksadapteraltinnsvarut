﻿using System.Web.Mvc;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Controllers
{
    [Authorize]    
    public class DistributionStatusController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}
