﻿using System.Collections.Generic;
using System.Web.Mvc;
using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Models.ViewModels;
using KommIT.FIKS.AdapterAltinnSvarUt.Services;
using Microsoft.AspNet.Identity;
using Serilog;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Controllers
{
    [Authorize]
    public class FormSettingsController : Controller
    {
        private readonly IMunicipalityService _municipalityService;
        private readonly IFormService _formService;
        public ILogger Logger { private get; set; }

        public FormSettingsController(IMunicipalityService municipalityService, IFormService formService)
        {
            _municipalityService = municipalityService;
            _formService = formService;
        }
        
        /// <summary>
        /// Show list of municipalities which the current user has access to administrate.
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            List<Municipality> municipalities = _municipalityService.GetMunicipalitiesForUser(User.Identity.GetUserId());
            List<MunicipalityViewModel> municipalityViewModels = new MunicipalityViewModelMapper().MapToViewModel(municipalities);
            return View(municipalityViewModels);
        }

        /// <summary>
        /// Show list of form settings for a specific municipality.
        /// </summary>
        /// <param name="municipalityCode"></param>
        /// <returns></returns>
        public ActionResult Forms(string municipalityCode)
        {
            var userId = User.Identity.GetUserId();
            Municipality municipality = _municipalityService.GetMunicipalityWithAccessControl(municipalityCode, userId);

            if (municipality == null)
            {
                Logger.Information("Access denied for User {user}[{userId}] at /FormSettings/Forms/{municipality}", User.Identity.Name, User.Identity.GetUserId(), municipalityCode);
                return new HttpUnauthorizedResult();
            }

            MunicipalitySettingsViewModel settingsViewModel = new MunicipalitySettingsViewModelMapper().MapToViewModel(municipality);

            return View(settingsViewModel);
        }
    }
}