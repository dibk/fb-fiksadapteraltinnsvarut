using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Models.UserAccess;
using KommIT.FIKS.AdapterAltinnSvarUt.Services;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Extensions.Logging;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private readonly ApplicationSignInManager _signInManager;
        private readonly IAuthenticationManager _authManager;
        private readonly ApplicationDbContext _context;
        private readonly IMunicipalityService _municipalityService;
        private readonly ClientSystemService _clientSystemService;
        private readonly ApplicationUserManager _userManager;

        public ILogger<AccountController> _logger;

        public AccountController(ApplicationUserManager userManager,
                                 ApplicationSignInManager signInManager,
                                 IAuthenticationManager authManager,
                                 ApplicationDbContext context,
                                 IMunicipalityService municipalityService,
                                 ClientSystemService clientSystemService,
                                 ILogger<AccountController> logger)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _authManager = authManager;
            _context = context;
            _municipalityService = municipalityService;
            _clientSystemService = clientSystemService;
            _logger = logger;
        }

        public ApplicationSignInManager SignInManager
        {
            get { return _signInManager; }
        }

        public ApplicationUserManager UserManager
        {
            get { return _userManager; }
        }

        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // This doesn't count login failures towards account lockout
            // To enable password failures to trigger account lockout, change to shouldLockout: true
            var result = await SignInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, shouldLockout: false);

            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);

                case SignInStatus.LockedOut:
                    return View("Lockout");

                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = model.RememberMe });

                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid login attempt.");
                    return View(model);
            }
        }

        //
        // GET: /Account/VerifyCode
        [AllowAnonymous]
        public async Task<ActionResult> VerifyCode(string provider, string returnUrl, bool rememberMe)
        {
            // Require that the user has already logged in via username/password or external login
            if (!await SignInManager.HasBeenVerifiedAsync())
            {
                return View("Error");
            }
            return View(new VerifyCodeViewModel { Provider = provider, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/VerifyCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyCode(VerifyCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // The following code protects for brute force attacks against the two factor codes.
            // If a user enters incorrect codes for a specified amount of time then the user account
            // will be locked out for a specified amount of time.
            // You can configure the account lockout settings in IdentityConfig
            var result = await SignInManager.TwoFactorSignInAsync(model.Provider, model.Code, isPersistent: model.RememberMe, rememberBrowser: model.RememberBrowser);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(model.ReturnUrl);

                case SignInStatus.LockedOut:
                    return View("Lockout");

                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid code.");
                    return View(model);
            }
        }

        //
        // GET: /Account/Register
        [Authorize(Roles = ApplicationRole.Administrator)]
        public ActionResult Register()
        {
            RegisterViewModel model = new RegisterViewModel();
            var municipalities = _context.Municipalities.AsEnumerable()
            .Select(x => new
            {
                Municipalities = x.Code,
                Name = $"{x.Name} ({x.Code})"
            }).ToList();
            ViewBag.Municipalities = new MultiSelectList(municipalities, "Municipalities", "Name");
            return View(model);
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [Authorize(Roles = ApplicationRole.Administrator)]
        public async Task<ActionResult> Register(RegisterViewModel model, string[] Municipalities)
        {
            var allErrors = ModelState.Values.SelectMany(v => v.Errors);
            if (ModelState.IsValid)
            {
                var userStore = new UserStore<ApplicationUser>(_context);
                var userManager = new UserManager<ApplicationUser>(userStore);
                var password = "0A" + Membership.GeneratePassword(12, 1);

                var municipalities = _context.Municipalities.AsEnumerable()
                .Select(x => new
                {
                    Municipalities = x.Code,
                    Name = $"{x.Name} ({x.Code})"
                }).ToList();
                ViewBag.Municipalities = new MultiSelectList(municipalities, "Municipalities", "Name");

                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                var result = await UserManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    await SendConfirmationEmail(user);

                    userManager.AddToRole(user.Id, model.Role);
                    _municipalityService.UpdateMunicipalityApplicationUser(Municipalities, user.Id);

                    return RedirectToAction("ManageLogins", "Manage");
                }
                AddErrors(result);
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        // GET: /Account/Edit
        [Authorize(Roles = ApplicationRole.Administrator)]
        public ActionResult Edit(string userId)
        {
            RegisterViewModel model = new RegisterViewModel();

            if (ModelState.IsValid)
            {
                var user = UserManager.FindByName(userId);

                if (user == null)
                {
                    return View("Error");
                }

                model.UserId = user.Id;
                var RolesForUser = UserManager.GetRoles(user.Id);

                model.Email = user.Email;
                if (RolesForUser[0] != null)
                    model.Role = RolesForUser[0];

                //Load municipalities for user
                var municipalities = _context.Municipalities.AsEnumerable()
                                 .Select(x => new
                                 {
                                     Municipalities = x.Code,
                                     Name = $"{x.Name} ({x.Code})"
                                 }).ToList();
                ViewBag.Municipalities = new MultiSelectList(municipalities, "Municipalities", "Name");

                // TODO FIX THIS!
                List<string> municipalityCodes = _municipalityService.GetMunicipalitiesForUser(user.Id).Select(m => m.Code).ToList();
                ViewBag.MunicipalitiesSelected = municipalityCodes;

                //Load client system for user
                var systems = _clientSystemService.GetClientSystems().Select(x => new
                {
                    SystemKey = x.SystemKey,
                    Name = $"{x.DisplayName}"
                }).ToList();

                ViewBag.ClientSystem = new SelectList(systems, "SystemKey", "Name");

                var claims = UserManager.GetClaims(user.Id);
                var clientsystemClaims = claims.Where(c => c.Type == "clientsystem").Select(c => c.Value).ToList();
                ViewBag.ClientSystemSelected = clientsystemClaims;
            }

            return View(model);
        }

        // GET: /Account/EditUser
        [HttpPost]
        [Authorize(Roles = ApplicationRole.Administrator)]
        public ActionResult EditUser(RegisterViewModel model, string[] Municipalities, string[] ClientSystems)
        {
            if (ModelState.IsValid)
            {
                var user = UserManager.FindByEmail(model.UserId);

                var userEmail = UserManager.FindByEmail(model.Email);

                if (user == null)
                {
                    return View("Error");
                }

                if (userEmail != null)
                {
                    if (user.Id != userEmail.Id)
                    {
                        ModelState.AddModelError("emailAlreadyTaken", new Exception("Please choose another email"));
                    }
                }

                user.Email = model.Email;
                user.UserName = model.Email;

                //Update role for user
                var userRoles = user.Roles.ToList();

                foreach (var role in userRoles)
                {
                    user.Roles.Remove(role);
                }

                var roleID = _context.Roles.Where(x => x.Name == model.Role).FirstOrDefault().Id;

                user.Roles.Add(new IdentityUserRole { UserId = user.Id, RoleId = roleID });
                UserManager.Update<ApplicationUser, string>(user);

                _municipalityService.UpdateMunicipalityApplicationUser(Municipalities, user.Id);

                //Update client system for user
                var userClaims = UserManager.GetClaims(user.Id); user.Claims.Where(c => c.ClaimType == "clientsystem").ToList();
                foreach (var claim in userClaims)
                {
                    UserManager.RemoveClaim(user.Id, claim);
                }

                if (ClientSystems != null)
                    foreach (var system in ClientSystems)
                    {
                        UserManager.AddClaim(user.Id, new System.Security.Claims.Claim("clientsystem", system));
                    }
                
                if (User.IsInRole("administrator"))
                {
                    return RedirectToAction("ManageLogins", "Manage");
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }

            return View("Edit", model);
        }

        [AllowAnonymous]
        public async Task<ActionResult> ConfirmAccount(string userId, string code)
        {
            if (userId == null || code == null)
            {
                _logger.LogError("ConfirmAccount failed - userId={0}, code={1}", userId, code);
                return View("Error");
            }

            var accountIsAlreadyConfirmed = await UserManager.IsEmailConfirmedAsync(userId);
            if (accountIsAlreadyConfirmed == true)
            {
                _logger.LogError("ConfirmAccount failed - account already confirmed, userId={0}", userId);
                //   return View("Error");
            }
            else
            {
                var result = await UserManager.ConfirmEmailAsync(userId, code);
                if (!result.Succeeded)
                {
                    var errors = result.Errors.Aggregate((concat, s) => $"{concat} - {s}");
                    _logger.LogError("Unable to confirm account for userId={0} - errors: {1}", userId, errors);
                    return View("Error");
                }
                {
                    _logger.LogInformation("Account confirmed for userId={0}", userId);
                }
            }

            Session.Abandon(); //this force a new session to be generated and a new request forgery token to be generated
            return View();
        }

        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        [HttpPost]
        public async Task<ActionResult> ConfirmAccount(ConfirmAccountViewModel model)
        {
            if (ModelState.IsValid)
            {
                var userId = model.userId;
                var applicationUser = await UserManager.FindUserById(userId);
                _logger.LogInformation($"Adding new password for user: {userId}, after account confirmation.");
                var result = await UserManager.PasswordValidator.ValidateAsync(model.Password);
                if (result.Succeeded)
                {
                    if (UserManager.HasPassword(userId))
                        await UserManager.RemovePasswordAsync(userId);

                    await UserManager.AddPasswordAsync(userId, model.Password);
                    await SignInManager.SignInAsync(applicationUser, isPersistent: false, rememberBrowser: false);
                    return RedirectToAction("Index", "Home");
                }
                ModelState.AddModelError("password", "The password must have lowercase, uppercase and numbers.");
            }
            return View(model);
        }

        //
        // GET: /Account/ForgotPassword
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        //
        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByNameAsync(model.Email);
                if (user == null)
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    return View("ForgotPasswordConfirmation");
                }

                if (!(await UserManager.IsEmailConfirmedAsync(user.Id))) // When the user haven't confirmed it's email account reset password should send a new configrmation email.
                {
                    await SendConfirmationEmail(user);

                    return View("ForgotPasswordConfirmation");
                }

                // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                // Send an email with this link
                string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                await UserManager.SendEmailAsync(user.Id, "Reset Password", "Please reset your password by clicking <a href=\"" + callbackUrl + "\">here</a>");
                return RedirectToAction("ForgotPasswordConfirmation", "Account");
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ForgotPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        //
        // GET: /Account/ResetPassword
        [AllowAnonymous]
        public ActionResult ResetPassword(string code)
        {
            return code == null ? View("Error") : View();
        }

        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await UserManager.FindByNameAsync(model.Email);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            var result = await UserManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            AddErrors(result);
            return View();
        }

        //
        // GET: /Account/ResetPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        //
        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }

        //
        // GET: /Account/SendCode
        [AllowAnonymous]
        public async Task<ActionResult> SendCode(string returnUrl, bool rememberMe)
        {
            var userId = await SignInManager.GetVerifiedUserIdAsync();
            if (userId == null)
            {
                return View("Error");
            }
            var userFactors = await UserManager.GetValidTwoFactorProvidersAsync(userId);
            var factorOptions = userFactors.Select(purpose => new SelectListItem { Text = purpose, Value = purpose }).ToList();
            return View(new SendCodeViewModel { Providers = factorOptions, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/SendCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SendCode(SendCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            // Generate the token and send it
            if (!await SignInManager.SendTwoFactorCodeAsync(model.SelectedProvider))
            {
                return View("Error");
            }
            return RedirectToAction("VerifyCode", new { Provider = model.SelectedProvider, ReturnUrl = model.ReturnUrl, RememberMe = model.RememberMe });
        }

        //
        // GET: /Account/ExternalLoginCallback
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return RedirectToAction("Login");
            }

            // Sign in the user with this external login provider if the user already has a login
            var result = await SignInManager.ExternalSignInAsync(loginInfo, isPersistent: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);

                case SignInStatus.LockedOut:
                    return View("Lockout");

                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = false });

                case SignInStatus.Failure:
                default:
                    // If the user does not have an account, then prompt the user to create an account
                    ViewBag.ReturnUrl = returnUrl;
                    ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
                    return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { Email = loginInfo.Email });
            }
        }

        //
        // POST: /Account/ExternalLoginConfirmation
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Manage");
            }

            if (ModelState.IsValid)
            {
                // Get the information about the user from the external login provider
                var info = await AuthenticationManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                var result = await UserManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await UserManager.AddLoginAsync(user.Id, info.Login);
                    if (result.Succeeded)
                    {
                        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                        return RedirectToLocal(returnUrl);
                    }
                }
                AddErrors(result);
            }

            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        //
        // POST: /Account/LogOff
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Index", "Home");
        }

        //
        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        private async Task SendConfirmationEmail(ApplicationUser user)
        {
            string token = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
            _logger.LogInformation($"User created: id={user.Id} email={user.UserName} Email confirmation token={token}");

            var callbackUrl = Url.Action("ConfirmAccount", "Account", new { userId = user.Id, code = token }, protocol: Request.Url.Scheme);
            _logger.LogInformation($"Callback url for confirmation: {callbackUrl}");

            var emailBody = string.Format(Resources.Web.ConfirmAccountEmailBody, user.UserName, callbackUrl);
            await UserManager.SendEmailAsync(user.Id, Resources.Web.ConfirmAccountEmailSubject, emailBody);
        }

        #region Helpers

        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get { return _authManager; }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }

        #endregion Helpers
    }
}