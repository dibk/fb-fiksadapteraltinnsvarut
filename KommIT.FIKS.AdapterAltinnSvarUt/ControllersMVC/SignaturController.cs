﻿using System.Configuration;
using System.Web.Mvc;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Controllers
{
    public class SignaturController : Controller
    {
        // GET: Signatur
        public ActionResult Tiltakshaver()
        {
            var sabyClientHost = ConfigurationManager.AppSettings["Tiltakshaverssignatur:ClientUrl"];

            if (string.IsNullOrEmpty(sabyClientHost))
                return View();
            else
            {
                var context = HttpContext;
                var path = context.Request.Path;
                
                //Hent ut samtykke id
                var id = path.Substring(path.LastIndexOf('/') + 1);

                return Redirect($"{sabyClientHost}/{id}");
            }
        }
    }
}