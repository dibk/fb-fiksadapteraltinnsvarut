﻿using KommIT.FIKS.AdapterAltinnSvarUt.Models.UserAccess;
using KommIT.FIKS.AdapterAltinnSvarUt.Models.ViewModels;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Authentication;
using Microsoft.AspNet.Identity;
using System.Linq;
using System.Web.Mvc;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Controllers
{
    [Authorize(Roles = ApplicationRole.SystemIntegrator)]
    public class ApiAuthenticationController : Controller
    {
        private readonly IAPIAuthenticationService _apiAuthenticationService;

        public ApiAuthenticationController(IAPIAuthenticationService apiAuthenticationService)
        {
            _apiAuthenticationService = apiAuthenticationService;
        }

        // GET: ApiAuthentication
        public ActionResult Index()
        {
            ViewBag.StatusMessage = "";
            //message == ManageMessageId.ChangePasswordSuccess ? "Your password has been changed."

            ApiAuthenticationViewModel model = new ApiAuthenticationViewModel();
            var usersApiUsername = _apiAuthenticationService.GetApiUsernameForUserId(User.Identity.GetUserId());
            if (usersApiUsername != null)
            {
                model.Username = usersApiUsername;
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult Index(ApiAuthenticationViewModel model)
        {
            var allErrors = ModelState.Values.SelectMany(v => v.Errors);
            if (ModelState.IsValid)
            {
                //ApplicationUser applicationUser = UserManager.FindById(User.Identity.GetUserId());
                var user = new APIAuthentication(model.Username, model.Password, User.Identity.GetUserId());
                bool isRequestSuccess = _apiAuthenticationService.AddOrUpdateApiCredentials(user);
                ViewBag.StatusMessage = "Api bruker er opprettet";
            }
            else
            {
                ViewBag.StatusMessage = "Noe gikk galt";
            }

            return View(model);
        }
    }
}