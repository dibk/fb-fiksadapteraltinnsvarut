﻿using Serilog;
using System.Web.Http.Filters;

namespace KommIT.FIKS.AdapterAltinnSvarUt.ErrorHandler
{
    public class LogExceptionFilterWebApiAttribute :  ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            var logger = Log.ForContext<LogExceptionFilterWebApiAttribute>();
            logger.Error(actionExecutedContext.Exception, "An error occured");

            base.OnException(actionExecutedContext);
        }
    }
}