﻿using Serilog;
using System.Web.Mvc;

namespace KommIT.FIKS.AdapterAltinnSvarUt.ErrorHandler
{
    public class LogExceptionFilterMvcAttribute : System.Web.Mvc.IExceptionFilter
    {
        public void OnException(ExceptionContext filterContext)
        {
            var logger = Log.ForContext<LogExceptionFilterWebApiAttribute>();
            logger.Error(filterContext.Exception, "An error occured");
        }
    }
}