using FtB_SubmittalQueueRepository;
using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Services;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Altinn;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Azure;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Encryption;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Notification;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Prefill;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.WS.Altinn;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;
using Newtonsoft.Json;
using Serilog;
using System;
using System.Configuration;
using System.IO;
using System.Text;

namespace KommIT.FIKS.AdapterAltinnSvarUt.QueuePreparators
{
    [NotificationServiceDataFormatFilter(SupportedDataFormatIds = new string[] { "6326" })]
    public class ExportToSubmittalQueueService : IFormDistributionServiceV2, INotificationServiceV2
    {
        private readonly string _serviceBusConnectionString = ConfigurationManager.AppSettings["FtBSenderServiceBusConnectionString"];
        private readonly string _queueName = ConfigurationManager.AppSettings["FtBSubmittalQueue"];
        private readonly ILogger _logger;
        private readonly ILogEntryService _logEntryService;
        private readonly IFileStorage _privateFileStorage;
        private readonly IPublicFileStorage _publicFileStorage;

        private IServiceBusQueueClientFactory _serviceBusQueueClientFactory;

        public ExportToSubmittalQueueService(ILogger logger,
                                             ILogEntryService logEntryService,
                                             IFileStorage fileStorage,
                                             IPublicFileStorage publicFileStorage)
        {
            _serviceBusQueueClientFactory = new ServiceBusQueueClientFactory(_serviceBusConnectionString);
            _logger = logger;
            _logEntryService = logEntryService;
            _privateFileStorage = fileStorage;
            _publicFileStorage = publicFileStorage;
        }

        public void SaveArchivedItemToBlobStorage(string containerName, ArchivedFormTaskDQBE archivedForm)
        {
            try
            {
                var encryptedReporteeId = new DecryptionFactory().GetDecryptor().EncryptText(archivedForm.Reportee);
                string containerNameLowerCase = containerName.ToLower();
                var archivedItemInfo = new ArchivedItemInformation()
                {
                    ArchiveReference = archivedForm.ArchiveReference,
                    DataFormatID = archivedForm.Forms[0].DataFormatID,
                    DataFormatVersionID = archivedForm.Forms[0].DataFormatVersionID,
                    ServiceCode = archivedForm.ServiceCode,
                    ServiceEditionCode = archivedForm.ServiceEditionCode,
                    EncryptedReporteeId = encryptedReporteeId
                };
                SerializeAndStoreData(archivedForm.ArchiveReference, containerName, archivedItemInfo, BlobStorageMetadataTypeEnum.ArchivedItemInformation);
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Error occurred when storing archived item {ArchiveReference} to storage {Container}", archivedForm?.ArchiveReference, containerName);
                _logEntryService.Save(new LogEntry(archivedForm.ArchiveReference, $"En feil oppstod ved lagring av informasjon om innsending: {ex.Message}", "Error"));
                throw;
            }
        }

        private void SerializeAndStoreData(string archiveReference, string containerName, object data, BlobStorageMetadataTypeEnum type)
        {
            var metadata = new BlobMetadata()
            {
                BlobType = type.ToString()
            };

            try
            {
                _logger.Information($"Trying to store {type} to blobstorage");
                string serializedData = JsonConvert.SerializeObject(data);
                _privateFileStorage.EstablishFolder(containerName.ToLower(), containerName.ToLower());
                string f = _privateFileStorage.AddFileAsByteArrayToFolder(containerName.ToLower()
                                   , $"{type}_{archiveReference}.json"
                                   , Encoding.UTF8.GetBytes(serializedData)
                                   , "application/json"
                                   , metadata.ToMetadataItems());
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Error occurred when storing {Type} to storage {Container}", type, containerName);
                _logEntryService.Save(new LogEntry(containerName.ToUpper(), $"En feil oppstod ved lagring av informasjon om innsending: {ex.Message}", "Error"));
                throw;
            }
        }

        public void Distribute(FormData formData)
        {
            try
            {
                string publicBlobContainerName = Guid.NewGuid().ToString();

                SaveMainFormAndAttachmentsToPublicBlobStorage(publicBlobContainerName, formData);
                SaveFormDataToPrivateBlobStorage(formData.ArchiveReference.ToLower(), formData, publicBlobContainerName);

                var containerMetadata = new BlobMetadata()
                {
                    ArchiveReference = formData.ArchiveReference,
                    DateCreated = DateTime.Now
                };

                _publicFileStorage.AddMetadataToContainer(publicBlobContainerName, containerMetadata.ToMetadataItems());

                Enqueue(formData.ArchiveReference);
                _logEntryService.Save(new LogEntry(formData.ArchiveReference, $"Skjema er lagt i kø for forbredelse til Altinn-distribusjon.", "Info"));
            }
            catch (Exception ex)
            {                
                _logger.Error(ex, "Error occurred when storing data and enqueuing for distribution");
                _logEntryService.Save(new LogEntry(formData.ArchiveReference, $"En feil oppstod ved køing for innsendingskø: {ex.Message}", "Error"));
                throw;
            }
        }

        private void Enqueue(string archiveReference)
        {
            archiveReference = archiveReference.ToLower();
            var qc = new QueueClient(_serviceBusQueueClientFactory, _queueName);
            var queueItem = new SubmittalQueueItem { ArchiveReference = archiveReference };
            AsyncHelper.RunSync(async () => await qc.AddToQueue(queueItem));
        }

        private void SaveFormDataToPrivateBlobStorage(string containerName, FormData formData)
        {
            SaveFormDataToPrivateBlobStorage(containerName, formData, null);
        }

        private void SaveFormDataToPrivateBlobStorage(string containerName, FormData formData, string publicBlobContainerName)
        {
            try
            {
                string containerNameLowerCase = containerName.ToLower();
                _logEntryService.Save(new LogEntry(formData.ArchiveReference, $"Formdata skal persisteres i private BlobStorage.", "Info"));
                _privateFileStorage.EstablishFolder(containerNameLowerCase, containerNameLowerCase);

                byte[] formXMLAsByteArray = System.Text.Encoding.UTF8.GetBytes(formData.FormDataAsXml);

                var blobMetadata = new BlobMetadata()
                {
                    BlobType = BlobStorageMetadataTypeEnum.FormData.ToString(),
                    PublicBlobContainerName = !string.IsNullOrEmpty(publicBlobContainerName) ? publicBlobContainerName : null,
                    DataFormatId = formData.MainformDataFormatID,
                    DataFormatVersion = formData.MainformDataFormatVersionID,
                    AttachmentTypeName = AttachmentSetting.GetMainformDataType(formData.MainFormPdf.AttachmentTypeName)
                };

                string f = _privateFileStorage.AddFileAsByteArrayToFolder(containerNameLowerCase
                                        , $"Formdata_{formData.ArchiveReference}_{formData.MainformDataFormatID}_{formData.MainformDataFormatVersionID}.xml"
                                        , formXMLAsByteArray
                                        , "application/xml"
                                        , blobMetadata.ToMetadataItems());

                _logEntryService.Save(new LogEntry(formData.ArchiveReference, $"Formdata er persistert i BlobStorage.", "Info"));
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Error occurred when persisting formdata {FormName} to storage {Container}", formData?.Mainform?.GetName(), containerName);
                _logEntryService.Save(new LogEntry(formData.ArchiveReference, $"En feil oppstod ved lagring av formdata til BlobStorage: {ex.Message}", "Error"));
                throw;
            }
        }

        private void SaveMainFormAndAttachmentsToPublicBlobStorage(string blobContainerName, FormData formData)
        {
            SaveMainFormAndAttachmentsToBlobStorage((IFileStorage)_publicFileStorage, blobContainerName, formData);
        }

        private void SaveMainFormAndAttachmentsToPrivateBlobStorage(string blobContainerName, FormData formData)
        {
            SaveMainFormAndAttachmentsToBlobStorage(_privateFileStorage, blobContainerName, formData);
        }

        private void SaveMainFormAndAttachmentsToBlobStorage(IFileStorage fileStorage, string blobContainerName, FormData formData)
        {
            try
            {
                _logEntryService.Save(new LogEntry(formData.ArchiveReference, $"Skjemadata og vedlegg skal persisteres i BlobStorage.", "Info"));
                fileStorage.EstablishFolder(blobContainerName, blobContainerName);

                //Persisting main pdf
                var mainPdfBlobMetadata = new BlobMetadata()
                {
                    BlobType = BlobStorageMetadataTypeEnum.MainForm.ToString(),
                    AttachmentTypeName = AttachmentSetting.GetMainformDataType(formData.MainFormPdf.AttachmentTypeName),
                };

                var MainFileUri = fileStorage.AddFileAsByteArrayToFolder(blobContainerName
                    , formData.MainFormPdf.FileName
                    , formData.MainFormPdf.AttachmentData
                    , MimeTypeResolver.GetMimeType(formData.MainFormPdf.FileName)
                    , mainPdfBlobMetadata.ToMetadataItems());

                var vedleggsopplysninger = formData.GetVedleggOpplysningerSkjema();

                //Persisting all attachments
                foreach (var attachment in formData.Attachments)
                {
                    try
                    {

                    var index = formData.Attachments.IndexOf(attachment);
                    var attachmentFileExtention = Path.GetExtension(attachment.FileName).Substring(1);

                    var blobMetadata = new BlobMetadata()
                    {
                        BlobType = BlobStorageMetadataTypeEnum.SubmittalAttachment.ToString(),
                        AttachmentTypeName = AttachmentSetting.GetAttachmentType(attachment),
                    };

                    var metaDataItems = blobMetadata.ToMetadataItems();
                    var attachmentMetadata = attachment.GetVedleggMetadata(vedleggsopplysninger);

                    if (attachmentMetadata != null)
                        metaDataItems.AddRange(attachmentMetadata.ToMetadataItems());

                    var attachmentUri = fileStorage.AddFileAsByteArrayToFolder(blobContainerName
                                        , $"{index}_{attachment.FileName}"
                                        , attachment.AttachmentData
                                        , MimeTypeResolver.GetMimeType(attachment.FileName)
                                        , metaDataItems);
                    }
                    catch (Exception ex)
                    {
                        _logger.Error(ex, "Error occurred when persisting attachment {AttachmentFilename} with {AttachmentTypeName} to storage {Container}.", attachment?.FileName, attachment?.AttachmentTypeName, blobContainerName);
                        throw;
                    }
                }

                _logEntryService.Save(new LogEntry(formData.ArchiveReference, $"Skjemadata og vedlegg er persistert i BlobStorage.", "Info"));
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Error occurred when persisting attachments to storage.");
                _logEntryService.Save(new LogEntry(formData.ArchiveReference, $"En feil oppstod ved lagring til BlobStorage: {ex.Message}", "Error"));
                throw;
            }
        }

        public void SaveValidationResultToBlobStorage(string containerName, ValidationResult validationResult)
        {
            SerializeAndStoreData(containerName, containerName, validationResult, BlobStorageMetadataTypeEnum.ValidationResult);
        }

        public void SendNotification(FormData formData)
        {
            StoreInPrivateBlobAndEnqueue(formData);
        }

        public void HandoverToCoreFunctions(FormData formData)
        {
            StoreInPrivateBlobAndEnqueue(formData);
        }

        private void StoreInPrivateBlobAndEnqueue(FormData formData)
        {
            try
            {
                SaveFormDataToPrivateBlobStorage(formData.ArchiveReference.ToLower(), formData);
                SaveMainFormAndAttachmentsToPrivateBlobStorage(formData.ArchiveReference.ToLower(), formData);
                StoreAllSubformsInBlob(formData); //PDF and xml

                Enqueue(formData.ArchiveReference);
                _logEntryService.Save(new LogEntry(formData.ArchiveReference, $"Skjema er lagt i kø for forbredelse til Altinn-distribusjon.", "Info"));
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Error occurred when storing data and enqueuing");
                _logEntryService.Save(new LogEntry(formData.ArchiveReference, $"En feil oppstod ved køing for innsendingskø: {ex.Message}", "Error"));
                throw;
            }
        }

        private void StoreAllSubformsInBlob(FormData formData)
        {
            foreach (var subform in formData.Subforms)
            {
                StoreSubformInBlob(formData.ArchiveReference.ToLower(), subform);
            }
        }

        private void StoreSubformInBlob(string containerName, Subform subform)
        {
            try
            {
                _logEntryService.Save(new LogEntry(containerName, $"Subforms skal persisteres i BlobStorage.", "Info"));
                _privateFileStorage.EstablishFolder(containerName, containerName);

                var subformAttachmentTypeName = AttachmentSetting.GetSubformDataType(subform.FormName);

                //Persisting subform pdf
                var subformPdfBlobMetadata = new BlobMetadata()
                {
                    BlobType = BlobStorageMetadataTypeEnum.Subform.ToString(),
                    AttachmentTypeName = subformAttachmentTypeName,
                    DataFormatId = subform.DataFormatId,
                    DataFormatVersion = subform.DataFormatVersion,
                    SubformName = subform.FormName,
                };

                var fileUri = _privateFileStorage.AddFileAsByteArrayToFolder(containerName
                    , $"{subform.FormName}.pdf"
                    , subform.PdfFileBytes
                    , $"application/pdf"
                    , subformPdfBlobMetadata.ToMetadataItems());

                //Persisting subform xml
                var subformXmlBlobMetadata = new BlobMetadata()
                {
                    BlobType = BlobStorageMetadataTypeEnum.Subform.ToString(),
                    AttachmentTypeName = subformAttachmentTypeName,
                    DataFormatId = subform.DataFormatId,
                    DataFormatVersion = subform.DataFormatVersion,
                    SubformName = subform.FormName,
                };

                byte[] formDataXml = new UTF8Encoding(true).GetBytes(subform.FormDataXml);

                fileUri = _privateFileStorage.AddFileAsByteArrayToFolder(containerName
                    , $"{subform.FormName}.xml"
                    , formDataXml
                    , $"application/xml"
                    , subformXmlBlobMetadata.ToMetadataItems());

                _logEntryService.Save(new LogEntry(containerName.ToUpper(), $"Subforms er persistert i BlobStorage.", "Info"));
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Error occurred when persisting subform {SubForm} to storage {Container}.", subform?.FormName, containerName);
                _logEntryService.Save(new LogEntry(containerName.ToUpper(), $"En feil oppstod ved lagring til BlobStorage: {ex.Message}", "Error"));
                throw;
            }
        }
    }
}