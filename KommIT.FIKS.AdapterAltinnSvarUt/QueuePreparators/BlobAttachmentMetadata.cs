﻿using KommIT.FIKS.AdapterAltinnSvarUt.Services.Azure;
using System;
using System.Collections.Generic;
using System.Linq;

namespace KommIT.FIKS.AdapterAltinnSvarUt.QueuePreparators
{
    public class BlobAttachmentMetadata
    {
        public string VedleggId { get; set; }
        public string VedleggFilnavn { get; set; }
        public string VedleggIfcValidationId { get; set; }
        public string VedleggVersjonsnr { get; set; }
        public DateTime? VedleggVersjonsdato { get; set; }
        public string VedleggKategori { get; set; }
        public string VedleggBeskrivelse { get; set; }
        public bool? VedleggFulgtNabovarsel { get; set; }

        public List<KeyValuePair<string, string>> ToKeyValuePairs()
        {
            return ToMetadataItems().Select(s => s.ToKVP()).ToList();
        }

        public List<MetadataItem> ToMetadataItems()
        {
            var metadataItems = new List<MetadataItem>();
            if (!string.IsNullOrEmpty(VedleggId))
                metadataItems.Add(new MetadataItem() { Key = BlobAttachmentMetadataKeys.VedleggId, Value = System.Web.HttpUtility.UrlEncode(VedleggId) });

            if (!string.IsNullOrEmpty(VedleggFilnavn))
                metadataItems.Add(new MetadataItem() { Key = BlobAttachmentMetadataKeys.VedleggFilnavn, Value = System.Web.HttpUtility.UrlEncode(VedleggFilnavn) });

            if (!string.IsNullOrEmpty(VedleggIfcValidationId))
                metadataItems.Add(new MetadataItem() { Key = BlobAttachmentMetadataKeys.VedleggIfcValidationId, Value = System.Web.HttpUtility.UrlEncode(VedleggIfcValidationId) });

            if (!string.IsNullOrEmpty(VedleggVersjonsnr))
                metadataItems.Add(new MetadataItem() { Key = BlobAttachmentMetadataKeys.VedleggVersjonsnr, Value = System.Web.HttpUtility.UrlEncode(VedleggVersjonsnr) });

            if (VedleggVersjonsdato.HasValue)
                metadataItems.Add(new MetadataItem() { Key = BlobAttachmentMetadataKeys.VedleggVersjonsdato, Value = VedleggVersjonsdato.Value.ToString("yyyyMMdd") });

            if (!string.IsNullOrEmpty(VedleggKategori))
                metadataItems.Add(new MetadataItem() { Key = BlobAttachmentMetadataKeys.VedleggKategori, Value = System.Web.HttpUtility.UrlEncode(VedleggKategori) });

            if (VedleggFulgtNabovarsel.HasValue)
                metadataItems.Add(new MetadataItem() { Key = BlobAttachmentMetadataKeys.VedleggFulgtNabovarsel, Value = VedleggFulgtNabovarsel.Value.ToString() });

            if (string.IsNullOrEmpty(VedleggBeskrivelse))
                metadataItems.Add(new MetadataItem() { Key = BlobAttachmentMetadataKeys.VedleggBeskrivelse, Value = System.Web.HttpUtility.UrlEncode(VedleggBeskrivelse) });

            return metadataItems;
        }

        private static class BlobAttachmentMetadataKeys
        {
            public static string VedleggId => "VedleggId";
            public static string VedleggFilnavn => "VedleggFilnavn";
            public static string VedleggIfcValidationId => "VedleggIfcValidationId";
            public static string VedleggVersjonsnr => "VedleggVersjonsnr";
            public static string VedleggVersjonsdato => "VedleggVersjonsdato";
            public static string VedleggKategori => "VedleggKategori";
            public static string VedleggFulgtNabovarsel => "VedleggFulgtNabovarsel";
            public static string VedleggBeskrivelse => "VedleggBeskrivelse";
        }
    }
}