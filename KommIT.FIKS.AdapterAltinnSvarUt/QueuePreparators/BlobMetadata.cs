﻿using KommIT.FIKS.AdapterAltinnSvarUt.Services.Azure;
using System;
using System.Collections.Generic;

namespace KommIT.FIKS.AdapterAltinnSvarUt.QueuePreparators
{
    public class BlobMetadata
    {
        public string AttachmentTypeName { get; set; }
        public string SubformName { get; set; }
        public string BlobType { get; set; }
        public string ArchiveReference { get; set; }
        public DateTime? DateCreated { get; set; }
        public string PublicBlobContainerName { get; set; }
        public string DataFormatId { get; set; }
        public string DataFormatVersion { get; set; }

        public List<MetadataItem> ToMetadataItems()
        {
            var metadataItems = new List<MetadataItem>();
            if (!string.IsNullOrEmpty(AttachmentTypeName))
                metadataItems.Add(new MetadataItem() { Key = BlobMetadataKeys.AttachmentTypeName, Value = System.Web.HttpUtility.UrlEncode(AttachmentTypeName) });

            if (!string.IsNullOrEmpty(PublicBlobContainerName))
                metadataItems.Add(new MetadataItem() { Key = BlobMetadataKeys.PublicBlobContainerName, Value = PublicBlobContainerName });

            if (!string.IsNullOrEmpty(BlobType))
                metadataItems.Add(new MetadataItem() { Key = BlobMetadataKeys.BlobType, Value = BlobType });

            if (!string.IsNullOrEmpty(ArchiveReference))
                metadataItems.Add(new MetadataItem() { Key = BlobMetadataKeys.ArchiveReference, Value = ArchiveReference });

            if (DateCreated.HasValue)
                metadataItems.Add(new MetadataItem() { Key = BlobMetadataKeys.DateCreated, Value = DateCreated.Value.ToString("yyyyMMdd") });

            if (!string.IsNullOrEmpty(DataFormatId))
                metadataItems.Add(new MetadataItem() { Key = BlobMetadataKeys.DataFormatId, Value = DataFormatId });

            if (!string.IsNullOrEmpty(DataFormatVersion))
                metadataItems.Add(new MetadataItem() { Key = BlobMetadataKeys.DataFormatVersion, Value = DataFormatVersion });

            if (!string.IsNullOrEmpty(SubformName))
                metadataItems.Add(new MetadataItem() { Key = BlobMetadataKeys.SubformName, Value = System.Web.HttpUtility.UrlEncode(SubformName) });

            return metadataItems;
        }

        private static class BlobMetadataKeys
        {
            public static string AttachmentTypeName => "AttachmentTypeName";
            public static string PublicBlobContainerName => "PublicBlobContainerName";
            public static string BlobType => "Type";
            public static string ArchiveReference => "ArchiveReference";
            public static string DateCreated => "DateCreated";
            public static string DataFormatId => "DataFormatId";
            public static string DataFormatVersion => "DataFormatVersion";
            public static string SubformName => "SubformName";
        }
    }
}