﻿namespace KommIT.FIKS.AdapterAltinnSvarUt.QueuePreparators
{
    public class ArchivedItemInformation
    {
        public string ArchiveReference { get; set; }
        public string DataFormatID { get; set; }
        public int DataFormatVersionID { get; set; }
        public string ServiceCode { get; set; }
        public string ServiceEditionCode { get; set; }
        public string EncryptedReporteeId { get; set; }
    }
}