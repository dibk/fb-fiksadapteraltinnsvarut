﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Caching;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;
using Newtonsoft.Json;
using Serilog;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.PostalCode
{
    public class BringPostalCodeProvider : IPostalCodeProvider, IDisposable
    {
        public string URL = "https://api.bring.com/shippingguide/api/postalCode.json";
        private readonly ILogger logger = Log.ForContext<BringPostalCodeProvider>();
        private ICacheService _cacheService;

        public BringPostalCodeProvider(ICacheService cacheService)
        {
            _cacheService = cacheService;
        }

        public BringPostalCodeProvider()
        {
            _cacheService = MemoryCacheService.Instance;
        }

        public PostalCodeValidationResult ValidatePostnr(string pnr, string country)
        {
            if (string.IsNullOrEmpty(pnr))
                return null;

            if (CountryCodeHandler.IsCountryNorway(country))
                country = "NO";

            //Check cache
            var cachedItem = _cacheService.GetObject($"PostalCodeValidation-{pnr}-{country}") as PostalCodeValidationResult;
            if (cachedItem != null)
            {
                logger.Verbose("Found in cache {postalCode} - {country}", pnr, country);
                return cachedItem;
            }

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(URL);

            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            // Add Client-URL to header, requierment from bring com.
            client.DefaultRequestHeaders.Add("X-Bring-Client-URL", @"https://dibk.no/");

            try
            {
                var queryParameters = $"?pnr={pnr}&country={country.ToUpper()}";

                HttpResponseMessage response = AsyncHelper.RunSync(async () => await client.GetAsync(queryParameters));

                if (response.IsSuccessStatusCode)
                {
                    //Parse the response body.
                    var dataObjects = AsyncHelper.RunSync(async () => await response.Content.ReadAsStringAsync());
                    var bringModel = JsonConvert.DeserializeObject<PostalCodeValidationResult>(dataObjects);
                    logger.Debug("Successfully retrieved postal code information from {URL}. Input: {postalCode} - {countryCode}", URL, pnr, country);

                    //Add to cache
                    _cacheService.AddOrReplaceCachedItem($"PostalCodeValidation-{pnr}-{country}", bringModel, DateTimeOffset.Now.AddHours(6));
                    logger.Verbose("Added to cache {postalCode} - {country}", pnr, country);

                    return bringModel;
                }
            }
            catch (Exception e)
            {
                logger.Error(e, "An error occurred while retrieving postal code from {URL}. Input: {postalCode} - {countryCode}", URL, pnr, country);
            }
            return null;
        }

        public void Dispose()
        {
            _cacheService = null;
        }
    }
}