﻿namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.PostalCode
{
    public interface IPostalCodeProvider
    {
        PostalCodeValidationResult ValidatePostnr(string pnr, string country);
    }
}