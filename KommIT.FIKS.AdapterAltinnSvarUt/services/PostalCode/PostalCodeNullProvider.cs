﻿namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.PostalCode
{
    public class PostalCodeNullProvider : IPostalCodeProvider
    {
        public PostalCodeValidationResult ValidatePostnr(string pnr, string country)
        {
            return null;
        }
    }
}