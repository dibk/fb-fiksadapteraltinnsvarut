﻿using KommIT.FIKS.AdapterAltinnSvarUt.Utils;
using Serilog;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.PostalCode
{
    public interface IPostalCodeProviderFactory
    {
        IPostalCodeProvider GetPostalCodeProvider();
    }
    public class PostalCodeProviderFactory : IPostalCodeProviderFactory
    {
        private readonly ILogger _logger = Log.ForContext<PostalCodeProviderFactory>();
        public static bool _isFeatureEnabled = Feature.IsFeatureEnabled(FeatureFlags.BringPostnummerValidering);

        public IPostalCodeProvider GetPostalCodeProvider()
        {
            if (_isFeatureEnabled)
                return new BringPostalCodeProvider();
            else
            {
                _logger.Debug("Postal code validation turned off, return NULL-provider");
                return new PostalCodeNullProvider(); }
        }
    }
}