﻿namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.PostalCode
{
    public class PostalCodeValidationResult
    {
        public string Result { get; set; }
        public bool Valid { get; set; }
        public string PostalCodeType { get; set; }
    }
}