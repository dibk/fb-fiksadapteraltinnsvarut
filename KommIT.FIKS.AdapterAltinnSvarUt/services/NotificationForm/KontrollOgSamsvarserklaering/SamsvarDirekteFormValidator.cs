﻿using DIBK.FBygg.Altinn.MapperCodelist;
using KommIT.FIKS.AdapterAltinnSvarUt.Logger;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Matrikkel;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Validations;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;
using no.kxml.skjema.dibk.samsvarserklaeringDirekteOpprettet;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.NotificationForm
{
    /// <summary>
    /// Validator for Erklæring om ansvarsett
    /// </summary>
    public class SamsvarDirekteFormValidator
    {
        private ILogger _logger = Log.ForContext<SamsvarDirekteFormValidator>();

        private ValidationResult _validationResult;
        CodeListService _codeListService = new CodeListService();
        private readonly string _skjema = "samsvarserklaeringDirekteOpprettet";

        public IMunicipalityValidator MunicipalityValidator { get; set; }
        public SamsvarDirekteFormValidator()
        {
            var form = new SamsvarserklaeringDirekteType();
            MunicipalityValidator = new MunicipalityValidator();
            _validationResult = new ValidationResult();
            _validationResult.AddRule(_skjema, "5315.1.1", "", "Innhold må være i henhold til informasjonsmodell/xsd for skjema.", "", "ERROR", "");
            _validationResult.AddRule(_skjema, "5315.1.3", "", "Systemet må fylle ut samme navn som er brukt i registrering for Altinn API i fraSluttbrukersystem.", Helpers.GetFullClassPath(() => form.fraSluttbrukersystem), "ERROR", "");

            //Eiendommens
            _validationResult.AddRule(_skjema, "5315.1.4", "", "Du må oppgi hvilken eiendom/hvilke eiendommer byggesøknaden gjelder.", Helpers.GetFullClassPath(() => form.eiendomByggested[0]), "ERROR", "");
            _validationResult.AddRule(_skjema, "5315.1.4.1", "", "Kommunenummer må fylles ut for eiendom/byggested.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].eiendomsidentifikasjon.kommunenummer), "ERROR", "");
            _validationResult.AddRule(_skjema, "5315.1.4.2", "", "Kommunenummer for eiendom/byggested finnes ikke i kodeliste: https://register.geonorge.no/sosi-kodelister/inndelinger/inndelingsbase/kommunenummer", Helpers.GetFullClassPath(() => form.eiendomByggested[0].eiendomsidentifikasjon.kommunenummer), "ERROR", "");
            _validationResult.AddRule(_skjema, "5315.1.4.3", "", "Kommunenummer for eiendom/byggested har ugyldig status. Du kan sjekke status på https://register.geonorge.no/sosi-kodelister/inndelinger/inndelingsbase/kommunenummer", Helpers.GetFullClassPath(() => form.eiendomByggested[0].eiendomsidentifikasjon.kommunenummer), "ERROR", "");//
            _validationResult.AddRule(_skjema, "5315.1.4.4", "", "Gårdsnummer og bruksnummer må fylles ut for eiendom/byggested.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].eiendomsidentifikasjon), "ERROR", "");
            //_validationResult.AddRule(_skjema, "5315.1.4.5", "", "Gårdsnummer '{0}' og bruksnummer '{1}' for eiendom/byggested må være tall.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].eiendomsidentifikasjon.gaardsnummer), "ERROR", "");
            _validationResult.AddRule(_skjema, "5315.1.4.6", "1.3", "Gårdsnummer '{0}' for eiendom/byggested må være '0' eller større.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].eiendomsidentifikasjon.gaardsnummer), "ERROR", null);
            _validationResult.AddRule(_skjema, "5315.1.4.6.1", "1.4", "Bruksnummer '{0}' for eiendom/byggested må være '0' eller større.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].eiendomsidentifikasjon.bruksnummer), "ERROR", null);
            _validationResult.AddRule(_skjema, "5315.1.4.7", "", "Hvis bygningsnummer {0} er oppgitt for eiendom/byggested, må det være et tall.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].bygningsnummer), "ERROR", "");
            _validationResult.AddRule(_skjema, "5315.1.4.8", "", "Bygningsnummer for eiendom/byggested må være større enn '0'.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].bygningsnummer), "ERROR", "");
            //Eiendommens --> Adress
            _validationResult.AddRule(_skjema, "5315.1.4.9", "", "Postadresse for eiendom/byggested bør fylles ut.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].adresse), "WARNING", "");
            _validationResult.AddRule(_skjema, "5315.1.4.9.1", "", "Adresselinje 1 for eiendom/byggested bør fylles ut.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].adresse.adresselinje1), "WARNING", "");
            _validationResult.AddRule(_skjema, "5315.1.4.9.2", "", "Du bør også oppgi gatenavn, husnummer og eventuell bokstav for eiendom/byggested. Du kan sjekke riktig adresse på https://seeiendom.no", Helpers.GetFullClassPath(() => form.eiendomByggested[0].adresse.gatenavn), "WARNING", "");//
            _validationResult.AddRule(_skjema, "5315.1.4.10", "", "Kommunenavn bør fylles ut for eiendom/byggested.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].kommunenavn), "WARNING", "");
            _validationResult.AddRule(_skjema, "5315.1.4.11", "", "Når bruksenhetsnummer/'bolignummer' er fylt ut, må det følge riktig format (f. eks. H0101). Se https://www.kartverket.no/eiendom/adressering/bruksenhetsnummer/.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].bolignummer), "ERROR", "");

            //if (_matrikkelFlag)
            //{
            _validationResult.AddRule(_skjema, "5315.1.4.12", "", "Hvis Eiendomsidentifikasjon [{0}-{1}/{2}/{3}/{4}] er oppgitt for eiendom/byggested, bør den være gyldig i matrikkelen. Du kan sjekke riktig informasjon på https://seeiendom.no", Helpers.GetFullClassPath(() => form.eiendomByggested[0].eiendomsidentifikasjon), "WARNING", "");
            _validationResult.AddRule(_skjema, "5315.1.4.13", "", "Eiendomsidentifikasjon [{0}-{1}/{2}/{3}/{4}] for eiendom/byggested ble ikke bekreftet mot matrikkelen.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].eiendomsidentifikasjon), "WARNING", "");
            _validationResult.AddRule(_skjema, "5315.1.4.14", "", "Hvis bygningsnummer [{0}] er oppgitt for eiendom/byggested, bør den være gyldig i matrikkelen på aktuelt matrikkelnummer. Du kan sjekke riktig bygningsnummer på https://seeiendom.no", Helpers.GetFullClassPath(() => form.eiendomByggested[0].bygningsnummer), "WARNING", "");
            _validationResult.AddRule(_skjema, "5315.1.4.15", "", "Du har oppgitt feil gateadresse for eiendom/byggested. Du kan sjekke riktig adresse på https://seeiendom.no", Helpers.GetFullClassPath(() => form.eiendomByggested[0].adresse.gatenavn), "ERROR", "");
            //}

            //Ansvarsrett
            _validationResult.AddRule(_skjema, "5315.1.6", "", "Ansvarsrett må fylles ut.", Helpers.GetFullClassPath(() => form.ansvarsrett), "ERROR", "");
            _validationResult.AddRule(_skjema, "5315.1.6.1", "", "'Funksjon' for ansvarsrett må fylles ut.", Helpers.GetFullClassPath(() => form.ansvarsrett.funksjon), "ERROR", "");

            _validationResult.AddRule(_skjema, "5315.1.1.2", "", "Kodeverdien for 'funksjon' for ansvarsrett må fylles ut.", Helpers.GetFullClassPath(() => form.ansvarsrett.funksjon.kodeverdi), "ERROR", "");
            _validationResult.AddRule(_skjema, "5315.1.2.2", "", "Ugyldig kodeverdi '{0}' i henhold til kodeliste for 'funksjon' for ansvarsrett. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/byggesoknad/funksjon", Helpers.GetFullClassPath(() => form.ansvarsrett.funksjon.kodeverdi), "ERROR", "");

            _validationResult.AddRule(_skjema, "5315.1.6.2", "", "Ugyldig funksjon '{0}' for samsvar. Du kan sjekke riktig funksjon på https://register.geonorge.no/byggesoknad/funksjon", Helpers.GetFullClassPath(() => form.ansvarsrett.funksjon.kodeverdi), "ERROR", "");
            _validationResult.AddRule(_skjema, "5315.1.6.3", "", "For funksjonen 'Ansvarlig prosjektering', må foretaket krysse av for at det ikke er avdekket gjenstående arbeider innenfor arbeidsområdet som er til hinder for å gi tillatelse.", Helpers.GetFullClassPath(() => form.ansvarsrett.prosjekterende), "ERROR", "");
            _validationResult.AddRule(_skjema, "5315.1.6.4", "", "For funksjonen 'Ansvarlig prosjektering', må foretaket krysse av for hvilken tillatelse som kan gis innenfor arbeidsområdet.", Helpers.GetFullClassPath(() => form.ansvarsrett.prosjekterende), "ERROR", "");
            _validationResult.AddRule(_skjema, "5315.1.6.5", "", "For funksjonen 'Ansvarlig prosjektering', kan foretaket bare krysse av for én tillatelse innenfor arbeidsområdet.", Helpers.GetFullClassPath(() => form.ansvarsrett.prosjekterende), "ERROR", "");
            _validationResult.AddRule(_skjema, "5315.1.6.6", "", "For funksjonen 'Ansvarlig utførelse', må informasjon om utførelsen fylles ut.", Helpers.GetFullClassPath(() => form.ansvarsrett.utfoerende), "ERROR", "");
            _validationResult.AddRule(_skjema, "5315.1.6.7", "", "For funksjonen 'Ansvarlig utførelse', kan foretaket bare krysse av for enten ferdigattest eller midlertidig brukstillatelse.", Helpers.GetFullClassPath(() => form.ansvarsrett.utfoerende), "ERROR", "");
            _validationResult.AddRule(_skjema, "5315.1.6.8", "", "For funksjonen 'Ansvarlig utførelse', må foretaket krysse av for om tiltaket har tilstrekkelig sikkerhet.", Helpers.GetFullClassPath(() => form.ansvarsrett.utfoerende.harTilstrekkeligSikkerhet), "ERROR", "");
            _validationResult.AddRule(_skjema, "5315.1.6.9", "", "For funksjonen 'Ansvarlig utførelse' og midlertidig brukstillatelse, må gjenstående arbeider fylles ut.", Helpers.GetFullClassPath(() => form.ansvarsrett.utfoerende.midlertidigBrukstillatelseGjenstaaende), "ERROR", "");
            _validationResult.AddRule(_skjema, "5315.1.6.10", "", "For funksjonen 'Ansvarlig utførelse' og midlertidig brukstillatelse, må gjenstående arbeider fylles ut innenfor delen av tiltaket det søkes midlertidig brukstillatelse for.", Helpers.GetFullClassPath(() => form.ansvarsrett.utfoerende.midlertidigBrukstillatelseGjenstaaende.gjenstaaendeInnenfor), "ERROR", "");
            _validationResult.AddRule(_skjema, "5315.1.6.12", "", "For funksjonen 'Ansvarlig utførelse' og midlertidig brukstillatelse, må type arbeider fylles ut.", Helpers.GetFullClassPath(() => form.ansvarsrett.utfoerende.typeArbeider), "ERROR", "");
            _validationResult.AddRule(_skjema, "5315.1.6.13", "", "For funksjonen 'Ansvarlig utførelse' og midlertidig brukstillatelse, må datoen for når arbeidet vil bli utført innen, fylles ut.", Helpers.GetFullClassPath(() => form.ansvarsrett.utfoerende.utfoertInnen), "ERROR", "");
            _validationResult.AddRule(_skjema, "5315.1.6.14", "", "For 'Ansvarlig utførelse' og midlertidig brukstillatelse, må datoen for når arbeidet vil bli utført, være innen 14 dager.", Helpers.GetFullClassPath(() => form.ansvarsrett.utfoerende.utfoertInnen), "ERROR", "");
            _validationResult.AddRule(_skjema, "5315.1.6.15", "", "Beskrivelse av ansvarsområdet må fylles ut.", Helpers.GetFullClassPath(() => form.ansvarsrett.beskrivelseAvAnsvarsomraadet), "ERROR", "");
            _validationResult.AddRule(_skjema, "5315.1.6.16", "", "Du må bekrefte at prosjektering er i samsvar med produksjonsunderlag.", Helpers.GetFullClassPath(() => form.erklaeringProsjektering), "ERROR", "");

            //Ny Foretak
            _validationResult.AddRule(_skjema, "5315.1.7", "", "Foretak må fylles ut.", Helpers.GetFullClassPath(() => form.foretak), "ERROR", "");
            _validationResult.AddRule(_skjema, "5315.1.7.1", "", "Kodeverdien for 'partstype' for foretak må fylles ut.", Helpers.GetFullClassPath(() => form.foretak.partstype.kodeverdi), "ERROR", "");
            _validationResult.AddRule(_skjema, "5315.1.7.2", "", "Ugyldig kodeverdi '{0}' i henhold til kodeliste for 'partstype' for foretak. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/byggesoknad/partstype", Helpers.GetFullClassPath(() => form.foretak.partstype.kodeverdi), "ERROR", "");
            _validationResult.AddRule(_skjema, "5315.1.7.3", "", "Foretaket må være en organisasjon.", Helpers.GetFullClassPath(() => form.foretak.foedselsnummer), "ERROR", "");
            _validationResult.AddRule(_skjema, "5315.1.7.4", "", "Organisasjonsnummer for foretaket må fylles ut.", Helpers.GetFullClassPath(() => form.foretak.organisasjonsnummer), "ERROR", "");
            _validationResult.AddRule(_skjema, "5315.1.7.5", "", "Organisasjonsnummeret ('{0}') for foretaket har ikke gyldig kontrollsiffer.", Helpers.GetFullClassPath(() => form.foretak.organisasjonsnummer), "ERROR", "");
            _validationResult.AddRule(_skjema, "5315.1.7.6", "", "Organisasjonsnummeret ('{0}') for foretaket er ikke gyldig.", Helpers.GetFullClassPath(() => form.foretak.organisasjonsnummer), "ERROR", "");
            _validationResult.AddRule(_skjema, "5315.1.7.7", "", "Adresse bør fylles ut for foretaket.", Helpers.GetFullClassPath(() => form.foretak.adresse.adresselinje1), "WARNING", "");
            _validationResult.AddRule(_skjema, "5315.1.7.8", "", "Adresselinje 1 bør fylles ut for foretaket.", Helpers.GetFullClassPath(() => form.foretak.adresse.adresselinje1), "WARNING", "");
            _validationResult.AddRule(_skjema, "5315.1.7.9", "", "Ugyldig landkode for foretaket.", Helpers.GetFullClassPath(() => form.foretak.adresse.landkode), "WARNING", "");
            _validationResult.AddRule(_skjema, "5315.1.7.10", "", "Postnummer for foretaket bør angis.", Helpers.GetFullClassPath(() => form.foretak.adresse.postnr), "WARNING", "");
            _validationResult.AddRule(_skjema, "5315.1.7.11", "", "Postnummeret '{0}' for foretaket har ikke gyldig kontrollsiffer. Du kan sjekke riktig postnummer på http://adressesok.bring.no/", Helpers.GetFullClassPath(() => form.foretak.adresse.postnr), "WARNING", "");
            _validationResult.AddRule(_skjema, "5315.1.7.12", "", "Postnummeret '{0}' for foretaket er ugyldig. Du kan sjekke riktig postnummer på http://adressesok.bring.no/", Helpers.GetFullClassPath(() => form.foretak.adresse.postnr), "WARNING", "");
            _validationResult.AddRule(_skjema, "5315.1.7.13", "", "Postnummeret '{0}' for foretaket stemmer ikke overens med poststedet '{1}'. Postnummeret er fra '{2}'. Du kan sjekke riktig Postnummer/poststed på http://adressesok.bring.no/", Helpers.GetFullClassPath(() => form.foretak.adresse.postnr), "WARNING", "");
            _validationResult.AddRule(_skjema, "5315.1.7.14", "", "Postnummeret til foretaket ble ikke validert.", Helpers.GetFullClassPath(() => form.foretak.adresse.postnr), "WARNING", "");
            _validationResult.AddRule(_skjema, "5315.1.7.15", "", "Navnet til foretaket må fylles ut.", Helpers.GetFullClassPath(() => form.foretak.navn), "ERROR", "");
            _validationResult.AddRule(_skjema, "5315.1.7.16", "", "Navnet til kontaktpersonen for foretaket bør fylles ut.", Helpers.GetFullClassPath(() => form.foretak.kontaktperson.navn), "WARNING", "");
            _validationResult.AddRule(_skjema, "5315.1.7.17", "", "Mobilnummer eller telefonnummer for foretaket bør fylles ut.", Helpers.GetFullClassPath(() => form.foretak), "WARNING", "");
            _validationResult.AddRule(_skjema, "5315.1.7.18", "", "E-postadresse for foretaket bør fylles ut.", Helpers.GetFullClassPath(() => form.foretak.epost), "WARNING", "");


            //Ny søker
            _validationResult.AddRule(_skjema, "5315.1.8", "", "Ansvarlig søker må fylles ut.", Helpers.GetFullClassPath(() => form.ansvarligSoeker), "ERROR", "");
            _validationResult.AddRule(_skjema, "5315.1.8.1", "", "Kodeverdien for 'partstype' for ansvarlig søker må fylles ut.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.partstype.kodeverdi), "ERROR", "");
            _validationResult.AddRule(_skjema, "5315.1.8.2", "", "Ugyldig kodeverdi '{0}' i henhold til kodeliste for 'partstype' for ansvarlig søker. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/byggesoknad/partstype", Helpers.GetFullClassPath(() => form.ansvarligSoeker.partstype.kodeverdi), "ERROR", "");
            _validationResult.AddRule(_skjema, "5315.1.8.3", "", "Ansvarlig søker må være en organisasjon.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.foedselsnummer), "ERROR", "");
            _validationResult.AddRule(_skjema, "5315.1.8.4", "", "Organisasjonsnummer for ansvarlig søker må fylles ut.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.organisasjonsnummer), "ERROR", "");
            _validationResult.AddRule(_skjema, "5315.1.8.5", "", "Organisasjonsnummeret ('{0}') for ansvarlig søker har ikke gyldig kontrollsiffer.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.organisasjonsnummer), "ERROR", "");
            _validationResult.AddRule(_skjema, "5315.1.8.6", "", "Organisasjonsnummeret ('{0}') for ansvarlig søker er ikke gyldig.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.organisasjonsnummer), "ERROR", "");
            _validationResult.AddRule(_skjema, "5315.1.8.7", "", "Adresse bør fylles ut for ansvarlig søker.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.adresse.adresselinje1), "WARNING", "");
            _validationResult.AddRule(_skjema, "5315.1.8.8", "", "Adresselinje 1 bør fylles ut for ansvarlig søker.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.adresse.adresselinje1), "WARNING", "");
            _validationResult.AddRule(_skjema, "5315.1.8.9", "", "Ugyldig landkode for ansvarlig søker.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.adresse.landkode), "WARNING", "");
            _validationResult.AddRule(_skjema, "5315.1.8.10", "", "Postnummer for ansvarlig søker bør angis.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.adresse.postnr), "WARNING", "");
            _validationResult.AddRule(_skjema, "5315.1.8.11", "", "Postnummeret '{0}' for ansvarlig søker har ikke gyldig kontrollsiffer. Du kan sjekke riktig postnummer på http://adressesok.bring.no/", Helpers.GetFullClassPath(() => form.ansvarligSoeker.adresse.postnr), "WARNING", "");
            _validationResult.AddRule(_skjema, "5315.1.8.12", "", "Postnummeret '{0}' for ansvarlig søker er ugyldig. Du kan sjekke riktig postnummer på http://adressesok.bring.no/", Helpers.GetFullClassPath(() => form.ansvarligSoeker.adresse.postnr), "WARNING", "");
            _validationResult.AddRule(_skjema, "5315.1.8.13", "", "Postnummeret '{0}' for ansvarlig søker stemmer ikke overens med poststedet '{1}'. Postnummeret er fra '{2}'. Du kan sjekke riktig Postnummer/poststed på http://adressesok.bring.no/", Helpers.GetFullClassPath(() => form.ansvarligSoeker.adresse.postnr), "WARNING", "");
            _validationResult.AddRule(_skjema, "5315.1.8.14", "", "Postnummeret til ansvarlig søker ble ikke validert.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.adresse.postnr), "WARNING", "");
            _validationResult.AddRule(_skjema, "5315.1.8.15", "", "Navnet til ansvarlig søker må fylles ut.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.adresse.adresselinje1), "ERROR", "");
            _validationResult.AddRule(_skjema, "5315.1.8.16", "", "Navnet til kontaktpersonen for ansvarlig søker bør fylles ut.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.kontaktperson.navn), "WARNING", "");
            _validationResult.AddRule(_skjema, "5315.1.8.17", "", "Mobilnummer eller telefonnummer for ansvarlig søker bør fylles ut.", Helpers.GetFullClassPath(() => form.ansvarligSoeker), "WARNING", "");
            _validationResult.AddRule(_skjema, "5315.1.8.18", "", "E-postadresse for ansvarlig søker bør fylles ut.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.adresse.adresselinje1), "WARNING", "");

            //erklaeringKontroll
            _validationResult.AddRule(_skjema, "5315.1.10", "", "Du må bekrefte at utførelse er i samsvar med produksjonsunderlag.", Helpers.GetFullClassPath(() => form.erklaeringUtfoerelse), "ERROR", "");
        }
        public ValidationResult Validate(SamsvarserklaeringDirekteType form, List<string> formSetElements)
        {
            _logger.Debug("Starts validating form {dataFormatId} - {dataFormatVersion}", form.dataFormatId, form.dataFormatVersion);

            Parallel.Invoke(
                () => { EiendomByggestedValidering(form); },
                () => { AnsvarsrettValidering(form); },
                () => { ForetakValidation(form); },
                () => { SoekerValidation(form); },
                () => { FraSluttbrukersystem(form); }
                );

            return _validationResult;
        }

        internal ValidationResult GetResult() => _validationResult;


        internal void FraSluttbrukersystem(SamsvarserklaeringDirekteType form)
        {
            if (string.IsNullOrEmpty(form.fraSluttbrukersystem))
            {
                _validationResult.AddMessage("5315.1.3", null);
            }
        }

        internal void EiendomByggestedValidering(SamsvarserklaeringDirekteType form)
        {
            using (var logger = new PerfTimerLogger())
            {
                if (Helpers.ObjectIsNullOrEmpty(form.eiendomByggested))
                {
                    _validationResult.AddMessage("5315.1.4", null);
                }
                else
                {
                    foreach (var eiendom in form.eiendomByggested)
                    {
                        if (!Helpers.ObjectIsNullOrEmpty(eiendom.eiendomsidentifikasjon))
                        {
                            if (String.IsNullOrEmpty(eiendom.eiendomsidentifikasjon.kommunenummer))
                            {
                                _validationResult.AddMessage("5315.1.4.1", null);
                            }
                            else
                            {
                                var kommunenummer = eiendom.eiendomsidentifikasjon.kommunenummer;
                                GeneralValidationResult generalValidationResult = MunicipalityValidator.ValidateKommunenummer(kommunenummer);//GeneralValidations.Validate_kommunenummerStatus(kommunenummer);
                                if (generalValidationResult == null)
                                {
                                    _validationResult.AddMessage("5315.1.4.2", new[] { kommunenummer });
                                }
                                else
                                {
                                    if (generalValidationResult.Status != ValidationStatus.Ok)
                                    {
                                        _validationResult.AddMessage("5315.1.4.3", null);
                                    }
                                    else
                                    {
                                        var gaardsnummer = eiendom.eiendomsidentifikasjon.gaardsnummer;
                                        var bruksnummer = eiendom.eiendomsidentifikasjon.bruksnummer;

                                        if (string.IsNullOrEmpty(gaardsnummer) || string.IsNullOrEmpty(bruksnummer))
                                        {
                                            _validationResult.AddMessage("5315.1.4.4", null);
                                        }
                                        else
                                        {
                                            int gaardsnumerInt = 0;
                                            int bruksnummerInt = 0;
                                            if (!int.TryParse(gaardsnummer, out gaardsnumerInt) || !int.TryParse(bruksnummer, out bruksnummerInt))
                                            {
                                                //_validationResult.AddMessage("5315.1.4.5", new[] { eiendom.eiendomsidentifikasjon.gaardsnummer, eiendom.eiendomsidentifikasjon.bruksnummer });
                                            }
                                            else
                                            {
                                                int festenummerInt = 0;
                                                int seksjonsnummerInt = 0;
                                                int.TryParse(eiendom.eiendomsidentifikasjon.festenummer, out festenummerInt);
                                                int.TryParse(eiendom.eiendomsidentifikasjon.seksjonsnummer, out seksjonsnummerInt);

                                                if (gaardsnumerInt < 0 || bruksnummerInt < 0)
                                                {
                                                    if (gaardsnumerInt < 0 )
                                                        _validationResult.AddMessage("5315.1.4.6", new[] { eiendom.eiendomsidentifikasjon.gaardsnummer});

                                                    if (bruksnummerInt < 0)
                                                        _validationResult.AddMessage("5315.1.4.6.1", new[] { eiendom.eiendomsidentifikasjon.bruksnummer });
                                                }
                                                else
                                                {
                                                    //## MATRIKKEL
                                                    var matrikkelnrExist = new MatrikkelService().MatrikkelnrExist(kommunenummer, gaardsnumerInt, bruksnummerInt, festenummerInt, seksjonsnummerInt);
                                                    if (matrikkelnrExist.HasValue)
                                                    {
                                                        if (!matrikkelnrExist.Value)
                                                        {
                                                            _validationResult.AddMessage("5315.1.4.12", new[]
                                                            { kommunenummer,
                                                            eiendom.eiendomsidentifikasjon.gaardsnummer,
                                                            eiendom.eiendomsidentifikasjon.bruksnummer,
                                                            eiendom.eiendomsidentifikasjon.festenummer,
                                                            eiendom.eiendomsidentifikasjon.seksjonsnummer,});
                                                        }
                                                    }
                                                    else
                                                    {
                                                        _validationResult.AddMessage("5315.1.4.13", new[]
                                                        { kommunenummer,
                                                        eiendom.eiendomsidentifikasjon.gaardsnummer,
                                                        eiendom.eiendomsidentifikasjon.bruksnummer,
                                                        eiendom.eiendomsidentifikasjon.festenummer,
                                                        eiendom.eiendomsidentifikasjon.seksjonsnummer,});
                                                    }

                                                    if (!String.IsNullOrEmpty(eiendom.bygningsnummer))
                                                    {
                                                        long bygningsnrLong = 0;
                                                        if (!long.TryParse(eiendom.bygningsnummer, out bygningsnrLong))
                                                        {
                                                            _validationResult.AddMessage("5315.1.4.7", new[] { eiendom.bygningsnummer });
                                                        }
                                                        else
                                                        {
                                                            if (bygningsnrLong <= 0)
                                                            {
                                                                _validationResult.AddMessage("5315.1.4.8", new[] { eiendom.eiendomsidentifikasjon.gaardsnummer, eiendom.eiendomsidentifikasjon.bruksnummer });
                                                            }
                                                            else
                                                            {
                                                                ////## MATRIKKEL
                                                                var isBygningValid = new MatrikkelService().IsBygningOnMatrikkelnr(bygningsnrLong, kommunenummer, gaardsnumerInt, bruksnummerInt, festenummerInt, seksjonsnummerInt);
                                                                if (isBygningValid.HasValue && !isBygningValid.Value)
                                                                {
                                                                    _validationResult.AddMessage("5315.1.4.14", new[]
                                                                    {
                                                                    kommunenummer,
                                                                    eiendom.eiendomsidentifikasjon.gaardsnummer,
                                                                    eiendom.eiendomsidentifikasjon.bruksnummer,
                                                                    eiendom.eiendomsidentifikasjon.festenummer,
                                                                    eiendom.eiendomsidentifikasjon.seksjonsnummer,});
                                                                }
                                                            }
                                                        }
                                                    }

                                                    if (Helpers.ObjectIsNullOrEmpty(eiendom.adresse))
                                                    {
                                                        _validationResult.AddMessage("5315.1.4.9", null);
                                                    }
                                                    else
                                                    {
                                                        if (string.IsNullOrEmpty(eiendom.adresse?.adresselinje1))
                                                        {
                                                            _validationResult.AddMessage("5315.1.4.9.1", null);
                                                        }
                                                        else
                                                        {
                                                            if (String.IsNullOrEmpty(eiendom.adresse.gatenavn) || String.IsNullOrEmpty(eiendom.adresse.husnr))
                                                            {
                                                                _validationResult.AddMessage("5315.1.4.9.2", null);
                                                            }
                                                            else
                                                            {
                                                                //## MATRIKKEL
                                                                var isAdresseValid = new MatrikkelService().IsVegadresseOnMatrikkelnr(eiendom.adresse.gatenavn, eiendom.adresse.husnr, eiendom.adresse.bokstav, kommunenummer, gaardsnumerInt, bruksnummerInt, festenummerInt, seksjonsnummerInt);
                                                                if (isAdresseValid.HasValue && !isAdresseValid.Value)
                                                                {
                                                                    _validationResult.AddMessage("5315.1.4.15", null);
                                                                }
                                                            }
                                                        }
                                                    }

                                                }
                                            }
                                        }
                                    }

                                }
                            }
                        }
                        if (String.IsNullOrEmpty(eiendom.kommunenavn))
                            _validationResult.AddMessage("5315.1.4.10", null);

                        if (!String.IsNullOrEmpty(eiendom.bolignummer))
                        {
                            if (GeneralValidations.Validate_bruksenhetsnummer(eiendom.bolignummer).Status == ValidationStatus.Fail)
                                _validationResult.AddMessage("5315.1.4.11", null);

                        }
                    }
                }
            }
        }

        //
        public void AnsvarsrettValidering(SamsvarserklaeringDirekteType form)
        {
            using (var logger = new PerfTimerLogger())
            {
                if (Helpers.ObjectIsNullOrEmpty(form.ansvarsrett))
                {
                    _validationResult.AddMessage("5315.1.6", null);
                }
                else
                {
                    if (Helpers.ObjectIsNullOrEmpty(form.ansvarsrett.funksjon))
                    {
                        _validationResult.AddMessage("5315.1.6.1", null);
                    }
                    else
                    {
                        var funksjonKodeverdi = form.ansvarsrett.funksjon.kodeverdi;
                        if (String.IsNullOrEmpty(funksjonKodeverdi))
                        {
                            _validationResult.AddMessage("5315.1.1.2", null);
                        }
                        else
                        {
                            if (!_codeListService.IsCodelistValid("Funksjon", funksjonKodeverdi))
                            {
                                _validationResult.AddMessage("5315.1.2.2", new[] { funksjonKodeverdi });
                            }
                            else
                            {
                                if (!funksjonKodeverdi.Equals("PRO") && !funksjonKodeverdi.Equals("UTF"))
                                {
                                    _validationResult.AddMessage("5315.1.6.2", new[] { funksjonKodeverdi });
                                }
                                else
                                {

                                    if (funksjonKodeverdi.Equals("PRO"))
                                    {
                                        if (Helpers.ObjectIsNullOrEmpty(form.ansvarsrett.prosjekterende))
                                        {
                                            _validationResult.AddMessage("5315.1.6.3", new[] { funksjonKodeverdi });
                                        }
                                        else
                                        {
                                            var booleans = new[]
                                            {
                                            form.ansvarsrett.prosjekterende.okForFerdigattest.GetValueOrDefault(),
                                            form.ansvarsrett.prosjekterende.okForRammetillatelse.GetValueOrDefault(),
                                            form.ansvarsrett.prosjekterende.okForIgangsetting.GetValueOrDefault(),
                                            form.ansvarsrett.prosjekterende.okForMidlertidigBrukstillatelse.GetValueOrDefault()
                                            };

                                            var trueCount = booleans.Count(c => c);
                                            if (trueCount == 0)
                                            {
                                                _validationResult.AddMessage("5315.1.6.4", null);
                                            }
                                            else if (trueCount > 1)
                                            {
                                                _validationResult.AddMessage("5315.1.6.5", null);
                                            }
                                        }
                                        if (!form.erklaeringProsjektering.GetValueOrDefault())
                                        {
                                            _validationResult.AddMessage("5315.1.6.16", null);
                                        }
                                    }

                                    else if (funksjonKodeverdi.Equals("UTF"))
                                    {

                                        var utfoerende = form.ansvarsrett.utfoerende;
                                        if (Helpers.ObjectIsNullOrEmpty(form.ansvarsrett.utfoerende))
                                        {
                                            _validationResult.AddMessage("5315.1.6.6", null);
                                        }
                                        else
                                        {
                                            if (form.ansvarsrett.utfoerende.okForFerdigattest.GetValueOrDefault() && form.ansvarsrett.utfoerende.okMidlertidigBrukstillatelse.GetValueOrDefault())
                                            {
                                                _validationResult.AddMessage("5315.1.6.7", null);

                                            }
                                            else
                                            {
                                                if (form.ansvarsrett.utfoerende.okMidlertidigBrukstillatelse.GetValueOrDefault())
                                                {
                                                    if (!form.ansvarsrett.utfoerende.harTilstrekkeligSikkerhet.HasValue)
                                                    {
                                                        _validationResult.AddMessage("5315.1.6.8", null);
                                                    }
                                                    else
                                                    {
                                                        if (Helpers.ObjectIsNullOrEmpty(utfoerende.midlertidigBrukstillatelseGjenstaaende))
                                                        {
                                                            _validationResult.AddMessage("5315.1.6.9", null);
                                                        }
                                                        else
                                                        {
                                                            if (string.IsNullOrEmpty(utfoerende.midlertidigBrukstillatelseGjenstaaende.gjenstaaendeInnenfor))
                                                                _validationResult.AddMessage("5315.1.6.10", null);
                                                        }

                                                        if (!form.ansvarsrett.utfoerende.harTilstrekkeligSikkerhet.GetValueOrDefault(false))
                                                        {
                                                            if (string.IsNullOrEmpty(utfoerende.typeArbeider))
                                                                _validationResult.AddMessage("5315.1.6.12", null);
                                                            if (!utfoerende.utfoertInnen.HasValue)
                                                            {
                                                                _validationResult.AddMessage("5315.1.6.13", null);
                                                            }
                                                            else
                                                            {
                                                                if ((utfoerende.utfoertInnen.Value - DateTime.Now).TotalDays > 14)
                                                                {
                                                                    _validationResult.AddMessage("5315.1.6.14", null);
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        if (!form.erklaeringUtfoerelse.GetValueOrDefault())
                                        {
                                            _validationResult.AddMessage("5315.1.10", null);
                                        }
                                    }
                                }
                            }
                        }
                        if (string.IsNullOrEmpty(form.ansvarsrett.beskrivelseAvAnsvarsomraadet))
                        {
                            _validationResult.AddMessage("5315.1.6.15", null);
                        }

                    }
                }
            }
        }

        internal void ForetakValidation(SamsvarserklaeringDirekteType form)
        {
            using (var logger = new PerfTimerLogger())
            {
                if (Helpers.ObjectIsNullOrEmpty(form.foretak))
                {
                    _validationResult.AddMessage("5315.1.7", null);
                }
                else
                {
                    if (Helpers.ObjectIsNullOrEmpty(form.foretak.partstype?.kodeverdi))
                    {
                        _validationResult.AddMessage("5315.1.7.1", null);
                    }
                    else
                    {
                        if (!_codeListService.IsCodelistValid("Partstype", form.foretak.partstype.kodeverdi))
                        {
                            _validationResult.AddMessage("5315.1.7.2", new[] { form.foretak.partstype.kodeverdi });
                        }
                        else
                        {
                            if (form.foretak.partstype.kodeverdi == "Privatperson")
                            {

                                _validationResult.AddMessage("5315.1.7.3", null);
                            }
                            else
                            {
                                // Org number valid format: 
                                //    https://www.brreg.no/om-oss-nn/oppgavene-vare/registera-vare/om-einingsregisteret/organisasjonsnummeret/

                                if (String.IsNullOrEmpty(form.foretak.organisasjonsnummer))
                                {
                                    _validationResult.AddMessage("5315.1.7.4", null);
                                }
                                else
                                {
                                    Match isOrgNumValid = Regex.Match(form.foretak.organisasjonsnummer, "^([0-9])([0-9])([0-9])([0-9])([0-9])([0-9])([0-9])([0-9])([0-9])$");

                                    if (isOrgNumValid.Success)
                                    {
                                        int products = Convert.ToInt32(isOrgNumValid.Groups[1].Value) * 3 +
                                                       Convert.ToInt32(isOrgNumValid.Groups[2].Value) * 2 +
                                                       Convert.ToInt32(isOrgNumValid.Groups[3].Value) * 7 +
                                                       Convert.ToInt32(isOrgNumValid.Groups[4].Value) * 6 +
                                                       Convert.ToInt32(isOrgNumValid.Groups[5].Value) * 5 +
                                                       Convert.ToInt32(isOrgNumValid.Groups[6].Value) * 4 +
                                                       Convert.ToInt32(isOrgNumValid.Groups[7].Value) * 3 +
                                                       Convert.ToInt32(isOrgNumValid.Groups[8].Value) * 2;

                                        int controlDigit = 11 - (products % 11);
                                        if (controlDigit == 11)
                                        {
                                            controlDigit = 0;
                                        }

                                        if (controlDigit != Convert.ToInt32(isOrgNumValid.Groups[9].Value))
                                        {
                                            _validationResult.AddMessage("5315.1.7.5", new[] { form.foretak.organisasjonsnummer });
                                        }
                                    }
                                    else
                                    {
                                        _validationResult.AddMessage("5315.1.7.6", new[] { form.foretak.organisasjonsnummer });
                                    }
                                }
                                if (Helpers.ObjectIsNullOrEmpty(form.foretak.adresse))
                                {
                                    _validationResult.AddMessage("5315.1.7.7", null);
                                }
                                else
                                {
                                    if (String.IsNullOrEmpty(form.foretak.adresse.adresselinje1))
                                    {
                                        _validationResult.AddMessage("5315.1.7.8", null);
                                    }
                                    else
                                    {
                                        if (!CountryCodeHandler.IsCountryNorway(form.foretak.adresse.landkode))
                                        {
                                            if (!CountryCodeHandler.VerifyCountryCode(form.foretak.adresse.landkode))
                                            {
                                                _validationResult.AddMessage("5315.1.7.9", null);
                                            }
                                        }
                                        else
                                        {
                                            var postNr = form.foretak.adresse.postnr;
                                            var landkode = form.foretak.adresse.landkode;

                                            if (string.IsNullOrEmpty(form.foretak.adresse.postnr))
                                            {
                                                _validationResult.AddMessage("5315.1.7.10", null);
                                            }
                                            else
                                            {
                                                Match isPostNrValid = Regex.Match(postNr, "^([0-9])([0-9])([0-9])([0-9])$");

                                                if (!isPostNrValid.Success)
                                                {
                                                    _validationResult.AddMessage("5315.1.7.11", new[] { postNr });
                                                }
                                                else
                                                {

                                                    var postnrValidation = new PostalCode.BringPostalCodeProvider().ValidatePostnr(postNr, landkode);
                                                    if (postnrValidation != null)
                                                    {
                                                        if (!postnrValidation.Valid)
                                                        {
                                                            _validationResult.AddMessage("5315.1.7.12", new[] { postNr, form.foretak.adresse.poststed, postnrValidation.Result });
                                                        }
                                                        else
                                                        {
                                                            if (!postnrValidation.Result.Equals(form.foretak.adresse.poststed, StringComparison.CurrentCultureIgnoreCase))
                                                            {
                                                                _validationResult.AddMessage("5315.1.7.13", new[] { postNr, form.foretak.adresse.poststed, postnrValidation.Result });
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        _validationResult.AddMessage("5315.1.7.14", null);

                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                if (String.IsNullOrEmpty(form.foretak.navn))
                                    _validationResult.AddMessage("5315.1.7.15", null);

                                if (string.IsNullOrEmpty(form.foretak.kontaktperson?.navn))
                                    _validationResult.AddMessage("5315.1.7.16", null);

                                if (string.IsNullOrEmpty(form.foretak.mobilnummer) && string.IsNullOrEmpty(form.foretak.telefonnummer))
                                    _validationResult.AddMessage("5315.1.7.17", null);
                                if (String.IsNullOrEmpty(form.foretak.epost))
                                    _validationResult.AddMessage("5315.1.7.18", null);
                            }
                        }
                    }
                }
            }
        }

        internal void SoekerValidation(SamsvarserklaeringDirekteType form)
        {
            using (var logger = new PerfTimerLogger())
            {
                if (Helpers.ObjectIsNullOrEmpty(form.ansvarligSoeker))
                {
                    _validationResult.AddMessage("5315.1.8", null);
                }
                else
                {
                    if (Helpers.ObjectIsNullOrEmpty(form.ansvarligSoeker.partstype?.kodeverdi))
                    {
                        _validationResult.AddMessage("5315.1.8.1", null);
                    }
                    else
                    {
                        if (!_codeListService.IsCodelistValid("Partstype", form.ansvarligSoeker.partstype.kodeverdi))
                        {
                            _validationResult.AddMessage("5315.1.8.2", new[] { form.ansvarligSoeker.partstype.kodeverdi });
                        }
                        else
                        {
                            if (form.ansvarligSoeker.partstype.kodeverdi == "Privatperson")
                            {

                                _validationResult.AddMessage("5315.1.8.3", null);
                            }
                            else
                            {
                                // Org number valid format: 
                                //    https://www.brreg.no/om-oss-nn/oppgavene-vare/registera-vare/om-einingsregisteret/organisasjonsnummeret/

                                if (String.IsNullOrEmpty(form.ansvarligSoeker.organisasjonsnummer))
                                {
                                    _validationResult.AddMessage("5315.1.8.4", null);
                                }
                                else
                                {
                                    Match isOrgNumValid = Regex.Match(form.ansvarligSoeker.organisasjonsnummer, "^([0-9])([0-9])([0-9])([0-9])([0-9])([0-9])([0-9])([0-9])([0-9])$");

                                    if (isOrgNumValid.Success)
                                    {
                                        int products = Convert.ToInt32(isOrgNumValid.Groups[1].Value) * 3 +
                                                       Convert.ToInt32(isOrgNumValid.Groups[2].Value) * 2 +
                                                       Convert.ToInt32(isOrgNumValid.Groups[3].Value) * 7 +
                                                       Convert.ToInt32(isOrgNumValid.Groups[4].Value) * 6 +
                                                       Convert.ToInt32(isOrgNumValid.Groups[5].Value) * 5 +
                                                       Convert.ToInt32(isOrgNumValid.Groups[6].Value) * 4 +
                                                       Convert.ToInt32(isOrgNumValid.Groups[7].Value) * 3 +
                                                       Convert.ToInt32(isOrgNumValid.Groups[8].Value) * 2;

                                        int controlDigit = 11 - (products % 11);
                                        if (controlDigit == 11)
                                        {
                                            controlDigit = 0;
                                        }

                                        if (controlDigit != Convert.ToInt32(isOrgNumValid.Groups[9].Value))
                                        {
                                            _validationResult.AddMessage("5315.1.8.5", new[] { form.ansvarligSoeker.organisasjonsnummer });
                                        }
                                    }
                                    else
                                    {
                                        _validationResult.AddMessage("5315.1.8.6", new[] { form.ansvarligSoeker.organisasjonsnummer });
                                    }
                                }
                                if (Helpers.ObjectIsNullOrEmpty(form.ansvarligSoeker.adresse))
                                {
                                    _validationResult.AddMessage("5315.1.8.7", null);
                                }
                                else
                                {
                                    if (String.IsNullOrEmpty(form.ansvarligSoeker.adresse.adresselinje1))
                                    {
                                        _validationResult.AddMessage("5315.1.8.8", null);
                                    }
                                    else
                                    {
                                        if (!CountryCodeHandler.IsCountryNorway(form.ansvarligSoeker.adresse.landkode))
                                        {
                                            if (!CountryCodeHandler.VerifyCountryCode(form.ansvarligSoeker.adresse.landkode))
                                            {
                                                _validationResult.AddMessage("5315.1.8.9", null);
                                            }
                                        }
                                        else
                                        {
                                            var postNr = form.ansvarligSoeker.adresse.postnr;
                                            var landkode = form.ansvarligSoeker.adresse.landkode;

                                            if (string.IsNullOrEmpty(form.ansvarligSoeker.adresse.postnr))
                                            {
                                                _validationResult.AddMessage("5315.1.8.10", null);
                                            }
                                            else
                                            {
                                                Match isPostNrValid = Regex.Match(postNr, "^([0-9])([0-9])([0-9])([0-9])$");

                                                if (!isPostNrValid.Success)
                                                {
                                                    _validationResult.AddMessage("5315.1.8.11", new[] { postNr });
                                                }
                                                else
                                                {

                                                    var postnrValidation = new PostalCode.BringPostalCodeProvider().ValidatePostnr(postNr, landkode);
                                                    if (postnrValidation != null)
                                                    {
                                                        if (!postnrValidation.Valid)
                                                        {
                                                            _validationResult.AddMessage("5315.1.8.12", new[] { postNr, form.ansvarligSoeker.adresse.poststed, postnrValidation.Result });
                                                        }
                                                        else
                                                        {
                                                            if (!postnrValidation.Result.Equals(form.ansvarligSoeker.adresse.poststed, StringComparison.CurrentCultureIgnoreCase))
                                                            {
                                                                _validationResult.AddMessage("5315.1.8.13", new[] { postNr, form.ansvarligSoeker.adresse.poststed, postnrValidation.Result });
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        _validationResult.AddMessage("5315.1.8.14", null);

                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                if (String.IsNullOrEmpty(form.ansvarligSoeker.navn))
                                    _validationResult.AddMessage("5315.1.8.15", null);

                                if (string.IsNullOrEmpty(form.ansvarligSoeker.kontaktperson?.navn))
                                    _validationResult.AddMessage("5315.1.8.16", null);

                                if (string.IsNullOrEmpty(form.ansvarligSoeker.mobilnummer) && string.IsNullOrEmpty(form.ansvarligSoeker.telefonnummer))
                                    _validationResult.AddMessage("5315.1.8.17", null);

                                if (String.IsNullOrEmpty(form.ansvarligSoeker.epost))
                                    _validationResult.AddMessage("5315.1.8.18", null);
                            }
                        }
                    }
                }
            }
        }
    }
}
