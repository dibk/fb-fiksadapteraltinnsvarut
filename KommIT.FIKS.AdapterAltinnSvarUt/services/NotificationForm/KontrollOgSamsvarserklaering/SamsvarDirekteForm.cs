using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.NotificationForm;
using System;
using System.Collections.Generic;
using System.Linq;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services
{
    /// <summary>
    /// Direkte oppretting fra ansvarlig foretak
    /// </summary>
    public class SamsvarDirekteForm : IAltinnForm, INotificationForm
    {
        private const string DataFormatId = "6167";
        private const string DataFormatVersion = "44169";
        private const string SchemaFile = "samsvarserklaeringDirekteOpprettet.xsd";
        private const string Name = "Samsvarserklæring";
        private List<string> FormSetElements;
        private string FormTypePerFunction = "Samsvarserklæring";
        private readonly Guid _GuidReplayKey = Guid.NewGuid();

        public void SetFormSetElements(List<string> attachmentlist)
        {
            FormSetElements = attachmentlist;
        }

        public bool? ValidateAuthenticatedSubmitterVsFormData(string authenticatedSubmitter, ValidationResult validationResult)
        {
            bool isSubmitterValid = Helpers.ValidateEntityOfAltinnSubmitterDefault(authenticatedSubmitter, GetPropertyIdentifiers());

            if (!isSubmitterValid)
            {
                Helpers.RecordErrorAltinnSubmitter(validationResult);
            }
            return isSubmitterValid;
        }

        private no.kxml.skjema.dibk.samsvarserklaeringDirekteOpprettet.SamsvarserklaeringDirekteType form;

        public string GetNotificationMessageTitle()
        {
            return $"{FormTypePerFunction} er signert fra {form.foretak.navn}";
        }

        public string GetNotificationMessageSender()
        {
            return form.foretak.navn;
        }

        public string GetNotificationMessageSummary()
        {
            return $"Melding er sendt via Fellestjenester Bygg fra Direktoratet for Byggkvalitet";
        }

        public string GetNotificationMessageBody()
        {
            return
                $"Vedlagt {FormTypePerFunction.ToLower()} gjelder adresse {form.eiendomByggested.First().adresse.adresselinje1}, {form.eiendomByggested.First().adresse.postnr} {form.eiendomByggested.First().adresse.poststed}";
        }

        public PropertyIdentifiers GetPropertyIdentifiers()
        {
            PropertyIdentifiers propertyIdentifiers = new PropertyIdentifiers();

            propertyIdentifiers.Kommunenummer = form.eiendomByggested[0].eiendomsidentifikasjon.kommunenummer;
            propertyIdentifiers.Gaardsnummer = form.eiendomByggested[0].eiendomsidentifikasjon.gaardsnummer;
            propertyIdentifiers.Bruksnummer = form.eiendomByggested[0].eiendomsidentifikasjon.bruksnummer;
            propertyIdentifiers.Festenummer = form.eiendomByggested[0].eiendomsidentifikasjon.festenummer;
            propertyIdentifiers.Seksjonsnummer = form.eiendomByggested[0].eiendomsidentifikasjon.seksjonsnummer;
            propertyIdentifiers.Adresselinje1 = form.eiendomByggested[0].adresse.adresselinje1;
            propertyIdentifiers.Adresselinje2 = form.eiendomByggested[0].adresse.adresselinje2;
            propertyIdentifiers.Adresselinje3 = form.eiendomByggested[0].adresse.adresselinje3;
            propertyIdentifiers.Postnr = form.eiendomByggested[0].adresse.postnr;
            propertyIdentifiers.Poststed = form.eiendomByggested[0].adresse.poststed;
            propertyIdentifiers.Landkode = form.eiendomByggested[0].adresse.landkode;
            propertyIdentifiers.Bygningsnummer = form.eiendomByggested[0].bygningsnummer;
            propertyIdentifiers.Bolignummer = form.eiendomByggested[0].bolignummer;
            propertyIdentifiers.SoeknadSkjemaNavn = Name;
            propertyIdentifiers.FraSluttbrukersystem = form.fraSluttbrukersystem;

            propertyIdentifiers.KommunensSaksnummerAar = form.kommunensSaksnummer.saksaar;
            propertyIdentifiers.KommunensSaksnummerSekvensnummer = form.kommunensSaksnummer.sakssekvensnummer;

            propertyIdentifiers.AnsvarligForetakOrgNr = form.foretak.organisasjonsnummer;

            return propertyIdentifiers;
        }


        public string GetDataFormatId()
        {
            return DataFormatId;
        }

        public string GetDataFormatVersion()
        {
            return DataFormatVersion;
        }

        public string GetMunicipalityCode()
        {
            return GetPropertyIdentifiers().Kommunenummer;
        }

        public string GetSchemaFile()
        {
            return SchemaFile;
        }

        public string GetName()
        {
            return Name;
        }

        public void InitiateForm(string formDataAsXml)
        {
            form = Utils.SerializeUtil.DeserializeFromString<no.kxml.skjema.dibk.samsvarserklaeringDirekteOpprettet.SamsvarserklaeringDirekteType>(formDataAsXml);
        }

        public ValidationResult ValidateData()
        {
            return new SamsvarDirekteFormValidator().Validate(form, FormSetElements);
        }

        public string GetSendToReporteeId()
        {
            return Utils.Helpers.ReturnFirstOrgNumberOrPersonalNumber(
                form.ansvarligSoeker.organisasjonsnummer,
                form.ansvarligSoeker.foedselsnummer).IdNumber;
        }

        public bool IsReply()
        {
            return false;
            //Guid incomingGuid;
            //return Guid.TryParse(form.hovedinnsendingsnummer, out incomingGuid);
        }

        public Guid GetReplyKey()
        {
            return _GuidReplayKey;
        }

        public int GetReplyDueDays()
        {
            return 0;
        }

        public string GetNotificationXMLFilename()
        {
            return Resources.TextStrings.NotificationSamsvarserklaeringXMLFilename;
        }

        public string GetNotificationXMLTitle()
        {
            return Resources.TextStrings.NotificationKontrollSamsvarXMLTitle;
        }

        public string GetNotificationPdfFilename()
        {
            return Resources.TextStrings.NotificationSamsvarserklaeringPdfFilename;
        }

        public string GetNotificationPdfTitle()
        {
            return Resources.TextStrings.NotificationSamsvarserklaeringPdfTitle;
        }

        public bool DoEmailNotification()
        {
            if (_emailnotification != null) return true;
            else return false;
        }

        public void SetEmailNotification(string email, string subject, string emailcontent, string smscontent)
        {
            _emailnotification = new EmailNotification();
            _emailnotification.Email = email;
            _emailnotification.EmailSubject = subject;
            _emailnotification.EmailContent = emailcontent;
            _emailnotification.SmsContent = smscontent;
        }
        private EmailNotification _emailnotification;
        public EmailNotification GetEmailNotification()
        {
            return _emailnotification;
        }

        public IEnumerable<CorrespondenceAttachmentsType> CorrespondenceAttachmentsTypes => new List<CorrespondenceAttachmentsType>()
        {
            CorrespondenceAttachmentsType.MainFormPdf, 
            CorrespondenceAttachmentsType.MainFormXml, 
            CorrespondenceAttachmentsType.Attachments
        };

        public IEnumerable<StoreToBlob> StoreToBlobs => new List<StoreToBlob>()
        {
            StoreToBlob.MainFormPdf, 
            StoreToBlob.MainFormXml, 
            StoreToBlob.Attachments
        };

        private DateTime? _replyDeadline;
        public void SetReplyDeadline(DateTime? ReplyDeadline)
        {
            _replyDeadline = ReplyDeadline;
        }

        public DateTime? GetReplyDeadline()
        {
            return _replyDeadline;
        }
        public bool IsStoreMessageFile()
        {
            return false;
        }
    }
}