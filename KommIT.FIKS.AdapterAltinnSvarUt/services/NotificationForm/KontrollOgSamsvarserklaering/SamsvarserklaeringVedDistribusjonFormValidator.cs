﻿using System;
using System.Linq;
using DIBK.FBygg.Altinn.MapperCodelist;
using KommIT.FIKS.AdapterAltinnSvarUt.Resources;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Validations;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;
using no.kxml.skjema.dibk.samsvarserklaeringVedDistribusjon;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services
{
    public class SamsvarserklaeringVedDistribusjonFormValidator
    {
        CodeListService _codeListService = new CodeListService();
        private readonly string _skjema = "samsvarserklaeringVedDistribusjon";

        private ValidationResult _validationResult = new ValidationResult();

        public SamsvarserklaeringVedDistribusjonFormValidator()
        {
            _validationResult = new ValidationResult();
            var form = new SamsvarserklaeringVedDistribusjonType();
            _validationResult.AddRule("5207.1.1", "Innhold må være i henhold til informasjonsmodell/xsd for skjema.", "", "ERROR", "");
            _validationResult.AddRule(_skjema, "5207.1.3", "", "Må fylle ut samme navn som er brukt i registrering for Altinn API i fraSluttbrukersystem.", Helpers.GetFullClassPath(() => form.fraSluttbrukersystem), "ERROR", "");
        }

        public ValidationResult Validate(SamsvarserklaeringVedDistribusjonType form)
        {
            FraSluttbrukersystemErUtfylt(form);
            return _validationResult;
        }
        private void FraSluttbrukersystemErUtfylt(SamsvarserklaeringVedDistribusjonType form)
        {
            if (String.IsNullOrEmpty(form.fraSluttbrukersystem))
            {
                _validationResult.AddMessage("5207.1.3", null);
            }
        }


    }
}