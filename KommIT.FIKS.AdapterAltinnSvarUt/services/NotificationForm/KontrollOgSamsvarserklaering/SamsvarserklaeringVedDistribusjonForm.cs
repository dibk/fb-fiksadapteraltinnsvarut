﻿using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.NotificationForm.KontrollOgSamsvarserklaering;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;
using no.kxml.skjema.dibk.samsvarserklaeringVedDistribusjon;
using System;
using System.Collections.Generic;
using System.Linq;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services
{
    /// <summary>
    /// Brukes i distribusjonstjenesten som preutfylt skjema og i retur som notifikasjonsskjema
    /// </summary>
    public class SamsvarserklaeringVedDistribusjonForm : IAltinnForm, INotificationForm, IPrefillForm
    {
        private const string DataFormatId = "5905";
        private const string DataFormatVersion = "43432";
        private const string SchemaFile = "samsvarserklaeringVedDistribusjon.xsd";
        private const string Name = "Samsvarserklæring";

        private SamsvarserklaeringVedDistribusjonType _form;

        private string _archiveReferenceOfDistribution = String.Empty;
        private string _altinnReferenceForPrefillFrom = String.Empty;


        private List<string> FormSetElements;
        public void SetFormSetElements(List<string> attachmentlist)
        {
            FormSetElements = attachmentlist;
        }

        public bool? ValidateAuthenticatedSubmitterVsFormData(string authenticatedSubmitter, ValidationResult validationResult)
        {
            bool isSubmitterValid = Helpers.ValidateEntityOfAltinnSubmitterDefault(authenticatedSubmitter, GetPropertyIdentifiers());

            if (!isSubmitterValid)
            {
                Helpers.RecordErrorAltinnSubmitter(validationResult);
            }
            return isSubmitterValid;
        }

        public ReporteeInformation GetReportee()
        {
            return Utils.Helpers.ReturnFirstOrgNumberOrPersonalNumber(
                    _form.ansvarligSoeker.organisasjonsnummer,
                    _form.ansvarligSoeker.foedselsnummer);
        }
        public string GetNotificationMessageTitle()
        {
            string prosjektnavn = _form.prosjektnavn;
            string adresselinje1 = _form.eiendomByggested.Length == 0 ? "" : _form.eiendomByggested.First().adresse.adresselinje1; // Length is Hack for FBHS-1535 
            string ansvarligSoekerNavn = _form.ansvarligSoeker.navn;
            string ansvarligSoekerOrgNr = _form.ansvarligSoeker.organisasjonsnummer;
            return KontrollOgSamsvarMessages.GetNotificationMessageTitle(prosjektnavn, adresselinje1, ansvarligSoekerNavn, ansvarligSoekerOrgNr);
            
        }
        public string GetNotificationMessageSender()
        {
            return _form.foretak.navn;
        }
        public string GetNotificationMessageSummary()
        {
            return "";
        }

        public string GetNotificationMessageBody()
        {
            string ansvarligSoekerNavn = _form.ansvarligSoeker.navn;
            string foretakNavn = _form.foretak.navn;
            string kodebeskrivelse = _form.ansvarsrett.funksjon.kodebeskrivelse;
            string adresselinje1 = _form.eiendomByggested.Length == 0 ? "" : _form.eiendomByggested.First().adresse.adresselinje1;
            string gaardsnummer = _form.eiendomByggested.Length == 0 ? "" : _form.eiendomByggested.First().eiendomsidentifikasjon.gaardsnummer;
            string bruksnummer = _form.eiendomByggested.Length == 0 ? "" : _form.eiendomByggested.First().eiendomsidentifikasjon.bruksnummer;
            string prosjektnavn = _form.prosjektnavn;
            
            return KontrollOgSamsvarMessages.GetNotificationMessageBodySamsvarserklaering(ansvarligSoekerNavn, foretakNavn,
                kodebeskrivelse, adresselinje1, gaardsnummer, bruksnummer, prosjektnavn);
        }

        public PropertyIdentifiers GetPropertyIdentifiers()
        {
            PropertyIdentifiers propertyIdentifiers = new PropertyIdentifiers();

            if (_form.eiendomByggested.Length != 0)
            {
                propertyIdentifiers.Kommunenummer = _form.eiendomByggested[0].eiendomsidentifikasjon.kommunenummer;
                propertyIdentifiers.Gaardsnummer = _form.eiendomByggested[0].eiendomsidentifikasjon.gaardsnummer;
                propertyIdentifiers.Bruksnummer = _form.eiendomByggested[0].eiendomsidentifikasjon.bruksnummer;
                propertyIdentifiers.Festenummer = _form.eiendomByggested[0].eiendomsidentifikasjon.festenummer;
                propertyIdentifiers.Seksjonsnummer = _form.eiendomByggested[0].eiendomsidentifikasjon.seksjonsnummer;
                propertyIdentifiers.Adresselinje1 = _form.eiendomByggested[0].adresse.adresselinje1;
                propertyIdentifiers.Adresselinje2 = _form.eiendomByggested[0].adresse.adresselinje2;
                propertyIdentifiers.Adresselinje3 = _form.eiendomByggested[0].adresse.adresselinje3;
                propertyIdentifiers.Postnr = _form.eiendomByggested[0].adresse.postnr;
                propertyIdentifiers.Poststed = _form.eiendomByggested[0].adresse.poststed;
                propertyIdentifiers.Landkode = _form.eiendomByggested[0].adresse.landkode;
                propertyIdentifiers.Bygningsnummer = _form.eiendomByggested[0].bygningsnummer;
                propertyIdentifiers.Bolignummer = _form.eiendomByggested[0].bolignummer;
            }

            propertyIdentifiers.SoeknadSkjemaNavn = Name;
            propertyIdentifiers.FraSluttbrukersystem = _form.fraSluttbrukersystem;

            propertyIdentifiers.KommunensSaksnummerAar = _form.kommunensSaksnummer.saksaar;
            propertyIdentifiers.KommunensSaksnummerSekvensnummer = _form.kommunensSaksnummer.sakssekvensnummer;

            return propertyIdentifiers;
        }


        public string GetDataFormatId()
        {
            return DataFormatId;
        }

        public string GetDataFormatVersion()
        {
            return DataFormatVersion;
        }

        public string GetMunicipalityCode()
        {
            return GetPropertyIdentifiers().Kommunenummer;
        }

        public string GetSchemaFile()
        {
            return SchemaFile;
        }

        public string GetName()
        {
            return Name;
        }

        public void InitiateForm(string formDataAsXml)
        {
            _form = Utils.SerializeUtil.DeserializeFromString<SamsvarserklaeringVedDistribusjonType>(formDataAsXml);
        }

        public ValidationResult ValidateData()
        {
            return new SamsvarserklaeringVedDistribusjonFormValidator().Validate(_form);
        }

        public string GetSendToReporteeId()
        {
            return Utils.Helpers.ReturnFirstOrgNumberOrPersonalNumber(
                        _form.ansvarligSoeker.organisasjonsnummer,
                        _form.ansvarligSoeker.foedselsnummer).IdNumber;
        }

        public bool IsReply()
        {
            Guid incomingGuid;
            return Guid.TryParse(_form.hovedinnsendingsnummer, out incomingGuid);
        }

        public Guid GetReplyKey()
        {
            Guid incomingGuid;
            var isReply = Guid.TryParse(_form.hovedinnsendingsnummer, out incomingGuid);
            return incomingGuid;
        }

        public int GetReplyDueDays()
        {
            return 0;
        }
        public string GetNotificationXMLFilename()
        {
            return Resources.TextStrings.NotificationSamsvarserklaeringXMLFilename;
        }

        public string GetNotificationXMLTitle()
        {
            return Resources.TextStrings.NotificationSamsvarserklaeringXMLTitle;
        }

        public string GetNotificationPdfFilename()
        {
            return Resources.TextStrings.NotificationSamsvarserklaeringPdfFilename;
        }

        public string GetNotificationPdfTitle()
        {
            return Resources.TextStrings.NotificationSamsvarserklaeringPdfTitle;
        }

        public string GetPrefillSendToReporteeId()
        {
            return Utils.Helpers.ReturnFirstOrgNumberOrPersonalNumber(
                       _form.foretak.organisasjonsnummer,
                       _form.foretak.foedselsnummer).IdNumber;
        }

        public string GetFormXML()
        {
            return Utils.SerializeUtil.Serialize(_form);
        }

        public string GetPrefillServiceCode()
        {
            return "5207";
        }

        public string GetPrefillServiceEditionCode()
        {
            return "2";
        }

        public string GetPrefillNotificationTitle()
        {
            string foretakOrgNr = _form.foretak.organisasjonsnummer;
            string foretakNavn = _form.foretak.navn;
            string adresselinje1 = _form.eiendomByggested.Length == 0 ? "" : _form.eiendomByggested.First().adresse.adresselinje1;
            string prosjektnavn = _form.prosjektnavn;
            string kontaktperson = _form.foretak.kontaktperson.navn;

            return KontrollOgSamsvarMessages.GetPrefillNotificationTitleSamsvarserklaering(prosjektnavn, adresselinje1,
                foretakNavn, foretakOrgNr, kontaktperson);
        }

        public string GetPrefillNotificationSummary()
        {
            return $"";
        }

        public string GetPrefillNotificationBody(bool? pdf, string replyLink = "")
        {
            string kontaktperson = _form.ansvarligSoeker.kontaktperson.navn;
            string kontaktpersonTlf = _form.ansvarligSoeker.kontaktperson.telefonnummer;
            string kontaktpersonMobil = _form.ansvarligSoeker.kontaktperson.mobilnummer;
            string kontaktpersonEpost = _form.ansvarligSoeker.kontaktperson.epost;
            string ansvarligSoekerNavn = _form.ansvarligSoeker.navn;
            string ansvarligSoekerTlf = _form.ansvarligSoeker.telefonnummer;
            string ansvarligSoekerMobil = _form.ansvarligSoeker.mobilnummer;
            string ansvarligSoekerEpost = _form.ansvarligSoeker.epost;
            string foretakNavn = _form.foretak.navn;
            string kontaktpersonForetak = _form.foretak.kontaktperson.navn;
            string adresselinje1 = _form.eiendomByggested.First().adresse.adresselinje1;
            string gardsnummer = _form.eiendomByggested.First().eiendomsidentifikasjon.gaardsnummer;
            string bruksnummer = _form.eiendomByggested.First().eiendomsidentifikasjon.bruksnummer;
            string kodebeskrivelse = _form.ansvarsrett.funksjon.kodebeskrivelse;

            return KontrollOgSamsvarMessages.GetPrefillNotificationBodySamsvarserklaering(kontaktpersonForetak,
                kontaktpersonTlf, kontaktpersonMobil, kontaktpersonEpost,
                ansvarligSoekerNavn, ansvarligSoekerTlf, ansvarligSoekerMobil, ansvarligSoekerEpost,
                foretakNavn, adresselinje1, gardsnummer, bruksnummer, kodebeskrivelse);
        }

        public int GetPrefillNotificationDueDays()
        {
            return 0;
        }

        public void SetPrefillKey(string key)
        {
            _form.hovedinnsendingsnummer = key;
        }

        public string GetPrefillKey()
        {
            return _form.hovedinnsendingsnummer;
        }

        public string GetPrefillLinkText()
        {
            return Resources.TextStrings.SamsvarserklaeringLinkText;
        }

        public string GetPrefillMessageSender()
        {
            return _form.ansvarligSoeker.navn;
        }

        public string GetPrefillOurReference()
        {
            return _form.ansvarsrett.soeknadssystemetsReferanse;
        }

        public bool DoEmailNotification()
        {
            if (_emailnotification != null) return true;
            else return false;
        }

        public void SetEmailNotification(string email, string subject, string emailcontent, string smscontent)
        {
            _emailnotification = new EmailNotification();
            _emailnotification.Email = email;
            _emailnotification.EmailSubject = subject;
            _emailnotification.EmailContent = emailcontent;
            _emailnotification.SmsContent = smscontent;
        }
        private EmailNotification _emailnotification;
        public EmailNotification GetEmailNotification()
        {
            return _emailnotification;
        }

        public string GetArchiveReferenceOfDistribution()
        {
            return _archiveReferenceOfDistribution;
        }

        public void SetArchiveReferenceOfDistribution(string ar)
        {
            _archiveReferenceOfDistribution = ar;
        }

        public string GetAltinnReferenceForPrefillFrom()
        {
            return _altinnReferenceForPrefillFrom;
        }

        public void SetAltinnReferenceForPrefillFrom(string altinnReference)
        {
            _altinnReferenceForPrefillFrom = altinnReference;
        }

        public NotificationEnums.NotificationCarrier GetNotificationType()
        {
            return NotificationEnums.NotificationCarrier.EmailFromDistributionOrAltinnWhenInvalidEmailAddress;
        }

        public string GetAltinnNotificationTemplate()
        {
            return Resources.TextStrings.AltinnNotificationTemplate;
        }


        public NotificationEnums.NotificationChannel GetNotificationChannel()
        {
            return NotificationEnums.NotificationChannel.Correspondence;
        }

        public bool ConfigureSecondNotification()
        {
            return false;
        }

        public IEnumerable<CorrespondenceAttachmentsType> CorrespondenceAttachmentsTypes => new List<CorrespondenceAttachmentsType>()
        {
            CorrespondenceAttachmentsType.MainFormPdf, 
            CorrespondenceAttachmentsType.MainFormXml, 
            CorrespondenceAttachmentsType.Attachments
        };

        public IEnumerable<StoreToBlob> StoreToBlobs => new List<StoreToBlob>()
        {
            StoreToBlob.MainFormPdf, 
            StoreToBlob.MainFormXml, 
            StoreToBlob.Attachments
        };

        private DateTime? _replyDeadline;
        public void SetReplyDeadline(DateTime? ReplyDeadline)
        {
            _replyDeadline = ReplyDeadline;
        }

        public DateTime? GetReplyDeadline()
        {
            return _replyDeadline;
        }
        public bool IsStoreMessageFile()
        {
            return false;
        }
    }
}