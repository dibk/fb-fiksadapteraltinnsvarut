﻿using System;
using System.Collections.Generic;
using DIBK.FBygg.Altinn.MapperCodelist;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Validations;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;
using no.kxml.skjema.dibk.kontrollerklaeringVedDistribusjon;
using Serilog;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.NotificationForm.KontrollOgSamsvarserklaering
{
    public class KontrollerklaeringVedDistribusjonFormValidator
    {
        private ILogger _logger = Log.ForContext<KontrollerklaeringVedDistribusjonFormValidator>();

        CodeListService _codeListService = new CodeListService();
        private readonly string _skjema = "kontrollerklaeringVedDistribusjon";

        private ValidationResult _validationResult;

        public KontrollerklaeringVedDistribusjonFormValidator()
        {
            _validationResult = new ValidationResult();
            var form =new KontrollerklaeringType();
            _validationResult.AddRule("5445.1.1", "Innhold må være i henhold til informasjonsmodell/xsd for skjema.", "", "ERROR", "");
            _validationResult.AddRule(_skjema,"5445.1.3", "","Må fylle ut samme navn som er brukt i registrering for Altinn API i fraSluttbrukersystem.", Helpers.GetFullClassPath(() => form.fraSluttbrukersystem), "ERROR", "");
        }

        public ValidationResult Validate(KontrollerklaeringType form, List<string> formSetElements)
        {
            FraSluttbrukersystemErUtfylt(form);
            return _validationResult;
        }

        private void FraSluttbrukersystemErUtfylt(KontrollerklaeringType form)
        {
            if (String.IsNullOrEmpty(form.fraSluttbrukersystem))
            {
                _validationResult.AddMessage("5445.1.3",null);
            }
        }
   }

}