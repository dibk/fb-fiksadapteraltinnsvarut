﻿using System;
using System.Collections.Generic;
using System.EnterpriseServices.Internal;
using System.Linq;
using System.Web;
using Org.BouncyCastle.Asn1;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.NotificationForm.KontrollOgSamsvarserklaering
{
    public static class KontrollOgSamsvarMessages
    {
        /// <summary>
        /// Altinn melding - Tittel på varsel om signert kontrollerklæring
        /// </summary>
        /// <param name="prosjektnavn">Navn på prosjekt</param>
        /// <param name="adresselinje1">Eiendom byggested sin adresselinje1</param>
        /// <param name="ansvarligSokerNavn">Navn på ansvarlig søker</param>
        /// <param name="ansvarligSokerOrgnr">Ansvarlig søker sitt organisasjonsnummer</param>
        /// <returns></returns>
        public static string GetSignedKontrollErklaeringAltinnTitle(string prosjektnavn, string adresselinje1, string ansvarligSokerNavn, string ansvarligSokerOrgnr)
        {
            string title = $"Kontrollerklæring er signert - ";

            title += !string.IsNullOrWhiteSpace(prosjektnavn) ? prosjektnavn : adresselinje1;

            title += $", til {ansvarligSokerNavn} (org.nr. {ansvarligSokerOrgnr})";

            return title;
        }

        
        /// <summary>
        /// Altinn melding - Tittel varsel om på kontrollerklæring
        /// </summary>
        /// <param name="prosjektnavn">Navn på prosjekt</param>
        /// <param name="adresselinje1">Eiendom byggested sin adresselinje1</param>
        /// <param name="foretakNavn">Navn på foretak</param>
        /// <param name="foretakOrgNr">Foretak sitt organisasjonsnummer</param>
        /// <returns></returns>
        public static string GetPrefillNotificationTitle(string prosjektnavn, string adresselinje1, string foretakNavn, string foretakOrgNr, string kontaktperson)
        {
            string title = $"Kontrollerklæring til signering - ";

            title += !string.IsNullOrWhiteSpace(prosjektnavn) ? prosjektnavn : adresselinje1;

            string contactPersonText = ContactPersonInTitleText(kontaktperson);

            title += $"{contactPersonText}, {foretakNavn} (org.nr. {foretakOrgNr})";

            return title;
        }



        /// <summary>
        /// Altinn melding - Innholdet i varsel om mottatt kontrollerklæring
        /// </summary>
        /// <param name="kontaktperson">Foretak sin kontaktperson</param>
        /// <param name="kontaktpersonTlf">Ansvarlig søker kontaktperson sitt telefonnr</param>
        /// <param name="kontaktpersonMobil">Ansvarlig søker kontaktperson sitt mobilnr</param>
        /// <param name="kontaktpersonEpost">Ansvarlig søker kontaktperson sin epost</param>
        /// <param name="ansvarligSoekerNavn">Ansvarlig søker navn</param>
        /// <param name="ansvarligSoekerTlf">Ansvarlig søker telefonnr</param>
        /// <param name="ansvarligSoekerMobil">Ansvarlig søker mobilnr</param>
        /// <param name="ansvarligSoekerEpost">Ansvarlig søker epost</param>
        /// <param name="foretakNavn">Navn på foretak</param>
        /// <param name="adresselinje1">Eiendom byggested sin adresselinje1</param>
        /// <param name="gardsnummer">Eiendom byggested sitt gårdsnummer</param>
        /// <param name="bruksnummer">Eiendom byggested sitt bruksnummer</param>
        /// <param name="kodebeskrivelse">Ansvarsrett kodebeskrivelse</param>
        /// <returns></returns>
        public static string GetPrefillNotificationBody(string kontaktperson, string kontaktpersonTlf, string kontaktpersonMobil, string kontaktpersonEpost, string ansvarligSoekerNavn, string ansvarligSoekerTlf, string ansvarligSoekerMobil,
            string ansvarligSoekerEpost, string foretakNavn, string adresselinje1, string gardsnummer, string bruksnummer,
            string kodebeskrivelse)
        {

            string telefon = GetPhoneNumber(kontaktpersonTlf, kontaktpersonMobil, ansvarligSoekerTlf, ansvarligSoekerMobil);
            string epost = GetEmail(kontaktpersonEpost, ansvarligSoekerEpost);
            string contactPersonText = ContactPersonBodyContent(kontaktperson);

            string body = $"{ foretakNavn} har fått en kontrollerklæring til signering for { adresselinje1} " + $"(gårdsnr. {gardsnummer}, bruksnr. {bruksnummer})<br><br>";
            body += $"Trykk på lenken under for å fylle ut og signere erklæringen. Etter signering blir erklæringen sendt til den som står ansvarlig for byggeprosjektet.<br><br>";
            body += $"{contactPersonText} <br><br>";
            body += $"Erklæringen gjelder {kodebeskrivelse.ToLower()}.<br><br>";
            body += $"Spørsmål om erklæringen kan rettes til {ansvarligSoekerNavn}, på telefon {telefon} eller e-post {epost}.<br><br>";
            body += $"Altinnreferanse: ARXXXXXX<br><br>";
            return body;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ansvarligSoekerNavn">Navn på ansvarlig søker</param>
        /// <param name="foretakNavn">Navn på foretak</param>
        /// <param name="kodebeskrivelse">Ansvarsrett kodebeskrivelse</param>
        /// <param name="adresselinje1">Eiendom byggested sin adresselinje1</param>
        /// <param name="gardsnummer">Eiendom byggested sitt gårdsnummer</param>
        /// <param name="bruksnummer">Eiendom byggested sitt bruksnummer</param>
        /// <param name="prosjektnavn">Navn på prosjekt</param>
        /// <returns></returns>
        public static string GetNotificationMessageBodyKontrollerklaering(string ansvarligSoekerNavn, string foretakNavn, string kodebeskrivelse, string adresselinje1,
            string gardsnummer, string bruksnummer, string prosjektnavn)
        {
            string body = $"{ansvarligSoekerNavn} har fått en signert kontrollerklæring fra {foretakNavn}.<br><br>";
            body += $"Erklæringen gjelder {kodebeskrivelse.ToLower()}.<br><br>";
            body += $"Adresse: {adresselinje1} " + $"(gårdsnr. {gardsnummer}, bruksnr. {bruksnummer}).<br><br>";
            body += $"Prosjektnavn: {prosjektnavn}.<br><br>";
            body += $"Den signerte kontrollerklæringen skal beholdes av den som står ansvarlig for byggeprosjektet. Datoen for når erklæringen er signert, føres opp i gjennomføringsplanen. Gjennomføringsplanen skal sendes til kommunen sammen med byggesøknaden.<br><br>";
            return body;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="prosjektnavn">Navn på prosjekt</param>
        /// <param name="adresselinje1">Eiendom byggested sin adresselinje1</param>
        /// <param name="ansvarligSoekerNavn">Navn på ansvarlig søker</param>
        /// <param name="ansvarligSoekerOrgNr">Organisasjonsnummer til ansvarlig søker</param>
        /// <returns></returns>
        public static string GetNotificationMessageTitle(string prosjektnavn, string adresselinje1, string ansvarligSoekerNavn, string ansvarligSoekerOrgNr)
        {
            string title = $"Samsvarserklæring er signert - ";

            title += !string.IsNullOrWhiteSpace(prosjektnavn) ? prosjektnavn : adresselinje1;

            title += $", til {ansvarligSoekerNavn} (org.nr. {ansvarligSoekerOrgNr})";

            return title;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="ansvarligSoekerNavn">Navn på ansvarlig søker</param>
        /// <param name="foretakNavn">Navn på foretak</param>
        /// <param name="kodebeskrivelse">Ansvarsrett kodebeskrivelse</param>
        /// <param name="adresselinje1">Eiendom byggested sin adresselinje1</param>
        /// <param name="gaardsnummer">Eiendom byggested sitt gårdsnummer</param>
        /// <param name="bruksnummer">Eiendom byggested sitt bruksnummer</param>
        /// <param name="prosjektnavn">Navn på prosjekt</param>
        /// <returns></returns>
        public static string GetNotificationMessageBodySamsvarserklaering(string ansvarligSoekerNavn, string foretakNavn, string kodebeskrivelse, string adresselinje1, string gaardsnummer, string bruksnummer, string prosjektnavn)
        {
            string body = $"{ansvarligSoekerNavn} har fått en signert samsvarserklæring fra {foretakNavn}.<br><br>";
            body += $"Erklæringen gjelder {kodebeskrivelse.ToLower()}.<br><br>";
            body += $"Adresse: {adresselinje1} " + $"(gårdsnr. {gaardsnummer}, bruksnr. {bruksnummer}).<br><br>";
            body += $"Prosjektnavn: {prosjektnavn}.<br><br>";
            body += $"Den signerte samsvarserklæring skal beholdes av den som står ansvarlig for byggeprosjektet. Datoen for når erklæringen er signert, føres opp i gjennomføringsplanen. Gjennomføringsplanen skal sendes til kommunen sammen med byggesøknaden.<br><br>";
            return body;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="kontaktperson">Foretak sin kontaktperson for signering</param>
        /// <param name="kontaktpersonTlf">Ansvarlig søker kontaktperson sitt telefonnr</param>
        /// <param name="kontaktpersonMobil">Ansvarlig søker kontaktperson sitt mobilnr</param>
        /// <param name="kontaktpersonEpost">Ansvarlig søker kontaktperson sin epost</param>
        /// <param name="ansvarligSoekerNavn">Ansvarlig søker navn</param>
        /// <param name="ansvarligSoekerTlf">Ansvarlig søker telefonnr</param>
        /// <param name="ansvarligSoekerMobil">Ansvarlig søker mobilnr</param>
        /// <param name="ansvarligSoekerEpost">Ansvarlig søker epost</param>
        /// <param name="foretakNavn">Navn på foretak</param>
        /// <param name="adresselinje1">Eiendom byggested sin adresselinje1</param>
        /// <param name="gardsnummer">Eiendom byggested sitt gårdsnummer</param>
        /// <param name="bruksnummer">Eiendom byggested sitt bruksnummer</param>
        /// <param name="kodebeskrivelse">Ansvarsrett kodebeskrivelse</param>
        /// <returns></returns>
        public static string GetPrefillNotificationBodySamsvarserklaering
        (string kontaktperson, string kontaktpersonTlf, string kontaktpersonMobil, string kontaktpersonEpost, string ansvarligSoekerNavn, string ansvarligSoekerTlf, string ansvarligSoekerMobil,
            string ansvarligSoekerEpost, string foretakNavn, string adresselinje1, string gardsnummer, string bruksnummer,
            string kodebeskrivelse)
        {
            string telefon = GetPhoneNumber(kontaktpersonTlf, kontaktpersonMobil, ansvarligSoekerTlf, ansvarligSoekerMobil);
            string epost = GetEmail(kontaktpersonEpost, ansvarligSoekerEpost);
            string contactPersonText = ContactPersonBodyContent(kontaktperson);


            string body = $"{ foretakNavn} har fått en samsvarserklæring til signering for { adresselinje1} " + $"(gårdsnr. {gardsnummer}, bruksnr. {bruksnummer})<br><br>";
            body += $"Trykk på lenken under for å fylle ut og signere erklæringen. Etter signering blir erklæringen sendt til den som står ansvarlig for byggeprosjektet.<br><br>";
            body += $"{contactPersonText} <br><br>";
            body += $"Erklæringen gjelder {kodebeskrivelse.ToLower()}.<br><br>";
            body += $"Spørsmål om erklæringen kan rettes til {ansvarligSoekerNavn}, på telefon {telefon} eller e-post {epost}.<br><br>";
            body += $"Altinnreferanse: ARXXXXXX<br><br>";
            return body;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="prosjektnavn">Navn på prosjekt</param>
        /// <param name="adresselinje1">Eiendom byggested sin adresselinje1</param>
        /// <param name="foretakNavn">Navn på foretak</param>
        /// <param name="foretakOrgNr">Foretak sitt organisasjonsnummer</param>
        /// <param name="kontaktperson">Foretak sin kontaktperson</param>
        /// <returns></returns>
        public static string GetPrefillNotificationTitleSamsvarserklaering(string prosjektnavn, string adresselinje1, string foretakNavn, string foretakOrgNr, string kontaktperson)
        {
            string contactPersonInTitle = ContactPersonInTitleText(kontaktperson);
            string title = $"Samsvarserklæring til signering - ";

            title += !string.IsNullOrWhiteSpace(prosjektnavn) ? prosjektnavn : adresselinje1;

            title += $"{contactPersonInTitle}, {foretakNavn} (org.nr. {foretakOrgNr})";

            return title;
        }

        /// <summary>
        /// Distribusjon - KontrollOgSamsvarserklaeringForm
        /// </summary>
        /// <param name="kodeverdi">Ansvarsområde kodeverdi</param>
        /// <param name="prosjektnavn">Navn på prosjekt</param>
        /// <param name="adresselinje1">Eiendom byggested sin adresselinje1</param>
        /// <param name="foretakNavn">Navn på foretak</param>
        /// <param name="foretakOrgNr">Foretaket sitt organisasjonsnummer</param>
        /// <returns></returns>
        public static string GetDistributionMessageTitle(string kodeverdi, string prosjektnavn, string adresselinje1, string foretakNavn, string foretakOrgNr, string kontaktperson)
        {
            string contactPersonInTitle = ContactPersonInTitleText(kontaktperson);
            var messageTitle = kodeverdi == "KONTROLL" ? $"Kontrollerklæring" : $"Samsvarserklæring";
            messageTitle += $" til signering - ";

            messageTitle += !string.IsNullOrWhiteSpace(prosjektnavn) ? $"{prosjektnavn}" : $"{adresselinje1}";

            messageTitle += $"{contactPersonInTitle}, {foretakNavn} (org.nr. {foretakOrgNr})";

            return messageTitle;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="kodeverdi">Ansvarsrett kodeverdi</param>
        /// <param name="prosjektnavn">Navn på prosjekt</param>
        /// <param name="adresselinje1">Eiendom byggested sin adresselinje1</param>
        /// <param name="foretakNavn">Navn på foretak</param>
        /// <returns></returns>
        public static string GetEmailSubject(string kodeverdi, string prosjektnavn, string adresselinje1, string foretakNavn)
        {
            string emailSubject = kodeverdi == "KONTROLL"
                ? $"Kontrollerklæring til signering - "
                : $"Samsvarserklæring til signering - ";

            string prosjektnavnEllerByggestedAdresse = !string.IsNullOrWhiteSpace(prosjektnavn)
                ? prosjektnavn
                : adresselinje1;

            emailSubject += prosjektnavnEllerByggestedAdresse;
            emailSubject += $", til {foretakNavn}";

            return emailSubject;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="kodeverdi">Ansvarsrett kodeverdi</param>
        /// <param name="kodebeskrivelse">Ansvarsrett kodebeskrivelse</param>
        /// <param name="prosjektnavn">Navn på prosjekt</param>
        /// <param name="adresselinje1">Eiendom byggested sin adresselinje1</param>
        /// <param name="kontaktpersonForetak">Foretak sin kontaktperson</param>
        /// <param name="kontaktpersonTlf">Ansvarlig søker kontaktperson sitt telefonnummer</param>
        /// <param name="kontaktpersonMobil">Ansvarlig søker kontaktperson sitt mobilnummer</param>
        /// <param name="kontaktpersonEpost">Ansvarlig søker kontaktperson sin epost</param>
        /// <param name="kontaktpersonNavn">Ansvarlig søker kontaktperson sitt navn</param>
        /// <param name="ansvarligSoekerTlf">Ansvarlig søker sitt telefonnummer</param>
        /// <param name="ansvarligSoekerMobil">Ansvarlig søker sitt mobilnummer</param>
        /// <param name="ansvarligSoekerEpost">Ansvarlig søker sin epost</param>
        /// <param name="ansvarligSoekerNavn">Ansvarlig søker sitt navn</param>
        /// <param name="foretakNavn">navn på foretak</param>
        /// <param name="foretakOrgNr">foretak sitt organisasjonsnummer</param>
        /// <param name="kommunenavn">navn på kommune</param>
        /// <returns></returns>
        public static string GetEmailContent(string kodeverdi, string kodebeskrivelse, string prosjektnavn, string adresselinje1,
            string kontaktpersonForetak, string kontaktpersonTlf, string kontaktpersonMobil, string kontaktpersonEpost, string kontaktpersonNavn,
            string ansvarligSoekerTlf, string ansvarligSoekerMobil, string ansvarligSoekerEpost, string ansvarligSoekerNavn, string foretakNavn, string foretakOrgNr, string kommunenavn)
        {
            string erklaering = kodeverdi == "KONTROLL"
                ? $"Kontrollerklæring"
                : $"Samsvarserklæring";

            string prosjektnavnEllerByggestedAdresse = !string.IsNullOrWhiteSpace(prosjektnavn)
                ? prosjektnavn
                : adresselinje1;

            string telefon = GetPhoneNumber(kontaktpersonTlf, kontaktpersonMobil, ansvarligSoekerTlf, ansvarligSoekerMobil);
            string epost = GetEmail(kontaktpersonEpost, ansvarligSoekerEpost);
            string contactPersonText = ContactPersonBodyContent(kontaktpersonForetak);

            string emailContent =
                $"<p>{foretakNavn} har fått en {erklaering.ToLower()} til signering for {prosjektnavnEllerByggestedAdresse} i {kommunenavn} kommune. " +
                $"Logg inn på Altinn eller <a href='https://www.altinn.no/'>følg denne lenken</a>. </p>" +
                $"<p>Erklæringen gjelder {kodebeskrivelse}. <br>" +
                $"{contactPersonText}</p>" +
                $"<p>For å signere erklæringen, må du representere {foretakNavn} (org.nr. {foretakOrgNr}) i Altinn. " +
                $"Den som skal signere erklæringen, må ha rollen 'Plan- og byggesak' i firmaet. <br>Se <a href='https://dibk.no/verktoy-og-veivisere/andre-fagomrader/fellestjenester-bygg/slik-gir-du-rettigheter-til-byggesak-i-altinn'>veileder om rettigheter til byggesak i Altinn</a> for mer informasjon. </p>" +
                $"<p>Med vennlig hilsen, <br> " +
                $"{kontaktpersonNavn} for {ansvarligSoekerNavn}. </p>" +
                $"<p><em>Det er ikke mulig å svare på denne e-posten. Har du spørsmål om erklæringen, kan du spørre {ansvarligSoekerNavn} på e-post {epost} eller telefon {telefon}. <br>" +
                $"Spørsmål om signering i Altinn kan du rette til Direktoratet for byggkvalitet: ftb@dibk.no. Oppgi referanse ARXXXXXX.</em></p>";

            return emailContent;
        }

        private static string ContactPersonBodyContent(string kontaktpersonForetak)
        {
            return !string.IsNullOrWhiteSpace(kontaktpersonForetak) ? $"Kontaktperson for signering er {kontaktpersonForetak}. " : "";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="kodeverdi">Ansvarsrett kodeverdi</param>
        /// <param name="prosjektnavn">Navn på prosjekt</param>
        /// <param name="adresselinje1">Eiendom byggested sin adresselinje1</param>
        /// <param name="foretakNavn">Navn på foretak</param>
        /// <param name="foretakOrgNr">Foretak sitt organisasjonsnummer</param>
        /// <param name="ansvarligSoeker">Navn på ansvarlig søker</param>
        /// <returns></returns>
        public static string GetSMSNotificationMessage(string kodeverdi, string prosjektnavn, string adresselinje1, string foretakNavn, string foretakOrgNr, string ansvarligSoeker)
        {
            string erklaering = kodeverdi == "KONTROLL"
                ? $"Kontrollerklæring"
                : $"Samsvarserklæring";
            string prosjektnavnEllerByggestedAdresse = !string.IsNullOrWhiteSpace(prosjektnavn)
                ? prosjektnavn
                : adresselinje1;

            string sms =
                $"{foretakNavn} (org.nr. {foretakOrgNr}) har fått en " +
                $"{erklaering.ToLower()} til signering for {prosjektnavnEllerByggestedAdresse}. Logg inn på altinn.no for å signere. " +
                $"Referanse: ARXXXXXX. Hilsen {ansvarligSoeker}";
            return sms;
        }



        private static string GetPhoneNumber(string kontaktpersonTlf, string kontaktpersonMobil, string ansvarligSoekerTlf, string ansvarligSoekerMobil)
        {
            if (!string.IsNullOrWhiteSpace(kontaktpersonTlf))
            {
                return kontaktpersonTlf;
            }

            if (!string.IsNullOrWhiteSpace(kontaktpersonMobil))
            {
                return kontaktpersonMobil;
            }

            if (!string.IsNullOrWhiteSpace(ansvarligSoekerTlf))
            {
                return ansvarligSoekerTlf;
            }

            if (!string.IsNullOrWhiteSpace(ansvarligSoekerMobil))
            {
                return ansvarligSoekerMobil;
            }
            return "";
        }


        private static string GetEmail(string kontaktpersonEpost, string ansvarligSoekerEpost)
        {
            if (!string.IsNullOrWhiteSpace(kontaktpersonEpost))
            {
                return kontaktpersonEpost;
            }
            if (!string.IsNullOrWhiteSpace(ansvarligSoekerEpost))
            {
                return ansvarligSoekerEpost;
            }

            return "";
        }

        private static string ContactPersonInTitleText(string kontaktperson)
        {
            return !string.IsNullOrWhiteSpace(kontaktperson) ? $", til {kontaktperson}" : "";
        }


    }

}