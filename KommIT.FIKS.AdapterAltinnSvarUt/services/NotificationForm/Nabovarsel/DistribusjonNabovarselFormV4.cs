﻿using KommIT.FIKS.AdapterAltinnSvarUt.Extensions;
using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Encryption;
using no.kxml.skjema.dibk.nabovarselsvarV3;
using no.kxml.skjema.dibk.nabovarselV4;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Prefill;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;
using EiendommensAdresseType = no.kxml.skjema.dibk.nabovarselsvarV3.EiendommensAdresseType;
using EiendomType = no.kxml.skjema.dibk.nabovarselsvarV3.EiendomType;
using EnkelAdresseType = no.kxml.skjema.dibk.nabovarselsvarV3.EnkelAdresseType;
using KodeType = no.kxml.skjema.dibk.nabovarselsvarV3.KodeType;
using MatrikkelnummerType = no.kxml.skjema.dibk.nabovarselsvarV3.MatrikkelnummerType;
using NaboGjenboerType = no.kxml.skjema.dibk.nabovarselV4.NaboGjenboerType;
using PartType = no.kxml.skjema.dibk.nabovarselsvarV3.PartType;
using System.Configuration;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.NotificationForm.Nabovarsel
{
    public class DistribusjonNabovarselFormV4 : IAltinnForm, IDistributionForm, INabovarselDistribution
    {
        private const string DataFormatId = "6303";
        private const string DataFormatVersion = "44820";
        private const string SchemaFile = "nabovarselV4.xsd";
        private const string Name = "Distribusjon av nabovarsel";

        private no.kxml.skjema.dibk.nabovarselV4.NabovarselType form;

        private List<string> FormSetElements;

        public void SetFormSetElements(List<string> attachmentlist)
        {
            FormSetElements = attachmentlist;
        }

        public bool? ValidateAuthenticatedSubmitterVsFormData(string authenticatedSubmitter, ValidationResult validationResult)
        {
            bool isSubmitterValid = Helpers.ValidateEntityOfAltinnSubmitterDefault(authenticatedSubmitter, GetPropertyIdentifiers());

            if (!isSubmitterValid)
            {
                Helpers.RecordErrorAltinnSubmitter(validationResult);
            }
            return isSubmitterValid;
        }

        public string GetDistributionMessageTitle()
        {
            string adresselinje1 = form.eiendomByggested.First().adresse.adresselinje1;
            string ansvarligSoeker = form.ansvarligSoeker.navn;
            return NabovarselMessages.GetDistributionMessageTitle(adresselinje1, ansvarligSoeker);
        }

        public string GetDistributionMessageSummary()
        {
            return $"";
        }

        public string GetDistributionMessageBody()
        {
            string adresselinje1 = form.eiendomByggested.First().adresse.adresselinje1;
            string postnr = form.eiendomByggested.First().adresse.postnr;
            string poststed = form.eiendomByggested.First().adresse.poststed;
            string tiltakshaver = form.tiltakshaver.navn;
            string ansvarligSoeker = form.ansvarligSoeker.navn;

            return NabovarselMessages.GetDistributionMessageBody(adresselinje1, postnr, poststed, tiltakshaver, ansvarligSoeker);
        }

        /// <summary>
        /// Hente ut svarskjema/prefill skjema fra et distribusjonsskjema
        /// </summary>
        /// <returns></returns>
        public List<IAltinnForm> GetPrefillForms()
        {
            return GetPrefillFormsLocal();
        }

        private string GetSMSNotificationMessage(string nabo, string orgnr, EiendomType gjelderNaboeiendom)
        {
            var adresse = gjelderNaboeiendom.adresse;
            string adresselinje1 = "";

            if (adresse != null)
            {
                adresselinje1 = gjelderNaboeiendom.adresse.adresselinje1;
            }
            string gaardsnummer = gjelderNaboeiendom.eiendomsidentifikasjon.gaardsnummer;
            string bruksnummer = gjelderNaboeiendom.eiendomsidentifikasjon.bruksnummer;

            return NabovarselMessages.GetNaboSMSNotificationMessage(adresselinje1, gaardsnummer, bruksnummer, nabo, orgnr);
        }


        private string GetEmailNotificationBody(string nabo, string orgnr)
        {
            var adresse = form.eiendomByggested.First().adresse;
            string adresselinje1 = "";

            if (adresse != null)
            {
                adresselinje1 = form.eiendomByggested.First().adresse.adresselinje1;
            }

            string gaardsnummer = form.eiendomByggested.First().eiendomsidentifikasjon.gaardsnummer;
            string bruksnummer = form.eiendomByggested.First().eiendomsidentifikasjon.bruksnummer;
            string festenummer = form.eiendomByggested.First().eiendomsidentifikasjon.festenummer;
            string seksjonsnummer = form.eiendomByggested.First().eiendomsidentifikasjon.seksjonsnummer;
            string kommunenavn = form.eiendomByggested.First().kommunenavn;
            TiltakType[] beskrivelseAvTiltak = form.beskrivelseAvTiltak;
            string ansvarligSoekerNavn = form.ansvarligSoeker.navn;
            string kontaktpersonTlf = KontaktpersonTlf(); ;
            string kontaktpersonEpost = KontaktpersonEpost();


            return NabovarselMessages.GetEmailNotificationBody(adresselinje1, gaardsnummer, bruksnummer, festenummer,
                seksjonsnummer, nabo, orgnr, kommunenavn, beskrivelseAvTiltak, kontaktpersonTlf, kontaktpersonEpost,
                ansvarligSoekerNavn);
        }

        private string KontaktpersonEpost()
        {
            if (!form.tiltakshaver.kontaktperson.epost.IsNullOrEmpty())
            {
                return form.tiltakshaver.kontaktperson.epost;
            }
            if (!form.ansvarligSoeker.kontaktperson.epost.IsNullOrEmpty())
            {
                return form.ansvarligSoeker.kontaktperson.epost;
            }
            if (!form.ansvarligSoeker.epost.IsNullOrEmpty())
            {
                return form.ansvarligSoeker.epost;
            }
            if (!form.tiltakshaver.epost.IsNullOrEmpty())
            {
                return form.tiltakshaver.epost;
            }

            return "";
        }

        private string KontaktpersonTlf()
        {
            if (!form.tiltakshaver.kontaktperson.telefonnummer.IsNullOrEmpty())
            {
                return form.tiltakshaver.kontaktperson.telefonnummer;
            }

            if (!form.ansvarligSoeker.kontaktperson.telefonnummer.IsNullOrEmpty())
            {
                return form.ansvarligSoeker.kontaktperson.telefonnummer;
            }

            if (!form.ansvarligSoeker.telefonnummer.IsNullOrEmpty())
            {
                return form.ansvarligSoeker.telefonnummer;
            }

            if (!form.tiltakshaver.telefonnummer.IsNullOrEmpty())
            {
                return form.tiltakshaver.telefonnummer;
            }

            return "";

        }

        public PropertyIdentifiers GetPropertyIdentifiers()
        {
            PropertyIdentifiers propertyIdentifiers = new PropertyIdentifiers();

            propertyIdentifiers.Kommunenummer = form.eiendomByggested[0].eiendomsidentifikasjon.kommunenummer;
            propertyIdentifiers.Gaardsnummer = form.eiendomByggested[0].eiendomsidentifikasjon.gaardsnummer;
            propertyIdentifiers.Bruksnummer = form.eiendomByggested[0].eiendomsidentifikasjon.bruksnummer;
            propertyIdentifiers.Festenummer = form.eiendomByggested[0].eiendomsidentifikasjon.festenummer;
            propertyIdentifiers.Seksjonsnummer = form.eiendomByggested[0].eiendomsidentifikasjon.seksjonsnummer;
            propertyIdentifiers.Adresselinje1 = form.eiendomByggested[0].adresse.adresselinje1;
            propertyIdentifiers.Adresselinje2 = form.eiendomByggested[0].adresse.adresselinje2;
            propertyIdentifiers.Adresselinje3 = form.eiendomByggested[0].adresse.adresselinje3;
            propertyIdentifiers.Postnr = form.eiendomByggested[0].adresse.postnr;
            propertyIdentifiers.Poststed = form.eiendomByggested[0].adresse.poststed;
            propertyIdentifiers.Landkode = form.eiendomByggested[0].adresse.landkode;
            propertyIdentifiers.Bygningsnummer = form.eiendomByggested[0].bygningsnummer;
            propertyIdentifiers.Bolignummer = form.eiendomByggested[0].bolignummer;
            propertyIdentifiers.SoeknadSkjemaNavn = Name;
            propertyIdentifiers.FraSluttbrukersystem = form.fraSluttbrukersystem;

            propertyIdentifiers.AnsvarligSokerNavn = form.ansvarligSoeker.navn;
            propertyIdentifiers.AnsvarligSokerOrgnr = form.ansvarligSoeker.organisasjonsnummer;
            propertyIdentifiers.AnsvarligSokerFnr = form.ansvarligSoeker.foedselsnummer;

            return propertyIdentifiers;
        }

        public string GetDataFormatId()
        {
            return DataFormatId;
        }

        public string GetDataFormatVersion()
        {
            return DataFormatVersion;
        }

        public string GetMunicipalityCode()
        {
            return form.eiendomByggested[0].eiendomsidentifikasjon.kommunenummer;
        }

        public string GetSchemaFile()
        {
            return SchemaFile;
        }

        public string GetName()
        {
            return Name;
        }

        public void InitiateForm(string formDataAsXml)
        {
            form = Utils.SerializeUtil.DeserializeFromString<no.kxml.skjema.dibk.nabovarselV4.NabovarselType>(formDataAsXml);
        }

        public ValidationResult ValidateData()
        {
            var validationResult = new NabovarselV4Validator().Validate(form, FormSetElements);
            return validationResult;
        }

        public void CopyMainPdfToFormsAttchments(FormData formData)
        {
            if (formData.MainFormPdf != null)
            {
                formData.MainFormPdf.FileName = "Nabovarsel.pdf";
                formData.Attachments.Insert(0, formData.MainFormPdf);
            }
        }

        public string GetProjectName()
        {
            return form.prosjektnavn;
        }

        public string GetContactPerson()
        {
            StringBuilder builder = new StringBuilder();
            if (!form.tiltakshaver.kontaktperson.navn.IsNullOrEmpty())
            {
                builder.Append(form.tiltakshaver.kontaktperson.navn);
                if (!form.tiltakshaver.kontaktperson.mobilnummer.IsNullOrEmpty())
                {
                    builder.Append("<br> Mobil: ");
                    builder.Append(form.tiltakshaver.kontaktperson.mobilnummer);
                }

                if (!form.tiltakshaver.kontaktperson.telefonnummer.IsNullOrEmpty())
                {
                    builder.Append("<br> Telefon: ");
                    builder.Append(form.tiltakshaver.kontaktperson.telefonnummer);
                }

                if (!form.tiltakshaver.kontaktperson.epost.IsNullOrEmpty())
                {
                    builder.Append("<br> E-post: ");
                    builder.Append(form.tiltakshaver.kontaktperson.epost);
                    builder.Append("<br><br>");
                }
            }

            if (!form.ansvarligSoeker.kontaktperson.navn.IsNullOrEmpty())
            {
                builder.Append(form.ansvarligSoeker.kontaktperson.navn);
                if (!form.ansvarligSoeker.kontaktperson.mobilnummer.IsNullOrEmpty())
                {
                    builder.Append("<br> Mobil: ");
                    builder.Append(form.ansvarligSoeker.kontaktperson.mobilnummer);
                }

                if (!form.ansvarligSoeker.kontaktperson.telefonnummer.IsNullOrEmpty())
                {
                    builder.Append("<br> Telefon: ");
                    builder.Append(form.ansvarligSoeker.kontaktperson.telefonnummer);
                }

                if (!form.ansvarligSoeker.kontaktperson.epost.IsNullOrEmpty())
                {
                    builder.Append("<br> E-post: ");
                    builder.Append(form.ansvarligSoeker.kontaktperson.epost);
                }
            }

            else if (!form.ansvarligSoeker.navn.IsNullOrEmpty())
            {
                builder.Append(form.ansvarligSoeker.navn);
                if (!form.ansvarligSoeker.mobilnummer.IsNullOrEmpty())
                {
                    builder.Append("<br> Mobil: ");
                    builder.Append(form.ansvarligSoeker.mobilnummer);
                }
                if (!form.ansvarligSoeker.telefonnummer.IsNullOrEmpty())
                {
                    builder.Append("<br> Telefon: ");
                    builder.Append(form.ansvarligSoeker.telefonnummer);
                }
                if (!form.ansvarligSoeker.epost.IsNullOrEmpty())
                {
                    builder.Append("<br> E-post: ");
                    builder.Append(form.ansvarligSoeker.epost);
                }
            }

            else
            {
                builder.Append(form.tiltakshaver.navn);
                if (!form.tiltakshaver.mobilnummer.IsNullOrEmpty())
                {
                    builder.Append("<br> Mobil: ");
                    builder.Append(form.tiltakshaver.mobilnummer);
                }
                if (!form.tiltakshaver.telefonnummer.IsNullOrEmpty())
                {
                    builder.Append("<br> Telefon: ");
                    builder.Append(form.tiltakshaver.telefonnummer);
                }
                if (!form.tiltakshaver.epost.IsNullOrEmpty())
                {
                    builder.Append("<br> E-post: ");
                    builder.Append(form.tiltakshaver.epost);
                }
            }



            return builder.ToString();
        }

        public bool PrintIsEnabled()
        {
            return true;
        }

        public IDistributionReceiptService GetDistributionReceiptService()
        {
            return new DistributionReceiptService();
        }

        private List<IAltinnForm> GetPrefillFormsLocal()
        {
            // Naboliste sortert etter foedselsnummer og orgnummer, slik at naboer med mange eiendommer ligger under samme id.
            Dictionary<string, List<NaboGjenboerType>> sortedNeighbors = sortNeighborsByIdCode();

            List<SvarPaaNabovarselType> neighborReplyForm = generateNeighborList(sortedNeighbors);


            List<IAltinnForm> prefillForms = new List<IAltinnForm>();

            foreach (var nabovarselSvar in neighborReplyForm)
            {
                SvarPaNabovarselFormV3 prefilledSvarPaaNabovarsel = new SvarPaNabovarselFormV3();

                // Get a list of the sluttbrukersystemVaarReferanse included in this form
                // (for cases were same entity are receiveing a consolidate notice of more than one owned property)

                List<string> requestingSystemReferences = new List<string>();
                foreach (var ownedproperty in nabovarselSvar.nabo.gjelderNaboeiendom)
                {
                    requestingSystemReferences.Add(ownedproperty.sluttbrukersystemVaarReferanse);
                }

                prefilledSvarPaaNabovarsel.SetSluttbrukersystemVaarReferanse(requestingSystemReferences);

                // e-post og sms oppsett fra denne klassen blir satt opp også i svarPaaNabovarselFormV3
                prefilledSvarPaaNabovarsel.SetEmailNotification(nabovarselSvar.nabo.epost,
                    $"Du har mottatt et nabovarsel for byggeplaner i nærheten av {nabovarselSvar.nabo.gjelderNaboeiendom.First().adresse.adresselinje1}",
                    GetEmailNotificationBody(nabovarselSvar.nabo.navn, nabovarselSvar.nabo.organisasjonsnummer),
                    GetSMSNotificationMessage(nabovarselSvar.nabo.navn, nabovarselSvar.nabo.organisasjonsnummer, nabovarselSvar.nabo.gjelderNaboeiendom.First()));

                prefilledSvarPaaNabovarsel.InitiateForm(Utils.SerializeUtil.Serialize(nabovarselSvar));

                prefillForms.Add(prefilledSvarPaaNabovarsel);
            }

            return prefillForms;
        }

        private Dictionary<string, List<NaboGjenboerType>> sortNeighborsByIdCode()
        {
            Dictionary<string, List<NaboGjenboerType>> neighborsGrouped =
                new Dictionary<string, List<NaboGjenboerType>>();

            foreach (NaboGjenboerType naboeier in form.naboeier)
            {
                var keyId = "";

                if (naboeier.partstype.kodeverdi.Equals("Privatperson", StringComparison.InvariantCultureIgnoreCase))
                {
                    keyId = Helpers.GetDecryptedFnr(naboeier.foedselsnummer);
                }
                else
                {
                    keyId = naboeier.organisasjonsnummer;
                }

                if (neighborsGrouped.ContainsKey(keyId))
                {
                    neighborsGrouped[keyId].Add(naboeier);
                }
                else
                {
                    neighborsGrouped.Add(keyId, new List<NaboGjenboerType> { naboeier });
                }
            }

            return neighborsGrouped;
        }

        private List<SvarPaaNabovarselType> generateNeighborList(Dictionary<string, List<NaboGjenboerType>> data)
        {
            List<SvarPaaNabovarselType> svarPaaNabovarselListe = new List<SvarPaaNabovarselType>();

            foreach (KeyValuePair<string, List<NaboGjenboerType>> naboeier in data)
            {
                SvarPaaNabovarselType svarPaaNabovarsel = new SvarPaaNabovarselType();

                // Ansvarlig søker
                var adresseansvarligsoker = new EnkelAdresseType();
                if (form.ansvarligSoeker.adresse != null)
                {
                    adresseansvarligsoker.adresselinje1 = form.ansvarligSoeker.adresse.adresselinje1;
                    adresseansvarligsoker.adresselinje2 = form.ansvarligSoeker.adresse.adresselinje2;
                    adresseansvarligsoker.adresselinje3 = form.ansvarligSoeker.adresse.adresselinje3;
                    adresseansvarligsoker.postnr = form.ansvarligSoeker.adresse.postnr;
                    adresseansvarligsoker.poststed = form.ansvarligSoeker.adresse.poststed;
                    adresseansvarligsoker.landkode = form.ansvarligSoeker.adresse.landkode;
                }

                svarPaaNabovarsel.ansvarligSoeker = new PartType()
                {
                    partstype = new KodeType()
                    {
                        kodeverdi = form.ansvarligSoeker.partstype.kodeverdi,
                        kodebeskrivelse = form.ansvarligSoeker.partstype.kodebeskrivelse
                    },

                    foedselsnummer = form.ansvarligSoeker.foedselsnummer,
                    organisasjonsnummer = form.ansvarligSoeker.organisasjonsnummer,
                    navn = form.ansvarligSoeker.navn,
                    adresse = adresseansvarligsoker,
                    telefonnummer = form.ansvarligSoeker.telefonnummer,
                    mobilnummer = form.ansvarligSoeker.mobilnummer,
                    epost = form.ansvarligSoeker.epost
                };

                // EiendomByggested
                List<EiendomType> eiendomsliste = new List<EiendomType>();

                foreach (var eiendom in form.eiendomByggested)
                {
                    var edm = new EiendomType()
                    {
                        eiendomsidentifikasjon = new MatrikkelnummerType()
                        {
                            kommunenummer = eiendom.eiendomsidentifikasjon.kommunenummer,
                            gaardsnummer = eiendom.eiendomsidentifikasjon.gaardsnummer,
                            bruksnummer = eiendom.eiendomsidentifikasjon.bruksnummer,
                            festenummer = eiendom.eiendomsidentifikasjon.festenummer,
                            seksjonsnummer = eiendom.eiendomsidentifikasjon.seksjonsnummer
                        },

                        adresse = new EiendommensAdresseType()
                        {
                            adresselinje1 = eiendom.adresse.adresselinje1,
                            adresselinje2 = eiendom.adresse.adresselinje2,
                            adresselinje3 = eiendom.adresse.adresselinje3,
                            postnr = eiendom.adresse.postnr,
                            poststed = eiendom.adresse.poststed,
                            landkode = eiendom.adresse.landkode,
                            gatenavn = eiendom.adresse.gatenavn,
                            husnr = eiendom.adresse.husnr,
                            bokstav = eiendom.adresse.bokstav
                        },

                        bygningsnummer =
                            eiendom.bygningsnummer,

                        bolignummer = eiendom.bolignummer,
                        kommunenavn = eiendom.kommunenavn
                    };

                    eiendomsliste.Add(edm);
                }

                svarPaaNabovarsel.eiendomByggested = eiendomsliste.ToArray();


                // Nabo
                List<EiendomType> gjelderNaboeiendom = new List<EiendomType>();

                foreach (var naboeiendom in naboeier.Value)
                {
                    var edm = new EiendomType();

                    edm.eiendomsidentifikasjon = new MatrikkelnummerType()
                    {
                        kommunenummer = naboeiendom.gjelderNaboeiendom.eiendomsidentifikasjon.kommunenummer,
                        gaardsnummer = naboeiendom.gjelderNaboeiendom.eiendomsidentifikasjon.gaardsnummer,
                        bruksnummer = naboeiendom.gjelderNaboeiendom.eiendomsidentifikasjon.bruksnummer,
                        festenummer = naboeiendom.gjelderNaboeiendom.eiendomsidentifikasjon.festenummer,
                        seksjonsnummer = naboeiendom.gjelderNaboeiendom.eiendomsidentifikasjon.seksjonsnummer
                    };

                    edm.adresse = new EiendommensAdresseType()
                    {
                        adresselinje1 = naboeiendom.gjelderNaboeiendom.adresse.adresselinje1,
                        adresselinje2 = naboeiendom.gjelderNaboeiendom.adresse.adresselinje2,
                        adresselinje3 = naboeiendom.gjelderNaboeiendom.adresse.adresselinje3,
                        postnr = naboeiendom.gjelderNaboeiendom.adresse.postnr,
                        poststed = naboeiendom.gjelderNaboeiendom.adresse.poststed,
                        landkode = naboeiendom.gjelderNaboeiendom.adresse.landkode,
                        gatenavn = naboeiendom.gjelderNaboeiendom.adresse.gatenavn,
                        husnr = naboeiendom.gjelderNaboeiendom.adresse.husnr,
                        bokstav = naboeiendom.gjelderNaboeiendom.adresse.bokstav
                    };

                    edm.bygningsnummer = naboeiendom.gjelderNaboeiendom.bygningsnummer;
                    edm.bolignummer = naboeiendom.gjelderNaboeiendom.bolignummer;
                    edm.kommunenavn = naboeiendom.gjelderNaboeiendom.kommunenavn;
                    edm.sluttbrukersystemVaarReferanse = naboeiendom.sluttbrukersystemVaarReferanse;


                    gjelderNaboeiendom.Add(edm);
                }

                var naboFirstElement = naboeier.Value.FirstOrDefault();

                svarPaaNabovarsel.nabo = new no.kxml.skjema.dibk.nabovarselsvarV3.NaboGjenboerType()
                {
                    partstype = new KodeType()
                    {
                        kodeverdi = naboFirstElement.partstype.kodeverdi,
                        kodebeskrivelse = naboFirstElement.partstype.kodebeskrivelse
                    },

                    foedselsnummer = naboFirstElement.foedselsnummer,

                    organisasjonsnummer = naboFirstElement.organisasjonsnummer,

                    navn = naboFirstElement.navn,

                    adresse = new EnkelAdresseType()
                    {
                        adresselinje1 = naboFirstElement.adresse.adresselinje1,
                        adresselinje2 = naboFirstElement.adresse.adresselinje2,
                        adresselinje3 = naboFirstElement.adresse.adresselinje3,
                        postnr = naboFirstElement.adresse.postnr,
                        poststed = naboFirstElement.adresse.poststed,
                        landkode = naboFirstElement.adresse.landkode
                    },

                    telefonnummer = naboFirstElement.telefonnummer,
                    mobilnummer = naboFirstElement.mobilnummer,
                    epost = naboFirstElement.epost,
                    gjelderNaboeiendom = gjelderNaboeiendom.ToArray(),
                    harPersonligSamtykkeTilTiltaketSpecified = false
                };


                svarPaaNabovarsel.hovedinnsendingsnummer = form.hovedinnsendingsnummer;
                svarPaaNabovarsel.fraSluttbrukersystem = form.fraSluttbrukersystem;
                svarPaaNabovarsel.prosjektnavn = form.prosjektnavn;
                svarPaaNabovarsel.samtykkeTilTiltaketSpecified = false;

                svarPaaNabovarsel.tiltakshaver = new PartType()
                {
                    partstype = new KodeType()
                    {
                        kodeverdi = form.tiltakshaver.partstype.kodeverdi,
                        kodebeskrivelse = form.tiltakshaver.partstype.kodebeskrivelse
                    },

                    foedselsnummer = form.tiltakshaver.foedselsnummer,
                    organisasjonsnummer = form.tiltakshaver.organisasjonsnummer,
                    navn = form.tiltakshaver.navn,
                    adresse = new EnkelAdresseType()
                    {
                        adresselinje1 = form.tiltakshaver.adresse.adresselinje1,
                        adresselinje2 = form.tiltakshaver.adresse.adresselinje2,
                        adresselinje3 = form.tiltakshaver.adresse.adresselinje3,
                        postnr = form.tiltakshaver.adresse.postnr,
                        poststed = form.tiltakshaver.adresse.poststed,
                        landkode = form.tiltakshaver.adresse.landkode
                    },

                    telefonnummer = form.tiltakshaver.telefonnummer,
                    mobilnummer = form.tiltakshaver.mobilnummer,
                    epost = form.tiltakshaver.epost
                };
                svarPaaNabovarselListe.Add(svarPaaNabovarsel);
            }

            return svarPaaNabovarselListe;
        }

    }
}
