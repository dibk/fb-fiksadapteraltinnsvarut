using KommIT.FIKS.AdapterAltinnSvarUt.Extensions;
using no.kxml.skjema.dibk.nabovarselV4;
using System;
using System.Text;
using EiendomType = no.kxml.skjema.dibk.nabovarselsvarV3.EiendomType;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.NotificationForm.Nabovarsel
{
    /// <summary>
    /// Meldinger ved distribusjon og svar på nabovarsler.
    /// </summary>
    public static class NabovarselMessages
    {
        /// <summary>
        /// Tittel på sending av nabovarsel
        /// </summary>
        /// <param name="adresselinje1">Eiendom byggested sin adresselinje1</param>
        /// <param name="ansvarligSoeker">Navn på ansvarig søker</param>
        /// <returns></returns>
        public static string GetDistributionMessageTitle(string adresselinje1, string ansvarligSoeker)
        {
            return $"Nabovarsel for byggeplaner i {adresselinje1} fra {ansvarligSoeker}";
        }

        /// <summary>
        /// Altinnmelding ved sending/distribusjon av nabovarsel
        /// </summary>
        /// <param name="adresselinje1">Eiendom byggested sin adresselinje1</param>
        /// <param name="postnr">Eiendom byggested sitt postnr</param>
        /// <param name="poststed">Eiendom byggested sitt poststed</param>
        /// <param name="tiltakshaver">Navn på tiltakshaver</param>
        /// <param name="ansvarligSoeker">Navn på ansvarlig søker</param>
        /// <returns></returns>
        public static string GetDistributionMessageBody(string adresselinje1, string postnr, string poststed, string tiltakshaver, string ansvarligSoeker)
        {
            string body = $"Vedlagt nabovarsel for adresse {adresselinje1}, {postnr} {poststed}<br>";
            body += $" Tiltakshaver er {tiltakshaver}<br>";
            body += $" Ansvarlig søker er {ansvarligSoeker}<br>";
            body += $" Du kan svare med vedlagte svarskjema innen 14 dager til ansvarlig søker om du har merknader til byggeplanene. <br>";
            body += $" Dersom du som nabo/gjenboer ikke har merknad til byggeprosjektet, og derfor ikke trenger 14 dagers fristen for å komme med";
            body += $" innspill, er det til stor hjelp om du kvitterer med vedlagte svarskjema for at du ikke trenger 14 dagers - perioden.";
            return body;
        }

        /// <summary>
        /// SMSmelding ved nabovarsling
        /// </summary>
        /// <param name="adresselinje1">Adresselinje1 til aktuell nabo</param>
        /// <param name="gaardsnummer">Gårdsnummer til aktuell nabo</param>
        /// <param name="bruksnummer">Bruksnummer til aktuell nabo</param>
        /// <param name="nabo">Navn på nabo</param>
        /// <param name="orgnr">Nabo sitt organisasjonsnummer, dersom det finnes.</param>
        /// <returns></returns>
        public static string GetNaboSMSNotificationMessage(string adresselinje1, string gaardsnummer, string bruksnummer, string nabo, string orgnr)
        {
            string naboeiendom = !string.IsNullOrWhiteSpace(adresselinje1)
                ? $"{adresselinje1}"
                : $"eiendom med gårds- og bruksnummer {gaardsnummer}/{bruksnummer}";

            string evtOrgnr = "";
            if (!string.IsNullOrEmpty(orgnr)) evtOrgnr = $" (org.nr. {orgnr})";

            string body = $"{nabo}{evtOrgnr} har mottatt et nabovarsel fordi noen har planer om å bygge i nærheten av {naboeiendom}. Logg inn på Altinn for å se nabovarselet. Du må svare innen 14 dager hvis du har merknader.";
            return body;
        }

        /// <summary>
        /// Epostmelding ved nabovarsling
        /// </summary>
        /// <param name="adresselinje1">Eiendom byggested sin adresse </param>
        /// <param name="gaardsnummer">Eiendom byggested sitt gårdsnummer</param>
        /// <param name="bruksnummer">Eiendom byggested sitt bruksnummer</param>
        /// <param name="festenummer">Eiendom byggested sitt festenummer</param>
        /// <param name="seksjonsnummer">Eiendom byggested sitt seksjonsnummer</param>
        /// <param name="nabo">Navn på nabo</param>
        /// <param name="orgnr">Nabo sitt organisasjonsnummer</param>
        /// <param name="kommunenavn">Navn på kommune</param>
        /// <param name="beskrivelseAvTiltak">Beskrivelse av tiltak</param>
        /// <param name="telefon">Telefon til kontaktperson</param>
        /// <param name="epost">Epost til kontaktperson</param>
        /// <param name="ansvarligSoekerNavn">Navn på ansvarlig søker</param>
        /// <returns></returns>
        public static string GetEmailNotificationBody(string adresselinje1, string gaardsnummer, string bruksnummer, string festenummer, string seksjonsnummer, string nabo, string orgnr, string kommunenavn, TiltakType[] beskrivelseAvTiltak, string telefon, string epost, string ansvarligSoekerNavn)
        {
            string nabovarselFor = !string.IsNullOrWhiteSpace(adresselinje1)
                ? $"i {adresselinje1}"
                : $"på eiendom med gårds- og bruksnummer {gaardsnummer}/{bruksnummer}/{festenummer}/{seksjonsnummer}";

            string evtOrgnr = "";
            if (!string.IsNullOrEmpty(orgnr)) evtOrgnr = $" (org.nr. {orgnr})";

            string omOrg = "";
            if (!string.IsNullOrEmpty(orgnr)) omOrg = $"Den som skal svare på nabovarselet, må ha riktige rettigheter i Altinn. <a href='https://dibk.no/verktoy-og-veivisere/andre-fagomrader/fellestjenester-bygg/slik-gir-du-rettigheter-til-byggesak-i-altinn'> Her ser du hvordan du får rettigheter til byggesak i Altinn</a>";

            string body = $"Til {nabo}{evtOrgnr}<br>";
            body += $" <br>";
            body += $"Du har mottatt et nabovarsel for byggeplaner {nabovarselFor} i {kommunenavn}. <br>";
            body += $" <br>";

            body += $"Varselet gjelder: <br>";
            foreach (var item in beskrivelseAvTiltak)
            {
                foreach (var tiltak in item.type)
                {
                    body += $" - {tiltak.kodebeskrivelse}<br>";
                }
            }

            body += $" <br>";
            body += $"<strong>Logg inn på Altinn for å se og svare på nabovarselet.</strong><br>";
            body += $"Du må svare innen 14 dager dersom du har merknader.<br><br>";
            body += $"<strong>Har du spørsmål?</strong><br>";
            body += $"Spørsmål om innholdet i nabovarselet må du rette til {ansvarligSoekerNavn}.<br>";
            body += $"Du kan også se vanlige spørsmål og svar om nabovarsel på dibk.no/nabovarsel-i-altinn<br>";
            body += $" <br>";
            body += $"Med vennlig hilsen <br>";
            body += $"{ansvarligSoekerNavn} <br>";
            body += $" <br>";
            body += $"Telefon: {telefon} <br>";
            body += $"Epost: {epost} <br>";
            body += $" <br>";
            body += $"<em>Det er ikke mulig å svare på denne e-posten.<br>";
            body += $"Altinnreferanse er ARXXXXXX.</em><br>";
            body += $" <br>";
            return body;
        }

        /// <summary>
        /// Tittel på melding om svar på nabovarsel. Mottas av ansvarlig søker
        /// </summary>
        /// <param name="byggestedAdresse">Byggestedets adresse</param>
        /// <param name="prosjektnavn">Navn på prosjekt</param>
        /// <returns></returns>
        public static string GetNotificationMessageTitle(string byggestedAdresse, string prosjektnavn)
        {
            string prosjektNavnText = prosjektnavn == null ? "" : $", {prosjektnavn}";
            return $"Svar på nabovarsel, {byggestedAdresse}{prosjektNavnText}";
        }


        /// <summary>
        /// Svarmelding på nabovarsel
        /// </summary>
        /// <param name="adresselinje1">Byggested eiendom sin adresselinje1</param>
        /// <param name="gaardsnummer">Byggested eiendom sitt gårdsnummer</param>
        /// <param name="bruksnummer">Byggested eiendom sitt bruksnummer</param>
        /// <param name="festenummer">Byggested eiendom sitt festenummer</param>
        /// <param name="seksjonsnummer">Byggested eiendom sitt seksjonsnummer</param>
        /// <returns></returns>
        public static string GetNotificationMessageBody(string adresselinje1, string gaardsnummer, string bruksnummer, string festenummer, string seksjonsnummer, string postnr, string poststed)
        {
            string nabovarselFor = "";
            if (!String.IsNullOrEmpty(adresselinje1))
                nabovarselFor = $"{adresselinje1}, {postnr} {poststed}";
            else
                nabovarselFor = $"eiendom med gårds- og bruksnummer {gaardsnummer}/{bruksnummer}/{festenummer}/{seksjonsnummer}";

            string body = $" Vedlagt svar på nabovarsel gjelder {nabovarselFor}<br><br>";
            body += $" Du må sende eventuelle merknader fra naboer, med dine kommentarer til hver merknad, til kommunen sammen med byggesøknaden. Du kan også velge å endre byggeprosjektet og nabovarsle på nytt.<br>";
            return body;
        }

        /// <summary>
        /// Svar på nabovarsel - Prefill notifikasjon tittel
        /// </summary>
        /// <param name="adresselinje1">Eiendom byggested sin adresselinje1</param>
        /// <param name="nabo">Navn på nabo</param>
        /// <param name="orgnr">Organisasjonsnr til nabo (hvis foretak)</param>
        /// <returns></returns>
        public static string GetPrefillNotificationTitle(string adresselinje1, string orgnr, string nabo)
        {
            string orgNrText = orgnr != null ? $" ({orgnr})" : "";

            return $"Nabovarsel for {adresselinje1} til {nabo}{orgNrText}";
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="adresselinje1">Eiendom byggested sin adresselinje1</param>
        /// <param name="postnr">Eiendom byggested sitt postnr </param>
        /// <param name="poststed">Eiendom byggested sitt poststed</param>
        /// <param name="gaardsnummer">Eiendom byggested sitt gårdsnummer</param>
        /// <param name="bruksnummer">Eiendom byggested sitt bruksnummer</param>
        /// <param name="festenummer">Eiendom byggested sitt festenummer</param>
        /// <param name="seksjonsnummer">Eiendom byggested sitt seksjonsnummer</param>
        /// <param name="ansvarligSoeker">Navn på ansvarlig søker</param>
        /// <param name="ansvarligSoekerEpost">Epost til ansvarlig søker</param>
        /// <param name="kontaktpersonTlf">Telefonnummer til ansvarlig søker</param>
        /// <param name="ansvarligSoekerAdresselinje1">Ansvarlig søker sin adresselinje1</param>
        /// <param name="ansvarligSoekerPostnr">Ansvarlig søker sitt postnr</param>
        /// <param name="ansvarligSoekerPoststed">Ansvarlig søker sitt poststed</param>
        /// <param name="gjelderNaboeiendom">Naboer som varsles om byggeplaner</param>
        /// <param name="prefillLinkText">Link til svarskjema</param>
        /// <param name="nabo">Navn på nabo</param>
        /// <param name="naboAdresselinje1">Nabo sin adresselinje1</param>
        /// <param name="naboPostnr">Nabo sitt postnr</param>
        /// <param name="naboPoststed">Nabo sitt poststed</param>
        /// <param name="pdf">Skal det lages en pdf</param>
        /// <param name="replyLink">Finnes det en replay link</param>
        /// <returns></returns>
        public static string GetPrefillNotificationBody(string adresselinje1, string postnr, string poststed, string gaardsnummer, string bruksnummer, string festenummer, string seksjonsnummer,
            string ansvarligSoeker, string ansvarligSoekerEpost, string kontaktpersonTlf, string ansvarligSoekerAdresselinje1, string ansvarligSoekerPostnr, string ansvarligSoekerPoststed,
            EiendomType[] gjelderNaboeiendom, string prefillLinkText,
            string nabo, string naboAdresselinje1, string naboPostnr, string naboPoststed,
            bool? pdf = false, string replyLink = "")
        {
            string nabovarselFor = "";
            nabovarselFor = !String.IsNullOrEmpty(adresselinje1) ?
                $"i {adresselinje1}, {postnr} {poststed} (gårdsnr. {gaardsnummer}, bruksnr. {bruksnummer})" :
                $"på eiendom med gårdsnr.: {gaardsnummer}, bruksnr.: {bruksnummer}, festenr.: {festenummer}, seksjonsnr.: {seksjonsnummer}";
            
            string body;

            if (pdf.HasValue && pdf.Value)
            {
                body = CreateBodyForPDF(nabovarselFor, nabo, naboAdresselinje1, naboPostnr, naboPoststed, gjelderNaboeiendom,
                ansvarligSoeker, ansvarligSoekerAdresselinje1, ansvarligSoekerPostnr, ansvarligSoekerPoststed, ansvarligSoekerEpost, kontaktpersonTlf);
            }
            else
            {
                body = $" Du har mottatt et nabovarsel for byggeplaner {nabovarselFor}. Klikk på dokumentene nederst i denne meldingen for å åpne nabovarselet og vedleggene.<br>";
                body += $" <br>";
                body += $" <strong>Hvorfor har jeg fått nabovarsel?</strong><br>";
                body += $" Du mottar nabovarselet fordi {ansvarligSoeker} har planer om å bygge eller gjøre endringer i nærheten av følgende eiendom: <br>";

                foreach (var naboeiendom in gjelderNaboeiendom)
                {
                    StringBuilder eiendomsdata = new StringBuilder();
                    if (!naboeiendom.adresse.adresselinje1.IsNullOrEmpty())
                    {
                        eiendomsdata.Append("- " + naboeiendom.adresse.adresselinje1);
                    }
                    if (!naboeiendom.eiendomsidentifikasjon.gaardsnummer.IsNullOrEmpty())
                    {
                        eiendomsdata.Append(", gårdsnr. " + naboeiendom.eiendomsidentifikasjon.gaardsnummer);
                    }
                    if (!naboeiendom.eiendomsidentifikasjon.bruksnummer.IsNullOrEmpty())
                    {
                        eiendomsdata.Append(", bruksnr. " + naboeiendom.eiendomsidentifikasjon.bruksnummer);
                    }
                    if (!naboeiendom.eiendomsidentifikasjon.seksjonsnummer.IsNullOrEmpty())
                    {
                        eiendomsdata.Append(", seksjonsnr. " + naboeiendom.eiendomsidentifikasjon.seksjonsnummer);
                    }
                    if (!naboeiendom.eiendomsidentifikasjon.festenummer.IsNullOrEmpty())
                    {
                        eiendomsdata.Append(", festenr. " + naboeiendom.eiendomsidentifikasjon.festenummer + ".");
                    }
                    body = body + $"{eiendomsdata}<br>";
                }

                body += $" <br>";
                body += $" Du kan bruke svarskjemaet nederst i meldingen for å si om du er enig eller uenig i byggeplanene. Hvis du har spørsmål om byggeplanene, kan du kontakte {ansvarligSoeker}";
                body += $" <br><br>";
                
                body += $" <strong>Har du merknader til byggeplanene?</strong><br>";
                body += $" Du kan sende inn merknader hvis du mener at byggeplanene påvirker deg negativt. Du må sende merknader til byggeplanene innen 14 dager. " +
                              $"Fristen finner du øverst til høyre i denne meldingen. Bruke svarskjemet under til å sende inn merknader.<br>";
                body += $" <br>";
                body += $" <strong>Hva skjer med merknadene dine?</strong><br>";
                body += $" Den som er ansvarlig søker for byggeprosjektet skal sende merknadene dine til kommunen sammen med byggesøknaden. Kommunen vurderer om merknadene fra deg og andre naboer påvirker byggeplanene.<br>";
                body += $" <br>";
                body += $" <strong>Har du ingen merknader til byggeplanene?</strong><br>";
                body += $" Da trenger du ikke gjøre noe som helst, men det er til hjelp for søker om du krysser av for at du ikke har merknader i svarskjemaet under.<br>";

                body += $" <br>";
                body += $" <strong>Vil du vite mer om nabovarsling?</strong><br>";
                body += $" <a href='https://dibk.no/bygge-selv/slik-varsler-du-naboene/har-du-fatt-nabovarsel'>Les mer på nettsiden til Direktoratet for byggkvalitet</a><br>";
                body += $" <br>";
                body += $" <strong>Svarskjema for merknader</strong><br>";

                if (!String.IsNullOrEmpty(replyLink))
                {
                    body = body + $" <a href='{replyLink}'>{prefillLinkText}</a><br>";
                    body = body + $" <br>";
                }
            }

            return body;
        }

        /// <summary>
        /// Svar på nabovarsel PDF
        /// </summary>
        /// <param name="nabovarselFor"></param>
        /// <param name="nabo">Navn på nabo</param>
        /// <param name="naboAdresselinje1">Nabo sin adresselinje1</param>
        /// <param name="naboPostnr">Nabo sitt postnr</param>
        /// <param name="naboPoststed">Nabo sitt poststed</param>
        /// <param name="ansvarligSoeker">Navn på ansvarlig søker</param>
        /// <param name="ansvarligSoekerAdresselinje1">Ansvarlig søker sin adresselinje 1</param>
        /// <param name="ansvarligSoekerPostnr">Ansvarlig søker sitt postnr</param>
        /// <param name="ansvarligSoekerPoststed">Ansvarlig søker sitt poststed</param>
        /// <param name="ansvarligSoekerEpost">Ansvarlig søker sin epost</param>
        /// <param name="gjelderNaboeiendom">Hvilke eiendommer det gjelder</param>
        /// 
        /// <returns></returns>
        private static string CreateBodyForPDF(string nabovarselFor, string nabo, string naboAdresselinje1, string naboPostnr, string naboPoststed, EiendomType[] gjelderNaboeiendom,
            string ansvarligSoeker, string ansvarligSoekerAdresselinje1, string ansvarligSoekerPostnr, string ansvarligSoekerPoststed, string ansvarligSoekerEpost, string ansvarligSoekerTlf)
        {
            var deadlineDate = DateTime.Now.AddDays(14);
            var deadlinedateNOFormat = deadlineDate.ToString("dd.MM.yy");
            var date = DateTime.Now.ToString("dd.MM.yyy");

            var body = $"<div class='Name'> <b>{nabo}</b></div>";
            body += $"<div class='Paragraf'>{naboAdresselinje1}</div>";
            body += $"<div class='Paragraf'>{naboPostnr} {naboPoststed}</div>";
            body += $"<div class='Paragraf PaddingTop'>{date}</div>";
            body += $"<div class='Heading PaddingTop Paddingbottom'><b> Melding om nabovarsel</b></div>";
            body += $"<div class='Paragraf PaddingTop'>Du har mottatt et nabovarsel for {nabovarselFor}.</div> ";
            body += $"<div class='SubHeading'>Hvorfor har jeg fått nabovarsel?</div>";
            body += $"<div class='Paragraf'>Du mottar nabovarselet fordi {ansvarligSoeker} har planer om å bygge eller gjøre endringer i nærheten av følgende eiendom: </div>";
            foreach (var naboeiendom in gjelderNaboeiendom)
            {
                body += $"<div class='Paragraf'> - {naboeiendom.adresse.adresselinje1} (gårdsnr. {naboeiendom.eiendomsidentifikasjon.gaardsnummer}, bruksnr. {naboeiendom.eiendomsidentifikasjon.bruksnummer})</div>";
            }

            body += $"<div class='SubHeading'>Har du merknader til byggeplanene?</div>";
            body += $"<div class='Paragraf'>Du kan sende inn merknader hvis du mener at byggeplanene påvirker deg negativt. Du må sende merknader til byggeplanene innen 14 dager. Fristen er {deadlinedateNOFormat}.</div> ";

            body += $"<div class='SubHeading'>Hva skjer med merknadene dine? </div>";
            body += $"<div class='Paragraf'>Den som er ansvarlig søker for byggeprosjektet skal sende merknadene dine til kommunen sammen med byggesøknaden.</div>";
            body += $"<div class='Paragraf'>Kommunen vurderer om merknadene fra deg og andre naboer påvirker byggeplanene.</div>";

            body += $"<div class='SubHeading'>Har du ingen merknader til byggeplanene?</div>";
            body += $"<div class='Paragraf'>Da trenger du ikke gjøre noe som helst.</div> ";

            body += $"<div class='SubHeading'>Merknader sendes til:</div>";
            body += $"<div class='Paragraf'>{ansvarligSoeker}</div>";
            body += $"<div class='Paragraf'>Adresse: {ansvarligSoekerAdresselinje1}, {ansvarligSoekerPostnr} {ansvarligSoekerPoststed}.</div>";
            body += $"<div class='Paragraf'>Merknader kan også sendes på e-post: {ansvarligSoekerEpost}</div>";

            return body;
        }

        public static string GetNotificationMessageSummary(string nabo)
        {
            return $"Melding er sendt fra {nabo} via Direktoratet for Byggkvalitet";

        }
    }
}