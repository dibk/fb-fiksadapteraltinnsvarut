﻿using KommIT.FIKS.AdapterAltinnSvarUt.Extensions;
using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using no.kxml.skjema.dibk.nabovarselsvarV3;
using System;
using System.Collections.Generic;
using System.Linq;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.NotificationForm.Nabovarsel
{
    public class SvarPaNabovarselFormV3 : IAltinnForm, INotificationForm, IPrefillForm, INabovarselSvar
    {

        private const string DataFormatId = "6173";
        private const string DataFormatVersion = "44215";
        private const string SchemaFile = "nabovarselsvarV3.xsd";
        private const string Name = "Svar på nabovarsel";
        private List<string> FormSetElements;
        private List<string> SluttbrukersystemVaarReferanse;

        public void SetFormSetElements(List<string> attachmentlist)
        {
            FormSetElements = attachmentlist;
        }

        public bool? ValidateAuthenticatedSubmitterVsFormData(string authenticatedSubmitter, ValidationResult validationResult)
        {
            bool isSubmitterValid = Helpers.ValidateEntityOfAltinnSubmitterDefault(authenticatedSubmitter, GetPropertyIdentifiers());

            if (!isSubmitterValid)
            {
                Helpers.RecordErrorAltinnSubmitter(validationResult);
            }
            return isSubmitterValid;
        }

        private no.kxml.skjema.dibk.nabovarselsvarV3.SvarPaaNabovarselType form;

        private string _archiveReferenceOfDistribution = String.Empty;
        private string _altinnReferenceForPrefillFrom = String.Empty;

        public string GetSendToReporteeId()
        {
            return Utils.Helpers.ReturnFirstOrgNumberOrPersonalNumber(
                        form.ansvarligSoeker.organisasjonsnummer,
                        form.ansvarligSoeker.foedselsnummer).IdNumber;
        }
        public string GetNotificationMessageTitle()
        {
            string byggestedsAdresse = form.eiendomByggested.First().adresse.adresselinje1;
            string prosjektnavn = form.prosjektnavn;

            return NabovarselMessages.GetNotificationMessageTitle(byggestedsAdresse, prosjektnavn);

        }
        public string GetNotificationMessageSender()
        {
            return form.nabo.navn;
        }
        public string GetNotificationMessageSummary()
        {
            string nabo = form.nabo.navn;
            return NabovarselMessages.GetNotificationMessageSummary(nabo);
        }

        public string GetNotificationMessageBody()
        {
            var adresse = form.eiendomByggested.First().adresse;
            string adresselinje1 = "";
            string postnr = "";
            string poststed = "";

            if (adresse != null)
            {
                adresselinje1 = form.eiendomByggested.First().adresse.adresselinje1;
                postnr = form.eiendomByggested.First().adresse.postnr;
                poststed = form.eiendomByggested.First().adresse.poststed;
            }

            string gaardsnummer = form.eiendomByggested.First().eiendomsidentifikasjon.gaardsnummer;
            string bruksnummer = form.eiendomByggested.First().eiendomsidentifikasjon.bruksnummer;
            string festenummer = form.eiendomByggested.First().eiendomsidentifikasjon.festenummer;
            string seksjonsnummer = form.eiendomByggested.First().eiendomsidentifikasjon.seksjonsnummer;
            
            return NabovarselMessages.GetNotificationMessageBody(adresselinje1, gaardsnummer, bruksnummer, festenummer, seksjonsnummer, postnr, poststed);
        }

        public bool isReply()
        {
            Guid incommingGuid;
            return Guid.TryParse(form.hovedinnsendingsnummer, out incommingGuid);
        }

        public Guid GetIncommingGuid()
        {
            Guid incommingGuid;
            var isReplyAnsvarsrett = Guid.TryParse(form.hovedinnsendingsnummer, out incommingGuid);
            if (!isReplyAnsvarsrett)
            {
                incommingGuid = Guid.NewGuid();
            }
            return incommingGuid;
        }



        public PropertyIdentifiers GetPropertyIdentifiers()
        {
            PropertyIdentifiers propertyIdentifiers = new PropertyIdentifiers();

            propertyIdentifiers.Kommunenummer = form.eiendomByggested[0].eiendomsidentifikasjon.kommunenummer;
            propertyIdentifiers.Gaardsnummer = form.eiendomByggested[0].eiendomsidentifikasjon.gaardsnummer;
            propertyIdentifiers.Bruksnummer = form.eiendomByggested[0].eiendomsidentifikasjon.bruksnummer;
            propertyIdentifiers.Festenummer = form.eiendomByggested[0].eiendomsidentifikasjon.festenummer;
            propertyIdentifiers.Seksjonsnummer = form.eiendomByggested[0].eiendomsidentifikasjon.seksjonsnummer;
            propertyIdentifiers.Adresselinje1 = form.eiendomByggested[0].adresse.adresselinje1;
            propertyIdentifiers.Adresselinje2 = form.eiendomByggested[0].adresse.adresselinje2;
            propertyIdentifiers.Adresselinje3 = form.eiendomByggested[0].adresse.adresselinje3;
            propertyIdentifiers.Postnr = form.eiendomByggested[0].adresse.postnr;
            propertyIdentifiers.Poststed = form.eiendomByggested[0].adresse.poststed;
            propertyIdentifiers.Landkode = form.eiendomByggested[0].adresse.landkode;
            propertyIdentifiers.Bygningsnummer = form.eiendomByggested[0].bygningsnummer;
            propertyIdentifiers.Bolignummer = form.eiendomByggested[0].bolignummer;
            propertyIdentifiers.SoeknadSkjemaNavn = Name;
            propertyIdentifiers.FraSluttbrukersystem = form.fraSluttbrukersystem;


            return propertyIdentifiers;
        }


        public string GetDataFormatId()
        {
            return DataFormatId;
        }

        public string GetDataFormatVersion()
        {
            return DataFormatVersion;
        }

        public string GetMunicipalityCode()
        {
            return GetPropertyIdentifiers().Kommunenummer;
        }

        public string GetSchemaFile()
        {
            return SchemaFile;
        }

        public string GetName()
        {
            return Name;
        }

        public void InitiateForm(string formDataAsXml)
        {
            form = Utils.SerializeUtil.DeserializeFromString<SvarPaaNabovarselType>(formDataAsXml);
        }

        public ValidationResult ValidateData()
        {
            ValidationResult valResult = new ValidationResult(0, 0);
            
            return valResult;
        }

        public bool IsReply()
        {
            Guid incomingGuid;
            return Guid.TryParse(form.hovedinnsendingsnummer, out incomingGuid);
        }

        public Guid GetReplyKey()
        {
            Guid incomingGuid;
            var isReply = Guid.TryParse(form.hovedinnsendingsnummer, out incomingGuid);
            return incomingGuid;
        }

        public int GetReplyDueDays()
        {
            return 14;
        }

        public string GetNotificationXMLFilename()
        {
            return Resources.TextStrings.NotificationNabovarselXMLFilename;
        }

        public string GetNotificationXMLTitle()
        {
            return Resources.TextStrings.NotificationNabovarselXMLTitle;
        }

        public string GetNotificationPdfFilename()
        {
            return Resources.TextStrings.NotificationNabovarselPdfFilename;
        }

        public string GetNotificationPdfTitle()
        {
            return Resources.TextStrings.NotificationNabovarselPdfTitle;
        }

        public string GetPrefillSendToReporteeId()
        {
            return Utils.Helpers.ReturnFirstOrgNumberOrPersonalNumber(
                        form.nabo.organisasjonsnummer,
                        form.nabo.foedselsnummer).IdNumber;
        }

        public string GetFormXML()
        {
            return Utils.SerializeUtil.Serialize(form);
        }

        public string GetPrefillServiceCode()
        {
            return "4699";
        }

        public string GetPrefillServiceEditionCode()
        {
            return "3";
        }

        public string GetPrefillNotificationTitle()
        {
            string adresselinje1 = "";
            if (form.eiendomByggested.First().adresse != null)
            {
                adresselinje1 = form.eiendomByggested.First().adresse.adresselinje1;
            }
            string nabo = form.nabo.navn;
            string orgnr = form.nabo.organisasjonsnummer;
            
            return NabovarselMessages.GetPrefillNotificationTitle(adresselinje1, orgnr, nabo);

        }

        public string GetPrefillNotificationSummary()
        {
            return $"";
        }


        public string GetPrefillNotificationBody(bool? pdf = false, string replyLink = "")
        {
            string adresselinje1 = "";
            string postnr = "";
            string poststed = "";
            string gaardsnummer = form.eiendomByggested.First().eiendomsidentifikasjon.gaardsnummer;
            string bruksnummer = form.eiendomByggested.First().eiendomsidentifikasjon.bruksnummer;
            string festenummer = form.eiendomByggested.First().eiendomsidentifikasjon.festenummer;
            string seksjonsnummer = form.eiendomByggested.First().eiendomsidentifikasjon.seksjonsnummer;
            if (form.eiendomByggested.First().adresse != null)
            {
                adresselinje1 = form.eiendomByggested.First().adresse.adresselinje1;
                postnr = form.eiendomByggested.First().adresse.postnr;
                poststed = form.eiendomByggested.First().adresse.poststed;
            }

            string ansvarligSoeker = form.ansvarligSoeker.navn;
            string ansvarligSoekerEpost = form.ansvarligSoeker.epost;
            string kontaktpersonTlf = KontaktpersonTlf();
            string ansvarligSoekerAdresselinje1 = form.ansvarligSoeker.adresse.adresselinje1;
            string ansvarligSoekerPostnr = form.ansvarligSoeker.adresse.postnr;
            string ansvarligSoekerPoststed = form.ansvarligSoeker.adresse.poststed;
            
            string prefillLinkText = GetPrefillLinkText();
            
            string nabo = form.nabo.navn;
            string naboAdresselinje1 = form.nabo.adresse.adresselinje1;
            string naboPostnr = form.nabo.adresse.postnr;
            string naboPoststed = form.nabo.adresse.poststed;

            return NabovarselMessages.GetPrefillNotificationBody(adresselinje1, postnr, poststed, gaardsnummer,
                bruksnummer, festenummer, seksjonsnummer,
                ansvarligSoeker, ansvarligSoekerEpost, kontaktpersonTlf, ansvarligSoekerAdresselinje1,
                ansvarligSoekerPostnr, ansvarligSoekerPoststed, form.nabo.gjelderNaboeiendom, prefillLinkText,
                nabo, naboAdresselinje1, naboPostnr, naboPoststed, pdf, replyLink);
        }

        
        public int GetPrefillNotificationDueDays()
        {
            return 14;
        }

        public void SetPrefillKey(string key)
        {
            form.hovedinnsendingsnummer = key;
        }
        public string GetPrefillKey()
        {
            return form.hovedinnsendingsnummer;
        }
        public string GetPrefillLinkText()
        {
            return Resources.TextStrings.NabovarselLinkText;
        }

        public string GetPrefillMessageSender()
        {
            return form.ansvarligSoeker.navn;
        }

        public string GetPrefillOurReference()
        {
            return form.nabo.gjelderNaboeiendom.FirstOrDefault().sluttbrukersystemVaarReferanse;
        }

        public bool DoEmailNotification()
        {
            if (_emailnotification != null) return true;
            else return false;
        }

        public void SetEmailNotification(string email, string subject, string emailcontent, string smscontent)
        {
            _emailnotification = new EmailNotification();
            _emailnotification.Email = email;
            _emailnotification.EmailSubject = subject;
            _emailnotification.EmailContent = emailcontent;
            _emailnotification.SmsContent = smscontent;
        }
        private EmailNotification _emailnotification;
        public EmailNotification GetEmailNotification()
        {
            return _emailnotification;
        }

        public string GetArchiveReferenceOfDistribution()
        {
            return _archiveReferenceOfDistribution;
        }

        public void SetArchiveReferenceOfDistribution(string ar)
        {
            _archiveReferenceOfDistribution = ar;
        }

        public string GetAltinnReferenceForPrefillFrom()
        {
            return _altinnReferenceForPrefillFrom;
        }

        public void SetAltinnReferenceForPrefillFrom(string altinnReference)
        {
            _altinnReferenceForPrefillFrom = altinnReference;
        }

        public NotificationEnums.NotificationCarrier GetNotificationType()
        {
            return NotificationEnums.NotificationCarrier.AltinnEmailPreferred;
        }

        public string GetAltinnNotificationTemplate()
        {
            return Resources.TextStrings.AltinnNotificationTemplateNabo;
        }


        public NotificationEnums.NotificationChannel GetNotificationChannel()
        {
            return NotificationEnums.NotificationChannel.CorrespondenceWithPrefillEndpointValidation;
        }

        public bool ConfigureSecondNotification()
        {
            return false;
        }

        public Address GetNeighborMailingAddress()
        {
            return  new Address(form.nabo.navn, 
                                form.nabo.adresse.adresselinje1, form.nabo.adresse.adresselinje2, form.nabo.adresse.adresselinje3, 
                                form.nabo.adresse.poststed,
                                form.nabo.adresse.postnr,
                                form.nabo.adresse.landkode);
        }

        public Address GetApplicantMailingAddress()
        {
            var addr =  new Address(form.ansvarligSoeker.navn,
                form.ansvarligSoeker.adresse.adresselinje1, form.ansvarligSoeker.adresse.adresselinje2, form.ansvarligSoeker.adresse.adresselinje3,
                form.ansvarligSoeker.adresse.poststed,
                form.ansvarligSoeker.adresse.postnr,
                form.ansvarligSoeker.adresse.landkode);

            if (!String.IsNullOrWhiteSpace(form.ansvarligSoeker.organisasjonsnummer))
            {
                addr.OrganizationNumber = form.ansvarligSoeker.organisasjonsnummer;
            }
            else if (!String.IsNullOrWhiteSpace(form.ansvarligSoeker.foedselsnummer))
            {
                addr.SosialServiceNumber = form.ansvarligSoeker.foedselsnummer;
            }
            else
            {
                addr.OrganizationNumber = null;
                addr.SosialServiceNumber = null;
            }
            return addr;
        }

        public List<string> GetSluttbrukersystemVaarReferanse()
        {
            return SluttbrukersystemVaarReferanse;
        }

        public void SetSluttbrukersystemVaarReferanse(List<string> refs)
        {
            SluttbrukersystemVaarReferanse = refs;
        }

        private DateTime? _replyDeadline;
        public void SetReplyDeadline(DateTime? ReplyDeadline)
        {
            _replyDeadline = ReplyDeadline;
        }

        public DateTime? GetReplyDeadline()
        {
            return _replyDeadline;
        }

        public KvitteringForNabovarsel GetkvitteringForNabovarsel()
        {
            var kvitteringForNabovarsel = new KvitteringForNabovarsel()
            {
                Hovedinnsendingsnummer = form.hovedinnsendingsnummer,
                NaboNavn = form.nabo.navn,
                GjelderNaboeiendommer = new List<Eiendom>(),
                Eiendommer = new List<Eiendom>()
            };
            if (form.nabo.adresse != null)
            {
                kvitteringForNabovarsel.NaboAdress = string.Concat(form.nabo.adresse.adresselinje1, " ", form.nabo.adresse.adresselinje2, " ", form.nabo.adresse.adresselinje3);
                kvitteringForNabovarsel.NaboPostnr = form.nabo.adresse.postnr;
                kvitteringForNabovarsel.NaboPoststed = form.nabo.adresse.poststed;
            }

            foreach (var gjelderNaboeiendom in form.nabo.gjelderNaboeiendom)
            {
                var naboeiendom = new Eiendom()
                {
                    Gaardsnummer = gjelderNaboeiendom.eiendomsidentifikasjon.gaardsnummer,
                    Bruksnumme = gjelderNaboeiendom.eiendomsidentifikasjon.bruksnummer,
                    Festenummer = gjelderNaboeiendom.eiendomsidentifikasjon.festenummer,
                    Seksjonsnummer = gjelderNaboeiendom.eiendomsidentifikasjon.seksjonsnummer,
                    Kommunenavn = gjelderNaboeiendom.kommunenavn
                };
                if (gjelderNaboeiendom.adresse != null)
                {
                    naboeiendom.EiendomsAdress = gjelderNaboeiendom.adresse.adresselinje1;
                    if (!gjelderNaboeiendom.adresse.adresselinje2.IsNullOrEmpty())
                    {
                        naboeiendom.EiendomsAdress = naboeiendom.EiendomsAdress + ", " + gjelderNaboeiendom.adresse.adresselinje2;
                    }
                    if (!gjelderNaboeiendom.adresse.adresselinje3.IsNullOrEmpty())
                    {
                        naboeiendom.EiendomsAdress = naboeiendom.EiendomsAdress + ", " + gjelderNaboeiendom.adresse.adresselinje3;
                    }
                    naboeiendom.EiendomsPostnr = gjelderNaboeiendom.adresse.postnr;
                    naboeiendom.EiendomsPoststed = gjelderNaboeiendom.adresse.poststed;
                }
                kvitteringForNabovarsel.GjelderNaboeiendommer.Add(naboeiendom);
            }
         
            foreach (EiendomType eiendomType in form.eiendomByggested)
            {
                var eiendom = new Eiendom()
                {
                    Gaardsnummer = eiendomType.eiendomsidentifikasjon.gaardsnummer,
                    Bruksnumme = eiendomType.eiendomsidentifikasjon.bruksnummer,
                    Festenummer = eiendomType.eiendomsidentifikasjon.festenummer,
                    Seksjonsnummer = eiendomType.eiendomsidentifikasjon.seksjonsnummer,
                    Kommunenavn = eiendomType.kommunenavn,

                };
                if (eiendomType.adresse != null)
                {
                    eiendom.EiendomsAdress = string.Concat(eiendomType.adresse.adresselinje1, " ",
                        eiendomType.adresse.adresselinje2, " ", eiendomType.adresse.adresselinje3);
                    eiendom.EiendomsPostnr = eiendomType.adresse.postnr;
                    eiendom.EiendomsPoststed = eiendomType.adresse.poststed;
                }
                kvitteringForNabovarsel.Eiendommer.Add(eiendom);
            }
            return kvitteringForNabovarsel;
        }

        private string KontaktpersonTlf()
        {
            if (!form.ansvarligSoeker.telefonnummer.IsNullOrEmpty())
            {
                return form.ansvarligSoeker.telefonnummer;
            }

            if (!form.ansvarligSoeker.mobilnummer.IsNullOrEmpty())
            {
                return form.ansvarligSoeker.mobilnummer;
            }

            return "";

        }
        public bool IsStoreMessageFile()
        {
            return false;
        }
        public IEnumerable<CorrespondenceAttachmentsType> CorrespondenceAttachmentsTypes => new List<CorrespondenceAttachmentsType>()
        {
            CorrespondenceAttachmentsType.MainFormPdf, 
            CorrespondenceAttachmentsType.MainFormXml, 
            CorrespondenceAttachmentsType.Attachments
        };

        public IEnumerable<StoreToBlob> StoreToBlobs => new List<StoreToBlob>()
        {
            StoreToBlob.MainFormPdf, 
            StoreToBlob.MainFormXml, 
            StoreToBlob.Attachments
        };


    }
}