﻿using System.Collections.Generic;
using KommIT.FIKS.AdapterAltinnSvarUt.Models;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.NotificationForm.Nabovarsel
{
   public interface INabovarselSvar
    {
        KvitteringForNabovarsel GetkvitteringForNabovarsel();
        Address GetNeighborMailingAddress();
        Address GetApplicantMailingAddress();
        List<string> GetSluttbrukersystemVaarReferanse();
        void SetSluttbrukersystemVaarReferanse(List<string> refs);
    }
}
