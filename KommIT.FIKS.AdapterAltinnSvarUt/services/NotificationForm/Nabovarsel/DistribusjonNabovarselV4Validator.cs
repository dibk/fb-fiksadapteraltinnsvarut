using DIBK.FBygg.Altinn.MapperCodelist;
using KommIT.FIKS.AdapterAltinnSvarUt.Logger;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Matrikkel;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.PostalCode;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Validations;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;
using no.kxml.skjema.dibk.nabovarselV4;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.NotificationForm.Nabovarsel
{
    /// <summary>
    /// Validator for NabovarselV3
    /// </summary>
    public class NabovarselV4Validator
    {
        private ILogger _logger = Log.ForContext<NabovarselV4Validator>();
        private CodeListService _codeListService = new CodeListService();

        public ValidationResult _validationResult;
        private List<string> _tiltakstyperISoeknad = new List<string>();
        private IMunicipalityValidator _municipalityValidator;

        public IMunicipalityValidator MunicipalityValidator
        {
            get { return _municipalityValidator == null ? new MunicipalityValidator() : _municipalityValidator; }
            set { _municipalityValidator = value; }
        }

        public IMatrikkelService _matrikkelService { get; set; }
        public IPostalCodeProvider _postalCodeProvider { get; set; }

        public NabovarselV4Validator()
        {
            _matrikkelService = new MatrikkelService();
            _postalCodeProvider = new BringPostalCodeProvider();

            var form = new NabovarselType();
            var _skjema = "Nabovarsel";
            _validationResult = new ValidationResult()
            {
                Soknadtype = "NV",
            };

            _validationResult.AddRule(_skjema, "4655.1.1", null, "Innhold må være ihht informasjonsmodell/xsd for skjema.", "/Vedlegg", "ERROR", null);

            //FraSluttbrukersystemErUtfylt
            _validationResult.AddRule(_skjema, "4655.3.17", null, "Systemet må fylle ut samme navn som er brukt i registrering for Altinn API i fraSluttbrukersystem.", Helpers.GetFullClassPath(() => form.fraSluttbrukersystem), "ERROR", null);

            //EiendomByggestedValidering
            _validationResult.AddRule(_skjema, "4655.1.1.13", "1.2", "Kommunenummer må fylles ut for eiendom/byggested.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].eiendomsidentifikasjon.kommunenummer), "ERROR", null);
            _validationResult.AddRule(_skjema, "4655.1.1.13.1", "1.2", "Kommunenummeret '{0}' for eiendom/byggested finnes ikke i kodelisten. Du kan sjekke riktig kommunenummer på https://register.geonorge.no/sosi-kodelister/inndelinger/inndelingsbase/kommunenummer", Helpers.GetFullClassPath(() => form.eiendomByggested[0].eiendomsidentifikasjon.kommunenummer), "ERROR", null);
            _validationResult.AddRule(_skjema, "4655.1.1.13.2", "1.2", "Kommunenummeret '{0}' for eiendom/byggested har ugyldig status ({1}). Du kan sjekke status på https://register.geonorge.no/sosi-kodelister/inndelinger/inndelingsbase/kommunenummer", Helpers.GetFullClassPath(() => form.eiendomByggested[0].eiendomsidentifikasjon.kommunenummer), "ERROR", null);
            _validationResult.AddRule(_skjema, "4655.3.25.1", "1.3", "Gårdsnummer bør fylles ut for eiendom/byggested.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].eiendomsidentifikasjon.gaardsnummer), "WARNING", null);
            _validationResult.AddRule(_skjema, "4655.3.25.2", "1.4", "Bruksnummer bør fylles ut for eiendom/byggested.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].eiendomsidentifikasjon.bruksnummer), "WARNING", null);
            _validationResult.AddRule(_skjema, "4655.3.25.3", "1.3", "Gårdsnummer '{0}' for eiendom/byggested bør være '0' eller større.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].eiendomsidentifikasjon.gaardsnummer), "WARNING", null);
            _validationResult.AddRule(_skjema, "4655.3.25.4", "1.4", "Bruksnummer '{0}' for eiendom/byggested bør være '0' eller større.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].eiendomsidentifikasjon.bruksnummer), "WARNING", null);
            _validationResult.AddRule(_skjema, "4655.3.26", "1.6", "Du har oppgitt følgende bygningsnummer for eiendom/byggested: ‘{0}'. Bygningsnummeret bør være et tall.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].bygningsnummer), "WARNING", null);
            //_validationResult.AddRule(_skjema, "4655.3.26.2", "1.6", "Bygningsnummer for eiendom/byggested bør være større enn '0'.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].bygningsnummer), "WARNING", null);

            //if (_matrikkelFlag)
            //{
            _validationResult.AddRule(_skjema, "4655.3.25.5", "1.72", "Når eiendomsidentifikasjon [{0}-{1}/{2}/{3}/{4}] er oppgitt for eiendom/byggested, bør den være gyldig i matrikkelen. Du kan sjekke riktig informasjon på https://seeiendom.no", Helpers.GetFullClassPath(() => form.eiendomByggested[0].eiendomsidentifikasjon), "WARNING", null);
            _validationResult.AddRule(_skjema, "4655.3.25", "1.72", "Eiendomsidentifikasjon [{0}-{1}/{2}/{3}/{4}] for eiendom/byggested ble ikke validert mot matrikkelen. Identifikasjonen kan være riktig, men en teknisk feil gjør at vi ikke kan bekrefte det.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].eiendomsidentifikasjon), "WARNING", null);
            _validationResult.AddRule(_skjema, "4655.3.26.1", "1.6", "Når bygningsnummer [{0}] er oppgitt for eiendom/byggested, bør det være gyldig i matrikkelen på aktuelt matrikkelnummer. Du kan sjekke riktig bygningsnummer på https://seeiendom.no", Helpers.GetFullClassPath(() => form.eiendomByggested[0].bygningsnummer), "WARNING", null);
            _validationResult.AddRule(_skjema, "4655.3.27", "1.8", "Gateadressen for eiendom/byggested ble ikke validert mot matrikkelen. Gateadressen kan være riktig, men en teknisk feil gjør at vi ikke kan bekrefte det. Du kan sjekke adresse på https://seeiendom.no", Helpers.GetFullClassPath(() => form.eiendomByggested[0].adresse.gatenavn), "WARNING", null);
            _validationResult.AddRule(_skjema, "4655.3.27.1", "1.8", "Du har oppgitt feil gateadresse for eiendom/byggested. Du kan sjekke riktig adresse på https://seeiendom.no", Helpers.GetFullClassPath(() => form.eiendomByggested[0].adresse.gatenavn), "WARNING", null);
            //}

            _validationResult.AddRule(_skjema, "4655.3.12", "1.8", "Postadresse for eiendom/byggested bør fylles ut.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].adresse), "WARNING", null);
            _validationResult.AddRule(_skjema, "4655.3.12.1", "1.8", "Adresselinje 1 for eiendom/byggested bør fylles ut.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].adresse.adresselinje1), "WARNING", null);
            _validationResult.AddRule(_skjema, "4655.3.28", "1.8", "Du bør oppgi gatenavn, husnummer og eventuell bokstav for eiendom/byggested slik at adressen kan valideres mot matrikkelen. Du kan sjekke riktig adresse på https://seeiendom.no", Helpers.GetFullClassPath(() => form.eiendomByggested[0].adresse.gatenavn), "WARNING", null); //
            _validationResult.AddRule(_skjema, "4655.3.29", null, "Navnet på kommunen bør fylles ut for eiendom/byggested.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].kommunenavn), "WARNING", null);
            _validationResult.AddRule(_skjema, "4655.3.30", null, "Når bruksenhetsnummer/bolignummer er fylt ut, må det følge riktig format (for eksempel H0101). Se https://www.kartverket.no/eiendom/adressering/bruksenhetsnummer/.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].bolignummer), "WARNING", null);

            //MinstEnNaboeier
            _validationResult.AddRule(_skjema, "4655.1.1.1", null, "Du må oppgi minst én nabo.", Helpers.GetFullClassPath(() => form.naboeier[0]), "ERROR", null);

            //ValiderNaboeier
            _validationResult.AddRule(_skjema, "4655.1.1.1.1", null, "Navnet til nabo/gjenboer må fylles ut.", Helpers.GetFullClassPath(() => form.naboeier[0].navn), "ERROR", null);
            _validationResult.AddRule(_skjema, "4655.1.2.2", null, "'{0}' er en ugyldig kodeverdi for partstypen til nabo/gjenboer. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/byggesoknad/partstype.", Helpers.GetFullClassPath(() => form.naboeier[0].partstype.kodeverdi), "ERROR", null);
            _validationResult.AddRule(_skjema, "4655.1.1.16", null, "Partstype må fylles ut for nabo/gjenboer", Helpers.GetFullClassPath(() => form.naboeier[0].partstype), "ERROR", null);
            //foedselnumer
            _validationResult.AddRule(_skjema, "4655.3.18", null, "Fødselsnummeret til nabo/gjenboer skal være kryptert.", Helpers.GetFullClassPath(() => _form.naboeier[0].foedselsnummer), "ERROR", null);
            _validationResult.AddRule(_skjema, "4655.1.4", null, "Fødselsnummer må fylles ut når nabo er en privatperson.", Helpers.GetFullClassPath(() => form.naboeier[0].foedselsnummer), "ERROR", Helpers.GetFullClassPath(() => form.naboeier[0].partstype.kodeverdi));
            _validationResult.AddRule(_skjema, "4655.1.4.1", null, "Fødselsnummeret til nabo er ikke gyldig.", Helpers.GetFullClassPath(() => form.naboeier[0].foedselsnummer), "ERROR", Helpers.GetFullClassPath(() => form.naboeier[0].partstype.kodeverdi));
            _validationResult.AddRule(_skjema, "4655.1.4.2", null, "Fødselsnummeret til nabo har ikke gyldig kontrollsiffer.", Helpers.GetFullClassPath(() => form.naboeier[0].foedselsnummer), "ERROR", Helpers.GetFullClassPath(() => form.naboeier[0].partstype.kodeverdi));
            _validationResult.AddRule(_skjema, "4655.1.4.3", null, "Fødselsnummeret til nabo kan ikke dekrypteres.", Helpers.GetFullClassPath(() => form.naboeier[0].foedselsnummer), "ERROR", Helpers.GetFullClassPath(() => form.naboeier[0].partstype.kodeverdi));

            //organisasjonsnummer
            _validationResult.AddRule(_skjema, "4655.1.4.4", null, "Organisasjonsnummeret for nabo/gjenboer må fylles ut.", Helpers.GetFullClassPath(() => form.naboeier[0].organisasjonsnummer), "ERROR", Helpers.GetFullClassPath(() => form.naboeier[0].partstype.kodeverdi));
            _validationResult.AddRule(_skjema, "4655.1.4.5", null, "Organisasjonsnummeret ('{0}') for nabo/gjenboer har ikke gyldig kontrollsiffer.", Helpers.GetFullClassPath(() => form.naboeier[0].organisasjonsnummer), "ERROR", Helpers.GetFullClassPath(() => form.naboeier[0].partstype.kodeverdi));
            _validationResult.AddRule(_skjema, "4655.1.4.6", null, "Organisasjonsnummeret ('{0}') til nabo/gjenboer er ikke gyldig.", Helpers.GetFullClassPath(() => form.naboeier[0].organisasjonsnummer), "ERROR", Helpers.GetFullClassPath(() => form.naboeier[0].partstype.kodeverdi));
            //Adresse
            _validationResult.AddRule(_skjema, "4655.1.1.3", null, "Postadressen til nabo/gjenboer bør fylles ut.", Helpers.GetFullClassPath(() => form.naboeier[0].adresse), "WARNING", null);
            _validationResult.AddRule(_skjema, "4655.1.1.3.1", null, "Adresselinje 1 bør fylles ut for nabo/gjenboer.", Helpers.GetFullClassPath(() => form.naboeier[0].adresse.adresselinje1), "WARNING", null);
            _validationResult.AddRule(_skjema, "4655.3.24", null, "Postnummeret til nabo/gjenboer bør angis.", Helpers.GetFullClassPath(() => form.naboeier[0].adresse.postnr), "WARNING", null);
            _validationResult.AddRule(_skjema, "4655.3.24.1", null, "Postnummeret '{0}' til nabo/gjenboer har ikke gyldig kontrollsiffer.", Helpers.GetFullClassPath(() => form.naboeier[0].adresse.postnr), "WARNING", null);
            _validationResult.AddRule(_skjema, "4655.3.24.2", null, "Postnummeret '{0}' til nabo/gjenboer er ugyldig. Du kan sjekke riktig postnummer på http://adressesok.bring.no/.", Helpers.GetFullClassPath(() => form.naboeier[0].adresse.postnr), "WARNING", null);
            _validationResult.AddRule(_skjema, "4655.3.24.3", null, "Postnummeret '{0}' til nabo/gjenboer stemmer ikke overens med poststedet '{1}'. Postnummeret er fra '{2}'. Du kan sjekke riktig postnummer/poststed på http://adressesok.bring.no/.", Helpers.GetFullClassPath(() => form.naboeier[0].adresse.postnr), "WARNING", null);
            _validationResult.AddRule(_skjema, "4655.3.24.4", null, "Postnummeret '{0}' til tiltakshaver ble ikke validert. Postnummeret kan være riktig, men en teknisk feil gjør at vi ikke kan bekrefte det.", Helpers.GetFullClassPath(() => form.naboeier[0].adresse.postnr), "WARNING", null);
            _validationResult.AddRule(_skjema, "4655.3.31", null, "Landkoden for nabo/gjenboers adresse er ugyldig.", Helpers.GetFullClassPath(() => _form.naboeier[0].adresse.landkode), "WARNING", null);
            _validationResult.AddRule(_skjema, "4655.1.2.3", null, "Kommunenummeret '{0}' for naboeiendommen finnes ikke i kodelisten. Du kan sjekke riktig kommunenummer på https://register.geonorge.no/sosi-kodelister/inndelinger/inndelingsbase/kommunenummer.", Helpers.GetFullClassPath(() => _form.naboeier[0].gjelderNaboeiendom.eiendomsidentifikasjon.kommunenummer), "ERROR", null);
            _validationResult.AddRule(_skjema, "4655.1.2.4", null, "Kommunenummeret '{0}' for naboeiendom har ugyldig status ({1}). Du kan sjekke status på https://register.geonorge.no/sosi-kodelister/inndelinger/inndelingsbase/kommunenummer.", Helpers.GetFullClassPath(() => _form.naboeier[0].gjelderNaboeiendom.eiendomsidentifikasjon.kommunenummer), "ERROR", null);
            _validationResult.AddRule(_skjema, "4655.3.13", null, "Postadressen for naboeiendommen bør fylles ut.", Helpers.GetFullClassPath(() => _form.naboeier[0].gjelderNaboeiendom.adresse), "WARNING", null);
            _validationResult.AddRule(_skjema, "4655.3.13.1", null, "Adresselinje 1 for naboeiendommen bør fylles ut.", Helpers.GetFullClassPath(() => _form.naboeier[0].gjelderNaboeiendom.adresse.adresselinje1), "WARNING", null);
            _validationResult.AddRule(_skjema, "4655.3.19", null, "Postnummeret til nabo/gjenboer bør angis.", Helpers.GetFullClassPath(() => form.naboeier[0].gjelderNaboeiendom.adresse.postnr), "WARNING", null);
            _validationResult.AddRule(_skjema, "4655.3.19.1", null, "Postnummeret '{0}' til nabo/gjenboer har ikke gyldig kontrollsiffer.", Helpers.GetFullClassPath(() => form.naboeier[0].gjelderNaboeiendom.adresse.postnr), "WARNING", null);
            _validationResult.AddRule(_skjema, "4655.3.19.2", null, "Postnummeret '{0}' til nabo/gjenboer er ugyldig. Du kan sjekke riktig postnummer på http://adressesok.bring.no/.", Helpers.GetFullClassPath(() => form.naboeier[0].gjelderNaboeiendom.adresse.postnr), "WARNING", null);
            _validationResult.AddRule(_skjema, "4655.3.19.3", null, "Postnummeret '{0}' til nabo/gjenboer stemmer ikke overens med poststedet '{1}'. Postnummeret er fra '{2}'. Du kan sjekke riktig postnummer/poststed på http://adressesok.bring.no/.", Helpers.GetFullClassPath(() => form.naboeier[0].gjelderNaboeiendom.adresse.postnr), "WARNING", null);
            _validationResult.AddRule(_skjema, "4655.3.19.4", null, "Postnummeret '{0}' til tiltakshaver ble ikke validert. Postnummeret kan være riktig, men en teknisk feil gjør at vi ikke kan bekrefte det.", Helpers.GetFullClassPath(() => form.naboeier[0].gjelderNaboeiendom.adresse.postnr), "WARNING", null);
            _validationResult.AddRule(_skjema, "4655.3.20", null, "Landkoden for naboeiendommens adresse er ugyldig", Helpers.GetFullClassPath(() => _form.naboeier[0].gjelderNaboeiendom.adresse.landkode), "WARNING", null);
            _validationResult.AddRule(_skjema, "4655.1.1.4", null, "Du må oppgi en eiendom for hver nabo/gjenboer.", Helpers.GetFullClassPath(() => _form.naboeier[0].gjelderNaboeiendom), "ERROR", null);
            _validationResult.AddRule(_skjema, "4655.1.1.5", null, "Denne naboen kan ikke varsles digitalt. Ta kontakt med kommunen for å få hjelp til å varsle naboen.", Helpers.GetFullClassPath(() => _form.naboeier[0]), "ERROR", null);

            //SoekerValidation
            _validationResult.AddRule(_skjema, "4655.1.1.6", null, "Du må fylle ut 'adresse', 'partstype' og 'navn' for ansvarlig søker.", Helpers.GetFullClassPath(() => _form.ansvarligSoeker), "ERROR", null);
            _validationResult.AddRule(_skjema, "4655.1.2.5", null, "'{0}' er en ugyldig kodeverdi for partstypen til ansvarlig søker. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/byggesoknad/partstype.", Helpers.GetFullClassPath(() => _form.ansvarligSoeker.partstype.kodeverdi), "ERROR", null);
            //**new
            _validationResult.AddRule(_skjema, "4655.1.5", null, "Fødselsnummer må fylles ut når ansvarlig søker er en privatperson.", Helpers.GetFullClassPath(() => _form.ansvarligSoeker.partstype.kodeverdi), "ERROR", null);
            _validationResult.AddRule(_skjema, "4655.1.5.1", null, "Fødselsnummeret til ansvarlig søker er ikke gyldig.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.foedselsnummer), "ERROR", null);
            _validationResult.AddRule(_skjema, "4655.1.5.2", null, "Fødselsnummeret til ansvarlig søker har ikke gyldig kontrollsiffer.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.foedselsnummer), "ERROR", null);
            _validationResult.AddRule(_skjema, "4655.1.5.3", null, "Fødselsnummeret til ansvarlig søker kan ikke dekrypteres.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.foedselsnummer), "ERROR", null);
            _validationResult.AddRule(_skjema, "4655.1.5.4", null, "Organisasjonsnummeret til ansvarlig søker må fylles ut.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.organisasjonsnummer), "ERROR", null);
            _validationResult.AddRule(_skjema, "4655.1.5.4.1", null, "Organisasjonsnummeret ('{0}') til ansvarlig søker har ikke gyldig kontrollsiffer.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.organisasjonsnummer), "ERROR", null);
            _validationResult.AddRule(_skjema, "4655.1.5.4.2", null, "Organisasjonsnummeret ('{0}') til ansvarlig søker er ikke gyldig.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.organisasjonsnummer), "ERROR", null);
            //**
            _validationResult.AddRule(_skjema, "4655.1.1.7", null, "Adresselinje 1 bør fylles ut for ansvarlig søker.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.adresse.adresselinje1), "WARNING", null);
            _validationResult.AddRule(_skjema, "4655.3.22", null, "Postnummeret til ansvarlig søker bør angis.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.adresse.postnr), "WARNING", null);
            _validationResult.AddRule(_skjema, "4655.3.22.1", null, "Postnummeret '{0}' til ansvarlig søker har ikke gyldig kontrollsiffer.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.adresse.postnr), "WARNING", null);
            _validationResult.AddRule(_skjema, "4655.3.22.2", null, "Postnummeret '{0}' til ansvarlig søker er ugyldig. Du kan sjekke riktig postnummer på http://adressesok.bring.no/.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.adresse.postnr), "WARNING", null);
            _validationResult.AddRule(_skjema, "4655.3.22.3", null, "Postnummeret '{0}' til ansvarlig søker stemmer ikke overens med poststedet '{1}'. Riktig postnummer er '{2}'. Du kan sjekke riktig poststed på http://adressesok.bring.no/.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.adresse.postnr), "WARNING", null);
            _validationResult.AddRule(_skjema, "4655.3.22.4", null, "Postnummeret '{0}' til ansvarlig søker ble ikke validert. Postnummeret kan være riktig, men en teknisk feil gjør at vi ikke kan bekrefte det.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.adresse.postnr), "WARNING", null);
            _validationResult.AddRule(_skjema, "4655.3.23", null, "Landkoden for ansvarlig søkers adresse er ugyldig.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.adresse.landkode), "ERROR", "");
            _validationResult.AddRule(_skjema, "4655.3.15", null, "Telefonnummeret eller mobilnummeret til ansvarlig søker bør fylles ut.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.mobilnummer), "WARNING", Helpers.GetFullClassPath(() => form.ansvarligSoeker.telefonnummer));
            _validationResult.AddRule(_skjema, "4655.3.14", null, "E-postadressen til ansvarlig søker bør fylles ut.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.epost), "WARNING", null);

            //tiltakshaver.KontaktPersonValidering
            _validationResult.AddRule(_skjema, "4655.3.21", null, "Kontaktpersonen til tiltakshaver bør fylles ut.", Helpers.GetFullClassPath(() => form.tiltakshaver.kontaktperson.navn), "WARNING", null);
            //TiltakshaverHarLovligKodeverdi
            _validationResult.AddRule(_skjema, "4655.1.1.8", null, "Partstype må fylles ut for tiltakshaver.", Helpers.GetFullClassPath(() => form.tiltakshaver.partstype), "ERROR", null);
            _validationResult.AddRule(_skjema, "4655.1.2.6", null, "'{0}' er en ugyldig kodeverdi for partstypen til tiltakshaver. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/byggesoknad/partstype.", Helpers.GetFullClassPath(() => form.tiltakshaver.partstype.kodeverdi), "ERROR", null);
            _validationResult.AddRule(_skjema, "4655.1.1.9", null, "Navnet til tiltakshaver bør fylles ut.", Helpers.GetFullClassPath(() => form.tiltakshaver.navn), "WARNING", null);

            //MinstEtTiltak
            _validationResult.AddRule(_skjema, "4655.1.1.10", null, "Minst ett tiltak må beskrives.", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak), "ERROR", null);
            _validationResult.AddRule(_skjema, "4655.1.1.11", null, "Minst én tiltakstype må være registrert.", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak[0].type), "ERROR", null);
            _validationResult.AddRule(_skjema, "4655.1.2.7", null, "'{0}' er en ugyldig kodeverdi for tiltakstype. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/kodelister/byggesoknad/tiltaktype.", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak[0].type[0].kodeverdi), "ERROR", null);
            _validationResult.AddRule(_skjema, "4655.1.1.12", null, "Du må oppgi hvilket tiltak nabovarselet gjelder.", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak[0].bruk.tiltaksformaal), "ERROR", null);
            _validationResult.AddRule(_skjema, "4655.1.2.8", null, "'{0}' er en ugyldig kodeverdi for tiltaksformål. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/kodelister/byggesoknad/tiltaksformal.", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak[0].bruk.tiltaksformaal[0].kodeverdi), "ERROR", null);

            //GjeldendePlanHarLovligVerdi
            _validationResult.AddRule(_skjema, "4655.1.2.9", null, "'{0}' er en ugyldig kodeverdi for plantype. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/kodelister/byggesoknad/plantype.", Helpers.GetFullClassPath(() => form.gjeldendePlan.plantype.kodeverdi), "ERROR", null);
            _validationResult.AddRule(_skjema, "4655.1.1.14", null, "Når plantype er valgt, må kodebeskrivelse angis.", Helpers.GetFullClassPath(() => form.gjeldendePlan.plantype.kodebeskrivelse), "ERROR", null);

            //DispensasjonstypeErLovlig
            //_validationResult.AddRule(_skjema, "4655.3.10.1", null, "Det er valgt at det skal søkes om dispensasjon. Da må du velge en dispensasjonstype. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/byggesoknad/dispensasjonstype.", Helpers.GetFullClassPath(() => form.dispensasjon[0].dispensasjonstype), "ERROR", Helpers.GetFullClassPath(() => form.dispensasjon));
            _validationResult.AddRule(_skjema, "4655.1.2.10", null, "'{0}' er en ugyldig kodeverdi for dispensasjonstype. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/byggesoknad/dispensasjonstype.", Helpers.GetFullClassPath(() => form.dispensasjon[0].dispensasjonstype.kodeverdi), "ERROR", null);
            _validationResult.AddRule(_skjema, "4655.3.10", null, "Det er valgt at det skal søkes om dispensasjon. Da må du legge inn beskrivelse og begrunnelse for dispensasjonen i tekstfeltene, eller i vedlegget 'Dispensasjonssøknad'.", Helpers.GetFullClassPath(() => form.dispensasjon[0].beskrivelse), "ERROR", "/Vedlegg");
            _validationResult.AddRule(_skjema, "4655.3.11", null, "Det er valgt at det skal søkes om dispensasjon. Da må du også skrive en begrunnelse for dispensasjonen.", Helpers.GetFullClassPath(() => form.dispensasjon[0].begrunnelse), "ERROR", null);
            //TegningValidering
            _validationResult.AddRule(_skjema, "4655.2.32", "1.80", "Minst én tegning med en av vedleggstypene ‘TegningEksisterendeSnitt’, ‘TegningNyttSnitt’, ‘TegningEksisterendeFasade’ eller ‘TegningNyFasade’ må være vedlagt nabovarselet.", "/Vedlegg", "ERROR", null);
            _validationResult.AddRule(_skjema, "4655.2.8", "1.80", "Snittegning må være vedlagt søknaden for valgte tiltakstyper, jfr. nasjonal sjekkliste punkt 1.80. Vi anbefaler at vedlegget ‘TegningNyttSnitt’ legges ved. Gjelder tiltaket eksisterende bygning, skal også ‘TegningEksisterendeSnitt’ være vedlagt.", "/Vedlegg", "WARNING", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak[0].type[0].kodeverdi));
            _validationResult.AddRule(_skjema, "4655.2.9", "1.81", "Fasadetegning må være vedlagt søknaden for valgte tiltakstyper, jfr. nasjonal sjekkliste punkt 1.81. Vi anbefaler at vedlegget ‘TegningNyFasade’ legges ved. Gjelder tiltaket eksisterende bygning, skal også ‘TegningEksisterendeFasade’ være vedlagt.", "/Vedlegg", "WARNING", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak[0].type[0].kodeverdi));

            //SituasjonsplanValidering
            _validationResult.AddRule(_skjema, "4655.2.7", "1.73", "Vedlegg ‘Situasjonsplan’ mangler for valgte tiltakstyper, jfr. nasjonal sjekkliste punkt 1.73.", "/Vedlegg", "ERROR", null);

            //MinstEnEiendom
            _validationResult.AddRule(_skjema, "4655.1.1.15", "1.73", "Du må oppgi hvilken eiendom/hvilke eiendommer nabovarselet gjelder.", Helpers.GetFullClassPath(() => form.eiendomByggested[0]), "ERROR", null);
        }

        public ValidationResult Validate(NabovarselType form, List<string> FormSetElements)
        {
            _form = form;
            _formSetElements = FormSetElements;
            _logger.Debug("Starts validating form {dataFormatId} - {dataFormatVersion}", form.dataFormatId, form.dataFormatVersion);
            _tiltakstyperISoeknad = new List<string>();

            MinstEtTiltak(_form);
            _validationResult.tiltakstyperISoeknad = _tiltakstyperISoeknad;

            //var parallelOptions = new ParallelOptions() { MaxDegreeOfParallelism = 10 };
            Parallel.Invoke(
                () => { FraSluttbrukersystemErUtfylt(_form); },
                //() => { EiendomByggestedValidering(_form); },
                () => { MinstEnNaboeier(_form); },
                () => { SoekerValidation(_form); },
                () => { TiltakshaverHarLovligKodeverdi(_form); },
                () => { GjeldendePlanHarLovligVerdi(_form); },
                () => { DispensasjonstypeErLovlig(_form, _formSetElements); },
                () => { SituasjonsplanValidering(_form, _formSetElements); },
                () => { TegningValidering(FormSetElements); },
                () => { KontaktPersonValidering(_form); }
            );

            ParallelEiendomByggestedValidering(_form);
            ParallelNaboeierValidering(_form);

            return _validationResult;
        }

        private NabovarselType _form = null;
        private List<string> _formSetElements = new List<string>();

        internal ValidationResult GetResult() => _validationResult;

        internal void FraSluttbrukersystemErUtfylt(NabovarselType form)
        {
            if (String.IsNullOrEmpty(form.fraSluttbrukersystem))
            {
                _validationResult.AddMessage("4655.3.17", null);
            }
        }

        internal void EiendomByggestedValidering(EiendomType eiendom)
        {
            if (eiendom.eiendomsidentifikasjon != null)
            {
                var kommunenummer = eiendom.eiendomsidentifikasjon.kommunenummer;
                var kommunenummerValideringStatus = MunicipalityValidator.Validate_kommunenummerStatus(kommunenummer);
                if (kommunenummerValideringStatus.Status != MunicipalityValidation.Ok)
                {
                    //_validationResult.AddError($"Kommunenummer må fylles ut for eiendom / byggested.", "4655.1.1", "/Nabovarsel/eiendomByggested/eiendom/eiendomsidentifikasjon/kommunenummer");

                    switch (kommunenummerValideringStatus.Status)
                    {
                        case MunicipalityValidation.Empty:
                            _validationResult.AddMessage("4655.1.1.13", null);
                            break;

                        case MunicipalityValidation.Invalid:
                            _validationResult.AddMessage("4655.1.1.13.1", new[] { kommunenummer });
                            break;

                        case MunicipalityValidation.Expired:
                            _validationResult.AddMessage("4655.1.1.13.2", new[] { kommunenummer, kommunenummerValideringStatus.Status.ToString() });
                            break;
                    }
                }
                else
                {
                    var gaardsnummer = eiendom.eiendomsidentifikasjon.gaardsnummer;
                    var bruksnummer = eiendom.eiendomsidentifikasjon.bruksnummer;

                    if (string.IsNullOrEmpty(gaardsnummer) || string.IsNullOrEmpty(bruksnummer))
                    {
                        if (string.IsNullOrEmpty(gaardsnummer))
                            _validationResult.AddMessage("4655.3.25.1", null);

                        if (string.IsNullOrEmpty(bruksnummer))
                            _validationResult.AddMessage("4655.3.25.2", null);
                    }
                    else
                    {
                        int gaardsnumerInt = 0;
                        int bruksnummerInt = 0;
                        if (int.TryParse(gaardsnummer, out gaardsnumerInt) && int.TryParse(bruksnummer, out bruksnummerInt))
                        {
                            int festenummerInt = 0;
                            int seksjonsnummerInt = 0;
                            int.TryParse(eiendom.eiendomsidentifikasjon.festenummer, out festenummerInt);
                            int.TryParse(eiendom.eiendomsidentifikasjon.seksjonsnummer, out seksjonsnummerInt);

                            if (gaardsnumerInt < 0 || bruksnummerInt < 0)
                            {
                                if (gaardsnumerInt < 0)
                                    _validationResult.AddMessage("4655.3.25.3", new[] { eiendom.eiendomsidentifikasjon.gaardsnummer });

                                if (bruksnummerInt < 0)
                                    _validationResult.AddMessage("4655.3.25.4", new[] { eiendom.eiendomsidentifikasjon.bruksnummer });
                            }
                            else
                            {
                                //## MATRIKKEL
                                var matrikkelnrExist = _matrikkelService.MatrikkelnrExist(kommunenummer, gaardsnumerInt, bruksnummerInt, festenummerInt, seksjonsnummerInt);
                                if (matrikkelnrExist.HasValue)
                                {
                                    if (!matrikkelnrExist.Value)
                                    {
                                        _validationResult.AddMessage("4655.3.25.5", new[]
                                        {
                                                    kommunenummer,
                                                    eiendom.eiendomsidentifikasjon.gaardsnummer,
                                                    eiendom.eiendomsidentifikasjon.bruksnummer,
                                                    eiendom.eiendomsidentifikasjon.festenummer,
                                                    eiendom.eiendomsidentifikasjon.seksjonsnummer,
                                                });
                                    }
                                }
                                else
                                {
                                    _validationResult.AddMessage("4655.3.25", new[]
                                    {
                                                kommunenummer,
                                                eiendom.eiendomsidentifikasjon.gaardsnummer,
                                                eiendom.eiendomsidentifikasjon.bruksnummer,
                                                eiendom.eiendomsidentifikasjon.festenummer,
                                                eiendom.eiendomsidentifikasjon.seksjonsnummer,
                                            });
                                }

                                if (!String.IsNullOrEmpty(eiendom.bygningsnummer))
                                {
                                    long bygningsnrLong = 0;
                                    if (!long.TryParse(eiendom.bygningsnummer, out bygningsnrLong))
                                    {
                                        _validationResult.AddMessage("4655.3.26", new[] { eiendom.bygningsnummer });
                                    }
                                    else
                                    {
                                        if (bygningsnrLong <= 0)
                                        {
                                            //_validationResult.AddMessage("4655.3.26.2", new[] { eiendom.eiendomsidentifikasjon.gaardsnummer, eiendom.eiendomsidentifikasjon.bruksnummer });
                                        }
                                        else
                                        {
                                            ////## MATRIKKEL
                                            var isBygningValid = _matrikkelService.IsBygningOnMatrikkelnr(bygningsnrLong, kommunenummer, gaardsnumerInt, bruksnummerInt, festenummerInt, seksjonsnummerInt);
                                            if (!isBygningValid.GetValueOrDefault())
                                            {
                                                _validationResult.AddMessage("4655.3.26.1", new[] { eiendom.bygningsnummer });
                                            }
                                        }
                                    }
                                }

                                if (Helpers.ObjectIsNullOrEmpty(eiendom.adresse))
                                {
                                    _validationResult.AddMessage("4655.3.12", null);
                                }
                                else
                                {
                                    if (string.IsNullOrEmpty(eiendom.adresse?.adresselinje1))
                                    {
                                        _validationResult.AddMessage("4655.3.12.1", null);
                                    }
                                    else
                                    {
                                        if (String.IsNullOrEmpty(eiendom.adresse.gatenavn) || String.IsNullOrEmpty(eiendom.adresse.husnr))
                                        {
                                            _validationResult.AddMessage("4655.3.28", null);
                                        }
                                        else
                                        {
                                            //## MATRIKKEL
                                            var isAdresseValid = _matrikkelService.IsVegadresseOnMatrikkelnr(eiendom.adresse.gatenavn, eiendom.adresse.husnr, eiendom.adresse.bokstav, kommunenummer, gaardsnumerInt, bruksnummerInt, festenummerInt, seksjonsnummerInt);
                                            if (isAdresseValid == null)
                                            {
                                                _validationResult.AddMessage("4655.3.27", null);
                                            }
                                            else if (!isAdresseValid.GetValueOrDefault())
                                            {
                                                _validationResult.AddMessage("4655.3.27.1", null);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if (String.IsNullOrEmpty(eiendom.kommunenavn))
            {
                _validationResult.AddMessage("4655.3.29", null);
            }

            if (!String.IsNullOrEmpty(eiendom.bolignummer))
            {
                if (GeneralValidations.Validate_bruksenhetsnummer(eiendom.bolignummer).Status == ValidationStatus.Fail)
                {
                    _validationResult.AddMessage("4655.3.30", null);
                }
            }
        }

        internal void ParallelEiendomByggestedValidering(NabovarselType form)
        {
            using (var logger = new PerfTimerLogger())
            {
                if (MinstEnEiendom(form))
                {
                    Parallel.ForEach(form.eiendomByggested, (eiendomByggested) =>
                    {
                        EiendomByggestedValidering(eiendomByggested);
                    });
                }
            }
        }

        internal void EiendomByggestedValidering(NabovarselType form)
        {
            using (var logger = new PerfTimerLogger())
            {
                if (MinstEnEiendom(form))
                {
                    foreach (var eiendom in form.eiendomByggested)
                    {
                        EiendomByggestedValidering(eiendom);
                    }
                }
            }
        }

        internal void MinstEnNaboeier(NabovarselType form)
        {
            using (var logger = new PerfTimerLogger())
            {
                if (Helpers.ObjectIsNullOrEmpty(form.naboeier))
                {
                    _validationResult.AddMessage("4655.1.1.1", null);
                }
            }
        }

        internal void ParallelNaboeierValidering(NabovarselType form)
        {
            _logger.Debug("ParallelNaboeierValidering naboeier count: {naboEiere}", form.naboeier.Count());

            Parallel.ForEach(form.naboeier, (naboEier) =>
            {
                ValiderNaboeier(naboEier);
            });
        }

        private void ValiderNaboeier(NaboGjenboerType naboEier)
        {
            using (var logger = new PerfTimerLogger())
            {
                if (string.IsNullOrEmpty(naboEier.navn))
                {
                    _validationResult.AddMessage("4655.1.1.1.1", null);
                    return;
                }
                if (!naboEier.navn.ToLower().Contains("beskyttet"))
                {
                    if (!Helpers.ObjectIsNullOrEmpty(naboEier.partstype))
                    {
                        if (!_codeListService.IsCodelistValid("Partstype", naboEier.partstype.kodeverdi))
                        {
                            _validationResult.AddMessage("4655.1.2.2", new string[] { naboEier.partstype.kodeverdi });
                        }
                        else
                        {
                            if (naboEier.partstype.kodeverdi.Equals("Privatperson", StringComparison.OrdinalIgnoreCase))
                            {
                                var foedselsnummerValidation = GeneralValidations.Validate_foedselsnummer(naboEier.foedselsnummer);

                                if (foedselsnummerValidation == GeneralValidations.FoedselnumerValidation.Empty)
                                {
                                    _validationResult.AddMessage("4655.1.4", null);
                                }
                                else if (naboEier.foedselsnummer.Length < 12)
                                {
                                    _validationResult.AddMessage("4655.3.18", null);
                                }
                                else
                                {
                                    switch (foedselsnummerValidation)
                                    {
                                        case GeneralValidations.FoedselnumerValidation.Invalid:
                                            _validationResult.AddMessage("4655.1.4.1", null);
                                            break;

                                        case GeneralValidations.FoedselnumerValidation.InvalidDigitsControl:
                                            _validationResult.AddMessage("4655.1.4.2", null);
                                            break;

                                        case GeneralValidations.FoedselnumerValidation.InvalidEncryption:
                                            _validationResult.AddMessage("4655.1.4.3", null);
                                            break;
                                    }
                                }
                            }
                            else
                            {
                                var organisasjonsnummerValidation = GeneralValidations.Validate_OrgnummerEnum(naboEier.organisasjonsnummer);
                                switch (organisasjonsnummerValidation)
                                {
                                    case GeneralValidations.OrganisasjonsnummerValidation.Empty:
                                        _validationResult.AddMessage("4655.1.4.4", null);
                                        break;

                                    case GeneralValidations.OrganisasjonsnummerValidation.Invalid:
                                        _validationResult.AddMessage("4655.1.4.5", new[] { naboEier.organisasjonsnummer });
                                        break;

                                    case GeneralValidations.OrganisasjonsnummerValidation.InvalidDigitsControl:
                                        _validationResult.AddMessage("4655.1.4.6", new[] { naboEier.organisasjonsnummer });
                                        break;
                                }
                            }
                        }
                    }
                    else
                    {
                        _validationResult.AddMessage("4655.1.1.16", null);
                    }

                    if (!Helpers.ObjectIsNullOrEmpty(naboEier.adresse))
                    {
                        if (String.IsNullOrEmpty(naboEier.adresse.adresselinje1))
                        {
                            _validationResult.AddMessage("4655.1.1.3.1", null);
                        }
                        //In another validation if "addressline1" is null we do not validate the rest
                        if (!CountryCodeHandler.IsCountryNorway(naboEier.adresse.landkode))
                        {
                            if (!CountryCodeHandler.VerifyCountryCode(naboEier.adresse.landkode))
                            {
                                _validationResult.AddMessage("4655.3.31", null);
                            }
                        }
                        else
                        {
                            var postNr = naboEier.adresse.postnr;
                            var landkode = naboEier.adresse.landkode;

                            if (string.IsNullOrEmpty(postNr))
                            {
                                _validationResult.AddMessage("4655.3.24", null);
                            }
                            else
                            {
                                if (!GeneralValidations.postNumberMatch(postNr).Success)
                                {
                                    _validationResult.AddMessage("4655.3.24.1", new[] { postNr });
                                }
                                else
                                {
                                    var postnrValidation = _postalCodeProvider.ValidatePostnr(postNr, landkode);
                                    if (!Helpers.ObjectIsNullOrEmpty(postnrValidation))
                                    {
                                        if (!postnrValidation.Valid)
                                        {
                                            _validationResult.AddMessage("4655.3.24.2", null);
                                        }
                                        else
                                        {
                                            if (!postnrValidation.Result.Equals(naboEier.adresse.poststed, StringComparison.CurrentCultureIgnoreCase))
                                            {
                                                _validationResult.AddMessage("4655.3.24.3", new[] { postNr, naboEier.adresse.poststed, postnrValidation.Result });
                                            }
                                        }
                                    }
                                    else
                                    {
                                        _validationResult.AddMessage("4655.3.24.4", null);
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        _validationResult.AddMessage("4655.1.1.3", null);
                    }

                    if (!Helpers.ObjectIsNullOrEmpty(naboEier.gjelderNaboeiendom))
                    {
                        var kommunenummer = naboEier.gjelderNaboeiendom?.eiendomsidentifikasjon?.kommunenummer;
                        var kommunenummerValideringStatus = MunicipalityValidator.Validate_kommunenummerStatus(kommunenummer);
                        if (kommunenummerValideringStatus.Status != MunicipalityValidation.Ok)
                        {
                            switch (kommunenummerValideringStatus.Status)
                            {
                                case MunicipalityValidation.Empty:
                                    //_validationResult.AddMessage("4655.1.2...", null);
                                    break;

                                case MunicipalityValidation.Invalid:
                                    _validationResult.AddMessage("4655.1.2.3", new[] { kommunenummer });
                                    break;

                                case MunicipalityValidation.Expired:
                                    _validationResult.AddMessage("4655.1.2.4", new[] { kommunenummer, kommunenummerValideringStatus.Status.ToString() });
                                    break;
                            }
                        }

                        if (!Helpers.ObjectIsNullOrEmpty(naboEier.gjelderNaboeiendom.adresse))
                        {
                            if (String.IsNullOrEmpty(naboEier.gjelderNaboeiendom.adresse.adresselinje1))
                            {
                                _validationResult.AddMessage("4655.3.13.1", null);
                            }
                            //In another validation if "addressline1" is null we do not validate the rest
                            if (!CountryCodeHandler.IsCountryNorway(naboEier.gjelderNaboeiendom.adresse.landkode))
                            {
                                if (!CountryCodeHandler.VerifyCountryCode(naboEier.gjelderNaboeiendom.adresse.landkode))
                                {
                                    _validationResult.AddMessage("4655.3.20", null);
                                }
                            }
                            else
                            {
                                var postNr = naboEier.gjelderNaboeiendom.adresse.postnr;
                                var landkode = naboEier.gjelderNaboeiendom.adresse.landkode;

                                if (string.IsNullOrEmpty(postNr))
                                {
                                    _validationResult.AddMessage("4655.3.19", null);
                                }
                                else
                                {
                                    if (!GeneralValidations.postNumberMatch(postNr).Success)
                                    {
                                        _validationResult.AddMessage("4655.3.19.1", new[] { postNr });
                                    }
                                    else
                                    {
                                        var postnrValidation = _postalCodeProvider.ValidatePostnr(postNr, landkode);
                                        if (!Helpers.ObjectIsNullOrEmpty(postnrValidation))
                                        {
                                            if (!postnrValidation.Valid)
                                            {
                                                _validationResult.AddMessage("4655.3.19.2", null);
                                            }
                                            else
                                            {
                                                if (!postnrValidation.Result.Equals(naboEier.gjelderNaboeiendom.adresse.poststed, StringComparison.CurrentCultureIgnoreCase))
                                                {
                                                    _validationResult.AddMessage("4655.3.19.3", new[] { postNr, naboEier.gjelderNaboeiendom.adresse.poststed, postnrValidation.Result });
                                                }
                                            }
                                        }
                                        else
                                        {
                                            _validationResult.AddMessage("4655.3.19.4", null);
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            _validationResult.AddMessage("4655.3.13", null);
                        }
                        //if (string.IsNullOrEmpty(naboEier.gjelderNaboeiendom.eier))
                        //{
                        //    _validationResult.AddMessage("4655.3.37", Helpers.GetFullClassPath(() => form.naboeier[0].gjelderNaboeiendom.eier), _skjema);
                        //}
                    }
                    else
                    {
                        _validationResult.AddMessage("4655.1.1.4", null);
                    }
                }
                else
                {
                    _validationResult.AddMessage("4655.1.1.5", null);
                }
            }
        }

        internal void SoekerValidation(NabovarselType form)
        {
            using (var logger = new PerfTimerLogger())
            {
                // validere at alle data for søker(tiltakshaver elle ansvarligSoeker) er med
                var ansvarlidSoekerHaveEnoughData = (!PartsTypeIsNullOrEmpry(form.ansvarligSoeker) && !Helpers.ObjectIsNullOrEmpty(form.ansvarligSoeker?.adresse) && !string.IsNullOrEmpty(form.ansvarligSoeker?.navn));

                if (ansvarlidSoekerHaveEnoughData)
                {
                    if (form.ansvarligSoeker.partstype != null)
                    {
                        if (!_codeListService.IsCodelistValid("Partstype", form.ansvarligSoeker.partstype.kodeverdi))
                        {
                            _validationResult.AddMessage("4655.1.2.5", new[] { form.ansvarligSoeker.partstype.kodeverdi });
                        }
                        else
                        {
                            if (form.ansvarligSoeker.partstype.kodeverdi == "Privatperson")
                            {
                                var foedselsnummerValidation = GeneralValidations.Validate_foedselsnummer(form.ansvarligSoeker.foedselsnummer);
                                switch (foedselsnummerValidation)
                                {
                                    case GeneralValidations.FoedselnumerValidation.Empty:
                                        _validationResult.AddMessage("4655.1.5", null);
                                        break;

                                    case GeneralValidations.FoedselnumerValidation.Invalid:
                                        _validationResult.AddMessage("4655.1.5.1", null);
                                        break;

                                    case GeneralValidations.FoedselnumerValidation.InvalidDigitsControl:
                                        _validationResult.AddMessage("4655.1.5.2", null);
                                        break;

                                    case GeneralValidations.FoedselnumerValidation.InvalidEncryption:
                                        _validationResult.AddMessage("4655.1.5.3", null);
                                        break;
                                }
                            }
                            else
                            {
                                var organisasjonsnummerValidation = GeneralValidations.Validate_OrgnummerEnum(form.ansvarligSoeker.organisasjonsnummer);
                                switch (organisasjonsnummerValidation)
                                {
                                    case GeneralValidations.OrganisasjonsnummerValidation.Empty:
                                        _validationResult.AddMessage("4655.1.5.4", null);
                                        break;

                                    case GeneralValidations.OrganisasjonsnummerValidation.Invalid:
                                        _validationResult.AddMessage("4655.1.5.4.1", new[] { form.ansvarligSoeker.organisasjonsnummer });
                                        break;

                                    case GeneralValidations.OrganisasjonsnummerValidation.InvalidDigitsControl:
                                        _validationResult.AddMessage("4655.1.5.4.2", new[] { form.ansvarligSoeker.organisasjonsnummer });
                                        break;
                                }
                                //if (string.IsNullOrEmpty(form.ansvarligSoeker.kontaktperson?.navn))
                                //    _validationResult.AddMessage("4655.3.21", Helpers.GetFullClassPath(() => form.ansvarligSoeker.kontaktperson.navn), _skjema);
                            }
                        }
                    }
                    //trenger ikke fordi vi bekrefter at adress, Navn og PartsType er alti med for søker (å kjøres validering)
                    //else
                    //{
                    //    var rule = _validationResult.GetRule("4402.1.??");
                    //    _validationResult.AddError(rule.message, rule.id, $"/EndringAvTillatelse/{soeker}/partstype");
                    //}
                    if (form.ansvarligSoeker.adresse != null)
                    {
                        if (String.IsNullOrEmpty(form.ansvarligSoeker.adresse.adresselinje1))
                        {
                            _validationResult.AddMessage("4655.1.1.7", null);
                        }
                        //In another validation if "addressline1" is null we do not validate the rest
                        if (!CountryCodeHandler.IsCountryNorway(form.ansvarligSoeker.adresse.landkode))
                        {
                            if (!CountryCodeHandler.VerifyCountryCode(form.ansvarligSoeker.adresse.landkode))
                            {
                                _validationResult.AddMessage("4655.3.23", null);
                            }
                        }
                        else
                        {
                            var postNr = form.ansvarligSoeker.adresse.postnr;
                            var landkode = form.ansvarligSoeker.adresse.landkode;

                            if (string.IsNullOrEmpty(postNr))
                            {
                                _validationResult.AddMessage("4655.3.22", null);
                            }
                            else
                            {
                                if (!GeneralValidations.postNumberMatch(postNr).Success)
                                {
                                    _validationResult.AddMessage("4655.3.22.1", new[] { postNr });
                                }
                                else
                                {
                                    var postnrValidation = _postalCodeProvider.ValidatePostnr(postNr, landkode);
                                    if (!Helpers.ObjectIsNullOrEmpty(postnrValidation))
                                    {
                                        if (!postnrValidation.Valid)
                                        {
                                            _validationResult.AddMessage("4655.3.22.2", new[] { postNr });
                                        }
                                        else
                                        {
                                            if (!postnrValidation.Result.Equals(form.ansvarligSoeker.adresse.poststed, StringComparison.CurrentCultureIgnoreCase))
                                            {
                                                _validationResult.AddMessage("4655.3.22.3", new[] { postNr, form.ansvarligSoeker.adresse.poststed, postnrValidation.Result });
                                            }
                                        }
                                    }
                                    else
                                    {
                                        _validationResult.AddMessage("4655.3.22.4", null);
                                    }
                                }
                            }
                        }
                    }
                    //trenger ikke fordi vi bekrefter at adress, Navn og PartsType er alti med for å kjøres validering
                    //else
                    //{
                    //    var rule = _validationResult.GetRule("4402.1.??");
                    //    _validationResult.AddError(rule.message, rule.id, $"/EndringAvTillatelse/{soeker}/adresse");
                    //}

                    if (string.IsNullOrEmpty(form.ansvarligSoeker.mobilnummer) && string.IsNullOrEmpty(form.ansvarligSoeker.telefonnummer))
                    {
                        _validationResult.AddMessage("4655.3.15", null);
                    }

                    if (string.IsNullOrEmpty(form.ansvarligSoeker.epost))
                    {
                        _validationResult.AddMessage("4655.3.14", null);
                    }
                }
                else
                {
                    _validationResult.AddMessage("4655.1.1.6", null);
                }
            }
        }

        internal void KontaktPersonValidering(NabovarselType form)
        {
            using (var logger = new PerfTimerLogger())
            {
                if (form.tiltakshaver?.kontaktperson?.navn == null)
                {
                    _validationResult.AddMessage("4655.3.21", null);
                }
            }
        }

        internal void TiltakshaverHarLovligKodeverdi(NabovarselType form)
        {
            using (var logger = new PerfTimerLogger())
            {
                if (form.tiltakshaver != null)
                {
                    if (form.tiltakshaver.partstype != null)
                    {
                        var partstypeKodeverdi = form.tiltakshaver.partstype.kodeverdi;

                        if (!_codeListService.IsCodelistValid("Partstype", partstypeKodeverdi))
                        {
                            _validationResult.AddMessage("4655.1.2.6", new string[] { partstypeKodeverdi });
                        }
                    }
                    else
                    {
                        _validationResult.AddMessage("4655.1.1.8", null);
                    }

                    if (string.IsNullOrEmpty(form.tiltakshaver.navn))
                    {
                        _validationResult.AddMessage("4655.1.1.9", null);
                    }
                }
            }
        }

        internal void MinstEtTiltak(NabovarselType form)
        {
            using (var logger = new PerfTimerLogger())
            {
                if (Helpers.ObjectIsNullOrEmpty(form.beskrivelseAvTiltak))
                {
                    _validationResult.AddMessage("4655.1.1.10", null);
                }
                else
                {
                    if (form.beskrivelseAvTiltak.All(b => b.type == null))
                    {
                        _validationResult.AddMessage("4655.1.1.11", null);
                    }
                    else
                    {
                        Dictionary<string, CodelistFormat> codeData = _codeListService.GetCodelist("tiltaktype", RegistryType.KodelisteByggesoknad);
                        Dictionary<string, CodelistFormat> codeDataTiltaksformaal = _codeListService.GetCodelist("tiltaksformal", RegistryType.KodelisteByggesoknad);
                        Dictionary<string, CodelistFormat> codeDataTiltakutenansvarsrett = _codeListService.GetCodelist("tiltakstyperutenansvarsrett", RegistryType.KodelisteByggesoknad);
                        foreach (var beskTiltak in form.beskrivelseAvTiltak)
                        {
                            foreach (var tiltakstype in beskTiltak.type)
                            {
                                if (!String.IsNullOrEmpty(tiltakstype.kodeverdi))
                                {
                                    if (!codeData.ContainsKey(tiltakstype.kodeverdi) && !codeDataTiltakutenansvarsrett.ContainsKey(tiltakstype.kodeverdi))
                                    {
                                        _validationResult.AddMessage("4655.1.2.7", new[] { tiltakstype.kodeverdi });
                                    }
                                    else _tiltakstyperISoeknad.Add(tiltakstype.kodeverdi);
                                }
                            }

                            if (beskTiltak.bruk != null)
                            {
                                //tiltaksformaal
                                if (beskTiltak.bruk.tiltaksformaal == null)
                                {
                                    _validationResult.AddMessage("4655.1.1.12", null);
                                }
                                else
                                {
                                    foreach (var tiltaksformaal in beskTiltak.bruk.tiltaksformaal)
                                    {
                                        if (!String.IsNullOrEmpty(tiltaksformaal.kodeverdi))
                                        {
                                            if (!codeDataTiltaksformaal.ContainsKey(tiltaksformaal.kodeverdi))
                                            {
                                                _validationResult.AddMessage("4655.1.2.8", new[] { tiltaksformaal.kodeverdi });
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        internal void GjeldendePlanHarLovligVerdi(NabovarselType form)
        {
            using (var logger = new PerfTimerLogger())
            {
                var planTypeKodeverdi = form.gjeldendePlan?.plantype?.kodeverdi;
                if (!string.IsNullOrEmpty(planTypeKodeverdi))
                {
                    if (!_codeListService.IsCodelistValid("Plantype", planTypeKodeverdi))
                    {
                        _validationResult.AddMessage("4655.1.2.9", new[] { planTypeKodeverdi });
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(form.gjeldendePlan.plantype.kodebeskrivelse))
                        {
                            _validationResult.AddMessage("4655.1.1.14", new[] { planTypeKodeverdi });
                        }
                    }
                }
            }
        }

        internal void DispensasjonstypeErLovlig(NabovarselType form, List<string> formSetElements)
        {
            using (var logger = new PerfTimerLogger())
            {
                if (!Helpers.ObjectIsNullOrEmpty(form.dispensasjon))
                {
                    foreach (var dispensasjon in form.dispensasjon)
                    {
                        //if (Helpers.ObjectIsNullOrEmpty(dispensasjon.dispensasjonstype))
                        //{
                        //    _validationResult.AddMessage("4655.3.10.1", null);
                        //}
                        //else
                        //{
                        if (!_codeListService.IsCodeValidInCodeList("dispensasjonstype", dispensasjon.dispensasjonstype.kodeverdi))
                        {
                            _validationResult.AddMessage("4655.1.2.10", new[] { dispensasjon.dispensasjonstype.kodeverdi });
                        }
                        else
                        {
                            if (!formSetElements.Any(f => f.Equals("Dispensasjonssoeknad")))
                            {
                                if (String.IsNullOrEmpty(dispensasjon.beskrivelse))
                                    _validationResult.AddMessage("4655.3.10", null);
                                if (String.IsNullOrEmpty(dispensasjon.begrunnelse))
                                    _validationResult.AddMessage("4655.3.11", null);
                            }
                        }
                        //}
                    }
                }
            }
        }

        //Vedlegg validering
        internal void TegningValidering(List<string> formSetElements)
        {
            var tengningen = new List<string>() {
                    "TegningNyttSnitt",
                    "TegningEksisterendeSnitt",
                    "TegningEksisterendeFasade",
                    "TegningNyFasade"
                };

            bool hasDrawing = formSetElements?.Select(x => x)
                .Intersect(tengningen)
                .Any() ?? false;

            //Minst en tegning
            if (!hasDrawing)
                _validationResult.AddMessage("4655.2.32", null);
            else
            {
                // SnittTegnin validering
                if (!formSetElements.Contains("TegningNyttSnitt"))
                    _validationResult.AddMessage("4655.2.8", null);
                // Fasadetegning Validering
                if (!formSetElements.Contains("TegningNyFasade"))
                    _validationResult.AddMessage("4655.2.9", null);
            }
        }

        internal void SituasjonsplanValidering(NabovarselType form, List<string> formSetElements)
        {
            using (var logger = new PerfTimerLogger())
            {
                if (formSetElements == null || !formSetElements.Contains("Situasjonsplan"))
                    _validationResult.AddMessage("4655.2.7", null);
            }
        }

        private bool PartsTypeIsNullOrEmpry(PartType soeker)
        {
            using (var logger = new PerfTimerLogger())
            {
                if (soeker != null)
                {
                    return Helpers.ObjectIsNullOrEmpty(soeker) || string.IsNullOrEmpty(soeker.partstype?.kodeverdi);
                }

                return false;
            }
        }

        internal bool MinstEnEiendom(NabovarselType form)
        {
            using (var logger = new PerfTimerLogger())
            {
                if (form.eiendomByggested == null || !form.eiendomByggested.Any())
                {
                    _validationResult.AddMessage("4655.1.1.15", null);
                }
                return form.eiendomByggested != null && (form.eiendomByggested != null || form.eiendomByggested.Any());
            }
        }
    }
}