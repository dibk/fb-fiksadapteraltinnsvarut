﻿using System;
using System.Collections.Generic;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services
{
    public interface IFormMetadata
    {
        void SetArchiveReference(string ar);
    }
}