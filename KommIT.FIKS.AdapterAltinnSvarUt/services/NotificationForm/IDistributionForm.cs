﻿using System.Collections.Generic;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services
{
    public interface IDistributionForm
    {
        string GetDistributionMessageTitle();

        string GetDistributionMessageSummary();
        string GetDistributionMessageBody();

        //Get PrefillForms
        List<IAltinnForm> GetPrefillForms();
    }


    
}