﻿using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.NotificationForm.Nabovarsel;
using no.kxml.skjema.dibk.nabovarselsvarPlan;
using System;
using System.Collections.Generic;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.NotificationForm.NabovarselPlan
{
    public class SvarPaNabovarselPlanForm : IAltinnForm, INotificationForm, IPrefillForm, INabovarselSvar
    {

        private const string DataFormatId = "6326";
        private const string DataFormatVersion = "44843";
        private const string SchemaFile = "nabovarselsvarPlan.xsd";
        private const string Name = "Uttalelse til oppstart av reguleringsplanarbeid";
        private List<string> FormSetElements;
        private List<string> SluttbrukersystemVaarReferanse;

        private string _message;

        // TODO PLAN
        // .. AR referanse
        // .. Plan og nabovarsel kjører samme løp, men plan skal ikke i print
        


        public void SetFormSetElements(List<string> attachmentlist)
        {
            FormSetElements = attachmentlist;
        }

        public bool? ValidateAuthenticatedSubmitterVsFormData(string authenticatedSubmitter, ValidationResult validationResult)
        {
            bool isSubmitterValid = Helpers.ValidateEntityOfAltinnSubmitterDefault(authenticatedSubmitter, GetPropertyIdentifiers());

            if (!isSubmitterValid)
            {
                Helpers.RecordErrorAltinnSubmitter(validationResult);
            }
            return isSubmitterValid;
        }

        private no.kxml.skjema.dibk.nabovarselsvarPlan.SvarPaaNabovarselPlanType form;

        private string _archiveReferenceOfDistribution = String.Empty;
        private string _altinnReferenceForPrefillFrom = String.Empty;

        public string GetSendToReporteeId()
        {
            return Utils.Helpers.ReturnFirstOrgNumberOrPersonalNumber(
                        form.forslagsstiller.organisasjonsnummer,
                        form.forslagsstiller.foedselsnummer).IdNumber;
        }
        public string GetNotificationMessageTitle()
        {
            string planNavn = form.planNavn;
            string planid = form.planid;

            return NabovarselPlanMessages.GetNotificationMessageTitle(planNavn, planid);
        }
        public string GetNotificationMessageSender()
        {
            return form.beroertPart.navn;
        }
        public string GetNotificationMessageSummary()
        {
            return $"";

        }
        public string GetNotificationMessageBody()
        {
            string planNavn = form.planNavn;
            string planId = form.planid;
            string kontaktperson = form.forslagsstiller.kontaktperson.navn;
            string berortPart = form.beroertPart.navn;
            string orgnr = form.forslagsstiller.organisasjonsnummer;
            string forslagsstiller = form.forslagsstiller.navn;

            return NabovarselPlanMessages.GetNotificationMessageBody(orgnr, planNavn, planId, kontaktperson, berortPart, forslagsstiller);
        }

        public bool isReply()
        {
            Guid incommingGuid;
            return Guid.TryParse(form.hovedinnsendingsnummer, out incommingGuid);
        }

        public Guid GetIncommingGuid()
        {
            Guid incommingGuid;
            var isReplyAnsvarsrett = Guid.TryParse(form.hovedinnsendingsnummer, out incommingGuid);
            if (!isReplyAnsvarsrett)
            {
                incommingGuid = Guid.NewGuid();
            }
            return incommingGuid;
        }



        public PropertyIdentifiers GetPropertyIdentifiers()
        {
            PropertyIdentifiers propertyIdentifiers = new PropertyIdentifiers();

            propertyIdentifiers.SoeknadSkjemaNavn = Name;
            propertyIdentifiers.FraSluttbrukersystem = form.fraSluttbrukersystem;

            return propertyIdentifiers;
        }


        public string GetDataFormatId()
        {
            return DataFormatId;
        }

        public string GetDataFormatVersion()
        {
            return DataFormatVersion;
        }

        public string GetMunicipalityCode()
        {
            return GetPropertyIdentifiers().Kommunenummer;
        }

        public string GetSchemaFile()
        {
            return SchemaFile;
        }

        public string GetName()
        {
            return Name;
        }

        public void InitiateForm(string formDataAsXml)
        {
            form = Utils.SerializeUtil.DeserializeFromString<SvarPaaNabovarselPlanType>(formDataAsXml);
        }

        public ValidationResult ValidateData()
        {
            ValidationResult valResult = new ValidationResult(0, 0);

            return valResult;
        }

        public bool IsReply()
        {
            Guid incomingGuid;
            return Guid.TryParse(form.hovedinnsendingsnummer, out incomingGuid);
        }

        public Guid GetReplyKey()
        {
            Guid incomingGuid;
            var isReply = Guid.TryParse(form.hovedinnsendingsnummer, out incomingGuid);
            return incomingGuid;
        }

        public int GetReplyDueDays()
        {
            return 0;
        }

        public string GetNotificationXMLFilename()
        {
            return Resources.TextStrings.NotificationNabovarselXMLFilename;
        }

        public string GetNotificationXMLTitle()
        {
            return Resources.TextStrings.NotificationNabovarselXMLTitle;
        }

        public string GetNotificationPdfFilename()
        {
            return Resources.TextStrings.NotificationNabovarselPdfFilename;
        }

        public string GetNotificationPdfTitle()
        {
            return Resources.TextStrings.NotificationNabovarselPdfTitle;
        }

        public string GetPrefillSendToReporteeId()
        {
            return Utils.Helpers.ReturnFirstOrgNumberOrPersonalNumber(
                        form.beroertPart.organisasjonsnummer,
                        form.beroertPart.foedselsnummer).IdNumber;
        }

        public string GetFormXML()
        {
            return Utils.SerializeUtil.Serialize(form);
        }

        public string GetPrefillServiceCode()
        {
            return "5419";
        }

        public string GetPrefillServiceEditionCode()
        {
            return "1";
        }

        public string GetPrefillNotificationTitle()
        {
            return NabovarselPlanMessages.GetPrefillNotificationTitle(form.planNavn, form.planid);
        }

        public string GetPrefillNotificationSummary()
        {
            return $"";
        }


        // Melding i Altinn:
        public string GetPrefillNotificationBody(bool? pdf = false, string replyLink = "")
        {
            PartType forslagsstiller = form.forslagsstiller;
            BeroertPartType beroertPart = form.beroertPart;
            DateTime? fristForInnspill = form.fristForInnspill;
            string kommune = form.kommune;
            
            return NabovarselPlanMessages.GetPrefillNotificationBody(forslagsstiller, beroertPart, fristForInnspill, kommune, pdf, replyLink);

        }


        public int GetPrefillNotificationDueDays()
        {
            return 0;
        }

        public void SetPrefillKey(string key)
        {
            form.hovedinnsendingsnummer = key;
        }
        public string GetPrefillKey()
        {
            return form.hovedinnsendingsnummer;
        }
        public string GetPrefillLinkText()
        {
            return Resources.TextStrings.NabovarselPlanLinkText;
        }

        public string GetPrefillMessageSender()
        {
            return form.forslagsstiller.navn;
        }

        public string GetPrefillOurReference()
        {
            return form.beroertPart.systemReferanse;
        }

        public bool DoEmailNotification()
        {
            if (_emailnotification != null) return true;
            else return false;
        }

        public void SetEmailNotification(string email, string subject, string emailcontent, string smscontent)
        {
            _emailnotification = new EmailNotification();
            _emailnotification.Email = email;
            _emailnotification.EmailSubject = subject;
            _emailnotification.EmailContent = emailcontent;
            _emailnotification.SmsContent = smscontent;
        }
        private EmailNotification _emailnotification;
        public EmailNotification GetEmailNotification()
        {
            return _emailnotification;
        }

        public string GetArchiveReferenceOfDistribution()
        {
            return _archiveReferenceOfDistribution;
        }

        public void SetArchiveReferenceOfDistribution(string ar)
        {
            _archiveReferenceOfDistribution = ar;
        }

        public string GetAltinnReferenceForPrefillFrom()
        {
            return _altinnReferenceForPrefillFrom;
        }

        public void SetAltinnReferenceForPrefillFrom(string altinnReference)
        {
            _altinnReferenceForPrefillFrom = altinnReference;
        }

        public NotificationEnums.NotificationCarrier GetNotificationType()
        {
            return NotificationEnums.NotificationCarrier.AltinnEmailPreferred;
        }

        public string GetAltinnNotificationTemplate()
        {
            return Resources.TextStrings.AltinnNotificationTemplateNabo;
        }


        public NotificationEnums.NotificationChannel GetNotificationChannel()
        {
            return NotificationEnums.NotificationChannel.CorrespondenceWithPrefillEndpointValidation;
        }

        public bool ConfigureSecondNotification()
        {
            return false;
        }

        public Address GetNeighborMailingAddress()
        {
            return new Address(form.beroertPart.navn,
                                form.beroertPart.adresse.adresselinje1, form.beroertPart.adresse.adresselinje2, form.beroertPart.adresse.adresselinje3,
                                form.beroertPart.adresse.poststed,
                                form.beroertPart.adresse.postnr,
                                form.beroertPart.adresse.landkode);
        }

        public Address GetApplicantMailingAddress()
        {
            var addr = new Address(form.forslagsstiller.navn,
                form.forslagsstiller.adresse.adresselinje1, form.forslagsstiller.adresse.adresselinje2, form.forslagsstiller.adresse.adresselinje3,
                form.forslagsstiller.adresse.poststed,
                form.forslagsstiller.adresse.postnr,
                form.forslagsstiller.adresse.landkode);

            if (!String.IsNullOrWhiteSpace(form.forslagsstiller.organisasjonsnummer))
            {
                addr.OrganizationNumber = form.forslagsstiller.organisasjonsnummer;
            }
            else if (!String.IsNullOrWhiteSpace(form.forslagsstiller.foedselsnummer))
            {
                addr.SosialServiceNumber = form.forslagsstiller.foedselsnummer;
            }
            else
            {
                addr.OrganizationNumber = null;
                addr.SosialServiceNumber = null;
            }
            return addr;
        }

        public List<string> GetSluttbrukersystemVaarReferanse()
        {
            return SluttbrukersystemVaarReferanse;
        }

        public void SetSluttbrukersystemVaarReferanse(List<string> refs)
        {
            SluttbrukersystemVaarReferanse = refs;
        }

        private DateTime? _replyDeadline;
        public void SetReplyDeadline(DateTime? ReplyDeadline)
        {
            _replyDeadline = ReplyDeadline;
        }

        public DateTime? GetReplyDeadline()
        {
            return _replyDeadline;
        }

        public KvitteringForNabovarsel GetkvitteringForNabovarsel()
        {
            var kvitteringForNabovarsel = new KvitteringForNabovarsel()
            {
                Hovedinnsendingsnummer = form.hovedinnsendingsnummer,
                NaboNavn = form.beroertPart.navn,
                GjelderNaboeiendommer = new List<Eiendom>(),
                Eiendommer = new List<Eiendom>()
            };
            if (form.beroertPart.adresse != null)
            {
                kvitteringForNabovarsel.NaboAdress = string.Concat(form.beroertPart.adresse.adresselinje1, " ", form.beroertPart.adresse.adresselinje2, " ", form.beroertPart.adresse.adresselinje3);
                kvitteringForNabovarsel.NaboPostnr = form.beroertPart.adresse.postnr;
                kvitteringForNabovarsel.NaboPoststed = form.beroertPart.adresse.poststed;
            }

            return kvitteringForNabovarsel;
        }
        public bool IsStoreMessageFile()
        {
            return false;
        }
        public IEnumerable<CorrespondenceAttachmentsType> CorrespondenceAttachmentsTypes => new List<CorrespondenceAttachmentsType>()
        {
            CorrespondenceAttachmentsType.MainFormPdf, 
            CorrespondenceAttachmentsType.MainFormXml, 
            CorrespondenceAttachmentsType.Attachments
        };

        public IEnumerable<StoreToBlob> StoreToBlobs => new List<StoreToBlob>()
        {
            StoreToBlob.MainFormPdf, 
            StoreToBlob.MainFormXml, 
            StoreToBlob.Attachments
        };


    }
}