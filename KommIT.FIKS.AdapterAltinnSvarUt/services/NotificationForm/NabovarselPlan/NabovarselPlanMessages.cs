using no.kxml.skjema.dibk.nabovarselsvarPlan;
using System;
using System.Linq;
using KommIT.FIKS.AdapterAltinnSvarUt.Extensions;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.NotificationForm.NabovarselPlan
{
    public static class NabovarselPlanMessages
    {
        private static string _message;

        public static string GetDistributionMessageSummary()
        {
            return $"";
        }

        public static string GetDistributionMessageBody()
        {
            return "";
        }

        public static string GetSMSNotificationMessage(string orgnr, string berortPartNavn, string kommunenavn, string fristForInnspill, string forslagsstillerNavn)
        {
            string evtOrgnr = "";
            if (!string.IsNullOrEmpty(orgnr)) evtOrgnr = $" (org.nr. {orgnr})";

            string body =
                $"{berortPartNavn}{evtOrgnr} har fått varsel om oppstart av arbeid med reguleringsplan i {kommunenavn} kommune. " +
                $"Logg inn på www.altinn.no for å se varselet." +
                $"Du må svare innen {(fristForInnspill)} hvis du vil uttale deg. Altinn-referanse: ARXXXXXX." +
                $"Hilsen {forslagsstillerNavn}";
            return body;
        }

        /// <summary>
        /// Epost som blir sendt fra forslagsstiller til berørt part for å varsle om oppstart av planarbeid
        /// </summary>
        /// <param name="orgnr">Org nr til nabo</param>
        /// <param name="nabo">Navn på nabo</param>
        /// <param name="berortPartNavn">Navn på berørt part</param>
        /// <param name="berortPartOrgNr">Org nr til berørt part</param>
        /// <param name="kommunenavn">Navn på kommune hvor planarbeidet skal skje </param>
        /// <param name="plannavn">Navnet på planen</param>
        /// <param name="fristForInnspill">Dato for siste frist man kan komme med innspill</param>
        /// <param name="forslagsstiller">Forslagsstiller, de som skal starte planarbeidet</param>
        /// <returns></returns>
        public static string GetEmailNotificationBody(string orgnr, string nabo, string kommunenavn, string plannavn, string fristForInnspill, no.kxml.skjema.dibk.nabovarselPlan.PartType forslagsstiller)
        {
            string evtOrgnr = "";
            if (!string.IsNullOrEmpty(orgnr)) evtOrgnr = $"(org.nr. {orgnr})";

            // Dersom berørt part er en organisasjon
            

            // Melding
            string body = $"Til {nabo} {evtOrgnr}";

            body += $"<p>" +
                    $"Vi varsler om oppstart av arbeid med reguleringsplan i {kommunenavn} kommune. <br>" +
                    $"Navnet på reguleringsplanen er: {plannavn}." +
                    $"</p>";

            body += $"Vi sender deg dette varselet fordi du kan være berørt eller har interesser i nærheten av området vi vil regulere.";

            body += $"<p>" +
                    $"<strong>Vil du ikke uttale deg?</strong><br> " +
                    $"Da trenger du ikke å gjøre noe som helst." +
                    $"</p>";

            body += $"<p>" +
                    $"<strong>Du kan uttale deg om oppstarten av reguleringsplanarbeidet</strong><br>" +
                    $"<a href='https://www.altinn.no'>Logg inn på Altinn for å se varselet</a> og svar innen {fristForInnspill}." +
                    $"</p>";

            if (!string.IsNullOrEmpty(orgnr))
            {
                body += $"<p>" +
                        $"<strong>Om tilgang i Altinn</strong><br>" +
                        $"For å se varselet om oppstart, må du representere {nabo} {evtOrgnr} i Altinn. <br>" +
                        $"Den som skal se og svare på varselet, må ha rollen «Plan- og byggesak» i organisasjonen. " +
                        $"Se <a href='https://dibk.no/verktoy-og-veivisere/andre-fagomrader/fellestjenester-bygg/slik-gir-du-rettigheter-til-byggesak-i-altinn/'>veileder om rettigheter i Altinn</a> for mer informasjon." +
                        $"</p>";
            }


            body += $"<p>" +
                    $"Med vennlig hilsen,<br>" +
                    $"Forslagsstiller {forslagsstiller.navn}<br>";

            var telefon = Telefon(forslagsstiller);
            body += $"Telefon: {telefon} <br>";
            
            var epost = Epost(forslagsstiller);
            body += $"Epost: {epost}";
            
            body += $"</p>";
            
            body += $"<p><em>Det er ikke mulig å svare på denne e-posten. Spørsmål om innholdet i varselet, må du rette til {forslagsstiller.navn}.</em></p>";

            return body;
        }

        private static string Epost(no.kxml.skjema.dibk.nabovarselPlan.PartType forslagsstiller)
        {
            string epost = "";
            if (!string.IsNullOrEmpty(forslagsstiller.kontaktperson.epost))
            {
                epost = forslagsstiller.kontaktperson.epost;
            }
            else if (!string.IsNullOrEmpty(forslagsstiller.epost))
            {
                epost = forslagsstiller.epost;
            }

            return epost;
        }

        private static string Telefon(no.kxml.skjema.dibk.nabovarselPlan.PartType forslagsstiller)
        {
            string telefon = "";
            if (!string.IsNullOrEmpty(forslagsstiller.kontaktperson.telefonnummer))
            {
                telefon = forslagsstiller.kontaktperson.telefonnummer;
            }
            else if (!string.IsNullOrEmpty(forslagsstiller.telefon))
            {
                telefon = forslagsstiller.telefon;
            }

            return telefon;
        }

        public static string GetNotificationMessageTitle(string planNavn, string planid)
        {
            return $"Uttalelse - Oppstart av reguleringsplanarbeid ({planid})";
        }

        public static string GetNotificationMessageBody(string orgnr, string planNavn, string planId,
            string kontaktperson, string berortPart, string forslagsstiller)
        {
            string body =
                $"<p>Du har fått en uttalelse på oppstart av reguleringsplanarbeid. Trykk på filen nederst i denne meldingen for å lese uttalelsen. </p>";

            body += $"Navn på plan: {planNavn} <br>";
            body += $"Planident:    {planId} <br>";

            //TODO PLAN
            // BuildMessageString($"Altinn referanse:    {AR} <br>"); ask from KMD but we do not have access to that data 

            return body;
        }

        /// <summary>
        /// Tittel på altinn melding prefill til berørt part.
        /// </summary>
        /// <param name="planid"></param>
        /// <returns></returns>
        public static string GetPrefillNotificationTitle(string planNavn, string planid)
        {
            return $"Varsel - oppstart av reguleringsplanarbeid - {planNavn}";
        }

        /// <summary>
        /// Altinn melding prefill til berørt part.
        /// </summary>
        /// <param name="forslagsstiller">Forslagsstiller som ønsker å endre eller bygge</param>
        /// <param name="beroertPart">Part som blir berørt av endringen/byggingen  </param>
        /// <param name="fristForInnspill">Dato på frist for innspill</param>
        /// <param name="kommune">Aktuell kommune</param>
        /// <param name="pdf">Skal det lages en pdf</param>
        /// <param name="replyLink">Link til svarskjema(?)</param>
        /// <returns></returns>
        public static string GetPrefillNotificationBody(PartType forslagsstiller, BeroertPartType beroertPart, DateTime? fristForInnspill, string kommune, bool? pdf = false,
            string replyLink = "")
        {
            if (pdf.HasValue && pdf.Value)
            {
                createBodyForPDF(beroertPart, forslagsstiller, fristForInnspill);
            }
            else
            {
                _message = String.Empty;
                string datoFristInnspill = String.Empty;
                if (fristForInnspill.HasValue)
                {
                    datoFristInnspill = String.Format("{0:MM.dd.yyyy}", fristForInnspill);
                }
                BuildMessageString($"<p>");
                BuildMessageString($"{forslagsstiller.navn} ønsker å starte arbeid med reguleringsplan i {kommune} kommune.");
                BuildMessageString($"</p>");

                BuildMessageString($"<p>");
                BuildMessageString($"<strong>Klikk på dokumentene nederst i denne meldingen for å lese varselbrevet, og for å se kart over området.</strong>");
                BuildMessageString($"</p>");

                BuildMessageString($"<p>");
                BuildMessageString($"<strong>Vil du ikke uttale deg?</strong><br>");
                BuildMessageString("Da trenger du ikke å gjøre noe som helst.");
                BuildMessageString("</p>");

                BuildMessageString($"<p>");
                BuildMessageString($"<strong>Vil du uttale deg om arbeidet med reguleringsplanen?</strong><br>");
                BuildMessageString($"Du kan bruke svarskjemaet under for å uttale deg om arbeidet med reguleringsplanen. " +
                                   $"<br>Fristen for å sende uttalelse, er <strong>{datoFristInnspill}</strong></p>");

                BuildMessageString($"<p><strong>Hva skjer videre?</strong>");
                BuildMessageString($"<ol>");
                BuildMessageString($"<li>1. Uttalelsen sendes til {forslagsstiller.navn}. Du får ikke svar på uttalelsen din.</li>");
                BuildMessageString($"<li>2. Alle uttalelser er med og danner grunnlag for reguleringsplanforslaget som senere skal behandles av kommunen.</li>");
                BuildMessageString($"<li>3. Du blir varselet på nytt når forslaget til reguleringsplan er klart til offentlig ettersyn. Da får du en ny mulighet til å uttale deg.</li>");
                BuildMessageString($"</ol>");
                BuildMessageString($"</p>");

                BuildMessageString($"<p>");
                BuildMessageString($"<strong>Hva er en reguleringsplan?</strong><br>");
                BuildMessageString("En reguleringsplan er kommunens måte å styre utviklingen i et bestemt område av kommunen. <br>" +
                                   "Gjennom planen kan politikerne blant annet bestemme hvordan vi skal bo, hvor vi skal ha parker, " +
                                   "næring og fritidsområder, og hvordan vi skal bevege oss i lokalmiljøet.");
                BuildMessageString("</p>");

                BuildMessageString($"<p>");
                BuildMessageString($"<strong>Hvorfor får jeg varsel?</strong><br>");


                if (beroertPart.gjelderEiendom != null && !beroertPart.gjelderEiendom.IsNullOrEmpty())
                {
                    BuildMessageString("Du får dette varselet fordi du kan være berørt eller har interesser i nærheten av: <br>");

                    foreach (var eiendom in beroertPart.gjelderEiendom)
                    {
                        if (eiendom.adresse != null)
                        {
                            BuildMessageString($"- {eiendom.adresse.adresselinje1} ");
                            if (eiendom.eiendomsidentifikasjon != null)
                            {
                                BuildMessageString($"(");
                                if (eiendom.eiendomsidentifikasjon.gaardsnummer != null)
                                    BuildMessageString($"gårdsnr. {eiendom.eiendomsidentifikasjon.gaardsnummer}, ");
                                if (eiendom.eiendomsidentifikasjon.bruksnummer != null)
                                    BuildMessageString($"bruksnr. {eiendom.eiendomsidentifikasjon.bruksnummer}, ");
                                if (eiendom.eiendomsidentifikasjon.festenummer != null)
                                    BuildMessageString($"festenr. {eiendom.eiendomsidentifikasjon.festenummer}, ");
                                if (eiendom.eiendomsidentifikasjon.seksjonsnummer != null)
                                    BuildMessageString($"seksjonsnr. {eiendom.eiendomsidentifikasjon.seksjonsnummer}");
                                BuildMessageString($") ");
                            }
                            BuildMessageString($"<br>");

                        }
                    }

                }
                else
                {
                    BuildMessageString("Vi sender deg dette varselet fordi du kan være berørt eller har interesser i nærheten av området vi vil regulere.");
                }

                BuildMessageString("</p>");


                BuildMessageString($"<p>");
                BuildMessageString($"<strong>Har du spørsmål om reguleringsplanen?</strong><br>");
                BuildMessageString($"Ta kontakt på e-post {forslagsstiller.kontaktperson.epost} eller telefon {forslagsstiller.kontaktperson.telefonnummer}.</p>");

                if (!String.IsNullOrEmpty(replyLink))
                {
                    BuildMessageString($"<a href='{replyLink}'>{GetPrefillLinkText()}</a>");
                }
            }

            //return "";
            return _message;
        }

        private static void BuildMessageString(string newMessage)
        {
            _message = _message + newMessage;
        }

        public static string GetPrefillLinkText()
        {
            return Resources.TextStrings.NabovarselPlanLinkText;
        }

        private static void createBodyForPDF(BeroertPartType beroertPart, PartType forslagsstiller, DateTime? fristForInnspill)
        {
            _message = String.Empty;
            var date = DateTime.Now.AddDays(0);
            var dateNOFormat = date.ToString("dd.MM.yy");

            var body = $"<div class='Name'> <b>{beroertPart.navn}</b></div>";
            body += $"<div class='Paragraf'>{beroertPart.adresse.adresselinje1}</div>";
            body += $"<div class='Paragraf'>{beroertPart.adresse.postnr} {beroertPart.adresse.poststed}</div>";
            body += $"<div class='Heading PaddingTop Paddingbottom'><b> Melding om varsel</b></div>";
            body +=
                $"<div class='Paragraf PaddingTop'>Du har mottatt et varsel om oppstart av reguleringsplanarbeid.</div> ";
            body += $"<div class='SubHeading'>Hvorfor får jeg varsel?</div>";
            body +=
                $"<div class='Paragraf'>Du har mottatt varsel fordi {forslagsstiller.navn} har planer om å bygge eller gjøre endringer i nærheten av følgende eiendom: </div>";
            //foreach (var naboeiendom in form.nabo.gjelderNaboeiendom)
            //    {
            //        body = body + $"<div class='Paragraf'> - {naboeiendom.adresse.adresselinje1} (gårdsnr. {naboeiendom.eiendomsidentifikasjon.gaardsnummer}, bruksnr. {naboeiendom.eiendomsidentifikasjon.bruksnummer})</div>";
            //    }
            body += $"<div class='SubHeading'>Har du spørsmål om reguleringsplanarbeidet?</div>";
            body +=
                $"<div class='Paragraf'>Ta kontakt med forslagsstiller {forslagsstiller.navn}, e-post {forslagsstiller.epost} eller telefon {forslagsstiller.telefon}.</div> ";

            body += $"<div class='SubHeading'>Har du merknader til reguleringsplanarbeidet?</div>";
            body +=
                $"<div class='Paragraf'>Du kan sende inn din uttalelse til reguleringsplanarbeidet. Du må sende det innen {fristForInnspill}.</div> ";

            body += $"<div class='SubHeading'>Hva skjer med merknadene dine? </div>";
            body +=
                $"<div class='Paragraf'Du får ikke et eget svarbrev, men dine uttalelser blir vurdert og omtalt av forslagsstiller, og blir en del av det samlede planmaterialet som behandles videre i kommunen. " +
                $"Videre får du også mulighet til å uttale deg til planforslaget under høring og offentlig ettersyn av reguleringsplanen.</div>";

            body += $"<div class='SubHeading'>Har du ingen merknader til byggeplanene?</div>";
            body += $"<div class='Paragraf'>Da trenger du ikke gjøre noe som helst.</div> ";

            body += $"<div class='SubHeading'>Merknader sendes til:</div>";
            body += $"<div class='Paragraf'>{forslagsstiller.navn}</div>";
            body +=
                $"<div class='Paragraf'>Adresse: {forslagsstiller.adresse.adresselinje1}, {forslagsstiller.adresse.postnr} {forslagsstiller.adresse.poststed}.</div>";
            body += $"<div class='Paragraf'>Merknader kan også sendes på e-post: {forslagsstiller.epost}</div>";

            _message = body;
        }

    }
}