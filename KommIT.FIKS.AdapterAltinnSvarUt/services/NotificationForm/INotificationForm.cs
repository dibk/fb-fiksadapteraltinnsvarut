﻿using System;
using System.Collections.Generic;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services
{
    public interface INotificationForm
    {
        string GetNotificationMessageTitle();

        string GetNotificationMessageSummary();
        string GetNotificationMessageBody();

        string GetSendToReporteeId();

        string GetNotificationMessageSender();

        bool IsReply();
        
        /// <summary>
        /// Sjekke om det er en fil for å lagre i Blob storage
        /// </summary>
        /// <returns></returns>
        bool IsStoreMessageFile();

        Guid GetReplyKey();

        int GetReplyDueDays();
        void SetReplyDeadline(DateTime? ReplyDeadline);

        DateTime? GetReplyDeadline();

        string GetNotificationXMLFilename();
        string GetNotificationXMLTitle();
        string GetNotificationPdfFilename();
        string GetNotificationPdfTitle();

        /// <summary>
        /// Sjekker om det er mulig å sende epostvarsel via altinn
        /// </summary>
        /// <returns></returns>
        bool DoEmailNotification();

        void SetEmailNotification(string email, string subject, string emailcontent, string smscontent);

        IEnumerable<CorrespondenceAttachmentsType> CorrespondenceAttachmentsTypes { get; }
        IEnumerable<StoreToBlob> StoreToBlobs { get; }

        EmailNotification GetEmailNotification();
    }

    public enum CorrespondenceAttachmentsType
    { 
        MainFormPdf,
        MainFormXml,
        Attachments
    }

    public enum StoreToBlob
    {
        MainFormPdf,
        MainFormXml,
        Attachments
    }


}