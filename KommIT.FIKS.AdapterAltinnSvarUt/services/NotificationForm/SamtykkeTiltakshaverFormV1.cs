﻿using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services
{
    public class SamtykkeTiltakshaverFormV1 : IAltinnForm, INotificationForm, IFormMetadata
    {

        private readonly Guid _GuidReplayKey = Guid.NewGuid();
        private const string DataFormatId = "6147";
        private const string DataFormatVersion = "44097";
        private const string SchemaFile = "tiltakshaversSignatur.xsd";
        private const string Name = "Samtykke tiltakshaver";

        private string _archiveReference;

        private List<string> FormSetElements;
        public void SetFormSetElements(List<string> attachmentlist)
        {
            FormSetElements = attachmentlist;
        }

        public bool? ValidateAuthenticatedSubmitterVsFormData(string authenticatedSubmitter, ValidationResult validationResult)
        {
            bool isSubmitterValid = Helpers.ValidateEntityOfAltinnSubmitterDefault(authenticatedSubmitter, GetPropertyIdentifiers());

            if (!isSubmitterValid)
            {
                Helpers.RecordErrorAltinnSubmitter(validationResult);
            }
            return isSubmitterValid;
        }

        private no.kxml.skjema.dibk.tiltakshaversSignatur.TiltakshaversSignaturType form;



        private string GetFormattedAddress()
        {
            var propIdentifiers = GetPropertyIdentifiers();
            string addr = string.Empty;

            if (!string.IsNullOrWhiteSpace(propIdentifiers.Adresselinje1))
            {
                addr = $"{propIdentifiers.Adresselinje1}";

                if (!string.IsNullOrWhiteSpace(propIdentifiers.Gaardsnummer) && !string.IsNullOrWhiteSpace(propIdentifiers.Bruksnummer))
                {
                    addr = $"{addr} (gårdsnr. {propIdentifiers.Gaardsnummer}, bruksnr. {propIdentifiers.Bruksnummer})";
                }
            }
            return addr;
        }


        public string GetNotificationMessageTitle()
        {

            // Fasit:
            // Tiltakshavers samtykke - prosjektnavn (prosjektnr) ellers: byggested.adresse (gårdsnr. X, bruksnr. X), fra tiltakshaver.navn

            string title = "Tiltakshavers samtykke";
            string addr = GetFormattedAddress();

            if (!string.IsNullOrWhiteSpace(form.prosjektnavn))
            {
                title = $"{title} - {form.prosjektnavn} (prosjektnr)";
            } 
            else if (!string.IsNullOrWhiteSpace(addr))
            {
                title = $"{title} - {addr}";
            }
            
            if (!string.IsNullOrWhiteSpace(form.tiltakshaver))
            {
                title = $"{title}, fra {form.tiltakshaver}";
            }

            return title;
        }

        public string GetNotificationMessageSender()
        {
            return form.signatur.signertAv;
        }

        public string GetNotificationMessageSummary()
        {
            //return $"Melding er sendt via Fellestjenester Bygg fra Direktoratet for Byggkvalitet";
            return string.Empty;
        }
        public string GetNotificationMessageBody()
        {
            // Fasit
            // Arkitekt Flink(org.nr. 123456789) har fått tiltakshavers samtykke for byggested.adresse(gårdsnr.X, bruksnr.Y).
            //
            //    Samtykket er signert av tiltakshaver.navn.


            string message = $"{form.ansvarligSoeker.navn} (org.nr. {form.ansvarligSoeker.organisasjonsnummer}) har fått tiltakshavers samtykke for {GetFormattedAddress()}.";
            message = message + " <br><br>";
            message = message + $"Samtykket er signert av {form.tiltakshaver}.";

            if (_archiveReference != null)
            {
                message = message + " <br><br>";
                message = message + $"Altinnreferanse: {_archiveReference}.";
            }


            return message;
        }

        public PropertyIdentifiers GetPropertyIdentifiers()
        {
            PropertyIdentifiers propertyIdentifiers = new PropertyIdentifiers();


            propertyIdentifiers.Adresselinje1 = form.eiendomByggested[0].adresse.adresselinje1;

            propertyIdentifiers.Postnr = form.eiendomByggested[0].adresse.postnr;
            propertyIdentifiers.Poststed = form.eiendomByggested[0].adresse.poststed;

            propertyIdentifiers.Gaardsnummer = form.eiendomByggested[0].eiendomsidentifikasjon.gaardsnummer;
            propertyIdentifiers.Bruksnummer = form.eiendomByggested[0].eiendomsidentifikasjon.bruksnummer;

            propertyIdentifiers.SoeknadSkjemaNavn = Name;
            propertyIdentifiers.FraSluttbrukersystem = "";



            return propertyIdentifiers;
        }


        public string GetDataFormatId()
        {
            return DataFormatId;
        }

        public string GetDataFormatVersion()
        {
            return DataFormatVersion;
        }

        public string GetMunicipalityCode()
        {
            return "";
        }

        public string GetSchemaFile()
        {
            return SchemaFile;
        }

        public string GetName()
        {
            return Name;
        }

        public void InitiateForm(string formDataAsXml)
        {
            form = Utils.SerializeUtil.DeserializeFromString<no.kxml.skjema.dibk.tiltakshaversSignatur.TiltakshaversSignaturType>(formDataAsXml);
        }

        public ValidationResult ValidateData()
        {
            ValidationResult valResult = new ValidationResult(0, 0);


            return valResult;


        }

        public string GetSendToReporteeId()
        {

            return Utils.Helpers.ReturnFirstOrgNumberOrPersonalNumber(
                        form.ansvarligSoeker.organisasjonsnummer,
                        form.ansvarligSoeker.foedselsnummer).IdNumber;
        }

        public bool IsReply()
        {
            return false;
        }

        public Guid GetReplyKey()
        {
            //Guid incomingGuid = new Guid();
            //var isReply = Guid.TryParse(form.hovedinnsendingsnummer, out incomingGuid);
            return _GuidReplayKey;
        }

        public int GetReplyDueDays()
        {
            return 0;
        }
        public string GetNotificationXMLFilename()
        {
            return Resources.TextStrings.NotificationSamtykkeTiltakshaverXMLFilename;
        }

        public string GetNotificationXMLTitle()
        {
            return Resources.TextStrings.NotificationSamtykkeTiltakshaverXMLTitle;
        }

        public string GetNotificationPdfFilename()
        {
            return Resources.TextStrings.NotificationSamtykkeTiltakshaverPdfFilename;
        }

        public string GetNotificationPdfTitle()
        {
            return Resources.TextStrings.NotificationSamtykkeTiltakshaverPdfTitle;
        }
        public bool DoEmailNotification()
        {
            if (_emailnotification != null) return true;
            else return false;
        }

        public void SetEmailNotification(string email, string subject, string emailcontent, string smscontent)
        {
            _emailnotification = new EmailNotification();
            _emailnotification.Email = email;
            _emailnotification.EmailSubject = subject;
            _emailnotification.EmailContent = emailcontent;
            _emailnotification.SmsContent = smscontent;
        }
        private EmailNotification _emailnotification;
        public EmailNotification GetEmailNotification()
        {
            return _emailnotification;
        }

        private DateTime? _replyDeadline;

        public IEnumerable<CorrespondenceAttachmentsType> CorrespondenceAttachmentsTypes => new List<CorrespondenceAttachmentsType>() { CorrespondenceAttachmentsType.MainFormPdf };
        public IEnumerable<StoreToBlob> StoreToBlobs => new List<StoreToBlob>() { StoreToBlob.MainFormPdf, StoreToBlob.MainFormXml };

        public void SetReplyDeadline(DateTime? ReplyDeadline)
        {
            _replyDeadline = ReplyDeadline;
        }

        public DateTime? GetReplyDeadline()
        {
            return _replyDeadline;
        }

        public bool IsStoreMessageFile()
        {
            return true;
        }


        public void SetArchiveReference(string ar)
        {
            _archiveReference = ar;
        }
    }
}