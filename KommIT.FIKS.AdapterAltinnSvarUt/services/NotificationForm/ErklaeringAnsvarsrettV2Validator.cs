﻿using System;
using System.Collections.Generic;
using System.Linq;
using DIBK.FBygg.Altinn.MapperCodelist;
using KommIT.FIKS.AdapterAltinnSvarUt.Resources;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Validations;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;
using Microsoft.Owin.Security;
using no.kxml.skjema.dibk.ansvarsrettDirekteOpprettet;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.NotificationForm
{
    /// <summary>
    /// Validator for Erklæring om ansvarsett
    /// </summary>
    public class ErklaeringAnsvarsrettV2Validator
    {
        private ValidationResult _validationResult = new ValidationResult();
        CodeListService _codeListService = new CodeListService();
        private readonly MunicipalityValidator _municipalityValidator;
        public ErklaeringAnsvarsrettV2Validator()
        {
            _municipalityValidator = new MunicipalityValidator();
        }
        public ValidationResult Validate(ErklaeringAnsvarsrettType form, List<string> formSetElements)
        {
            _validationResult = new ValidationResult();

            MinstEnEiendom(form);

            MinstEtAnsvarsomraade(form);

            ValidereAnsvarsomraader(form);

            EiendomByggestedValidering(form);

            //KunEnAnsvarligSoeker(form);

            //ForetaketsKontaktpersonErPaakrevd(form); 

            AnsvarligForetakGyldigOrganisasjonsNr(form);

            AnsvarligSoekerGyldigOrganisasjonsNr(form); // Hvordan blir det med utenlandske foretak?

            // Riktig erklæring må fylles ut i forhold til hva som er fylt ut i skjema - erklæringsboksene må kobles til kodeliste ansvarsomraade/funksjon
            AnsvarligSokerOgForetakValidering(form, formSetElements);


            return _validationResult;
        }

        internal ValidationResult GetResult() => _validationResult;

        public void AnsvarligSokerOgForetakValidering(ErklaeringAnsvarsrettType form, List<string> formSetElements)
        {

            var hovedskjema = new[]
            {
                "Søknad om igangsettingstillatelse",
                "Søknad om rammetillatelse",
                "Søknad om tillatelse i ett trinn",
                "Gjennomføringsplan", "GjennomføringsplanV4"
            };

            if (formSetElements.Any(attachments => hovedskjema.Any(attachments.Equals)))
            {
                if (!Helpers.ObjectIsNullOrEmpty(form.ansvarsrett?.foretak?.organisasjonsnummer) && !Helpers.ObjectIsNullOrEmpty(form.ansvarligSoeker?.organisasjonsnummer))
                {
                    if (form.ansvarsrett?.foretak?.organisasjonsnummer != form.ansvarligSoeker?.organisasjonsnummer)
                    {
                        _validationResult.AddError("Underskjema Erklæring om ansvarsrett kan bare brukes for ansvarlig søkers ansvar", "4965.2.7", "/ErklaeringAnsvarsrett/ansvarsrett/foretak/organisasjonsnummer");
                    }
                }
            }
        }

        internal void MinstEtAnsvarsomraade(ErklaeringAnsvarsrettType form)
        {
            if (form.ansvarsrett == null || form.ansvarsrett.ansvarsomraader == null ||
                 !form.ansvarsrett.ansvarsomraader.Any())
                _validationResult.AddError(ValidationError.MinstEtAnsvarsomraade, "4965.1.1",
                    "ErklaeringAnsvarsrett/ansvarsrett/ansvarsomraader");
        }

        internal void ValidereAnsvarsomraader(ErklaeringAnsvarsrettType form)
        {

            if (!Helpers.ObjectIsNullOrEmpty(form.ansvarsrett?.ansvarsomraader))
            {
                foreach (var ansvarsomraade in form.ansvarsrett.ansvarsomraader)
                {

                    var funkKodeverdi = ansvarsomraade.funksjon?.kodeverdi;
                    if (!string.IsNullOrEmpty(funkKodeverdi))
                    {
                        if (!_codeListService.IsCodelistValid("Funksjon", ansvarsomraade.funksjon?.kodeverdi))
                        {
                            _validationResult.AddError($"Funksjon har ugyldig verdi {ansvarsomraade.funksjon?.kodeverdi}", "4965.1.2", "/ErklaeringAnsvarsrett/ansvarsrett/ansvarsomraader[0]/funksjon/kodeverdi");
                        }
                    }

                    if (String.IsNullOrEmpty(ansvarsomraade.beskrivelseAvAnsvarsomraade))
                    {
                        _validationResult.AddError(ValidationError.MinstEtAnsvarsomraade, "4965.1.1",
                        "/ErklaeringAnsvarsrett/ansvarsrett/ansvarsomraader[0]/beskrivelseAvAnsvarsomraade");
                    }

                    if (!_codeListService.IsCodelistValid("Tiltaksklasse", ansvarsomraade.tiltaksklasse?.kodeverdi))
                    {
                        _validationResult.AddError($"Tiltaksklasse har ugyldig verdi {ansvarsomraade.tiltaksklasse?.kodeverdi}", "4965.1.2", "/ErklaeringAnsvarsrett/ansvarsrett/ansvarsomraader[0]/tiltaksklasse/kodeverdi");
                    }
                }
            }
        }

        internal void MinstEnEiendom(ErklaeringAnsvarsrettType form)
        {
            if (form.eiendomByggested == null || !form.eiendomByggested.Any())
                _validationResult.AddError(ValidationError.MinstEnEiendom, "4965.1.1", "/ErklaeringAnsvarsrett/eiendomByggested/eiendom");
        }

        internal void EiendomByggestedValidering(ErklaeringAnsvarsrettType form)
        {
            foreach (var eiendom in form.eiendomByggested)
            {
                GyldigKommuneNrEiendom(eiendom.eiendomsidentifikasjon.kommunenummer);
                //Aktuelt å verifisere mot matrikkel?
            }
        }

        internal void GyldigKommuneNrEiendom(string kommunenr)
        {
            GeneralValidationResult generalValidationResult =
                _municipalityValidator.ValidateKommunenummer(kommunenr);
            if (generalValidationResult.Status != ValidationStatus.Ok)
            {
                _validationResult.AddError($"{generalValidationResult.Message}", "4965.1.2",
                    "/ErklaeringAnsvarsrett/eiendomByggested/eiendom/eiendomsidentifikasjon/kommunenummer");
            }

        }
        internal void AnsvarligForetakGyldigOrganisasjonsNr(ErklaeringAnsvarsrettType form)
        {
            if (form.ansvarsrett != null)
            {
                if (form.ansvarsrett.foretak != null)
                {
                    if (string.IsNullOrEmpty(form.ansvarsrett.foretak.organisasjonsnummer))
                        _validationResult.AddError("Må sendes av ansvarlig foretak med gyldig organisasjonsnummer", "4965.2.4", "ErklaeringAnsvarsrett/ansvarsrett/foretak/organisasjonsnummer");
                    else
                    {
                        var test = GeneralValidations.Validate_orgnummer(form.ansvarsrett.foretak.organisasjonsnummer);
                        if ((test.Status != ValidationStatus.Ok))
                            _validationResult.AddError("Må sendes av ansvarlig foretak med gyldig organisasjonsnummer", "4965.2.4", "ErklaeringAnsvarsrett/ansvarsrett/foretak/organisasjonsnummer");
                    }
                    if (string.IsNullOrEmpty(form.ansvarsrett.foretak.navn))
                        _validationResult.AddError("Må sendes av ansvarlig foretak med navn", "4965.2.4", "ErklaeringAnsvarsrett/ansvarsrett/foretak/navn");
                    if (form.ansvarsrett.foretak.adresse != null)
                    {
                        if (string.IsNullOrEmpty(form.ansvarsrett.foretak.adresse.poststed))
                            _validationResult.AddError("Må sendes av ansvarlig foretak med adresse", "4965.2.4", "ErklaeringAnsvarsrett/ansvarsrett/foretak/adresse/poststed");
                        if (string.IsNullOrEmpty(form.ansvarsrett.foretak.adresse.postnr))
                            _validationResult.AddError("Må sendes av ansvarlig foretak med adresse", "4965.2.4", "ErklaeringAnsvarsrett/ansvarsrett/foretak/adresse/postnr");
                    }
                    else
                    {
                        _validationResult.AddError("Må sendes av ansvarlig foretak med adresse", "4965.2.4", "ErklaeringAnsvarsrett/ansvarsrett/foretak/adresse");
                    }
                    if (form.ansvarsrett.foretak.partstype != null)
                    {
                        if (!_codeListService.IsCodelistValid("Partstype", form.ansvarsrett.foretak.partstype.kodeverdi))
                        {
                            _validationResult.AddError($"Kodeliste Partstype verdi {form.ansvarsrett.foretak.partstype.kodeverdi} er ugyldig", "4965.1.2", "ErklaeringAnsvarsrett/ansvarsrett/foretak/partstype/kodeverdi");
                        }
                    }
                    else
                    {
                        _validationResult.AddError("Ansvarlig foretak må ha partstype definert", "4965.1.1", "ErklaeringAnsvarsrett/ansvarsrett/foretak/partstype");
                    }


                }
                else
                {
                    _validationResult.AddError("Må sendes av ansvarlig foretak", "4965.2.4", "ErklaeringAnsvarsrett/ansvarsrett/foretak");
                }
            }
            else
            {
                _validationResult.AddError("Må sendes av ansvarlig foretak", "4965.2.4", "ErklaeringAnsvarsrett/ansvarsrett");
            }
        }

        internal void AnsvarligSoekerGyldigOrganisasjonsNr(ErklaeringAnsvarsrettType form)
        {
            if (string.IsNullOrEmpty(form.ansvarligSoeker.organisasjonsnummer))
                _validationResult.AddError(ValidationError.AnsvarligSoekerGyldigOrgNr, "4965.2.5", "ErklaeringAnsvarsrett/ansvarligSoeker/organisasjonsnummer");
            else
            {
                var test = GeneralValidations.Validate_orgnummer(form.ansvarligSoeker.organisasjonsnummer);
                if ((test.Status != ValidationStatus.Ok))
                    _validationResult.AddError(ValidationError.AnsvarligSoekerGyldigOrgNr, "4965.2.5", "ErklaeringAnsvarsrett/ansvarligSoeker/organisasjonsnummer");
            }

            if (form.ansvarligSoeker.adresse != null)
            {
                if (String.IsNullOrEmpty(form.ansvarligSoeker.adresse.poststed))
                {
                    _validationResult.AddError($"Må sendes til ansvarlig søker med gyldig poststed ", "4965.2.5", "/ErklaeringAnsvarsrett/ansvarligSoeker/adresse/poststed");
                }
                if (String.IsNullOrEmpty(form.ansvarligSoeker.adresse.postnr))
                {
                    _validationResult.AddError($"Må sendes til ansvarlig søker med gyldig postnr ", "4965.2.5", "/ErklaeringAnsvarsrett/ansvarligSoeker/adresse/postnr");
                }

                var generalValidationResult = GeneralValidations.Validate_postnummer(form.ansvarligSoeker.adresse.postnr, form.ansvarligSoeker.adresse.landkode);

                if (generalValidationResult?.Status == ValidationStatus.Fail)
                    _validationResult.AddError($"{generalValidationResult.Message} for ansvarlig søker", "4965.2.5", "/ErklaeringAnsvarsrett/ansvarligSoeker/adresse/postnr");
                if (generalValidationResult?.Status == ValidationStatus.Warning)
                    _validationResult.AddWarning($"{generalValidationResult.Message} for ansvarlig søker", "4965.2.5", "/ErklaeringAnsvarsrett/ansvarligSoeker/adresse/postnr");
            }
            else
            {
                _validationResult.AddWarning("Adresse bør fylles ut for ansvarlig søker", "4965.2.5", "/ErklaeringAnsvarsrett/ansvarligSoeker/adresse");
            }

            if (string.IsNullOrEmpty(form.ansvarligSoeker?.kontaktperson?.navn))
            {
                _validationResult.AddWarning(ValidationError.AnsvarligSoekerKontaktperson, "4965.2.6", "/ErklaeringAnsvarsrett/ansvarligSoeker/kontaktperson/navn");

            }
        }

    }
}
