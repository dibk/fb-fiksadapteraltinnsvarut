﻿using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;
using System;
using System.Linq;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.NotificationForm;
using System.Collections.Generic;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services
{
    /// <summary>
    /// Brukes i distribusjonstjenesten som preutfylt skjema og i retur som notifikasjonsskjema
    /// </summary>
    public class DistribusjonErklaeringAnsvarsrettFormV2: IAltinnForm, INotificationForm, IPrefillForm
    {
        private const string DataFormatId = "5699";
        private const string DataFormatVersion = "42465";
        private const string SchemaFile = "ansvarsrettVedDistribusjon.xsd";
        private const string Name = "Erklæring om ansvarsrett";

        private no.kxml.skjema.dibk.ansvarsrettVedDistribusjon.ErklaeringAnsvarsrettType form;

        private string _archiveReferenceOfDistribution = String.Empty;
        private string _altinnReferenceForPrefillFrom = String.Empty;


        private List<string> FormSetElements;
        public void SetFormSetElements(List<string> attachmentlist)
        {
            FormSetElements = attachmentlist;
        }

        public bool? ValidateAuthenticatedSubmitterVsFormData(string authenticatedSubmitter, ValidationResult validationResult)
        {
            bool isSubmitterValid = Helpers.ValidateEntityOfAltinnSubmitterDefault(authenticatedSubmitter, GetPropertyIdentifiers());

            if (!isSubmitterValid)
            {
                Helpers.RecordErrorAltinnSubmitter(validationResult);
            }
            return isSubmitterValid;
        }

        public ReporteeInformation GetReportee()
        {
            return Utils.Helpers.ReturnFirstOrgNumberOrPersonalNumber(
                        form.ansvarligSoeker.organisasjonsnummer,
                        form.ansvarligSoeker.foedselsnummer);
        }
        public string GetNotificationMessageTitle()
        {
            return $"Erklæring om ansvarsrett er signert fra {form.ansvarsrett.foretak.navn}";
        }
        public string GetNotificationMessageSender()
        {
            return form.ansvarsrett.foretak.navn;
        }
        public string GetNotificationMessageSummary()
        {
            return $"Melding er sendt via Fellestjenester Bygg fra Direktoratet for Byggkvalitet";
        }

        public string GetNotificationMessageBody()
        {
            return $"Vedlagt erklæring gjelder adresse {form.eiendomByggested.First().adresse.adresselinje1}, {form.eiendomByggested.First().adresse.postnr} {form.eiendomByggested.First().adresse.poststed}";
        }
        public PropertyIdentifiers GetPropertyIdentifiers()
        {
            PropertyIdentifiers propertyIdentifiers = new PropertyIdentifiers();

            propertyIdentifiers.Kommunenummer = form.eiendomByggested[0].eiendomsidentifikasjon.kommunenummer;
            propertyIdentifiers.Gaardsnummer = form.eiendomByggested[0].eiendomsidentifikasjon.gaardsnummer;
            propertyIdentifiers.Bruksnummer = form.eiendomByggested[0].eiendomsidentifikasjon.bruksnummer;
            propertyIdentifiers.Festenummer = form.eiendomByggested[0].eiendomsidentifikasjon.festenummer;
            propertyIdentifiers.Seksjonsnummer = form.eiendomByggested[0].eiendomsidentifikasjon.seksjonsnummer;
            propertyIdentifiers.Adresselinje1 = form.eiendomByggested[0].adresse.adresselinje1;
            propertyIdentifiers.Adresselinje2 = form.eiendomByggested[0].adresse.adresselinje2;
            propertyIdentifiers.Adresselinje3 = form.eiendomByggested[0].adresse.adresselinje3;
            propertyIdentifiers.Postnr = form.eiendomByggested[0].adresse.postnr;
            propertyIdentifiers.Poststed = form.eiendomByggested[0].adresse.poststed;
            propertyIdentifiers.Landkode = form.eiendomByggested[0].adresse.landkode;
            propertyIdentifiers.Bygningsnummer = form.eiendomByggested[0].bygningsnummer;
            propertyIdentifiers.Bolignummer = form.eiendomByggested[0].bolignummer;
            propertyIdentifiers.SoeknadSkjemaNavn = Name;
            propertyIdentifiers.FraSluttbrukersystem = form.fraSluttbrukersystem;

            propertyIdentifiers.KommunensSaksnummerAar = form.kommunensSaksnummer.saksaar;
            propertyIdentifiers.KommunensSaksnummerSekvensnummer = form.kommunensSaksnummer.sakssekvensnummer;

            return propertyIdentifiers;
        }


        public string GetDataFormatId()
        {
            return DataFormatId;
        }

        public string GetDataFormatVersion()
        {
            return DataFormatVersion;
        }

        public string GetMunicipalityCode()
        {
            return GetPropertyIdentifiers().Kommunenummer;
        }

        public string GetSchemaFile()
        {
            return SchemaFile;
        }

        public string GetName()
        {
            return Name;
        }

        public void InitiateForm(string formDataAsXml)
        {
            form = Utils.SerializeUtil.DeserializeFromString<no.kxml.skjema.dibk.ansvarsrettVedDistribusjon.ErklaeringAnsvarsrettType>(formDataAsXml);
        }

        public ValidationResult ValidateData()
        {
            return new DistribusjonErklaeringAnsvarsrettV2Validator().Validate(form);
        }

        public string GetSendToReporteeId()
        {
            return Utils.Helpers.ReturnFirstOrgNumberOrPersonalNumber(
                        form.ansvarligSoeker.organisasjonsnummer,
                        form.ansvarligSoeker.foedselsnummer).IdNumber;
        }

        public bool IsReply()
        {
            Guid incomingGuid;
            return Guid.TryParse(form.hovedinnsendingsnummer, out incomingGuid);
        }

        public Guid GetReplyKey()
        {
            Guid incomingGuid;
            var isReply = Guid.TryParse(form.hovedinnsendingsnummer, out incomingGuid);
            return incomingGuid;
        }

        public int GetReplyDueDays()
        {
            return 0;
        }
        public string GetNotificationXMLFilename()
        {
            return Resources.TextStrings.NotificationAnsvarsrettXMLFilename;
        }

        public string GetNotificationXMLTitle()
        {
            return Resources.TextStrings.NotificationAnsvarsrettXMLTitle;
        }

        public string GetNotificationPdfFilename()
        {
            return Resources.TextStrings.NotificationAnsvarsrettPdfFilename;
        }

        public string GetNotificationPdfTitle()
        {
            return Resources.TextStrings.NotificationAnsvarsrettPdfTitle;
        }

        public string GetPrefillSendToReporteeId()
        {
            return Utils.Helpers.ReturnFirstOrgNumberOrPersonalNumber(
                       form.ansvarsrett.foretak.organisasjonsnummer,
                       form.ansvarsrett.foretak.foedselsnummer).IdNumber;
        }

        public string GetFormXML()
        {
            return Utils.SerializeUtil.Serialize(form);
        }

        public string GetPrefillServiceCode()
        {
            return "4419";
        }

        public string GetPrefillServiceEditionCode()
        {
            return "3";
        }

        public string GetPrefillNotificationTitle()
        {
            return $"Forespørsel om erklæring av ansvarsrett for tiltak i {form.eiendomByggested.First().adresse.adresselinje1} fra {form.ansvarligSoeker.navn}";
        }

        public string GetPrefillNotificationSummary()
        {
            return $"Melding er sendt via Fellestjenester Bygg fra Direktoratet for Byggkvalitet";
        }

        public string GetPrefillNotificationBody(bool? pdf, string replyLink = "")
        {
            string body = $"Vedlagt erklæring gjelder adresse {form.eiendomByggested.First().adresse.adresselinje1}, {form.eiendomByggested.First().adresse.postnr} {form.eiendomByggested.First().adresse.poststed}<br>";
            body = body + $" Ansvarlig søker er {form.ansvarligSoeker.navn}<br>" ;
            body = body + $" Du kan erklære ansvarsrett med vedlagte svarskjema til ansvarlig søker som sender samlet til kommunen";
            return body;
        }

        public int GetPrefillNotificationDueDays()
        {
            return 0;
        }

        public void SetPrefillKey(string key)
        {
            form.hovedinnsendingsnummer = key;
        }

        public string GetPrefillKey()
        {
            return form.hovedinnsendingsnummer;
        }

        public string GetPrefillLinkText()
        {
            return Resources.TextStrings.AnsvarsrettLinkText;
        }

        public string GetPrefillMessageSender()
        {
            return form.ansvarligSoeker.navn;
        }

        public string GetPrefillOurReference()
        {
            return form.ansvarsrett.ansvarsomraader.First().vaarReferanse;
        }

        public bool DoEmailNotification()
        {
            if (_emailnotification != null) return true;
            else return false;
        }

        public void SetEmailNotification(string email, string subject, string emailcontent, string smscontent)
        {
            _emailnotification = new EmailNotification();
            _emailnotification.Email = email;
            _emailnotification.EmailSubject = subject;
            _emailnotification.EmailContent = emailcontent;
            _emailnotification.SmsContent = smscontent;
        }
        private EmailNotification _emailnotification;
        public EmailNotification GetEmailNotification()
        {
            return _emailnotification;
        }


        public string GetArchiveReferenceOfDistribution()
        {
            return _archiveReferenceOfDistribution;
        }

        public void SetArchiveReferenceOfDistribution(string ar)
        {
            _archiveReferenceOfDistribution = ar;
        }

        public string GetAltinnReferenceForPrefillFrom()
        {
            return _altinnReferenceForPrefillFrom;
        }

        public void SetAltinnReferenceForPrefillFrom(string altinnReference)
        {
            _altinnReferenceForPrefillFrom = altinnReference;
        }

        public NotificationEnums.NotificationCarrier GetNotificationType()
        {
            return NotificationEnums.NotificationCarrier.EmailFromDistributionOrAltinnWhenInvalidEmailAddress;
        }

        public string GetAltinnNotificationTemplate()
        {
            return Resources.TextStrings.AltinnNotificationTemplate;
        }

        public NotificationEnums.NotificationChannel GetNotificationChannel()
        {
            return NotificationEnums.NotificationChannel.Correspondence;
        }

        public bool ConfigureSecondNotification()
        {
            return false;
        }

        private DateTime? _replyDeadline;

        public IEnumerable<CorrespondenceAttachmentsType> CorrespondenceAttachmentsTypes => new List<CorrespondenceAttachmentsType>()
        {
            CorrespondenceAttachmentsType.MainFormPdf, 
            CorrespondenceAttachmentsType.MainFormXml, 
            CorrespondenceAttachmentsType.Attachments
        };

        public IEnumerable<StoreToBlob> StoreToBlobs => new List<StoreToBlob>()
        {
            StoreToBlob.MainFormPdf, 
            StoreToBlob.MainFormXml, 
            StoreToBlob.Attachments
        };

        public void SetReplyDeadline(DateTime? ReplyDeadline) {
            _replyDeadline = ReplyDeadline;
        }

        public DateTime? GetReplyDeadline() {
            return _replyDeadline;
        }
        public bool IsStoreMessageFile()
        {
            return false;
        }
    }
}