﻿using System.Linq;
using DIBK.FBygg.Altinn.MapperCodelist;
using KommIT.FIKS.AdapterAltinnSvarUt.Resources;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Validations;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.NotificationForm
{
    /// <summary>
    /// Validator for Erklæring om ansvarsett til bruk i distribusjonstjeneste.
    /// </summary>
    public class DistribusjonErklaeringAnsvarsrettV2Validator
    {
        private ValidationResult _validationResult;
        private string _skjema = "ErklaeringAnsvarsrett";
        CodeListService _codeListService = new CodeListService();



        public DistribusjonErklaeringAnsvarsrettV2Validator()
        {
            _validationResult = new ValidationResult();
            _validationResult.AddRule("4419.1.1", "Minst et ansvarsområde må være definert.", "", "ERROR", "");
            _validationResult.AddRule("4419.1.1", "Minst en eiendom må være definert.", "", "ERROR", "");
            _validationResult.AddRule("4419.1.6", ValidationError.GyldigForetakPartstype, "", "ERROR", "");
            _validationResult.AddRule("4419.1.7", ValidationError.UgyldigPartstype, "", "ERROR", "");
            _validationResult.AddRule("4419.1.8", ValidationError.AnsvarligSoekerKontaktperson, "", "WARNING", "");

        }

        public ValidationResult Validate(no.kxml.skjema.dibk.ansvarsrettVedDistribusjon.ErklaeringAnsvarsrettType form)
        {


            MinstEnEiendom(form);

            MinstEtAnsvarsomraade(form);

            AnsvarligSokerValidering(form);

            return _validationResult;
        }

        public void MinstEtAnsvarsomraade(no.kxml.skjema.dibk.ansvarsrettVedDistribusjon.ErklaeringAnsvarsrettType form)
        {
            if (form.ansvarsrett == null || form.ansvarsrett.ansvarsomraader == null ||
                !form.ansvarsrett.ansvarsomraader.Any())
                _validationResult.AddError(ValidationError.MinstEtAnsvarsomraade, "4419.1.1",
                    "/ErklaeringAnsvarsrett/ansvarsrett/ansvarsomraader");
        }

        public void MinstEnEiendom(no.kxml.skjema.dibk.ansvarsrettVedDistribusjon.ErklaeringAnsvarsrettType form)
        {
            if (form.eiendomByggested == null || !form.eiendomByggested.Any())
                _validationResult.AddError(ValidationError.MinstEnEiendom, "4419.1.1",
                    "/ErklaeringAnsvarsrett/eiendomByggested");
        }

        public void AnsvarligSokerValidering(no.kxml.skjema.dibk.ansvarsrettVedDistribusjon.ErklaeringAnsvarsrettType form)
        {
            if (form.ansvarligSoeker?.partstype != null)
            {
                if (!_codeListService.IsCodelistValid("Partstype", form.ansvarligSoeker.partstype.kodeverdi))
                {
                    _validationResult.AddMessage("4419.1.7", Helpers.GetFullClassPath(() => form.ansvarligSoeker.partstype), _skjema, new[] { form.ansvarligSoeker.partstype.kodeverdi });
                }
                else
                {
                    if (form.ansvarligSoeker.partstype.kodeverdi == "Privatperson")
                    {
                        GeneralValidationResult generalValidationResult = GeneralValidations.Validate_fodselsnummer(form.ansvarligSoeker.foedselsnummer);

                        if (generalValidationResult.Status != ValidationStatus.Ok)
                        {
                            //_validationResult.AddMessage("4419.1.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.foedselsnummer), _skjema, new[] { generalValidationResult.Message });
                        }
                    }
                    else
                    {
                        GeneralValidationResult generalValidationResult = GeneralValidations.Validate_orgnummer(form.ansvarligSoeker.organisasjonsnummer);

                        if (generalValidationResult.Status != ValidationStatus.Ok)
                        {
                            //_validationResult.AddMessage("4419.1.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.organisasjonsnummer), _skjema, new[] { generalValidationResult.Message });
                        }

                        if (string.IsNullOrEmpty(form.ansvarligSoeker.kontaktperson?.navn))
                            _validationResult.AddMessage("4419.1.8", Helpers.GetFullClassPath(() => form.ansvarligSoeker.kontaktperson.navn), _skjema);
                    }
                }
            }
            else
            {
                _validationResult.AddMessage("4419.1.6", Helpers.GetFullClassPath(() => form.ansvarligSoeker.partstype),_skjema);
            }
        }

        public ValidationResult GetResult()
        {
            return _validationResult;
        }
    }
}