﻿using System;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services
{
    public class EmailNotification
    {
        public string Email;
        public string EmailSubject;
        public string EmailContent;
        public string SmsContent;
    }
}