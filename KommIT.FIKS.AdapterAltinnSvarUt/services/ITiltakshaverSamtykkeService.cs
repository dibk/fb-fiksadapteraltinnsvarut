﻿using KommIT.FIKS.AdapterAltinnSvarUt.Models.ApiModels;
using KommIT.FIKS.AdapterAltinnSvarUt.Models.Signatur;
using System;
using System.Collections.Generic;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services
{
    public interface ITiltakshaverSamtykkeService
    {
        Guid AddNew(SamtykkeTiltakshaverNy ny);
        void AddVedlegg(Guid samtykkeId, int vedleggId, string navn, string vedleggstype, string storageUri, long fileSize);
        void AddVedlegg(string samtykkeId, int vedleggId, string navn, string vedleggstype, string storageUri, long fileSize);
        int PrepareVedleggRow(Guid samtykkeId);
        TiltakshaversSamtykkeVedlegg GetVedlegg(string samtykkeId, int vedleggId);
        TiltakshaversSamtykkeVedlegg GetVedlegg(Guid samtykkeId, int vedleggId);
        bool DeleteVedlegg(Guid samtykkeId, int vedleggId);
        TiltakshaversSamtykke GetTiltakshaversSamtykke(string samtykkeId);
        TiltakshaversSamtykke GetTiltakshaversSamtykke(Guid samtykkeId);
        TiltakshaversSamtykkeTiltaktype GetTiltakstype(SamtykkeTiltakshaverNyTiltaktype nyTiltaktype);
        bool SamtykkeExists(Guid samtykkeId);
        bool SamtykkeExists(string samtykkeId);
        void SaveReporteeToSamtykke(string archivereference, string reportee);
        void SamtykkeGodkjent(string samtykkeId, string altinnArkivreferanse, string tiltakshaversOrganisasjonsnummer);
        void SamtykkeGodkjent(Guid samtykkeId, string altinnArkivreferanse, string tiltakshaversOrganisasjonsnummer);
        void SamtykkeSignert(string samtykkeId);
        void ArchiveSamtykke(Guid samtykkeId);
        void ArchiveSamtykke(string samtykkeId);
        string GetSamtykkeTiltakshaverId(string archiveReference);
        string GetReporteePersonalNumberFromSamtykke(Guid samtykkeId);
        void UpdateVedleggStatus(string samtykkeId, bool uploaded);
        bool PersonalNumberExistForTiltakshaverId(Guid samtykkeId);
        List<string> GetCandidatesForDeletion(int deleteAfterDaysLimit = 365);
        void MarkAsDeleted(string samtykkeId);
    }
}