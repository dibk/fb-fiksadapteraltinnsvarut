﻿using System.Collections.Generic;
using System.IO;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Azure
{
    public class FileStorage : IFileStorage
    {
        private readonly BlobStorage _blobStorage;

        public FileStorage(BlobStorage blobStorage)
        {
            _blobStorage = blobStorage;
        }

        public string AddFileAsByteArrayToFolder(string folderName, string fileName, byte[] fileBuffer, string mimetype)
        {
            return AddFileAsByteArrayToFolder(folderName, fileName, fileBuffer, mimetype, null);
        }

        public virtual string AddFileAsByteArrayToFolder(string folderName, string fileName, byte[] fileBuffer, string mimetype, IEnumerable<MetadataItem> metadataItems)
        {
            if (!IsFolderPresent(folderName))
                _blobStorage.SetUpBlobContainer(folderName);

            var uri = _blobStorage.AddFileAsByteArrayToContainer(folderName, fileName, fileBuffer, mimetype, metadataItems);
            
            return uri;
        }

        public string AddFileAsStreamToFolder(string folderName, string fileName, Stream filestream, string mimetype, IEnumerable<MetadataItem> metadataItems = null)
        {
            if (!IsFolderPresent(folderName))
                _blobStorage.SetUpBlobContainer(folderName);

            var uri = _blobStorage.AddFileAsStreamToContainer(folderName, fileName, filestream, mimetype, metadataItems);
            
            return uri;
        }

        public void EstablishFolder(string folderName, string metadataArchiveReference)
        {
            _blobStorage.SetUpBlobContainer(folderName);
        }

        public bool IsFolderPresent(string folderName)
        {
            var cloudBlobContainer = _blobStorage.GetContainerClient(folderName);
            if (cloudBlobContainer != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public byte[] GetFile(string containerName, string fileName)
        {
            return _blobStorage.GetFileFromBlob(containerName, fileName);
        }

        public void StreamFile(string folderName, string fileName, Stream outStream)
        {
            _blobStorage.StreamFileFromBlob(folderName, fileName, outStream);
        }

        public void StreamFile(string uri, Stream outStream)
        {
            _blobStorage.StreamFileFromBlob(uri, outStream);
        }

        public bool DeleteContainer(string containerName)
        {
            return _blobStorage.DeleteContainer(containerName);
        }

        public bool DeleteFile(string container, string fileName)
        {
            return _blobStorage.DeleteBlob(container, fileName);
        }

        public void StreamFileFromLocation(string fileUri, Stream stream)
        {
            _blobStorage.StreamFileFromBlob(fileUri, stream);
        }

        public bool DeleteFileFromLocation(string fileUri)
        {
            return _blobStorage.DeleteBlobFromUri(fileUri);
        }
    }
}