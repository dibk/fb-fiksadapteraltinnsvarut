﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Azure
{
    public enum BlobStorageMetadataTypeEnum
    {
        ArchivedItemInformation,
        MainForm,
        FormData,
        Subform,
        SubmittalAttachment,
        ValidationResult,
        AltinnFormTaskQueueItemForm,
        FormSupplementation
    }
}