﻿using System.Collections.Generic;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Azure
{
    public class PublicFileStorage : FileStorage, IPublicFileStorage
    {
        private readonly PublicBlobStorage _publicBlobStorage;

        public PublicFileStorage(PublicBlobStorage publicBlobStorage) : base(publicBlobStorage)
        {
            _publicBlobStorage = publicBlobStorage;
        }

        public override string AddFileAsByteArrayToFolder(string folderName, string fileName, byte[] fileBuffer, string mimetype, IEnumerable<MetadataItem> metadataItems)
        {
            var uri = _publicBlobStorage.AddFileAsByteArrayToPublicContainer(folderName, fileName, fileBuffer, mimetype, metadataItems);

            return uri;
        }
        public void AddMetadataToContainer(string containerName, IEnumerable<MetadataItem> metadataItems)
        {
            _publicBlobStorage.AddMetadataToContainer(containerName, metadataItems);
        }
    }
}