﻿using Azure;
using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Models;
using Serilog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using Azure.Core;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Azure
{
    public class BlobStorage
    {
        protected string AzureBlobConnectionString;
        public ILogger Logger { private get; set; }

        public BlobStorage()
        {
            AzureBlobConnectionString = ConfigurationManager.AppSettings["StorageConnectionString"];
        }

        private static BlobClientOptions BlobClientOptions => new BlobClientOptions
        {
            Diagnostics =
            {
                IsLoggingEnabled = false,
                IsTelemetryEnabled = false,
                IsDistributedTracingEnabled = false
            },
            Retry =
            {
                MaxRetries = 3, 
                Mode = RetryMode.Exponential
            }
        };

        public BlobContainerClient GetContainerClient(string containerName)
        {
            return new BlobContainerClient(AzureBlobConnectionString, containerName, BlobClientOptions);
        }

        private BlobClient GetBlobClient(string containerName, string blobName)
        {
            return new BlobClient(AzureBlobConnectionString, containerName, blobName, BlobClientOptions);
        }

        private BlobClient GetBlobClient(string fileUrl)
        {
            var b = new BlobClient(new Uri(fileUrl));            
            return new BlobClient(AzureBlobConnectionString, b.BlobContainerName, b.Name, BlobClientOptions);
        }

        public BlobContainerClient SetUpBlobContainer(string containerName)
        {
            var client = GetContainerClient(containerName);

            try
            {
                var metaData = new Dictionary<string, string>();
                metaData.Add("DateCreated", DateTime.Now.ToString("yyyyMMdd"));
                client.CreateIfNotExists(PublicAccessType.None, metaData);
            }
            catch (RequestFailedException e)
            {
                Logger.Fatal($"Error setting up Blob contatiner: {e.Message}, {e.ToString()}");
                throw;
            }

            return client;
        }

        public string AddFileAsByteArrayToContainer(string containerName, string fileName, byte[] fileBytes, string mimeType, IEnumerable<MetadataItem> metadataItems = null)
        {
            SetUpBlobContainer(containerName);

            var blobClient = GetBlobClient(containerName, fileName);

            var options = new BlobUploadOptions
            {
                HttpHeaders = new BlobHttpHeaders() { ContentType = mimeType },
                Metadata = ConvertMetadataItems(metadataItems)
            };

            var content = new BinaryData(fileBytes);
            var result = blobClient.Upload(content, options);

            return blobClient.Uri.ToString();
        }

        public string AddFileAsStreamToContainer(string containerName, string fileName, Stream fileStream, string mimeType, IEnumerable<MetadataItem> metadataItems = null)
        {
            SetUpBlobContainer(containerName);

            var blobClient = GetBlobClient(containerName, fileName);

            var options = new BlobUploadOptions
            {
                HttpHeaders = new BlobHttpHeaders() { ContentType = mimeType },
                Metadata = ConvertMetadataItems(metadataItems)
            };

            var result = blobClient.Upload(fileStream, options);

            return blobClient.Uri.ToString();
        }

        public void StreamFileFromBlobUri(string fileUri, Stream stream)
        {
            var blobClient = GetBlobClient(fileUri);
            blobClient.DownloadTo(stream);            
        }

        private static IDictionary<string, string> ConvertMetadataItems(IEnumerable<MetadataItem> metadataItems)
        {
            Dictionary<string, string> metadata = null;
            if (metadataItems != null)
            {
                metadata = new Dictionary<string, string>();

                foreach (var metadataItem in metadataItems)
                {
                    metadata.Add(metadataItem.Key, metadataItem.Value);
                }
            }

            return metadata;
        }

        public byte[] GetFileFromBlob(string containerName, string fileName)
        {
            var blob = GetBlobClient(containerName, fileName);
            using (var ms = new MemoryStream())
            {
                blob.DownloadTo(ms);
                return ms.ToArray();
            }
        }

        public byte[] GetBlobContentAsBytes(string containerName, BlobStorageMetadataTypeEnum blobStorageMetadataTypeEnum)
        {
            string blobName = GetBlobName(containerName, blobStorageMetadataTypeEnum);
            if (GetBlobName(containerName, blobStorageMetadataTypeEnum) != null)
            {
                return GetFileFromBlob(containerName, blobName);
            }
            else
            {
                throw new ArgumentOutOfRangeException($"Error: No blob of type {Enum.GetName(typeof(BlobStorageMetadataTypeEnum), blobStorageMetadataTypeEnum)} found in container {containerName}.");
            }
        }
        public string GetBlobContentAsString(string containerName, BlobStorageMetadataTypeEnum blobStorageMetadataTypeEnum)
        {
            string blobName = GetBlobName(containerName, blobStorageMetadataTypeEnum);
            if (GetBlobName(containerName, blobStorageMetadataTypeEnum) != null)
            {
                return GetBlobContentAsString(containerName, blobName);
            }
            else
            {
                throw new ArgumentOutOfRangeException($"Error: No blob of type {Enum.GetName(typeof(BlobStorageMetadataTypeEnum), blobStorageMetadataTypeEnum)} found in container {containerName}.");
            }
        }

        private string GetBlobContentAsString(string containerName, string blobName)
        {
            var blob = GetBlobClient(containerName.ToLower(), blobName);

            var response = blob.Download();
            string blobStringContent = null;
            using (var streamReader = new StreamReader(response.Value.Content, Encoding.UTF8))
            {
                blobStringContent = streamReader.ReadToEnd();
            }

            return blobStringContent;
        }

        public void StreamFileFromBlob(string uri, Stream outStream)
        {
            var tempClient = new BlobClient(new Uri(uri));
            var containerName = tempClient.BlobContainerName;
            var blobName = tempClient.Name;
            var blobClient = new BlobClient(AzureBlobConnectionString, containerName, blobName);

            blobClient.DownloadTo(outStream);
        }

        public void StreamFileFromBlob(string containerName, string fileName, Stream outStream)
        {
            var blob = GetBlobClient(containerName, fileName);

            blob.DownloadTo(outStream);
        }
        

        private string GetBlobName(string containerName, BlobStorageMetadataTypeEnum blobStorageMetadataTypeEnum)
        {
            var cloudBlobContainer = GetContainerClient(containerName);
            var blobs = cloudBlobContainer.GetBlobs(BlobTraits.Metadata).ToList();

            return blobs.Where(blob => blob.Metadata.Any(meta => meta.Key.Equals("type", StringComparison.OrdinalIgnoreCase) &&
                                                                 meta.Value.Equals(Enum.GetName(typeof(BlobStorageMetadataTypeEnum), blobStorageMetadataTypeEnum)))
            ).Select(field => field.Name).FirstOrDefault();
        }

        public bool DeleteContainer(string containerName)
        {
            var container = GetContainerClient(containerName);
            return container.DeleteIfExists();
        }

        public bool DeleteBlob(string containerName, string fileName)
        {
            var blobClient = GetBlobClient(containerName, fileName);
            return blobClient.DeleteIfExists();
        }

        public bool DeleteBlobFromUri(string fileUri)
        {
            var blobClient = GetBlobClient(fileUri);
            return blobClient.DeleteIfExists();
        }
    }
}