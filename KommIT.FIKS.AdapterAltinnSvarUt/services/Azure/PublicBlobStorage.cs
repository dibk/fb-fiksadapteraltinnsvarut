﻿using Azure.Storage.Blobs.Models;
using System;
using System.Collections.Generic;
using System.Configuration;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Azure
{
    public class PublicBlobStorage : BlobStorage
    {
        public PublicBlobStorage()
        {
            AzureBlobConnectionString = ConfigurationManager.AppSettings["PublicStorageConnectionString"];
        }

        public string AddFileAsByteArrayToPublicContainer(string containerName, string fileName,
            byte[] fileBytes, string mimeType, IEnumerable<MetadataItem> metadataItems)
        {
            var containerClient = GetContainerClient(containerName);
            containerClient.CreateIfNotExists();
            containerClient.SetAccessPolicy(accessType: PublicAccessType.Blob);

            var blobClient = containerClient.GetBlobClient(fileName);

            var options = new BlobUploadOptions()
            {
                HttpHeaders = new BlobHttpHeaders() { ContentType = mimeType },
            };

            if (metadataItems != null)
            {
                var metadata = new Dictionary<string, string>();
                foreach (var metadataItem in metadataItems)
                {
                    metadata.Add(metadataItem.Key, metadataItem.Value);
                }

                options.Metadata = metadata;
            }

            var content = new BinaryData(fileBytes);
            var response = blobClient.Upload(content, options);

            return blobClient.Uri.ToString();
        }

        public void AddMetadataToContainer(string containerName, IEnumerable<MetadataItem> metadataItems)
        {
            var container = GetContainerClient(containerName);
            var metadata = container.GetProperties().Value.Metadata;
            foreach (var metadataItem in metadataItems)
            {
                if (!metadata.ContainsKey(metadataItem.Key))
                    metadata.Add(metadataItem.Key, metadataItem.Value);
                else
                    metadata[metadataItem.Key] = metadataItem.Value;
            }
            container.SetMetadata(metadata);
        }
    }
}