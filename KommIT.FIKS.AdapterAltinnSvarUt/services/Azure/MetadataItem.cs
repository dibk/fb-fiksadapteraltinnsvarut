﻿using System.Collections.Generic;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Azure
{
    public class MetadataItem
    {
        public string Key { get; set; }
        public string Value { get; set; }

        public KeyValuePair<string, string> ToKVP()
        {
            return new KeyValuePair<string, string>(Key, Value);
        }
    }
}