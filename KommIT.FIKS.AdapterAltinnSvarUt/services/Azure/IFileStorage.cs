﻿using System.Collections.Generic;
using System.IO;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Azure
{
    public interface IFileStorage
    {
        void EstablishFolder(string folderName, string metadataArchiveReference);
        string AddFileAsByteArrayToFolder(string folderName, string fileName, byte[] fileBuffer, string mimetype);
        string AddFileAsByteArrayToFolder(string folderName, string fileName, byte[] fileBuffer, string mimetype, IEnumerable<MetadataItem> metadataItem);
        string AddFileAsStreamToFolder(string folderName, string fileName, Stream fileStream, string mimetype, IEnumerable<MetadataItem> metadataItems = null);
        bool IsFolderPresent(string folderName);
        void StreamFile(string folderName, string fileName, Stream outStream);
        void StreamFile(string uri, Stream outStream);
        bool DeleteContainer(string containerName);
        bool DeleteFile(string container, string fileName);
        byte[] GetFile(string containerName, string fileName);
        void StreamFileFromLocation(string fileUri, Stream stream);
        bool DeleteFileFromLocation(string referanse);
    }
}
