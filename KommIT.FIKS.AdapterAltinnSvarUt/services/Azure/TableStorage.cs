﻿using Azure;
using Azure.Data.Tables;
using System;
using System.Configuration;
using System.Linq;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Azure
{
    public class TableStorage<T> : ITableStorage<T> where T : class, ITableEntity, new()
    {
        private readonly string _azureBlobConnectionString;
        public TableStorage()
        {
            _azureBlobConnectionString = ConfigurationManager.AppSettings["StorageConnectionString"];
        }
        public T Get(string partitionKey, string rowKey)
        {
            string tableNameFromType = GetTableName();
            var client = new TableClient(_azureBlobConnectionString, tableNameFromType);
            var result = client.Query<T>(p => p.PartitionKey == partitionKey.ToLower() && p.RowKey == rowKey.ToLower());

            return result.FirstOrDefault();
        }

        private string GetTableName()
        {
            if (typeof(T) == typeof(DistributionSubmittalEntity))
                return "ftbDistributionSubmittal";
            else
                throw new Exception("Illegal table storage name");

        }
    }

    public class DistributionSubmittalEntity : ITableEntity
    {
        public string PartitionKey { get; set; }
        public string RowKey { get; set; }
        public DateTimeOffset? Timestamp { get; set; }
        public ETag ETag { get; set; }
        public string SenderId { get; set; }
        public DateTime ReplyDeadline { get; set; }
        public int ReceiverCount { get; set; }
        public DateTime CreatedTimeStamp { get; set; }
        public string Status { get; set; }
    }
}