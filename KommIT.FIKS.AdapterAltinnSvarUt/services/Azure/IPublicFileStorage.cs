﻿using System.Collections.Generic;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Azure
{
    public interface IPublicFileStorage
    {
        void EstablishFolder(string folderName, string metadataArchiveReference);
        string AddFileAsByteArrayToFolder(string folderName, string fileName, byte[] fileBuffer, string mimetype, IEnumerable<MetadataItem> metadataItems);
        void AddMetadataToContainer(string containerName, IEnumerable<MetadataItem> metadataItems);
    }
}
