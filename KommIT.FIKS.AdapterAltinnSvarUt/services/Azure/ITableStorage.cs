﻿using Azure.Data.Tables;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Azure
{
    public interface ITableStorage<T> where T : ITableEntity
    {
        T Get(string partitionKey, string rowKey);
    }
}