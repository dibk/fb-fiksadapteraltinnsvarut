﻿using Hangfire;
using KommIT.FIKS.AdapterAltinnSvarUt.HangfireConfiguration;
using KommIT.FIKS.AdapterAltinnSvarUt.Logger;
using Serilog;
using System;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.MunicipalityApi
{
    public class MunicipalityMemoryCacheRefreshScheduler
    {
        private MunicipalityCacheService _municipalityCacheService;        

        public MunicipalityMemoryCacheRefreshScheduler(MunicipalityCacheService municipalityCacheService)
        {
            _municipalityCacheService = municipalityCacheService;
        }

        [HangfireBasicJobLogger]
        public void RefreshMunicipalityCache()
        {
            _municipalityCacheService.RefreshMunicipalityCache();
        }

        public void ScheduleMunicipalityMemoryCacheRefreshScheduler()
        {
            Log.Logger.Debug("Schedules caching of code lists");
            //Initial loading of code lists into cache
            BackgroundJob.Schedule(() => RefreshMunicipalityCache(), TimeSpan.FromSeconds(10));

            //Scheduling refresh of cache each # hours
            RecurringJob.AddOrUpdate(() => RefreshMunicipalityCache(), CronExpressions.HourInterval(6));
        }
    }
}