﻿using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Caching;
using Serilog;
using System;
using System.Linq;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.MunicipalityApi
{
    public class MunicipalityCacheService
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly ILogger logger = Log.ForContext<MunicipalityCacheService>();
        public MunicipalityCacheService(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void RefreshMunicipalityCache()
        {
            var municipalities = _dbContext.Municipalities.ToList();
            foreach (var item in municipalities)
            {
                if (!string.IsNullOrEmpty(item?.Code))
                    MemoryCacheService.Instance.AddOrReplaceCachedItem($"Municipality-{item.Code}", item, DateTime.Now.AddHours(12));
            }
        }

        public Municipality GetMunicipality(string municipalityLookupValue)
        {
            var cacheService = MemoryCacheService.Instance;
            var cachedItem = cacheService.GetObject($"Municipality-{municipalityLookupValue}") as Municipality;
            if (cachedItem != null)
            {
                logger.Verbose("Municipality item found in cache {municipalityCode}", municipalityLookupValue);
                return cachedItem;
            }

            var municipality = _dbContext.Municipalities.Find(municipalityLookupValue);

            if(municipality != null)
                cacheService.AddOrReplaceCachedItem($"Municipality-{municipalityLookupValue}", municipality, DateTime.Now.AddHours(12));

            return municipality;
        }
    }
}