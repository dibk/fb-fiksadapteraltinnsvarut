﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using Nest;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.SearchEngine
{
    [ElasticsearchType(IdProperty = nameof(Id))]
    public class ApplicationLogDocument
    {
        public int Id { get; set; }
        public DateTime Timestamp { get; set; }
        public string SequenceTag { get; set; }
        public string Message { get; set; }
        public string Type { get; set; }
        public string Reference { get; set; }
        public string DataFormatId { get; set; }
        public string DataFormatVersion { get; set; }
        public string CodeIdentifier { get; set; }
        public string Trace { get; set; }
        public string ProcessLabelId { get; set; }
        public long? EventDuration { get; set; }

        public ApplicationLogDocument(){}

        public ApplicationLogDocument(ApplicationLog applicationLog)
        {
            Id = applicationLog.Id;
            Timestamp = applicationLog.Timestamp;
            SequenceTag = applicationLog.SequenceTag;
            Message = applicationLog.Message;
            Type = applicationLog.Type;
            Reference = applicationLog.Reference;
            DataFormatId = applicationLog.DataFormatId;
            DataFormatVersion = applicationLog.DataFormatVersion;
            CodeIdentifier = applicationLog.CodeIdentifier;
            Trace = applicationLog.Trace;
            ProcessLabelId = applicationLog.ProcessLabelId;
            EventDuration = applicationLog.EventDuration;
        }

        public static IEnumerable<ApplicationLogDocument> ToList(List<ApplicationLog> applicationLogs)
        {
            return applicationLogs.Select(entry => new ApplicationLogDocument(entry)).ToList();
        }

    }
}