using System;
using System.Configuration;
using Nest;
using Serilog;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.SearchEngine
{
    public class ElasticUtil
    {
        public const string IndexNameForms = "forms";
        public const string IndexNameLogEntries = "logentries";
        public const string IndexNameDistributionForms = "distributionforms";
        public const string IndexNameApplicationLog = "applicationlog";
        public const string IndexNameSvarUtForsendelseStatus = "svarutforsendelsestatus";
        public const string IndexNameSubforms = "subforms";
        public const string IndexNameSoeknadsData = "soeknadsdata";

        /// <summary>
        /// Return a new instance of the ElasticClient.
        /// </summary>
        /// <param name="indexName">Name of the index to use as default in all operations.</param>
        /// <returns></returns>
        public static ElasticClient GetClient(string indexName)
        {
            var url = ConfigurationManager.AppSettings["SearchEngine:ConnectionUrl"];

            Log.ForContext<ElasticUtil>().Verbose("Creating new ElasticClient with url {ElasticUrl}", url);
            
            ConnectionSettings settings = new ConnectionSettings(new Uri(url))
                .DefaultIndex(indexName)
                .DisableDirectStreaming() // enable debugging information of request queries
                .ThrowExceptions(); 

            string username = ConfigurationManager.AppSettings["SearchEngine:ConnectionUsername"];
            string password = ConfigurationManager.AppSettings["SearchEngine:ConnectionPassword"];
            if (username != null)
            {
                settings.BasicAuthentication(username, password);
            }
            
            return new ElasticClient(settings);
        }
    }
}