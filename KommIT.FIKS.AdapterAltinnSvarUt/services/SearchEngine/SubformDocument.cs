﻿using Nest;
using System;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.SearchEngine
{
    [ElasticsearchType(IdProperty = nameof(Id))]
    public class SubformDocument
    {
        public string Id { get; set; }
        public DateTime? ArchiveTimestamp { get; set; }
        /// <summary>
        /// Hovedskjemaets arkivreferense
        /// </summary>
        public string MainformArchiveReference { get; set; }
        /// <summary>
        /// Hovedskjemaets navn
        /// </summary>
        public string MainformName { get; set; }
        /// <summary>
        /// Hovedskjemaets dataformatid
        /// </summary>
        public string MainformDataformatId { get; set; }
        /// <summary>
        /// Hovedskjemaets dataformatversjon
        /// </summary>
        public string MainformDataformatVersion { get; set; }
        /// <summary>
        /// Underskjemaets navn
        /// </summary>
        public string SubformName { get; set; }
        /// <summary>
        /// Underskjemaets dataformatid
        /// </summary>
        public string SubformDataformatId { get; set; }
        /// <summary>
        /// Underskjemaets dataformatversjon
        /// </summary>
        public string SubformDataformatVersion { get; set; }
    }
}