﻿using Nest;
using System;
using System.Collections.Generic;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.SearchEngine
{
    /// <summary>
    /// This class can encapsulate the main information submitted through the form, providing methods and attributes to access and manipulate the data.
    /// </summary>
    [ElasticsearchType(IdProperty = nameof(ArchiveReference))]
    public class SoeknadsData
    {
        public DateTime? ArchiveTimestamp { get; set; }

        /// <summary>
        ///     Arkivreferanse i altinn (MessageId i REST API)
        /// </summary>
        public string ArchiveReference { get; set; }

        /// <summary>
        ///
        /// </summary>
        public List<string> Tiltakstyper { get; set; }

        /// <summary>
        ///
        /// </summary>
        public List<string> Dispensasjonstyper { get; set; }

        /// <summary>
        /// Kommunenr det sendes til
        /// </summary>
        public string Kommunenummer { get; set; }

        /// <summary>
        /// Navn på skjematype - samme som står her https://fellesbygg.dibk.no/
        /// </summary>
        public string FormType { get; set; }

        /// <summary>
        /// Sluttbrukersystem som gjør innsending
        /// </summary>
        public string Application { get; set; }

        /// <summary>
        ///
        /// </summary>
        public List<string> Anleggstype { get; set; }

        /// <summary>
        ///
        /// </summary>
        public List<string> Bygningstype { get; set; }

        /// <summary>
        ///
        /// </summary>
        public List<string> Naeringsgruppe { get; set; }

        /// <summary>
        ///
        /// </summary>
        public List<string> Tiltaksformaal { get; set; }
    }
}