﻿using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using Nest;
using System;
using System.Collections.Generic;
using System.Linq;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.SearchEngine
{
    [ElasticsearchType(IdProperty = nameof(SvarUtForsendelsesId))]
    public class SvarUtForsendelseStatusDocument
    {
        public SvarUtForsendelseStatusDocument(SvarUtForsendelsesStatus svarUtForsendelsesStatus)
        {
            this.SvarUtForsendelsesId = svarUtForsendelsesStatus.SvarUtForsendelsesId;
            this.ArchiveReference = svarUtForsendelsesStatus.ArchiveReference;
            this.EndretStatusDato = svarUtForsendelsesStatus.EndretStatusDato;
            this.ForsendelseStatus = svarUtForsendelsesStatus.ForsendelseStatus;
            this.Kommune = svarUtForsendelsesStatus.MunicipalityName;
        }
        public string SvarUtForsendelsesId { get; set; }
        public string ArchiveReference { get; set; }
        public string ForsendelseStatus { get; set; }
        public DateTime? EndretStatusDato { get; set; }
        public string Kommune { get; set; }

        public static IEnumerable<SvarUtForsendelseStatusDocument> ToList(IEnumerable<SvarUtForsendelsesStatus> svarUtForsendelseStatuses)
        {
            return svarUtForsendelseStatuses.Select(entry => new SvarUtForsendelseStatusDocument(entry)).ToList();
        }
    }
}