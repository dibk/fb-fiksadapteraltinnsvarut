using System;
using System.Collections.Generic;
using System.Linq;
using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;
using Nest;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.SearchEngine
{
    
    /// <summary>
    /// A mirror of the DistributionForm class. This is used in the search engine index. 
    /// </summary>
    [ElasticsearchType(IdProperty = nameof(Id))]
    public class DistributionFormDocument
    {
        public string Id { get; set; }

        /// <summary>
        /// ID på innsendt distribusjonstjeneste (opprinnelsen)
        /// </summary>
        public string InitialArchiveReference { get; set; }
        /// <summary>
        /// Sluttbrukersystem sin referanse til forsendelsene (Hovedinnsendingsnr i distribusjon skjema datamodell)
        /// </summary>
        public string InitialExternalSystemReference { get; set; }
        /// <summary>
        /// Type distribusjonstjeneste
        /// </summary>
        public string DistributionType { get; set; }
        /// <summary>
        /// Referanse til preutfyllingsfunsjonen
        /// </summary>
        public string SubmitAndInstantiatePrefilledFormTaskReceiptId { get; set; }

        /// <summary>
        /// Tidspunkt prefill er sendt
        /// </summary>
        public DateTime? SubmitAndInstantiatePrefilled { get; set; }
        /// <summary>
        /// Sluttbrukersystem sin referanse til forsendelse (VaarReferanse i skjema datamodell)
        /// </summary>
        public string ExternalSystemReference { get; set; }
        /// <summary>
        /// Referanse til signert preutfylt innsending
        /// </summary>
        public string SignedArchiveReference { get; set; }
        /// <summary>
        /// Tidspunkt mottak av signert skjema
        /// </summary>
        public DateTime? Signed { get; set; }
        /// <summary>
        /// Referanse til videresendt melding med signert skjema (receiptSent)
        /// </summary>
        public string RecieptSentArchiveReference { get; set; }
        /// <summary>
        /// Tidspunkt videresending av signert skjema (receiptSent)
        /// </summary>
        public DateTime? RecieptSent { get; set; }
        /// <summary>
        /// Feilmelding ved status=error
        /// </summary>
        public string ErrorMessage { get; set; }
        /// <summary>
        /// Status på distribusjonen
        /// </summary>
        public string DistributionStatus { get; set; }

        /// <summary>
        /// Om distribusjonen har gått til print pga reservert
        /// </summary>
        public bool Printed { get; set; }
        
        public DistributionFormDocument(DistributionForm form)
        {
            Id = form.Id.ToString();
            InitialArchiveReference = form.InitialArchiveReference;
            InitialExternalSystemReference = form.InitialExternalSystemReference;
            DistributionType = form.DistributionType;
            SubmitAndInstantiatePrefilledFormTaskReceiptId = form.SubmitAndInstantiatePrefilledFormTaskReceiptId;
            SubmitAndInstantiatePrefilled = form.SubmitAndInstantiatePrefilled.ToElkDateTime();
            ExternalSystemReference = form.ExternalSystemReference;
            SignedArchiveReference = form.SignedArchiveReference;
            Signed = form.Signed.ToElkDateTime();
            RecieptSentArchiveReference = form.RecieptSentArchiveReference;
            RecieptSent = form.RecieptSent.ToElkDateTime();
            ErrorMessage = form.ErrorMessage;
            DistributionStatus = form.DistributionStatus.ToString();
            Printed = form.Printed;
        }

        public static List<DistributionFormDocument> ToList(IEnumerable<DistributionForm> distributionForms)
        {
            return distributionForms.Select(entry => new DistributionFormDocument(entry)).ToList();
        }
    }
}