using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using Nest;
using Serilog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Threading.Tasks;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.SearchEngine
{
    /// <summary>
    /// Index objects in an ElasticSearch server. All indexing methods check the enabled flag in app settings before
    /// communication with ElasticSearch.
    /// </summary>
    public class ElasticIndexer : ISearchEngineIndexer
    {
        private readonly ILogger _log = Log.ForContext<ElasticIndexer>();

        public async Task AddAsync(FormMetadata formMetadata)
        {
            if (IsEnabled())
            {
                _log.Verbose("Start indexing form metadata.");
                await IndexDocumentAsync(ElasticUtil.IndexNameForms, new FormMetadataDocument(formMetadata));
            }
        }

        public void Add(FormMetadata formMetadata)
        {
            if (IsEnabled())
            {
                _log.Verbose("Start indexing form metadata.");
                IndexDocument(ElasticUtil.IndexNameForms, new FormMetadataDocument(formMetadata));
            }
        }

        public async Task AddManyAsync(IEnumerable<FormMetadata> items)
        {
            if (IsEnabled())
            {
                _log.Verbose("Start indexing many form metadata objects.");
                await IndexManyAsync(ElasticUtil.IndexNameForms, FormMetadataDocument.ToList(items));
            }
        }

        public async Task AddAsync(LogEntry logEntry)
        {
            if (IsEnabled())
            {
                _log.Verbose("Start indexing log entry.");
                await IndexDocumentAsync(ElasticUtil.IndexNameLogEntries, new LogEntryDocument(logEntry));
            }
        }

        public void Add(LogEntry logEntry)
        {
            if (IsEnabled())
            {
                _log.Verbose("Start indexing log entry.");
                IndexDocument(ElasticUtil.IndexNameLogEntries, new LogEntryDocument(logEntry));
            }
        }

        public async Task AddManyAsync(IEnumerable<LogEntry> logEntries)
        {
            if (IsEnabled())
            {
                _log.Verbose("Start indexing many log entry objects.");
                await IndexManyAsync(ElasticUtil.IndexNameLogEntries, LogEntryDocument.ToList(logEntries));
            }
        }

        public void Add(ApplicationLog applicationLog)
        {
            if (IsEnabled())
            {
                _log.Verbose("Start indexing log entry.");
                IndexDocument(ElasticUtil.IndexNameApplicationLog, new ApplicationLogDocument(applicationLog));
            }
        }

        public async Task AddAsync(ApplicationLog applicationLog)
        {
            if (IsEnabled())
            {
                _log.Verbose("Start indexing log entry.");
                await IndexDocumentAsync(ElasticUtil.IndexNameLogEntries, new ApplicationLogDocument(applicationLog));
            }
        }

        public async Task AddManyAsync(List<ApplicationLog> applicationLogs)
        {
            if (IsEnabled())
            {
                _log.Verbose("Start indexing many log entry objects.");
                await IndexManyAsync(ElasticUtil.IndexNameLogEntries, ApplicationLogDocument.ToList(applicationLogs));
            }
        }

        public async Task AddAsync(DistributionForm distributionForm)
        {
            if (IsEnabled())
            {
                _log.Verbose("Start indexing distribution form.");
                await IndexDocumentAsync(ElasticUtil.IndexNameDistributionForms, new DistributionFormDocument(distributionForm));
            }
        }

        public void Add(DistributionForm distributionForm)
        {
            if (IsEnabled())
            {
                _log.Verbose("Start indexing distribution form.");
                IndexDocument(ElasticUtil.IndexNameDistributionForms, new DistributionFormDocument(distributionForm));
            }
        }

        public async Task AddManyAsync(IEnumerable<DistributionForm> distributionForms)
        {
            if (IsEnabled())
            {
                _log.Verbose("Start indexing many distribution form objects.");
                await IndexManyAsync(ElasticUtil.IndexNameDistributionForms, DistributionFormDocument.ToList(distributionForms));
            }
        }

        public async Task AddManyAsync(IEnumerable<SubformDocument> subformDocuments)
        {
            if (IsEnabled())
            {
                _log.Verbose("Start indexing many subform objects");
                await IndexManyAsync(ElasticUtil.IndexNameSubforms, subformDocuments);
            }
        }

        private async Task IndexDocumentAsync<TDoc>(string indexName, TDoc doc) where TDoc : class
        {
            try
            {
                var stopWatch = new Stopwatch();
                stopWatch.Start();
                await ElasticUtil.GetClient(indexName).IndexDocumentAsync(doc);
                stopWatch.Stop();
                _log.Verbose("Finished indexing document in: {ElapsedMilliseconds} ms", stopWatch.ElapsedMilliseconds);
            }
            catch (Exception e)
            {
                _log.Error(e, "Error while indexing document");
            }
        }

        private void IndexDocument<TDoc>(string indexName, TDoc doc) where TDoc : class
        {
            try
            {
                var stopWatch = new Stopwatch();
                stopWatch.Start();
                ElasticUtil.GetClient(indexName).IndexDocument(doc);
                stopWatch.Stop();
                _log.Verbose("Finished indexing document in: {ElapsedMilliseconds} ms", stopWatch.ElapsedMilliseconds);
            }
            catch (Exception e)
            {
                _log.Error(e, "Error while indexing document");
            }
        }

        private async Task IndexManyAsync<TDoc>(string indexName, IEnumerable<TDoc> docs) where TDoc : class
        {
            try
            {
                var stopWatch = new Stopwatch();
                stopWatch.Start();
                await ElasticUtil.GetClient(indexName).IndexManyAsync(docs);
                stopWatch.Stop();
                _log.Verbose("Finished indexing document in: {ElapsedMilliseconds} ms", stopWatch.ElapsedMilliseconds);
            }
            catch (Exception e)
            {
                _log.Error(e, "Error while indexing documents");
            }
        }

        private bool IsEnabled()
        {
            var configValueEnabled = ConfigurationManager.AppSettings["SearchEngine:Enabled"];
            var isEnabled = !string.IsNullOrEmpty(configValueEnabled) && configValueEnabled == "true";

            if (!isEnabled)
                _log.Verbose("Search engine indexing is disabled");

            return isEnabled;
        }

        public void Add(SvarUtForsendelsesStatus svarUtForsendelsesStatus)
        {
            if (IsEnabled())
            {
                _log.Verbose("Start indexing SvarUtForsendelseStatus.");
                IndexDocument(ElasticUtil.IndexNameSvarUtForsendelseStatus, new SvarUtForsendelseStatusDocument(svarUtForsendelsesStatus));
            }
        }

        public async Task AddManyAsync(List<SvarUtForsendelsesStatus> svarUtForsendelsesStatuses)
        {
            if (IsEnabled())
            {
                _log.Verbose("Start indexing many distribution form objects.");
                await IndexManyAsync(ElasticUtil.IndexNameSvarUtForsendelseStatus, SvarUtForsendelseStatusDocument.ToList(svarUtForsendelsesStatuses));
            }
        }

        public void Add(SoeknadsData soeknadsData)
        {
            if (IsEnabled())
            {
                _log.Verbose("Start indexing soeknadsData.");
                IndexDocument(ElasticUtil.IndexNameSoeknadsData, soeknadsData);
            }
        }
    }
}