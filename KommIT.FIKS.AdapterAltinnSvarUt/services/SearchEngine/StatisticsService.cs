using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Nest;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.SearchEngine
{
    public interface IStatisticsService
    {
        Dictionary<string, string> GetNumberOfFormsGroupedByType();
        Dictionary<string, string> GetNumberOfLogEntryTypes();
        Dictionary<string, string> GetNumberOfApplicationLogTypes();
    }

    public class StatisticsService : IStatisticsService
    {
        public Dictionary<string, string> GetNumberOfFormsGroupedByType()
        {
            var now = DateTime.Now;
            DateTime nowMinus30Days = now.Subtract(TimeSpan.FromDays(30));
            return GetNumberOfFormsGroupedByType(nowMinus30Days, now);
        }

        public Dictionary<string, string> GetNumberOfFormsGroupedByType(DateTime greaterThan, DateTime lessThan)
        {
            var formCountGroupedByType = new Dictionary<string, string>();
            var aggregationTermName = "group_by_formtype";

            ElasticClient elasticClient = ElasticUtil.GetClient(ElasticUtil.IndexNameForms);

            ISearchResponse<FormMetadataDocument> response = elasticClient.Search<FormMetadataDocument>(s => s
                    .Size(0)
                    /*          .Query(q => q.Bool(b => b
                                  .Filter(DateRangeFilterForms(greaterThan, lessThan))
                                  .Must(m => m.MatchAll()))
                              )*/
                    .Aggregations(aggs => aggs
                        .Terms(aggregationTermName, t => t.Field(f => f.FormType.Suffix("keyword"))))
            );

            BucketAggregate aggregate = (BucketAggregate)response.Aggregations[aggregationTermName];
            foreach (var bucket in aggregate.Items)
            {
                var item = (KeyedBucket<object>)bucket;
                string formName = (string)item.Key;
                string formCount = item.DocCount.ToString();
                formCountGroupedByType.Add(formName, formCount);
            }

            return formCountGroupedByType;
        }

        public Dictionary<string, string> GetNumberOfLogEntryTypes()
        {
            var now = DateTime.Now;
            DateTime nowMinus30Days = now.Subtract(TimeSpan.FromDays(30));
            return GetNumberOfLogEntryTypes(nowMinus30Days, now);
        }


        public Dictionary<string, string> GetNumberOfLogEntryTypes(DateTime greaterThan, DateTime lessThan)
        {
            var formCountGroupedByType = new Dictionary<string, string>();
            var aggregationTermName = "group_by_type";

            ElasticClient elasticClient = ElasticUtil.GetClient(ElasticUtil.IndexNameLogEntries);

            ISearchResponse<LogEntryDocument> response = elasticClient.Search<LogEntryDocument>(s => s
                .Size(0)
                .Query(q => q.Bool(b => b
                    .Filter(DateRangeFilterLogEntries(greaterThan, lessThan))
                    .Must(m => m.MatchAll()))
                )
                .Aggregations(aggs => aggs
                    .Terms(aggregationTermName, t => t.Field(f => f.Type.Suffix("keyword"))))
            );

            BucketAggregate aggregate = (BucketAggregate)response.Aggregations[aggregationTermName];
            foreach (var bucket in aggregate.Items)
            {
                var item = (KeyedBucket<object>)bucket;
                string formName = (string)item.Key;
                string formCount = item.DocCount.ToString();
                formCountGroupedByType.Add(formName, formCount);
            }

            return formCountGroupedByType;
        }
        public Dictionary<string, string> GetNumberOfApplicationLogTypes()
        {
            var now = DateTime.Now;
            DateTime nowMinus30Days = now.Subtract(TimeSpan.FromDays(30));
            return GetNumberOfApplicationLogTypes(nowMinus30Days, now);
        }


        public Dictionary<string, string> GetNumberOfApplicationLogTypes(DateTime greaterThan, DateTime lessThan)
        {
            var formCountGroupedByType = new Dictionary<string, string>();
            var aggregationTermName = "group_by_type";

            ElasticClient elasticClient = ElasticUtil.GetClient(ElasticUtil.IndexNameApplicationLog);

            ISearchResponse<ApplicationLogDocument> response = elasticClient.Search<ApplicationLogDocument>(s => s
                .Size(0)
                .Query(q => q.Bool(b => b
                    .Filter(DateRangeFilterApplicationLog(greaterThan, lessThan))
                    .Must(m => m.MatchAll()))
                )
                .Aggregations(aggs => aggs
                    .Terms(aggregationTermName, t => t.Field(f => f.Type.Suffix("keyword"))))
            );

            BucketAggregate aggregate = (BucketAggregate)response.Aggregations[aggregationTermName];
            foreach (var bucket in aggregate.Items)
            {
                var item = (KeyedBucket<object>)bucket;
                string formName = (string)item.Key;
                string formCount = item.DocCount.ToString();
                formCountGroupedByType.Add(formName, formCount);
            }

            return formCountGroupedByType;
        }




        private static Func<QueryContainerDescriptor<LogEntryDocument>, QueryContainer> DateRangeFilterLogEntries(DateTime greaterThan, DateTime lessThan)
        {
            return f => f.
                DateRange(r => r
                   .Field(doc => doc.Timestamp)
                   .GreaterThan(new DateTime(2019, 03, 02))
                   .LessThan(DateMath.Now));
        }
        private static Func<QueryContainerDescriptor<ApplicationLogDocument>, QueryContainer> DateRangeFilterApplicationLog(DateTime greaterThan, DateTime lessThan)
        {
            return f => f.
                DateRange(r => r
                   .Field(doc => doc.Timestamp)
                   .GreaterThan(new DateTime(2019, 03, 02))
                   .LessThan(DateMath.Now));
        }

        private static Func<QueryContainerDescriptor<FormMetadataDocument>, QueryContainer> DateRangeFilterForms(DateTime greaterThan, DateTime lessThan)
        {
            return f => f.
                DateRange(r => r
                   .Field(doc => doc.ArchiveTimestamp)
                   .GreaterThan(new DateTime(2019, 03, 02))
                   .LessThan(DateMath.Now));
        }


    }
}