using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.SearchEngine
{
    /// <summary>
    /// Managing the search engine index.
    /// </summary>
    public interface ISearchEngineIndexer
    {
        /// <summary>
        /// Add form metadata to search engine index. ArchiveReference is the unique key. The same metadata object can
        /// be added multiple times. If the form already exists, the index will be updated.
        /// </summary>
        /// <param name="formMetadata"></param>
        /// <returns></returns>
        Task AddAsync(FormMetadata formMetadata);

        /// <summary>
        /// Add form metadata to search engine index. ArchiveReference is the unique key. The same metadata object can
        /// be added multiple times. If the form already exists, the index will be updated. Synchronus version
        /// </summary>
        /// <param name="formMetadata"></param>
        /// <returns></returns>
        void Add(FormMetadata formMetadata);

        /// <summary>
        /// Add batches of form metadata objects to search engine index. ArchiveReference is the unique key. The same metadata object can
        /// be added multiple times. If the form already exists, the index will be updated.
        /// </summary>
        /// <param name="items"></param>
        /// <returns></returns>
        Task AddManyAsync(IEnumerable<FormMetadata> items);

        /// <summary>
        /// Add log entry to search engine index. The Id of the log entry is the unique key. The same object can be
        /// added multiple times. If the entry already exists, the index will be updated.
        /// </summary>
        /// <param name="logEntry"></param>
        /// <returns></returns>
        Task AddAsync(LogEntry logEntry);

        /// <summary>
        /// Add log entry to search engine index. The Id of the log entry is the unique key. The same object can be
        /// added multiple times. If the entry already exists, the index will be updated. Synchronus version
        /// </summary>
        /// <param name="logEntry"></param>
        /// <returns></returns>
        void Add(LogEntry logEntry);

        /// <summary>
        /// Add batches of log entries to search engine index. The Id of the log entry is the unique key. The same object can be
        /// added multiple times. If the entry already exists, the index will be updated.
        /// </summary>
        /// <param name="logEntries"></param>
        /// <returns></returns>
        Task AddManyAsync(IEnumerable<LogEntry> logEntries);

        /// <summary>
        /// Add applicationLog to search engine index. The Id of the application log is the unique key. The same object can be
        /// added multiple times. If the entry already exists, the index will be updated.
        /// </summary>
        /// <param name="applicationLog"></param>
        /// <returns></returns>
        Task AddAsync(ApplicationLog applicationLog);

        /// <summary>
        /// Add pplicationLog to search engine index. The Id of the pplicationLog is the unique key. The same object can be
        /// added multiple times. If the entry already exists, the index will be updated. Synchronus version
        /// </summary>
        /// <param name="applicationLog"></param>
        /// <returns></returns>
        void Add(ApplicationLog applicationLog);

        /// <summary>
        /// Add batches of pplicationLogs to search engine index. The Id of the pplicationLog is the unique key. The same object can be
        /// added multiple times. If the entry already exists, the index will be updated.
        /// </summary>
        /// <param name="applicationLogs"></param>
        /// <returns></returns>
        Task AddManyAsync(List<ApplicationLog> applicationLogs);

        /// <summary>
        /// Add distribution form to search engine index. The Id of the distribution form is the unique key. The same object can be
        /// added multiple times. If the entry already exists, the index will be updated.
        /// </summary>
        /// <param name="distributionForm"></param>
        /// <returns></returns>
        Task AddAsync(DistributionForm distributionForm);

        /// <summary>
        /// Add distribution form to search engine index. The Id of the distribution form is the unique key. The same object can be
        /// added multiple times. If the entry already exists, the index will be updated. Synchronus version
        /// </summary>
        /// <param name="distributionForm"></param>
        /// <returns></returns>
        void Add(DistributionForm distributionForm);

        /// <summary>
        /// Add batches of distribution forms to search engine index. The Id of the distribution form is the unique key. The same object can be
        /// added multiple times. If the entry already exists, the index will be updated.
        /// </summary>
        /// <param name="distributionForms"></param>
        /// <returns></returns>
        Task AddManyAsync(IEnumerable<DistributionForm> distributionForms);

        void Add(SvarUtForsendelsesStatus svarUtForsendelsesStatus);
        Task AddManyAsync(List<SvarUtForsendelsesStatus> svarUtForsendelsesStatuses);

        Task AddManyAsync(IEnumerable<SubformDocument> subformDocuments);

        void Add(SoeknadsData formSubmission);

    }
}