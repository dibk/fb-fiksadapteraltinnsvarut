using System;
using System.Collections.Generic;
using System.Linq;
using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using Nest;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.SearchEngine
{
    /// <summary>
    ///  A mirror of the LogEntry class. This is used in the search engine index. 
    /// </summary>
    [ElasticsearchType(IdProperty = nameof(Id))]
    public class LogEntryDocument
    {
        public int Id { get; set; }
        public DateTime Timestamp { get; set; }
        public string ArchiveReference { get; set; }
        public string Message { get; set; }
        public string Type { get; set; }
        public bool OnlyInternal { get; set; }
        public string Url { get; set; }
        public string EventId { get; set; }
        public long? EventDuration { get; set; }

        public LogEntryDocument(LogEntry logEntry)
        {
            Id = logEntry.Id;
            Timestamp = logEntry.Timestamp;
            ArchiveReference = logEntry.ArchiveReference;
            Message = logEntry.Message;
            Type = logEntry.Type;
            OnlyInternal = logEntry.OnlyInternal;
            Url = logEntry.Url;
            EventId = logEntry.EventId;
            EventDuration = logEntry.EventDuration;

        }

        public static IEnumerable<LogEntryDocument> ToList(IEnumerable<LogEntry> logEntries)
        {
            return logEntries.Select(entry => new LogEntryDocument(entry)).ToList();
        }
    }
}