﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Hangfire;
using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;
using Serilog;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.SearchEngine
{
    public interface IBatchIndexer
    {
        Task RunIndexingOfFormsMetadata();
        Task RunIndexingOfLogEntries();
        Task RunIndexingOfDistributionForms(DateTime? indexFromDate);
        Task RunIndexingOfApplicationLog();


        void StartBatchIndexingFormMetadataJob();
        void StartBatchIndexingLogEntriesJob();
        void StartBatchIndexingDistributionFormsJob(DateTime? indexFromDate);
        void StartBatchIndexingApplicationLogJob();

    }

    public class BatchIndexer : IBatchIndexer
    {
        private readonly ILogger _log = Log.ForContext<BatchIndexer>();

        private readonly ApplicationDbContext _context;
        private readonly ISearchEngineIndexer _searchEngineIndexer;

        private const int DefaultBatchSize = 100;
        private readonly int _batchSize;
        
        public BatchIndexer(ApplicationDbContext context, ISearchEngineIndexer searchEngineIndexer)
        {
            _context = context;
            _searchEngineIndexer = searchEngineIndexer;
            _batchSize = GetBatchSizeFromConfig();
        }

        private int GetBatchSizeFromConfig()
        {
            var batchSizeFromConfig = ConfigurationManager.AppSettings[ConfigKeys.SearchEngineBatchSize];
            int result;
            if (int.TryParse(batchSizeFromConfig, out result) && result > 0)
                return result;
            else
            {
                _log.Warning("Unable to parse search engine batch size from config, using default. key=" + ConfigKeys.SearchEngineBatchSize + ", value=" + batchSizeFromConfig);
                return DefaultBatchSize;
            }
        }

        public async Task RunIndexingOfFormsMetadata()
        {
            _log.Debug("Start batch indexing of forms metadata.");
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            var query = _context.FormMetadata.OrderBy(m => m.ArchiveTimestamp).AsNoTracking();

            var count = query.Count();

            var cursor = 0;
            while (cursor < count)
            {
                List<FormMetadata> docs = query.Skip(cursor).Take(_batchSize).ToList();
                await _searchEngineIndexer.AddManyAsync(docs);
                cursor += _batchSize;
            }
            stopwatch.Stop();
            _log.Debug("Finished batch indexing of forms metadata. duration=" + stopwatch.ElapsedMilliseconds + "ms");
        }

        public async Task RunIndexingOfLogEntries()
        {
            _log.Debug("Start batch indexing of log entries.");
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            var query = _context.LogEntries.OrderBy(m => m.Timestamp).AsNoTracking();

            var count = query.Count();

            var cursor = 0;
            while (cursor < count)
            {
                List<LogEntry> logEntries = query.Skip(cursor).Take(_batchSize).ToList();
                await _searchEngineIndexer.AddManyAsync(logEntries);
                cursor += _batchSize;
            }
            stopwatch.Stop();
            _log.Debug("Finished batch indexing of log entries. duration=" + stopwatch.ElapsedMilliseconds + "ms");
        }
        public async Task RunIndexingOfApplicationLog() 
        {
            _log.Debug("Start batch indexing of ApplicationLog entries.");
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            var query = _context.ApplicationLog.OrderBy(m => m.Timestamp).AsNoTracking();

            var count = query.Count();

            var cursor = 0;
            while (cursor < count)
            {
                List<ApplicationLog> applicationLog = query.Skip(cursor).Take(_batchSize).ToList();
                await _searchEngineIndexer.AddManyAsync(applicationLog);
                cursor += _batchSize;
            }
            stopwatch.Stop();
            _log.Debug("Finished batch indexing of ApplicationLog entries. duration=" + stopwatch.ElapsedMilliseconds + "ms");
        }

        public async Task RunIndexingOfDistributionForms(DateTime? indexFromDate)
        {
            _log.Debug("Start batch indexing of distribution forms.");
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            var query = _context.DistributionForms
                .OrderBy(m => m.SubmitAndInstantiatePrefilled)
                .AsNoTracking();

           // if (indexFromDate != null)
           //     query = query.Where(f => f.SubmitAndInstantiatePrefilled > indexFromDate.Value);

            var count = query.Count();

            var cursor = 0;
            while (cursor < count)
            {
                List<DistributionForm> distributionForms = query.Skip(cursor).Take(_batchSize).ToList();
                await _searchEngineIndexer.AddManyAsync(distributionForms);
                cursor += _batchSize;
            }
            stopwatch.Stop();
            _log.Debug("Finished batch indexing of distribution forms. duration=" + stopwatch.ElapsedMilliseconds + "ms");
        }

        /// <summary>
        /// Start Hangfire background job indexing form metadata
        /// </summary>
        public void StartBatchIndexingFormMetadataJob()
        {
            BackgroundJob.Enqueue<IBatchIndexer>(x => x.RunIndexingOfFormsMetadata());
        }

        /// <summary>
        /// Start Hangfire background job indexing log entries
        /// </summary>
        public void StartBatchIndexingLogEntriesJob()
        {
            BackgroundJob.Enqueue<IBatchIndexer>(x => x.RunIndexingOfLogEntries());
        }
        /// <summary>
        ///  Start Hangfire background job indexing applicationlog
        /// </summary>
        public void StartBatchIndexingApplicationLogJob()
        {
            BackgroundJob.Enqueue<IBatchIndexer>(x => x.RunIndexingOfApplicationLog());
        }

        /// <summary>
        /// Start Hangfire background job indexing distribution forms
        /// </summary>
        public void StartBatchIndexingDistributionFormsJob(DateTime? indexFromDate)
        {
            BackgroundJob.Enqueue<IBatchIndexer>(x => x.RunIndexingOfDistributionForms(indexFromDate));
        }
    }
}