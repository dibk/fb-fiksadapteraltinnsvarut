﻿using System.Collections.Generic;
using System.Linq;
using KommIT.FIKS.AdapterAltinnSvarUt.Models;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services
{
    public class FormService : IFormService
    {
        private readonly ApplicationDbContext _dbContext;

        public FormService(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<Form> GetDefaultForms()
        {
            return _dbContext.Set<Form>().OrderBy(f => f.Name).ToList();
        }

        public Form Get(int formId)
        {
            return _dbContext.Set<Form>().Find(formId);
        }
    }
}