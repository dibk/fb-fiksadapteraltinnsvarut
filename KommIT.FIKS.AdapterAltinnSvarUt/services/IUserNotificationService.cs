﻿using System;
using System.Collections.Generic;
using KommIT.FIKS.AdapterAltinnSvarUt.Models;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services
{
    public interface IUserNotificationService
    {
        void DeleteEntries(DateTime expired);
        void AddUserNotification(Models.UserNotification userNotification);
        List<Models.UserNotification> GetActiveNotifications();
        void UpdateStatus(UserNotificationStatus userNotificationStatus, string messageReference);
        void UpdateRecord(Models.UserNotification userNotification);
    }
}
