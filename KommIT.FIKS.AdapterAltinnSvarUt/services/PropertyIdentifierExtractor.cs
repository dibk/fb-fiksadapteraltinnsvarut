﻿using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services
{
    public class PropertyIdentifierExtractor
    {
        /*
        Lagre innstilinger i et eget objekt tilknyttet en kommune.
        Kun brukere som har lov til å administrere en kommune kan endre innstillingene.
        Innstillingene som skal endres:
        tittel
        klassering (denne må vi avklare mer med Tor Kjetil)
        Tittel skal bygges opp som en tekstreng basert på nøkkelord adskilt med mellomrom.
        De predefinerte nøkkelordene:
        tiltakstype
        bygningstypekode / anleggstypekode
        adresse
        gårdsnummer
        bruksnummer
        festenummer
        seksjonsnummer
        */

        //private const string TagKommunenummer = "{kommune}";
        //private const string TagGaardsnummer = "{gnr}";
        //private const string TagBruksnummer = "{bnr}";
        //private const string TagFestenummer = "{fnr}";
        //private const string TagSeksjonsnummer = "{snr}";
        //private const string TagAdresselinje1 = "{adr1}";
        //private const string TagAdresselinje2 = "{adr2}";
        //private const string TagAdresselinje3 = "{adr3}";
        //private const string TagPostnr = "{postnr}";
        //private const string TagPoststed = "{poststed}";
        //private const string TagLandkode = "{land}";
        //private const string TagBygningsnummer = "{byggnr}";
        //private const string TagBolignummer = "{bolignr}";
        //private const string TagSoeknadSkjemaNavn = "{skjema}";

        ////private const string TagSoeknadSkjemaSubType = "{underskjema}";
        //private const string TagTiltakType = "{tiltak}";

        //private const string TagAltinnArkivReferanse = "{altinnreferanse}";
        //private const string FormatEiendomDefault = "{gnr}/{bnr}/{fnr}/{snr}";
        public const string FormatDefault = "{adr1} {skjema} {gnr}/{bnr}";

        //public const string FormatDefaultRammesoeknad = "{adr1} {skjema} {underskjema} {tiltak} {gnr}/{bnr}";
        public const string FormatDefaultRammesoeknad = "{adr1} {skjema} {tiltak} {gnr}/{bnr}";

        // Generated the SvarUt title based on the preferences set in the MinicipailtySettings database
        //public string GenerateSvarUtDocumentTitle(PropertyIdentifiers propertyIdentifiers, Receiver receiver)
        //{
        //    // Get the list of Municipality settings associated with the Receiver->Municipalities setting
        //    List<string> formatStringsForMunicipality = new List<string>();

        //    // If there are no strings found, assign a default format string
        //    if (formatStringsForMunicipality.Count == 0)
        //    {
        //        formatStringsForMunicipality.Add(FormatDefault);
        //    }

        //    // Choose the last format string in the list
        //    StringBuilder generatedTitle = new StringBuilder(formatStringsForMunicipality.Last());

        //    // Replace the tags with actual data
        //    generatedTitle.Replace(TagKommunenummer, propertyIdentifiers.Kommunenummer);
        //    generatedTitle.Replace(TagGaardsnummer, propertyIdentifiers.Gaardsnummer);
        //    generatedTitle.Replace(TagBruksnummer, propertyIdentifiers.Bruksnummer);
        //    generatedTitle.Replace(TagFestenummer, propertyIdentifiers.Festenummer);
        //    generatedTitle.Replace(TagSeksjonsnummer, propertyIdentifiers.Seksjonsnummer);
        //    generatedTitle.Replace(TagAdresselinje1, propertyIdentifiers.Adresselinje1);
        //    generatedTitle.Replace(TagAdresselinje2, propertyIdentifiers.Adresselinje2);
        //    generatedTitle.Replace(TagAdresselinje3, propertyIdentifiers.Adresselinje3);
        //    generatedTitle.Replace(TagPostnr, propertyIdentifiers.Postnr);
        //    generatedTitle.Replace(TagPoststed, propertyIdentifiers.Poststed);
        //    generatedTitle.Replace(TagLandkode, propertyIdentifiers.Landkode);
        //    generatedTitle.Replace(TagBygningsnummer, propertyIdentifiers.Bygningsnummer);
        //    generatedTitle.Replace(TagBolignummer, propertyIdentifiers.Bolignummer);
        //    generatedTitle.Replace(TagSoeknadSkjemaNavn, propertyIdentifiers.SoeknadSkjemaNavn);
        //    // generatedTitle.Replace(TagSoeknadSkjemaSubType, propertyIdentifiers.SoeknadSkjemaSubType);
        //    generatedTitle.Replace(TagTiltakType, propertyIdentifiers.TiltakType);

        //    generatedTitle.Replace(TagSoeknadSkjemaNavn, propertyIdentifiers.SoeknadSkjemaNavn);

        //    return generatedTitle.ToString();
        //}

        //public List<SvarUtClassificationValue> GenerateSvarUtDocumentClassifications(PropertyIdentifiers propertyIdentifiers, Receiver receiver, FormData form)
        //{
        //    List<SvarUtClassificationValue> classificationsValues = new List<SvarUtClassificationValue>();

        //    //Default classifications
        //    if (classificationsValues.Count == 0)
        //    {
        //        classificationsValues.Add(new SvarUtClassificationValue(1, "GBNR", FormatEiendomDefault));
        //        classificationsValues.Add(new SvarUtClassificationValue(2, "SID", TagAltinnArkivReferanse));
        //    }

        //    foreach (var classification in classificationsValues)
        //    {
        //        // Replace the tags with actual data
        //        classification.OrganizingValue = classification.OrganizingValue.Replace(TagKommunenummer, propertyIdentifiers.Kommunenummer);
        //        classification.OrganizingValue = classification.OrganizingValue.Replace(TagGaardsnummer, propertyIdentifiers.Gaardsnummer);
        //        classification.OrganizingValue = classification.OrganizingValue.Replace(TagBruksnummer, propertyIdentifiers.Bruksnummer);
        //        classification.OrganizingValue = classification.OrganizingValue.Replace(TagFestenummer, propertyIdentifiers.Festenummer);
        //        classification.OrganizingValue = classification.OrganizingValue.Replace(TagSeksjonsnummer, propertyIdentifiers.Seksjonsnummer);
        //        classification.OrganizingValue = classification.OrganizingValue.Replace(TagAdresselinje1, propertyIdentifiers.Adresselinje1);
        //        classification.OrganizingValue = classification.OrganizingValue.Replace(TagAdresselinje2, propertyIdentifiers.Adresselinje2);
        //        classification.OrganizingValue = classification.OrganizingValue.Replace(TagAdresselinje3, propertyIdentifiers.Adresselinje3);
        //        classification.OrganizingValue = classification.OrganizingValue.Replace(TagPostnr, propertyIdentifiers.Postnr);
        //        classification.OrganizingValue = classification.OrganizingValue.Replace(TagPoststed, propertyIdentifiers.Poststed);
        //        classification.OrganizingValue = classification.OrganizingValue.Replace(TagLandkode, propertyIdentifiers.Landkode);
        //        classification.OrganizingValue = classification.OrganizingValue.Replace(TagBygningsnummer, propertyIdentifiers.Bygningsnummer);
        //        classification.OrganizingValue = classification.OrganizingValue.Replace(TagBolignummer, propertyIdentifiers.Bolignummer);
        //        classification.OrganizingValue = classification.OrganizingValue.Replace(TagSoeknadSkjemaNavn, propertyIdentifiers.SoeknadSkjemaNavn);
        //        //classification.OrganizingValue = classification.OrganizingValue.Replace(TagSoeknadSkjemaSubType, propertyIdentifiers.SoeknadSkjemaSubType);
        //        classification.OrganizingValue = classification.OrganizingValue.Replace(TagTiltakType, propertyIdentifiers.TiltakType);
        //        classification.OrganizingValue = classification.OrganizingValue.Replace(TagAltinnArkivReferanse, form.ArchiveReference);
        //        classification.OrganizingValue = classification.OrganizingValue.Replace(TagSoeknadSkjemaNavn, propertyIdentifiers.SoeknadSkjemaNavn);
        //    }

        //    return classificationsValues;
        //}

        //public List<SvarUtAttachmentSetting> GenerateSvarUtDocumentAttachmentSettings(PropertyIdentifiers propertyIdentifiers, Receiver receiver, FormData formData)
        //{
        //    List<SvarUtAttachmentSetting> attachmentsAll = new List<SvarUtAttachmentSetting>();

        //    int dokumentNumber = 1;


        //    foreach (var type in AttachmentSetting.Types)
        //    {
        //        SvarUtAttachmentSetting settingExists = null;
        //        if (attachmentsAll.Count() > 0) settingExists = attachmentsAll.FirstOrDefault(s => s.AttatchmentTypeName == type.Key);
        //        if (settingExists == null)
        //        {
        //            string ebyggesakDoktype = AttachmentSetting.eByggesakTypes.FirstOrDefault(t => t.Key == type.Key).Value;
        //            attachmentsAll.Add(new SvarUtAttachmentSetting
        //            { AttatchmentTypeName = type.Key, Title = type.Value, DocumentNumber = dokumentNumber, DocumentType = ebyggesakDoktype, DocumentStatus = "F", TilknyttetRegistreringSom = "V" });
        //        }

        //        dokumentNumber++;
        //    }

        //    return attachmentsAll;
        //}

        //public List<Title> GetTitles(string selectedTitle = "")
        //{
        //    if (string.IsNullOrWhiteSpace(selectedTitle))
        //        selectedTitle = "";

        //    List<Title> titles = new List<Title>();
        //    titles.Add(new Title { Tag = TagKommunenummer, Description = "Kommunenummer", ExampleValue = "1111", Selected = Selected(TagKommunenummer, selectedTitle), Separator = GetSeparator(TagKommunenummer, selectedTitle) });
        //    titles.Add(new Title { Tag = TagGaardsnummer, Description = "Gårdsnummer", ExampleValue = "22", Selected = Selected(TagGaardsnummer, selectedTitle), Separator = GetSeparator(TagGaardsnummer, selectedTitle) });
        //    titles.Add(new Title { Tag = TagBruksnummer, Description = "Bruksnummer", ExampleValue = "33", Selected = Selected(TagBruksnummer, selectedTitle), Separator = GetSeparator(TagBruksnummer, selectedTitle) });
        //    titles.Add(new Title { Tag = TagFestenummer, Description = "Festenummer", ExampleValue = "44", Selected = Selected(TagFestenummer, selectedTitle), Separator = GetSeparator(TagFestenummer, selectedTitle) });
        //    titles.Add(new Title { Tag = TagSeksjonsnummer, Description = "Seksjonsnummer", ExampleValue = "55", Selected = Selected(TagSeksjonsnummer, selectedTitle), Separator = GetSeparator(TagSeksjonsnummer, selectedTitle) });
        //    titles.Add(new Title { Tag = TagAdresselinje1, Description = "Adresselinje1", ExampleValue = "Hovedgata 2", Selected = Selected(TagAdresselinje1, selectedTitle), Separator = GetSeparator(TagAdresselinje1, selectedTitle) });
        //    titles.Add(new Title { Tag = TagAdresselinje2, Description = "Adresselinje2", ExampleValue = "Hovedgata 2", Selected = Selected(TagAdresselinje2, selectedTitle), Separator = GetSeparator(TagAdresselinje2, selectedTitle) });
        //    titles.Add(new Title { Tag = TagAdresselinje3, Description = "Adresselinje3", ExampleValue = "Leilighet A", Selected = Selected(TagAdresselinje3, selectedTitle), Separator = GetSeparator(TagAdresselinje3, selectedTitle) });
        //    titles.Add(new Title { Tag = TagPostnr, Description = "Postnummer", ExampleValue = "3825", Selected = Selected(TagPostnr, selectedTitle), Separator = GetSeparator(TagPostnr, selectedTitle) });
        //    titles.Add(new Title { Tag = TagPoststed, Description = "Poststed", ExampleValue = "Lunde", Selected = Selected(TagPoststed, selectedTitle), Separator = GetSeparator(TagPoststed, selectedTitle) });
        //    titles.Add(new Title { Tag = TagLandkode, Description = "Landkode", ExampleValue = "NO", Selected = Selected(TagLandkode, selectedTitle), Separator = GetSeparator(TagLandkode, selectedTitle) });
        //    titles.Add(new Title { Tag = TagBygningsnummer, Description = "Bygningsnummer", ExampleValue = "Hovedgata 2", Selected = Selected(TagBygningsnummer, selectedTitle), Separator = GetSeparator(TagBygningsnummer, selectedTitle) });
        //    titles.Add(new Title { Tag = TagBolignummer, Description = "Bolignummer", ExampleValue = "4T", Selected = Selected(TagBolignummer, selectedTitle), Separator = GetSeparator(TagBolignummer, selectedTitle) });
        //    titles.Add(new Title { Tag = TagSoeknadSkjemaNavn, Description = "Søknadstype", ExampleValue = "Gjennomføringsplan", Selected = Selected(TagSoeknadSkjemaNavn, selectedTitle), Separator = GetSeparator(TagSoeknadSkjemaNavn, selectedTitle) });

        //    if (!string.IsNullOrEmpty(selectedTitle))
        //        titles = SortSelected(titles, selectedTitle);

        //    return titles;
        //}

        //public List<Title> GetOrganizingValues(string selectedTitle = "")
        //{
        //    if (string.IsNullOrWhiteSpace(selectedTitle))
        //        selectedTitle = "";

        //    List<Title> titles = new List<Title>();
        //    titles.Add(new Title { Tag = TagKommunenummer, Description = "Kommunenummer", ExampleValue = "1111", Selected = Selected(TagKommunenummer, selectedTitle), Separator = GetSeparator(TagKommunenummer, selectedTitle) });
        //    titles.Add(new Title { Tag = TagGaardsnummer, Description = "Gårdsnummer", ExampleValue = "2", Selected = Selected(TagGaardsnummer, selectedTitle), Separator = GetSeparator(TagGaardsnummer, selectedTitle) });
        //    titles.Add(new Title { Tag = TagBruksnummer, Description = "Bruksnummer", ExampleValue = "3", Selected = Selected(TagBruksnummer, selectedTitle), Separator = GetSeparator(TagBruksnummer, selectedTitle) });
        //    titles.Add(new Title { Tag = TagFestenummer, Description = "Festenummer", ExampleValue = "44", Selected = Selected(TagFestenummer, selectedTitle), Separator = GetSeparator(TagFestenummer, selectedTitle) });
        //    titles.Add(new Title { Tag = TagSeksjonsnummer, Description = "Seksjonsnummer", ExampleValue = "55", Selected = Selected(TagSeksjonsnummer, selectedTitle), Separator = GetSeparator(TagSeksjonsnummer, selectedTitle) });
        //    titles.Add(new Title { Tag = TagAdresselinje1, Description = "Adresselinje1", ExampleValue = "Hovedgata 2", Selected = Selected(TagAdresselinje1, selectedTitle), Separator = GetSeparator(TagAdresselinje1, selectedTitle) });
        //    titles.Add(new Title { Tag = TagAdresselinje2, Description = "Adresselinje2", ExampleValue = "Hovedgata 2", Selected = Selected(TagAdresselinje2, selectedTitle), Separator = GetSeparator(TagAdresselinje2, selectedTitle) });
        //    titles.Add(new Title { Tag = TagAdresselinje3, Description = "Adresselinje3", ExampleValue = "Leilighet A", Selected = Selected(TagAdresselinje3, selectedTitle), Separator = GetSeparator(TagAdresselinje3, selectedTitle) });
        //    titles.Add(new Title { Tag = TagPostnr, Description = "Postnummer", ExampleValue = "3825", Selected = Selected(TagPostnr, selectedTitle), Separator = GetSeparator(TagPostnr, selectedTitle) });
        //    titles.Add(new Title { Tag = TagPoststed, Description = "Poststed", ExampleValue = "Lunde", Selected = Selected(TagPoststed, selectedTitle), Separator = GetSeparator(TagPoststed, selectedTitle) });
        //    titles.Add(new Title { Tag = TagLandkode, Description = "Landkode", ExampleValue = "NO", Selected = Selected(TagLandkode, selectedTitle), Separator = GetSeparator(TagLandkode, selectedTitle) });
        //    titles.Add(new Title { Tag = TagBygningsnummer, Description = "Bygningsnummer", ExampleValue = "Hovedgata 2", Selected = Selected(TagBygningsnummer, selectedTitle), Separator = GetSeparator(TagBygningsnummer, selectedTitle) });
        //    titles.Add(new Title { Tag = TagBolignummer, Description = "Bolignummer", ExampleValue = "4T", Selected = Selected(TagBolignummer, selectedTitle), Separator = GetSeparator(TagBolignummer, selectedTitle) });
        //    titles.Add(new Title { Tag = TagSoeknadSkjemaNavn, Description = "Søknadstype", ExampleValue = "Gjennomføringsplan", Selected = Selected(TagSoeknadSkjemaNavn, selectedTitle), Separator = GetSeparator(TagSoeknadSkjemaNavn, selectedTitle) });
        //    titles.Add(new Title { Tag = TagAltinnArkivReferanse, Description = "Altinn arkivreferanse", ExampleValue = "AR1234567", Selected = Selected(TagAltinnArkivReferanse, selectedTitle), Separator = GetSeparator(TagAltinnArkivReferanse, selectedTitle) });

        //    if (!string.IsNullOrEmpty(selectedTitle))
        //        titles = SortSelected(titles, selectedTitle);

        //    return titles;
        //}

        //private List<Title> SortSelected(List<Title> titles, string selectedTitle)
        //{
        //    for (int i = 0; i < titles.Count; i++)
        //    {
        //        titles[i].SelectedOrder = selectedTitle.IndexOf(titles[i].Tag);
        //    }

        //    return titles;
        //}

        //private string GetSeparator(string tag, string selectedTitles)
        //{
        //    string separator = "/"; //default
        //    int positionstartTag = selectedTitles.IndexOf(tag);
        //    int positionEndTag = -1;
        //    if (positionstartTag != -1)
        //        positionEndTag = positionstartTag + tag.Length;

        //    int nextTagPosition = positionEndTag != -1 ? selectedTitles.IndexOf("{", positionEndTag) : -1;

        //    if (positionEndTag != -1 && nextTagPosition != -1)
        //    {
        //        int length = nextTagPosition - positionEndTag;
        //        separator = selectedTitles.Substring(positionEndTag, length);
        //    }

        //    return separator;
        //}

        //private bool Selected(string tag, string selectedTitles)
        //{
        //    return selectedTitles.Contains(tag);
        //}
    }

    //public class Title
    //{
    //    public string Tag { get; set; }
    //    public string Description { get; set; }
    //    public string ExampleValue { get; set; }
    //    public bool Selected { get; set; }
    //    public int SelectedOrder { get; set; }
    //    public string Separator { get; set; }
    //}

    public class SvarUtAttachmentSetting
    {
        public int Id { get; set; }
        
        public int DocumentNumber { get; set; }
        public string AttatchmentTypeName { get; set; }
        
        public string Title { get; set; } // AttatchmentTypeNameLanguage
        
        public string DocumentType { get; set; } //dokumentkategori for arkivet
        
        public string DocumentStatus { get; set; } = "F";
        
        public string TilknyttetRegistreringSom { get; set; } = "V";
    }
}