﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Receipt
{
    public interface IReceiptService
    {
        void SaveReceipt(Models.Receipt receipt);

    }
}
