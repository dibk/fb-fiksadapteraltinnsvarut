﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KommIT.FIKS.AdapterAltinnSvarUt.Models;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Receipt
{
    public class ReceiptService : IReceiptService
    {
        private readonly ApplicationDbContext _dbContext;

        public ReceiptService(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void SaveReceipt(Models.Receipt receipt)
        {
            _dbContext.Receipts.Add(receipt);
            _dbContext.SaveChanges();
        }
    }
}