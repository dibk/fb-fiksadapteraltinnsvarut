﻿using System.Collections.Generic;
using KommIT.FIKS.AdapterAltinnSvarUt.Models;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services
{
    public interface IMunicipalityService
    {
        Municipality GetMunicipality(string code);        
        List<Municipality> GetMunicipalitiesForUser(string userId);
        Municipality GetMunicipalityWithAccessControl(string code, string userId);
        void UpdateMunicipalityApplicationUser(string[] municipalities, string userId);
        bool UserHasAccessToMunicipality(string municipalityCode, string userId);        
        void UpdateMunicipality(Municipality municipality);
    }
}
