﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.Text;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Email
{
    public class EmailClient
    {

        public void SendEmail(string receiverAddress, string subject, string htmlBody)
        {
            var emailApiServer = ConfigurationManager.AppSettings.Get("FtpbApiEmailUrl");
            var url = $"{emailApiServer}/api/email";

            var payload = new EmailMessage();
            var receiver = new List<EmailAddress>() { new EmailAddress() { Address = receiverAddress }};
            payload.To = receiver;
            payload.Subject = subject;
            payload.HtmlBody = htmlBody;

            var jsonPayload = JsonConvert.SerializeObject(payload);

            using(var client = new WebClient())
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                client.Encoding = Encoding.UTF8;                
                client.Headers.Add(HttpRequestHeader.ContentType, "application/json");
                var result = client.UploadString(url, jsonPayload);
            }

        }
    }

    internal class EmailAddress
    {
        public string Address { get; set; }
        public string DisplayName { get; set; }
    }

    internal class EmailMessage
    {
        public IEnumerable<EmailAddress> To { get; set; }
        public IEnumerable<EmailAddress> CC { get; set; }
        public IEnumerable<EmailAddress> Bcc { get; set; }
        public EmailAddress From { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public string HtmlBody { get; set; }
    }

}