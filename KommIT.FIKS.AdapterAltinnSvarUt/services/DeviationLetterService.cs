using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.SearchEngine;
using Serilog;
using System.Collections.Generic;
using System.Linq;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services
{
    public class DeviationLetterService : IDeviationLetterService
    {
        private readonly ILogger _log = Log.ForContext<DeviationLetterService>();

        private readonly ApplicationDbContext _dbContext;
        private readonly ISearchEngineIndexer _searchEngineIndexer;

        public DeviationLetterService(ApplicationDbContext dbContext, ISearchEngineIndexer searchEngineIndexer)
        {
            _dbContext = dbContext;
            _searchEngineIndexer = searchEngineIndexer;
        }

        public void SaveDeviationLetter(DeviationLetter deviationLetter)
        {
            _dbContext.DeviationLetter.Add(deviationLetter);
            _dbContext.SaveChanges();
        }

        public List<DeviationLetter> GetListOfDeviationLettersByArchiveReference(string archiveReference)
        {
            var queryResults = from d in _dbContext.DeviationLetter
                               where d.AltinnArchiveReference == archiveReference
                               select d;

            return queryResults.ToList();
        }

        public List<DeviationLetter> GetListOfDeviationLettersByCaseYearAndSequence(string kommunenr, int saksaar, long sakssekvensnummer)
        {
            var queryResults = from d in _dbContext.DeviationLetter
                               where d.MunicipalityArchiveCaseYear == saksaar &&
                                     d.MunicipalityArchiveCaseSequence == sakssekvensnummer &&
                                     d.MunicipalityCode == kommunenr
                               select d;

            return queryResults.ToList();
        }

        public List<DeviationLetter> GetListOfDeviationLettersByArchiveYearAndSequence(string kommunenr, long journalsekvensnummer, int journalaar)
        {
            var queryResults = from d in _dbContext.DeviationLetter
                               where d.ArchiveSequence == journalsekvensnummer &&
                                     d.ArchiveYear == journalaar &&
                                     d.MunicipalityCode == kommunenr
                               select d;
            return queryResults.ToList();
        }
    }
}