﻿using Hangfire;
using KommIT.FIKS.AdapterAltinnSvarUt.HangfireConfiguration;
using KommIT.FIKS.AdapterAltinnSvarUt.Logger;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Azure;
using Nest;
using Serilog;
using Serilog.Context;
using System;
using System.Configuration;
using System.Linq;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services
{
    public class TiltakshaverSamtykkeCleanUpService
    {
        private readonly ITiltakshaverSamtykkeService _samtykkeService;
        private readonly IFileDownloadStatusService _fileDownloadStatusService;
        private readonly IFileStorage _fileStorage;
        private readonly IFormMetadataService _formMetadataService;
        private readonly ILogger _logger = Log.ForContext<TiltakshaverSamtykkeCleanUpService>();

        public TiltakshaverSamtykkeCleanUpService(ITiltakshaverSamtykkeService samtykkeService,
                                                  IFileDownloadStatusService fileDownloadStatusService,
                                                  IFileStorage fileStorage,
                                                  IFormMetadataService formMetadataService)
        {
            _samtykkeService = samtykkeService;
            _fileDownloadStatusService = fileDownloadStatusService;
            _fileStorage = fileStorage;
            _formMetadataService = formMetadataService;
        }

        private const string _jobName = "ScheduleSaByCleanUpJobs";

        public void ScheduleCleanUpJobs()
        {
            var scheduleJobString = ConfigurationManager.AppSettings["Tiltakshaverssignatur:CleanUpJob:Enabled"];
            bool scheduleJob;
            if (!bool.TryParse(scheduleJobString, out scheduleJob))
                _logger.Error("Unable to find or parse Tiltakshaverssignatur:CleanUpJob:Enabled to boolean value, will not schedule job");

            if (scheduleJob)
                RecurringJob.AddOrUpdate(_jobName, () => IdentifyAndScheduleCleanUps(), HangfireConfiguration.CronExpressions.DailyAt("1", "1"));
            else
                RecurringJob.RemoveIfExists(_jobName);
        }

        [HangfireBasicJobLogger]
        [JobDisplayName("SaBy - Create clean up jobs")]
        public void IdentifyAndScheduleCleanUps()
        {
            var deleteAfterDaysString = ConfigurationManager.AppSettings["Tiltakshaverssignatur:CleanUpJob:DeleteAfterDays"];
            int defaultDays = 365;
            int configuredDays = 0;
            if (!int.TryParse(deleteAfterDaysString, out configuredDays))
            {
                _logger.Error("Unable to find or parse Tiltakshaverssignatur:CleanUpJob:Enabled to boolean value, will not schedule job");
                configuredDays = defaultDays;
            }

            var ids = _samtykkeService.GetCandidatesForDeletion(configuredDays);

            //BackgroundJob.Enqueue(() => CleanUpSaByEntity(ids.FirstOrDefault())); //Lokal test

            foreach (var id in ids)
            {
                BackgroundJob.Enqueue(() => CleanUpSaByEntity(id));
            }
        }

        [HangfireBasicJobLogger]
        [JobDisplayName("SaBy - Clean up #{0}")]
        public void CleanUpSaByEntity(string id)
        {
            using (LogContext.PushProperty("SaById", id))
            {
                _logger.Information("Starts marking tiltakshaverssamtykke as deleted");
                var ts = _samtykkeService.GetTiltakshaversSamtykke(id);

                if (ts != null && !string.IsNullOrEmpty(ts.AltinnArkivreferanse))
                {
                    var fm = _formMetadataService.GetFormMetadata(ts.AltinnArkivreferanse);
                    if (fm != null)
                    {
                        fm.Slettet = DateTime.Now;
                        _formMetadataService.UpdateFormMetadata(fm);
                    }
                    else
                        _logger.Warning("Unable to find formmetadata for {ArchiveReference}", ts.AltinnArkivreferanse);

                    var files = _fileDownloadStatusService.GetFileDownloadStatusesForArchiveReference(ts.AltinnArkivreferanse);

                    var containers = files.Select(s => s.Guid.ToString()).Distinct();
                    foreach (var containerName in containers)
                    {
                        _logger.Information("Deletes container {ContainerName}", containerName);
                        _fileStorage.DeleteContainer(containerName);
                    }

                    foreach (var file in files)
                    {
                        _logger.Information("Marks FileDownloadStatus {Id} for file {FileName} as deleted.", file.Id.ToString(), file.Filename);
                        file.IsDeleted = true;
                        _fileDownloadStatusService.Update(file);
                    }
                }

                //Forsikrer at container med opplastede filer er slettet
                _logger.Information("Makes sure container for signed docs is deleted");
                _fileStorage.DeleteContainer(ts.Id.ToString());

                //Rydder opp i samtykke data i databasen
                _samtykkeService.MarkAsDeleted(id);
            }
        }
    }
}