﻿using KommIT.FIKS.AdapterAltinnSvarUt.Models;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services
{
    public interface IFormShippingService
    {
        void Ship(FormData formData);
    }
}
