﻿using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.WS.Altinn;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Altinn
{
    public class AltinnFormDeserializer : IAltinnFormDeserializer
    {
        private readonly ILogger _logger = Log.ForContext<AltinnFormDeserializer>();

        public FormData DeserializeForm(ArchivedFormTaskDQBE archivedFormTask)
        {
            _logger.Debug("{ArchiveReference} deserialization", archivedFormTask.ArchiveReference);

            string formDataAsXml = archivedFormTask.Forms.First().FormData;
            var formData = new FormData(archivedFormTask.ArchiveReference, formDataAsXml);
            formData.ArchiveTimestamp = archivedFormTask.ArchiveTimeStamp;
            formData.MainformDataFormatID = archivedFormTask.Forms.First().DataFormatID;
            formData.MainformDataFormatVersionID = archivedFormTask.Forms.First().DataFormatVersionID.ToString();
            formData.Reportee = archivedFormTask.Reportee;

            Deserialize(formData, formDataAsXml);

            formData.Attachments = ExtractAttacment(archivedFormTask);

            formData.Subforms = ExtractSubform(archivedFormTask);

            if (formData.Mainform != null)
                formData.Mainform.SetFormSetElements(GetFormSetElements(formData));

            return formData;
        }

        private List<string> GetFormSetElements(FormData formData)
        {
            List<string> list = new List<string>();

            foreach (var item in formData.Attachments)
            {
                list.Add(item.AttachmentTypeName);
            }
            foreach (var item in formData.Subforms)
            {
                list.Add(item.FormName);
            }

            return list;
        }

        private List<Subform> ExtractSubform(ArchivedFormTaskDQBE archivedFormTask)
        {
            var subformList = new List<Subform>();

            for (int i = 1; i < archivedFormTask.Forms.Count; i++)
            {
                subformList.Add(new Subform(archivedFormTask.Forms[i].DataFormatID, archivedFormTask.Forms[i].DataFormatVersionID.ToString(), archivedFormTask.Forms[i].FormData));
            }
            return subformList;
        }

        private static List<Attachment> ExtractAttacment(ArchivedFormTaskDQBE archivedFormTask)
        {
            var attachments = new List<Attachment>();

            foreach (var item in archivedFormTask.Attachments)
            {
                attachments.Add(new Attachment(item.ArchiveReference, item.AttachmentData, item.AttachmentType, item.AttachmentTypeName, item.FileName, item.IsEncrypted));
            }

            return attachments;
        }

        internal static IEnumerable<IAltinnForm> GetForms()
        {
            var instances = from t in Assembly.GetExecutingAssembly().GetTypes()
                            where t.GetInterfaces().Contains(typeof(KommIT.FIKS.AdapterAltinnSvarUt.Services.IAltinnForm))
                                     && t.GetConstructor(Type.EmptyTypes) != null
                            select Activator.CreateInstance(t) as KommIT.FIKS.AdapterAltinnSvarUt.Services.IAltinnForm;
            return instances;
        }

        public static void Deserialize(FormData formData, string formDataAsXml)
        {
            var instances = GetForms();

            foreach (var item in instances)
            {
                if (item.GetDataFormatId() == formData.MainformDataFormatID && item.GetDataFormatVersion() == formData.MainformDataFormatVersionID)
                {
                    formData.Mainform = item;
                    formData.Mainform.InitiateForm(formDataAsXml);

                    var formMeta = formData.Mainform as IFormMetadata;
                    formMeta?.SetArchiveReference(formData.ArchiveReference);
                }
            }
        }
    }
}