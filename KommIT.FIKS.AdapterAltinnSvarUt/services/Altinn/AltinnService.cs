﻿using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.WS;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.WS.Altinn;
using Polly;
using Serilog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.ServiceModel;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Altinn
{
    public abstract class AltinnService : IAltinnService, IDisposable
    {
        protected abstract string Username { get; }
        protected abstract string Password { get; }

        private readonly IDownloadStreamAttachment _downloadAttachment;
        private readonly IApplicationLogService _applicationLogService;
        private readonly ILogger _logger;
        DownloadQueueExternalBasicClient _client;
        private bool disposedValue;

        public AltinnService(IDownloadStreamAttachment downloadAttachment, IApplicationLogService applicationLogService, ILogger logger)
        {
            _downloadAttachment = downloadAttachment;
            _applicationLogService = applicationLogService;
            _logger = logger;
        }
        
        private void CloseClient()
        {
            if (_client != null)
                try
                {
                    _client.Close();
                }
                catch (Exception ex)
                {
                    _logger.Error(ex, "Error occured when closing DownloadQueueClient, state: {ClientState}", _client?.State);
                }
        }

        private DownloadQueueExternalBasicClient GetClient()
        {
            if (_client == null || _client.State != CommunicationState.Opened)
                _client = new DownloadQueueExternalBasicClient();

            return _client;
        }

        private DownloadQueueItemBEList GetDownloadQueueItems(string username, string password, string csvServiceCodes)
        {
            var stopWatch = new Stopwatch();
            stopWatch.Start();
            var items = new DownloadQueueItemBEList();
            
            var appLog = new ApplicationLogBuilder(ProcessLabel.AltinnDlqQueue, "AltinnService.cs");

            try
            {
                if (csvServiceCodes != null)
                {
                    // Loop through each csv service code in the _serviceCode string and retrive the queued items at Altinn
                    foreach (string serviceCode in csvServiceCodes.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries))
                    {
                        DownloadQueueItemBEList list = GetClient().GetDownloadQueueItems(username, password, serviceCode.Trim());  // Altinn returns a List<DownloadQueueItemBE>
                        items.AddRange(list);
                    }
                }
                else
                {
                    DownloadQueueItemBEList list = GetClient().GetDownloadQueueItems(username, password, null);  // Altinn returns a List<DownloadQueueItemBE>
                    items.AddRange(list);
                }

                _client.Close();
            }
            catch (FaultException<AltinnFault> fe)
            {
                _logger.Error(fe, "AltinnFault occurred when downloading items from DownloadQueue: {AltinnErrorMessage}", fe.Detail.AltinnErrorMessage);
                _client.Abort();
                throw;
            }
            catch (CommunicationException)
            {
                _client.Abort();
                throw;
            }
            catch (TimeoutException)
            {
                _client.Abort();
                throw;
            }
            catch (Exception)
            {
                _client.Abort();
                throw;
            }

            _applicationLogService.Save(appLog.SetMessage($"Completed getting DLQ list from Altinn", ApplicationLog.Info, stopWatch));

            return items;
        }

        /// <summary>
        /// Downloads list of all the pensding items in the Altinn DownloadQueue
        /// </summary>
        /// <returns>List of type DownloadQueueItemBE</returns>
        public List<DownloadQueueItemBE> GetAllAltinnDlqPendingForms()
        {
            var stopWatch = new Stopwatch();
            stopWatch.Start();
            var appLog = new ApplicationLogBuilder(ProcessLabel.AltinnDlqQueue, "AltinnService.cs");
            _applicationLogService.Save(appLog.SetMessage("Getting pending forms from DownloadQueue", ApplicationLog.Info));

            DownloadQueueItemBEList list = new DownloadQueueItemBEList();

            try
            {
                list.AddRange(GetDownloadQueueItems(Username, Password, null));
            }
            catch (Exception e)
            {
                _logger.Error(e, "Error ({ExceptionType}) occurred when GetAllAltinnDlqPendingForms with Altinn {Username} DLQ - {ExternalResource} - ExceptionMessage: {ExceptionMessage}", e.GetType().ToString(), Username, "DownloadQueueExternalBasicClient.GetDownloadQueueItems", e.Message);
            }

            return list;
        }

        public ArchivedFormTaskDQBE GetItemByArchiveReference(string archiveReference)
        {
            ArchivedFormTaskDQBE taskDQBE = null;
            try
            {
                taskDQBE = GetArchivedFormTaskBasicDQ(Username, Password, archiveReference);
            }
            catch (Exception e)
            {
                _logger.Error(e, "Error ({ExceptionType}) occurred when GetItemByArchiveReference with Altinn {Username} - {ExternalResource} - ExceptionMessage: {ExceptionMessage}", e.GetType().ToString(), Username, "DownloadQueueExternalBasicClient.GetArchivedFormTaskBasicDQAsync", e.Message);
                throw;
            }

            return taskDQBE;
        }


        private ArchivedFormTaskDQBE GetArchivedFormTaskBasicDQ(string username, string password, string archiveReference)
        {
            var stopWatch = new Stopwatch();
            stopWatch.Start();
            var appLog = new ApplicationLogBuilder(ProcessLabel.AltinnDlqForm, "AltinnService.cs");
            appLog.SetReference(archiveReference);
            _applicationLogService.Save(appLog.SetMessage("Downloading form from Altinn", ApplicationLog.Info));

            ArchivedFormTaskDQBE archivedFormTask = null;

            var policy = GetRetryPolicy("Altinn-GetArchivedForm");
            policy.Execute(() =>
            {
                try
                {   
                    archivedFormTask = GetClient().GetArchivedFormTaskBasicDQ(username, password, archiveReference, 1044, false);
                }
                catch (FaultException<AltinnFault> fe)
                {
                    _logger.Error(fe, "AltinnFault occurred when downloading from DownloadQueue: {AltinnErrorMessage}", fe.Detail.AltinnErrorMessage);
                    _client.Abort();
                    throw;
                }
                catch (CommunicationException cex)
                {
                    _client.Abort();
                    throw;
                }
                catch (TimeoutException)
                {
                    _client.Abort();
                    throw;
                }
                catch (Exception)
                {
                    _client.Abort();
                    throw;
                }
            });

            if (archivedFormTask != null)
                foreach (var attachment in archivedFormTask.Attachments)
                {
                    if (attachment.AttachmentData == null)
                    {
                        //Retry policy!!
                        policy.Execute(() =>
                        {
                            attachment.AttachmentData = _downloadAttachment.GetAttachmentDataStreamed(attachment.AttachmentId, username, password);
                        });
                    }
                }
            _applicationLogService.Save(appLog.SetMessage($"Completed downloading form from Altinn", ApplicationLog.Info, stopWatch));

            return archivedFormTask;
        }

        public bool PurgeItem(string archiveReference)
        {
            var successfullyPurged = false;
            var stopWatch = new Stopwatch();
            stopWatch.Start();
            var appLog = new ApplicationLogBuilder(ProcessLabel.AltinnDlqDelete, "AltinnService.cs");
            appLog.SetReference(archiveReference);
            _applicationLogService.Save(appLog.SetMessage("Deleting form from Altinn", ApplicationLog.Info));

            try
            {
                var policy = GetRetryPolicy("Altinn-PurgeItem");
                return policy.Execute(() =>
                {
                    try
                    {
                        var result = GetClient().PurgeItem(Username, Password, archiveReference);

                        if (result.Equals("OK", StringComparison.OrdinalIgnoreCase))
                            successfullyPurged = true;
                    }
                    catch (FaultException<AltinnFault> fe)
                    {
                        _logger.Error(fe, "AltinnFault occurred when purging from DownloadQueue: {AltinnErrorMessage}", fe.Detail.AltinnErrorMessage);
                        _client.Abort();
                        throw;
                    }
                    catch (CommunicationException)
                    {
                        _client.Abort();
                        throw;
                    }
                    catch (TimeoutException)
                    {
                        _client.Abort();
                        throw;
                    }
                    catch (Exception)
                    {
                        _client.Abort();
                        throw;
                    }

                    _applicationLogService.Save(appLog.SetMessage($"Completed deleting form from Altinn", ApplicationLog.Info, stopWatch));
                    return successfullyPurged;
                });
            }
            catch (Exception e)
            {
                _logger.Error(e, "Error ({ExceptionType}) occurred when communicating with Altinn for {ServiceOwner} - {ExternalResource} - ExceptionMessage: {ExceptionMessage}", e.GetType().ToString(), Username, "DownloadQueueExternalBasicClient.PurgeItem", e.Message);
                throw;
            }
        }

        public byte[] GetPdfForm(string ArchiveReference, string DataFormatId, string DataFormatVersion)
        {
            byte[] pdf = null;
            try
            {
                pdf = GetFormSetPdfBasic(ArchiveReference, DataFormatId, DataFormatVersion, Username, Password);
            }
            catch (Exception)
            {
                throw;
            }

            return pdf;
        }

        private byte[] GetFormSetPdfBasic(string archiveReference, string dataFormatId, string dataFormatVersion, string username, string password)
        {
            var stopWatch = new Stopwatch();
            stopWatch.Start();
            var appLog = new ApplicationLogBuilder(ProcessLabel.AltinnDlqPdf, "AltinnService.cs");
            _applicationLogService.Save(appLog.SetMessage("Downloading main form PDF from Altinn ... single message log", ApplicationLog.Info));

            try
            {
                //Retry policy!!
                var policy = GetRetryPolicy("Altinn-GetFormPdf");
                return policy.Execute(() =>
                {
                    try
                    {
                        var pdfBytes = GetClient().GetFormSetPdfBasic(username, password, archiveReference, 1044, dataFormatId, Convert.ToInt32(dataFormatVersion));
                        
                        return pdfBytes;
                    }
                    catch (FaultException<AltinnFault> fe)
                    {
                        _logger.Error(fe, "AltinnFault occurred when purging from DownloadQueue: {AltinnErrorMessage}", fe.Detail.AltinnErrorMessage);
                        _client.Abort();

                        if (fe.Detail.AltinnErrorMessage.Contains("This agency system is not allowed to access requested resources. Incorrect service owner"))
                            return null;
                        else
                            throw;
                    }
                    catch (CommunicationException)
                    {
                        _client.Abort();
                        throw;
                    }
                    catch (TimeoutException)
                    {
                        _client.Abort();
                        throw;
                    }
                    catch (Exception)
                    {
                        _client.Abort();
                        throw;
                    }
                });
            }
            catch (Exception e)
            {
                _logger.Error(e, "Error ({ExceptionType}) occurred when communicating with Altinn - {ExternalResource} - ExceptionMessage: {ExceptionMessage}", e.GetType().ToString(), "DownloadQueueExternalBasicClient.GetFormSetPdfBasic", e.Message);
                throw;
            }
        }

        private Policy GetRetryPolicy(string altinnApi)
        {
            List<int> retryAttemptWaitStructure = new List<int> { 5, 10, 30, 60, 120 };
            var retryCount = 0;
            var policy = Policy.Handle<ServerTooBusyException>()
                .Or<TimeoutException>()
                .Or<EndpointNotFoundException>()
                .Or<CommunicationException>(c => c.Message.Contains("A connection that was expected to be kept alive was closed by the server"))
                .WaitAndRetry(
                retryCount: retryAttemptWaitStructure.Count,
                sleepDurationProvider: attempt =>
                {
                    var jitterer = new Random();
                    var timeSpan = TimeSpan.FromSeconds(retryAttemptWaitStructure[retryCount])
                           + TimeSpan.FromMilliseconds(jitterer.Next(0, 1000));
                    retryCount++;

                    return timeSpan;
                },
                onRetry: (exception, calculatedWaitDuration) =>
                {
                    _logger.Debug(exception, "Failed to contact {RetryApi} {RetryCount} time(s). Waits for {CalculatedWaitDuration} and retries", altinnApi, retryCount, calculatedWaitDuration);
                });

            return policy;
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    CloseClient();
                    _client = null;
                }

                disposedValue = true;
            }
        }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}