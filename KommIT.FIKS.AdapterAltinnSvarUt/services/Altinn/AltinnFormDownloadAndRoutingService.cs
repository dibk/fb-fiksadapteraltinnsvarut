using Hangfire;
using KommIT.FIKS.AdapterAltinnSvarUt.HangfireConfiguration;
using KommIT.FIKS.AdapterAltinnSvarUt.Logger;
using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.QueuePreparators;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.FtId;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Notification;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Prefill;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.SearchEngine;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.WS.Altinn;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;
using Serilog;
using Serilog.Context;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Altinn
{
    public class AltinnFormDownloadAndRoutingService
    {
        public virtual string ServiceOwner
        { get { return "DIBK"; } }

        public virtual string ScheduledJobName
        { get { return $"DownloadAndEnqueueFromAltinn-{ServiceOwner}"; } }

        private IAltinnService _altinnService;
        private IFormDistributionServiceV2 _formDistributionService;
        private readonly IAltinnFormDeserializer _formDeserializer;
        private readonly ExportToSubmittalQueueService _exportToSubmittalQueueService;
        private readonly ISearchEngineIndexer _searchEngine;
        private IFormShippingService _formShippingsService;
        private readonly ILogEntryService _logEntryService;
        private readonly IFormMetadataService _formMetadataService;
        private INotificationServiceResolver _notificationServiceResolver;
        private readonly SendCorrespondenceHelper _correspondenceHelper;
        private readonly ILogger _logger = Log.ForContext<AltinnFormDownloadAndRoutingService>();
        private readonly string _serviceCodeShipping;
        private readonly string _serviceCodeDistribution;
        private readonly string _serviceCodeNotification;
        private readonly string _serviceCodeToCoreFunctions;
        private readonly string _queueServiceCodeToSubmittalQueue;
        private readonly IApplicationLogService _applicationLogService;
        private readonly IFtIdService _ftIdService;

        public AltinnFormDownloadAndRoutingService(IAltinnService altinnService,
                                                   IAltinnFormDeserializer formDeserializer,
                                                   IFormShippingService formShippingsService,
                                                   ILogEntryService logEntryService,
                                                   IFormMetadataService formMetadataService,
                                                   FormDistributionServiceV2 formDistributionService,
                                                   INotificationServiceResolver notificationServiceResolver,
                                                   SendCorrespondenceHelper correspondenceHelper,
                                                   IApplicationLogService applicationLogService,
                                                   ExportToSubmittalQueueService exportToSubmittalQueueService,
                                                   ISearchEngineIndexer searchEngine,
                                                   IFtIdService ftIdService)
        {
            _altinnService = altinnService;
            _formDeserializer = formDeserializer;
            _formShippingsService = formShippingsService;
            _logEntryService = logEntryService;
            _formMetadataService = formMetadataService;
            _formDistributionService = formDistributionService;
            _notificationServiceResolver = notificationServiceResolver;
            _correspondenceHelper = correspondenceHelper;
            _applicationLogService = applicationLogService;
            _exportToSubmittalQueueService = exportToSubmittalQueueService;
            _searchEngine = searchEngine;
            _ftIdService = ftIdService;
            _serviceCodeShipping = ConfigurationManager.AppSettings["Altinn.ServiceCodeShipping"];
            _serviceCodeDistribution = ConfigurationManager.AppSettings["Altinn.ServiceCodeDistributuion"];
            _serviceCodeNotification = ConfigurationManager.AppSettings["Altinn.ServiceCodeNotification"];
            _serviceCodeToCoreFunctions = ConfigurationManager.AppSettings["Altinn.ServiceCodeToCoreFunctions"];
            _queueServiceCodeToSubmittalQueue = ConfigurationManager.AppSettings["Altinn.QueueServiceCodeToSubmittalQueue"];
        }

        /// <summary>
        /// Retreives all queued forms from Altinn
        /// </summary>
        /// <returns>List of queued forms in altinn</returns>
        private List<DownloadFormForProcessing> GetQueuedFormsFromAltinn()
        {
            var formsForProcessing = new List<DownloadFormForProcessing>();

            List<DownloadQueueItemBE> allPendingForms = _altinnService.GetAllAltinnDlqPendingForms();

            foreach (var form in allPendingForms)
            {
                var formForProcessing = new DownloadFormForProcessing()
                {
                    ArchivedDate = form.ArchivedDate,
                    ArchiveReference = form.ArchiveReference,
                    ServiceCode = form.ServiceCode,
                    ServiceEditionCode = form.ServiceEditionCode,
                    ReporteeType = GetReporteeTypeFrom(form.ReporteeType),
                    FormProcessingType = GetFormProcessingTypeFrom(form.ServiceCode),
                    Queued = _formMetadataService.IsFormInQueue(form.ArchiveReference),
                    DoProcess = true,
                    QueueFormToSubmittalQueue = QueueFormToSubmittalQueue(form.ServiceCode)
                };
                formsForProcessing.Add(formForProcessing);
            }

            return formsForProcessing;
        }

        private bool QueueFormToSubmittalQueue(string serviceCode)
        {
            return _queueServiceCodeToSubmittalQueue.Contains(serviceCode);
        }

        /// <summary>
        /// Helper method to map between Altinn-reportee type and internal reportee type
        /// </summary>
        /// <param name="downloadQueueReporteeType"></param>
        /// <returns></returns>
        private ReporteeType GetReporteeTypeFrom(DownloadQueueReporteeType downloadQueueReporteeType)
        {
            var retVal = ReporteeType.Unkown;
            switch (downloadQueueReporteeType)
            {
                case DownloadQueueReporteeType.Organisation:
                    retVal = ReporteeType.Organisation;
                    break;

                case DownloadQueueReporteeType.Person:
                    retVal = ReporteeType.Person;
                    break;

                case DownloadQueueReporteeType.Selfregistereduser:
                default:
                    retVal = ReporteeType.Unkown;
                    break;
            }

            return retVal;
        }

        /// <summary>
        /// Helper method for retreiving FormProcessingType on service code given by altinn
        /// </summary>
        /// <param name="serviceCode">Service code to get FormProcessingType from</param>
        /// <returns></returns>
        private FormProcessingType GetFormProcessingTypeFrom(string serviceCode)
        {
            if (_serviceCodeShipping.Contains(serviceCode))
                return FormProcessingType.Shipping;
            else if (_serviceCodeDistribution.Contains(serviceCode))
                return FormProcessingType.Distribution;
            else if (_serviceCodeNotification.Contains(serviceCode))
                return FormProcessingType.Notification;
            else if (_serviceCodeToCoreFunctions.Contains(serviceCode))
                return FormProcessingType.CoreFunctionForwarder;
            else
                return FormProcessingType.Unknown;
        }

        /// <summary>
        /// Initiates handling of each form based on its form processing type
        /// </summary>
        /// <param name="formsForProcessing">Forms retreived from Altinn</param>
        public void QueueFormsReadyForProcessing(List<DownloadFormForProcessing> formsForProcessing)
        {
            try
            {
                LogStatisticsAndErrors(formsForProcessing);

                foreach (var form in formsForProcessing)
                {
                    var processSpecificTexts = GetTextsFor(form.FormProcessingType);

                    if (form.DoProcess)
                        if (!form.Queued)
                        {
                            _formMetadataService.SaveArchiveReference(form.ArchiveReference, processSpecificTexts.Item1);

                            _logger.Debug("{ArchiveReference} -- Form in queue for {ProcessingType} processing", form.ArchiveReference, processSpecificTexts.Item1);
                            _logEntryService.Save(new LogEntry(form.ArchiveReference, $"Skjema er lagt i kø for behandling til {processSpecificTexts.Item2}.", "Info"));
                            TelemetryLogger.LogAltinnTelemetry<AltinnFormDownloadAndRoutingService>("Hovedinnsending/svar", null, form.ServiceCode, form.ServiceEditionCode.ToString());
                            switch (form.FormProcessingType)
                            {
                                case FormProcessingType.Shipping:
                                    BackgroundJob.Enqueue(() => ShipItem(form.ArchiveReference, form.QueueFormToSubmittalQueue));
                                    break;

                                case FormProcessingType.Notification:
                                    BackgroundJob.Enqueue(() => ItemForNotification(form.ArchiveReference, form.QueueFormToSubmittalQueue));
                                    break;

                                case FormProcessingType.Distribution:
                                    BackgroundJob.Enqueue(() => ItemForDistribution(form.ArchiveReference, form.QueueFormToSubmittalQueue));
                                    break;

                                case FormProcessingType.CoreFunctionForwarder:
                                    BackgroundJob.Enqueue(() => ItemToCoreFunctions(form.ArchiveReference));
                                    break;

                                case FormProcessingType.Unknown:
                                default:
                                    //Already logged in LogStatisticsAndErorrs method
                                    break;
                            }
                        }
                        else
                            _logEntryService.Save(new LogEntry(form.ArchiveReference, $"Skjema er alt lagt i kø for behandling til {processSpecificTexts.Item2}. Venter på job som kjører.", "Info"));
                }
            }
            catch (Exception e)
            {
                _logger.Debug("Error setting up queues for form processing");
                _logger.Error(e, "Error while queuing forms ready for shipping.");
                throw;
            }
        }

        /// <summary>
        /// Adds log elements to application log with information on the forms retreived from Altinn.
        /// </summary>
        /// <param name="formsForProcessing">Forms retreived from Altinn</param>
        private void LogStatisticsAndErrors(List<DownloadFormForProcessing> formsForProcessing)
        {
            var appLog = new ApplicationLogBuilder(ProcessLabel.FormRouting);
            //Statistics
            // - Total number of downloaded forms
            var totalFormsDownloadedFromQueueLogElement = $"Forms downloaded: {formsForProcessing.Count()}";

            // - Total number of forms already on queue
            var totalFormsAlreadyOnQueueLogElement = $"Forms already queued: {formsForProcessing.Where(f => f.Queued).Count()}";

            // - Totals numbers by processig type
            var numbersPrProcessingTypeList = new List<string>();

            foreach (var formsByProcessingType in formsForProcessing.GroupBy(f => f.FormProcessingType).Select(grp => grp.ToList()).ToList())
            {
                numbersPrProcessingTypeList.Add($"{formsByProcessingType.First().FormProcessingType.ToString()}: {formsByProcessingType.Count()}");
            }

            var numbersPrProcessingTypeLogElement = string.Join(", ", numbersPrProcessingTypeList);

            _applicationLogService.Save(appLog.SetMessage($"{totalFormsDownloadedFromQueueLogElement} / {totalFormsAlreadyOnQueueLogElement} / Forms pr processing type: {numbersPrProcessingTypeLogElement}", ApplicationLog.Info));
            _logger.Debug("{TotalFormsDownloadedFromQueueLogElement} / {TotalFormsAlreadyOnQueueLogElement} / Forms pr processing type: {NumbersPrProcessingTypeLogElement}",
                totalFormsDownloadedFromQueueLogElement,
                totalFormsAlreadyOnQueueLogElement,
                numbersPrProcessingTypeLogElement);

            //Error logging
            // - Logging unknown forms
            foreach (var unkownForm in formsForProcessing.Where(f => f.FormProcessingType == FormProcessingType.Unknown).ToList())
                _applicationLogService.Save(appLog.SetMessage($"Form has unknown processing type: {unkownForm.ToString()}", ApplicationLog.Error));

            // - Logging forms that have been queued for more than x hours..
            var hours = 1;
            foreach (var staleForm in formsForProcessing.Where(f => f.ArchivedDate < DateTime.Now.AddHours(-hours)).ToList())
            {
                if (staleForm.FormProcessingType == FormProcessingType.Distribution)
                {
                    var distributionForms = _formMetadataService.GetDistributionForm(staleForm.ArchiveReference);
                    if (distributionForms != null && distributionForms.Count() > 0)
                    {
                        var max = distributionForms.Select(p => p.SubmitAndInstantiatePrefilled).Max();
                        if (max != null)
                        {
                            if (max < DateTime.Now.AddHours(-hours))
                                _applicationLogService.Save(appLog.SetMessage($"Form has been queued for more than {hours} hours  {staleForm.ToString()}", ApplicationLog.Error));
                            else
                                continue;
                        }
                    }
                }

                _applicationLogService.Save(appLog.SetMessage($"Form has been queued for more than {hours} hours  {staleForm.ToString()}", ApplicationLog.Error));
            }
        }

        private Tuple<string, string> GetTextsFor(FormProcessingType formProcessingType)
        {
            var text1 = string.Empty;
            var text2 = string.Empty;

            switch (formProcessingType)
            {
                case FormProcessingType.Shipping:
                    text1 = "SvarUt";
                    text2 = "SvarUt";
                    break;

                case FormProcessingType.Notification:
                    text1 = "Altinn - meldinger";
                    text2 = "Meldingstjeneste";
                    break;

                case FormProcessingType.Distribution:
                    text1 = "Altinn - distribusjon";
                    text2 = "Distribusjonstjeneste";
                    break;

                case FormProcessingType.CoreFunctionForwarder:
                    text1 = "Altinn/SvarUt";
                    text2 = "Altinn/SvarUt";
                    break;

                case FormProcessingType.Unknown:
                default:
                    break;
            }

            return Tuple.Create(text1, text2);
        }

        public void StartReccuringHangfireJobForForms()
        {
            // https://en.wikipedia.org/wiki/Cron
            string cronExp = "*/5 * * * *";

            RecurringJob.AddOrUpdate(this.ScheduledJobName, () => TasksForCronJob(), cronExp);
        }

        [HangfireBasicJobLogger]
        [HangfireDisableMultipleQueuedItems]
        public void TasksForCronJob()
        {
            GetFormsReadyForProcessing(null);
        }

        public void GetFormsReadyForProcessing(string processOnlyThisArchiveReference)
        {
            var appLog = new ApplicationLogBuilder(ProcessLabel.FormRouting);

            _logger.Debug("Getting Altinn Download Queue");
            try
            {
                var formsForProcessing = GetQueuedFormsFromAltinn();

                if (processOnlyThisArchiveReference != null)
                {
                    foreach (var form in from form in formsForProcessing
                                         where !form.ArchiveReference.Trim().Equals(processOnlyThisArchiveReference.Trim(), StringComparison.OrdinalIgnoreCase)
                                         select form)
                    {
                        form.DoProcess = false;
                    }
                }

                QueueFormsReadyForProcessing(formsForProcessing);
            }
            catch (FaultException<AltinnFault> altinnFault)
            {
                // handle a altinnFault
                _logger.Error(altinnFault, "FaultException while getting forms ready for shipping. Altinn error: {AltinnErrorMessage}", altinnFault.Detail.AltinnErrorMessage);
                throw;
            }
            catch (Exception e)
            {
                _logger.Error(e, "Error while getting forms ready for shipping.");
                throw;
            }
        }

        //Yes this is a copy of the one above.. Will be merged in some way later on....
        public void InitiateProcessingOf(string archiveReference, bool forceProcessingIfQueued = false)
        {
            var appLog = new ApplicationLogBuilder(ProcessLabel.FormRouting);

            _logger.Debug("Getting Altinn Download Queue");
            try
            {
                if (archiveReference != null)
                {
                    var formsForProcessing = GetQueuedFormsFromAltinn();

                    if (!formsForProcessing.Exists(f => f.ArchiveReference.Trim().Equals(archiveReference.Trim(), StringComparison.OrdinalIgnoreCase)))
                    {
                        //Try to find the form from Altinn

                        ArchivedFormTaskDQBE altinnForm = null;

                        altinnForm = _altinnService.GetItemByArchiveReference(archiveReference);

                        if (altinnForm == null)
                            throw new ArgumentException($"ArchiveReference {archiveReference} was not found in queue on Altinn");
                        else
                        {
                            var formForProcessing = new DownloadFormForProcessing()
                            {
                                ArchivedDate = altinnForm.ArchiveTimeStamp,
                                ArchiveReference = altinnForm.ArchiveReference,
                                ServiceCode = altinnForm.ServiceCode,
                                ServiceEditionCode = int.Parse(altinnForm.ServiceEditionCode),
                                ReporteeType = ReporteeType.Unkown,
                                FormProcessingType = GetFormProcessingTypeFrom(altinnForm.ServiceCode),
                                Queued = _formMetadataService.IsFormInQueue(altinnForm.ArchiveReference),
                                QueueFormToSubmittalQueue = QueueFormToSubmittalQueue(altinnForm.ServiceCode),
                                DoProcess = true
                            };
                            formsForProcessing.Clear();
                            formsForProcessing.Add(formForProcessing);
                        }
                    }

                    foreach (var form in from form in formsForProcessing
                                         where !form.ArchiveReference.Trim().Equals(archiveReference.Trim(), StringComparison.OrdinalIgnoreCase)
                                         select form)
                    {
                        form.DoProcess = false;
                    }

                    if (forceProcessingIfQueued)
                    {
                        var form = formsForProcessing.Where(f => f.ArchiveReference.Trim().Equals(archiveReference.Trim(), StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
                        _applicationLogService.Save(appLog.SetMessage($"Initiating forced processing of {0}", archiveReference, ApplicationLog.Info));
                        if (form != null)
                            form.Queued = false;
                    }

                    QueueFormsReadyForProcessing(formsForProcessing);
                }
                else
                    throw new ArgumentException("An archive reference must be provided");
            }
            catch (FaultException<AltinnFault> altinnFault)
            {
                // handle a altinnFault
                _logger.Error(altinnFault, "FaultException while getting forms ready for shipping. Altinn error: {AltinnErrorMessage}", altinnFault.Detail.AltinnErrorMessage);
                throw;
            }
            catch (Exception e)
            {
                _logger.Error(e, "Error while getting forms ready for shipping.");
                throw;
            }
        }

        private bool IsFormProcessable(ArchivedFormTaskDQBE archivedForm)
        {
            if (!AltinnFormTypeResolver.IsKnownForm(archivedForm.Forms.First().DataFormatID, archivedForm.Forms.First().DataFormatVersionID.ToString()))
            {
                _logger.Debug("{ArchiveReference} form is not supported", archivedForm.ArchiveReference);
                _logEntryService.Save(new LogEntry(archivedForm.ArchiveReference, Resources.TextStrings.ShippingErrorDownloadAndDeserialize + " skjema (" + archivedForm.ServiceCode + "/" + archivedForm.ServiceEditionCode + ") støttes ikke", "Error"));

                string title = String.Format(Resources.TextStrings.ShippingErrorTitle, archivedForm.ArchiveReference);
                string summary = Resources.TextStrings.ShippingErrorSummary;
                string body = String.Format(Resources.TextStrings.ShippingErrorBody, archivedForm.ArchiveReference) + Environment.NewLine + Resources.TextStrings.ShippingErrorDownloadAndDeserialize + " skjema (" + archivedForm.ServiceCode + "/" + archivedForm.ServiceEditionCode + ") støttes ikke";
                _correspondenceHelper.SendSimpleNotificaitonToReportee(archivedForm.Reportee, title, summary, body, archivedForm.ArchiveReference);
                _formMetadataService.UpdateValidationResultToFormMetadata(archivedForm.ArchiveReference, "Feil", 1, 0);

                BackgroundJob.Enqueue(() => PurgeItem(archivedForm.ArchiveReference));

                return false;
            }

            FormValidationService val = new FormValidationService();
            var valResultStruct = val.ValidateFormStructure(archivedForm.Forms.First().DataFormatID, archivedForm.Forms.First().DataFormatVersionID.ToString(), archivedForm.Forms.First().FormData);
            if (valResultStruct.HasErrors())
            {
                _logger.Debug("{ArchiveReference} failed validation for structure issues", archivedForm.ArchiveReference);
                // Send notificaiton to Altinn user
                string title = String.Format(Resources.TextStrings.ShippingErrorTitle, archivedForm.ArchiveReference);
                string summary = Resources.TextStrings.ShippingErrorSummary;
                string body = String.Format(Resources.TextStrings.ShippingErrorBody, archivedForm.ArchiveReference) + Environment.NewLine + Resources.TextStrings.ShippingErrorStructure + valResultStruct.ToString();

                _logEntryService.Save(new LogEntry(archivedForm.ArchiveReference, Resources.TextStrings.ShippingErrorStructure + valResultStruct.ToString(), "Error"));
                _formMetadataService.UpdateValidationResultToFormMetadata(archivedForm.ArchiveReference, "Feil - strukturvalidering", valResultStruct.Errors, valResultStruct.Warnings);
                _correspondenceHelper.SendSimpleNotificaitonToReportee(archivedForm.Reportee, title, summary, body, archivedForm.ArchiveReference);

                BackgroundJob.Enqueue(() => PurgeItem(archivedForm.ArchiveReference));

                val = null;
                return false;
            }

            return true;
        }

        [HangfireFormProcessingJobLogger]
        [DisplayName("Forwared item #{0} to Core Functions")]
        public void ItemToCoreFunctions(string archiveReference)
        {
            using (LogContext.PushProperty("ArchiveReference", archiveReference))
            {
                _logger.Debug("Form being processed for shipping");

                ArchivedFormTaskDQBE archivedForm;
                try
                {
                    archivedForm = _altinnService.GetItemByArchiveReference(archiveReference);
                    LogFormInfo(archivedForm);
                }
                catch (Exception exception)
                {
                    _logger.Debug(exception, "Exception when downloading from DLQ");
                    _logEntryService.Save(new LogEntry(archiveReference,
                        "En feil oppstod ved henting av skjema fra Altinns tjenestearkiv for DiBK.: " +
                        exception.Message, "Error"));
                    throw; // rethrow exception so hangfire will retry
                }

                //Some attributes from the arhived item is needed for later processing
                _exportToSubmittalQueueService.SaveArchivedItemToBlobStorage(archiveReference, archivedForm);

                FormData formData = null;
                var ftId = GetFtId(archivedForm.Forms.First());

                formData = _formDeserializer.DeserializeForm(archivedForm);

                //Clears the input to reduce memory consumption
                var serviceCode = archivedForm.ServiceCode;
                archivedForm = null;

                _logger.Debug("Downloading PDF");

                var attachment = new Attachment
                {
                    AttachmentData = _altinnService.GetPdfForm(formData.ArchiveReference, formData.MainformDataFormatID, formData.MainformDataFormatVersionID),
                    ArchiveReference = formData.ArchiveReference,
                    AttachmentType = "application/pdf",
                    AttachmentTypeName = AttachmentSetting.GetAttachmentTypeName(serviceCode),
                    FileName = AttachmentSetting.GetMainFormPDFAttachmentFileName(serviceCode),
                    IsEncrypted = false
                };
                formData.MainFormPdf = attachment;

                foreach (var subform in formData.Subforms)
                {
                    subform.AddPdfFile(_altinnService.GetPdfForm(formData.ArchiveReference, subform.DataFormatId, subform.DataFormatVersion));
                }

                _formMetadataService.SaveFormDataToFormMetadataLog(formData);
                _formMetadataService.UpdateStatusToFormMetadata(archiveReference, "Under behandling");

                _exportToSubmittalQueueService.HandoverToCoreFunctions(formData);

                LogFtId(archiveReference, ftId);
                LogSubforms(formData);

                BackgroundJob.Enqueue(() => PurgeItem(archiveReference));
            }
        }

        [HangfireFormProcessingJobLogger]
        [DisplayName("Ship item #{0} to SvarUt")]
        public void ShipItem(string archiveReference, bool queueFormToSubmittalQueue)
        {
            using (LogContext.PushProperty("ArchiveReference", archiveReference))
            {
                _logger.Debug("Form is being processed for shipping");

                ArchivedFormTaskDQBE archivedForm;

                try
                {
                    archivedForm = _altinnService.GetItemByArchiveReference(archiveReference); // Get item from Altinn
                    LogFormInfo(archivedForm);
                }
                catch (Exception exception)
                {
                    _logger.Debug(exception, "Exception when downloading from DLQ");
                    _logEntryService.Save(new LogEntry(archiveReference,
                        "En feil oppstod ved henting av skjema fra Altinns tjenestearkiv for DiBK.: " +
                        exception.Message, "Error"));
                    throw; // rethrow exception so hangfire will retry
                }

                ShipAltinnForm(archiveReference, archivedForm, queueFormToSubmittalQueue);
            }
        }

        public void ShipAltinnForm(string archiveReference, ArchivedFormTaskDQBE archivedForm, bool queueFormToSubmittalQueue)
        {
            FormData formData = null;
            int validationWarnings = 0;
            var reportee = archivedForm.Reportee;
            var serviceCode = archivedForm.ServiceCode;
            var ftbId = string.Empty;

            try
            {
                _logger.Debug("Form is being validated for structure issues");
                //Validate structure
                string mainDataFormatID = archivedForm.Forms.First().DataFormatID;
                string mainDataFormatVersionID = archivedForm.Forms.First().DataFormatVersionID.ToString();

                ftbId = GetFtId(archivedForm.Forms.First());

                if (!IsFormProcessable(archivedForm))
                    return;

                formData = _formDeserializer.DeserializeForm(archivedForm);

                //Logge metadata om innsending
                _formMetadataService.SaveFormDataToFormMetadataLog(formData);

                //Validate data
                _logger.Debug("Form is being validated for data issues");
                // Validate data
                var valResultData = formData.Mainform.ValidateData();
                // Validate Altinn reportee
                formData.Mainform.ValidateAuthenticatedSubmitterVsFormData(reportee, valResultData);
                if (valResultData.HasErrors())
                {
                    _logger.Debug("Failed validation for data issues");

                    string title = String.Format(Resources.TextStrings.ShippingErrorTitle, archiveReference);
                    string summary = Resources.TextStrings.ShippingErrorSummary;
                    string body = String.Format(Resources.TextStrings.ShippingErrorBody, archiveReference) + Resources.TextStrings.ShippingErrorValidation + Environment.NewLine + valResultData.ToString();
                    _correspondenceHelper.SendSimpleNotificaitonToReportee(reportee, title, summary, body, archiveReference);

                    _logEntryService.Save(new LogEntry(archiveReference, Resources.TextStrings.ShippingErrorValidation + valResultData.ToString(), "Error"));
                    _formMetadataService.UpdateValidationResultToFormMetadata(archiveReference, "Feil - datavalidering", valResultData.Errors, valResultData.Warnings);

                    BackgroundJob.Enqueue(() => PurgeItem(archiveReference));

                    return;
                }
                if (valResultData.HasErrors() == false && valResultData.Warnings > 0)
                {
                    validationWarnings = valResultData.Warnings;
                    _logEntryService.Save(new LogEntry(archiveReference, "Valideringsresultat: " + valResultData.ToString(), "Info"));
                }

                //TODO remove and fix in svarutservice
                _logger.Debug("Downloading PDF");

                var attachment = new Attachment
                {
                    AttachmentData = _altinnService.GetPdfForm(formData.ArchiveReference, formData.MainformDataFormatID, formData.MainformDataFormatVersionID),
                    ArchiveReference = formData.ArchiveReference,
                    AttachmentType = "application/pdf",
                    AttachmentTypeName = AttachmentSetting.GetAttachmentTypeName(serviceCode),
                    FileName = AttachmentSetting.GetMainFormPDFAttachmentFileName(serviceCode),
                    IsEncrypted = false
                };
                formData.MainFormPdf = attachment;

                foreach (var subform in formData.Subforms)
                {
                    subform.AddPdfFile(_altinnService.GetPdfForm(formData.ArchiveReference, subform.DataFormatId, subform.DataFormatVersion));
                }
                //Add validation report to attachment (i praksis alt som er advarsler men går videre)
                var validationReport = MakeValidationreport(formData, valResultData);
                if (validationReport != null) formData.Attachments.Add(validationReport);
            }
            catch (Exception exception)
            {
                _logger.Debug(exception, "Exception when validating and processing form");
                _logEntryService.Save(new LogEntry(archiveReference, Resources.TextStrings.ShippingErrorDownloadAndDeserialize + exception.Message, "Error"));

                BackgroundJob.Enqueue(() => PurgeItem(archiveReference));

                return;
            }

            try
            {
                //Clears the input to reduce memory consumption
                archivedForm = null;

                _logger.Debug("Sending form for shipping to SvarUt");
                _formShippingsService.Ship(formData);

                _formMetadataService.UpdateValidationResultToFormMetadata(archiveReference, "Ok", 0, validationWarnings);

                LogFtId(archiveReference, ftbId);
                LogSubforms(formData);
                LogSoeknadsData(formData);

                BackgroundJob.Enqueue(() => PurgeItem(archiveReference));
            }
            catch (Exception exception)
            {
                _logger.Debug(exception, "Exception sending form for SvarUt shipping");

                _logEntryService.Save(new LogEntry(archiveReference, Resources.TextStrings.ShippingErrorSvarUt + exception.Message, "Error"));

                string title = String.Format(Resources.TextStrings.ShippingErrorTitle, archiveReference);
                string summary = Resources.TextStrings.ShippingErrorSummary;
                string body = String.Format(Resources.TextStrings.ShippingErrorBody, archiveReference) + Environment.NewLine + Resources.TextStrings.ShippingErrorSvarUt + exception.Message;
                _correspondenceHelper.SendSimpleNotificaitonToReportee(reportee, title, summary, body, archiveReference);
                _formMetadataService.UpdateValidationResultToFormMetadata(archiveReference, "Feil", 1, 0);

                BackgroundJob.Enqueue(() => PurgeItem(archiveReference));

                return;
            }
        }

        private static Attachment MakeValidationreport(FormData formData, ValidationResult valResultData)
        {
            if (valResultData != null)
            {
                var xmlserializer = new XmlSerializer(typeof(ValidationResult));
                var stringWriter = new StringWriter();
                string validationResult = "";
                using (var writer = XmlWriter.Create(stringWriter))
                {
                    xmlserializer.Serialize(writer, valResultData);
                    validationResult = stringWriter.ToString();
                }

                var validationReport = new Attachment
                {
                    AttachmentData = Encoding.UTF8.GetBytes(validationResult),
                    ArchiveReference = formData.ArchiveReference,
                    AttachmentType = "application/xml",
                    AttachmentTypeName = "KONTROLL",
                    FileName = "valideringsrapportFraFellestjenesterBygg.xml",
                    IsEncrypted = false
                };
                return validationReport;
            }
            else return null;
        }

        [HangfireFormProcessingJobLogger]
        [DisplayName("Distribution list #{0} for processing")]
        public void ItemForDistribution(string archiveReference, bool queueFormToSubmittalQueue)
        {
            using (LogContext.PushProperty("ArchiveReference", archiveReference))
            {
                _logger.Debug("Form is being processed for distribution");
                ArchivedFormTaskDQBE archivedForm;
                try
                {
                    _logger.Debug("Getting form from Altinn");
                    archivedForm = _altinnService.GetItemByArchiveReference(archiveReference);

                    LogFormInfo(archivedForm);
                }
                catch (Exception exception)
                {
                    _logger.Debug(exception, "Exception getting form from Altinn");
                    _logEntryService.Save(new LogEntry(archiveReference, "En feil oppstod ved henting av skjema fra Altinns tjenestearkiv for DiBK.: " + exception.Message, "Error"));
                    throw; // rethrow exception so hangfire will retry
                }

                try
                {
                    if (queueFormToSubmittalQueue)
                    {
                        _formDistributionService = _exportToSubmittalQueueService;

                        //Some attributes from the archived item is needed for later processing
                        _exportToSubmittalQueueService.SaveArchivedItemToBlobStorage(archiveReference, archivedForm);
                    }

                    ProcessItemForDistribution(archiveReference, archivedForm);
                }
                catch (Exception exception)
                {
                    _logger.Debug(exception, "Exception when processing form for distribution");
                    _logEntryService.Save(new LogEntry(archiveReference,
                        "Et unntak oppstod ved behandling av distribusjon skjema" + exception.Message, "Error"));
                    throw; // rethrow exception so hangfire will retry                 }
                }
            }
        }

        private void SaveFormAttachmentMetadata(string archiveReference, ArchivedAttachmentExternalListDQBE archivedAttachments)
        {
            var l = new List<FormAttachmentMetadata>();

            foreach (var formAttachment in archivedAttachments)
            {
                formAttachment.FileName = formAttachment.FileName.Replace("&amp;", "og");
                l.Add(new FormAttachmentMetadata()
                {
                    ArchiveReference = archiveReference,
                    AttachmentType = formAttachment.AttachmentTypeName,
                    FileName = formAttachment.FileName,
                    Size = formAttachment.AttachmentData.Length,
                    Source = "UserProvided",
                    MimeType = MimeTypeResolver.GetMimeType(formAttachment.FileName)
                });
            }

            if (l.Count > 0)
                _formMetadataService.SaveFormAttachmentMetadata(l);
        }

        private void ProcessItemForDistribution(string archiveReference, ArchivedFormTaskDQBE archivedForm)
        {
            try
            {
                SendToDistribution(archiveReference, archivedForm);
            }
            catch (Exception exception)
            {
                _logger.Debug(exception, "Exception during distribution ");
                _logEntryService.Save(new LogEntry(archiveReference, "En feil oppstod ved distribusjon av Altinnskjema for DiBK.: " + exception.Message, "Error"));
                throw;
            }
        }

        public void SendToDistribution(string archiveReference, ArchivedFormTaskDQBE archivedForm)
        {
            FormData distributionServiceFormData;
            int validationWarnings = 0;
            var reportee = archivedForm.Reportee;
            var serviceCode = archivedForm.ServiceCode;
            var ftbId = string.Empty;

            try
            {
                _logEntryService.Save(new LogEntry(archiveReference, Resources.TextStrings.LogMessageDownloadedForm, "Info"));
                string mainDataFormatID = archivedForm.Forms.First().DataFormatID;
                string mainDataFormatVersionID = archivedForm.Forms.First().DataFormatVersionID.ToString();

                ftbId = GetFtId(archivedForm.Forms.First());

                if (!IsFormProcessable(archivedForm))
                    return;

                distributionServiceFormData = _formDeserializer.DeserializeForm(archivedForm);

                //Clears the input to reduce memory consumption
                archivedForm = null;

                //Logge metadata om innsending
                _formMetadataService.SaveFormDataToFormMetadataLog(distributionServiceFormData);
                //Validate data
                _logger.Debug("Form is being validated for data issues");
                var valResultData = distributionServiceFormData.Mainform.ValidateData();
                // Validate Altinn reportee
                distributionServiceFormData.Mainform.ValidateAuthenticatedSubmitterVsFormData(reportee, valResultData);

                if (valResultData.HasErrors())
                {
                    _logger.Debug("Form failed validation for data issues");

                    string title = String.Format(Resources.TextStrings.ShippingErrorTitle, archiveReference);
                    string summary = Resources.TextStrings.ShippingErrorSummary;
                    string body = String.Format(Resources.TextStrings.ShippingErrorBody, archiveReference) + Resources.TextStrings.ShippingErrorValidation + Environment.NewLine + valResultData.ToString();
                    _correspondenceHelper.SendSimpleNotificaitonToReportee(reportee, title, summary, body, archiveReference);

                    _logEntryService.Save(new LogEntry(archiveReference, Resources.TextStrings.ShippingErrorValidation + valResultData.ToString(), "Error"));
                    _formMetadataService.UpdateValidationResultToFormMetadata(archiveReference, "Feil - datavalidering", valResultData.Errors, valResultData.Warnings);

                    BackgroundJob.Enqueue(() => PurgeItem(archiveReference));
                    return;
                }
                if (valResultData.HasErrors() == false && valResultData.Warnings > 0)
                {
                    validationWarnings = valResultData.Warnings;
                    //Logge advarsler
                    _logEntryService.Save(new LogEntry(archiveReference, "Valideringsresultat: " + valResultData.ToString(), "Info"));
                }

                if (_formDistributionService is ExportToSubmittalQueueService)
                {
                    _exportToSubmittalQueueService.SaveValidationResultToBlobStorage(archiveReference, valResultData);
                }

                // Get main for PDF from ALtinn
                _logger.Debug("Downloading PDF");
                var attachment = new Attachment
                {
                    AttachmentData = GetAttachmentData(archiveReference, distributionServiceFormData),
                    ArchiveReference = archiveReference,
                    AttachmentType = "application/pdf",
                    AttachmentTypeName = AttachmentSetting.GetAttachmentTypeName(serviceCode),
                    FileName = AttachmentSetting.GetMainFormPDFAttachmentFileName(serviceCode),
                    IsEncrypted = false
                };
                distributionServiceFormData.MainFormPdf = attachment;

                foreach (var subform in distributionServiceFormData.Subforms)
                {
                    subform.AddPdfFile(_altinnService.GetPdfForm(archiveReference, subform.DataFormatId, subform.DataFormatVersion));
                }
            }
            catch (Exception exception)
            {
                _logger.Error(exception, "Error occured for distribution");
                _logEntryService.Save(new LogEntry(archiveReference, Resources.TextStrings.ShippingErrorDownloadAndDeserialize + exception.Message, "Error"));

                BackgroundJob.Enqueue(() => PurgeItem(archiveReference));

                return;
            }

            try
            {
                _logger.Debug("Sending form to distribution processing");
                _formDistributionService.Distribute(distributionServiceFormData);

                if (_formDistributionService is ExportToSubmittalQueueService)
                    _formMetadataService.UpdateValidationResultToFormMetadata(archiveReference, "Under behandling", 0, validationWarnings);
                else
                    _formMetadataService.UpdateValidationResultToFormMetadata(archiveReference, "Ok", 0, validationWarnings);

                LogFtId(archiveReference, ftbId);
                LogSubforms(distributionServiceFormData);

                BackgroundJob.Enqueue(() => PurgeItem(archiveReference));
            }
            catch (Exception exception)
            {
                _logger.Debug(exception, "Exception in distribution processing");
                _logEntryService.Save(new LogEntry(archiveReference, "En feil oppstod ved behandling av distribusjonsskjema: " + exception.Message, "Error"));

                BackgroundJob.Enqueue(() => PurgeItem(archiveReference));

                return;
            }
        }

        private byte[] GetAttachmentData(string archiveReference, FormData distributionServiceFormData)
        {
            try
            {
                return _altinnService.GetPdfForm(archiveReference, distributionServiceFormData.MainformDataFormatID, distributionServiceFormData.MainformDataFormatVersionID);
            }
            catch (Exception exception)
            {
                _logger.Debug(exception, "Exception during getting PDF from Altinnservice");
                _logEntryService.Save(new LogEntry(archiveReference, "En feil oppstod ved henting av vedlegg fra Altinn: " + exception.Message, "Error"));
                throw;
            }
        }

        [HangfireFormProcessingJobLogger]
        [DisplayName("Item #{0} for distribution reciept")]
        public void ItemForNotification(string archiveReference, bool queueFormToSubmittalQueue)
        {
            using (LogContext.PushProperty("ArchiveReference", archiveReference))
            {
                _logger.Debug("Form is being processed for notification");

                ArchivedFormTaskDQBE archivedForm;

                try
                {
                    _logger.Debug("Getting form from Altinn");
                    archivedForm = _altinnService.GetItemByArchiveReference(archiveReference); // Get item from Altinn
                    LogFormInfo(archivedForm);

                    if (queueFormToSubmittalQueue)
                    {
                        _exportToSubmittalQueueService.SaveArchivedItemToBlobStorage(archiveReference, archivedForm); //Some attributes from the arhived item is needed for later processing
                    }
                }
                catch (Exception exception)
                {
                    _logger.Debug(exception, "Exception getting form from Altinn");
                    _logEntryService.Save(new LogEntry(archiveReference, "En feil oppstod ved henting av skjema fra Altinns tjenestearkiv for DiBK.: " + exception.Message, "Error"));
                    throw; // rethrow exception so hangfire will retry
                }

                SendToNotification(archiveReference, archivedForm);
            }
        }

        public void SendToNotification(string archiveReference, ArchivedFormTaskDQBE archivedForm)
        {
            FormData formData;
            int validationWarnings = 0;
            var reportee = archivedForm.Reportee;
            var serviceCode = archivedForm.ServiceCode;
            var ftbId = string.Empty;

            try
            {
                _logger.Debug("Form is being validated for structure issues");
                //Validate structure                ;
                string mainDataFormatID = archivedForm.Forms.First().DataFormatID;
                string mainDataFormatVersionID = archivedForm.Forms.First().DataFormatVersionID.ToString();

                ftbId = GetFtId(archivedForm.Forms.First());

                if (!IsFormProcessable(archivedForm))
                    return;

                formData = _formDeserializer.DeserializeForm(archivedForm);

                //Clears the input to reduce memory consumption
                archivedForm = null;

                _formMetadataService.SaveFormDataToFormMetadataLog(formData);

                //Validate data
                _logger.Debug("Form is being validated for data issues");
                var valResultData = formData.Mainform.ValidateData();

                // Validate Altinn reportee
                if (ShouldNotificationFormBeValidatedForCorrectReportee(serviceCode))
                {
                    formData.Mainform.ValidateAuthenticatedSubmitterVsFormData(reportee, valResultData);
                }

                if (valResultData.HasErrors())
                {
                    _logger.Debug("Form failed validation for data issues");

                    string title = String.Format(Resources.TextStrings.ShippingErrorTitle, archiveReference);
                    string summary = Resources.TextStrings.ShippingErrorSummary;
                    string body = String.Format(Resources.TextStrings.ShippingErrorBody, archiveReference) + Resources.TextStrings.ShippingErrorValidation + Environment.NewLine + valResultData.ToString();
                    _correspondenceHelper.SendSimpleNotificaitonToReportee(reportee, title, summary, body, archiveReference);

                    _logEntryService.Save(new LogEntry(archiveReference, Resources.TextStrings.ShippingErrorValidation + valResultData.ToString(), "Error"));
                    _formMetadataService.UpdateValidationResultToFormMetadata(archiveReference, "Feil - datavalidering", valResultData.Errors, valResultData.Warnings);

                    BackgroundJob.Enqueue(() => PurgeItem(archiveReference));

                    return;
                }
                if (valResultData.HasErrors() == false && valResultData.Warnings > 0)
                {
                    validationWarnings = valResultData.Warnings;
                    //Logge advarsler
                    _logEntryService.Save(new LogEntry(archiveReference, "Valideringsresultat: " + valResultData.ToString(), "Info"));
                }

                if (_notificationServiceResolver.GetService(formData.MainformDataFormatID) is ExportToSubmittalQueueService)
                {
                    _exportToSubmittalQueueService.SaveValidationResultToBlobStorage(archiveReference, valResultData);
                }

                _logger.Debug("Getting PDF");
                var attachment = new Attachment
                {
                    AttachmentData = _altinnService.GetPdfForm(formData.ArchiveReference, formData.MainformDataFormatID, formData.MainformDataFormatVersionID),
                    ArchiveReference = formData.ArchiveReference,
                    AttachmentType = "application/pdf",
                    AttachmentTypeName = AttachmentSetting.GetAttachmentTypeName(serviceCode),
                    FileName = AttachmentSetting.GetMainFormPDFAttachmentFileName(serviceCode),
                    IsEncrypted = false
                };
                formData.MainFormPdf = attachment;

                foreach (var subform in formData.Subforms)
                {
                    subform.AddPdfFile(_altinnService.GetPdfForm(formData.ArchiveReference, subform.DataFormatId, subform.DataFormatVersion));
                }
            }
            catch (Exception ex)
            {
                _logger.Debug(ex, "Exception while deserializing data/getting PDF");
                _logEntryService.Save(new LogEntry(archiveReference, Resources.TextStrings.ShippingErrorDownloadAndDeserialize + ex.Message, "Error"));

                BackgroundJob.Enqueue(() => PurgeItem(archiveReference));

                return;
            }
            try
            {
                _logger.Debug("Sending form for notification processing");

                _notificationServiceResolver.GetService(formData.MainformDataFormatID).SendNotification(formData);
                _formMetadataService.UpdateValidationResultToFormMetadata(archiveReference, "Ok", 0, validationWarnings);

                LogFtId(archiveReference, ftbId);
                LogSubforms(formData);

                BackgroundJob.Enqueue(() => PurgeItem(archiveReference));
            }
            catch (Exception exception)
            {
                _logger.Debug(exception, "Exception in notification processing");

                _logEntryService.Save(new LogEntry(archiveReference, "En feil oppstod ved behandling av skjema for meldingstjeneste: " + exception.Message, "Error"));

                string title = String.Format(Resources.TextStrings.ShippingErrorTitle, archiveReference);
                string summary = Resources.TextStrings.ShippingErrorSummary;
                string body = String.Format(Resources.TextStrings.ShippingErrorBody, archiveReference) + Environment.NewLine + Resources.TextStrings.ShippingErrorNotification + exception.Message;
                _correspondenceHelper.SendSimpleNotificaitonToReportee(reportee, title, summary, body, archiveReference);
                _formMetadataService.UpdateValidationResultToFormMetadata(archiveReference, "Feil", 1, 0);

                BackgroundJob.Enqueue(() => PurgeItem(archiveReference));

                return;
            }
        }

        [Queue(queue: HangfireQueueNames.Purge)]
        [HangfireFormProcessingJobLogger]
        [DisplayName("Purge item #{0} from Altinn DownloadQueue")]
        public void PurgeItem(string archiveReference)
        {
            var formMetadata = _formMetadataService.GetFormMetadata(archiveReference);

            if (formMetadata != null && formMetadata.Status.Equals("I kø", StringComparison.CurrentCultureIgnoreCase))
            {
                _logEntryService.Save(new LogEntry(archiveReference, "Reached action to purge form from Altinn DLQ while in queued state, skipping command", LogEntry.Error, LogEntry.InternalMsg));
                _logEntryService.Save(new LogEntry(archiveReference, "OPERATIONS HANDS ON debugging needed to resolve situation", LogEntry.Error, LogEntry.InternalMsg));
                _logger.Error("Reached action to purge form from Altinn DLQ while in queued state, skipping command");
            }
            else
            {
                _logEntryService.Save(new LogEntry(archiveReference, "Form purged from Altinn", LogEntry.Info, LogEntry.InternalMsg));
                _logger.Debug("Purging form from Altinn DLQ");
                _altinnService.PurgeItem(archiveReference);
            }
        }

        private string GetFtId(ArchivedFormDQBE form)
        {
            return XmlUtil.GetElementValue(form.FormData, "ftbid");
        }

        private void LogFtId(string archiveReference, string ftId)
        {
            try
            {

                if (!string.IsNullOrEmpty(ftId))
                    if (_ftIdService.FtIdExists(ftId))
                    {
                        _logEntryService.Save(new LogEntry(archiveReference, $"The form has an FtId assigned: {ftId}", LogEntry.Info, LogEntry.InternalMsg));
                        _logger.Information("Form has FtId {FtId} assigned", ftId);

                        _formMetadataService.AddFtId(archiveReference, ftId);
                    }
                    else
                    {
                        _logEntryService.Save(new LogEntry(archiveReference, $"The forms FtId is invalid: {ftId}", LogEntry.Info, LogEntry.InternalMsg));
                        _logger.Warning("Form FtId has not been assigned by FtPB: {FtId}", ftId);
                    }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "An unexpected error occured while logging FtId");
            }
        }

        private bool ShouldNotificationFormBeValidatedForCorrectReportee(string serviceCodeForNotificationForm)
        {
            var serviceCodesForReporteeVal = ConfigurationManager.AppSettings["Altinn.ValidateReporteeForNotification"].
                Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries).
                Select(p => p.Trim()).ToList();

            return serviceCodesForReporteeVal.Contains(serviceCodeForNotificationForm);
        }

        private void LogSubforms(FormData formData)
        {
            try
            {
                if (formData?.Subforms?.Count > 0)
                {
                    var subformDocs = new List<SubformDocument>();
                    foreach (var subForm in formData.Subforms)
                    {
                        var subformDoc = new SubformDocument()
                        {
                            Id = $"{formData.ArchiveReference}-{CryptographyHelpers.GenerateMD5Hash($"{formData.ArchiveTimestamp.Value} - {subForm.FormDataXml}")}",
                            ArchiveTimestamp = formData.ArchiveTimestamp.ToElkDateTime(),
                            MainformArchiveReference = formData.ArchiveReference,
                            MainformDataformatId = formData.MainformDataFormatID,
                            MainformDataformatVersion = formData.MainformDataFormatVersionID,
                            MainformName = formData.Mainform.GetName(),
                            SubformDataformatId = subForm.DataFormatId,
                            SubformDataformatVersion = subForm.DataFormatVersion,
                            SubformName = subForm.FormName
                        };
                        subformDocs.Add(subformDoc);
                    }

                    AsyncHelper.RunSync(async () => await _searchEngine.AddManyAsync(subformDocs));
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Unable to log subforms to Elasticsearch");
            }
        }

        private void LogFormInfo(ArchivedFormTaskDQBE archivedForm)
        {
            var archiveReference = archivedForm.ArchiveReference;

            _formMetadataService.UpdateServiceCodeToFormMetadata(archiveReference, archivedForm.ServiceCode, archivedForm.ServiceEditionCode);
            _logEntryService.Save(new LogEntry(archiveReference, Resources.TextStrings.LogMessageDownloadedForm + " Med " + archivedForm.AttachmentsInResponse + " vedlegg og " + archivedForm.FormsInResponse + " skjema.", "Info"));

            var formsLogMessageBuilder = new StringBuilder();
            formsLogMessageBuilder.Append($"Skjema (dataformatid, dataformatversjon): ");
            var forms = string.Join(", ", archivedForm.Forms.Select(p => $"({p.DataFormatID}, {p.DataFormatVersionID})"));
            formsLogMessageBuilder.Append(forms);

            _logEntryService.Save(new LogEntry(archiveReference, formsLogMessageBuilder.ToString(), "Info"));
            _logger.Information("{SubformList}", formsLogMessageBuilder.ToString());

            if (archivedForm.AttachmentsInResponse > 0)
            {
                var attachmentLogMessageBuilder = new StringBuilder();
                attachmentLogMessageBuilder.Append("Vedlegg: ");
                var attachments = string.Join(", ", archivedForm.Attachments.Select(p => $"{p.AttachmentTypeName} {p.FileName}[{p.AttachmentData.Length}]"));
                attachmentLogMessageBuilder.Append(attachments);

                _logEntryService.Save(new LogEntry(archiveReference, attachmentLogMessageBuilder.ToString(), "Info"));

                int attachmentDataTotal = archivedForm.Attachments.Sum(t => t.AttachmentData.Length);
                _logger.Debug("Attachments in total {AttachmetDataTotal} bytes.", attachmentDataTotal);
                _logger.Debug("Attachments on form are: {AttachmentInfo}", attachmentLogMessageBuilder.ToString());

                SaveFormAttachmentMetadata(archiveReference, archivedForm.Attachments);
            }
        }

        private void LogSoeknadsData(FormData formData)
        {
            try
            {
                var formDataXml = formData.FormDataAsXml;

                var tiltakstypesV3 = XmlUtil.GetCodeListByElementNameOrXpath(formDataXml, null, "//beskrivelseAvTiltak/type");
                var tiltakstypesV2 = XmlUtil.GetCodeListByElementNameOrXpath(formDataXml, null, "//beskrivelseAvTiltak/tiltak/type");
                var tiltakstypesTa = XmlUtil.GetCodeListByElementNameOrXpath(formDataXml, null, "//beskrivelseAvTiltak/type/type");

                var tiltakstyper = new List<string>();
                if (tiltakstypesV3 != null)
                    tiltakstyper.AddRange(tiltakstypesV3);
                if (tiltakstypesV2 != null)
                    tiltakstyper.AddRange(tiltakstypesV2);
                if (tiltakstypesTa != null)
                    tiltakstyper.AddRange(tiltakstypesTa);

                var dispensasjonTypes = XmlUtil.GetCodeListByElementNameOrXpath(formDataXml, "dispensasjonstype");
                var anleggstype = XmlUtil.GetCodeListByElementNameOrXpath(formDataXml, "anleggstype");
                var naeringsgruppe = XmlUtil.GetCodeListByElementNameOrXpath(formDataXml, "naeringsgruppe");
                var bygningstype = XmlUtil.GetCodeListByElementNameOrXpath(formDataXml, "bygningstype");
                var tiltaksformaal = XmlUtil.GetCodeListByElementNameOrXpath(formDataXml, "tiltaksformaal");

                var soeknadsData = new SoeknadsData()
                {
                    ArchiveTimestamp = formData.ArchiveTimestamp.ToElkDateTime(),
                    ArchiveReference = formData.ArchiveReference,
                    Kommunenummer = formData.Mainform.GetPropertyIdentifiers().Kommunenummer,
                    Tiltakstyper = tiltakstyper,
                    Dispensasjonstyper = dispensasjonTypes,
                    FormType = formData.Mainform.GetName(),
                    Application = formData.Mainform.GetPropertyIdentifiers().FraSluttbrukersystem,
                    Anleggstype = anleggstype,
                    Bygningstype = bygningstype,
                    Naeringsgruppe = naeringsgruppe,
                    Tiltaksformaal = tiltaksformaal,
                };

                _searchEngine.Add(soeknadsData);
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Unable to log søknadsdata to Elasticsearch");
            }
        }
    }
}