﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KommIT.FIKS.AdapterAltinnSvarUt.Models;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.WS
{
    public interface IDownloadStreamAttachment
    {
        byte[] GetAttachmentDataStreamed(int attachmentId, string username, string password);

    }
}
