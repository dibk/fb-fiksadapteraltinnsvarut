﻿using System;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.WS.AltinnCorrespondenceAgency;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Altinn
{
    public interface IAltinnCorrespondenceService
    {
        ReceiptExternal SendNotification(string externalShipRef, InsertCorrespondenceV2 correspondence);
        CorrespondenceStatusResultV3 GetCorrespondenceStatus(string externalShipRef, string servicecode, int serviceedition);
        CorrespondenceStatusResultV3 GetCorrespondenceStatus(string externalShipRef, string servicecode, int serviceedition, DateTime instansiationTimeOfCorrespondence);
        void CloseClient();
    }
}