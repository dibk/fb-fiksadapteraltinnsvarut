﻿using Polly;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Altinn.Prefill
{
    public interface IPrefillPolicyProvider
    {
        Policy GetPolicy();
    }
}