﻿using Polly;
using Serilog;
using System;
using System.Collections.Generic;
using System.ServiceModel;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Altinn.Prefill
{
    public class PrefillPolicyProvider : IPrefillPolicyProvider
    {
        private readonly ILogger _log = Log.ForContext<PrefillPolicyProvider>();
        private readonly IPrefillPolicyConfigurationProvider _prefillPolicyConfigurationProvider;

        public PrefillPolicyProvider(IPrefillPolicyConfigurationProvider prefillPolicyConfigurationProvider)
        {
            _prefillPolicyConfigurationProvider = prefillPolicyConfigurationProvider;
        }
        public Policy GetPolicy()
        {
            List<int> retryAttemptWaitStructure = _prefillPolicyConfigurationProvider.RetryAttemptWaitSecondsStructure;
            var retryCount = 0;
            var policy = Policy.Handle<ServerTooBusyException>()
                .Or<TimeoutException>()
                .Or<EndpointNotFoundException>()
                .Or<CommunicationException>(c => c.Message.Contains("A connection that was expected to be kept alive was closed by the server"))                
                .WaitAndRetry(
                retryCount: retryAttemptWaitStructure.Count,
                sleepDurationProvider: attempt =>
                {
                    var jitterer = new Random();
                    var timeSpan = TimeSpan.FromSeconds(retryAttemptWaitStructure[retryCount])
                           + TimeSpan.FromMilliseconds(jitterer.Next(0, 1000));
                    retryCount++;

                    return timeSpan;
                },
                onRetry: (exception, calculatedWaitDuration) =>
                {
                    _log.Debug(exception, "Failed to contact {RetryApi} {RetryCount} time(s). Waits for {CalculatedWaitDuration} and retries", "Altinn-Prefill", retryCount, calculatedWaitDuration);
                });

            return policy;
        }

    }
}