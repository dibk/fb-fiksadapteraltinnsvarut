﻿namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Altinn.Prefill
{
    public interface IPrefillPolicyConfigurationProvider : IPolicyConfigurationProvider { }
    public class PrefillPolicyConfigurationProvider : PolicyConfigurationProvider, IPrefillPolicyConfigurationProvider
    {
        public override string RetryStructureSettingKey => "Polly:PrefillClient:RetryStructure";
    }
}