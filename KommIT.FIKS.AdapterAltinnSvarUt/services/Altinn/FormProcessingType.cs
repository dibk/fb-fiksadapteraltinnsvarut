﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Altinn
{
    public enum FormProcessingType
    {
        Shipping,
        Notification,
        Distribution,
        CoreFunctionForwarder,
        Unknown
    }
}