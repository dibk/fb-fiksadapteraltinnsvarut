﻿using KommIT.FIKS.AdapterAltinnSvarUt.Services.WS.AltinnPreFill;
using System;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Altinn
{
    /// <summary>
    ///  Interface for instantiating Altinn forms with prefill data
    /// </summary>
    public interface IAltinnPrefillService
    {
        ReceiptExternal SubmitAndInstantiatePrefilledForm(PrefillFormTask prefillFormTask, DateTime? dueDate);
        void CloseClient();
    }
}
