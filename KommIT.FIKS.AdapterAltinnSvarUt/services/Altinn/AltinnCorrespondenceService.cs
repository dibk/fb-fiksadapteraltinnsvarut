﻿using System;
using System.Configuration;
using System.ServiceModel;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Altinn.Correspondence;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.WS.AltinnCorrespondenceAgency;
using Serilog;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Altinn
{
    public class AltinnCorrespondenceService : IAltinnCorrespondenceService
    {

        private readonly string _username;
        private readonly string _password;
        private readonly string _systemUserCode;
        private readonly ILogger _logger;
        private CorrespondenceAgencyExternalBasicClient _correspondenceClient;
        private readonly ICorrespondencePolicyProvider _altinnCorrespondencePolicyProvider;

        public AltinnCorrespondenceService(ILogger logger, ICorrespondencePolicyProvider altinnCorrespondencePolicyProvider)
        {
            _username = ConfigurationManager.AppSettings["Altinn.DIBK.Username"];
            _password = ConfigurationManager.AppSettings["Altinn.DIBK.Password"];
            _systemUserCode = ConfigurationManager.AppSettings["Altinn.DIBK.ServiceOwnerCode"];
            _logger = logger;
            _altinnCorrespondencePolicyProvider = altinnCorrespondencePolicyProvider;
        }

        private CorrespondenceAgencyExternalBasicClient GetClient()
        {
            if (_correspondenceClient == null || _correspondenceClient.State != CommunicationState.Opened)
                _correspondenceClient = new CorrespondenceAgencyExternalBasicClient();
            return _correspondenceClient;

        }

        public ReceiptExternal SendNotification(string externalShipRef, InsertCorrespondenceV2 correspondence)
        {
            try
            {
                ReceiptExternal receiptExternal = null;

                var policy = _altinnCorrespondencePolicyProvider.GetPolicy();

                policy.Execute(() =>
                {
                    try
                    {
                        receiptExternal = GetClient().InsertCorrespondenceBasicV2(
                            _username,
                            _password,
                            _systemUserCode,
                            externalShipRef,
                            correspondence);
                    }
                    catch (CommunicationException)
                    {
                        _correspondenceClient.Abort();
                        throw;
                    }
                    catch (TimeoutException)
                    {
                        _correspondenceClient.Abort();
                        throw;
                    }
                    catch (Exception)
                    {
                        _correspondenceClient.Abort();
                        throw;
                    }

                    Logger.TelemetryLogger.LogAltinnTelemetry<AltinnCorrespondenceService>("Correspondence", _correspondenceClient.Endpoint.Address.Uri, correspondence.ServiceCode, correspondence.ServiceEdition.ToString());

                });

                return receiptExternal;
            }
            catch (FaultException<AltinnFault> e)
            {
                _logger.Error(e, "Error ({ExceptionType}) occurred when communicating with Altinn - {ExternalResource}. AltinnErrorMessage: {AltinnFaultErrorMessage} - ErrorId: {AltinnErrorCode} - ErrorGuid: {AltinnFaultErrorGuid} - AltinnExtendedErrorMessage: {AltinnFaultExtendedErrorMessage} - AltinnLocalizedErrorMessage: {AltinnFaultLocalizedErrorMessage}",
                    e.GetType().ToString(),
                    "CorrespondenceAgencyExternalBasicClient.InsertCorrespondenceBasicV2",
                    e.Detail.AltinnErrorMessage,
                    e.Detail.ErrorID,
                    e.Detail.ErrorGuid,
                    e.Detail.AltinnLocalizedErrorMessage,
                    e.Detail.AltinnExtendedErrorMessage);
                throw;
            }
            catch (FaultException e)
            {
                _logger.Error(e, "Error ({ExceptionType}) occurred when communicating with Altinn - {ExternalResource}. FaultMessage: {FaultMessage} - FaultAction: {FaultAction} - FaultCode: {FaultCode} -  FaultReason: {FaultReason}",
                    e.GetType().ToString(),
                    "CorrespondenceAgencyExternalBasicClient.InsertCorrespondenceBasicV2",
                    e.Message, e.Action, e.Code, e.Reason);
                throw;
            }
            catch (Exception e)
            {
                _logger.Error(e, "Error ({ExceptionType}) occurred when communicating with Altinn - {ExternalResource} - ExceptionMessage: {ExceptionMessage}", e.GetType().ToString(), "CorrespondenceAgencyExternalBasicClient.InsertCorrespondenceBasicV2", e.Message);
                throw;
            }
        }

        public CorrespondenceStatusResultV3 GetCorrespondenceStatus(string externalShipRef, string servicecode, int serviceedition)
        {
            CorrespondenceStatusFilterV3 filter = new CorrespondenceStatusFilterV3();
            filter.SendersReference = externalShipRef;
            filter.ServiceCode = servicecode;
            filter.ServiceEditionCode = serviceedition;

            return GetCorrespondenceStatusDetails(filter);
        }

        public CorrespondenceStatusResultV3 GetCorrespondenceStatus(string externalShipRef, string servicecode, int serviceedition, DateTime instansiationTimeOfCorrespondence)
        {
            CorrespondenceStatusFilterV3 filter = new CorrespondenceStatusFilterV3()
            {
                SendersReference = externalShipRef,
                ServiceCode = servicecode,
                ServiceEditionCode = serviceedition,
                CreatedAfterDate = instansiationTimeOfCorrespondence.AddMinutes(-15),
                CreatedBeforeDate = instansiationTimeOfCorrespondence.AddMinutes(15)
            };

            return GetCorrespondenceStatusDetails(filter);
        }

        private CorrespondenceStatusResultV3 GetCorrespondenceStatusDetails(CorrespondenceStatusFilterV3 filter)
        {
            var client = new CorrespondenceAgencyExternalBasicClient();
            try
            {
                CorrespondenceStatusResultV3 data = client.GetCorrespondenceStatusDetailsBasicV3(
                    _username,
                    _password,
                    filter);

                client.Close();
                return data;
            }
            catch (Exception e)
            {
                client.Abort();
                _logger.Error(e, "Error ({ExceptionType}) occurred when communicating with Altinn - {ExternalResource}", e.GetType().ToString(), "CorrespondenceAgencyExternalBasicClient.GetCorrespondenceStatusDetailsBasicV3");
                throw;
            }
        }

        public void CloseClient()
        {
            if (_correspondenceClient != null)
                try
                {
                    _correspondenceClient.Close();
                }
                catch (Exception ex)
                {
                    _logger.Error(ex, "Error occured when closing prefillClient, state: {ClientState}", _correspondenceClient?.State);
                }
        }
    }
}