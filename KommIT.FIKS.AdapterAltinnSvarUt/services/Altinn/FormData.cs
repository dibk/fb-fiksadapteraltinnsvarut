﻿using System;
using System.Collections.Generic;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Altinn;

using KommIT.FIKS.AdapterAltinnSvarUt.Services;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;
using no.kxml.skjema.dibk.vedlegg;
using System.Linq;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Models
{
    public class FormData
    {
        public string ArchiveReference { get; }
        public string FormDataAsXml { get; }

        public string SvarUtDocumentTitle { get; set; }
        public string ServiceCode { get; set; }
        public string ServiceEditionCode { get; set; }
        public string MainformDataFormatID { get; set; }
        public string MainformDataFormatVersionID { get; set; }
        public string Reportee { get; set; }

        public IAltinnForm Mainform { get; set; }

        public DateTime? ArchiveTimestamp { get; set; }

        public List<Attachment> Attachments { get; set; }

        public Attachment MainFormPdf { get; set; }

        public List<Subform> Subforms { get; set; }

        public List<SvarUtClassificationValue> SvarUtClassificationValues { get; set; }

        public List<SvarUtAttachmentSetting> SvarUtAttachmentSettings { get; set; }

        public FormData(string archiveReference, string formDataAsXml)
        {
            ArchiveReference = archiveReference;
            FormDataAsXml = formDataAsXml;
        }

        public VedleggsopplysningerType GetVedleggOpplysningerSkjema()
        {
            if (this.Subforms != null && this.Subforms.Count > 0)
            {
                var subform = this.Subforms.FirstOrDefault(p => p.DataFormatId == "5797");
                if (subform != null)
                {
                    return SerializeUtil.DeserializeFromString<VedleggsopplysningerType>(subform.FormDataXml);
                }
            }

            return null;
        }
    }

    public class SvarUtClassificationValue
    {
        public SvarUtClassificationValue(int sorting, string organizingPrinciple, string organizingValue)
        {
            Sorting = sorting;
            OrganizingPrinciple = organizingPrinciple;
            OrganizingValue = organizingValue;
        }

        public int Sorting { get; set; }
        public string OrganizingPrinciple { get; set; }
        public string OrganizingValue { get; set; }
    }
}