﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.ServiceModel;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.WS.AltinnNotification;
using Serilog;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Altinn
{
    public class AltinnNotificationService : IAltinnNotificationService
    {

        private readonly string _username;
        private readonly string _password;
        private readonly string _messageServiceCode;
        private readonly string _messageServiceEditionCode;
        private readonly ILogger _logger;

        public AltinnNotificationService(ILogger logger)
        {
            _username = ConfigurationManager.AppSettings["Altinn.DIBK.Username"];
            _password = ConfigurationManager.AppSettings["Altinn.DIBK.Password"];
            _messageServiceCode = ConfigurationManager.AppSettings["MessageServiceCode"];
            _messageServiceEditionCode = ConfigurationManager.AppSettings["MessageServiceCodeEdition"];
            _logger = logger;
        }

        public SendNotificationResultList SendStandaloneNotification(string reportee, string emailAdress, List<string> textTokensList)
        {
            try
            {
                var client = new NotificationAgencyExternalBasicClient();

                ReceiverEndPointBEList receiverEndPointBeList = GetReceiverEndPointList(emailAdress);
                TextTokenSubstitutionBEList textTokenSubstitutionBeList = GetTextTokens(textTokensList);

                StandaloneNotification standaloneNotification = new StandaloneNotification()
                {
                    IsReservable = true,
                    LanguageID = 1044,
                    NotificationType = Resources.TextStrings.AltinnNotificationTemplate,
                    ReporteeNumber = reportee,
                    ReceiverEndPoints = receiverEndPointBeList,
                    Service = new Service()
                    {
                        ServiceCode = _messageServiceCode,
                        ServiceEdition = Convert.ToInt32(_messageServiceEditionCode)
                    },
                    TextTokens = textTokenSubstitutionBeList,
                    ShipmentDateTime = DateTime.Now
                };

                try
                {
                    SendNotificationResultList data = client.SendStandaloneNotificationBasicV3(
                        _username,
                        _password,
                        new StandaloneNotificationBEList() { standaloneNotification });

                    client.Close();
                    Logger.TelemetryLogger.LogAltinnTelemetry<AltinnNotificationService>("Notification", client.Endpoint.Address.Uri, _messageServiceCode, _messageServiceEditionCode);
                    return data;
                }
                catch (CommunicationException)
                {
                    client.Abort();
                    throw;
                }
                catch (TimeoutException)
                {
                    client.Abort();
                    throw;
                }
                catch (Exception)
                {
                    client.Abort();
                    throw;
                }
            }
            catch (Exception e)
            {
                _logger.Error(e, "Error ({ExceptionType}) occurred when communicating with Altinn - {ExternalResource} - ExceptionMessage: {ExceptionMessage}", e.GetType().ToString(), "NotificationAgencyExternalBasicClient", e.Message);
                throw;
            }
        }


        private TextTokenSubstitutionBEList GetTextTokens(List<string> tokenList)
        {
            int tokenNum = 0;
            TextTokenSubstitutionBEList textTokens = new TextTokenSubstitutionBEList();

            foreach (var token in tokenList)
            {
                TextToken textToken = new TextToken()
                {
                    TokenNum = tokenNum,
                    TokenValue = token
                };
                textTokens.Add(textToken);
                tokenNum++;
            }

            return textTokens;
        }

        private ReceiverEndPointBEList GetReceiverEndPointList(string emailAddress)
        {
            ReceiverEndPointBEList receiverEndPointBeList = new ReceiverEndPointBEList();

            if (string.IsNullOrEmpty(emailAddress))
            {
                receiverEndPointBeList.Add(new ReceiverEndPoint
                {
                    TransportType = TransportType.EmailPreferred
                });
            }
            else
            {
                receiverEndPointBeList.Add(new ReceiverEndPoint
                {
                    ReceiverAddress = emailAddress,
                    TransportType = TransportType.Email
                });
            }
            return receiverEndPointBeList;
        }

        public string ReadMessageStatusForSingleNotification(SendNotificationResultList results)
        {
            if (results.Count == 1)
            {
                if (results[0].EndPoints.Count > 0)
                {
                    return $"Transport type {results[0].EndPoints[0].TransportType} from profile {results[0].EndPoints[0].RetrieveFromProfile}";
                }
            }
            return "Error. Unexpected result for single notification";
        }
    }
}