﻿using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using System;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Altinn
{
    public class Subform
    {
        public string FormDataXml { get; }
        public string DataFormatVersion { get; }
        public string DataFormatId { get; }
        public string FormName { get; }
        public byte[] PdfFileBytes { get; set; }
       

        public Subform(string dataFormatId, string dataFormatVersion, string formDataXml)
        {
            FormDataXml = formDataXml;
            DataFormatVersion = dataFormatVersion;
            DataFormatId = dataFormatId;
            FormName = AltinnFormTypeResolver.ResolveFormTypeFromDataFormatId(dataFormatId).ToString();
        }

        public void AddPdfFile(byte[] bytes)
        {
            PdfFileBytes = bytes;
        }
    }
}