﻿using System;
using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Altinn
{
    public static class AltinnFormTypeResolver
    {
        public static IEnumerable<IAltinnForm> GetForms()
        {
            var instances = from t in Assembly.GetExecutingAssembly().GetTypes()
                            where t.GetInterfaces().Contains(typeof(KommIT.FIKS.AdapterAltinnSvarUt.Services.IAltinnForm))
                                     && t.GetConstructor(Type.EmptyTypes) != null
                            select Activator.CreateInstance(t) as KommIT.FIKS.AdapterAltinnSvarUt.Services.IAltinnForm;
            return instances;
        }

        public static string ResolveFormTypeFromDataFormatId(string dataFormatId)
        {
            string name = "";
            var instances = GetForms();

            foreach (var item in instances)
            {
                if (item.GetDataFormatId() == dataFormatId)
                {
                    name = item.GetName();
                }
            }

            return name;
        }

        public static bool IsKnownForm(string dataFormatId, string dataFormatVersion)
        {
            string xsdSchema = "";

            var instances = GetForms();

            foreach (var item in instances)
            {
                if (item.GetDataFormatId() == dataFormatId && item.GetDataFormatVersion() == dataFormatVersion)
                {
                    xsdSchema = item.GetSchemaFile();
                }
            }
            if (string.IsNullOrEmpty(xsdSchema)) return false;
            else return true;
        }
    }
}