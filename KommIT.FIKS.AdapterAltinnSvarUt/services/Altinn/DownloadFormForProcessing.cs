﻿using System;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Altinn
{
    public class DownloadFormForProcessing
    {
        public bool Queued { get; set; }
        public FormProcessingType FormProcessingType { get; set; }
        public string ArchiveReference { get; set; }
        public DateTime ArchivedDate { get; set; }
        public string ServiceCode { get; set; }
        public int ServiceEditionCode { get; set; }
        public ReporteeType ReporteeType { get; set; }
        public bool DoProcess { get; set; }
        public DownloadFormForProcessing() { }
        public bool QueueFormToSubmittalQueue { get; set; }

        public override string ToString()
        {
            return $"ArchiveReference: {ArchiveReference}, ServiceCode: {ServiceCode}, ServiceEditionCode: {ServiceEditionCode}, ArchivedDate: {ArchivedDate}, FormProcessingType: {FormProcessingType}, ReporteeType: {ReporteeType}, Queued: {Queued}";
        }
    }
}