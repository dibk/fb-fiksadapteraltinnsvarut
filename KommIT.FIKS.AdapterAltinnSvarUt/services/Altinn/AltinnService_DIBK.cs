﻿using KommIT.FIKS.AdapterAltinnSvarUt.Services.WS;
using Serilog;
using System.Configuration;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Altinn
{
    public class AltinnService_DIBK : AltinnService
    {
        public AltinnService_DIBK(IDownloadStreamAttachment downloadAttachment, IApplicationLogService applicationLogService, ILogger logger) : base(downloadAttachment, applicationLogService, logger)
        { }

        protected override string Username { get { return ConfigurationManager.AppSettings["Altinn.DIBK.Username"]; } }

        protected override string Password { get { return ConfigurationManager.AppSettings["Altinn.DIBK.Password"]; } }
    }
}