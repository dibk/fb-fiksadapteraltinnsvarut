﻿using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.WS.Altinn;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Altinn
{
    public interface IAltinnFormDeserializer
    {
        FormData DeserializeForm(ArchivedFormTaskDQBE archivedFormTask);
    }
}
