﻿using Polly;
using Serilog;
using System;
using System.Collections.Generic;
using System.ServiceModel;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Altinn.Correspondence
{
    public class CorrespondencePolicyProvider : ICorrespondencePolicyProvider
    {
        private readonly ILogger _log = Log.ForContext<CorrespondencePolicyProvider>();
        private readonly ICorrespondencePolicyConfigurationProvider _correspondencePolicyConfigurationProvider;

        public CorrespondencePolicyProvider(ICorrespondencePolicyConfigurationProvider correspondencePolicyConfigurationProvider)
        {
            _correspondencePolicyConfigurationProvider = correspondencePolicyConfigurationProvider;
        }
        public Policy GetPolicy()
        {
            List<int> retryAttemptWaitStructure = _correspondencePolicyConfigurationProvider.RetryAttemptWaitSecondsStructure;
            var retryCount = 0;
            var policy = Policy.Handle<ServerTooBusyException>()
                .Or<TimeoutException>()
                .Or<EndpointNotFoundException>()
                .Or<CommunicationException>(c => c.Message.Contains("A connection that was expected to be kept alive was closed by the server"))                
                .WaitAndRetry(
                retryCount: retryAttemptWaitStructure.Count,
                sleepDurationProvider: attempt =>
                {
                    var jitterer = new Random();
                    var timeSpan = TimeSpan.FromSeconds(retryAttemptWaitStructure[retryCount])
                           + TimeSpan.FromMilliseconds(jitterer.Next(0, 1000));
                    retryCount++;

                    return timeSpan;
                },
                onRetry: (exception, calculatedWaitDuration) =>
                {
                    _log.Debug(exception, "Failed to contact {RetryApi} {RetryCount} time(s). Waits for {CalculatedWaitDuration} and retries", "Altinn-Correspondence", retryCount, calculatedWaitDuration);
                });

            return policy;
        }
    }
}