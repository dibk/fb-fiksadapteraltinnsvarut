﻿namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Altinn.Correspondence
{
    public interface ICorrespondencePolicyConfigurationProvider : IPolicyConfigurationProvider { }
    public class CorrespondencePolicyConfigurationProvider : PolicyConfigurationProvider, ICorrespondencePolicyConfigurationProvider
    {
        public override string RetryStructureSettingKey => "Polly:AltinnCorrespondenceClient:RetryStructure";
    }
}