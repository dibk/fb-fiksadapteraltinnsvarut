﻿using Polly;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Altinn.Correspondence
{
    public interface ICorrespondencePolicyProvider
    {
        Policy GetPolicy();
    }
}