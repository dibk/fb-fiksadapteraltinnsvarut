﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.WS.AltinnNotification;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Altinn
{
    public interface IAltinnNotificationService
    {
        SendNotificationResultList SendStandaloneNotification(string reportee, string emailAdress, List<string> textTokensList);
        string ReadMessageStatusForSingleNotification(SendNotificationResultList results);
    }
}