﻿using KommIT.FIKS.AdapterAltinnSvarUt.Services.WS;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.WS.DownloadStreamAttachment;
using Serilog;
using System;
using System.ServiceModel;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Altinn
{
    public class AltinnDownloadStreamAttachment : IDownloadStreamAttachment
    {
        private readonly ILogger _logger;

        public AltinnDownloadStreamAttachment(ILogger logger)
        {
            _logger = logger;
        }

        public byte[] GetAttachmentDataStreamed(int attachmentId, string username, string password)
        {
            byte[] attachmentDataStream = null;
            try
            {
                var client = new ServiceOwnerArchiveExternalStreamedBasicClient();

                try
                {
                    attachmentDataStream = client.GetAttachmentDataStreamedBasic(username, password, attachmentId);
                    client.Close();
                }
                catch (CommunicationException)
                {
                    client.Abort();
                    throw;
                }
                catch (TimeoutException)
                {
                    client.Abort();
                    throw;
                }
                catch (Exception)
                {
                    client.Abort();
                    throw;
                }

            }
            catch (Exception e)
            {
                _logger.Error(e, "Error ({ExceptionType}) occurred when communicating with Altinn - {ExternalResource} - ExceptionMessage: {ExceptionMessage}", e.GetType().ToString(), "ServiceOwnerArchiveExternalStreamedBasicClient.GetAttachmentDataStreamedBasic", e.Message);
                throw;
            }

            return attachmentDataStream;
        }
    }
}