﻿using KommIT.FIKS.AdapterAltinnSvarUt.Services.WS.Altinn;
using System.Collections.Generic;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Altinn
{
    public interface IAltinnService
    {
        /// <summary>
        /// Return details about archive reference.
        /// </summary>
        /// <param name="archiveReference"></param>
        /// <returns></returns>
        ArchivedFormTaskDQBE GetItemByArchiveReference(string archiveReference);

        /// <summary>
        /// Downloads list of all the pensding items in the Altinn DownloadQueue
        /// </summary>
        /// <returns>List of type DownloadQueueItemBE</returns>
        List<DownloadQueueItemBE> GetAllAltinnDlqPendingForms();

        /// <summary>
        /// Purge all items from Altinn Download Queue
        /// </summary>
        bool PurgeItem(string archiveReference);

        byte[] GetPdfForm(string ArchiveReference, string DataFormatId, string DataFormatVersion);
    }
}
