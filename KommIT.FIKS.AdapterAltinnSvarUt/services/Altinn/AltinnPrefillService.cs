﻿using KommIT.FIKS.AdapterAltinnSvarUt.Services.Altinn.Prefill;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.WS.AltinnPreFill;
using Serilog;
using System;
using System.Configuration;
using System.ServiceModel;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Altinn
{
    public class AltinnPrefillService : IAltinnPrefillService
    {
        private readonly string _username;
        private readonly string _password;
        private readonly string _externalBatchId = Guid.NewGuid().ToString();
        private readonly bool _doSaveFormTask = true;
        private readonly bool _doinstantiateFormTask = true;
        private readonly int? _caseId = null;
        private readonly string _instantitedOnBehalfOf = null;
        private readonly ILogger _logger;
        private PreFillExternalBasicClient _prefillClient;
        private readonly IPrefillPolicyProvider _prefillPolicyProvider;

        public AltinnPrefillService(ILogger logger, IPrefillPolicyProvider prefillPolicyProvider)
        {
            _username = ConfigurationManager.AppSettings["Altinn.DIBK.Username"];
            _password = ConfigurationManager.AppSettings["Altinn.DIBK.Password"];
            _logger = logger;
            _prefillPolicyProvider = prefillPolicyProvider;
        }

        public void CloseClient()
        {
            if (_prefillClient != null)
                try
                {
                    _prefillClient.Close();
                }
                catch (Exception ex)
                {
                    _logger.Error(ex, "Error occured when closing prefillClient, state: {ClientState}",_prefillClient?.State);
                }
        }

        private PreFillExternalBasicClient GetClient()
        {
            if (_prefillClient == null || _prefillClient.State != CommunicationState.Opened)
                _prefillClient = new PreFillExternalBasicClient();

            return _prefillClient;
        }

        public ReceiptExternal SubmitAndInstantiatePrefilledForm(PrefillFormTask prefillFormTask, DateTime? dueDate)
        {
            try
            {
                ReceiptExternal receiptExternal = null;

                var policy = _prefillPolicyProvider.GetPolicy();

                policy.Execute(() =>
                {
                    try
                    {
                        receiptExternal = GetClient().SubmitAndInstantiatePrefilledFormTaskBasic(
                                _username,
                                _password,
                                _externalBatchId,
                                prefillFormTask,
                                _doSaveFormTask,
                                _doinstantiateFormTask,
                                _caseId,
                                dueDate,
                                _instantitedOnBehalfOf);
                    }
                    catch (CommunicationException)
                    {
                        _prefillClient.Abort();
                        throw;
                    }
                    catch (TimeoutException)
                    {
                        _prefillClient.Abort();
                        throw;
                    }
                    catch (Exception)
                    {
                        _prefillClient.Abort();
                        throw;
                    }

                    Logger.TelemetryLogger.LogAltinnTelemetry<AltinnPrefillService>("Prefill", _prefillClient.Endpoint.Address.Uri, prefillFormTask.ExternalServiceCode, prefillFormTask.ExternalServiceEditionCode.ToString());
                });

                return receiptExternal;
            }
            catch (FaultException<AltinnFault> e)
            {
                _logger.Error(e, "Error ({ExceptionType}) occurred when communicating with Altinn - {ExternalResource}. AltinnErrorMessage: {AltinnFaultErrorMessage} - ErrorId: {AltinnErrorCode} - ErrorGuid: {AltinnFaultErrorGuid} - AltinnExtendedErrorMessage: {AltinnFaultExtendedErrorMessage} - AltinnLocalizedErrorMessage: {AltinnFaultLocalizedErrorMessage}",
                    e.GetType().ToString(),
                    "PreFillExternalBasicClient.SubmitAndInstantiatePrefilledFormTaskBasic",
                    e.Detail.AltinnErrorMessage,
                    e.Detail.ErrorID,
                    e.Detail.ErrorGuid,
                    e.Detail.AltinnLocalizedErrorMessage,
                    e.Detail.AltinnExtendedErrorMessage);
                throw;
            }
            catch (FaultException e)
            {
                _logger.Error(e, "Error ({ExceptionType}) occurred when communicating with Altinn - {ExternalResource}. FaultMessage: {FaultMessage} - FaultAction: {FaultAction} - FaultCode: {FaultCode} -  FaultReason: {FaultReason}",
                    e.GetType().ToString(),
                    "PreFillExternalBasicClient.SubmitAndInstantiatePrefilledFormTaskBasic",
                    e.Message, e.Action, e.Code, e.Reason);
                throw;
            }
            catch (Exception e)
            {
                _logger.Error(e, "Error ({ExceptionType}) occurred when communicating with Altinn - {ExternalResource} - ExceptionMessage: {ExceptionMessage}", e.GetType().ToString(), "PreFillExternalBasicClient.SubmitAndInstantiatePrefilledFormTaskBasic", e.Message);
                throw;
            }
        }
    }
}