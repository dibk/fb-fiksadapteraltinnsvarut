﻿using Serilog;
using System.Collections.Generic;
using System.Linq;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Checklist
{
    public class ChecklistUtilities
    {
        internal bool? IsRuleRelevantForApplication(string rulechecklistReferenceNumber, string soknadtype, List<string> tiltakstyperISoeknad)
        {
            if (string.IsNullOrEmpty(soknadtype) || string.IsNullOrEmpty(rulechecklistReferenceNumber) || tiltakstyperISoeknad == null || !tiltakstyperISoeknad.Any())
                return null;

            string[] inforequiredForTiltakstyper = ExternalChecklistService.GetActivityEnterpriseTerms(rulechecklistReferenceNumber, soknadtype);
            if (inforequiredForTiltakstyper == null || !inforequiredForTiltakstyper.Any())
            {
                Log.Error("Finnes ikke noen tiltakstypen for '{soknadtype}' søknad - sjekkliste punkt: '{checklistReference}'", soknadtype, rulechecklistReferenceNumber);
                return null;
            }

            var ruleAplaysToTiltakstype = tiltakstyperISoeknad.Any(tiltakstyper => inforequiredForTiltakstyper.Any(tiltakstyper.Equals));
            return ruleAplaysToTiltakstype;
        }
    }
}