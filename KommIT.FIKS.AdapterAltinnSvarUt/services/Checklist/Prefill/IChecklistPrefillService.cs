﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Checklist.Prefill
{
    public interface IChecklistPrefillService
    {
        Task<IEnumerable<ChecklistAnswer>> GetPrefillChecklist(ValidationResult validationResult, string processCategory);
        Task<IEnumerable<ChecklistAnswer>> GetPrefillChecklist(string form, string soknadstype, List<string> tiltaksyper);
    }
}