﻿using KommIT.FIKS.AdapterAltinnSvarUt.Models.PrefillChecklist;
using Newtonsoft.Json;
using Serilog;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Xml.Linq;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Checklist.Prefill
{
    public class ChecklistPrefillService : IChecklistPrefillService
    {
        private readonly ILogger _logger = Log.ForContext<ChecklistPrefillService>();

        public async Task<IEnumerable<ChecklistAnswer>> GetPrefillChecklist(ValidationResult validationResult, string processCategory)
        {
            _logger.Debug("Maps validation results to prefill checklist request payload for {ProcessCategory}", processCategory);
            var prefillRequestBody = PrefillChecklistInput.Map(validationResult);

            var checklistHost = ConfigurationManager.AppSettings.Get("FTBSjekklisteServer");
            var requestUri = "/api/sjekkliste/prefill";

            try
            {
                var httpclient = new HttpClient()
                {
                    BaseAddress = new Uri(checklistHost),
                };

                var jsonPostRequest = JsonConvert.SerializeObject(prefillRequestBody);

                var stringContent = new StringContent(jsonPostRequest, Encoding.UTF8, "application/json");
                var response = await httpclient.PostAsync(requestUri, stringContent);

                response.EnsureSuccessStatusCode();

                var jsonResponse = await response.Content.ReadAsStringAsync();
                var checklistAnswers = JsonConvert.DeserializeObject<List<ChecklistAnswer>>(jsonResponse);

                _logger.Debug("Prefilled checklist answers {ChecklistAnswersCount} retreived from {ChecklistHost}/{RequestUri}", checklistAnswers?.Count, checklistHost, requestUri);

                return checklistAnswers;
            }
            catch (Exception e)
            {
                _logger.Error(e, "An error occurred when requesting prefilled checklist from {ChecklistHost} on {RequestUri}", checklistHost, requestUri);
                throw;
            }
        }

        public async Task<IEnumerable<ChecklistAnswer>> GetPrefillChecklist(string form, string soknadstype, List<string> tiltaksyper)
        {
            _logger.Debug("Maps form data to prefill checklist request payload for {ProcessCategory}", soknadstype);
            var prefillRequestBody = PrefillChecklistByXpathInput.Map(form, soknadstype, tiltaksyper);

            var checklistHost = ConfigurationManager.AppSettings.Get("FTBSjekklisteServer");

            var requestUri = $"api/sjekkliste/prefill/xpath/{soknadstype}";

            try
            {
                var httpclient = new HttpClient()
                {
                    BaseAddress = new Uri(checklistHost),
                };

                var jsonPostRequest = JsonConvert.SerializeObject(prefillRequestBody);

                var stringContent = new StringContent(jsonPostRequest, Encoding.UTF8, "application/json");

                // Hent sjekkpunkter som er relevante å besvare for søknaden (ut fra søknadstype og tiltakstype)
                var response = await httpclient.PostAsync(requestUri, stringContent);

                response.EnsureSuccessStatusCode();

                var jsonResponse = await response.Content.ReadAsStringAsync();
                var checklistAnswers = JsonConvert.DeserializeObject<List<ChecklistAnswer>>(jsonResponse);

                _logger.Debug("Prefilled checklist answers {ChecklistAnswersCount} retreived from {ChecklistHost}/{RequestUri}", checklistAnswers?.Count, checklistHost, requestUri);

                return checklistAnswers;
            }
            catch (Exception e)
            {
                _logger.Error(e, "An error occurred when requesting prefilled checklist from {ChecklistHost} on {RequestUri}", checklistHost, requestUri);
                throw;
            }
        }
    }
}