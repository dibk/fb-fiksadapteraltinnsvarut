﻿using KommIT.FIKS.AdapterAltinnSvarUt.Services.Caching;
using Newtonsoft.Json;
using Serilog;
using System;
using System.Configuration;
using System.Net;
using System.Text;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Checklist
{
    public static class ExternalChecklistService
    {
        public static string[] GetActivityEnterpriseTerms(string referenceId, string processcategory)
        {
            //Get from cache
            var cachedItem = MemoryCacheService.Instance.GetObject($"Checklist-{referenceId}-{processcategory}") as string[];
            if (cachedItem != null)
            {
                Log.Verbose("Checklist item found in cache {referenceId} - {processCategory}", referenceId, processcategory);
                return cachedItem;
            }

            var result = string.Empty;

            // api/tiltakstyper/{prosesskategori}/sjekkpunkt/{pkt}")]
            string url = $"{ConfigurationManager.AppSettings["FTBSjekklisteServer"]}/api/tiltakstyper/{processcategory}/sjekkpunkt/{referenceId}";
            try
            {
                using (WebClient client = new WebClient())
                {
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                    client.Encoding = Encoding.UTF8;
                    client.Headers.Add(HttpRequestHeader.Accept, "application/json");
                    client.Headers.Add("foretaknavn", "dibk");
                    client.Headers.Add("system", "fellestjenester-plan-og-bygg");
                    result = client.DownloadString(url);
                }

                Log.Verbose("Checklist call {referenceId} - {processCategory} {url} {response}", referenceId, processcategory, url, result);
            }
            catch (System.Exception ex)
            {
                Log.Error(ex, "Feil ved tilgangen til sjekklister, {referenceId} - {processCategory} url: {url} ", referenceId, processcategory, url);
            }

            string[] response = null;

            try
            {
                response = JsonConvert.DeserializeObject<string[]>(result);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Feil ved deserialisering av sjekkliste svar, for '{soknadtype}' søknad - sjekkliste punkt: '{checklistReference}', result: {response}", processcategory, referenceId, result);
            }

            //Add to cache
            if (response != null)
            {
                MemoryCacheService.Instance.AddOrReplaceCachedItem($"Checklist-{referenceId}-{processcategory}", response, DateTime.Now.AddHours(6));
                Log.Verbose("Checklist item added to cache {referenceId} - {processCategory}", referenceId, processcategory);
            }

            return response;
        }
    }
}