﻿using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.DecryptFnrForTh;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Prefill;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Svarut.REST;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services
{
    public class FormShippingService : IFormShippingService
    {
        private readonly SvarUtRestService _svarUtService;
        private readonly SvarUtRestBuilder _svarUtRestBuilder;

        private readonly IFormMetadataService _formMetadataService;

        private readonly IMunicipalityService _municipalityService;
        private readonly IDecryptFnr _decryptFnr;
        private readonly ITiltakshaversSignedApprovalDocument _addSignatureDocument;
        private readonly ILogEntryService _logEntryService;
        private readonly SvarUtRestRetryPolicyProvider _svarUtPolicyProvider;

        public FormShippingService(SvarUtRestService svarUtService,
                                   SvarUtRestBuilder svarUtRestBuilder,
                                   IFormMetadataService formMetadataService,
                                   IMunicipalityService municipalityService,
                                   IDecryptFnr decryptFnr,
                                   ILogEntryService logEntryService,
                                   ITiltakshaversSignedApprovalDocument addSignatureDocument,
                                   SvarUtRestRetryPolicyProvider svarUtPolicyProvider)
        {
            _svarUtService = svarUtService;
            _svarUtRestBuilder = svarUtRestBuilder;
            _formMetadataService = formMetadataService;
            _municipalityService = municipalityService;
            _decryptFnr = decryptFnr;
            _addSignatureDocument = addSignatureDocument;
            _logEntryService = logEntryService;
            _svarUtPolicyProvider = svarUtPolicyProvider;
        }

        public void Ship(FormData formData)
        {
            Municipality municipality = _municipalityService.GetMunicipality(formData.Mainform.GetMunicipalityCode());

            var receivingAuthority = new ReceiverProvider().GetReceivingAuthority(formData.MainformDataFormatID, municipality);

            // Add decrypted signature file for tiltakshaver
            ProcessTiltakshaversSignature(formData);

            // Check for subform GjenpartNabovarsel and decrypt neighbors fnrs
            AddAttachmentWithDecryptedPersonalNumbersForGjennpartNabovarsel(formData);

            _formMetadataService.SaveReceiver(receivingAuthority, formData.ArchiveReference);

            _formMetadataService.SaveFormDataToFormMetadataLog(formData);

            var retryPolicy = _svarUtPolicyProvider.GetRetryPolicy("SvarUt-Forsendelse");

            retryPolicy.Execute(() =>
            {
                _svarUtService.SendShipment(_svarUtRestBuilder.BuildForsendelse(formData, receivingAuthority.OrganizationNumber, receivingAuthority.Name));
            });

            
        }

        private bool AddAttachment(FormData formData, Attachment attachment)
        {
            if (attachment != null)
            {
                formData.Attachments = formData.Attachments ?? new List<Attachment>();
                formData.Attachments.Add(attachment);
                return true;
            }
            return false;
        }

        private Guid CheckForSingatureFromTiltakshaver(string formXml)
        {
            Guid signatureId = Guid.Empty;
            var samtykkeTilByggeplaner = XmlUtil.GetElementValue(formXml, "samtykkeTilByggeplaner");

            if (!string.IsNullOrEmpty(samtykkeTilByggeplaner))
            {
                if (!Guid.TryParse(samtykkeTilByggeplaner, out signatureId))
                {
                    // No valid GUID found
                    signatureId = Guid.Empty;
                }
            }
            return signatureId;
        }

        private void ProcessTiltakshaversSignature(FormData formData)
        {
            Guid thSamtykke = CheckForSingatureFromTiltakshaver(formData.FormDataAsXml);
            bool isAttacmentForSignaturePresent = formData.Attachments.Exists(a => a.AttachmentTypeName.Equals(ConfigurationManager.AppSettings["AttachmentName.TiltakshaverSignatur"]));

            if (thSamtykke != Guid.Empty)
            {
                _logEntryService.Save(LogEntry.NewInfo(formData.ArchiveReference, $"Søknadens metadata inneholder ID for Tiltakshavers samtykke til byggeplaner {thSamtykke.ToString()}"));

                // Check that data is available
                if (_decryptFnr.SignatureDataExists(thSamtykke) == false)
                {
                    _logEntryService.Save(LogEntry.NewError(formData.ArchiveReference, $"Finner ikke tiltakshavers signatur for den gitte IDen. Innsending skjer uten samtykke data"));
                    return;
                }

                if (_decryptFnr.HasPersonalNumberInformation(thSamtykke))
                {
                    var thSigAttachment = _decryptFnr.DecryptFnrForTh(thSamtykke, formData.ArchiveReference);
                    // Add attachment with personal number XML
                    if (!AddAttachment(formData, thSigAttachment))
                        _logEntryService.Save(LogEntry.NewError(formData.ArchiveReference,
                            "En feil oppstod ved behandling av fnr XML vedlegg til Tiltakshavers samtykke til byggeplaner"));
                    else
                        _logEntryService.Save(LogEntry.NewInfo(formData.ArchiveReference,
                            "Tiltakshaverssamtykke til byggeplaner -- fnr XML vedlegg lagt til"));
                }
                else
                    _logEntryService.Save(LogEntry.NewInfo(formData.ArchiveReference,
                        $"Tiltakshavers samtykke data inneholder ikke et fnr"));

                // Add attachment with signed PDF
                if (isAttacmentForSignaturePresent == false)
                {
                    var samtykkePdf = _addSignatureDocument.GetSignedAttachment(thSamtykke.ToString(), formData.ArchiveReference);
                    if (!AddAttachment(formData, samtykkePdf))
                        _logEntryService.Save(LogEntry.NewError(formData.ArchiveReference, "En feil oppstod ved behandling av signatur vedlegg fra tiltakshaver"));
                    else
                        _logEntryService.Save(LogEntry.NewInfo(formData.ArchiveReference, "Tiltakshaverssamtykke til byggeplaner -- signature PDF vedlegg lagt til"));
                }
                else
                    _logEntryService.Save(LogEntry.NewInfo(formData.ArchiveReference, "Tiltakshaverssamtykke til byggeplaner -- signature PDF er lagt ved fra søknadssystem"));
            }
        }

        private void AddAttachmentWithDecryptedPersonalNumbersForGjennpartNabovarsel(FormData formData)
        {
            if (formData?.Subforms != null && formData.Subforms.Any())
            {
                //If is any "GjenpartNabovarsel" - Gjenpart nabovarsel (DataFormatId: 5826, DataFormatVersion: 42895) /
                var gjenpartNaboEier = formData.Subforms.FirstOrDefault(atc => atc.DataFormatId == ConfigurationManager.AppSettings["Subform:GjenpartNabovarsel:DataFormatIds"]);
                if (gjenpartNaboEier != null)
                {
                    var listOfNaboeieriSubform = GjenpartNabovarselDecoder.GetListOfNeighborSsn(gjenpartNaboEier);
                    // get modell to the municipality
                    var naboeierModelTilKommune = GjenpartNabovarselDecoder.GetNaboeier(listOfNaboeieriSubform);
                    // create new attachement
                    var naboeierAttachement = GjenpartNabovarselDecoder.GetGjenpartNabovarselAttachement(formData.ArchiveReference, naboeierModelTilKommune);

                    //add Attach to the municipality
                    if (!AddAttachment(formData, naboeierAttachement))
                    {
                        // something failed
                    }
                }
            }
        }
    }

    public interface IReceivingAuthority
    {
        Authority AuthorityType { get; }
        string Name { get; }
        string OrganizationNumber { get; }
        Receiver Municipality { get; set; }
    }

    public class SektormyndighetReceivingAuthority : IReceivingAuthority
    {
        public Authority AuthorityType
        { get { return Authority.Sektormyndighet; } }

        public string Name { get; private set; }
        public string OrganizationNumber { get; private set; }
        public Receiver Municipality { get; set; }

        public SektormyndighetReceivingAuthority(string name, string organizationNumber)
        {
            Name = name;
            OrganizationNumber = organizationNumber;
        }

        public override string ToString()
        {
            return $"{Name} ({OrganizationNumber})";
        }
    }

    public class MunicipalityReceivingAuthority : IReceivingAuthority
    {
        public Authority AuthorityType
        { get { return Authority.Municipality; } }

        public string Name
        { get { return this.Municipality?.GetName(); } }

        public string OrganizationNumber
        { get { return this.Municipality?.GetOrganizationNumber(); } }

        public Receiver Municipality { get; set; }

        public override string ToString()
        {
            return $"{Name} kommune ({OrganizationNumber})";
        }
    }

    public enum Authority
    {
        Municipality,
        Sektormyndighet
    }

    public class ReceiverProvider
    {
        //TODO: Maybe sektormyndighet settings could have been exposed using a ISektormyndighetForm interface on the form implementaion? :thinking:
        public IReceivingAuthority GetReceivingAuthority(string dataFormatId, Municipality municipality)
        {
            IReceivingAuthority authority = null;

            var configIdsValue = ConfigurationManager.AppSettings["Sektormyndighet:ConfigIdentifiers"];

            string sektormyndighetConfigId = string.Empty;

            if (!string.IsNullOrEmpty(configIdsValue))
            {
                //Find sektormyndighet responsible for a given dataformatid
                foreach (var sektormyndighetId in configIdsValue.Split(';').ToList())
                {
                    var dataFormatIds = ConfigurationManager.AppSettings[$"Sektormyndighet:{sektormyndighetId}:DataFormatIds"];
                    if (dataFormatIds.Split(';').Contains(dataFormatId))
                    {
                        sektormyndighetConfigId = sektormyndighetId;
                        break;
                    }
                }
            }
            if (!string.IsNullOrEmpty(sektormyndighetConfigId))
            {
                //Get sektormyndighet from configuration, uses hardcoded until config is implemented
                var organizationNumber = ConfigurationManager.AppSettings[$"Sektormyndighet:{sektormyndighetConfigId}:Orgnr"];
                var receivingAuthorityName = ConfigurationManager.AppSettings[$"Sektormyndighet:{sektormyndighetConfigId}:Navn"];

                authority = new SektormyndighetReceivingAuthority(receivingAuthorityName, organizationNumber);

                //creates dummy municipality to be used in receiver
                var dummyMunicipality = new Municipality(municipality.Code, municipality.OrganizationNumber, municipality.Name);
                authority.Municipality = new Receiver(dummyMunicipality);
            }
            else
            {
                authority = new MunicipalityReceivingAuthority();
                authority.Municipality = new Receiver(municipality);
            }

            return authority;
        }
    }
}