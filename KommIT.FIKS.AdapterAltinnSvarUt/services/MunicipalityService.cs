﻿using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using System.Linq;
using System.Collections.Generic;
using System.Data.Entity;
using System;
using Serilog;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Authentication;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services
{
    public class MunicipalityService : IMunicipalityService
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly IAuthorizationService _authorizationService;
        private readonly IFormService _formService;

        public MunicipalityService(ApplicationDbContext dbContext, IAuthorizationService authorizationService, IFormService formService)
        {
            _dbContext = dbContext;
            _authorizationService = authorizationService;
            _formService = formService;
        }

        public Municipality GetMunicipality(string code)
        {
            Log.ForContext<MunicipalityService>().Debug("Fetches municipality data for code {municipalityCode}", code);
            Municipality municipality = _dbContext.Municipalities.Where(m => m.Code == code).FirstOrDefault();

            if (municipality == null)
            {
                Log.ForContext<MunicipalityService>().Debug("Did not find municipality data for code {municipalityCode}", code);
                throw new ArgumentException("Invalid municipality code [" + code + "]. Municipality was not found in database.");
            }
            Log.ForContext<MunicipalityService>().Debug("Returns municipality data for code {municipalityCode}", code);

            return municipality;
        }

        public void UpdateMunicipalityApplicationUser(string[] municipalities, string userId)
        {
            var user = _dbContext.Users.Include("MunicipalitiesToAdministrate")
            .Single(u => u.Id == userId);

            if (municipalities == null)
            {
                user.MunicipalitiesToAdministrate.Clear();
            }
            else
            {
                foreach (var municipality in user.MunicipalitiesToAdministrate.ToList())
                {
                    if (!municipalities.Contains(municipality.Code))
                        user.MunicipalitiesToAdministrate.Remove(municipality);
                }
                if (municipalities != null)
                {
                    foreach (var newMunicipality in municipalities)
                    {
                        if (!user.MunicipalitiesToAdministrate.Any(m => m.Code == newMunicipality))
                        {
                            var municipality = _dbContext.Municipalities.FirstOrDefault(m => m.Code.Trim().Equals(newMunicipality.Trim()));

                            //var newMun = new Municipality { Code = newMunicipality };
                            //_dbContext.Municipalities.Attach(newMun);
                            user.MunicipalitiesToAdministrate.Add(municipality);
                        }
                    }
                }
            }

            _dbContext.SaveChanges();
        }

        public bool UserHasAccessToMunicipality(string municipalityCode, string userId)
        {
            Municipality municipality = GetMunicipalityWithAccessControl(municipalityCode, userId);
            return municipality != null;
        }        

        public Municipality GetMunicipalityWithAccessControl(string municipalityCode, string userId)
        {
            Municipality municipality = _dbContext.Set<Municipality>().FirstOrDefault(m => m.Code == municipalityCode);

            if (municipality == null)
                throw new ArgumentException("Invalid municipality code.");

            bool isAdmin = _authorizationService.UserHasAdministratorRole(userId);

            if (isAdmin || municipality.AdministratedByUsers.Any(admUser => admUser.Id == userId))
                return municipality;

            return null;
        }

        public List<Municipality> GetMunicipalitiesForUser(string userId)
        {
            List<Municipality> municipalities;

            bool isAdmin = _authorizationService.UserHasAdministratorRole(userId);

            if (isAdmin)
            {
                municipalities = _dbContext.Set<Municipality>().OrderBy(m => m.Name).ToList();
            }
            else
            {
                municipalities = _dbContext.Set<Municipality>()
                    .Where(m => m.AdministratedByUsers.Any(u => u.Id == userId))
                    .OrderBy(m => m.Name)
                    .ToList();
            }

            return municipalities;
        }

        public void UpdateMunicipality(Municipality municipality)
        {
            _dbContext.Entry(municipality).State = EntityState.Modified;
            _dbContext.SaveChanges();
        }
    }
}