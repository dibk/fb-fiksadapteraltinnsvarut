﻿using KommIT.FIKS.AdapterAltinnSvarUt.Services.WS.AltinnCorrespondenceAgency;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.WS.AltinnNotification;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.UserNotification
{
    public interface ISendSecondNotifications
    {
        void IssueAndUpdateSecondNotifictions();
        string SendNotification(Models.UserNotification userNotification, StatusV2 statusMessage);
        string CheckAltinnNotificationStatus(SendNotificationResultList status);
    }
}