﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Altinn;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.WS.AltinnCorrespondenceAgency;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.WS.AltinnNotification;
using Serilog;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.UserNotification
{
    public class SendSecondNotifications : ISendSecondNotifications
    {
        private static IAltinnCorrespondenceService _corresponcanceService;
        private readonly UserNotificationHandler _userNotificationHandler;
        private readonly ILogEntryService _logEntryService;
        private readonly string _messageServiceCode;
        private readonly int _messageServiceEditionCode;
        private readonly IAltinnNotificationService _altinnNotificationService;
        private readonly ILogger _logger;


        public SendSecondNotifications(IAltinnCorrespondenceService corresponcanceService, UserNotificationHandler userNotificationHandler, ILogEntryService logEntryService, IAltinnNotificationService altinnNotificationService, ILogger logger)
        {
            _corresponcanceService = corresponcanceService;
            _userNotificationHandler = userNotificationHandler;
            _logEntryService = logEntryService;
            _altinnNotificationService = altinnNotificationService;
            _logger = logger;

            _messageServiceCode = ConfigurationManager.AppSettings["MessageServiceCode"]; ;
            _messageServiceEditionCode = Convert.ToInt32(ConfigurationManager.AppSettings["MessageServiceCodeEdition"]);
        
        }


        public void IssueAndUpdateSecondNotifictions()
        {

            try
            {
                // Read database for pending notificaitons
                List<Models.UserNotification> listUserNotifications = _userNotificationHandler.GetRecordsOfMessagesDueForNotification();
                _logger.Debug($"Notifications to be sent: {listUserNotifications.Count}");

                // Loop through the notifications
                foreach (var notification in listUserNotifications)
                {
                    // Read status from Altinn on the message
                    CorrespondenceStatusResultV3 result = _corresponcanceService.GetCorrespondenceStatus(notification.MessageReference, _messageServiceCode, _messageServiceEditionCode, notification.FirstNotification);
                    StatusV2 statusMessage = result.CorrespondenceStatusInformation.CorrespondenceStatusDetailsList.FirstOrDefault();

                    if (statusMessage != null && statusMessage.StatusChanges.Any(f => f.StatusType == CorrespondenceStatusTypeV2.Read))
                    {
                        //  - Update status in db if message is read
                        notification.Status = UserNotificationStatus.SecondNotificaitonNa;
                        _userNotificationHandler.UpdateRecord(notification);
                        _logger.Debug($"Message {statusMessage.SendersReference} is already being read, db was updated");
                    }
                    else if (statusMessage != null)
                    {
                        //  - Send out second notificaitons if not read
                        string status = SendNotification(notification, statusMessage);
                        //  - Update status in db 
                        notification.Status = UserNotificationStatus.Sent;
                        notification.NotificaitonReceipt = status;
                        notification.SMSText = "***";
                        notification.EmailBody = "***";
                        _userNotificationHandler.UpdateRecord(notification);
                        _logger.Debug($"Message {statusMessage.SendersReference} was notified a second time: {status}");
                    }
                    else
                    {
                        _logEntryService.Save(new LogEntry(notification.ArchiveReference, $"Feil ved innhenting av Altinn status på meldingen {notification.MessageReference}", "Error", true));
                        notification.Status = UserNotificationStatus.ErrorMessageStatus;
                        _userNotificationHandler.UpdateRecord(notification);
                        _logger.Error($"Message {notification.ArchiveReference}/{notification.MessageReference} failed to get Altinn correspondence status");
                    }
                }
            }
            catch (Exception e)
            {
                _logger.Fatal($"IssueAndUpdateSecondNotifictions exception: {e.Message}");
            }
        }


        public string SendNotification(Models.UserNotification userNotification, StatusV2 statusMessage)
        {
            string returnCode;

            List<string> tokenList = new List<string>();
            tokenList.Add("Påminnelse: " + userNotification.SMSText);
            tokenList.Add("Påminnelse: " + userNotification.EmailSubject);
            tokenList.Add(userNotification.EmailBody);

            SendNotificationResultList status = _altinnNotificationService.SendStandaloneNotification(statusMessage.Reportee, userNotification.EmailAdress, tokenList);

            string statusDecoded = CheckAltinnNotificationStatus(status);

            if (statusDecoded.Contains("Error"))
            {
                string message = $"Feil ved utsending av andregangsvarsel for distribusjon {userNotification.MessageReference}";
                _logEntryService.Save(new LogEntry(userNotification.ArchiveReference, message, "Error", false));

                message = $"Andregangsvarsel for distribusjon {userNotification.MessageReference} error: {statusDecoded.ToString()}";
                _logEntryService.Save(new LogEntry(userNotification.ArchiveReference, message, "Error", true));
                returnCode = "error";
            }
            else
            {
                string message = $"Andregangsvarsel sendt for distribusjon {userNotification.MessageReference}";
                _logEntryService.Save(new LogEntry(userNotification.ArchiveReference, message, "Info", true));
                returnCode = statusDecoded;
            }
            return returnCode;
        }


        public string CheckAltinnNotificationStatus(SendNotificationResultList status)
        {
            return _altinnNotificationService.ReadMessageStatusForSingleNotification(status);
        }
    }
}