﻿using System;
using System.Collections.Generic;
using System.Configuration;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.UserNotification
{
    public class UserNotificationHandler
    {
        private readonly IUserNotificationService _notificationService;



        public UserNotificationHandler(IUserNotificationService notificationService)
        {
            _notificationService = notificationService;
        }



        public void AddUserNotification(string archiveReference, string messageReference, DateTime firstNotification, string emailAdress, string sMSText, string emailSubject, string emailBody)
        {
            int numHours = Convert.ToInt32(Resources.TextStrings.ReNotificationPeriodInHours);
            DateTime secondNotification = DateTime.Now.AddHours(numHours);

            // for test environment
            if (ConfigurationManager.AppSettings["RunInTestMode"] == "true") secondNotification = DateTime.Now.AddMinutes(8);

            var userNotification = new Models.UserNotification(archiveReference, messageReference, firstNotification, secondNotification, emailAdress, sMSText, emailSubject, emailBody);

            _notificationService.AddUserNotification(userNotification);
        }



        public List<Models.UserNotification> GetRecordsOfMessagesDueForNotification()
        {
            return _notificationService.GetActiveNotifications();
        }


        public void UpdateRecord(Models.UserNotification userNotification)
        {
            _notificationService.UpdateRecord(userNotification);
        }

    }
}
