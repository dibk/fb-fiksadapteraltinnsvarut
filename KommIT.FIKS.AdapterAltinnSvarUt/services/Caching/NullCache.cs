﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Caching
{
    public class NullCache : ICacheService
    {
        public void AddOrReplaceCachedItem(string key, object item, DateTimeOffset expirationDateTime)
        {
            //Do nothing
        }

        public object GetObject(string key)
        {
            return null;
        }
    }
}