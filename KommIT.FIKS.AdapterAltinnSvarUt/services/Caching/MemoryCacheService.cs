﻿using Serilog;
using System;
using System.Runtime.Caching;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Caching
{
    public class MemoryCacheService : ICacheService
    {
        private static readonly object instancelock = new object();
        private static MemoryCacheService instance = null;
        private readonly ILogger _logger = Log.ForContext<MemoryCacheService>();
        private MemoryCacheService()
        {
        }

        public static ICacheService Instance
        {
            get
            {
                //Uses "Double-checked locking" mechanism to make sure that multiple threads only create ONE instance
                if (instance == null)
                {
                    lock (instancelock)
                    {
                        if (instance == null)
                        {
                            instance = new MemoryCacheService();                            
                        }
                    }
                }
                return instance;
            }
        }

        public object GetObject(string key)
        {
            _logger.Verbose("Get from cache; key: {key}", key);
            object retVal = MemoryCache.Default.Get(key.ToLower());
            return retVal;
        }

        public void AddOrReplaceCachedItem(string key, object item, DateTimeOffset expirationDateTime)
        {
            lock (instancelock)
            {
                _logger.Verbose("Add to cache; key: {key}", key);
                MemoryCache.Default.Set(key.ToLower(), item, expirationDateTime);
            }
        }
    }
}