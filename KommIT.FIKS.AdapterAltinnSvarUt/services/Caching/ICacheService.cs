﻿using System;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Caching
{
    public interface ICacheService
    {        
        /// <summary>
        /// Get cached value
        /// </summary>
        /// <param name="key"></param>
        /// <returns>The cached object</returns>
        object GetObject(string key);

        /// <summary>
        /// Add item to cache.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="item"></param>
        /// <param name="expirationDateTime"></param>
        void AddOrReplaceCachedItem(string key, object item, DateTimeOffset expirationDateTime);
    }
}
