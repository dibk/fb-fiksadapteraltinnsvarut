﻿using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Models.ApiModels;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Altinn;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Azure;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Prefill;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.WS.AltinnCorrespondenceAgency;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;
using Microsoft.Extensions.Logging;
using System;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel;
using System.Text;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Notification
{
    public class NotificationServiceV2 : INotificationServiceV2
    {

        private static IAltinnCorrespondenceService _correspondenceService;
        private readonly ILogEntryService _logEntryService;
        protected readonly IFormMetadataService _formMetadataService;
        protected readonly IFileStorage _fileStorage;
        private readonly IFileDownloadStatusService _fileDownloadStatusService;
        private readonly ISyncRecordsOfCombinedDistributions _syncRecordsOfCombinedDistributions;
        protected readonly ILogger<NotificationServiceV2> _logger;

        public NotificationServiceV2(IAltinnCorrespondenceService correspondenceService, ILogEntryService logEntryService, IFormMetadataService formMetadataService, IFileStorage fileStorage, IFileDownloadStatusService fileDownloadStatusService, ISyncRecordsOfCombinedDistributions syncRecordsOfCombinedDistributions, ILogger<NotificationServiceV2> logger)
        {
            _correspondenceService = correspondenceService;
            _logEntryService = logEntryService;
            _formMetadataService = formMetadataService;
            _fileStorage = fileStorage;
            _fileDownloadStatusService = fileDownloadStatusService;
            _syncRecordsOfCombinedDistributions = syncRecordsOfCombinedDistributions;
            _logger = logger;
        }


        public virtual void SendNotification(FormData formData)
        {
            _formMetadataService.SaveFormDataToFormMetadataLog(formData);

            if (formData.Mainform is INotificationForm)
            {
                SendNotificationForm(formData, (INotificationForm)formData.Mainform);
            }
            else
                throw new ArgumentException($"Unknown form type {formData.MainformDataFormatID} {formData.MainformDataFormatVersionID} sent for notification.");
        }

        private void SendNotificationForm(FormData formData, INotificationForm notificationForm)
        {
            var stopWatch = new Stopwatch();
            try
            {
                var messageServiceCode = ConfigurationManager.AppSettings["MessageServiceCode"];
                var messageServiceEditionCode = Convert.ToInt32(ConfigurationManager.AppSettings["MessageServiceCodeEdition"]);

                CheckIfNotificationIsAReplyToADistributionAndUpdateMetdataRecords(formData, notificationForm);

                var correspondanceBuilder = new CorrespondanceBuilder();

                InitiateCorrespondence(formData, notificationForm, correspondanceBuilder, messageServiceCode, messageServiceEditionCode);

                correspondanceBuilder.AddContent(notificationForm.GetNotificationMessageTitle(), notificationForm.GetNotificationMessageSummary(), notificationForm.GetNotificationMessageBody());

                //Start File Storage
                if (notificationForm.StoreToBlobs != null && notificationForm.StoreToBlobs.Any())
                    _fileStorage.EstablishFolder(notificationForm.GetReplyKey().ToString(), formData.ArchiveReference);


                //Hovedskjema/svarskjema
                if (notificationForm.CorrespondenceAttachmentsTypes != null && notificationForm.CorrespondenceAttachmentsTypes.Contains(CorrespondenceAttachmentsType.MainFormPdf))
                    correspondanceBuilder.AddBinaryAttachment(notificationForm.GetNotificationPdfFilename(), notificationForm.GetNotificationPdfTitle(), formData.MainFormPdf.AttachmentData, formData.ArchiveReference);

                ////Hovedskjema/svarskjema Add to blobstorage and filedownloadstatus
                if (notificationForm.StoreToBlobs.Contains(StoreToBlob.MainFormPdf))
                {
                    StoreFileToBlobStorage(formData, notificationForm, StoreToBlob.MainFormPdf);
                }

                //Maskinlesbar xml versjon av svarskjema
                if (notificationForm.CorrespondenceAttachmentsTypes != null && notificationForm.CorrespondenceAttachmentsTypes.Contains(CorrespondenceAttachmentsType.MainFormXml))
                    correspondanceBuilder.AddBinaryAttachment(notificationForm.GetNotificationXMLFilename(), notificationForm.GetNotificationXMLTitle(), Encoding.UTF8.GetBytes(formData.FormDataAsXml), formData.ArchiveReference);

                //Maskinlesbar xml versjon av svarskjema Add to blobstorage and filedownloadstatus
                if (notificationForm.StoreToBlobs.Contains(StoreToBlob.MainFormXml))
                    StoreFileToBlobStorage(formData, notificationForm, StoreToBlob.MainFormXml);

                //Attachements
                if (formData.Attachments != null && formData.Attachments.Count != 0)
                {
                    //Sortering på vedlegg etter gruppe i blankett
                    var sortedAttachments = new AttachmentSorter().GenerateSortedListOfAttachments(formData.Attachments);

                    for (int i = 0; i < sortedAttachments.Count(); i++)
                    {
                        var attachment = sortedAttachments[i].Attachment;
                        //Add Vedlegg to Correspondace
                        if (notificationForm.CorrespondenceAttachmentsTypes != null && notificationForm.CorrespondenceAttachmentsTypes.Contains(CorrespondenceAttachmentsType.Attachments))
                            correspondanceBuilder.AddBinaryAttachment(attachment.FileName, attachment.AttachmentTypeName, attachment.AttachmentData, attachment.ArchiveReference);

                        //Add to blobstorage and filedownloadstatus
                        if (notificationForm.StoreToBlobs.Contains(StoreToBlob.Attachments))
                        {
                            StoreFileToBlobStorage(formData, notificationForm, StoreToBlob.Attachments, i);
                        }
                    }
                }


                if (notificationForm.DoEmailNotification())
                {
                    var emailNotification = notificationForm.GetEmailNotification();
                    var prefillForm = formData as IPrefillForm;
                    var notificationTemplate = prefillForm?.GetAltinnNotificationTemplate();

                    correspondanceBuilder.AddEmailAndSmsNotification(NotificationEnums.NotificationCarrier.AltinnEmailPreferred, Resources.TextStrings.DistributionFromEmail, emailNotification.Email, emailNotification.EmailSubject, emailNotification.EmailContent, notificationTemplate, emailNotification.SmsContent);
                }

                var correspondence = correspondanceBuilder.Build();
                var refKey = Guid.NewGuid().ToString();
                stopWatch.Start();
                var correspondenceResponse = _correspondenceService.SendNotification(refKey, correspondence);
                stopWatch.Stop();
                _logEntryService.Save(LogEntry.NewInfoInternal(formData.ArchiveReference, $"Utsending via meldingstjeneste til Altinn: Resultat {correspondenceResponse.ReceiptStatusCode.ToString()}", ProcessLabel.AltinnNotificationCorrespondence, stopWatch));



                if (correspondenceResponse.ReceiptStatusCode != ReceiptStatusEnum.OK)
                {
                    string errorMsg = $"Unntak ved Altinn utsendelse av melding for {formData.Mainform.GetName()} {notificationForm.GetSendToReporteeId()} {correspondenceResponse.ReceiptText}";
                    _logEntryService.Save(LogEntry.NewErrorInternal(formData.ArchiveReference, errorMsg, ProcessLabel.AltinnNotificationCorrespondence));
                    _logEntryService.Save(LogEntry.NewError(formData.ArchiveReference, $"Feil ved levering til Altinn bruker. Feilmelding fra Altinn: {correspondenceResponse.ReceiptText}", ProcessLabel.AltinnNotificationCorrespondence));

                    _formMetadataService.UpdateStatusToFormMetadata(formData.ArchiveReference, "Feil");
                    _syncRecordsOfCombinedDistributions.Sync(formData.Mainform, notificationForm.GetReplyKey());
                    if (notificationForm.IsReply())
                    {
                        _formMetadataService.SaveDistributionFormStatusNotifiedError(
                            notificationForm.GetReplyKey(),
                            DateTime.Now, //replaces correspondanceResponse.LastChanged from Altinn API
                            correspondenceResponse.ReceiptText);
                        _syncRecordsOfCombinedDistributions.Sync(formData.Mainform, notificationForm.GetReplyKey());
                    }
                }
                else
                {
                    _logEntryService.Save(new LogEntry(formData.ArchiveReference, $"Altinn kvittering for {formData.Mainform.GetName()} er sendt ut til mottaker", "Info"));
                    if (notificationForm.IsReply())
                    {
                        var kvitteringsid = correspondenceResponse.ReceiptId.ToString();
                        try
                        {
                            stopWatch.Start();
                            var result = _correspondenceService.GetCorrespondenceStatus(refKey, messageServiceCode, messageServiceEditionCode);
                            stopWatch.Stop();
                            if (result != null && result.CorrespondenceStatusInformation != null &&
                                result.CorrespondenceStatusInformation.CorrespondenceStatusDetailsList != null)
                                if (result.CorrespondenceStatusInformation.CorrespondenceStatusDetailsList.Count() > 0)
                                    kvitteringsid = result.CorrespondenceStatusInformation
                                        .CorrespondenceStatusDetailsList.First().CorrespondenceID.ToString();
                        }
                        catch (FaultException<AltinnFault> af)
                        {
                            _logEntryService.Save(LogEntry.NewError(formData.ArchiveReference, $"Altinn unntak ved henting av Altinn melding status: {af.Message}", ProcessLabel.AltinnNotificationCorrespondence, stopWatch));
                        }
                        catch (Exception e)
                        {
                            _logEntryService.Save(LogEntry.NewError(formData.ArchiveReference, $"Ukjent unntak ved henting av Altinn melding status: {e.Message}", ProcessLabel.AltinnNotificationCorrespondence, stopWatch));
                        }

                        _formMetadataService.SaveDistributionFormStatusNotified(
                            notificationForm.GetReplyKey(),
                            DistributionStatus.receiptSent,
                            DateTime.Now, //replaces correspondanceResponse.LastChanged
                            kvitteringsid
                            );
                        _syncRecordsOfCombinedDistributions.Sync(formData.Mainform, notificationForm.GetReplyKey());
                    }
                }
            }
            catch (FaultException<AltinnFault> af)
            {
                var m = af.Message + " " + af.Detail.AltinnErrorMessage;
                _logEntryService.Save(LogEntry.NewError(formData.ArchiveReference, $"Altinn unntak ved utsending av Altinn melding: {m}", ProcessLabel.AltinnNotificationCorrespondence));
                _formMetadataService.UpdateStatusToFormMetadata(formData.ArchiveReference, "Feil");
            }
            catch (Exception e)
            {
                _logEntryService.Save(LogEntry.NewError(formData.ArchiveReference, $"Ukjent unntak i meldingstjeneste: {e}", ProcessLabel.AltinnNotificationCorrespondence));
                _formMetadataService.UpdateStatusToFormMetadata(formData.ArchiveReference, "Feil");
            }
        }

        private void StoreFileToBlobStorage(FormData formData, INotificationForm notificationForm, StoreToBlob typeBlob, int attachmentIndex = 0)
        {
            string fileName = String.Empty;
            byte[] fileBuffer = null;
            string attachmentTypeName = String.Empty;
            var archiveReference = formData.ArchiveReference;
            var guid = notificationForm.GetReplyKey();
            var formName = formData.Mainform.GetName();

            switch (typeBlob)
            {
                //Hovedskjema - svarskjema PDF
                case StoreToBlob.MainFormPdf:
                    fileName = notificationForm.GetNotificationPdfFilename();
                    fileBuffer = formData.MainFormPdf.AttachmentData;
                    //attachmentTypeName = "AttachmentData";
                    attachmentTypeName = "SkjemaPdf";
                    break;

                // Maskinlesbar xml versjon av svarskjema
                case StoreToBlob.MainFormXml:
                    fileName = notificationForm.GetNotificationXMLFilename();
                    fileBuffer = Encoding.UTF8.GetBytes(formData.FormDataAsXml);
                    attachmentTypeName = "MaskinlesbarXml";
                    break;

                // Vedleggene
                case StoreToBlob.Attachments:
                    if (attachmentIndex < formData.Attachments.Count)
                    {
                        var attachment = formData.Attachments[attachmentIndex];
                        var attachmentId = attachmentIndex + 1;
                        fileName = $"Vedlegg{attachmentId}.{attachment.FileName}";
                        fileBuffer = attachment.AttachmentData;
                        attachmentTypeName = attachment.AttachmentTypeName;
                    }
                    break;
            }

            StoreFileToBlobStorage(archiveReference, guid, formName, fileName, fileBuffer, attachmentTypeName);

        }

        private void StoreFileToBlobStorage(string archiveReference, Guid folderName, string formName, string fileName, byte[] data, string attachmentTypeName)
        {
            FilTyperForNedlasting filTyperForNedlasting = FilTyperForNedlastingHelper.GetEnumForAttachmentTypeName(attachmentTypeName);
            var mimeType = MimeTypeResolver.GetMimeType(fileName);

            var blobUri = _fileStorage.AddFileAsByteArrayToFolder(folderName.ToString(), fileName, data, mimeType);

            if (!string.IsNullOrEmpty(blobUri))
            {
                var fileDownloadStatus = new FileDownloadStatus(
                        archiveReference,
                        folderName,
                        formName,
                        filTyperForNedlasting,
                        fileName, 
                        blobUri, 
                        mimeType);

                _fileDownloadStatusService.Add(fileDownloadStatus);
            }
        }

        private static void InitiateCorrespondence(FormData formData, INotificationForm notificationForm,
            CorrespondanceBuilder correspondanceBuilder, string messageServiceCode, int messageServiceEditionCode)
        {
            if (notificationForm.GetReplyDueDays() == 0)
            {
                if (notificationForm.GetReplyDeadline().HasValue)
                    correspondanceBuilder.SetUpCorrespondence(messageServiceCode,
                        messageServiceEditionCode.ToString(), notificationForm.GetSendToReporteeId(),
                        formData.ArchiveReference, notificationForm.GetReplyDeadline().Value);
                else
                    correspondanceBuilder.SetUpCorrespondence(messageServiceCode,
                        messageServiceEditionCode.ToString(), notificationForm.GetSendToReporteeId(),
                        formData.ArchiveReference, false);
            }
            else if (notificationForm.IsReply())
            {
                correspondanceBuilder.SetUpCorrespondence(messageServiceCode,
                    messageServiceEditionCode.ToString(), notificationForm.GetSendToReporteeId(),
                    formData.ArchiveReference, false);
            }
            else
            {
                correspondanceBuilder.SetUpCorrespondence(messageServiceCode,
                    messageServiceEditionCode.ToString(), notificationForm.GetSendToReporteeId(),
                    formData.ArchiveReference, DateTime.Now.AddDays(notificationForm.GetReplyDueDays()));
            }
        }


        private void CheckIfNotificationIsAReplyToADistributionAndUpdateMetdataRecords(FormData formData, INotificationForm notificationForm)
        {
            if (notificationForm.IsReply())
            {
                if (!_formMetadataService.SaveDistributionFormStatusSigned(notificationForm.GetReplyKey(),
                    DistributionStatus.signed, formData.ArchiveTimestamp.Value, formData.ArchiveReference))
                {
                    _logEntryService.Save(new LogEntry(formData.ArchiveReference,
                        $"Unntak ved utsendelse av melding for innsendingstjeneste {formData.Mainform.GetName()}. Finner ikke tilsvarende utgående distribusjon.",
                        LogEntry.Error, LogEntry.ExternalMsg));
                }
                else
                {
                    _logEntryService.Save(new LogEntry(formData.ArchiveReference,
                        $"Fant svar fra en distribusjonstjeneste {formData.Mainform.GetName()} id {notificationForm.GetReplyKey().ToString()}",
                        LogEntry.Info, LogEntry.InternalMsg));
                    _syncRecordsOfCombinedDistributions.Sync(formData.Mainform, notificationForm.GetReplyKey());
                }
            }
        }
    }

    internal class AttachmentData
    {
        public string Uri { get; set; }
        public string FileName { get; set; }
        public string MimeType { get; set; }
        public FilTyperForNedlasting AttachmentType { get; set; }

        public AttachmentData(string uri, string fileName, string mimeType, FilTyperForNedlasting attachmentType)
        {
            Uri = uri;
            FileName = fileName;
            MimeType = mimeType;
            AttachmentType = attachmentType;
        }

        public AttachmentData() { }
    }
}