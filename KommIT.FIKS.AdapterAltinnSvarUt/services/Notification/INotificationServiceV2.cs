﻿using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Notification
{
    public interface INotificationServiceV2
    {
        void SendNotification(FormData formData);
    }
}