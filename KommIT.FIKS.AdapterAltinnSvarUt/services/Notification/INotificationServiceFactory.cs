﻿namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Notification
{
    public interface INotificationServiceResolver
    {
        INotificationServiceV2 GetService(string dataFormatId);
    }
}