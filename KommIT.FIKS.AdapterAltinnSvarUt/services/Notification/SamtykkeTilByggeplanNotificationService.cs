﻿using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Altinn;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Azure;
using Microsoft.Extensions.Logging;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Notification
{
    [NotificationServiceDataFormatFilter(SupportedDataFormatIds = new string[] { "6147" })]
    public class SamtykkeTilByggeplanNotificationService : NotificationServiceV2
    {
        private readonly ITiltakshaverSamtykkeService _tiltakshaverSamtykkeService;

        public SamtykkeTilByggeplanNotificationService(IAltinnCorrespondenceService correspondenceService,
                                                       ILogEntryService logEntryService,
                                                       IFormMetadataService formMetadataService,
                                                       IFileStorage fileStorage,
                                                       IFileDownloadStatusService fileDownloadStatusService,
                                                       ISyncRecordsOfCombinedDistributions syncRecordsOfCombinedDistributions,
                                                       ITiltakshaverSamtykkeService tiltakshaverSamtykkeService,
                                                       ILogger<SamtykkeTilByggeplanNotificationService> logger)
            : base(correspondenceService,
                   logEntryService,
                   formMetadataService,
                   fileStorage,
                   fileDownloadStatusService,
                   syncRecordsOfCombinedDistributions,
                   logger)
        {
            _tiltakshaverSamtykkeService = tiltakshaverSamtykkeService;
        }

        public override void SendNotification(FormData formData)
        {
            // https://arkitektum.atlassian.net/browse/FBYGG-2374 (Slette blob etter utfoert sig)
            // https://arkitektum.atlassian.net/browse/FBYGG-2405 (Slette dobbelt opp vedlegg ... )
            // https://arkitektum.atlassian.net/browse/FBYGG-2946 (Bare PDF i melding til AS) DONE!
            // https://arkitektum.atlassian.net/browse/FBYGG-2960 (Rydd i database tabeller ... samme navn )

            _tiltakshaverSamtykkeService.SaveReporteeToSamtykke(formData.ArchiveReference, formData.Reportee);
            base.SendNotification(formData);

            var id = _tiltakshaverSamtykkeService.GetSamtykkeTiltakshaverId(formData.ArchiveReference);

            if (!string.IsNullOrEmpty(id))
                _tiltakshaverSamtykkeService.ArchiveSamtykke(id);

            //Slett container med samme id som tiltakshaversamtykke
            var containerDeleted = _fileStorage.DeleteContainer(id);

            //Sett tiltakshaverssamtykkevedlegg.uploaded = true (kva nå enn det betyr...)
            _tiltakshaverSamtykkeService.UpdateVedleggStatus(id, true);

            //Oppdater formmetadata.application med FraSluttbrukerSystem
            // da dette ikkje er med i XMLen
            UpdateFormMetadataApplication(id, formData.ArchiveReference);
        }

        private void UpdateFormMetadataApplication(string tiltakshaverSamtykkeId, string archiveReference)
        {
            try
            {
                var tiltakshaverssamtykke = _tiltakshaverSamtykkeService.GetTiltakshaversSamtykke(tiltakshaverSamtykkeId);
                var formmmetadata = _formMetadataService.GetFormMetadata(archiveReference);

                if (formmmetadata != null && tiltakshaverssamtykke != null && !string.IsNullOrEmpty(tiltakshaverssamtykke.FraSluttbrukerSystem))
                {
                    formmmetadata.Application = tiltakshaverssamtykke.FraSluttbrukerSystem;
                    _formMetadataService.UpdateFormMetadata(formmmetadata);
                }
            }
            catch (System.Exception ex)
            {
                _logger.LogError(ex, "Error updating form metadata application");
            }
        }
    }
}