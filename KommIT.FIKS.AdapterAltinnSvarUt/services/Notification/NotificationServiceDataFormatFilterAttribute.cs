﻿using System;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Notification
{
    public class NotificationServiceDataFormatFilterAttribute : Attribute
    {
        public string[] SupportedDataFormatIds { get; set; }
    }
}