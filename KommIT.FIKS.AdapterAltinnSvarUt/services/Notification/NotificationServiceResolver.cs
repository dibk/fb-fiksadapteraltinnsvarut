﻿using System.Collections.Generic;
using System.Linq;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Notification
{
    public class NotificationServiceResolver : INotificationServiceResolver
    {
        private readonly IEnumerable<INotificationServiceV2> _services;

        public NotificationServiceResolver(IEnumerable<INotificationServiceV2> services)
        {
            _services = services;
        }

        public INotificationServiceV2 GetService(string dataFormatId)
        {
            INotificationServiceV2 service = null;

            foreach (var serviceCandidate in _services)
            {
                var configuredAttribute = serviceCandidate.GetType().GetCustomAttributes(typeof(NotificationServiceDataFormatFilterAttribute), false).FirstOrDefault() as NotificationServiceDataFormatFilterAttribute;
                if (configuredAttribute != null)
                    if (configuredAttribute.SupportedDataFormatIds.Contains(dataFormatId))
                    {
                        service = serviceCandidate;
                        break;
                    }
            }

            if (service == null)
                service = GetDefaultService();
                
            return service;
        }

        private INotificationServiceV2 GetDefaultService()
        {
            var defaultService = _services.Where(s => s.GetType().GetCustomAttributes(typeof(NotificationServiceDataFormatFilterAttribute), false).FirstOrDefault() == null).FirstOrDefault();
            return defaultService;
        }
    }
}