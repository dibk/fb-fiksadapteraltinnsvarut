﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Altinn;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Azure;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.WS.AltinnCorrespondenceAgency;
using Serilog;
using System.Configuration;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Notification
{
    public class SendCorrespondenceHelper
    {

        private static IAltinnCorrespondenceService _correspondenceService;
        private readonly ILogEntryService _logEntryService;
        private readonly ILogger _logger;


        public SendCorrespondenceHelper(IAltinnCorrespondenceService correspondenceService, ILogEntryService logEntryService, ILogger logger)
        {
            _correspondenceService = correspondenceService;
            _logEntryService = logEntryService;
            _logger = logger;
        }


        public void SendSimpleNotificaitonToReportee(string reportee, string title, string summary, string body, string archiveReference)
        {
            string messageServiceCode = ConfigurationManager.AppSettings["MessageServiceCode"]; ;
            int messageServiceEditionCode = Convert.ToInt32(ConfigurationManager.AppSettings["MessageServiceCodeEdition"]);


            CorrespondanceBuilder correspondanceBuilder = new CorrespondanceBuilder();
            correspondanceBuilder.SetUpCorrespondence(messageServiceCode, messageServiceEditionCode.ToString(), reportee, archiveReference, false); //Uten å ta hensyn til reservasjon
            correspondanceBuilder.AddContent(title, summary, body);
            
            InsertCorrespondenceV2 correspondence = correspondanceBuilder.Build();
            string refKey = Guid.NewGuid().ToString();
            ReceiptExternal correspondanceResponse = _correspondenceService.SendNotification(archiveReference, correspondence);

            if (correspondanceResponse.ReceiptStatusCode != ReceiptStatusEnum.OK)
            {
                _logEntryService.Save(new LogEntry(archiveReference, Resources.TextStrings.NotificationExceptionSendSimpleToReportee ,"Error"));
                _logger.Error($"Error sending notificaiton to Altinn for reportee {reportee} and archive reference {archiveReference} {correspondanceResponse.ReceiptHistory}");
                throw new InvalidOperationException($"Error sending notificaiton to Altinn for reportee {reportee} and archive reference {archiveReference} {correspondanceResponse.ReceiptHistory}");
            }
        }
    }
}