﻿using DIBK.FBygg.Altinn.MapperCodelist;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.PostalCode;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Validations;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;
using no.kxml.skjema.dibk.ferdigattestV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.SvarUtForm
{
    public class FerdigattestFormV2Validator
    {
        CodeListService _codeListService = new CodeListService();

        private ValidationResult _validationResult = new ValidationResult();
        internal ValidationResult GetResult() => _validationResult;
        public IMunicipalityValidator MunicipalityValidator { get; set; }
        public IPostalCodeProvider PostalCodeProvider { get; set; }


        public FerdigattestFormV2Validator()
        {
            MunicipalityValidator = new MunicipalityValidator();
            PostalCodeProvider = new BringPostalCodeProvider();

            _validationResult = new ValidationResult();
            var skjema = "Ferdigattest";
            var form = new FerdigattestType();

            _validationResult.AddRule("4400.1.1", "Innhold må være ihht informasjonsmodell/xsd for skjema.", "", "ERROR", "");
            _validationResult.AddRule("4400.1.2", "Koder må være ihht publiserte kodelister i XSD skjema.", "", "ERROR", "");



            _validationResult.AddRule(skjema, "4400.1.6", null, "Må fylle ut fraSluttbrukersystem med samme navn som brukt i registrering for Altinn API", Helpers.GetFullClassPath(() => form.fraSluttbrukersystem), "ERROR", null);
            //EiendomByggestedValidation
            _validationResult.AddRule(skjema, "4400.1.1.2", null, "Minst en eiendom må være definert", Helpers.GetFullClassPath(() => form.eiendomByggested[0]), "ERROR", null);
            _validationResult.AddRule(skjema, "4400.1.1.1", null, "kommunenummer må fylles ut for eiendom/byggested", Helpers.GetFullClassPath(() => form.eiendomByggested[0].eiendomsidentifikasjon.kommunenummer), "ERROR", null);
            _validationResult.AddRule(skjema, "4400.1.2.1", null, "Kommunenavn bør fylles ut for eiendom/byggested", Helpers.GetFullClassPath(() => form.eiendomByggested[0].kommunenavn), "WARNING", null);
            _validationResult.AddRule(skjema, "4400.2.13", null, "Hvis bruksenhetsnummer(bolignummer) er fylt ut må det følge riktig format (^[HKLU]\\d{4}$) (feks H0101) se https://www.kartverket.no/eiendom/adressering/bruksenhetsnummer/", Helpers.GetFullClassPath(() => form.eiendomByggested[0].bolignummer), "ERROR", null);
            //Validate_kommunenummerStatus
            _validationResult.AddRule(skjema, "4400.1.2.2", null, "kommunenummer må fylles ut for eiendom/byggested", Helpers.GetFullClassPath(() => form.eiendomByggested[0].eiendomsidentifikasjon.kommunenummer), "ERROR", null);
            _validationResult.AddRule(skjema, "4400.1.2.3", null, "Kommunenummeret '{0}' for eiendom/byggested finnes ikke i kodelisten. Du kan sjekke riktig kommunenummer på https://register.geonorge.no/sosi-kodelister/inndelinger/inndelingsbase/kommunenummer", Helpers.GetFullClassPath(() => form.eiendomByggested[0].eiendomsidentifikasjon.kommunenummer), "ERROR", null);
            _validationResult.AddRule(skjema, "4400.1.2.4", null, "Kommunenummeret '{0}' for eiendom/byggested har ugyldig status ({1}). Du kan sjekke status på https://register.geonorge.no/sosi-kodelister/inndelinger/inndelingsbase/kommunenummer", Helpers.GetFullClassPath(() => form.eiendomByggested[0].eiendomsidentifikasjon.kommunenummer), "ERROR", null);

            //HartilfredstillerTiltaketKraveneFerdigattest
            _validationResult.AddRule(skjema, "4400.2.7", null, "Når tiltaket ikke tilfredsstiller kravene til ferdigattest må utført innen, type arbeider og bekreftelse innen fylles ut.", Helpers.GetFullClassPath(() => form.tilfredstillerTiltaketKraveneFerdigattest), "ERROR", null);
            _validationResult.AddRule(skjema, "4400.2.8", null, "Følgende arbeider vil bli utført innen kan være maks om 14 dager", Helpers.GetFullClassPath(() => form.utfoertInnen), "ERROR", null);
            _validationResult.AddRule(skjema, "4400.2.9", null, "Bekreftelse på at disse arbeidene er utført vil være kommunen i hende innen kan være maks om 14 dager.", Helpers.GetFullClassPath(() => form.bekreftelseInnen), "ERROR", null);
            //KommunensSaksnummerValidator
            _validationResult.AddRule(skjema, "4400.2.11", null, "Kommunens saksnummer må være fylt ut.", Helpers.GetFullClassPath(() => form.kommunensSaksnummer), "ERROR", null);

            //foretattIkkeSoeknadspliktigeJusteringerValifatoTegningValidering
            _validationResult.AddRule(skjema, "4400.2.12", null, "Vedlegg for Tegning ny plan, Tegning ny fasade, Tegning nytt snitt skal bare sendes inn om de er knyttet til vilkår eller inneholder ikke søknadspliktige endringer.", "/Vedlegg", "WARNING", Helpers.GetFullClassPath(() => form.foretattIkkeSoeknadspliktigeJusteringer));
            //KvitteringForLevertAvfallVedleggValidering
            _validationResult.AddRule(skjema, "4400.2.14", null, "Du har lagt ved ‘Sluttrapport for bygningsavfall’. Da må du også legge ved vedlegget ‘KvitteringForLevertAvfall’ med oversikt over avfallet som er levert i sluttrapporten.", "/Vedlegg", "ERROR", null);
            _validationResult.AddRule(skjema, "4400.2.16", null, "Hvis det er krav om å utarbeide avfallsplan i hovedsøknaden, må ‘Sluttrapport for bygningsavfall’ legges ved.", "/Vedlegg", "WARNING", null);

            //TODO situasjonsplan og avkjøringsplan?

            //søker validering
            _validationResult.AddRule(skjema, "4400.1.1.4", null, "Du har ikke fylt ut informasjon om søker. Hvis du søker med ansvarsrett, må du fylle ut informasjon om ansvarlig søker. For tiltak uten ansvarsrett, må du fylle ut informasjon om tiltakshaver.", Helpers.GetFullClassPath(() => form), "ERROR", null);

            //Tiltakshaver
            _validationResult.AddRule(skjema, "4400.2.6", null, "Kodeverdien for 'partstype' til tiltakshaver må fylles ut.", Helpers.GetFullClassPath(() => form.tiltakshaver.partstype.kodeverdi), "ERROR", null);
            _validationResult.AddRule(skjema, "4400.2.6.1", null, "'{0}' er en ugyldig kodeverdi for partstypen til tiltakshaver. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/byggesoknad/partstype", Helpers.GetFullClassPath(() => form.tiltakshaver.partstype.kodeverdi), "ERROR", null);
            //Tiltakshaver foedselsnummer
            _validationResult.AddRule(skjema, "4400.2.6.2", null, "Fødselsnummer må fylles ut når tiltakshaver er en privatperson.", Helpers.GetFullClassPath(() => form.tiltakshaver.foedselsnummer), "ERROR", null);
            _validationResult.AddRule(skjema, "4400.2.6.2.1", null, "Fødselsnummeret til tiltakshaver er ikke gyldig.", Helpers.GetFullClassPath(() => form.tiltakshaver.foedselsnummer), "ERROR", null);
            _validationResult.AddRule(skjema, "4400.2.6.2.2", null, "Fødselsnummeret til tiltakshaver har ikke gyldig kontrollsiffer.", Helpers.GetFullClassPath(() => form.tiltakshaver.foedselsnummer), "ERROR", null);
            _validationResult.AddRule(skjema, "4400.2.6.2.3", null, "Fødselsnummeret til tiltakshaver kan ikke dekrypteres.", Helpers.GetFullClassPath(() => form.tiltakshaver.foedselsnummer), "ERROR", null);
            //Tiltakshaver organisasjonsnummer      
            _validationResult.AddRule(skjema, "4400.2.6.3", null, "Organisasjonsnummeret til tiltakshaver må fylles ut når tiltakshaver er en organisasjon.", Helpers.GetFullClassPath(() => form.tiltakshaver.organisasjonsnummer), "ERROR", null);
            _validationResult.AddRule(skjema, "4400.2.6.3.1", null, "Organisasjonsnummeret ('{0}') til tiltakshaver har ikke gyldig kontrollsiffer.", Helpers.GetFullClassPath(() => form.tiltakshaver.organisasjonsnummer), "ERROR", null);
            _validationResult.AddRule(skjema, "4400.2.6.3.2", null, "Organisasjonsnummeret ('{0}') til tiltakshaver er ikke gyldig.", Helpers.GetFullClassPath(() => form.tiltakshaver.organisasjonsnummer), "ERROR", null);
            _validationResult.AddRule(skjema, "4400.2.6.4", null, "Kontaktperson bør fylles ut når tiltakshaver er en organisasjon.", Helpers.GetFullClassPath(() => form.tiltakshaver.kontaktperson), "WARNING", null);
            //tiltakshaver adresse
            _validationResult.AddRule(skjema, "4400.2.6.5", null, "Adressen til tiltakshaver bør fylles ut.", Helpers.GetFullClassPath(() => form.tiltakshaver.adresse), "WARNING", null);
            _validationResult.AddRule(skjema, "4400.2.6.6", null, "Telefonnummeret eller mobilnummeret til tiltakshaver må fylles ut.", Helpers.GetFullClassPath(() => form.tiltakshaver.mobilnummer), "WARNING", Helpers.GetFullClassPath(() => form.tiltakshaver.telefonnummer));
            _validationResult.AddRule(skjema, "4400.2.6.7", null, "Navnet til tiltakshaver må fylles ut.", Helpers.GetFullClassPath(() => form.tiltakshaver.navn), "ERROR", null);

            //Ansvarlig søker
            _validationResult.AddRule(skjema, "4400.1.1.3", null, "Kodeverdien for 'partstype' til ansvarlig søker må fylles ut.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.partstype.kodeverdi), "ERROR", null);
            _validationResult.AddRule(skjema, "4400.1.2.5", null, "'{0}' er en ugyldig kodeverdi for partstypen til ansvarlig søker. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/byggesoknad/partstype", Helpers.GetFullClassPath(() => form.ansvarligSoeker.partstype.kodeverdi), "ERROR", null);

            //Ansvarlig søker foedselsnummer
            _validationResult.AddRule(skjema, "4400.2.5", null, "Fødselsnummer må fylles ut når ansvarlig søker er en privatperson.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.foedselsnummer), "ERROR", null);
            _validationResult.AddRule(skjema, "4400.2.5.1", null, "Fødselsnummeret til ansvarlig søker er ikke gyldig.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.foedselsnummer), "ERROR", null);
            _validationResult.AddRule(skjema, "4400.2.5.2", null, "Fødselsnummeret til ansvarlig søker har ikke gyldig kontrollsiffer.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.foedselsnummer), "ERROR", null);
            _validationResult.AddRule(skjema, "4400.2.5.3", null, "Fødselsnummeret til ansvarlig søker kan ikke dekrypteres.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.foedselsnummer), "ERROR", null);
            //Ansvarlig søker organisasjonsnummer      
            _validationResult.AddRule(skjema, "4400.2.5.4", null, "Organisasjonsnummeret til ansvarlig søker må fylles ut.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.organisasjonsnummer), "ERROR", null);
            _validationResult.AddRule(skjema, "4400.2.5.5", null, "Organisasjonsnummeret ('{0}') til ansvarlig søker har ikke gyldig kontrollsiffer.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.organisasjonsnummer), "ERROR", null);
            _validationResult.AddRule(skjema, "4400.2.5.6", null, "Organisasjonsnummeret ('{0}') til ansvarlig søker er ikke gyldig.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.organisasjonsnummer), "ERROR", null);
            _validationResult.AddRule(skjema, "4400.2.14.1", null, "Kontaktpersonen til ansvarlig søker bør fylles ut.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.kontaktperson), "WARNING", null);
            //Ansvarlig søker adresse
            _validationResult.AddRule(skjema, "4400.1.4", null, "Adressen til ansvarlig søker bør fylles ut.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.adresse), "WARNING", null);
            _validationResult.AddRule(skjema, "4400.1.4.1", null, "Adresselinje 1 bør fylles ut for ansvarlig søker.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.adresse.adresselinje1), "WARNING", null);
            _validationResult.AddRule(skjema, "4400.1.4.2", null, "Ugyldig landkode for ansvarlig søker.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.adresse.landkode), "WARNING", null);
            _validationResult.AddRule(skjema, "4400.1.4.3", null, "Postnummeret til ansvarlig søker bør angis.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.adresse.postnr), "WARNING", null);
            _validationResult.AddRule(skjema, "4400.1.4.4", null, "Postnummeret '{0}' til ansvarlig søker har ikke gyldig kontrollsiffer.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.adresse.postnr), "WARNING", null);
            _validationResult.AddRule(skjema, "4400.1.4.4.1", null, "Postnummeret '{0}' til ansvarlig søker er ugyldig. Du kan sjekke riktig postnummer på http://adressesok.bring.no/", Helpers.GetFullClassPath(() => form.ansvarligSoeker.adresse.postnr), "WARNING", null);
            _validationResult.AddRule(skjema, "4400.1.4.5", null, "Postnummeret '{0}' til ansvarlig søker stemmer ikke overens med poststedet '{1}'. Riktig postnummer er '{2}'. Du kan sjekke riktig poststed på http://adressesok.bring.no/", Helpers.GetFullClassPath(() => form.ansvarligSoeker.adresse.postnr), "WARNING", null);
            _validationResult.AddRule(skjema, "4400.1.4.6", null, "Postnummeret '{0}' til ansvarlig søker ble ikke validert. Postnummeret kan være riktig, men en teknisk feil gjør at vi ikke kan bekrefte det.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.adresse.postnr), "WARNING", null);
            _validationResult.AddRule(skjema, "4400.2.15", null, "Telefonnummeret eller mobilnummeret til ansvarlig søker bør fylles ut.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.mobilnummer), "WARNING", Helpers.GetFullClassPath(() => form.ansvarligSoeker.telefonnummer));

            _validationResult.AddRule(skjema, "4400.1.4.8", null, "Navnet til ansvarlig søker må fylles ut.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.navn), "ERROR", null);
        }

        public ValidationResult Validate(FerdigattestType form, List<string> formSetElements)
        {
            Parallel.Invoke(
                () => { FraSluttbrukersystemValidation(form); },
                () => { EiendomByggestedValidation(form); },
                () => { SoekerValidation(form); },
                () => { HartilfredstillerTiltaketKraveneFerdigattest(form); },
                () => { KommunensSaksnummerValidator(form); },
                () => { ForetattIkkeSoeknadspliktigeJusteringerValifatoTegningValidering(form, formSetElements); },
                () => { KvitteringForLevertAvfallVedleggValidering(form, formSetElements); },
                () => { AvfallVedleggValidering(form, formSetElements); }
                );

            return _validationResult;
        }

        internal void AvfallVedleggValidering(FerdigattestType form, List<string> formSetElements)
        {
            if (formSetElements != null)
                if (!formSetElements.Contains("SluttrapportForBygningsavfall") && !formSetElements.Contains("Sluttrapport for bygningsavfall"))
                    _validationResult.AddMessage("4400.2.16", "/Vedlegg");
        }

        internal void KvitteringForLevertAvfallVedleggValidering(FerdigattestType form, List<string> formSetElements)
        {
            if (formSetElements != null)
                if (formSetElements.Contains("SluttrapportForBygningsavfall") || formSetElements.Contains("Sluttrapport for bygningsavfall"))
                    if (!formSetElements.Contains("KvitteringForLevertAvfall"))
                    {
                        //_validationResult.AddError("Veielapp(er) skal med dersom det skal være sluttrapport for avfallplan", "4400.2.14", "/Vedlegg");
                        _validationResult.AddMessage("4400.2.14", "/Vedlegg");
                    }
        }

        internal void FraSluttbrukersystemValidation(FerdigattestType form)
        {
            if (String.IsNullOrEmpty(form.fraSluttbrukersystem))
                _validationResult.AddMessage("4400.1.6", null);
        }

        internal void SoekerValidation(FerdigattestType form)
        {
            if (Helpers.ObjectIsNullOrEmpty(form.ansvarligSoeker) && Helpers.ObjectIsNullOrEmpty(form.tiltakshaver))
            {
                _validationResult.AddMessage("4400.1.1.4", null);
            }
            else
            {
                if (Helpers.ObjectIsNullOrEmpty(form.ansvarligSoeker))
                {
                    if (Helpers.ObjectIsNullOrEmpty(form.tiltakshaver.partstype))
                    {
                        _validationResult.AddMessage("4400.2.6", null);
                    }
                    else
                    {
                        if (!_codeListService.IsCodelistValid("Partstype", form.tiltakshaver.partstype?.kodeverdi))
                        {
                            _validationResult.AddMessage("4400.2.6.1", new[] { form.tiltakshaver.partstype?.kodeverdi });
                        }
                        else
                        {
                            if (form.tiltakshaver.partstype?.kodeverdi == "Privatperson")
                            {
                                var foedselsnummerValidation = GeneralValidations.Validate_foedselsnummer(form.tiltakshaver.foedselsnummer);
                                switch (foedselsnummerValidation)
                                {
                                    case GeneralValidations.FoedselnumerValidation.Empty:
                                        _validationResult.AddMessage("4400.2.6.2", null);
                                        break;
                                    case GeneralValidations.FoedselnumerValidation.Invalid:
                                        _validationResult.AddMessage("4400.2.6.2.1", null);
                                        break;
                                    case GeneralValidations.FoedselnumerValidation.InvalidDigitsControl:
                                        _validationResult.AddMessage("4400.2.6.2.2", null);
                                        break;
                                    case GeneralValidations.FoedselnumerValidation.InvalidEncryption:
                                        _validationResult.AddMessage("4400.2.6.2.3", null);
                                        break;
                                }
                            }
                            else
                            {
                                var organisasjonsnummerValidation = GeneralValidations.Validate_OrgnummerEnum(form.tiltakshaver.organisasjonsnummer);
                                switch (organisasjonsnummerValidation)
                                {
                                    case GeneralValidations.OrganisasjonsnummerValidation.Empty:
                                        _validationResult.AddMessage("4400.2.6.3", null);
                                        break;
                                    case GeneralValidations.OrganisasjonsnummerValidation.Invalid:
                                        _validationResult.AddMessage("4400.2.6.3.1", new[] { form.tiltakshaver.organisasjonsnummer });
                                        break;
                                    case GeneralValidations.OrganisasjonsnummerValidation.InvalidDigitsControl:
                                        _validationResult.AddMessage("4400.2.6.3.2", new[] { form.tiltakshaver.organisasjonsnummer });
                                        break;
                                }
                                if (Helpers.ObjectIsNullOrEmpty(form.tiltakshaver.kontaktperson))
                                {
                                    _validationResult.AddMessage("4400.2.6.4", null);
                                }
                            }
                        }
                    }
                    if (Helpers.ObjectIsNullOrEmpty(form.tiltakshaver.adresse))
                        _validationResult.AddMessage("4400.2.6.5", null);

                    if (string.IsNullOrEmpty(form.tiltakshaver.telefonnummer) && string.IsNullOrEmpty(form.tiltakshaver.mobilnummer))
                        _validationResult.AddMessage("4400.2.6.6", null);

                    if (string.IsNullOrEmpty(form.tiltakshaver.navn))
                        _validationResult.AddMessage("4400.2.6.7", null);

                }
                else
                {
                    if (form.ansvarligSoeker.partstype == null)
                    {
                        _validationResult.AddMessage("4400.1.1.3", null);
                    }
                    else
                    {
                        if (!_codeListService.IsCodelistValid("Partstype", form.ansvarligSoeker.partstype.kodeverdi))
                        {
                            _validationResult.AddMessage("4400.1.2.5", new[] { form.ansvarligSoeker.partstype.kodeverdi });
                        }
                        else
                        {

                            if (form.ansvarligSoeker.partstype.kodeverdi == "Privatperson")
                            {
                                var foedselsnummerValidation = GeneralValidations.Validate_foedselsnummer(form.ansvarligSoeker.foedselsnummer);
                                switch (foedselsnummerValidation)
                                {
                                    case GeneralValidations.FoedselnumerValidation.Empty:
                                        _validationResult.AddMessage("4400.2.5", null);
                                        break;
                                    case GeneralValidations.FoedselnumerValidation.Invalid:
                                        _validationResult.AddMessage("4400.2.5.1", null);
                                        break;
                                    case GeneralValidations.FoedselnumerValidation.InvalidDigitsControl:
                                        _validationResult.AddMessage("4400.2.5.2", null);
                                        break;
                                    case GeneralValidations.FoedselnumerValidation.InvalidEncryption:
                                        _validationResult.AddMessage("4400.2.5.3", null);
                                        break;
                                }
                            }
                            else
                            {
                                var organisasjonsnummerValidation = GeneralValidations.Validate_OrgnummerEnum(form.ansvarligSoeker.organisasjonsnummer);
                                switch (organisasjonsnummerValidation)
                                {
                                    case GeneralValidations.OrganisasjonsnummerValidation.Empty:
                                        _validationResult.AddMessage("4400.2.5.4", null);
                                        break;
                                    case GeneralValidations.OrganisasjonsnummerValidation.InvalidDigitsControl:
                                        _validationResult.AddMessage("4400.2.5.5", new[] { form.ansvarligSoeker.organisasjonsnummer });
                                        break;
                                    case GeneralValidations.OrganisasjonsnummerValidation.Invalid:
                                        _validationResult.AddMessage("4400.2.5.6", new[] { form.ansvarligSoeker.organisasjonsnummer });
                                        break;
                                }

                                if (string.IsNullOrEmpty(form.ansvarligSoeker.kontaktperson))
                                {
                                    _validationResult.AddMessage("4400.2.14.1", null);
                                }
                            }
                        }
                    }

                    if (Helpers.ObjectIsNullOrEmpty(form.ansvarligSoeker.adresse))
                    {
                        _validationResult.AddMessage("4400.1.4", null);
                    }
                    else
                    {
                        if (String.IsNullOrEmpty(form.ansvarligSoeker.adresse.adresselinje1))
                        {
                            _validationResult.AddMessage("4400.1.4.1", null);
                        }
                        else
                        {
                            if (!CountryCodeHandler.IsCountryNorway(form.ansvarligSoeker.adresse.landkode))
                            {
                                if (!CountryCodeHandler.VerifyCountryCode(form.ansvarligSoeker.adresse.landkode))
                                {
                                    _validationResult.AddMessage("4400.1.4.2", null);
                                }
                            }
                            else
                            {
                                var postNr = form.ansvarligSoeker.adresse.postnr;
                                var landkode = form.ansvarligSoeker.adresse.landkode;

                                if (string.IsNullOrEmpty(form.ansvarligSoeker.adresse.postnr))
                                {
                                    _validationResult.AddMessage("4400.1.4.3", null);
                                }
                                else
                                {
                                    if (!GeneralValidations.postNumberMatch(form.ansvarligSoeker.adresse.postnr).Success)
                                    {
                                        _validationResult.AddMessage("4400.1.4.4", new[] { postNr });
                                    }
                                    else
                                    {
                                        var postnrValidation = PostalCodeProvider.ValidatePostnr(postNr, landkode);
                                        if (Helpers.ObjectIsNullOrEmpty(postnrValidation))
                                        {
                                            _validationResult.AddMessage("4400.1.4.6", null);
                                        }
                                        else
                                        {
                                            if (!postnrValidation.Valid)
                                            {
                                                _validationResult.AddMessage("4400.1.4.4.1", null);
                                            }
                                            else
                                            {
                                                if (!postnrValidation.Result.Equals(form.ansvarligSoeker.adresse.poststed, StringComparison.CurrentCultureIgnoreCase))
                                                {
                                                    _validationResult.AddMessage("4400.1.4.5", new[] { postNr, form.ansvarligSoeker.adresse.poststed, postnrValidation.Result });
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if (string.IsNullOrEmpty(form.ansvarligSoeker.mobilnummer) && string.IsNullOrEmpty(form.ansvarligSoeker.telefonnummer))
                        _validationResult.AddMessage("4400.2.15", null);

                    if (String.IsNullOrEmpty(form.ansvarligSoeker.navn))
                        _validationResult.AddMessage("4400.1.4.8", null);
                }
            }
        }
        internal void HartilfredstillerTiltaketKraveneFerdigattest(FerdigattestType form)
        {
            if (form.tilfredstillerTiltaketKraveneFerdigattest.HasValue)
            {
                if (form.tilfredstillerTiltaketKraveneFerdigattest == false)
                {
                    if (!TiltaketIkkeHarTilstrekkelig(form))
                    {
                        _validationResult.AddMessage("4400.2.7", null);
                    }
                    if (form.utfoertInnenSpecified)
                    {
                        if (form.utfoertInnen != null)
                        {
                            if ((form.utfoertInnen.Value - DateTime.Now).TotalDays > 14)
                                _validationResult.AddMessage("4400.2.8", null);
                        }
                    }

                    if (form.bekreftelseInnenSpecified)
                    {
                        if (form.bekreftelseInnen != null)
                        {
                            if ((form.bekreftelseInnen.Value - DateTime.Now).TotalDays > 14)
                                _validationResult.AddMessage("4400.2.9", null);
                        }
                    }
                }
            }
        }

        internal bool TiltaketIkkeHarTilstrekkelig(FerdigattestType form)
        {
            if (string.IsNullOrEmpty(form.typeArbeider)) return false;
            if (string.IsNullOrEmpty(form.utfoertInnen.ToString())) return false;
            if (string.IsNullOrEmpty(form.bekreftelseInnen.ToString())) return false;
            return true;
        }

        internal void KommunensSaksnummerValidator(FerdigattestType form)
        {
            if (form.kommunensSaksnummer == null || string.IsNullOrEmpty(form.kommunensSaksnummer.saksaar) || string.IsNullOrEmpty(form.kommunensSaksnummer.sakssekvensnummer))
            {
                _validationResult.AddMessage("4400.2.11", null);
            }
        }

        internal void EiendomByggestedValidation(FerdigattestType form)
        {
            if (form.eiendomByggested == null || !form.eiendomByggested.Any())
                _validationResult.AddMessage("4400.1.1.2", null);
            else
            {
                foreach (var eiendom in form.eiendomByggested)
                {
                    if (eiendom.eiendomsidentifikasjon != null)
                    {
                        if (String.IsNullOrEmpty(eiendom.eiendomsidentifikasjon.kommunenummer))
                        {
                            _validationResult.AddMessage("4400.1.1.1", null);
                        }
                        else
                        {
                            string kommunenummer = eiendom.eiendomsidentifikasjon.kommunenummer;
                            var kommunenummerValideringStatus = MunicipalityValidator.Validate_kommunenummerStatus(kommunenummer);
                            if (kommunenummerValideringStatus?.Status != MunicipalityValidation.Ok)
                            {
                                switch (kommunenummerValideringStatus?.Status)
                                {
                                    case MunicipalityValidation.Empty:
                                        _validationResult.AddMessage("4400.1.2.2", null);
                                        break;
                                    case MunicipalityValidation.Invalid:
                                        _validationResult.AddMessage("4400.1.2.3", new[] { kommunenummer });
                                        break;
                                    case MunicipalityValidation.Expired:
                                        _validationResult.AddMessage("4400.1.2.4", new[] { kommunenummer, kommunenummerValideringStatus.Status.ToString() });
                                        break;
                                }
                            }
                        }
                    }

                    if (String.IsNullOrEmpty(eiendom.kommunenavn))
                        _validationResult.AddMessage("4400.1.2.1", null);

                    if (!String.IsNullOrEmpty(eiendom.bolignummer))
                    {
                        if (GeneralValidations.Validate_bruksenhetsnummer(eiendom.bolignummer).Status == ValidationStatus.Fail)
                            _validationResult.AddMessage("4400.2.13", null);
                    }
                }
            }
        }

        internal void ForetattIkkeSoeknadspliktigeJusteringerValifatoTegningValidering(FerdigattestType form, List<string> formSetElements)
        {
            var tengningen = new List<string>()
            {
                "TegningNyttSnitt",
                "TegningNyPlan",
                "TegningNyFasade",
            };

            bool hasDrawing = formSetElements?.Select(x => x)
                .Intersect(tengningen)
                .Any() ?? false;


            if (!form.foretattIkkeSoeknadspliktigeJusteringer.HasValue || form.foretattIkkeSoeknadspliktigeJusteringer.GetValueOrDefault())
            {
                if (!hasDrawing)
                {
                    _validationResult.AddMessage("4400.2.12", null);
                }
            }
        }


    }

}