﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DIBK.FBygg.Altinn.MapperCodelist;
using KommIT.FIKS.AdapterAltinnSvarUt.Resources;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Validations;
using no.kxml.skjema.dibk.igangsettingstillatelseV2;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.SvarUtForm
{
    public class IgangsettingstillatelseFormV2Validator
    {
        CodeListService _codeListService = new CodeListService();

        private readonly ValidationResult _validationResult = new ValidationResult();
        private List<string> _tiltakstyperISoeknad = new List<string>();

        internal ValidationResult GetResult() => _validationResult;

        public IMunicipalityValidator MunicipalityValidator { get; set; }
        public IgangsettingstillatelseFormV2Validator()
        {
            MunicipalityValidator = new MunicipalityValidator();
        }

        public ValidationResult Validate(IgangsettingstillatelseType form, List<string> formSetElements)
        {
            AnsvarValidering(form, formSetElements);
            SoknadsDelerAvTiltakenValidering(form);
            ArbeidsplasserValidering(form, formSetElements);
            KommunensSaksnummerValidering(form);
            EiendomByggestedValidation(form);
            AnsvarligSoekerValidation(form);

            return _validationResult;
        }

        internal void AnsvarValidering(IgangsettingstillatelseType form, List<string> formSetElements)
        {
            if (formSetElements == null)
            {
                _validationResult.AddError("Gjennomføringsplan eller Søknad om ansvarsrett for selvbygger skal følge med søknaden.", "4401.2.8", "/VedleggUnderskjema");
            }
            else if (!formSetElements.Contains("AnsvarsrettSelvbygger") && !formSetElements.Contains("Gjennomføringsplan") && !formSetElements.Contains("Gjennomfoeringsplan") && !formSetElements.Contains("GjennomføringsplanV4")) // && !formSetElements.Contains("GjennomforingsplanV4") ?
            {
                _validationResult.AddError("Gjennomføringsplan eller Søknad om ansvarsrett for selvbygger skal følge med søknaden.", "4401.2.8", "/VedleggUnderskjema");
            }
        }

        internal void SoknadsDelerAvTiltakenValidering(IgangsettingstillatelseType form)
        {
            if (!form.gjelderHeleTiltaket)
            {
                if (string.IsNullOrEmpty(form.delAvTiltaket))
                {
                    _validationResult.AddError("Når det søkes om deler av tiltaket må beskrivelse av del fylles ut", "4401.2.10", "/Igangsettingstillatelse/delAvTiltaket");
                }
            }
        }
        internal void ArbeidsplasserValidering(IgangsettingstillatelseType form, List<string> formSetElements)
        {
            if (form.beroererArbeidsplasser)
            {
                if (formSetElements == null)
                {
                    _validationResult.AddWarning("Når en velger at yrkesbygg berører arbeidsplasser må samtykke fra Arbeidstilsynet være på plass før igangsettingstillatelse kan gis. Vedlegg SamtykkeArbeidstilsynet  må legges ved denne søknaden.", "4401.2.11", "/Igangsettingstillatelse/beroererArbeidsplasser");
                }
                else if (!formSetElements.Contains("Søknad om Arbeidstilsynets samtykke") && !formSetElements.Contains("SamtykkeArbeidstilsynet") && !formSetElements.Contains("Søknad om arbeidstilsynets samtykke"))
                {
                    _validationResult.AddWarning("Når en velger at yrkesbygg berører arbeidsplasser må samtykke fra Arbeidstilsynet være på plass før igangsettingstillatelse kan gis. Vedlegg SamtykkeArbeidstilsynet  må legges ved denne søknaden.", "4401.2.11", "/Igangsettingstillatelse/beroererArbeidsplasser");
                }
            }
        }

        internal void KommunensSaksnummerValidering(IgangsettingstillatelseType form)
        {
            if (form.kommunensSaksnummer == null || string.IsNullOrEmpty(form.kommunensSaksnummer.saksaar) || string.IsNullOrEmpty(form.kommunensSaksnummer.sakssekvensnummer))
            {
                _validationResult.AddError("Kommunens saksnummer må være fylt ut.", "4401.2.12", "/Igangsettingstillatelse/kommunensSaksnummer");
            }
        }

        internal void EiendomByggestedValidation(IgangsettingstillatelseType form)
        {
            if (MinstEnEiendom(form))
            {
                foreach (var eiendom in form.eiendomByggested)
                {
                    if (eiendom.eiendomsidentifikasjon != null)
                    {
                        if (String.IsNullOrEmpty(eiendom.eiendomsidentifikasjon.kommunenummer))
                        {
                            _validationResult.AddError("kommunenummer må fylles ut for eiendom/byggested", "4401.1.1", "/Igangsettingstillatelse/eiendomByggested/eiendom[]/eiendomsidentifikasjon/kommunenummer");
                        }
                        else
                            GyldigKommuneNrEiendom(eiendom.eiendomsidentifikasjon.kommunenummer);
                    }

                    if (String.IsNullOrEmpty(eiendom.kommunenavn))
                    {
                        _validationResult.AddWarning("Kommunenavn bør fylles ut for eiendom/byggested", "4401.1.1", "/Igangsettingstillatelse/eiendomByggested/eiendom/kommunenavn");
                    }

                    if (!String.IsNullOrEmpty(eiendom.bolignummer))
                    {
                        if (GeneralValidations.Validate_bruksenhetsnummer(eiendom.bolignummer).Status == ValidationStatus.Fail)
                        {
                            _validationResult.AddError("Hvis bruksenhetsnummer(bolignummer) er fylt ut må det følge riktig format (^[HKLU]\\d{4}$) (feks H0101) se https://www.kartverket.no/eiendom/adressering/bruksenhetsnummer/", "4401.2.13", "/Igangsettingstillatelse/eiendomByggested/eiendom[]/bygningsnummer");
                        }
                    }
                }

            }
        }

        internal bool MinstEnEiendom(IgangsettingstillatelseType form)
        {
            if (form.eiendomByggested == null || !form.eiendomByggested.Any())
                _validationResult.AddError(ValidationError.MinstEnEiendom, "4401.1.1",
                    "/Igangsettingstillatelse/eiendomByggested/eiendom");

            return form.eiendomByggested != null && (form.eiendomByggested != null || form.eiendomByggested.Any());
        }

        internal void GyldigKommuneNrEiendom(string kommunenr)
        {
            GeneralValidationResult generalValidationResult = MunicipalityValidator.ValidateKommunenummer(kommunenr);
            if (generalValidationResult.Status != ValidationStatus.Ok)
            {
                _validationResult.AddError($"{generalValidationResult.Message}", "4401.1.1", "/Igangsettingstillatelse/eiendomByggested/eiendom[]/eiendomsidentifikasjon/kommunenummer");
            }
        }

        internal void AnsvarligSoekerValidation(IgangsettingstillatelseType form)
        {
            if (form.ansvarligSoeker != null)
            {
                if (form.ansvarligSoeker.partstype != null)
                {
                    if (!_codeListService.IsCodelistValid("Partstype", form.ansvarligSoeker.partstype.kodeverdi))
                    {
                        _validationResult.AddError(
                            $"Kodeliste Partstype verdi {form.ansvarligSoeker.partstype.kodeverdi} er ugyldig",
                            "4401.1.4", "/Igangsettingstillatelse/ansvarligSoeker/partstype/kodeverdi");
                    }
                    else
                    {
                        if (form.ansvarligSoeker.partstype.kodeverdi == "Privatperson")
                        {
                            GeneralValidationResult generalValidationResult =
                                GeneralValidations.Validate_fodselsnummer(form.ansvarligSoeker.foedselsnummer);

                            if (generalValidationResult.Status != ValidationStatus.Ok)
                            {
                                _validationResult.AddError($"{generalValidationResult.Message}", "4401.1.4",
                                    "/Igangsettingstillatelse/ansvarligSoeker/foedselsnummer");
                            }
                        }
                        else
                        {
                            GeneralValidationResult generalValidationResult =
                                GeneralValidations.Validate_orgnummer(form.ansvarligSoeker.organisasjonsnummer);

                            if (generalValidationResult.Status != ValidationStatus.Ok)
                            {
                                _validationResult.AddError($"{generalValidationResult.Message}", "4401.1.4",
                                    "/Igangsettingstillatelse/ansvarligSoeker/organisasjonsnummer");
                            }
                            if (string.IsNullOrEmpty(form.ansvarligSoeker.kontaktperson))
                            {
                                _validationResult.AddWarning(ValidationError.AnsvarligSoekerKontaktperson, "4401.2.14",
                                    "/Igangsettingstillatelse/ansvarligSoeker/kontaktperson");
                            }
                        }
                    }
                }
                else
                {
                    _validationResult.AddError("Ansvarlig søkers partstype må angis.", "4401.1.4", "/Igangsettingstillatelse/ansvarligSoeker/partstype");
                }

                IsFieldNullOrEmpty(form.ansvarligSoeker.navn, "Søkers navn", "4401.1.4", "/Igangsettingstillatelse/ansvarligSoeker/navn");

                if (form.ansvarligSoeker.adresse != null)
                {

                    if (String.IsNullOrEmpty(form.ansvarligSoeker.adresse.poststed))
                    {
                        _validationResult.AddError($"Må sendes til søker med gyldig poststed ", "4401.1.4", "/Igangsettingstillatelse/ansvarligSoeker/adresse/poststed");
                    }
                    if (String.IsNullOrEmpty(form.ansvarligSoeker.adresse.postnr))
                    {
                        _validationResult.AddError($"Må sendes til søker med gyldig postnr ", "4401.1.4", "/Igangsettingstillatelse/ansvarligSoeker/adresse/postnr");
                    }

                    var generalValidationResult = GeneralValidations.Validate_postnummer(form.ansvarligSoeker.adresse.postnr, form.ansvarligSoeker.adresse.landkode);

                    if (generalValidationResult?.Status == ValidationStatus.Fail)
                        _validationResult.AddError($"{generalValidationResult.Message} for søker", "4401.1.4", "/Igangsettingstillatelse/ansvarligSoeker/adresse/postnr");
                    if (generalValidationResult?.Status == ValidationStatus.Warning)
                        _validationResult.AddWarning($"{generalValidationResult.Message} for søker", "4401.1.4", "/Igangsettingstillatelse/ansvarligSoeker/adresse/postnr");
                }
                else
                {
                    _validationResult.AddWarning("Adresse bør fylles ut for søker", "4401.1.4", "/Igangsettingstillatelse/ansvarligSoeker/adresse");
                }

                

                if (string.IsNullOrEmpty(form.ansvarligSoeker.mobilnummer))
                {
                    if (string.IsNullOrEmpty(form.ansvarligSoeker.telefonnummer))
                    {
                        _validationResult.AddWarning(ValidationError.AnsvarligSoekerTelefonEllerMobil, "4401.2.15", "/Rammetillatelse/ansvarligSoeker/");
                    }
                }
            }
            else
            {
                _validationResult.AddError("Ansvarlig søkers må angis.", "4401.1.1", "/Igangsettingstillatelse/ansvarligSoeker");
            }



        }
       

        private void IsFieldNullOrEmpty(string fieldName, string message, string reference, string xpathField)
        {
            if (String.IsNullOrEmpty(fieldName))
            {
                _validationResult.AddError($"{message} må være utfylt", $"{reference}",
                    $"{xpathField}");
            }
        }
    }
}