using KommIT.FIKS.AdapterAltinnSvarUt.Logger;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Validations;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;
using no.kxml.skjema.dibk.rammesoknadV3;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.SvarUtForm
{
    public class RammesoknadFormV3Validator : FormValidator
    {
        internal ValidationResult GetResult() => _validationResult;

        public RammesoknadFormV3Validator() : base("RS")
        {
            var skjema = "Rammetillatelse";
            var form = new RammetillatelseType();

            _validationResult.AddRule(skjema, "4397.0.1", null, "En teknisk feil gjør at vi ikke kan bekrefte informasjonen du har oppgitt. Vi anbefaler at du vurderer om følgende punkt gjelder for tiltakstypen(e) du har valgt: '{0}'. Du finner punktene i de nasjonale sjekklistene for byggesak på https://sjekkliste-bygg.ft.dibk.no/checklist/RS", null, "ERROR", null);
            _validationResult.AddRule(skjema, "4397.1.1", null, "Innhold må være i henhold til informasjonsmodell/xsd for skjema.", null, "ERROR", null);
            _validationResult.AddRule(skjema, "4397.1.3", null, "Vedlegg må være i henhold til metadata for skjema. Vedleggstype eller underskjema '{0}' er ikke gyldig.", Helpers.GetFullClassPath(() => form), "ERROR", null);

            //beskrivelseAvTiltak
            //**beskrivelseAvTiltak.type
            _validationResult.AddRule(skjema, "4397.2.3", "1.10", "Minst én beskrivelse av tiltak må være registrert.", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak), "ERROR", null);
            _validationResult.AddRule(skjema, "4397.2.3.1", "1.10", "Du må velge en tiltakstype.", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.type), "ERROR", null);
            _validationResult.AddRule(skjema, "4397.2.3.2", "1.10", "Du må oppgi en tiltakstype, jfr. nasjonal sjekkliste punkt 1.10. Du kan sjekke gyldige tiltakstyper på https://register.geonorge.no/byggesoknad/tiltaktype", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.type[0]), "ERROR", null);
            _validationResult.AddRule(skjema, "4397.2.3.2.1", "1.10", "'{0}' er en ugyldig kodeverdi for tiltakstype. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/byggesoknad/tiltaktype", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.type[0].kodeverdi), "ERROR", null);
            _validationResult.AddRule(skjema, "4397.2.3.3", "1.10", "Når tiltakstype er valgt, må kodebeskrivelse fylles ut. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/byggesoknad/tiltaktype", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.type[0].kodebeskrivelse), "ERROR", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.type[0].kodeverdi));
            _validationResult.AddRule(skjema, "4397.2.3.4", "1.10", "Kodebeskrivelsen '{0}' stemmer ikke med den valgte kodeverdien for tiltakstype. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/byggesoknad/tiltaktype", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.type[0].kodebeskrivelse), "WARNING", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.type[0].kodeverdi));

            //**beskrivelseAvTiltak.bruk
            _validationResult.AddRule(skjema, "4397.2.3.5", null, "'{0}' er en ugyldig kodeverdi for anleggstype. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/byggesoknad/anleggstype", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.bruk.anleggstype.kodeverdi), "ERROR", null);
            _validationResult.AddRule(skjema, "4397.3.3.5.2", null, "Når anleggstype er valgt, må kodebeskrivelse fylles ut. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/byggesoknad/anleggstype", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.bruk.bygningstype.kodebeskrivelse), "ERROR", null);
            _validationResult.AddRule(skjema, "4397.3.3.5.3", null, "Kodebeskrivelsen '{0}' stemmer ikke med den valgte kodeverdien for anleggstype. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/byggesoknad/anleggstype", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.bruk.bygningstype.kodebeskrivelse), "WARNING", null);
            _validationResult.AddRule(skjema, "4397.2.3.6", null, "'{0}' er en ugyldig kodeverdi for næringsgruppe. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/byggesoknad/naeringsgruppe", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.bruk.naeringsgruppe.kodeverdi), "ERROR", null);
            _validationResult.AddRule(skjema, "4397.2.3.7", null, "'{0}' er en ugyldig kodeverdi for bygningstype. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/byggesoknad/bygningstype", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.bruk.bygningstype.kodeverdi), "ERROR", null);
            _validationResult.AddRule(skjema, "4397.2.3.8", null, "Når bygningstype er valgt, må kodebeskrivelse fylles ut. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/byggesoknad/bygningstype", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.bruk.bygningstype.kodebeskrivelse), "ERROR", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.bruk.bygningstype.kodeverdi));
            _validationResult.AddRule(skjema, "4397.2.3.9", null, "Kodebeskrivelsen '{0}' stemmer ikke med den valgte kodeverdien for bygningstype. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/byggesoknad/bygningstype", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.bruk.bygningstype.kodebeskrivelse), "WARNING", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.bruk.bygningstype.kodeverdi));
            _validationResult.AddRule(skjema, "4397.2.3.10", null, "'{0}' er en ugyldig kodeverdi for tiltaksformål. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/byggesoknad/tiltaksformal", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.bruk.tiltaksformaal[0].kodeverdi), "ERROR", null);
            _validationResult.AddRule(skjema, "4397.2.3.10.1", null, "Når tiltaksformål er valgt, må kodebeskrivelse fylles ut. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/byggesoknad/tiltaksformal", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.bruk.tiltaksformaal[0].kodebeskrivelse), "ERROR", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.bruk.tiltaksformaal[0].kodeverdi));
            _validationResult.AddRule(skjema, "4397.2.3.10.2", null, "Kodebeskrivelsen '{0}' stemmer ikke med den valgte kodeverdien for tiltaksformål. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/byggesoknad/tiltaksformal", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.bruk.tiltaksformaal[0].kodebeskrivelse), "WARNING", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.bruk.tiltaksformaal[0].kodeverdi));

            _validationResult.AddRule(skjema, "4397.2.3.11", null, "Når tiltaksformål er registrert som ‘Annet’, må beskrivelse av planlagt formål fylles ut.", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.bruk.beskrivPlanlagtFormaal), "ERROR", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.bruk.tiltaksformaal[0].kodeverdi));

            //Eiendommens--> eiendomsidentifikasjon
            _validationResult.AddRule(skjema, "4397.2.4", null, "Du må oppgi hvilken eiendom/hvilke eiendommer byggesøknaden gjelder.", Helpers.GetFullClassPath(() => form.eiendomByggested), "ERROR", null);
            _validationResult.AddRule(skjema, "4397.3.4.1", "1.2", "Kommunenummer må fylles ut for eiendom/byggested.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].eiendomsidentifikasjon.kommunenummer), "ERROR", null);
            _validationResult.AddRule(skjema, "4397.3.4.2", "1.2", "Kommunenummeret '{0}' for eiendom/byggested finnes ikke i kodelisten. Du kan sjekke riktig kommunenummer på https://register.geonorge.no/sosi-kodelister/inndelinger/inndelingsbase/kommunenummer", Helpers.GetFullClassPath(() => form.eiendomByggested[0].eiendomsidentifikasjon.kommunenummer), "ERROR", null);
            _validationResult.AddRule(skjema, "4397.3.4.3", "1.2", "Kommunenummeret '{0}' for eiendom/byggested har ugyldig status ({1}). Du kan sjekke status på https://register.geonorge.no/sosi-kodelister/inndelinger/inndelingsbase/kommunenummer", Helpers.GetFullClassPath(() => form.eiendomByggested[0].eiendomsidentifikasjon.kommunenummer), "ERROR", null); //
            _validationResult.AddRule(skjema, "4397.3.4.4", "1.3", "Gårdsnummer må fylles ut for eiendom/byggested.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].eiendomsidentifikasjon.gaardsnummer), "ERROR", null);
            _validationResult.AddRule(skjema, "4397.3.4.4.1", "1.4", "Bruksnummer må fylles ut for eiendom/byggested.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].eiendomsidentifikasjon.bruksnummer), "ERROR", null);
            _validationResult.AddRule(skjema, "4397.3.4.5", "1.3", "Gårdsnummer '{0}' for eiendom/byggested må være '0' eller større.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].eiendomsidentifikasjon.gaardsnummer), "ERROR", null);
            _validationResult.AddRule(skjema, "4397.3.4.6", "1.4", "Bruksnummer '{0}' for eiendom/byggested må være '0' eller større.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].eiendomsidentifikasjon.bruksnummer), "ERROR", null);
            _validationResult.AddRule(skjema, "4397.3.4.7", "1.6", "Du har oppgitt følgende bygningsnummer for eiendom/byggested: ‘{0}'. Bygningsnummeret må være et tall.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].bygningsnummer), "ERROR", null);
            _validationResult.AddRule(skjema, "4397.3.4.8", "1.6", "Bygningsnummer for eiendom/byggested må være større enn '0'.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].bygningsnummer), "ERROR", null);

            //Eiendommens --> Adress
            _validationResult.AddRule(skjema, "4397.3.4.9", "1.8", "Postadresse for eiendom/byggested bør fylles ut.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].adresse), "WARNING", null);
            _validationResult.AddRule(skjema, "4397.3.4.9.1", "1.8", "Adresselinje 1 for eiendom/byggested bør fylles ut.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].adresse.adresselinje1), "WARNING", null);
            _validationResult.AddRule(skjema, "4397.3.4.9.2", "1.8", "Du bør oppgi gatenavn, husnummer og eventuell bokstav for eiendom/byggested slik at adressen kan valideres mot matrikkelen. Du kan sjekke riktig adresse på https://seeiendom.no", Helpers.GetFullClassPath(() => form.eiendomByggested[0].adresse.gatenavn), "WARNING", null); //
            _validationResult.AddRule(skjema, "4397.3.4.10", null, "Navnet på kommunen bør fylles ut for eiendom/byggested.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].kommunenavn), "WARNING", null);
            _validationResult.AddRule(skjema, "4397.2.4.11", null, "Når bruksenhetsnummer/bolignummer er fylt ut, må det følge riktig format (for eksempel H0101). Se https://www.kartverket.no/eiendom/adressering/bruksenhetsnummer/.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].bolignummer), "ERROR", null);

            //if (_matrikkelFlag)
            //{
            _validationResult.AddRule(skjema, "4397.3.4.12", "1.72", "Når eiendomsidentifikasjon [{0}-{1}/{2}/{3}/{4}] er oppgitt for eiendom/byggested, bør den være gyldig i matrikkelen. Du kan sjekke riktig informasjon på https://seeiendom.no", Helpers.GetFullClassPath(() => form.eiendomByggested[0].eiendomsidentifikasjon), "WARNING", null);
            _validationResult.AddRule(skjema, "4397.3.4.13", "1.72", "Eiendomsidentifikasjon [{0}-{1}/{2}/{3}/{4}] for eiendom/byggested ble ikke validert mot matrikkelen. Identifikasjonen kan være riktig, men en teknisk feil gjør at vi ikke kan bekrefte det.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].eiendomsidentifikasjon), "WARNING", null);
            _validationResult.AddRule(skjema, "4397.3.4.14", "1.6", "Når bygningsnummer [{0}] er oppgitt for eiendom/byggested, bør det være gyldig i matrikkelen på aktuelt matrikkelnummer. Du kan sjekke riktig bygningsnummer på https://seeiendom.no", Helpers.GetFullClassPath(() => form.eiendomByggested[0].bygningsnummer), "WARNING", null);
            _validationResult.AddRule(skjema, "4397.3.4.15", "1.8", "Gateadressen for eiendom/byggested ble ikke validert mot matrikkelen. Gateadressen kan være riktig, men en teknisk feil gjør at vi ikke kan bekrefte det. Du kan sjekke adresse på https://seeiendom.no", Helpers.GetFullClassPath(() => form.eiendomByggested[0].adresse.gatenavn), "WARNING", null);
            _validationResult.AddRule(skjema, "4397.3.4.15.1", "1.8", "Du har oppgitt feil gateadresse for eiendom/byggested. Du kan sjekke riktig adresse på https://seeiendom.no", Helpers.GetFullClassPath(() => form.eiendomByggested[0].adresse.gatenavn), "WARNING", null);
            //}

            //Tiltakshaver
            _validationResult.AddRule(skjema, "4397.3.5", "1.91", "Informasjon om tiltakshaver må fylles ut, jfr. nasjonal sjekkliste punkt 1.91.", Helpers.GetFullClassPath(() => form.tiltakshaver.partstype), "ERROR", null);
            _validationResult.AddRule(skjema, "4397.1.5.1", null, "Kodeverdien for 'partstype' for tiltakshaver må fylles ut.", Helpers.GetFullClassPath(() => form.tiltakshaver.partstype.kodeverdi), "ERROR", null);
            _validationResult.AddRule(skjema, "4397.1.5.2", null, "'{0}' er en ugyldig kodeverdi for partstype for tiltakshaver. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/byggesoknad/partstype", Helpers.GetFullClassPath(() => form.tiltakshaver.partstype.kodeverdi), "ERROR", null);

            //tiltakshaver.organisasjonsnummer
            _validationResult.AddRule(skjema, "4397.3.5.4", null, "Organisasjonsnummeret til tiltakshaver bør fylles ut når tiltakshaver er en organisasjon.", Helpers.GetFullClassPath(() => form.tiltakshaver.organisasjonsnummer), "WARNING", null);
            _validationResult.AddRule(skjema, "4397.3.5.4.1", null, "Organisasjonsnummeret ('{0}') til tiltakshaver har ikke gyldig kontrollsiffer.", Helpers.GetFullClassPath(() => form.tiltakshaver.organisasjonsnummer), "ERROR", null);
            _validationResult.AddRule(skjema, "4397.3.5.4.2", null, "Organisasjonsnummeret ('{0}') til tiltakshaver er ikke gyldig.", Helpers.GetFullClassPath(() => form.tiltakshaver.organisasjonsnummer), "ERROR", null);
            //tiltakshaver.kontaktperson
            _validationResult.AddRule(skjema, "4397.2.5.4.3", null, "Tiltakshavers kontaktperson bør fylles ut når tiltakshaver er en organisasjon.", Helpers.GetFullClassPath(() => form.tiltakshaver.kontaktperson.navn), "WARNING", null);
            _validationResult.AddRule(skjema, "4397.3.5.4.3.1", null, "Navnet til tiltakshavers kontaktperson bør fylles ut når tiltakshaver er en organisasjon.", Helpers.GetFullClassPath(() => form.tiltakshaver.kontaktperson.navn), "WARNING", null);
            _validationResult.AddRule(skjema, "4397.2.5.4.3.2", null, "Telefonnummer eller mobilnummer til tiltakshavers kontaktperson bør fylles ut når tiltakshaver er en organisasjon.", Helpers.GetFullClassPath(() => form.tiltakshaver.kontaktperson), "WARNING", null);
            _validationResult.AddRule(skjema, "4397.3.5.4.3.3", null, "Telefonnummeret til tiltakshavers kontaktperson må kun inneholde tall og '+'.", Helpers.GetFullClassPath(() => form.tiltakshaver.kontaktperson.telefonnummer), "ERROR", null);
            _validationResult.AddRule(skjema, "4397.3.5.4.3.4", null, "Mobilnummeret til tiltakshavers kontaktperson må kun inneholde tall og '+'.", Helpers.GetFullClassPath(() => form.tiltakshaver.kontaktperson.mobilnummer), "ERROR", null);
            _validationResult.AddRule(skjema, "4397.3.5.4.3.5", null, "E-postadressen til tiltakshavers kontaktperson bør fylles ut når tiltakshaver er en organisasjon.", Helpers.GetFullClassPath(() => form.tiltakshaver.kontaktperson.epost), "WARNING", null);
            //Tiltakshaver.adress
            _validationResult.AddRule(skjema, "4397.3.5.5", null, "Adressen til tiltakshaver må fylles ut.", Helpers.GetFullClassPath(() => form.tiltakshaver.adresse), "ERROR", null);
            _validationResult.AddRule(skjema, "4397.3.5.5.1", null, "Adresselinje 1 må fylles ut for tiltakshaver.", Helpers.GetFullClassPath(() => form.tiltakshaver.adresse.adresselinje1), "ERROR", null);
            _validationResult.AddRule(skjema, "4397.3.5.5.2", null, "Landkoden for tiltakshavers adresse er ugyldig.", Helpers.GetFullClassPath(() => form.tiltakshaver.adresse.landkode), "WARNING", null);
            _validationResult.AddRule(skjema, "4397.3.5.5.3", null, "Postnummeret til tiltakshaver må angis.", Helpers.GetFullClassPath(() => form.tiltakshaver.adresse.postnr), "ERROR", null);
            _validationResult.AddRule(skjema, "4397.3.5.5.3.1", null, "Postnummeret '{0}' til tiltakshaver har ikke gyldig kontrollsiffer.", Helpers.GetFullClassPath(() => form.tiltakshaver.adresse.postnr), "ERROR", null);
            _validationResult.AddRule(skjema, "4397.3.5.5.3.2", null, "Postnummeret '{0}' til tiltakshaver er ugyldig. Du kan sjekke riktig postnummer på http://adressesok.bring.no/", Helpers.GetFullClassPath(() => form.tiltakshaver.adresse.postnr), "ERROR", null);
            _validationResult.AddRule(skjema, "4397.3.5.5.3.3", null, "Postnummeret '{0}' til tiltakshaver stemmer ikke overens med poststedet '{1}'. Postnummeret er fra '{2}'. Du kan sjekke riktig postnummer/poststed på http://adressesok.bring.no/", Helpers.GetFullClassPath(() => form.tiltakshaver.adresse.postnr), "ERROR", null);
            _validationResult.AddRule(skjema, "4397.3.5.5.3.4", null, "Postnummeret '{0}' til tiltakshaver ble ikke validert. Postnummeret kan være riktig, men en teknisk feil gjør at vi ikke kan bekrefte det.", Helpers.GetFullClassPath(() => form.tiltakshaver.adresse.postnr), "WARNING", null);
            //Tiltakshaver...
            _validationResult.AddRule(skjema, "4397.3.5.6", "1.91", "Navnet til tiltakshaver må fylles ut, jfr. nasjonal sjekkliste punkt 1.91.", Helpers.GetFullClassPath(() => form.tiltakshaver.navn), "ERROR", null);
            _validationResult.AddRule(skjema, "4397.3.5.7", null, "Telefonnummeret eller mobilnummeret til tiltakshaver må fylles ut.", Helpers.GetFullClassPath(() => form.tiltakshaver.mobilnummer), "ERROR", Helpers.GetFullClassPath(() => form.tiltakshaver.telefonnummer));
            _validationResult.AddRule(skjema, "4397.3.5.8", null, "Telefonnummeret til tiltakshaver må kun inneholde tall og '+'.", Helpers.GetFullClassPath(() => form.tiltakshaver.telefonnummer), "ERROR", null);
            _validationResult.AddRule(skjema, "4397.3.5.9", null, "Mobilnummeret til tiltakshaver må kun inneholde tall og '+'.", Helpers.GetFullClassPath(() => form.tiltakshaver.mobilnummer), "ERROR", null);
            _validationResult.AddRule(skjema, "4397.3.5.10", null, "E-postadressen til tiltakshaver må fylles ut.", Helpers.GetFullClassPath(() => form.tiltakshaver.epost), "ERROR", null);

            //rammebetingelser.adkomst
            _validationResult.AddRule(skjema, "4397.2.6.1", "7.1", "Informasjon om adkomst kreves for valgte tiltakstyper, jfr. nasjonal sjekkliste punkt 7.1.", Helpers.GetFullClassPath(() => form.rammebetingelser.adkomst), "ERROR", null);
            _validationResult.AddRule(skjema, "4397.2.6.1.1", "7.1", "Du må svare på om tiltaket medfører ny eller endret adkomst, jfr. nasjonal sjekkliste punkt 7.1.", Helpers.GetFullClassPath(() => form.rammebetingelser.adkomst.nyeEndretAdkomst), "ERROR", null);
            _validationResult.AddRule(skjema, "4397.2.6.2.1", null, "Det er valgt at tiltaket gir ny eller endret adkomst. Da bør vedlegget ‘Avkjørselsplan’ legges ved.", Helpers.GetFullClassPath(() => form.rammebetingelser.adkomst.nyeEndretAdkomst), "WARNING", "/Vedlegg");
            _validationResult.AddRule(skjema, "4397.2.6.2.2", null, "Når tiltaket gir ny eller endret adkomst, må du velge vegtype. Du kan sjekke riktig vegtype på https://register.geonorge.no/byggesoknad/vegtype", Helpers.GetFullClassPath(() => form.rammebetingelser.adkomst.vegtype), "ERROR", null);
            _validationResult.AddRule(skjema, "4397.1.6.2.3", null, "'{0}' er en ugyldig kodeverdi for vegtype. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/byggesoknad/vegtype", Helpers.GetFullClassPath(() => form.rammebetingelser.adkomst.vegtype[0].kodeverdi), "ERROR", null);
            _validationResult.AddRule(skjema, "4397.2.6.2.4", "7.2", "Når vegtypen er 'riksvei/fylkesvei', må du svare på om avkjøringstillatelse er gitt.", Helpers.GetFullClassPath(() => form.rammebetingelser.adkomst.erTillatelseGittRiksFylkesveg), "ERROR", Helpers.GetFullClassPath(() => form.rammebetingelser.adkomst.vegtype[0].kodeverdi));
            _validationResult.AddRule(skjema, "4397.2.6.2.5", "7.3", "Når vegtypen er 'kommunal vei', må du svare på om avkjøringstillatelse er gitt.", Helpers.GetFullClassPath(() => form.rammebetingelser.adkomst.erTillatelseGittKommunalVeg), "ERROR", Helpers.GetFullClassPath(() => form.rammebetingelser.adkomst.vegtype[0].kodeverdi));
            _validationResult.AddRule(skjema, "4397.2.6.2.6", "7.4", "Når vegtypen er 'privat vei', må du svare på om vegrett er sikret ved tinglyst erklæring.", Helpers.GetFullClassPath(() => form.rammebetingelser.adkomst.erTillatelseGittPrivatVeg), "ERROR", Helpers.GetFullClassPath(() => form.rammebetingelser.adkomst.vegtype[0].kodeverdi));

            //** rammebetingelser.generelleVilkaar
            _validationResult.AddRule(skjema, "4397.2.6.3.1", "5.13", "Du bør svare på om tiltaket berører byggverk oppført før 1850, jfr. nasjonal sjekkliste punkt 5.13.", Helpers.GetFullClassPath(() => form.rammebetingelser.generelleVilkaar.beroererTidligere1850), "WARNING", null);
            _validationResult.AddRule(skjema, "4397.2.6.3.1.1", "5.13", "Tiltaket berører bygg fra før 1850. Da bør du legge ved vedlegget ‘Uttalelse fra kulturminnemyndigheten’ (UttalelseKulturminnemyndighet), jfr. nasjonal sjekkliste punkt 5.13.", Helpers.GetFullClassPath(() => form.rammebetingelser.generelleVilkaar.beroererTidligere1850), "WARNING", null);

            _validationResult.AddRule(skjema, "4397.2.6.3.2.1", "5.1", "Du må svare på om tiltaket berører eksisterende eller fremtidige arbeidsplasser, jfr. nasjonal sjekkliste punkt 5.1.", Helpers.GetFullClassPath(() => form.rammebetingelser.generelleVilkaar.beroererArbeidsplasser), "ERROR", null);
            _validationResult.AddRule(skjema, "4397.2.6.3.2", "5.2", "Når en velger at yrkesbygg berører arbeidsplasser, må samtykke fra Arbeidstilsynet være på plass før igangsettingstillatelse kan gis. Det anbefales at vedlegg ‘SamtykkeArbeidstilsynet’ legges ved denne søknaden.", "/Vedlegg", "WARNING", Helpers.GetFullClassPath(() => form.rammebetingelser.generelleVilkaar.beroererArbeidsplasser));
            _validationResult.AddRule(skjema, "4397.2.6.3.3", "1.1", "Du må svare på om søknaden og alle relevante dokumenter er skrevet eller oversatt til norsk, svensk eller dansk.", Helpers.GetFullClassPath(() => form.rammebetingelser.generelleVilkaar.norskSvenskDansk), "ERROR", null);
            _validationResult.AddRule(skjema, "4397.2.6.3.4", "4.3", "Du må svare på om det skal utarbeides avfallsplan, jfr. nasjonal sjekkliste punkt 4.3.", Helpers.GetFullClassPath(() => form.rammebetingelser.generelleVilkaar.utarbeideAvfallsplan), "ERROR", null);

            //rammebetingelser.plan.gjeldendePlan
            _validationResult.AddRule(skjema, "4397.2.6.4", "3.1", "Informasjon om gjeldende plan kreves for valgte tiltakstyper, jfr. nasjonal sjekkliste punkt 3.1.", Helpers.GetFullClassPath(() => form.rammebetingelser.plan.gjeldendePlan), "ERROR", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.type));

            _validationResult.AddRule(skjema, "4397.2.6.4.1", "3.1", "Du må oppgi en plantype for gjeldende plan. Du kan sjekke gyldige plantyper på https://register.geonorge.no/byggesoknad/plantype", Helpers.GetFullClassPath(() => form.rammebetingelser.plan.gjeldendePlan.plantype), "ERROR", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.type));
            _validationResult.AddRule(skjema, "4397.2.6.4.1.1", "3.1", "'{0}' er en ugyldig kodeverdi for plantype. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/byggesoknad/plantype", Helpers.GetFullClassPath(() => form.rammebetingelser.plan.gjeldendePlan.plantype.kodeverdi), "ERROR", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.type));
            _validationResult.AddRule(skjema, "4397.2.6.4.2", "3.1", "Når plantype er valgt, må kodebeskrivelse fylles ut. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/byggesoknad/plantype", Helpers.GetFullClassPath(() => form.rammebetingelser.plan.gjeldendePlan.plantype.kodebeskrivelse), "ERROR", Helpers.GetFullClassPath(() => form.rammebetingelser.plan.gjeldendePlan.plantype.kodeverdi));
            _validationResult.AddRule(skjema, "4397.2.6.4.2.1", "3.1", "Kodebeskrivelsen '{0}' stemmer ikke med den valgte kodeverdien for plantype. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/byggesoknad/plantype", Helpers.GetFullClassPath(() => form.rammebetingelser.plan.gjeldendePlan.plantype.kodebeskrivelse), "WARNING", Helpers.GetFullClassPath(() => form.rammebetingelser.plan.gjeldendePlan.plantype.kodeverdi));
            //**rammebetingelser.plan.gjeldendePlan.beregningsregelGradAvUtnytting
            _validationResult.AddRule(skjema, "4397.2.6.4.3", "3.3", "Du må oppgi beregningsregel for grad av utnytting fra gjeldende plan for valgte tiltakstyper, jfr. nasjonal sjekkliste punkt 3.3. Du kan sjekke riktig beregningsregel på https://register.geonorge.no/byggesoknad/beregningsregelgradavutnytting", Helpers.GetFullClassPath(() => form.rammebetingelser.plan.gjeldendePlan.beregningsregelGradAvUtnytting), "ERROR", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.type));
            _validationResult.AddRule(skjema, "4397.2.6.4.4", "3.3", "'{0}' er en ugyldig kodeverdi for beregningsregel grad av utnytting. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/byggesoknad/beregningsregelgradavutnytting", Helpers.GetFullClassPath(() => form.rammebetingelser.plan.gjeldendePlan.beregningsregelGradAvUtnytting.kodeverdi), "ERROR", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.type));
            _validationResult.AddRule(skjema, "4397.2.6.4.5", "3.3", "Kodebeskrivelsen for beregningsregel grad av utnytting ('{0}') bør fylles ut. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/byggesoknad/beregningsregelgradavutnytting", Helpers.GetFullClassPath(() => form.rammebetingelser.plan.gjeldendePlan.beregningsregelGradAvUtnytting.kodeverdi), "WARNING", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.type));
            _validationResult.AddRule(skjema, "4397.2.6.4.6", "3.3", "Når beregningsregel for grad av utnytting er ‘Annet', må du legge ved vedlegg 'Underlag for beregning av utnytting’ (UnderlagUtnytting).", Helpers.GetFullClassPath(() => form.rammebetingelser.plan.gjeldendePlan.beregningsregelGradAvUtnytting.kodeverdi), "ERROR", "/Vedlegg");
            _validationResult.AddRule(skjema, "4397.2.6.4.7", "3.1", "Reguleringsformål bør fylles ut.", Helpers.GetFullClassPath(() => form.rammebetingelser.plan.gjeldendePlan.formaal), "WARNING", null);
            _validationResult.AddRule(skjema, "4397.2.6.4.8", "3.1", "Navnet på gjeldende plan bør fylles ut.", Helpers.GetFullClassPath(() => form.rammebetingelser.plan.gjeldendePlan.navn), "WARNING", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.type));

            //rammebetingelser.arealdisponering
            _validationResult.AddRule(skjema, "4397.3.6.5.11", "3.3", "Informasjon om arealdisponering kreves for valgte tiltakstyper, jfr. nasjonal sjekkliste punkt 3.3.", Helpers.GetFullClassPath(() => form.rammebetingelser.arealdisponering), "ERROR", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.type[0].kodeverdi));
            _validationResult.AddRule(skjema, "4397.2.6.5", "3.3", "Når beregningsregel for grad av utnytting er '{0}' kreves utfylte verdier for areal eksisterende bebyggelse, areal ny bebyggelse, areal bebyggelse som rives, parkeringsareal terreng og beregnet grad av utnytting.", Helpers.GetFullClassPath(() => form.rammebetingelser.arealdisponering), "ERROR", Helpers.GetFullClassPath(() => form.rammebetingelser.plan.gjeldendePlan.beregningsregelGradAvUtnytting.kodeverdi));
            _validationResult.AddRule(skjema, "4397.2.6.5.1", null, "Når beregningsregel for grad av utnytting er angitt i prosent, må tomtearealet beregnes.", Helpers.GetFullClassPath(() => form.rammebetingelser.arealdisponering.tomtearealBeregnet), "ERROR", Helpers.GetFullClassPath(() => form.rammebetingelser.plan.gjeldendePlan.beregningsregelGradAvUtnytting.kodeverdi));
            _validationResult.AddRule(skjema, "4397.2.6.5.2", null, "Utregning av utnyttelsesgrad kan ikke ha negative verdier for ‘arealBebyggelseEksisterende’.", Helpers.GetFullClassPath(() => form.rammebetingelser.arealdisponering.arealBebyggelseEksisterende), "ERROR", null);
            _validationResult.AddRule(skjema, "4397.2.6.5.3", null, "Utregning av utnyttelsesgrad kan ikke ha negative verdier for 'arealBebyggelseNytt'.", Helpers.GetFullClassPath(() => form.rammebetingelser.arealdisponering.arealBebyggelseNytt), "ERROR", null);
            _validationResult.AddRule(skjema, "4397.2.6.5.4", null, "Utregning av utnyttelsesgrad kan ikke ha negative verdier for ‘arealBebyggelseSomSkalRives’.", Helpers.GetFullClassPath(() => form.rammebetingelser.arealdisponering.arealBebyggelseSomSkalRives), "ERROR", null);
            _validationResult.AddRule(skjema, "4397.2.6.5.5", null, "Utregning av utnyttelsesgrad kan ikke ha negative verdier for ‘parkeringsarealTerreng’", Helpers.GetFullClassPath(() => form.rammebetingelser.arealdisponering.parkeringsarealTerreng), "ERROR", null);
            _validationResult.AddRule(skjema, "4397.2.6.5.6", null, "Utregning av utnyttelsesgrad kan ikke ha negative verdier for ‘beregnetGradAvUtnytting’.", Helpers.GetFullClassPath(() => form.rammebetingelser.arealdisponering.beregnetGradAvUtnytting), "ERROR", null);
            _validationResult.AddRule(skjema, "4397.2.6.5.7", null, "Utregning av utnyttelsesgrad kan ikke ha negative verdier for ‘tomtearealBeregnet’.", Helpers.GetFullClassPath(() => form.rammebetingelser.arealdisponering.tomtearealBeregnet), "ERROR", null);
            _validationResult.AddRule(skjema, "4397.2.6.5.8", null, "Utregning av utnyttelsesgrad kan ikke ha '0' for ‘tomtearealBeregnet’.", Helpers.GetFullClassPath(() => form.rammebetingelser.arealdisponering.tomtearealBeregnet), "WARNING", null);
            _validationResult.AddRule(skjema, "4397.2.6.5.9", null, "Utregningen av utnyttelsesgrad er ikke riktig for ‘{0}'. Utfylt grad av utnytting er ‘{1}' og beregnet grad av utnytting er ‘{2}'. Utregningen er: ‘arealBebyggelseEksisterende’ - 'arealBebyggelseSomSkalRives’ + 'arealBebyggelseNytt’ + 'parkeringsarealTerreng’.", Helpers.GetFullClassPath(() => form.rammebetingelser.arealdisponering), "ERROR", Helpers.GetFullClassPath(() => form.rammebetingelser.plan.gjeldendePlan.beregningsregelGradAvUtnytting.kodeverdi));
            _validationResult.AddRule(skjema, "4397.2.6.5.10", null, "Utregningen av utnyttelsesgrad er ikke riktig for '{0}'. Utfylt grad av utnytting er '{1}' og beregnet grad av utnytting er '{2}'. Utregning av prosent er: ('arealBebyggelseEksisterende' - ‘arealBebyggelseSomSkalRives’ + ‘arealBebyggelseNytt’ + ‘parkeringsarealTerreng’)/'tomtearealBeregnet'*100.", Helpers.GetFullClassPath(() => form.rammebetingelser.arealdisponering), "ERROR", Helpers.GetFullClassPath(() => form.rammebetingelser.plan.gjeldendePlan.beregningsregelGradAvUtnytting.kodeverdi));

            //rammebetingelser.vannforsyning
            _validationResult.AddRule(skjema, "4397.2.6.6", "9.1", "Informasjon om vannforsyning kreves for valgte tiltakstyper, jfr. nasjonal sjekkliste punkt 9.1.", Helpers.GetFullClassPath(() => form.rammebetingelser.vannforsyning), "ERROR", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.type));
            _validationResult.AddRule(skjema, "4397.2.6.6.1.1", "9.1", "Du må oppgi hva slags vannforsyning eiendommen har. Du kan sjekke riktig vannforsyning på https://register.geonorge.no/byggesoknad/vanntilknytning", Helpers.GetFullClassPath(() => form.rammebetingelser.vannforsyning.tilknytningstype), "ERROR", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.type));
            _validationResult.AddRule(skjema, "4397.2.6.6.1", "9.1", "'{0}' er en ugyldig kodeverdi for vannforsyning. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/byggesoknad/vanntilknytning", Helpers.GetFullClassPath(() => form.rammebetingelser.vannforsyning.tilknytningstype.kodeverdi), "ERROR", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.type));
            _validationResult.AddRule(skjema, "4397.2.6.6.1.2", "9.1", "Når vannforsyning er valgt, må kodebeskrivelse fylles ut. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/byggesoknad/vanntilknytning", Helpers.GetFullClassPath(() => form.rammebetingelser.vannforsyning.tilknytningstype.kodebeskrivelse), "ERROR", Helpers.GetFullClassPath(() => form.rammebetingelser.vannforsyning.tilknytningstype.kodeverdi));
            _validationResult.AddRule(skjema, "4397.2.6.6.1.3", "9.1", "Kodebeskrivelsen '{0}' stemmer ikke med den valgte kodeverdien for vannforsyning. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/byggesoknad/vanntilknytning", Helpers.GetFullClassPath(() => form.rammebetingelser.vannforsyning.tilknytningstype.kodebeskrivelse), "WARNING", Helpers.GetFullClassPath(() => form.rammebetingelser.vannforsyning.tilknytningstype.kodeverdi));
            _validationResult.AddRule(skjema, "4397.2.6.6.2", null, "Når 'annen privat vannforsyning' er angitt, må beskrivelse fylles ut.", Helpers.GetFullClassPath(() => form.rammebetingelser.vannforsyning.beskrivelse), "ERROR", Helpers.GetFullClassPath(() => form.rammebetingelser.vannforsyning.tilknytningstype.kodeverdi));
            //rammebetingelser.avloep
            _validationResult.AddRule(skjema, "4397.2.6.7", "8.1", "Informasjon om avløp kreves for valgte tiltakstyper, jfr. nasjonal sjekkliste punkt 8.1. Du kan sjekke kodeverdi på https://register.geonorge.no/byggesoknad/avlopstilknytning", Helpers.GetFullClassPath(() => form.rammebetingelser.avloep), "ERROR", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.type));
            _validationResult.AddRule(skjema, "4397.2.6.7.1.1", "8.1", "Du må oppgi om eiendommen er knyttet til privat eller offentlig avløpsløsning, jfr. nasjonal sjekkliste punkt 8.1.", Helpers.GetFullClassPath(() => form.rammebetingelser.avloep.tilknytningstype), "ERROR", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.type));
            _validationResult.AddRule(skjema, "4397.2.6.7.1", "8.1", "'{0}' er en ugyldig kodeverdi for avløpstilknytning. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/byggesoknad/avlopstilknytning", Helpers.GetFullClassPath(() => form.rammebetingelser.avloep.tilknytningstype.kodeverdi), "ERROR", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.type));
            _validationResult.AddRule(skjema, "4397.2.6.7.2", "8.1", "Når avløpstilknytning er valgt, må kodebeskrivelse fylles ut. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/byggesoknad/avlopstilknytning", Helpers.GetFullClassPath(() => form.rammebetingelser.avloep.tilknytningstype.kodebeskrivelse), "ERROR", Helpers.GetFullClassPath(() => form.rammebetingelser.avloep.tilknytningstype.kodeverdi));
            _validationResult.AddRule(skjema, "4397.2.6.7.2.1", "8.1", "Kodebeskrivelsen '{0}' stemmer ikke med den valgte kodeverdien for avløpstilknytning. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/byggesoknad/avlopstilknytning", Helpers.GetFullClassPath(() => form.rammebetingelser.avloep.tilknytningstype.kodebeskrivelse), "WARNING", Helpers.GetFullClassPath(() => form.rammebetingelser.avloep.tilknytningstype.kodeverdi));
            _validationResult.AddRule(skjema, "4397.2.6.7.3", null, "Når ‘privat avløpsanlegg’ er valgt, må du fylle ut om det skal installeres vannklosett.", Helpers.GetFullClassPath(() => form.rammebetingelser.avloep.installereVannklosett), "ERROR", Helpers.GetFullClassPath(() => form.rammebetingelser.avloep.tilknytningstype.kodeverdi));
            _validationResult.AddRule(skjema, "4397.2.6.7.4", "8.3", "Når ‘privat avløpsanlegg’ er valgt, må du fylle ut om det foreligger utslippstillatelse.", Helpers.GetFullClassPath(() => form.rammebetingelser.avloep.utslippstillatelse), "ERROR", Helpers.GetFullClassPath(() => form.rammebetingelser.avloep.tilknytningstype.kodeverdi));
            _validationResult.AddRule(skjema, "4397.3.6.7.5", "8.4", "Du må svare på om avløp skal ledes over annens grunn, jfr. nasjonal sjekkliste punkt 8.4.", Helpers.GetFullClassPath(() => form.rammebetingelser.avloep.krysserAvloepAnnensGrunn), "ERROR", Helpers.GetFullClassPath(() => form.rammebetingelser.avloep.tilknytningstype.kodeverdi));
            _validationResult.AddRule(skjema, "4397.2.6.7.5.1", "8.5", "Når avløp krysser annens grunn, må du svare på om det foreligger tinglyst erklæring, jfr. nasjonal sjekkliste punkt 8.5.", Helpers.GetFullClassPath(() => form.rammebetingelser.avloep.tinglystErklaering), "ERROR", Helpers.GetFullClassPath(() => form.rammebetingelser.avloep.krysserAvloepAnnensGrunn));
            _validationResult.AddRule(skjema, "4397.2.6.7.6", "8.7", "Du må svare på om takvann/overvann blir ført til terreng eller overvannsledning, jfr. nasjonal sjekkliste punkt 8.7.", Helpers.GetFullClassPath(() => form.rammebetingelser.avloep), "ERROR", "");
            _validationResult.AddRule(skjema, "4397.3.6.7.8", "9.5", "Du må svare på om vannforsyning skal ledes over annens grunn, jfr. nasjonal sjekkliste punkt 9.5.", Helpers.GetFullClassPath(() => form.rammebetingelser.vannforsyning.krysserVannforsyningAnnensGrunn), "ERROR", Helpers.GetFullClassPath(() => form.rammebetingelser.vannforsyning.tilknytningstype.kodeverdi));
            _validationResult.AddRule(skjema, "4397.2.6.7.8.1", "9.6", "Når vannforsyning krysser annens grunn, må du svare på om det foreligger tinglyst erklæring, jfr. nasjonal sjekkliste punkt 9.6.", Helpers.GetFullClassPath(() => form.rammebetingelser.vannforsyning.tinglystErklaering), "ERROR", Helpers.GetFullClassPath(() => form.rammebetingelser.vannforsyning.krysserVannforsyningAnnensGrunn));

            //rammebetingelser.kravTilByggegrunn
            _validationResult.AddRule(skjema, "4397.2.6.8.1", "11.1", "Du må svare på om byggverket skal plasseres i et område med fare for flom eller stormflo, jfr. nasjonal sjekkliste punkt 11.1.", Helpers.GetFullClassPath(() => form.rammebetingelser.kravTilByggegrunn.flomutsattOmraade), "ERROR", null);
            _validationResult.AddRule(skjema, "4397.2.6.8.1.1", "11.4", "Det er valgt at byggverket skal plasseres i et område med fare for flom eller stormflo. Da må sikkerhetsklassen fylles ut, jfr. nasjonal sjekkliste punkt 11.4.", Helpers.GetFullClassPath(() => form.rammebetingelser.kravTilByggegrunn.f1), "ERROR", Helpers.GetFullClassPath(() => form.rammebetingelser.kravTilByggegrunn.flomutsattOmraade));
            _validationResult.AddRule(skjema, "4397.2.6.8.1.2", "11.2", "Det er valgt at byggverket skal plasseres i et område med fare for flom eller stormflo. Da må vedlegget ‘Redegjørelse skred og flomfare’ legges ved, jfr. nasjonal sjekkliste punkt 11.2.", Helpers.GetFullClassPath(() => form.rammebetingelser.kravTilByggegrunn.flomutsattOmraade), "ERROR", "/Vedlegg");
            _validationResult.AddRule(skjema, "4397.2.6.8.2", "11.8", "Du må svare på om byggverket skal plasseres i et område med fare for skred, jfr. nasjonal sjekkliste punkt 11.8.", Helpers.GetFullClassPath(() => form.rammebetingelser.kravTilByggegrunn.skredutsattOmraade), "ERROR", null);
            _validationResult.AddRule(skjema, "4397.2.6.8.2.1", "11.11", "Det er valgt at byggverket skal plasseres i et område med fare for skred. Da må sikkerhetsklassen fylles ut, jfr. nasjonal sjekkliste punkt 11.11.", Helpers.GetFullClassPath(() => form.rammebetingelser.kravTilByggegrunn.s1), "ERROR", Helpers.GetFullClassPath(() => form.rammebetingelser.kravTilByggegrunn.skredutsattOmraade));
            _validationResult.AddRule(skjema, "4397.2.6.8.2.2", "11.9", "Det er valgt at byggverket skal plasseres i et område med fare for skred. Da må vedlegget ‘Redegjørelse skred og flomfare’ legges ved, jfr. nasjonal sjekkliste punkt 11.9.", Helpers.GetFullClassPath(() => form.rammebetingelser.kravTilByggegrunn), "ERROR", "/Vedlegg");
            _validationResult.AddRule(skjema, "4397.2.6.8.3", "10.1", "Du må svare på om byggverket skal plasseres i et område med fare for natur- og miljøforhold, jfr. nasjonal sjekkliste punkt 10.1.", Helpers.GetFullClassPath(() => form.rammebetingelser.kravTilByggegrunn.miljoeforhold), "ERROR", null);
            _validationResult.AddRule(skjema, "4397.2.6.8.3.1", "10.1", "Det er valgt at byggverket skal plasseres i et område med fare for natur- og miljøforhold. Da må vedlegget ‘Redegjørelse andre natur- og miljøforhold’ legges ved.", Helpers.GetFullClassPath(() => form.rammebetingelser.kravTilByggegrunn.miljoeforhold), "ERROR", "/Vedlegg");

            // rammebetingelser.plan.andrePlaner
            _validationResult.AddRule(skjema, "4397.2.6.9.2", null, "Når det foreligger andre planer må du oppgi en plantype. Du kan sjekke gyldige plantyper på https://register.geonorge.no/byggesoknad/plantype", Helpers.GetFullClassPath(() => form.rammebetingelser.plan.andrePlaner[0].plantype.kodeverdi), "ERROR", null);
            _validationResult.AddRule(skjema, "4397.2.6.9", null, "'{0}' er en ugyldig kodeverdi for plantype. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/byggesoknad/plantype", Helpers.GetFullClassPath(() => form.rammebetingelser.plan.andrePlaner[0].plantype.kodeverdi), "ERROR", null);
            _validationResult.AddRule(skjema, "4397.2.6.9.1", null, "Når det foreligger andre planer, må navnet på planen angis.", Helpers.GetFullClassPath(() => form.rammebetingelser.plan.andrePlaner[0].navn), "ERROR", null);
            //rammebetingelser.plassering
            _validationResult.AddRule(skjema, "4397.2.6.10", "14.18", "Du må svare på om vann- og avløpsledninger kan være i konflikt med tiltaket, jfr. nasjonal sjekkliste punkt 14.18.", Helpers.GetFullClassPath(() => form.rammebetingelser.plassering.konfliktVannOgAvloep), "ERROR", null);
            _validationResult.AddRule(skjema, "4397.2.6.10.1", "14.17", "Du må svare på om det er strømførende linje/kabel eller nettstasjon/transformator i, over eller i nærheten av tiltaket, jfr. nasjonal sjekkliste punkt 14.17.", Helpers.GetFullClassPath(() => form.rammebetingelser.plassering.konfliktHoeyspentkraftlinje), "ERROR", null);
            _validationResult.AddRule(skjema, "4397.2.6.10.2", "14.17", "Det er valgt at strømførende linje/kabel eller nettstasjon/transformator kan være i konflikt med tiltaket. Da bør vedlegget ‘Avklaring av plassering nær høyspentledning’ (AvklaringHoyspent) legges ved", Helpers.GetFullClassPath(() => form.rammebetingelser.plassering.konfliktHoeyspentkraftlinje), "WARNING", "/Vedlegg");
            _validationResult.AddRule(skjema, "4397.2.6.10.3", null, "Det er valgt at vann- og avløpsledninger kan være i konflikt med tiltaket. Da må vedlegget ‘Avklaring av plassering nær VA-ledninger’ (AvklaringVA) legges ved.", Helpers.GetFullClassPath(() => form.rammebetingelser.plassering.konfliktVannOgAvloep), "ERROR", "/Vedlegg");
            //Dispensasjons
            _validationResult.AddRule(skjema, "4397.2.7", null, "Det er valgt at det skal søkes om dispensasjon. Da må du velge en dispensasjonstype. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/byggesoknad/dispensasjonstype", Helpers.GetFullClassPath(() => form.dispensasjon[0].dispensasjonstype), "ERROR", Helpers.GetFullClassPath(() => form.dispensasjon));
            _validationResult.AddRule(skjema, "4397.2.7.1", null, "'{0}' er en ugyldig kodeverdi for dispensasjonstype. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/byggesoknad/dispensasjonstype", Helpers.GetFullClassPath(() => form.dispensasjon[0].dispensasjonstype.kodeverdi), "ERROR", null);
            _validationResult.AddRule(skjema, "4397.2.7.2", null, "Det er valgt at det skal søkes om dispensasjon. Da må du legge inn beskrivelse og begrunnelse for dispensasjonen i tekstfeltene, eller i vedlegget 'Dispensasjonssøknad'.", Helpers.GetFullClassPath(() => form.dispensasjon[0].beskrivelse), "ERROR", "/Vedlegg");
            _validationResult.AddRule(skjema, "4397.2.7.3", null, "Det er valgt at det skal søkes om dispensasjon. Da må du også skrive en begrunnelse for dispensasjonen.", Helpers.GetFullClassPath(() => form.dispensasjon[0].begrunnelse), "ERROR", null);
            //Metadata
            _validationResult.AddRule(skjema, "4397.2.8", null, "Systemet må fylle ut samme navn som er brukt i registrering for Altinn API i fraSluttbrukersystem.", Helpers.GetFullClassPath(() => form.metadata.fraSluttbrukersystem), "ERROR", null);
            //varslingValidering
            _validationResult.AddRule(skjema, "4397.2.9", "2.12", "Informasjon om nabovarsling mangler for tiltaket, jfr. nasjonal sjekkliste punkt 2.12.", Helpers.GetFullClassPath(() => form.varsling), "ERROR", null);
            _validationResult.AddRule(skjema, "4397.2.9.1", "2.3", "Når nabovarsling kreves for tiltaket, må underskjema ‘Opplysninger gitt i nabovarsel’ (Gjenpart nabovarsel) eller vedlegg ‘Nabovarsel’ legges ved.", Helpers.GetFullClassPath(() => form.varsling.fritattFraNabovarsling), "ERROR", "/Vedlegg");
            _validationResult.AddRule(skjema, "4397.2.9.2", "2.2", "Når nabovarsling kreves for tiltaket, må vedlegget ‘Kvittering for nabovarsel’ legges ved.", Helpers.GetFullClassPath(() => form.varsling.fritattFraNabovarsling), "ERROR", "/Vedlegg");
            _validationResult.AddRule(skjema, "4397.3.9.3", "2.6", "Når nabovarsling er påkrevd for tiltaket, må du svare på om det foreligger merknader.", Helpers.GetFullClassPath(() => form.varsling.foreliggerMerknader), "ERROR", Helpers.GetFullClassPath(() => form.varsling.fritattFraNabovarsling));
            _validationResult.AddRule(skjema, "4397.2.9.3.1", "2.6", "Du har valgt at det foreligger merknader fra nabovarsel. Da må antallet merknader fylles ut.", Helpers.GetFullClassPath(() => form.varsling.antallMerknader), "ERROR", Helpers.GetFullClassPath(() => form.varsling.foreliggerMerknader));
            _validationResult.AddRule(skjema, "4397.2.9.3.2", "2.7", "Du har valgt at det foreligger merknader fra nabovarsling. Da må du skrive din vurdering av merknadene i tekstboksen eller legge ved vedlegget 'Kommentar til nabomerknader'.", Helpers.GetFullClassPath(() => form.varsling.vurderingAvMerknader), "ERROR", Helpers.GetFullClassPath(() => form.varsling.foreliggerMerknader));
            _validationResult.AddRule(skjema, "4397.2.9.3.3", "2.7", "Du har valgt at det foreligger merknader fra nabovarsling. Da må vedlegget ‘Nabomerknader’ legges ved.", Helpers.GetFullClassPath(() => form.varsling.foreliggerMerknader), "ERROR", "/Vedlegg");
            _validationResult.AddRule(skjema, "4397.3.9.4", "2.12", "Du må svare på om tiltaket er fritatt fra nabovarsling.", Helpers.GetFullClassPath(() => form.varsling.fritattFraNabovarsling), "ERROR", null);

            //Faktura
            _validationResult.AddRule(skjema, "4397.3.10", null, "Fakturainformasjon bør fylles ut når tiltakshaver er en organisasjon.", Helpers.GetFullClassPath(() => form.fakturamottaker), "WARNING", Helpers.GetFullClassPath(() => form.tiltakshaver.partstype.kodeverdi));
            _validationResult.AddRule(skjema, "4397.3.10.4", null, "Du må velge om du ønsker elektronisk faktura (EHF) eller papirfaktura.", Helpers.GetFullClassPath(() => form.fakturamottaker.ehfFaktura), "ERROR", Helpers.GetFullClassPath(() => form.fakturamottaker));
            _validationResult.AddRule(skjema, "4397.3.10.1", null, "Organisasjonsnummeret til fakturamottaker må fylles ut når EHF-faktura er valgt.", Helpers.GetFullClassPath(() => form.fakturamottaker.organisasjonsnummer), "ERROR", Helpers.GetFullClassPath(() => form.fakturamottaker.ehfFaktura));
            _validationResult.AddRule(skjema, "4397.3.10.2", null, "Navnet til fakturamottaker må fylles ut.", Helpers.GetFullClassPath(() => form.fakturamottaker.navn), "ERROR", Helpers.GetFullClassPath(() => form.fakturamottaker));

            _validationResult.AddRule(skjema, "4397.3.10.3", null, "Adressen til fakturamottaker må fylles ut når papirfaktura er valgt.", Helpers.GetFullClassPath(() => form.fakturamottaker.adresse), "ERROR", Helpers.GetFullClassPath(() => form.fakturamottaker.fakturaPapir));
            _validationResult.AddRule(skjema, "4397.3.10.3.1", null, "Adresselinje 1 må fylles ut for fakturamottaker når papirfaktura er valgt.", Helpers.GetFullClassPath(() => form.fakturamottaker.adresse.adresselinje1), "ERROR", Helpers.GetFullClassPath(() => form.fakturamottaker.fakturaPapir));
            _validationResult.AddRule(skjema, "4397.3.10.3.2", null, "Postnummeret til fakturamottaker må fylles ut når papirfaktura er valgt.", Helpers.GetFullClassPath(() => form.fakturamottaker.adresse.postnr), "ERROR", Helpers.GetFullClassPath(() => form.fakturamottaker.fakturaPapir));
            _validationResult.AddRule(skjema, "4397.3.10.3.3", null, "Poststedet til fakturamottaker må fylles ut når papirfaktura er valgt.", Helpers.GetFullClassPath(() => form.fakturamottaker.adresse.poststed), "ERROR", Helpers.GetFullClassPath(() => form.fakturamottaker.fakturaPapir));

            //AnsvarsrettSoekerValidation
            _validationResult.AddRule(skjema, "4397.3.11", null, "Du må fylle ut informasjon om ansvarsrett for ansvarlig søker.", Helpers.GetFullClassPath(() => form.ansvarsrettSoeker), "ERROR", null);
            _validationResult.AddRule(skjema, "4397.3.11.1", null, "Du må fylle ut tiltaksklassen for ansvarlig søker.", Helpers.GetFullClassPath(() => form.ansvarsrettSoeker.tiltaksklasse), "ERROR", null);
            _validationResult.AddRule(skjema, "4397.3.11.2", null, "'{0}' er en ugyldig kodeverdi for tiltaksklasse. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/byggesoknad/tiltaksklasse", Helpers.GetFullClassPath(() => form.dispensasjon[0].dispensasjonstype.kodeverdi), "ERROR", null);
            _validationResult.AddRule(skjema, "4397.3.11.3", null, "Du må svare på om ansvarlig søker har sentral godkjenning for valgt tiltaksklasse.", Helpers.GetFullClassPath(() => form.ansvarsrettSoeker.harSentralGodkjenning), "ERROR", null);
            _validationResult.AddRule(skjema, "4397.3.11.4", null, "Du må krysse av for at foretaket erklærer ansvarsrett som ansvarlig søker.", Helpers.GetFullClassPath(() => form.ansvarsrettSoeker.erklaeringAnsvar), "ERROR", null);

            //ansvarligSoeker
            _validationResult.AddRule(skjema, "4397.1.12", null, "Informasjon om ansvarlig søker må fylles ut.", Helpers.GetFullClassPath(() => form.ansvarligSoeker), "ERROR", null);
            _validationResult.AddRule(skjema, "4397.1.12.1", null, "Kodeverdien for 'partstype' til ansvarlig søker må fylles ut.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.partstype.kodeverdi), "ERROR", null);
            _validationResult.AddRule(skjema, "4397.1.12.2", null, "'{0}' er en ugyldig kodeverdi for partstypen til ansvarlig søker. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/byggesoknad/partstype", Helpers.GetFullClassPath(() => form.ansvarsrettSoeker.tiltaksklasse.kodeverdi), "ERROR", null);
            //Ansvarlig søker.foedselsnummer
            _validationResult.AddRule(skjema, "4397.3.12.3", null, "Fødselsnummer må fylles ut når ansvarlig søker er en privatperson.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.foedselsnummer), "ERROR", null);
            _validationResult.AddRule(skjema, "4397.3.12.3.1", null, "Fødselsnummeret til ansvarlig søker er ikke gyldig.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.foedselsnummer), "ERROR", null);
            _validationResult.AddRule(skjema, "4397.3.12.3.2", null, "Fødselsnummeret til ansvarlig søker har ikke gyldig kontrollsiffer.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.foedselsnummer), "ERROR", null);
            _validationResult.AddRule(skjema, "4397.3.12.3.3", null, "Fødselsnummeret til ansvarlig søker kan ikke dekrypteres.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.foedselsnummer), "ERROR", null);
            //ansvarligSoeker.organisasjonsnummer
            _validationResult.AddRule(skjema, "4397.2.12.4", null, "Organisasjonsnummeret til ansvarlig søker må fylles ut.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.organisasjonsnummer), "ERROR", null);
            _validationResult.AddRule(skjema, "4397.2.12.4.1", null, "Organisasjonsnummeret ('{0}') til ansvarlig søker har ikke gyldig kontrollsiffer.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.organisasjonsnummer), "ERROR", null);
            _validationResult.AddRule(skjema, "4397.3.12.4.2", null, "Organisasjonsnummeret ('{0}') til ansvarlig søker er ikke gyldig.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.organisasjonsnummer), "ERROR", null);
            _validationResult.AddRule(skjema, "4397.2.12.4.3", null, "Kontaktpersonen til ansvarlig søker bør fylles ut.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.kontaktperson.navn), "WARNING", null);
            _validationResult.AddRule(skjema, "4397.3.12.4.3.1", null, "Navnet til kontaktpersonen for ansvarlig søker bør fylles ut.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.kontaktperson.navn), "WARNING", null);
            _validationResult.AddRule(skjema, "4397.3.12.4.3.2", null, "Telefonnummeret eller mobilnummeret til ansvarlig søkers kontaktperson bør fylles ut.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.kontaktperson), "WARNING", null);
            _validationResult.AddRule(skjema, "4397.3.12.4.3.3", null, "Telefonnummeret til ansvarlig søkers kontaktperson må kun inneholde tall og '+'.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.kontaktperson.telefonnummer), "ERROR", null);
            _validationResult.AddRule(skjema, "4397.3.12.4.3.4", null, "Mobilnummeret til ansvarlig søkers kontaktperson må kun inneholde tall og '+'.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.kontaktperson.mobilnummer), "ERROR", null);
            _validationResult.AddRule(skjema, "4397.3.12.4.3.5", null, "E-postadressen til ansvarlig søkers kontaktperson bør fylles ut.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.kontaktperson.epost), "WARNING", null);
            //ansvarligSoeker.adress
            _validationResult.AddRule(skjema, "4397.1.12.5", null, "Adressen til ansvarlig søker bør fylles ut.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.adresse), "WARNING", null);
            _validationResult.AddRule(skjema, "4397.3.12.5.1", null, "Adresselinje 1 bør fylles ut for ansvarlig søker.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.adresse.adresselinje1), "WARNING", null);
            _validationResult.AddRule(skjema, "4397.3.12.5.2", null, "Ugyldig landkode for ansvarlig søker.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.adresse.landkode), "WARNING", null);
            _validationResult.AddRule(skjema, "4397.1.12.5.3", null, "Postnummeret til ansvarlig søker bør angis.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.adresse.postnr), "WARNING", null);
            _validationResult.AddRule(skjema, "4397.3.12.5.3.1", null, "Postnummeret '{0}' til ansvarlig søker har ikke gyldig kontrollsiffer.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.adresse.postnr), "WARNING", null);
            _validationResult.AddRule(skjema, "4397.3.12.5.3.2", null, "Postnummeret '{0}' til ansvarlig søker er ugyldig. Du kan sjekke riktig postnummer på http://adressesok.bring.no/", Helpers.GetFullClassPath(() => form.ansvarligSoeker.adresse.postnr), "WARNING", null);
            _validationResult.AddRule(skjema, "4397.3.12.5.3.3", null, "Postnummeret '{0}' til ansvarlig søker stemmer ikke overens med poststedet '{1}'. Riktig postnummer er '{2}'. Du kan sjekke riktig poststed på http://adressesok.bring.no/", Helpers.GetFullClassPath(() => form.ansvarligSoeker.adresse.postnr), "WARNING", null);
            _validationResult.AddRule(skjema, "4397.3.12.5.3.4", null, "Postnummeret '{0}' til ansvarlig søker ble ikke validert. Postnummeret kan være riktig, men en teknisk feil gjør at vi ikke kan bekrefte det.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.adresse.postnr), "WARNING", null);
            //ansvarligSoeker...
            _validationResult.AddRule(skjema, "4397.3.12.6", null, "Navnet til ansvarlig søker må fylles ut.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.navn), "ERROR", null);
            _validationResult.AddRule(skjema, "4397.3.12.7", null, "Telefonnummeret eller mobilnummeret til ansvarlig søker bør fylles ut.", Helpers.GetFullClassPath(() => form.ansvarligSoeker), "WARNING", null);
            _validationResult.AddRule(skjema, "4397.3.12.8", null, "Telefonnummeret til ansvarlig søker må kun inneholde tall og '+'.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.telefonnummer), "ERROR", null);
            _validationResult.AddRule(skjema, "4397.3.12.9", null, "Mobilnummeret til ansvarlig søker må kun inneholde tall og '+'.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.mobilnummer), "ERROR", null);
            _validationResult.AddRule(skjema, "4397.3.12.10", null, "E-postadressen til ansvarlig søker bør fylles ut.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.epost), "WARNING", null);

            // Veddlegg validering
            _validationResult.AddRule(skjema, "4397.2.13.1", "1.73", "Vedlegg ‘Situasjonsplan’ mangler for valgte tiltakstyper i søknad, jfr. nasjonal sjekkliste punkt 1.73.", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.type[0].kodeverdi), "ERROR", "/Vedlegg");
            _validationResult.AddRule(skjema, "4397.2.13.2", null, "Minst én tegning med en av vedleggstypene ‘TegningEksisterendeSnitt’, ‘TegningNyttSnitt’, ‘TegningEksisterendePlan’, ‘TegningNyPlan’, ‘TegningEksisterendeFasade’ eller ‘TegningNyFasade’ må være vedlagt søknaden.", Helpers.GetFullClassPath(() => form), "ERROR", "/Vedlegg");
            _validationResult.AddRule(skjema, "4397.2.13.3", "1.79", "Plantegning må være vedlagt søknaden for valgte tiltakstyper, jfr. nasjonal sjekkliste punkt 1.79. Vi anbefaler at vedlegget ‘TegningNyPlan’ legges ved. Gjelder tiltaket eksisterende bygning, skal også ‘TegningEksisterendePlan’ være vedlagt.", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.type[0].kodeverdi), "WARNING", "/Vedlegg");
            _validationResult.AddRule(skjema, "4397.2.13.4", "1.80", "Snittegning må være vedlagt søknaden for valgte tiltakstyper, jfr. nasjonal sjekkliste punkt 1.80. Vi anbefaler at vedlegget ‘TegningNyttSnitt’ legges ved. Gjelder tiltaket eksisterende bygning, skal også ‘TegningEksisterendeSnitt’ være vedlagt.", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.type[0].kodeverdi), "WARNING", "/Vedlegg");
            _validationResult.AddRule(skjema, "4397.2.13.5", "1.81", "Fasadetegning må være vedlagt søknaden for valgte tiltakstyper, jfr. nasjonal sjekkliste punkt 1.81. Vi anbefaler at vedlegget ‘TegningNyFasade’ legges ved. Gjelder tiltaket eksisterende bygning, skal også ‘TegningEksisterendeFasade’ være vedlagt.", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.type[0].kodeverdi), "WARNING", "/Vedlegg");
            _validationResult.AddRule(skjema, "4397.2.13.6", "17.11", "Vedlegg eller underskjema ‘Gjennomføringsplan’ skal følge med søknaden.", Helpers.GetFullClassPath(() => form), "ERROR", "/VedleggUnderskjema");
            _validationResult.AddRule(skjema, "4397.2.13.7", "17.17", "Normalt skal gjennomføringsplan følge søknaden. Dersom selvbygger skal ha alt ansvar, skal det ikke sendes inn gjennomføringsplan.", Helpers.GetFullClassPath(() => form), "WARNING", "/VedleggUnderskjema");
            _validationResult.AddRule(skjema, "4397.2.13.7.1", "17.17", "Søknad om ansvarsrett for selvbygger skal følge med søknaden når ansvarlig søker er en privatperson.", Helpers.GetFullClassPath(() => form), "WARNING", "/VedleggUnderskjema");
            _validationResult.AddRule(skjema, "4397.2.13.8", "1.94", "Underskjemaet 'Matrikkelopplysninger' bør være utfylt og vedlagt søknaden for valgte tiltakstyper, jfr. nasjonal sjekkliste punkt 1.94.", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.type[0].kodeverdi), "WARNING", "/Vedlegg");
            _validationResult.AddRule(skjema, "4397.2.13.9", null, "Når 'byggesaksBIM' er vedlagt, bør underskjemaet 'Vedleggsopplysninger' fylles ut for å kunne verifisere at det er en godkjent byggesaksBIM.", Helpers.GetFullClassPath(() => form), "WARNING", "/Vedlegg");

            //rammebetingelser.loefteinnretninger
            _validationResult.AddRule(skjema, "4397.2.14", "18.22", "Informasjon om løfteinnretninger kreves for valgte tiltakstyper, jfr. nasjonal sjekkliste punkt 18.22.", Helpers.GetFullClassPath(() => form.rammebetingelser.loefteinnretninger), "ERROR", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.type[0].kodeverdi));
            _validationResult.AddRule(skjema, "4397.2.14.1", "18.22", "Du må svare på om det er løfteinnretninger som omfattes av TEK i bygningen, jfr. nasjonal sjekkliste punkt 18.22.", Helpers.GetFullClassPath(() => form.rammebetingelser.loefteinnretninger.erLoefteinnretningIBygning), "ERROR", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.type[0].kodeverdi));
            _validationResult.AddRule(skjema, "4397.2.14.2", "18.22", "Du må svare på om det planlegges løfteinnretninger som omfattes av TEK i bygningen, jfr. nasjonal sjekkliste punkt 18.22.", Helpers.GetFullClassPath(() => form.rammebetingelser.loefteinnretninger.planleggesLoefteinnretningIBygning), "ERROR", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.type[0].kodeverdi));
            _validationResult.AddRule(skjema, "4397.2.14.3", null, "Du har valgt at det planlegges løfteinnretninger i bygningen. Da må du fylle ut hvilke typer løfteinnretninger som planlegges.", Helpers.GetFullClassPath(() => form.rammebetingelser.loefteinnretninger), "ERROR", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.type[0].kodeverdi));
        }

        public ValidationResult Validate(RammetillatelseType form, List<string> formSetElements)
        {
            MinstEtTiltak(form);
            //Add to validationResults all "tiltakstypes"
            _validationResult.tiltakstyperISoeknad = _tiltakstyperISoeknad;

            Parallel.Invoke(
                () => { TiltakshaverValidation(form); },
                () => { EiendomByggestedValidation(form); },
                //Rammebetingelser
                () => { AdkomstValidering(form, formSetElements); },
                () => { ArealdisponeringValidering(form); },
                //**generelleVilkaar
                () => { KulturminneValidering(form, formSetElements); },
                () => { ArbeidsplasserValidering(form, formSetElements); },
                () => { VannforsyningValidering(form); },
                () => { AvloepValidering(form); },
                () => { KravTilByggegrunnValidering(form, formSetElements); },
                () => { LofteInnretningValidering(form); }, //TODO spør Hilde
                () => { PlasseringValidering(form, formSetElements); },
                () => { GenerelleVilkaarValidering(form); },
                //rammebetingelser.Plan
                () => { GjeldendePlanValidering(form, formSetElements); },
                () => { AndrePlanerValidering(form); },
                //Dispensasjon
                () => { DispensasjonValidering(form, formSetElements); },
                //Metadata
                () => { MetadataValidering(form); },
                //Varsling
                () => { VarslingValidering(form, formSetElements); },
                //Faktura
                () => { FakturamottakerValidation(form); },
                //ansvarsrettSoeker
                () => { AnsvarsrettSoekerValidation(form, formSetElements); },

                () => { AnsvarligSokerValidation(form); },

                //Vedlegg Validering
                () => { SituasjonsplanValidering(formSetElements); },
                () => { TegningValidering(formSetElements); },
                () => { PlantegningValidering(formSetElements); },
                () => { SnitttegningerValidering(formSetElements); },
                () => { FasadetegningerValidering(formSetElements); },
                () => { AnsvarValidering(form, formSetElements); },
                () => { MatrikkelopplysningValidering(formSetElements); },
                () => { ByggesaksBIMValidering(formSetElements); }

            );

            GetChecklistAnswer(form);

            return _validationResult;
        }

        internal void MinstEtTiltak(RammetillatelseType form)
        {
            if (Helpers.ObjectIsNullOrEmpty(form.beskrivelseAvTiltak))
            {
                _validationResult.AddMessage("4397.2.3", null);
            }
            else
            {
                if (Helpers.ObjectIsNullOrEmpty(form.beskrivelseAvTiltak.type))
                {
                    _validationResult.AddMessage("4397.2.3.1", null);
                }
                else
                {
                    foreach (var kodeType in form.beskrivelseAvTiltak.type)
                    {
                        if (string.IsNullOrEmpty(kodeType?.kodeverdi))
                        {
                            _validationResult.AddMessage("4397.2.3.2", null);
                        }
                        else
                        {
                            if (!_codeListService.IsCodeValidInCodeList("tiltaktype", kodeType.kodeverdi))
                            {
                                _validationResult.AddMessage("4397.2.3.2.1", new[] { kodeType.kodeverdi });
                            }
                            else
                            {
                                //Add tiltak type to list
                                _tiltakstyperISoeknad.Add(kodeType.kodeverdi);

                                if (String.IsNullOrEmpty(kodeType.kodebeskrivelse))
                                {
                                    _validationResult.AddMessage("4397.2.3.3", null);
                                }
                                else
                                {
                                    if (!_codeListService.IsCodelistLabelValid("tiltaktype", kodeType.kodeverdi, kodeType.kodebeskrivelse))
                                        _validationResult.AddMessage("4397.2.3.4", new[] { kodeType.kodebeskrivelse });
                                }
                            }
                        }
                    }
                    if (!Helpers.ObjectIsNullOrEmpty(form.beskrivelseAvTiltak.bruk))
                    {
                        //beskTiltak.bruk.anleggstype
                        if (!Helpers.ObjectIsNullOrEmpty(form.beskrivelseAvTiltak.bruk.anleggstype))
                        {
                            if (!string.IsNullOrEmpty(form.beskrivelseAvTiltak.bruk.anleggstype.kodeverdi))
                            {
                                if (!_codeListService.IsCodeValidInCodeList("anleggstype", form.beskrivelseAvTiltak.bruk.anleggstype.kodeverdi))
                                {
                                    _validationResult.AddMessage("4397.2.3.5", new[] { form.beskrivelseAvTiltak.bruk.anleggstype.kodeverdi });
                                }
                                else
                                {
                                    if (String.IsNullOrEmpty(form.beskrivelseAvTiltak.bruk.anleggstype.kodebeskrivelse))
                                    {
                                        _validationResult.AddMessage("4397.3.3.5.2", null);
                                    }
                                    else
                                    {
                                        if (!_codeListService.IsCodelistLabelValid("anleggstype", form.beskrivelseAvTiltak.bruk.anleggstype.kodeverdi, form.beskrivelseAvTiltak.bruk.anleggstype.kodebeskrivelse))
                                            _validationResult.AddMessage("4397.3.3.5.3", new[] { form.beskrivelseAvTiltak.bruk.anleggstype.kodebeskrivelse });
                                    }
                                }
                            }
                        }

                        //beskTiltak.bruk.naeringsgruppe
                        if (!Helpers.ObjectIsNullOrEmpty(form.beskrivelseAvTiltak.bruk.naeringsgruppe))
                        {
                            if (!string.IsNullOrEmpty(form.beskrivelseAvTiltak.bruk.naeringsgruppe.kodeverdi))
                            {
                                if (!_codeListService.IsCodeValidInCodeList("naeringsgruppe", form.beskrivelseAvTiltak.bruk.naeringsgruppe.kodeverdi))
                                {
                                    _validationResult.AddMessage("4397.2.3.6", new[] { form.beskrivelseAvTiltak.bruk.naeringsgruppe.kodeverdi });
                                }
                            }
                        }

                        //beskTiltak.bruk.bygningstype //(bare en her men kan være flere tiltak)
                        if (!Helpers.ObjectIsNullOrEmpty(form.beskrivelseAvTiltak.bruk.bygningstype))
                        {
                            if (!string.IsNullOrEmpty(form.beskrivelseAvTiltak.bruk.bygningstype.kodeverdi))
                            {
                                if (!_codeListService.IsCodeValidInCodeList("bygningstype", form.beskrivelseAvTiltak.bruk.bygningstype.kodeverdi))
                                {
                                    _validationResult.AddMessage("4397.2.3.7", new[] { form.beskrivelseAvTiltak.bruk.bygningstype.kodeverdi });
                                }
                                else
                                {
                                    if (String.IsNullOrEmpty(form.beskrivelseAvTiltak.bruk.bygningstype.kodebeskrivelse))
                                    {
                                        _validationResult.AddMessage("4397.2.3.8", null);
                                    }
                                    else
                                    {
                                        if (!_codeListService.IsCodelistLabelValid("bygningstype", form.beskrivelseAvTiltak.bruk.bygningstype.kodeverdi, form.beskrivelseAvTiltak.bruk.bygningstype.kodebeskrivelse))
                                            _validationResult.AddMessage("4397.2.3.9", new[] { form.beskrivelseAvTiltak.bruk.bygningstype.kodebeskrivelse });
                                    }
                                }
                            }
                        }

                        if (!Helpers.ObjectIsNullOrEmpty(form.beskrivelseAvTiltak.bruk.tiltaksformaal))
                        {
                            //tiltaksformaal
                            foreach (var tiltaksformaal in form.beskrivelseAvTiltak.bruk.tiltaksformaal)
                            {
                                if (!String.IsNullOrEmpty(tiltaksformaal.kodeverdi))
                                {
                                    if (!_codeListService.IsCodeValidInCodeList("tiltaksformal", tiltaksformaal.kodeverdi))
                                    {
                                        _validationResult.AddMessage("4397.2.3.10", new[] { tiltaksformaal.kodeverdi });
                                    }
                                    else
                                    {
                                        if (string.IsNullOrEmpty(tiltaksformaal.kodebeskrivelse))
                                        {
                                            _validationResult.AddMessage("4397.2.3.10.1", new[] { tiltaksformaal.kodeverdi });
                                        }
                                        else
                                        {
                                            if (!_codeListService.IsCodelistLabelValid("tiltaksformal", tiltaksformaal.kodeverdi, tiltaksformaal.kodebeskrivelse))
                                                _validationResult.AddMessage("4397.2.3.10.2", new[] { tiltaksformaal.kodebeskrivelse });
                                        }

                                        if (tiltaksformaal.kodeverdi == "Annet")
                                        {
                                            if (String.IsNullOrEmpty(form.beskrivelseAvTiltak.bruk.beskrivPlanlagtFormaal))
                                            {
                                                _validationResult.AddMessage("4397.2.3.11", null);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        internal void TiltakshaverValidation(RammetillatelseType form)
        {
            using (var logger = new PerfTimerLogger())
            {
                if (Helpers.ObjectIsNullOrEmpty(form.tiltakshaver?.partstype))
                {
                    _validationResult.AddMessage("4397.3.5", null);
                }
                else
                {
                    if (Helpers.ObjectIsNullOrEmpty(form.tiltakshaver.partstype?.kodeverdi))
                    {
                        _validationResult.AddMessage("4397.1.5.1", null);
                    }
                    else
                    {
                        if (!_codeListService.IsCodelistValid("Partstype", form.tiltakshaver.partstype?.kodeverdi))
                        {
                            _validationResult.AddMessage("4397.1.5.2", new[] { form.tiltakshaver.partstype?.kodeverdi });
                        }
                        else
                        {
                            if (form.tiltakshaver.partstype?.kodeverdi != "Privatperson")
                            {
                                var organisasjonsnummerValidation = GeneralValidations.Validate_OrgnummerEnum(form.tiltakshaver.organisasjonsnummer);
                                switch (organisasjonsnummerValidation)
                                {
                                    case GeneralValidations.OrganisasjonsnummerValidation.Empty:
                                        _validationResult.AddMessage("4397.3.5.4", null);
                                        break;

                                    case GeneralValidations.OrganisasjonsnummerValidation.InvalidDigitsControl:
                                        _validationResult.AddMessage("4397.3.5.4.2", new[] { form.tiltakshaver.organisasjonsnummer });
                                        break;

                                    case GeneralValidations.OrganisasjonsnummerValidation.Invalid:
                                        _validationResult.AddMessage("4397.3.5.4.1", new[] { form.tiltakshaver.organisasjonsnummer });

                                        break;
                                }

                                if (Helpers.ObjectIsNullOrEmpty(form.tiltakshaver.kontaktperson))
                                {
                                    _validationResult.AddMessage("4397.2.5.4.3", null);
                                }
                                else
                                {
                                    if (string.IsNullOrEmpty(form.tiltakshaver.kontaktperson.navn))
                                        _validationResult.AddMessage("4397.3.5.4.3.1", null);
                                    if (string.IsNullOrEmpty(form.tiltakshaver.kontaktperson.telefonnummer) && string.IsNullOrEmpty(form.tiltakshaver.kontaktperson.mobilnummer))
                                    {
                                        _validationResult.AddMessage("4397.2.5.4.3.2", null);
                                    }
                                    else
                                    {
                                        if (!string.IsNullOrEmpty(form.tiltakshaver.kontaktperson.telefonnummer))
                                        {
                                            var telefonNumber = form.tiltakshaver.kontaktperson.telefonnummer;
                                            var isValidTelefonNumber = telefonNumber.All(c => "+0123456789".Contains(c));
                                            if (!isValidTelefonNumber)
                                            {
                                                _validationResult.AddMessage("4397.3.5.4.3.3", null);
                                            }
                                        }
                                        if (!string.IsNullOrEmpty(form.tiltakshaver.kontaktperson.mobilnummer))
                                        {
                                            var mobilNummer = form.tiltakshaver.kontaktperson.mobilnummer;
                                            var isValidmobilnummer = mobilNummer.All(c => "+0123456789".Contains(c));
                                            if (!isValidmobilnummer)
                                            {
                                                _validationResult.AddMessage("4397.3.5.4.3.4", null);
                                            }
                                        }
                                    }

                                    if (string.IsNullOrEmpty(form.tiltakshaver.kontaktperson.epost))
                                        _validationResult.AddMessage("4397.3.5.4.3.5", null);
                                }
                            }

                            //Adresse
                            if (Helpers.ObjectIsNullOrEmpty(form.tiltakshaver.adresse))
                            {
                                _validationResult.AddMessage("4397.3.5.5", null);
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(form.tiltakshaver.adresse.adresselinje1))
                                {
                                    _validationResult.AddMessage("4397.3.5.5.1", null);
                                }
                                else
                                {
                                    if (!CountryCodeHandler.IsCountryNorway(form.tiltakshaver.adresse.landkode))
                                    {
                                        if (!CountryCodeHandler.VerifyCountryCode(form.tiltakshaver.adresse.landkode))
                                        {
                                            _validationResult.AddMessage("4397.3.5.5.2", null);
                                        }
                                    }
                                    else
                                    {
                                        var postNr = form.tiltakshaver.adresse.postnr;
                                        var landkode = form.tiltakshaver.adresse.landkode;

                                        if (string.IsNullOrEmpty(form.tiltakshaver.adresse.postnr))
                                        {
                                            _validationResult.AddMessage("4397.3.5.5.3", null);
                                        }
                                        else
                                        {
                                            if (!GeneralValidations.postNumberMatch(form.tiltakshaver.adresse.postnr).Success)
                                            {
                                                _validationResult.AddMessage("4397.3.5.5.3.1", new[] { postNr });
                                            }
                                            else
                                            {
                                                var postnrValidation = _postalCodeProvider.ValidatePostnr(postNr, landkode);
                                                if (!Helpers.ObjectIsNullOrEmpty(postnrValidation))
                                                {
                                                    if (!postnrValidation.Valid)
                                                    {
                                                        _validationResult.AddMessage("4397.3.5.5.3.2", null);
                                                    }
                                                    else
                                                    {
                                                        if (!postnrValidation.Result.Equals(form.tiltakshaver.adresse.poststed, StringComparison.CurrentCultureIgnoreCase))
                                                        {
                                                            _validationResult.AddMessage("4397.3.5.5.3.3", new[] { postNr, form.tiltakshaver.adresse.poststed, postnrValidation.Result });
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    _validationResult.AddMessage("4397.3.5.5.3.4", new[] { postNr });
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            if (string.IsNullOrEmpty(form.tiltakshaver.navn))
                                _validationResult.AddMessage("4397.3.5.6", null);
                            if (string.IsNullOrEmpty(form.tiltakshaver.telefonnummer) && string.IsNullOrEmpty(form.tiltakshaver.mobilnummer))
                            {
                                _validationResult.AddMessage("4397.3.5.7", null);
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(form.tiltakshaver.telefonnummer))
                                {
                                    var telefonNumber = form.tiltakshaver.telefonnummer;
                                    var isValidTelefonNumber = telefonNumber.All(c => "+0123456789".Contains(c));
                                    if (!isValidTelefonNumber)
                                    {
                                        _validationResult.AddMessage("4397.3.5.8", null);
                                    }
                                }
                                if (!string.IsNullOrEmpty(form.tiltakshaver.mobilnummer))
                                {
                                    var mobilNummer = form.tiltakshaver.mobilnummer;
                                    var isValidmobilnummer = mobilNummer.All(c => "+0123456789".Contains(c));
                                    if (!isValidmobilnummer)
                                    {
                                        _validationResult.AddMessage("4397.3.5.9", null);
                                    }
                                }
                            }
                            if (string.IsNullOrEmpty(form.tiltakshaver.epost))
                                _validationResult.AddMessage("4397.3.5.10", null);
                        }
                    }
                }
            }
        }

        internal void EiendomByggestedValidation(RammetillatelseType form)
        {
            if (Helpers.ObjectIsNullOrEmpty(form.eiendomByggested))
            {
                _validationResult.AddMessage("4397.2.4", null);
            }
            else
            {
                foreach (EiendomType eiendomType in form.eiendomByggested)
                {
                    if (eiendomType.eiendomsidentifikasjon != null)
                    {
                        var kommunenummer = eiendomType.eiendomsidentifikasjon.kommunenummer;
                        var kommunenummerValideringStatus = _municipalityValidator.Validate_kommunenummerStatus(kommunenummer);
                        if (kommunenummerValideringStatus.Status != MunicipalityValidation.Ok)
                        {
                            switch (kommunenummerValideringStatus.Status)
                            {
                                case MunicipalityValidation.Empty:
                                    _validationResult.AddMessage("4397.3.4.1", null);
                                    break;

                                case MunicipalityValidation.Invalid:
                                    _validationResult.AddMessage("4397.3.4.2", new[] { kommunenummer });
                                    break;

                                case MunicipalityValidation.Expired:
                                    _validationResult.AddMessage("4397.3.4.3", new[] { kommunenummer, kommunenummerValideringStatus.Status.ToString() });
                                    break;
                            }
                        }
                        else
                        {
                            var gaardsnummer = eiendomType.eiendomsidentifikasjon.gaardsnummer;
                            var bruksnummer = eiendomType.eiendomsidentifikasjon.bruksnummer;
                            //TODO kan grn or brn be 0
                            if (string.IsNullOrEmpty(gaardsnummer) || string.IsNullOrEmpty(bruksnummer))
                            {
                                if (string.IsNullOrEmpty(gaardsnummer))
                                    _validationResult.AddMessage("4397.3.4.4", null);

                                if (string.IsNullOrEmpty(bruksnummer))
                                    _validationResult.AddMessage("4397.3.4.4.1", null);
                            }
                            else
                            {
                                int gaardsnumerInt = 0;
                                int bruksnummerInt = 0;
                                if (int.TryParse(gaardsnummer, out gaardsnumerInt) && int.TryParse(bruksnummer, out bruksnummerInt))
                                {
                                    int festenummerInt = 0;
                                    int seksjonsnummerInt = 0;
                                    int.TryParse(eiendomType.eiendomsidentifikasjon.festenummer, out festenummerInt);
                                    int.TryParse(eiendomType.eiendomsidentifikasjon.seksjonsnummer, out seksjonsnummerInt);

                                    if (gaardsnumerInt < 0 || bruksnummerInt < 0)
                                    {
                                        if (gaardsnumerInt < 0)
                                            _validationResult.AddMessage("4397.3.4.5", new[] { eiendomType.eiendomsidentifikasjon.gaardsnummer });

                                        if (bruksnummerInt < 0)
                                            _validationResult.AddMessage("4397.3.4.6", new[] { eiendomType.eiendomsidentifikasjon.bruksnummer });
                                    }
                                    else
                                    {
                                        //## MATRIKKEL
                                        var matrikkelnrExist = _matrikkelService.MatrikkelnrExist(kommunenummer, gaardsnumerInt, bruksnummerInt, festenummerInt, seksjonsnummerInt);
                                        if (matrikkelnrExist.HasValue)
                                        {
                                            if (!matrikkelnrExist.Value)
                                            {
                                                _validationResult.AddMessage("4397.3.4.12", new[]
                                                {
                                                    kommunenummer,
                                                    eiendomType.eiendomsidentifikasjon.gaardsnummer,
                                                    eiendomType.eiendomsidentifikasjon.bruksnummer,
                                                    eiendomType.eiendomsidentifikasjon.festenummer,
                                                    eiendomType.eiendomsidentifikasjon.seksjonsnummer,
                                                });
                                            }
                                        }
                                        else
                                        {
                                            _validationResult.AddMessage("4397.3.4.13", new[]
                                            {
                                                kommunenummer,
                                                eiendomType.eiendomsidentifikasjon.gaardsnummer,
                                                eiendomType.eiendomsidentifikasjon.bruksnummer,
                                                eiendomType.eiendomsidentifikasjon.festenummer,
                                                eiendomType.eiendomsidentifikasjon.seksjonsnummer,
                                            });
                                        }

                                        if (!String.IsNullOrEmpty(eiendomType.bygningsnummer))
                                        {
                                            long bygningsnrLong = 0;
                                            if (!long.TryParse(eiendomType.bygningsnummer, out bygningsnrLong))
                                            {
                                                _validationResult.AddMessage("4397.3.4.7", new[] { eiendomType.bygningsnummer });
                                            }
                                            else
                                            {
                                                if (bygningsnrLong <= 0)
                                                {
                                                    _validationResult.AddMessage("4397.3.4.8", new[] { eiendomType.eiendomsidentifikasjon.gaardsnummer, eiendomType.eiendomsidentifikasjon.bruksnummer });
                                                }
                                                else
                                                {
                                                    ////## MATRIKKEL
                                                    var isBygningValid = _matrikkelService.IsBygningOnMatrikkelnr(bygningsnrLong, kommunenummer, gaardsnumerInt, bruksnummerInt, festenummerInt, seksjonsnummerInt);
                                                    if (!isBygningValid.GetValueOrDefault())
                                                    {
                                                        _validationResult.AddMessage("4397.3.4.14", new[]
                                                        {
                                                            kommunenummer,
                                                            eiendomType.eiendomsidentifikasjon.gaardsnummer,
                                                            eiendomType.eiendomsidentifikasjon.bruksnummer,
                                                            eiendomType.eiendomsidentifikasjon.festenummer,
                                                            eiendomType.eiendomsidentifikasjon.seksjonsnummer,
                                                        });
                                                    }
                                                }
                                            }
                                        }

                                        if (Helpers.ObjectIsNullOrEmpty(eiendomType.adresse))
                                        {
                                            _validationResult.AddMessage("4397.3.4.9", null);
                                        }
                                        else
                                        {
                                            if (string.IsNullOrEmpty(eiendomType.adresse?.adresselinje1))
                                            {
                                                _validationResult.AddMessage("4397.3.4.9.1", null);
                                            }
                                            else
                                            {
                                                if (String.IsNullOrEmpty(eiendomType.adresse.gatenavn) || String.IsNullOrEmpty(eiendomType.adresse.husnr))
                                                {
                                                    _validationResult.AddMessage("4397.3.4.9.2", null);
                                                }
                                                else
                                                {
                                                    //## MATRIKKEL
                                                    var isAdresseValid = _matrikkelService.IsVegadresseOnMatrikkelnr(eiendomType.adresse.gatenavn, eiendomType.adresse.husnr, eiendomType.adresse.bokstav, kommunenummer, gaardsnumerInt, bruksnummerInt, festenummerInt, seksjonsnummerInt);
                                                    if (isAdresseValid == null)
                                                    {
                                                        _validationResult.AddMessage("4397.3.4.15", null);
                                                    }
                                                    else if (!isAdresseValid.GetValueOrDefault())
                                                    {
                                                        _validationResult.AddMessage("4397.3.4.15.1", null);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if (String.IsNullOrEmpty(eiendomType.kommunenavn))
                    {
                        _validationResult.AddMessage("4397.3.4.10", null);
                    }

                    if (!String.IsNullOrEmpty(eiendomType.bolignummer))
                    {
                        if (GeneralValidations.Validate_bruksenhetsnummer(eiendomType.bolignummer).Status == ValidationStatus.Fail)
                        {
                            _validationResult.AddMessage("4397.2.4.11", null);
                        }
                    }
                }
            }
        }

        internal void LofteInnretningValidering(RammetillatelseType form)
        {
            //Er det løfteinnretninger som omfattes av TEK i bygningen?
            if (Helpers.ObjectIsNullOrEmpty(form.rammebetingelser?.loefteinnretninger))
            {
                _validationResult.AddMessage("4397.2.14", null);
            }
            else if (form.rammebetingelser.loefteinnretninger.erLoefteinnretningIBygning.HasValue == false)
            {
                _validationResult.AddMessage("4397.2.14.1", null);
            }
            else if (form.rammebetingelser.loefteinnretninger.planleggesLoefteinnretningIBygning.HasValue == false)
            {
                _validationResult.AddMessage("4397.2.14.2", null);
            }

            if (form.rammebetingelser != null)
            {
                if (form.rammebetingelser.loefteinnretninger != null)
                {
                    if (form.rammebetingelser.loefteinnretninger.planleggesLoefteinnretningIBygning.GetValueOrDefault())
                    {
                        if (form.rammebetingelser.loefteinnretninger.planleggesHeis.HasValue == false && form.rammebetingelser.loefteinnretninger.planleggesTrappeheis.HasValue == false
                            && form.rammebetingelser.loefteinnretninger.planleggesRulletrapp.HasValue == false && form.rammebetingelser.loefteinnretninger.planleggesLoefteplattform.HasValue == false)
                            _validationResult.AddMessage("4397.2.14.3", null);
                    }
                }
            }
        }

        //form.rammebetingelser.adkomst
        internal void AdkomstValidering(RammetillatelseType form, List<string> formSetElements)
        {
            //Hvis tiltaksstype krever adkomst - ref sjekkpunkt 7.1
            if (Helpers.ObjectIsNullOrEmpty(form.rammebetingelser?.adkomst))
            {
                _validationResult.AddMessage("4397.2.6.1", null);
            }
            else
            {
                if (form.rammebetingelser.adkomst.nyeEndretAdkomst.HasValue == false)
                {
                    _validationResult.AddMessage("4397.2.6.1.1", null);
                }
            }

            //Vegtype - liste
            if (form.rammebetingelser?.adkomst != null)
            {
                if (form.rammebetingelser.adkomst.nyeEndretAdkomst.HasValue)
                {
                    if (form.rammebetingelser.adkomst.nyeEndretAdkomst.GetValueOrDefault())
                    {
                        //Vedleggssjekk
                        if (formSetElements == null || !formSetElements.Any(v => v.Equals("Avkjoerselsplan")))
                        {
                            _validationResult.AddMessage("4397.2.6.2.1", null);
                        }

                        if (form.rammebetingelser.adkomst.vegtype == null)
                        {
                            _validationResult.AddMessage("4397.2.6.2.2", null);
                        }
                        else
                        {
                            foreach (var vegtype in form.rammebetingelser.adkomst.vegtype)
                            {
                                if (!_codeListService.IsCodelistValid("vegtype", vegtype.kodeverdi))
                                {
                                    _validationResult.AddMessage("4397.1.6.2.3", new[] { vegtype.kodeverdi });
                                }
                                //sjekke om tillatelse er gitt for aktuell vegtype
                                if (vegtype.kodeverdi == "RiksFylkesveg")
                                {
                                    if (!form.rammebetingelser.adkomst.erTillatelseGittRiksFylkesveg.HasValue)
                                    {
                                        _validationResult.AddMessage("4397.2.6.2.4", null);
                                    }
                                }
                                if (vegtype.kodeverdi == "KommunalVeg")
                                {
                                    if (!form.rammebetingelser.adkomst.erTillatelseGittKommunalVeg.HasValue)
                                    {
                                        _validationResult.AddMessage("4397.2.6.2.5", null);
                                    }
                                }
                                if (vegtype.kodeverdi == "PrivatVeg")
                                {
                                    if (!form.rammebetingelser.adkomst.erTillatelseGittPrivatVeg.HasValue)
                                    {
                                        _validationResult.AddMessage("4397.2.6.2.6", null);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        internal void GjeldendePlanValidering(RammetillatelseType form, List<string> formSetElements)
        {
            //tiltakstyper som krever arealdisponering
            if (Helpers.ObjectIsNullOrEmpty(form.rammebetingelser?.plan?.gjeldendePlan))
            {
                _validationResult.AddMessage("4397.2.6.4", null);
            }
            else
            {
                if (Helpers.ObjectIsNullOrEmpty(form.rammebetingelser.plan.gjeldendePlan.plantype))
                {
                    _validationResult.AddMessage("4397.2.6.4.1", null);
                }
                else
                {
                    var planType = form.rammebetingelser.plan.gjeldendePlan.plantype;

                    if (!_codeListService.IsCodelistValid("Plantype", planType.kodeverdi))
                    {
                        _validationResult.AddMessage("4397.2.6.4.1.1", new[] { planType.kodeverdi });
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(planType.kodebeskrivelse))
                        {
                            _validationResult.AddMessage("4397.2.6.4.2", null);
                        }
                        else
                        {
                            if (!_codeListService.IsCodelistLabelValid("Plantype", planType.kodeverdi, planType.kodebeskrivelse))
                            {
                                _validationResult.AddMessage("4397.2.6.4.2.1", new[] { planType.kodebeskrivelse });
                            }
                        }
                    }
                }

                if (Helpers.ObjectIsNullOrEmpty(form.rammebetingelser?.plan?.gjeldendePlan?.beregningsregelGradAvUtnytting))
                {
                    _validationResult.AddMessage("4397.2.6.4.3", null);
                }
                else
                {
                    var beregningsregelGradAvUtnyttingKodeverdi = form.rammebetingelser.plan.gjeldendePlan.beregningsregelGradAvUtnytting.kodeverdi;
                    if (!_codeListService.IsCodelistValid("beregningsregelGradAvUtnytting", beregningsregelGradAvUtnyttingKodeverdi))
                    {
                        _validationResult.AddMessage("4397.2.6.4.4", new[] { beregningsregelGradAvUtnyttingKodeverdi });
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(form.rammebetingelser.plan.gjeldendePlan.beregningsregelGradAvUtnytting.kodebeskrivelse))
                        {
                            _validationResult.AddMessage("4397.2.6.4.5", new[] { beregningsregelGradAvUtnyttingKodeverdi });
                        }

                        if (beregningsregelGradAvUtnyttingKodeverdi == "annetUdef")
                        {
                            if (formSetElements == null || !formSetElements.Contains("UnderlagUtnytting"))
                            {
                                _validationResult.AddMessage("4397.2.6.4.6", null);
                            }
                        }
                    }
                }
                if (string.IsNullOrEmpty(form.rammebetingelser?.plan?.gjeldendePlan?.formaal))
                {
                    _validationResult.AddMessage("4397.2.6.4.7", null);
                }

                if (string.IsNullOrEmpty(form.rammebetingelser?.plan?.gjeldendePlan?.navn))
                    _validationResult.AddMessage("4397.2.6.4.8", null);
            }
        }

        internal void ArealdisponeringValidering(RammetillatelseType form)
        {
            //tiltakstyper som krever arealdisponering

            if (!Helpers.ObjectIsNullOrEmpty(form.rammebetingelser?.plan?.gjeldendePlan?.beregningsregelGradAvUtnytting))
            {
                var beregnGradUtnyttingKodeverdi = form.rammebetingelser.plan.gjeldendePlan.beregningsregelGradAvUtnytting.kodeverdi;
                if (beregnGradUtnyttingKodeverdi != null && !beregnGradUtnyttingKodeverdi.Equals("annetUdef") && !beregnGradUtnyttingKodeverdi.Equals("ingenKrav"))
                {
                    if (!String.IsNullOrEmpty(beregnGradUtnyttingKodeverdi))
                    {
                        if (Helpers.ObjectIsNullOrEmpty(form.rammebetingelser.arealdisponering))
                        {
                            _validationResult.AddMessage("4397.3.6.5.11", null);
                        }
                        else
                        {
                            double utfyltGradAvUtnytting;
                            double beregnetGradAvUtnytting;
                            if (!form.rammebetingelser.arealdisponering.arealBebyggelseEksisterendeSpecified ||
                                !form.rammebetingelser.arealdisponering.arealBebyggelseNyttSpecified ||
                                !form.rammebetingelser.arealdisponering.arealBebyggelseSomSkalRivesSpecified ||
                                !form.rammebetingelser.arealdisponering.parkeringsarealTerrengSpecified ||
                                !form.rammebetingelser.arealdisponering.beregnetGradAvUtnyttingSpecified
                            )
                            {
                                _validationResult.AddMessage("4397.2.6.5", new[] { beregnGradUtnyttingKodeverdi });
                            }
                            else
                            {
                                //Krav om at regnestykke er riktig
                                //Sjekke på negative verdier
                                if (form.rammebetingelser.arealdisponering.arealBebyggelseEksisterende.Value < 0 ||
                                    form.rammebetingelser.arealdisponering.arealBebyggelseNytt.Value < 0 ||
                                    form.rammebetingelser.arealdisponering.arealBebyggelseSomSkalRives.Value < 0 ||
                                    form.rammebetingelser.arealdisponering.parkeringsarealTerreng.Value < 0 ||
                                    form.rammebetingelser.arealdisponering.beregnetGradAvUtnytting.Value < 0
                                )
                                {
                                    if (form.rammebetingelser.arealdisponering.arealBebyggelseEksisterende.Value < 0)
                                        _validationResult.AddMessage("4397.2.6.5.2", null);

                                    if (form.rammebetingelser.arealdisponering.arealBebyggelseNytt.Value < 0)
                                        _validationResult.AddMessage("4397.2.6.5.3", null);

                                    if (form.rammebetingelser.arealdisponering.arealBebyggelseSomSkalRives.Value < 0)
                                        _validationResult.AddMessage("4397.2.6.5.4", null);

                                    if (form.rammebetingelser.arealdisponering.parkeringsarealTerreng.Value < 0)
                                        _validationResult.AddMessage("4397.2.6.5.5", null);

                                    if (form.rammebetingelser.arealdisponering.beregnetGradAvUtnytting.Value < 0)
                                        _validationResult.AddMessage("4397.2.6.5.6", null);
                                }
                                else
                                {
                                    utfyltGradAvUtnytting = form.rammebetingelser.arealdisponering.beregnetGradAvUtnytting.Value;
                                    beregnetGradAvUtnytting = ValidationHelper.BeregnetGradAvUtnyttingAreal(
                                        form.rammebetingelser.arealdisponering.arealBebyggelseEksisterende.Value,
                                        form.rammebetingelser.arealdisponering.arealBebyggelseSomSkalRives.Value,
                                        form.rammebetingelser.arealdisponering.arealBebyggelseNytt.Value,
                                        form.rammebetingelser.arealdisponering.parkeringsarealTerreng.Value);

                                    switch (beregnGradUtnyttingKodeverdi)
                                    {
                                        case "BYA":
                                        case "BRA":
                                        case "T-BRA":
                                            if (!ValidationHelper.UtfyltOgBeregnetArealErDetSamme(utfyltGradAvUtnytting, beregnetGradAvUtnytting))
                                                _validationResult.AddMessage("4397.2.6.5.9", new[] { beregnGradUtnyttingKodeverdi, utfyltGradAvUtnytting.ToString("##.##"), beregnetGradAvUtnytting.ToString("##.##") });

                                            break;

                                        case "%BYA":
                                        case "%BRA":
                                        case "%TU":
                                            if (!form.rammebetingelser.arealdisponering.tomtearealBeregnetSpecified)
                                            {
                                                _validationResult.AddMessage("4397.2.6.5.1", null);
                                            }
                                            else
                                            {
                                                if (form.rammebetingelser.arealdisponering.tomtearealBeregnet.Value == 0)
                                                {
                                                    _validationResult.AddMessage("4397.2.6.5.8", null);
                                                }
                                                else if (form.rammebetingelser.arealdisponering.tomtearealBeregnet.Value < 0)
                                                {
                                                    _validationResult.AddMessage("4397.2.6.5.7", null);
                                                }
                                                else
                                                {
                                                    var porcentGradAvUtnyttingBeregnt = beregnetGradAvUtnytting / form.rammebetingelser.arealdisponering.tomtearealBeregnet.Value * 100;
                                                    if (!ValidationHelper.UtfyltOgBeregnetArealErDetSamme(utfyltGradAvUtnytting, porcentGradAvUtnyttingBeregnt))
                                                    {
                                                        _validationResult.AddMessage("4397.2.6.5.10", new[] { beregnGradUtnyttingKodeverdi, utfyltGradAvUtnytting.ToString("##.##"), porcentGradAvUtnyttingBeregnt.ToString("##.##") });
                                                    }
                                                }
                                            }
                                            break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        internal void KulturminneValidering(RammetillatelseType form, List<string> formSetElements)
        {
            if (Helpers.ObjectIsNullOrEmpty(form.rammebetingelser?.generelleVilkaar) || !form.rammebetingelser.generelleVilkaar.beroererTidligere1850.HasValue)
                _validationResult.AddMessage("4397.2.6.3.1", null);
            else
            {
                if (form.rammebetingelser.generelleVilkaar.beroererTidligere1850.GetValueOrDefault())
                {
                    if (formSetElements == null || !formSetElements.Contains("UttalelseKulturminnemyndighet"))
                    {
                        _validationResult.AddMessage("4397.2.6.3.1.1", null);
                    }
                }
            }
        }

        internal void ArbeidsplasserValidering(RammetillatelseType form, List<string> formSetElements)
        {
            var arbeidsplasserAtachs = new[]
            {
                "SamtykkeArbeidstilsynet",
                //"Søknad om Arbeidstilsynets samtykke",
                //"Søknad om arbeidstilsynets samtykke"
            };

            if (Helpers.ObjectIsNullOrEmpty(form.rammebetingelser.generelleVilkaar) || !form.rammebetingelser.generelleVilkaar.beroererArbeidsplasser.HasValue)
            {
                _validationResult.AddMessage("4397.2.6.3.2.1", null);
            }
            else
            {
                if (form.rammebetingelser.generelleVilkaar.beroererArbeidsplasser.GetValueOrDefault())
                {
                    bool hasAnyAttachments = formSetElements?.Select(x => x)
                        .Intersect(arbeidsplasserAtachs)
                        .Any() ?? false;
                    if (!hasAnyAttachments)
                    {
                        _validationResult.AddMessage("4397.2.6.3.2", null);
                    }
                }
            }
        }

        internal void GenerelleVilkaarValidering(RammetillatelseType form)
        {
            if (Helpers.ObjectIsNullOrEmpty(form.rammebetingelser?.generelleVilkaar) || !form.rammebetingelser.generelleVilkaar.norskSvenskDansk.HasValue)
                _validationResult.AddMessage("4397.2.6.3.3", null);

            if (Helpers.ObjectIsNullOrEmpty(form.rammebetingelser?.generelleVilkaar) || !form.rammebetingelser.generelleVilkaar.utarbeideAvfallsplan.HasValue)
                _validationResult.AddMessage("4397.2.6.3.4", null);
        }

        internal void VannforsyningValidering(RammetillatelseType form)
        {
            //Hvis tiltakstype krever vannforsyning
            if (Helpers.ObjectIsNullOrEmpty(form.rammebetingelser?.vannforsyning))
            {
                _validationResult.AddMessage("4397.2.6.6", null);
            }
            else
            {
                if (Helpers.ObjectIsNullOrEmpty(form.rammebetingelser.vannforsyning.tilknytningstype?.kodeverdi))
                {
                    _validationResult.AddMessage("4397.2.6.6.1.1", null);
                }
                else
                {
                    if (!_codeListService.IsCodeValidInCodeList("vanntilknytning", form.rammebetingelser.vannforsyning.tilknytningstype?.kodeverdi))
                    {
                        _validationResult.AddMessage("4397.2.6.6.1", new[] { form.rammebetingelser.vannforsyning.tilknytningstype?.kodeverdi });
                    }
                    else
                    {
                        if (Helpers.ObjectIsNullOrEmpty(form.rammebetingelser.vannforsyning.tilknytningstype?.kodebeskrivelse))
                        {
                            _validationResult.AddMessage("4397.2.6.6.1.2", new[] { form.rammebetingelser.vannforsyning.tilknytningstype?.kodeverdi });
                        }
                        else
                        {
                            if (!_codeListService.IsCodelistLabelValid("vanntilknytning", form.rammebetingelser.vannforsyning.tilknytningstype.kodeverdi, form.rammebetingelser.vannforsyning.tilknytningstype.kodebeskrivelse))
                                _validationResult.AddMessage("4397.2.6.6.1.3", new[] { form.rammebetingelser.vannforsyning.tilknytningstype.kodebeskrivelse });
                        }

                        if (form.rammebetingelser.vannforsyning.tilknytningstype.kodeverdi == "AnnenPrivatInnlagt")
                        {
                            if (String.IsNullOrEmpty(form.rammebetingelser.vannforsyning.beskrivelse))
                            {
                                _validationResult.AddMessage("4397.2.6.6.2", null);
                            }
                        }

                        if (form.rammebetingelser.vannforsyning.tilknytningstype.kodeverdi != "BerorerIkkeVannverk" && form.rammebetingelser.vannforsyning.tilknytningstype.kodeverdi != "AnnenPrivatIkkeInnlagt")
                        {
                            if (Helpers.ObjectIsNullOrEmpty(form.rammebetingelser?.vannforsyning?.krysserVannforsyningAnnensGrunn))
                            {
                                _validationResult.AddMessage("4397.3.6.7.8", null);
                            }
                            else
                            {
                                if (form.rammebetingelser.vannforsyning.krysserVannforsyningAnnensGrunn.GetValueOrDefault())
                                {
                                    if (form.rammebetingelser.vannforsyning.tinglystErklaering.HasValue == false)
                                        _validationResult.AddMessage("4397.2.6.7.8.1", null);
                                }
                            }
                        }
                    }
                }
            }
        }

        internal void AvloepValidering(RammetillatelseType form)
        {
            //Hvis tiltakstype krever avlop
            if (Helpers.ObjectIsNullOrEmpty(form.rammebetingelser?.avloep))
            {
                _validationResult.AddMessage("4397.2.6.7", null);
            }
            else
            {
                if (Helpers.ObjectIsNullOrEmpty(form.rammebetingelser.avloep.tilknytningstype?.kodeverdi))
                {
                    _validationResult.AddMessage("4397.2.6.7.1.1", null);
                }
                else
                {
                    if (!_codeListService.IsCodeValidInCodeList("avlopstilknytning", form.rammebetingelser.avloep.tilknytningstype?.kodeverdi))
                    {
                        _validationResult.AddMessage("4397.2.6.7.1", new[] { form.rammebetingelser.avloep.tilknytningstype?.kodeverdi });
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(form.rammebetingelser.avloep.tilknytningstype?.kodebeskrivelse))
                        {
                            _validationResult.AddMessage("4397.2.6.7.2", null);
                        }
                        else
                        {
                            if (!_codeListService.IsCodelistLabelValid("avlopstilknytning", form.rammebetingelser.avloep.tilknytningstype?.kodeverdi, form.rammebetingelser.avloep.tilknytningstype?.kodebeskrivelse))
                                _validationResult.AddMessage("4397.2.6.7.2.1", new[] { form.rammebetingelser.avloep.tilknytningstype?.kodebeskrivelse });
                        }

                        if (form.rammebetingelser.avloep.tilknytningstype.kodeverdi == "PrivatKloakk")
                        {
                            if (form.rammebetingelser.avloep.installereVannklosett.HasValue == false)
                                _validationResult.AddMessage("4397.2.6.7.3", null);

                            if (form.rammebetingelser.avloep.utslippstillatelse.HasValue == false)
                                _validationResult.AddMessage("4397.2.6.7.4", null);
                        }
                    }
                }

                if (Helpers.ObjectIsNullOrEmpty(form.rammebetingelser?.avloep?.krysserAvloepAnnensGrunn))
                {
                    var tilknytningstypeKodeverdi = form.rammebetingelser?.avloep?.tilknytningstype?.kodeverdi;
                    if (tilknytningstypeKodeverdi == null || !tilknytningstypeKodeverdi.Equals("IkkeAvlop", StringComparison.CurrentCultureIgnoreCase))
                    {
                        _validationResult.AddMessage("4397.3.6.7.5", null);
                    }
                }
                else
                {
                    if (form.rammebetingelser.avloep.krysserAvloepAnnensGrunn.GetValueOrDefault())
                    {
                        if (form.rammebetingelser.avloep.tinglystErklaering.HasValue == false)
                            _validationResult.AddMessage("4397.2.6.7.5.1", null);
                    }
                }
            }

            if (Helpers.ObjectIsNullOrEmpty(form.rammebetingelser?.avloep) || !form.rammebetingelser.avloep.overvannTerreng.HasValue && !form.rammebetingelser.avloep.overvannAvloepssystem.HasValue)
            {
                _validationResult.AddMessage("4397.2.6.7.6", null);
            }
        }

        internal void KravTilByggegrunnValidering(RammetillatelseType form, List<string> formSetElements)
        {
            if (Helpers.ObjectIsNullOrEmpty(form.rammebetingelser?.kravTilByggegrunn) || !form.rammebetingelser.kravTilByggegrunn.flomutsattOmraade.HasValue)
            {
                _validationResult.AddMessage("4397.2.6.8.1", null);
            }
            else
            {
                if (form.rammebetingelser.kravTilByggegrunn.flomutsattOmraade.GetValueOrDefault())
                {
                    if (!form.rammebetingelser.kravTilByggegrunn.f1.GetValueOrDefault() &&
                        !form.rammebetingelser.kravTilByggegrunn.f2.GetValueOrDefault() &&
                        !form.rammebetingelser.kravTilByggegrunn.f3.GetValueOrDefault())
                    {
                        _validationResult.AddMessage("4397.2.6.8.1.1", null);
                    }

                    if (formSetElements == null || !formSetElements.Contains("RedegjoerelseSkredOgFlom"))
                    {
                        _validationResult.AddMessage("4397.2.6.8.1.2", null);
                    }
                }
            }

            if (Helpers.ObjectIsNullOrEmpty(form.rammebetingelser?.kravTilByggegrunn) || !form.rammebetingelser.kravTilByggegrunn.skredutsattOmraade.HasValue)
            {
                _validationResult.AddMessage("4397.2.6.8.2", null);
            }
            else
            {
                if (form.rammebetingelser.kravTilByggegrunn.skredutsattOmraade.GetValueOrDefault())
                {
                    if (!form.rammebetingelser.kravTilByggegrunn.s1.GetValueOrDefault() &&
                        !form.rammebetingelser.kravTilByggegrunn.s2.GetValueOrDefault() &&
                        !form.rammebetingelser.kravTilByggegrunn.s3.GetValueOrDefault())
                    {
                        _validationResult.AddMessage("4397.2.6.8.2.1", null);
                    }
                    if (formSetElements == null || !formSetElements.Contains("RedegjoerelseSkredOgFlom"))
                    {
                        _validationResult.AddMessage("4397.2.6.8.2.2", null);
                    }
                }
            }

            if (Helpers.ObjectIsNullOrEmpty(form.rammebetingelser?.kravTilByggegrunn) || !form.rammebetingelser.kravTilByggegrunn.miljoeforhold.HasValue)
            {
                _validationResult.AddMessage("4397.2.6.8.3", null);
            }
            else
            {
                if (form.rammebetingelser.kravTilByggegrunn.miljoeforhold.GetValueOrDefault())
                {
                    if (formSetElements == null || !formSetElements.Contains("RedegjoerelseAndreNaturMiljoeforhold"))
                    {
                        _validationResult.AddMessage("4397.2.6.8.3.1", null);
                    }
                }
            }
        }

        internal void AndrePlanerValidering(RammetillatelseType form)
        {
            if (!Helpers.ObjectIsNullOrEmpty(form.rammebetingelser?.plan?.andrePlaner))
            {
                foreach (AndrePlanerType andrePlanerType in form.rammebetingelser.plan.andrePlaner)
                {
                    if (string.IsNullOrEmpty(andrePlanerType?.plantype?.kodeverdi))
                    {
                        _validationResult.AddMessage("4397.2.6.9.2", null);
                    }
                    else
                    {
                        if (!_codeListService.IsCodeValidInCodeList("plantype", andrePlanerType.plantype?.kodeverdi))
                        {
                            _validationResult.AddMessage("4397.2.6.9", null);
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(andrePlanerType.navn))
                            {
                                _validationResult.AddMessage("4397.2.6.9.1", null);
                            }
                        }
                    }
                }
            }
        }

        internal void PlasseringValidering(RammetillatelseType form, List<string> formSetElements)
        {
            //tiltakstyper som krever informasjon om plassering
            if (Helpers.ObjectIsNullOrEmpty(form.rammebetingelser.plassering) || form.rammebetingelser.plassering.konfliktVannOgAvloep.HasValue == false)
            {
                _validationResult.AddMessage("4397.2.6.10", null);
            }

            if (Helpers.ObjectIsNullOrEmpty(form.rammebetingelser.plassering) || form.rammebetingelser.plassering.konfliktHoeyspentkraftlinje.HasValue == false)
            {
                _validationResult.AddMessage("4397.2.6.10.1", null);
            }

            //Hvis krav til plassering
            if (form.rammebetingelser.plassering != null)
            {
                if (form.rammebetingelser.plassering.konfliktHoeyspentkraftlinje.GetValueOrDefault())
                {
                    if (formSetElements == null || !formSetElements.Contains("AvklaringHoyspent"))
                    {
                        _validationResult.AddMessage("4397.2.6.10.2", null);
                    }
                }

                if (form.rammebetingelser.plassering.konfliktVannOgAvloep.GetValueOrDefault())
                {
                    if (formSetElements == null || !formSetElements.Any(a => a.Equals("AvklaringVA")))
                    {
                        _validationResult.AddMessage("4397.2.6.10.3", null);
                    }
                }
            }
        }

        internal void DispensasjonValidering(RammetillatelseType form, List<string> formSetElements)
        {
            if (!Helpers.ObjectIsNullOrEmpty(form.dispensasjon))
            {
                foreach (var disp in form.dispensasjon)
                {
                    if (Helpers.ObjectIsNullOrEmpty(disp.dispensasjonstype))
                    {
                        _validationResult.AddMessage("4397.2.7", null);
                    }
                    else
                    {
                        if (!_codeListService.IsCodeValidInCodeList("dispensasjonstype", disp.dispensasjonstype.kodeverdi))
                        {
                            _validationResult.AddMessage("4397.2.7.1", new[] { disp.dispensasjonstype.kodeverdi });
                        }
                        else
                        {
                            if (!formSetElements.Any(f => f.Equals("Dispensasjonssoeknad")))
                            {
                                if (String.IsNullOrEmpty(disp.beskrivelse))
                                    _validationResult.AddMessage("4397.2.7.2", null);
                                if (String.IsNullOrEmpty(disp.begrunnelse))
                                    _validationResult.AddMessage("4397.2.7.3", null);
                            }
                        }
                    }
                }
            }
        }

        internal void MetadataValidering(RammetillatelseType form)
        {
            if (string.IsNullOrEmpty(form.metadata?.fraSluttbrukersystem))
                _validationResult.AddMessage("4397.2.8", null);
        }

        internal void VarslingValidering(RammetillatelseType form, List<string> formSetElements)
        {
            if (Helpers.ObjectIsNullOrEmpty(form.varsling))
            {
                _validationResult.AddMessage("4397.2.9", null);
            }
            else
            {
                if (form.varsling.fritattFraNabovarsling.HasValue == false)
                {
                    _validationResult.AddMessage("4397.3.9.4", null);
                }
                else
                {
                    if (!form.varsling.fritattFraNabovarsling.GetValueOrDefault())
                    {
                        if (formSetElements == null || !formSetElements.Any(a => a.Equals("Gjenpart nabovarsel")) && !formSetElements.Any(a => a.Equals("GjenpartNabovarsel")) && !formSetElements.Any(a => a.Equals("Opplysninger gitt i nabovarsel")) && !formSetElements.Any(a => a.Equals("Nabovarsel")))
                        {
                            _validationResult.AddMessage("4397.2.9.1", null);
                        }
                        if (formSetElements == null || !formSetElements.Any(a => a.Equals("KvitteringNabovarsel")) && !formSetElements.Any(a => a.Equals("Kvittering for nabovarsel")))
                        {
                            _validationResult.AddMessage("4397.2.9.2", null);
                        }

                        if (!form.varsling.foreliggerMerknader.HasValue)
                        {
                            _validationResult.AddMessage("4397.3.9.3", null);
                        }
                        else
                        {
                            if (form.varsling.foreliggerMerknader.GetValueOrDefault())
                            {
                                if (String.IsNullOrEmpty(form.varsling.antallMerknader))
                                {
                                    _validationResult.AddMessage("4397.2.9.3.1", null);
                                }
                                if (String.IsNullOrEmpty(form.varsling.vurderingAvMerknader) && (formSetElements == null || !formSetElements.Any(a => a.Equals("KommentarNabomerknader"))))
                                {
                                    _validationResult.AddMessage("4397.2.9.3.2", null);
                                }
                                if (formSetElements == null || !formSetElements.Contains("Nabomerknader"))
                                {
                                    _validationResult.AddMessage("4397.2.9.3.3", null);
                                }
                            }
                        }
                    }
                }
            }
        }

        internal void FakturamottakerValidation(RammetillatelseType form)
        {
            if (Helpers.ObjectIsNullOrEmpty(form.fakturamottaker))
            {
                var isTiltakshaverPrivatperson = form.tiltakshaver.partstype.kodeverdi != null && form.tiltakshaver.partstype.kodeverdi == "Privatperson";
                if (!isTiltakshaverPrivatperson)
                {
                    _validationResult.AddMessage("4397.3.10", null);
                }
            }
            else
            {
                var booleans = new[]
                {
                    form.fakturamottaker.ehfFaktura.GetValueOrDefault(),
                    form.fakturamottaker.fakturaPapir.GetValueOrDefault(),
                };
                var trueCount = booleans.Count(c => c);
                if (trueCount != 1)
                {
                    _validationResult.AddMessage("4397.3.10.4", null);
                }
                else
                {
                    if (form.fakturamottaker.ehfFaktura.GetValueOrDefault(false))
                    {
                        if (string.IsNullOrEmpty(form.fakturamottaker?.organisasjonsnummer))
                            _validationResult.AddMessage("4397.3.10.1", null);
                    }
                    if (string.IsNullOrEmpty(form.fakturamottaker?.navn))
                        _validationResult.AddMessage("4397.3.10.2", null);

                    if (form.fakturamottaker.fakturaPapir.GetValueOrDefault())
                    {
                        if (Helpers.ObjectIsNullOrEmpty(form.fakturamottaker?.adresse))
                        {
                            _validationResult.AddMessage("4397.3.10.3", null);
                        }
                        else
                        {
                            if (String.IsNullOrEmpty(form.fakturamottaker.adresse.adresselinje1))
                                _validationResult.AddMessage("4397.3.10.3.1", null);
                            if (string.IsNullOrEmpty(form.fakturamottaker.adresse.postnr))
                                _validationResult.AddMessage("4397.3.10.3.2", null);
                            if (string.IsNullOrEmpty(form.fakturamottaker.adresse.poststed))
                                _validationResult.AddMessage("4397.3.10.3.3", null);
                        }
                    }
                }
            }
        }

        internal void AnsvarsrettSoekerValidation(RammetillatelseType form, List<string> formSetElements)
        {
            if (Helpers.ObjectIsNullOrEmpty(form.ansvarsrettSoeker))
            {
                _validationResult.AddMessage("4397.3.11", null);
            }
            else
            {
                if (Helpers.ObjectIsNullOrEmpty(form.ansvarsrettSoeker.tiltaksklasse))
                {
                    _validationResult.AddMessage("4397.3.11.1", null);
                }
                else
                {
                    var tiltaksklasse = form.ansvarsrettSoeker?.tiltaksklasse?.kodeverdi;
                    if (!_codeListService.IsCodeValidInCodeList("tiltaksklasse", tiltaksklasse))
                    {
                        _validationResult.AddMessage("4397.3.11.2", new[] { tiltaksklasse });
                    }
                    else
                    {
                        var erklaeringAnsvar = new List<string>()
                        {
                            "Erklæring om ansvarsrett",
                            "ErklaeringAnsvarsrett",
                        };
                    }
                }

                if (!form.ansvarsrettSoeker.harSentralGodkjenning.HasValue)
                    _validationResult.AddMessage("4397.3.11.3", null);
                if (!form.ansvarsrettSoeker.erklaeringAnsvar.GetValueOrDefault(false))
                    _validationResult.AddMessage("4397.3.11.4", null);
            }
        }

        internal void AnsvarligSokerValidation(RammetillatelseType form)
        {
            using (var logger = new PerfTimerLogger())
            {
                if (Helpers.ObjectIsNullOrEmpty(form.ansvarligSoeker))
                {
                    _validationResult.AddMessage("4397.1.12", null);
                }
                else
                {
                    if (Helpers.ObjectIsNullOrEmpty(form.ansvarligSoeker.partstype?.kodeverdi))
                    {
                        _validationResult.AddMessage("4397.1.12.1", null);
                    }
                    else
                    {
                        if (!_codeListService.IsCodelistValid("Partstype", form.ansvarligSoeker.partstype?.kodeverdi))
                        {
                            _validationResult.AddMessage("4397.1.12.2", new[] { form.ansvarligSoeker.partstype?.kodeverdi });
                        }
                        else
                        {
                            if (form.ansvarligSoeker.partstype?.kodeverdi == "Privatperson")
                            {
                                var foedselsnummerValidation = GeneralValidations.Validate_foedselsnummer(form.ansvarligSoeker.foedselsnummer);
                                switch (foedselsnummerValidation)
                                {
                                    case GeneralValidations.FoedselnumerValidation.Empty:
                                        _validationResult.AddMessage("4397.3.12.3", null);
                                        break;

                                    case GeneralValidations.FoedselnumerValidation.Invalid:
                                        _validationResult.AddMessage("4397.3.12.3.1", null);
                                        break;

                                    case GeneralValidations.FoedselnumerValidation.InvalidDigitsControl:
                                        _validationResult.AddMessage("4397.3.12.3.2", null);
                                        break;

                                    case GeneralValidations.FoedselnumerValidation.InvalidEncryption:
                                        _validationResult.AddMessage("4397.3.12.3.3", null);
                                        break;
                                }
                            }
                            else
                            {
                                var organisasjonsnummerValidation = GeneralValidations.Validate_OrgnummerEnum(form.ansvarligSoeker.organisasjonsnummer);
                                switch (organisasjonsnummerValidation)
                                {
                                    case GeneralValidations.OrganisasjonsnummerValidation.Empty:
                                        _validationResult.AddMessage("4397.2.12.4", null);
                                        break;

                                    case GeneralValidations.OrganisasjonsnummerValidation.Invalid:
                                        _validationResult.AddMessage("4397.2.12.4.1", new[] { form.ansvarligSoeker.organisasjonsnummer });
                                        break;

                                    case GeneralValidations.OrganisasjonsnummerValidation.InvalidDigitsControl:
                                        _validationResult.AddMessage("4397.3.12.4.2", new[] { form.ansvarligSoeker.organisasjonsnummer });
                                        break;
                                }

                                if (Helpers.ObjectIsNullOrEmpty(form.ansvarligSoeker.kontaktperson))
                                {
                                    _validationResult.AddMessage("4397.2.12.4.3", null);
                                }
                                else
                                {
                                    if (string.IsNullOrEmpty(form.ansvarligSoeker.kontaktperson.navn))
                                        _validationResult.AddMessage("4397.3.12.4.3.1", null);
                                    if (string.IsNullOrEmpty(form.ansvarligSoeker.kontaktperson.telefonnummer) && string.IsNullOrEmpty(form.ansvarligSoeker.kontaktperson.mobilnummer))
                                    {
                                        _validationResult.AddMessage("4397.3.12.4.3.2", null);
                                    }
                                    else
                                    {
                                        if (!string.IsNullOrEmpty(form.ansvarligSoeker.kontaktperson.telefonnummer))
                                        {
                                            var telefonNumber = form.ansvarligSoeker.kontaktperson.telefonnummer;
                                            var isValidTelefonNumber = telefonNumber.All(c => "+0123456789".Contains(c));
                                            if (!isValidTelefonNumber)
                                            {
                                                _validationResult.AddMessage("4397.3.12.4.3.3", null);
                                            }
                                        }
                                        if (!string.IsNullOrEmpty(form.ansvarligSoeker.kontaktperson.mobilnummer))
                                        {
                                            var mobilNummer = form.ansvarligSoeker.kontaktperson.mobilnummer;
                                            var isValidmobilnummer = mobilNummer.All(c => "+0123456789".Contains(c));
                                            if (!isValidmobilnummer)
                                            {
                                                _validationResult.AddMessage("4397.3.12.4.3.4", null);
                                            }
                                        }
                                    }

                                    if (string.IsNullOrEmpty(form.ansvarligSoeker.kontaktperson.epost))
                                        _validationResult.AddMessage("4397.3.12.4.3.5", null);
                                }
                            }

                            //Adresse
                            if (Helpers.ObjectIsNullOrEmpty(form.ansvarligSoeker.adresse))
                            {
                                _validationResult.AddMessage("4397.1.12.5", null);
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(form.ansvarligSoeker.adresse.adresselinje1))
                                {
                                    _validationResult.AddMessage("4397.3.12.5.1", null);
                                }
                                else
                                {
                                    if (!CountryCodeHandler.IsCountryNorway(form.ansvarligSoeker.adresse.landkode))
                                    {
                                        if (!CountryCodeHandler.VerifyCountryCode(form.ansvarligSoeker.adresse.landkode))
                                        {
                                            _validationResult.AddMessage("4397.3.12.5.2", null);
                                        }
                                    }
                                    else
                                    {
                                        var postNr = form.ansvarligSoeker.adresse.postnr;
                                        var landkode = form.ansvarligSoeker.adresse.landkode;

                                        if (string.IsNullOrEmpty(form.ansvarligSoeker.adresse.postnr))
                                        {
                                            _validationResult.AddMessage("4397.1.12.5.3", null);
                                        }
                                        else
                                        {
                                            if (!GeneralValidations.postNumberMatch(form.ansvarligSoeker.adresse.postnr).Success)
                                            {
                                                _validationResult.AddMessage("4397.3.12.5.3.1", new[] { postNr });
                                            }
                                            else
                                            {
                                                var postnrValidation = _postalCodeProvider.ValidatePostnr(postNr, landkode);
                                                if (!Helpers.ObjectIsNullOrEmpty(postnrValidation))
                                                {
                                                    if (!postnrValidation.Valid)
                                                    {
                                                        _validationResult.AddMessage("4397.3.12.5.3.2", null);
                                                    }
                                                    else
                                                    {
                                                        if (!postnrValidation.Result.Equals(form.ansvarligSoeker.adresse.poststed, StringComparison.CurrentCultureIgnoreCase))
                                                        {
                                                            _validationResult.AddMessage("4397.3.12.5.3.3", new[] { postNr, form.ansvarligSoeker.adresse.poststed, postnrValidation.Result });
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    _validationResult.AddMessage("4397.3.12.5.3.4", null);
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            if (string.IsNullOrEmpty(form.ansvarligSoeker.navn))
                                _validationResult.AddMessage("4397.3.12.6", null);
                            if (string.IsNullOrEmpty(form.ansvarligSoeker.telefonnummer) && string.IsNullOrEmpty(form.ansvarligSoeker.mobilnummer))
                            {
                                _validationResult.AddMessage("4397.3.12.7", null);
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(form.ansvarligSoeker.telefonnummer))
                                {
                                    var telefonNumber = form.ansvarligSoeker.telefonnummer;
                                    var isValidTelefonNumber = telefonNumber.All(c => "+0123456789".Contains(c));
                                    if (!isValidTelefonNumber)
                                    {
                                        _validationResult.AddMessage("4397.3.12.8", null);
                                    }
                                }
                                if (!string.IsNullOrEmpty(form.ansvarligSoeker.mobilnummer))
                                {
                                    var mobilNummer = form.ansvarligSoeker.mobilnummer;
                                    var isValidmobilnummer = mobilNummer.All(c => "+0123456789".Contains(c));
                                    if (!isValidmobilnummer)
                                    {
                                        _validationResult.AddMessage("4397.3.12.9", null);
                                    }
                                }
                            }

                            if (string.IsNullOrEmpty(form.ansvarligSoeker.epost))
                                _validationResult.AddMessage("4397.3.12.10", null);
                        }
                    }
                }
            }
        }

        //Vedlegg validering
        internal void SituasjonsplanValidering(List<string> formSetElements)
        {
            if (formSetElements == null || !formSetElements.Contains("Situasjonsplan"))
                _validationResult.AddMessage("4397.2.13.1", null);
        }

        internal void TegningValidering(List<string> formSetElements)
        {
            var tengningen = new List<string>()
            {
                "TegningEksisterendeSnitt",
                "TegningNyttSnitt",
                "TegningEksisterendePlan",
                "TegningNyPlan",
                "TegningEksisterendeFasade",
                "TegningNyFasade"
            };

            bool hasDrawing = formSetElements?.Select(x => x)
                                  .Intersect(tengningen)
                                  .Any() ?? false;
            //Minst en tegning
            if (!hasDrawing)
                _validationResult.AddMessage("4397.2.13.2", null);
        }

        internal void PlantegningValidering(List<string> formSetElements)
        {
            if (formSetElements == null || (!formSetElements.Contains("TegningEksisterendePlan") && !formSetElements.Contains("TegningNyPlan")))
                _validationResult.AddMessage("4397.2.13.3", null);
        }

        internal void SnitttegningerValidering(List<string> formSetElements)
        {
            if (formSetElements == null || (!formSetElements.Contains("TegningEksisterendeSnitt") && !formSetElements.Contains("TegningNyttSnitt")))
                _validationResult.AddMessage("4397.2.13.4", null);
        }

        internal void FasadetegningerValidering(List<string> formSetElements)
        {
            if (formSetElements == null || (!formSetElements.Contains("TegningEksisterendeFasade") && !formSetElements.Contains("TegningNyFasade")))
                _validationResult.AddMessage("4397.2.13.5", null);
        }

        internal void AnsvarValidering(RammetillatelseType form, List<string> formSetElements)
        {
            var gjennomforingsplans = new List<string>()
            {
                "Gjennomføringsplan",
                "Gjennomfoeringsplan",
                "GjennomføringsplanV4",
            };
            var ansvarsrettSelvbygger = new List<string>()
            {
                "AnsvarsrettSelvbygger",
                "Søknad om ansvarsrett for selvbygger"
            };

            bool isGjennomforingsplans = formSetElements?.Select(x => x)
                                  .Intersect(gjennomforingsplans)
                                  .Any() ?? false;
            bool isAnsvarsrettSelvbygger = formSetElements?.Select(x => x)
                                  .Intersect(ansvarsrettSelvbygger)
                                  .Any() ?? false;

            if (form.ansvarligSoeker.partstype.kodeverdi != null && form.ansvarligSoeker.partstype.kodeverdi == "Privatperson")
            {
                if (!isGjennomforingsplans && isAnsvarsrettSelvbygger)
                {
                    _validationResult.AddMessage("4397.2.13.7", null);
                }
                if (!isAnsvarsrettSelvbygger)
                {
                    _validationResult.AddMessage("4397.2.13.7.1", null);
                }
            }
            else
            {
                if (!isGjennomforingsplans && !isAnsvarsrettSelvbygger)
                {
                    _validationResult.AddMessage("4397.2.13.6", null);
                }
            }
        }

        internal void MatrikkelopplysningValidering(List<string> formSetElements)
        {
            if (formSetElements == null || !formSetElements.Contains("Matrikkelopplysninger"))
            {
                _validationResult.AddMessage("4397.2.13.8", null);
            }
        }

        internal void ByggesaksBIMValidering(List<string> formSetElements)
        {
            if (formSetElements != null)
            {
                if (formSetElements.Contains("ByggesaksBIM"))
                {
                    if (!formSetElements.Contains("Vedleggsopplysninger"))
                    {
                        _validationResult.AddMessage("4397.2.13.9", null);
                    }
                }
            }
        }
    }
}