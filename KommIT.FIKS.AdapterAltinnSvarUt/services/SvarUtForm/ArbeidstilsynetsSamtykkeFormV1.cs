﻿using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Encryption;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.SvarUtForm;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.SvarUt
{
    public class ArbeidstilsynetsSamtykkeFormV1: IAltinnForm, SvarUtForm
    {
        private const string DataFormatId = "5547";
        private const string DataFormatVersion = "41999";
        private const string SchemaFile = "arbeidstilsynetsSamtykke.xsd";
        private const string Name = "Søknad om arbeidstilsynets samtykke";
        private List<string> FormSetElements;
        public void SetFormSetElements(List<string> attachmentlist)
        {
            FormSetElements = attachmentlist;
        }

        public bool? ValidateAuthenticatedSubmitterVsFormData(string authenticatedSubmitter, ValidationResult validationResult)
        {
            // If reportee = ArbeidstilsynetsSamtykke/tiltakshaver/foedselsnummer OK
            // if reportee = ArbeidstilsynetsSamtykke/tiltakshaver/organisasjonsnummer OK
            // if reportee = ArbeidstilsynetsSamtykke/ansvarligSoeker/organisasjonsnummer + vedlegg OK
            // if reportee = ArbeidstilsynetsSamtykke/ansvarligSoeker/foedselsnummer + vedlegg OK

            var propIds = GetPropertyIdentifiers();
            bool? isValid = null;
            
            bool isTiltakshaverSignatureAttachmentIncluded = FormSetElements.Contains("TiltakshaverSignatur");

            if (authenticatedSubmitter == propIds.TiltakshaversFnr || authenticatedSubmitter == propIds.TiltakshaversOrgnr)
            {
                isValid = true;
            } 
            else if (authenticatedSubmitter == propIds.AnsvarligSokerOrgnr || authenticatedSubmitter == propIds.AnsvarligSokerFnr)
            {
                if (isTiltakshaverSignatureAttachmentIncluded)
                {
                    isValid = true;
                }
                else
                {
                    Helpers.RecordValidationError(validationResult, "Du må sende med vedlegget TiltakshaverSignatur.", "4845.1.10");
                    isValid = false;
                }
            }
            else
            {
                Helpers.RecordErrorAltinnSubmitter(validationResult);
                isValid = false;
            }

            return isValid;
        }

        public void InitiateForm(string formDataAsXml) {
            
            form = Utils.SerializeUtil.DeserializeFromString<no.kxml.skjema.dibk.arbeidstilsynetsSamtykke.ArbeidstilsynetsSamtykkeType>(formDataAsXml);
        }

        private no.kxml.skjema.dibk.arbeidstilsynetsSamtykke.ArbeidstilsynetsSamtykkeType form;

        public PropertyIdentifiers GetPropertyIdentifiers()
        {
            PropertyIdentifiers propertyIdentifiers = new PropertyIdentifiers();

            propertyIdentifiers.Kommunenummer = form.eiendomByggested[0].eiendomsidentifikasjon.kommunenummer;
            propertyIdentifiers.Gaardsnummer = form.eiendomByggested[0].eiendomsidentifikasjon.gaardsnummer;
            propertyIdentifiers.Bruksnummer = form.eiendomByggested[0].eiendomsidentifikasjon.bruksnummer;
            propertyIdentifiers.Festenummer = form.eiendomByggested[0].eiendomsidentifikasjon.festenummer;
            propertyIdentifiers.Seksjonsnummer = form.eiendomByggested[0].eiendomsidentifikasjon.seksjonsnummer;
            propertyIdentifiers.Adresselinje1 = form.eiendomByggested[0].adresse.adresselinje1;
            propertyIdentifiers.Adresselinje2 = "";
            propertyIdentifiers.Adresselinje3 = "";
            propertyIdentifiers.Postnr = form.eiendomByggested[0].adresse.postnr;
            propertyIdentifiers.Poststed = form.eiendomByggested[0].adresse.poststed;
            propertyIdentifiers.Landkode = form.eiendomByggested[0].adresse.landkode;
            propertyIdentifiers.Bygningsnummer = form.eiendomByggested[0].bygningsnummer;
            propertyIdentifiers.Bolignummer = form.eiendomByggested[0].bolignummer;
            propertyIdentifiers.SoeknadSkjemaNavn = Name;
            propertyIdentifiers.FraSluttbrukersystem = form.fraSluttbrukersystem;


            // Tor Oskar tweaked 2.12.2020, TH data was mapped to AS, added TH to PropertyIdentifiers
            propertyIdentifiers.AnsvarligSokerNavn = form.ansvarligSoeker.navn;
            propertyIdentifiers.AnsvarligSokerOrgnr = form.ansvarligSoeker.organisasjonsnummer;
            if (form.ansvarligSoeker.partstype?.kodeverdi == "Privatperson")
                propertyIdentifiers.AnsvarligSokerFnr = Helpers.GetDecryptedFnr(form.ansvarligSoeker.foedselsnummer);

            propertyIdentifiers.TiltakshaversNavn = form.tiltakshaver.navn;
            propertyIdentifiers.TiltakshaversOrgnr = form.tiltakshaver.organisasjonsnummer;

            if (form.tiltakshaver.partstype?.kodeverdi == "Privatperson")
                propertyIdentifiers.TiltakshaversFnr = Helpers.GetDecryptedFnr(form.tiltakshaver.foedselsnummer);
            // End of the Tor Tweak

            if (form.tiltakshaver.adresse != null)
            {
                propertyIdentifiers.AnsvarligSokerAdresselinje1 = form.tiltakshaver.adresse.adresselinje1;
                propertyIdentifiers.AnsvarligSokerPostnr = form.tiltakshaver.adresse.postnr;
                propertyIdentifiers.AnsvarligSokerPoststed = form.tiltakshaver.adresse.poststed;
            }
            return propertyIdentifiers;
        }


        public ValidationResult ValidateData()
        {
            return new ArbeidstilsynetsSamtykkeFormV1Validation().Validate(form, FormSetElements);
        }

        public string GetDataFormatId()
        {
            return DataFormatId;
        }

        public string GetDataFormatVersion()
        {
            return DataFormatVersion;
        }

        public string GetMunicipalityCode()
        {
            return GetPropertyIdentifiers().Kommunenummer;
        }

        public string GetSchemaFile()
        {
            return SchemaFile;
        }

        public string GetName()
        {
            return Name;
        }

    }
}