﻿using DIBK.FBygg.Altinn.MapperCodelist;
using KommIT.FIKS.AdapterAltinnSvarUt.Resources;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Validations;
using no.kxml.skjema.dibk.gjennomforingsplanV4;
using System;
using System.Collections.Generic;
using System.Linq;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.SvarUtForm
{
    public class GjennomforingsplanFormV4Validator
    {
        CodeListService _codeListService = new CodeListService();

        private ValidationResult _validationResult = new ValidationResult();
        private readonly MunicipalityValidator _municipalityValidator;
        public GjennomforingsplanFormV4Validator()
        {
            _municipalityValidator = new MunicipalityValidator();
        }

        public ValidationResult Validate(GjennomfoeringsplanType form, List<string> FormSetElements)
        {
            _validationResult = new ValidationResult();

            EiendomByggestedValidation(form);

            FraSluttbrukersystemErUtfylt(form);

            SaksnummerErUtfyltVedHovedsoeknad(form, FormSetElements);

            AnsvarsomraadeValidation(form);

            AnsvarligSoekerValidation(form);

            return _validationResult;
        }

        internal void SaksnummerErUtfyltVedHovedsoeknad(GjennomfoeringsplanType form, List<string> formSetElements)
        {
            if (form.kommunensSaksnummer == null)
            {
                // Gjennomføringsplan som enda ikke har saksnummer må ha følgende hovedskjema i listen
                if (formSetElements != null && !formSetElements.Any(substring =>
                        substring.Contains("Søknad om rammetillatelse") ||
                        substring.Contains("Søknad om tillatelse i ett trinn") ||
                        substring.Contains("Søknad om igangsettingstillatelse") ||
                        substring.Contains("Søknad om midlertidig brukstillatelse") ||
                        substring.Contains("Søknad om ferdigattest") ||
                        substring.Contains("Søknad om endring av tillatelse")))
                {
                    _validationResult.AddError(
                        "Når gjennomføringsplan sendes som enkeltstående innsending(oppdatering av tidligere gjennomføringsplan) så er det krav om å oppgi kommunens saksnummer. Skjema vedlagt " +
                        String.Join(",", formSetElements), "4398.4.7",
                        "/Gjennomfoeringsplan/kommunensSaksnummer/saksaar");
                }
            }
            else
            {
                if (String.IsNullOrEmpty(form.kommunensSaksnummer.saksaar) ||
                    String.IsNullOrEmpty(form.kommunensSaksnummer.sakssekvensnummer))
                {
                    _validationResult.AddError(
                        "Når gjennomføringsplan sendes som enkeltstående innsending(oppdatering av tidligere gjennomføringsplan) så er det krav om å oppgi kommunens saksnummer.",
                        "4398.4.7", "/Gjennomfoeringsplan/kommunensSaksnummer/saksaar");
                }
            }
        }



        internal void SamsvarserklaeringValidation(AnsvarsomraadeType[] formGjennomfoeringsplan)
        {
            foreach (var ansvarsomraade in formGjennomfoeringsplan)
            {
                var funksjon = ansvarsomraade.funksjon.kodeverdi;

                var midlertidigbrukstillatelse =
                    ansvarsomraade.samsvarKontrollPlanlagtVedMidlertidigBrukstillatelse;
                var ferdigattest = ansvarsomraade.samsvarKontrollPlanlagtVedFerdigattest;
                var ettTrinn = ansvarsomraade.samsvarKontrollPlanlagtVedIgangsettingstillatelse;
                var rammetillatelse = ansvarsomraade.samsvarKontrollPlanlagtVedRammetillatelse;

                var midlertidigbrukstillatelseDato =
                    ansvarsomraade.samsvarKontrollForeliggerVedMidlertidigBrukstillatelse;
                var ferdigattestDato = ansvarsomraade.samsvarKontrollForeliggerVedFerdigattest;
                var ettTrinnDato = ansvarsomraade.samsvarKontrollForeliggerVedIgangsettingstillatelse;
                var rammetillatelseDato = ansvarsomraade.samsvarKontrollForeliggerVedRammetillatelse;

                List<bool> sjekkKryss = new List<bool>();
                List<DateTime?> sjekkDato = new List<DateTime?>();

             
               

                if (funksjon.Equals("SØK"))
                {
                    if(midlertidigbrukstillatelse) sjekkKryss.Add(midlertidigbrukstillatelse);
                    if (ferdigattest) sjekkKryss.Add(ferdigattest);
                    if (ettTrinn) sjekkKryss.Add(ettTrinn);
                    if (rammetillatelse) sjekkKryss.Add(rammetillatelse);

                    if (midlertidigbrukstillatelseDato != null) sjekkDato.Add(midlertidigbrukstillatelseDato);
                    if (ferdigattestDato != null) sjekkDato.Add(ferdigattestDato);
                    if (ettTrinnDato != null) sjekkDato.Add(ettTrinnDato);
                    if (rammetillatelseDato != null) sjekkDato.Add(rammetillatelseDato);

                    if (sjekkKryss.Any(st => st.Equals(true)))
                    {
                        _validationResult.AddError(
                            "Ingen av samsvarserklæring/kontrollerklæring feltene må være utfylt",
                            "", "");
                    }
                    if (sjekkDato.Any(st => st.Value != null))
                    {
                        _validationResult.AddError(
                            "Det må ikke foreligge en dato",
                            "", "");
                    }
                }

                
            }
        }

        

        internal void EiendomByggestedValidation(GjennomfoeringsplanType form)
        {
            if (MinstEnEiendom(form))
            {
                foreach (var eiendom in form.eiendomByggested)
                {
                    GyldigKommuneNrEiendom(form);

                }
               
            }

        }


        internal ValidationResult GetResult() => _validationResult;

        internal void GyldigKommuneNrEiendom(GjennomfoeringsplanType form)
        {
                GeneralValidationResult generalValidationResult =
                    _municipalityValidator.ValidateKommunenummer(form.eiendomByggested.First().eiendomsidentifikasjon
                        .kommunenummer);
                if (generalValidationResult.Status != ValidationStatus.Ok)
                {
                    _validationResult.AddError($"{generalValidationResult.Message}", "4398.1.2",
                        "/Gjennomfoeringsplan/eiendomByggested/eiendom/eiendomsidentifikasjon/kommunenummer");
                }
            
        }

        internal void AnsvarligSoekerValidation(GjennomfoeringsplanType form)
        {

            if (String.IsNullOrEmpty(form.ansvarligSoeker.organisasjonsnummer))
            {
                _validationResult.AddError(
                    "Søkers organisasjonsnummer må være utfylt. Må sendes inn av et foretak (ansvarlig søker)",
                    "4398.1.4", "/Gjennomfoeringsplan/ansvarligSoeker/organisasjonsnummer");
            }

            else
            {
                if (PartstypeVerdiErLovlig(form.ansvarligSoeker.partstype.kodeverdi, "4398.1.4",
                    "/Gjennomfoeringsplan/ansvarligSoeker/partstype/kodeverdi"))
                {
                    if (ErForetakValidation(form.ansvarligSoeker.partstype.kodeverdi, "Ansvarlig søker", "4398.1.4",
                        "/Gjennomfoeringsplan/ansvarligSoeker/partstype/kodeverdi"))
                    {
                        OrgNrValidation(form.ansvarligSoeker.organisasjonsnummer,
                            "/Gjennomfoeringsplan/ansvarligSoeker/organisasjonsnummer");
                    }
                }

            }

            IsFieldNullOrEmpty(form.ansvarligSoeker.navn, "Søkers navn", "4398.1.4", "/Gjennomfoeringsplan/ansvarligSoeker/navn");

            IsFieldNullOrEmpty(form.ansvarligSoeker.adresse.postnr, "Søkers postnummer", "4398.1.4",
                "/Gjennomfoeringsplan/ansvarligSoeker/adresse/postnr");

        }

        private void IsFieldNullOrEmpty(string fieldName, string message, string reference, string xpathField)
        {
            if (String.IsNullOrEmpty(fieldName))
            {
                _validationResult.AddError($"{message} må være utfylt", $"{reference}",
                    $"{xpathField}");
            }
        }

        private void IsFieldNullOrEmpty(bool? fieldName, string message, string reference, string xpathField)
        {
            if (fieldName.HasValue)
            {
                _validationResult.AddError($"{message} må være utfylt", $"{reference}",
                    $"{xpathField}");
            }
        }

        private bool ErForetakValidation(string partstypeKodeverdi, string message, string reference, string xpathField)
        {
            if (partstypeKodeverdi != "Foretak")
            {
                _validationResult.AddError($"{message} må være et foretak", $"{reference}",
                    $"{xpathField}");
            }

            return _validationResult.IsOk();
        }

        private void OrgNrValidation(string orgNr, string xpathField)
        {
            GeneralValidationResult generalValidationResult =
                GeneralValidations.Validate_orgnummer(orgNr);

            if (generalValidationResult.Status != ValidationStatus.Ok)
            {
                _validationResult.AddError($"{generalValidationResult.Message}", "",
                    $"{xpathField}");
            }
           
        }

        private bool PartstypeVerdiErLovlig(string partstypeKode, string reference, string xpathField)
        {
            if (!_codeListService.IsCodelistValid("Partstype", partstypeKode))
            {
                _validationResult.AddError(
                    $"Partstype har ugyldig verdi {partstypeKode}",
                    $"{reference}", $"{xpathField}");
            }

            return _validationResult.IsOk();
        }

        internal bool MinstEnEiendom(GjennomfoeringsplanType form)
        {
            if (form.eiendomByggested == null || !form.eiendomByggested.Any())
                _validationResult.AddError(ValidationError.MinstEnEiendom, "4762.1.1",
                    "/Gjennomfoeringsplan/eiendomByggested/eiendom");

            return (form.eiendomByggested != null || form.eiendomByggested.Any());
        }

        internal void FraSluttbrukersystemErUtfylt(GjennomfoeringsplanType form)
        {
            if (String.IsNullOrEmpty(form.fraSluttbrukersystem))
            {
                _validationResult.AddError(
                    "Må fylle ut fraSluttbrukersystem med samme navn som brukt i registrering for Altinn API",
                    "4398.1.3", "/Gjennomfoeringsplan/fraSluttbrukersystem");
            }
        }


        internal void AnsvarsomraadeValidation(GjennomfoeringsplanType form)
        {
            
            if (MinstEtAnsvarsomraade(form))
            {
                foreach (var ansvarsomraade in form.gjennomfoeringsplan)
                {
                    FunksjonHarLovligVerdi(ansvarsomraade.funksjon.kodeverdi);

                    IsFieldNullOrEmpty(ansvarsomraade.arbeidsomraade, "Beskrivelse av ansvarsområde ", "4398.1.1",
                        "/Gjennomfoeringsplan/ansvarsomraade/arbeidsomraade");
                
                    if (ansvarsomraade.tiltaksklasse != null)
                        TiltaksklasseHarLovligVerdi(ansvarsomraade.tiltaksklasse.kodeverdi);

                    SamsvarserklaeringValidation(form.gjennomfoeringsplan);
                }
            }
            
         }


        private void TiltaksklasseHarLovligVerdi(string tiltaksklasseKodeverdi)
        {
            if (!_codeListService.IsCodelistValid("Tiltaksklasse", tiltaksklasseKodeverdi))
            {
                _validationResult.AddError(
                    $"Tiltaksklasse har ugyldig verdi {tiltaksklasseKodeverdi}",
                    "4398.1.2", "/Gjennomfoeringsplan/ansvarsomraade/tiltaksklasse/kodeverdi");
            }
        }

    

        

        private void FunksjonHarLovligVerdi(string funksjonKodeverdi)
        {
            if (!_codeListService.IsCodelistValid("Funksjon", funksjonKodeverdi))
            {
                _validationResult.AddError(
                    $"Funksjon har ugyldig verdi {funksjonKodeverdi}",
                    "4398.1.2", "/Gjennomfoeringsplan/ansvarsomraade/funksjon/kodeverdi");
            }
        }

        private bool MinstEtAnsvarsomraade(GjennomfoeringsplanType form)
        {
            if (form.gjennomfoeringsplan.Count() == 0 || !form.gjennomfoeringsplan.Any())
            {
                _validationResult.AddError("Minst ett ansvarsområde må være registrert", "4398.1.1",
                    "/Gjennomfoeringsplan/ansvarsomraade");
            }
            return (form.gjennomfoeringsplan.Count() != 0 && form.gjennomfoeringsplan.Any());
        }
    }
}

