﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DIBK.FBygg.Altinn.MapperCodelist;
using KommIT.FIKS.AdapterAltinnSvarUt.Logger;
using KommIT.FIKS.AdapterAltinnSvarUt.Resources;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Checklist;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Matrikkel;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.PostalCode;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Validations;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;
using no.kxml.skjema.dibk.gjennomforingsplanV6;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.SvarUtForm
{
    public class GjennomforingsplanFormV6Validator
    {
        CodeListService _codeListService = new CodeListService();
        private List<string> _tiltakstyperISoeknad = new List<string>();

        private ValidationResult _validationResult = new ValidationResult();
        public IMunicipalityValidator _municipalityValidator { get; set; }
        public IMatrikkelService _matrikkelService { get; set; }
        public IPostalCodeProvider _postalCodeProvider { get; set; }

        public GjennomforingsplanFormV6Validator()
        {
            _municipalityValidator = new MunicipalityValidator();
            _matrikkelService = new MatrikkelService();
            _validationResult = new ValidationResult();
            _postalCodeProvider = new BringPostalCodeProvider();

            string _skjema = "Gjennomfoeringsplan";

            var form = new GjennomfoeringsplanType();

            // fraSluttbrukersystem
            _validationResult.AddRule(_skjema, "4398.1.1", null, "Innhold må være i henhold til informasjonsmodell/xsd for skjema.", null, "ERROR", null);
            _validationResult.AddRule(_skjema, "4398.6.3", null, "Vedlegg må være i henhold til metadata for skjema. Vedleggstype eller underskjema '{0}' er ikke gyldig.", Helpers.GetFullClassPath(() => form), "ERROR", null);
            //

            //Eiendommens--> eiendomsidentifikasjon
            _validationResult.AddRule(_skjema, "4398.1.4", null, "Du må oppgi hvilken eiendom/hvilke eiendommer byggesøknaden gjelder.", Helpers.GetFullClassPath(() => form.eiendomByggested), "ERROR", null);
            _validationResult.AddRule(_skjema, "4398.1.4.1", "1.2", "Kommunenummer må fylles ut for eiendom/byggested.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].eiendomsidentifikasjon.kommunenummer), "ERROR", null);
            _validationResult.AddRule(_skjema, "4398.1.4.2", null, "Kommunenummeret '{0}' for eiendom/byggested finnes ikke i kodelisten. Du kan sjekke riktig kommunenummer på https://register.geonorge.no/sosi-kodelister/inndelinger/inndelingsbase/kommunenummer", Helpers.GetFullClassPath(() => form.eiendomByggested[0].eiendomsidentifikasjon.kommunenummer), "ERROR", null);
            _validationResult.AddRule(_skjema, "4398.1.4.3", null, "Kommunenummer for eiendom/byggested har ugyldig status ({0}). Du kan sjekke status på https://register.geonorge.no/sosi-kodelister/inndelinger/inndelingsbase/kommunenummer", Helpers.GetFullClassPath(() => form.eiendomByggested[0].eiendomsidentifikasjon.kommunenummer), "ERROR", null);//
            _validationResult.AddRule(_skjema, "4398.6.4.4", "1.3", "Gårdsnummer må fylles ut for eiendom/byggested.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].eiendomsidentifikasjon.gaardsnummer), "ERROR", null);
            _validationResult.AddRule(_skjema, "4398.6.4.4.1", "1.4", "Bruksnummer må fylles ut for eiendom/byggested.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].eiendomsidentifikasjon.bruksnummer), "ERROR", null);
            _validationResult.AddRule(_skjema, "4398.6.4.5", null, "Gårdsnummer '{0}' for eiendom/byggested må være '0' eller større.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].eiendomsidentifikasjon.gaardsnummer), "ERROR", null);
            _validationResult.AddRule(_skjema, "4398.6.4.6", null, "Bruksnummer '{0}' for eiendom/byggested må være '0' eller større.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].eiendomsidentifikasjon.bruksnummer), "ERROR", null);
            _validationResult.AddRule(_skjema, "4398.1.4.8", null, "Bygningsnummer for eiendom/byggested må være større enn '0'.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].bygningsnummer), "ERROR", null);
            //Eiendommens --> Adress
            _validationResult.AddRule(_skjema, "4398.1.4.9", null, "Postadresse for eiendom/byggested bør fylles ut.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].adresse), "WARNING", null);
            _validationResult.AddRule(_skjema, "4398.1.4.9.1", "1.8", "Adresselinje 1 for eiendom/byggested bør fylles ut.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].adresse.adresselinje1), "WARNING", null);
            _validationResult.AddRule(_skjema, "4398.1.4.9.2", null, "Du bør også oppgi gatenavn, husnummer og eventuell bokstav for eiendom/byggested. Du kan sjekke riktig adresse på https://seeiendom.no", Helpers.GetFullClassPath(() => form.eiendomByggested[0].adresse.gatenavn), "WARNING", null);//
            _validationResult.AddRule(_skjema, "4398.1.4.10", null, "Kommunenavn bør fylles ut for eiendom/byggested.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].kommunenavn), "WARNING", null);
            _validationResult.AddRule(_skjema, "4398.1.4.11", null, "Når bruksenhetsnummer/bolignummer er fylt ut for eiendom/byggested, må det følge riktig format (for eksempel H0101). Se https://www.kartverket.no/eiendom/adressering/bruksenhetsnummer/", Helpers.GetFullClassPath(() => form.eiendomByggested[0].bolignummer), "ERROR", null);

            //if (_matrikkelFlag)
            //{
            _validationResult.AddRule(_skjema, "4398.1.4.12", null, "Når eiendomsidentifikasjon [{0}-{1}/{2}/{3}/{4}] er oppgitt for eiendom/byggested, bør den være gyldig i matrikkelen. Du kan sjekke riktig informasjon på https://seeiendom.no", Helpers.GetFullClassPath(() => form.eiendomByggested[0].eiendomsidentifikasjon), "WARNING", null);
            _validationResult.AddRule(_skjema, "4398.1.4.13", null, "Eiendomsidentifikasjon [{0}-{1}/{2}/{3}/{4}] for eiendom/byggested ble ikke validert mot matrikkelen. Identifikasjonen kan være riktig, men en teknisk feil gjør at vi ikke kan bekrefte det.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].eiendomsidentifikasjon), "WARNING", null);
            _validationResult.AddRule(_skjema, "4398.1.4.14", null, "Hvis bygningsnummer [{0}] er oppgitt for eiendom/byggested, bør det være gyldig i matrikkelen på aktuelt matrikkelnummer. Du kan sjekke riktig bygningsnummer på https://seeiendom.no", Helpers.GetFullClassPath(() => form.eiendomByggested[0].bygningsnummer), "WARNING", null);
            _validationResult.AddRule(_skjema, "4398.1.4.15", null, "Gateadressen for eiendom/byggested ble ikke validert mot matrikkelen. Gateadressen kan være riktig, men en teknisk feil gjør at vi ikke kan bekrefte det. Du kan sjekke adresse på https://seeiendom.no", Helpers.GetFullClassPath(() => form.eiendomByggested[0].adresse.gatenavn), "WARNING", null);
            _validationResult.AddRule(_skjema, "4398.1.4.15.1", null, "Du har oppgitt feil gateadresse for eiendom/byggested. Du kan sjekke riktig adresse på https://seeiendom.no", Helpers.GetFullClassPath(() => form.eiendomByggested[0].adresse.gatenavn), "WARNING", null);
            //}

            //Gjennomfoeringsplan/kommunensSaksnummer
            _validationResult.AddRule(_skjema, "4398.4.5.1", null, "Når gjennomføringsplanen sendes som enkeltstående innsending (oppdatering av tidligere gjennomføringsplan), må du oppgi kommunens saksnummer.", Helpers.GetFullClassPath(() => form.kommunensSaksnummer), "ERROR", null);
            _validationResult.AddRule(_skjema, "4398.4.5.1.1", null, "Når gjennomføringsplanen sendes som enkeltstående innsending (oppdatering av tidligere gjennomføringsplan), må du oppgi kommunens saksnummer med saksår.", Helpers.GetFullClassPath(() => form.kommunensSaksnummer.saksaar), "ERROR", null);
            _validationResult.AddRule(_skjema, "4398.4.5.1.2", null, "Når gjennomføringsplanen sendes som enkeltstående innsending (oppdatering av tidligere gjennomføringsplan), må du oppgi kommunens saksnummer med sekvensnummer.", Helpers.GetFullClassPath(() => form.kommunensSaksnummer.sakssekvensnummer), "ERROR", null);

            // ansvarligSoeker
            _validationResult.AddRule(_skjema, "4398.1.6", null, "Informasjon om ansvarlig søker må fylles ut.", Helpers.GetFullClassPath(() => form.ansvarligSoeker), "ERROR", null);
            _validationResult.AddRule(_skjema, "4398.1.6.1", null, "Kodeverdien for 'partstype' til ansvarlig søker må fylles ut.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.partstype.kodeverdi), "ERROR", null);
            _validationResult.AddRule(_skjema, "4398.1.6.2", null, "'{0}' er en ugyldig kodeverdi for partstypen til ansvarlig søker. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/byggesoknad/partstype", Helpers.GetFullClassPath(() => form.ansvarligSoeker.partstype.kodeverdi), "ERROR", null);
            //Ansvarlig søker.foedselsnummer
            _validationResult.AddRule(_skjema, "4398.6.6.3", null, "Fødselsnummer må fylles ut når ansvarlig søker er en privatperson.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.foedselsnummer), "ERROR", Helpers.GetFullClassPath(() => form.ansvarligSoeker.partstype.kodeverdi));
            _validationResult.AddRule(_skjema, "4398.6.6.3.1", null, "Fødselsnummeret til ansvarlig søker er ikke gyldig.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.foedselsnummer), "ERROR", Helpers.GetFullClassPath(() => form.ansvarligSoeker.partstype.kodeverdi));
            _validationResult.AddRule(_skjema, "4398.6.6.3.2", null, "Fødselsnummeret til ansvarlig søker har ikke gyldig kontrollsiffer.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.foedselsnummer), "ERROR", Helpers.GetFullClassPath(() => form.ansvarligSoeker.partstype.kodeverdi));
            _validationResult.AddRule(_skjema, "4398.6.6.3.3", null, "Fødselsnummeret til ansvarlig søker kan ikke dekrypteres.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.foedselsnummer), "ERROR", Helpers.GetFullClassPath(() => form.ansvarligSoeker.partstype.kodeverdi));
            //ansvarligSoeker.organisasjonsnummer      
            _validationResult.AddRule(_skjema, "4398.2.6.4", null, "Organisasjonsnummeret til ansvarlig søker må fylles ut.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.organisasjonsnummer), "ERROR", Helpers.GetFullClassPath(() => form.ansvarligSoeker.partstype.kodeverdi));
            _validationResult.AddRule(_skjema, "4398.2.6.4.1", null, "Organisasjonsnummeret ('{0}') til ansvarlig søker har ikke gyldig kontrollsiffer.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.organisasjonsnummer), "ERROR", Helpers.GetFullClassPath(() => form.ansvarligSoeker.partstype.kodeverdi));
            _validationResult.AddRule(_skjema, "4398.3.6.4.2", null, "Organisasjonsnummeret ('{0}') til ansvarlig søker er ikke gyldig.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.organisasjonsnummer), "ERROR", Helpers.GetFullClassPath(() => form.ansvarligSoeker.partstype.kodeverdi));
            //ansvarligSoeker.Data..
            _validationResult.AddRule(_skjema, "4398.3.6.6", null, "Navnet til ansvarlig søker må fylles ut.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.navn), "ERROR", null);

            //AnsvarsomraadeValidation
            _validationResult.AddRule(_skjema, "4398.1.7", null, "Minst ett ansvarsområde må fylles ut.", Helpers.GetFullClassPath(() => form.gjennomfoeringsplan), "ERROR", null);
            _validationResult.AddRule(_skjema, "4398.1.7.1", null, "'{0}' er en ugyldig kodeverdi for funksjon for ansvarsområdet. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/byggesoknad/funksjon", Helpers.GetFullClassPath(() => form.gjennomfoeringsplan[0].funksjon.kodeverdi), "ERROR", null);
            _validationResult.AddRule(_skjema, "4398.1.7.4", null, "Du må beskrive ansvarsområdet. Beskrivelsen må være identisk med det som står i erklæringen om ansvarsrett.", Helpers.GetFullClassPath(() => form.gjennomfoeringsplan[0].ansvarsomraade), "ERROR", null);
            _validationResult.AddRule(_skjema, "4398.6.7.5", null, "Du må velge en tiltaksklasse for ansvarsområdet. Tiltaksklassen må være identisk med det som står i erklæringen om ansvarsrett.", Helpers.GetFullClassPath(() => form.gjennomfoeringsplan[0].tiltaksklasse), "ERROR", null);
            _validationResult.AddRule(_skjema, "4398.1.7.5.1", null, "'{0}' er en ugyldig kodeverdi for tiltaksklasse. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/byggesoknad/tiltaksklasse", Helpers.GetFullClassPath(() => form.gjennomfoeringsplan[0].tiltaksklasse.kodeverdi), "ERROR", null);
            _validationResult.AddRule(_skjema, "4398.6.7.6", null, "Du må velge når samsvars-/kontrollerklæring er planlagt eller signert for ansvarsområdet. Planlagt tidspunkt skal være identisk med det som står i erklæringen om ansvarsrett. Hvis samsvars- eller kontrollerklæring er avgitt, skal du fylle inn datoen for når den siste erklæringen er signert. Det skal være minst én registrering for hvert ansvarsområde.", Helpers.GetFullClassPath(() => form.gjennomfoeringsplan), "ERROR", null);

            _validationResult.AddRule(_skjema, "4398.6.8.1", "17.13", "Du må fylle ut versjonsnummeret på gjennomføringsplanen.", Helpers.GetFullClassPath(() => form.versjon), "ERROR", null);

            //Gjennomfoeringsplan/metadata/fraSluttbrukersystem
            _validationResult.AddRule(_skjema, "4398.4.9.1", null, "Systemet må fylle ut samme navn som er brukt i registrering for Altinn API i ‘fraSluttbrukersystem’.", Helpers.GetFullClassPath(() => form.metadata.fraSluttbrukersystem), "ERROR", null);

        }

        public ValidationResult Validate(GjennomfoeringsplanType form, List<string> formSetElements)
        {
            Parallel.Invoke(
                () => { MetadataValidering(form); },
                () => { EiendomByggestedValidation(form); },
                () => { SaksnummerErUtfyltVedHovedsoeknad(form, formSetElements); },
                () => { FraSluttbrukersystemErUtfylt(form); },
                () => { AnsvarsomraadeValidation(form); },
                () => { AnsvarligSokerValidation(form); },
                () => { GeneralInfoValidation(form); }
        );
            return _validationResult;
        }

        internal void MetadataValidering(GjennomfoeringsplanType form)
        {
            if (string.IsNullOrEmpty(form.metadata?.fraSluttbrukersystem))
                _validationResult.AddMessage("4398.4.9.1", null);
        }

        internal ValidationResult GetResult() => _validationResult;
        internal void EiendomByggestedValidation(GjennomfoeringsplanType form)
        {
            if (Helpers.ObjectIsNullOrEmpty(form.eiendomByggested))
            {
                _validationResult.AddMessage("4398.1.4", null);
            }
            else
            {
                foreach (EiendomType eiendomType in form.eiendomByggested)
                {

                    if (eiendomType.eiendomsidentifikasjon != null)
                    {
                        var kommunenummer = eiendomType.eiendomsidentifikasjon.kommunenummer;
                        var kommunenummerValideringStatus = _municipalityValidator.Validate_kommunenummerStatus(kommunenummer);
                        if (kommunenummerValideringStatus.Status != MunicipalityValidation.Ok)
                        {

                            switch (kommunenummerValideringStatus.Status)
                            {
                                case MunicipalityValidation.Empty:
                                    _validationResult.AddMessage("4398.1.4.1", null);
                                    break;
                                case MunicipalityValidation.Invalid:
                                    _validationResult.AddMessage("4398.1.4.2", new[] { kommunenummer });
                                    break;
                                case MunicipalityValidation.Expired:
                                    _validationResult.AddMessage("4398.1.4.3", null);
                                    break;
                            }
                        }
                        else
                        {
                            var gaardsnummer = eiendomType.eiendomsidentifikasjon.gaardsnummer;
                            var bruksnummer = eiendomType.eiendomsidentifikasjon.bruksnummer;
                            if (string.IsNullOrEmpty(gaardsnummer) || string.IsNullOrEmpty(bruksnummer))
                            {
                                if (string.IsNullOrEmpty(gaardsnummer))
                                    _validationResult.AddMessage("4398.6.4.4", null);

                                if (string.IsNullOrEmpty(bruksnummer))
                                    _validationResult.AddMessage("4398.6.4.4.1", null);
                            }
                            else
                            {
                                int gaardsnumerInt = 0;
                                int bruksnummerInt = 0;
                                //TODO kan grn or brn be 0
                                if (int.TryParse(gaardsnummer, out gaardsnumerInt) && int.TryParse(bruksnummer, out bruksnummerInt))
                                {
                                    int festenummerInt = 0;
                                    int seksjonsnummerInt = 0;
                                    int.TryParse(eiendomType.eiendomsidentifikasjon.festenummer, out festenummerInt);
                                    int.TryParse(eiendomType.eiendomsidentifikasjon.seksjonsnummer, out seksjonsnummerInt);

                                    if (gaardsnumerInt < 0 || bruksnummerInt < 0)
                                    {
                                        if (gaardsnumerInt < 0)
                                            _validationResult.AddMessage("4398.6.4.5", new[] { eiendomType.eiendomsidentifikasjon.gaardsnummer });

                                        if (bruksnummerInt < 0)
                                            _validationResult.AddMessage("4398.6.4.6", new[] { eiendomType.eiendomsidentifikasjon.bruksnummer });
                                    }
                                    else
                                    {
                                        //## MATRIKKEL
                                        var matrikkelnrExist = _matrikkelService.MatrikkelnrExist(kommunenummer, gaardsnumerInt, bruksnummerInt, festenummerInt, seksjonsnummerInt);
                                        if (matrikkelnrExist.HasValue)
                                        {
                                            if (!matrikkelnrExist.Value)
                                            {
                                                _validationResult.AddMessage("4398.1.4.12", new[]
                                                {
                                                    kommunenummer,
                                                    eiendomType.eiendomsidentifikasjon.gaardsnummer,
                                                    eiendomType.eiendomsidentifikasjon.bruksnummer,
                                                    eiendomType.eiendomsidentifikasjon.festenummer,
                                                    eiendomType.eiendomsidentifikasjon.seksjonsnummer,
                                                });
                                            }
                                        }
                                        else
                                        {
                                            _validationResult.AddMessage("4398.1.4.13", new[]
                                            {
                                                kommunenummer,
                                                eiendomType.eiendomsidentifikasjon.gaardsnummer,
                                                eiendomType.eiendomsidentifikasjon.bruksnummer,
                                                eiendomType.eiendomsidentifikasjon.festenummer,
                                                eiendomType.eiendomsidentifikasjon.seksjonsnummer,
                                            });
                                        }

                                        if (!String.IsNullOrEmpty(eiendomType.bygningsnummer))
                                        {
                                            long bygningsnrLong = 0;
                                            if (long.TryParse(eiendomType.bygningsnummer, out bygningsnrLong))
                                            {
                                                if (bygningsnrLong <= 0)
                                                {
                                                    _validationResult.AddMessage("4398.1.4.8", new[] { eiendomType.eiendomsidentifikasjon.gaardsnummer, eiendomType.eiendomsidentifikasjon.bruksnummer });
                                                }
                                                else
                                                {
                                                    ////## MATRIKKEL
                                                    var isBygningValid = _matrikkelService.IsBygningOnMatrikkelnr(bygningsnrLong, kommunenummer, gaardsnumerInt, bruksnummerInt, festenummerInt, seksjonsnummerInt);
                                                    if (!isBygningValid.GetValueOrDefault())
                                                    {
                                                        _validationResult.AddMessage("4398.1.4.14", new[]
                                                        {
                                                            kommunenummer,
                                                            eiendomType.eiendomsidentifikasjon.gaardsnummer,
                                                            eiendomType.eiendomsidentifikasjon.bruksnummer,
                                                            eiendomType.eiendomsidentifikasjon.festenummer,
                                                            eiendomType.eiendomsidentifikasjon.seksjonsnummer,
                                                        });
                                                    }
                                                }
                                            }
                                        }

                                        if (Helpers.ObjectIsNullOrEmpty(eiendomType.adresse))
                                        {
                                            _validationResult.AddMessage("4398.1.4.9", null);
                                        }
                                        else
                                        {
                                            if (string.IsNullOrEmpty(eiendomType.adresse?.adresselinje1))
                                            {
                                                _validationResult.AddMessage("4398.1.4.9.1", null);
                                            }
                                            else
                                            {
                                                if (String.IsNullOrEmpty(eiendomType.adresse.gatenavn) || String.IsNullOrEmpty(eiendomType.adresse.husnr))
                                                {
                                                    _validationResult.AddMessage("4398.1.4.9.2", null);
                                                }
                                                else
                                                {
                                                    //## MATRIKKEL
                                                    var isAdresseValid = _matrikkelService.IsVegadresseOnMatrikkelnr(eiendomType.adresse.gatenavn, eiendomType.adresse.husnr, eiendomType.adresse.bokstav, kommunenummer, gaardsnumerInt, bruksnummerInt, festenummerInt, seksjonsnummerInt);
                                                    if (isAdresseValid == null)
                                                    {
                                                        _validationResult.AddMessage("4398.1.4.15", null);
                                                    }
                                                    else if (!isAdresseValid.GetValueOrDefault())
                                                    {
                                                        _validationResult.AddMessage("4398.1.4.15.1", null);
                                                    }
                                                }
                                            }
                                        }

                                    }
                                }
                            }
                        }
                    }

                    if (String.IsNullOrEmpty(eiendomType.kommunenavn))
                    {
                        _validationResult.AddMessage("4398.1.4.10", null);
                    }

                    if (!String.IsNullOrEmpty(eiendomType.bolignummer))
                    {
                        if (GeneralValidations.Validate_bruksenhetsnummer(eiendomType.bolignummer).Status == ValidationStatus.Fail)
                        {
                            _validationResult.AddMessage("4398.1.4.11", null);
                        }
                    }
                }
            }
        }

        internal void SaksnummerErUtfyltVedHovedsoeknad(GjennomfoeringsplanType form, List<string> formSetElements)
        {
            if (Helpers.ObjectIsNullOrEmpty(form.kommunensSaksnummer))
            {

                var mainForms = new List<string>()
                {
                    "Søknad om rammetillatelse",
                    "Søknad om tillatelse i ett trinn",
                    "Søknad om igangsettingstillatelse",
                    "Søknad om midlertidig brukstillatelse",
                    "Søknad om ferdigattest",
                    "Søknad om endring av tillatelse"

                };
                //https://stackoverflow.com/a/11092955
                bool isSubForm = formSetElements?.Select(x => x)
                                      .Intersect(mainForms)
                                      .Any() ?? false;
                if (!isSubForm)
                {
                    _validationResult.AddMessage("4398.4.5.1", null);
                }
            }
            else
            {
                if (String.IsNullOrEmpty(form.kommunensSaksnummer.saksaar))
                    _validationResult.AddMessage("4398.4.5.1.1", null);

                if (String.IsNullOrEmpty(form.kommunensSaksnummer.sakssekvensnummer))
                    _validationResult.AddMessage("4398.4.5.1.2", null);

            }
        }

        internal void FraSluttbrukersystemErUtfylt(GjennomfoeringsplanType form)
        {
            if (string.IsNullOrEmpty(form.metadata?.fraSluttbrukersystem))
            {
                _validationResult.AddMessage("4398.4.2", null); ;
            }
        }


        internal void AnsvarligSokerValidation(GjennomfoeringsplanType form)
        {
            using (var logger = new PerfTimerLogger())
            {
                if (Helpers.ObjectIsNullOrEmpty(form.ansvarligSoeker))
                {
                    _validationResult.AddMessage("4398.1.6", null);
                }
                else
                {
                    if (Helpers.ObjectIsNullOrEmpty(form.ansvarligSoeker.partstype?.kodeverdi))
                    {
                        _validationResult.AddMessage("4398.1.6.1", null);
                    }
                    else
                    {
                        if (!_codeListService.IsCodelistValid("Partstype", form.ansvarligSoeker.partstype?.kodeverdi))
                        {
                            _validationResult.AddMessage("4398.1.6.2", new[] { form.ansvarligSoeker.partstype?.kodeverdi });
                        }
                        else
                        {
                            if (form.ansvarligSoeker.partstype?.kodeverdi == "Privatperson")
                            {
                                var foedselsnummerValidation = GeneralValidations.Validate_foedselsnummer(form.ansvarligSoeker.foedselsnummer);
                                switch (foedselsnummerValidation)
                                {
                                    case GeneralValidations.FoedselnumerValidation.Empty:
                                        _validationResult.AddMessage("4398.6.6.3", null);
                                        break;
                                    case GeneralValidations.FoedselnumerValidation.Invalid:
                                        _validationResult.AddMessage("4398.6.6.3.1", null);
                                        break;
                                    case GeneralValidations.FoedselnumerValidation.InvalidDigitsControl:
                                        _validationResult.AddMessage("4398.6.6.3.2", null);
                                        break;
                                    case GeneralValidations.FoedselnumerValidation.InvalidEncryption:
                                        _validationResult.AddMessage("4398.6.6.3.3", null);
                                        break;
                                }
                            }
                            else
                            {
                                var organisasjonsnummerValidation = GeneralValidations.Validate_OrgnummerEnum(form.ansvarligSoeker.organisasjonsnummer);
                                switch (organisasjonsnummerValidation)
                                {
                                    case GeneralValidations.OrganisasjonsnummerValidation.Empty:
                                        _validationResult.AddMessage("4398.2.6.4", null);
                                        break;
                                    case GeneralValidations.OrganisasjonsnummerValidation.Invalid:
                                        _validationResult.AddMessage("4398.2.6.4.1", new[] { form.ansvarligSoeker.organisasjonsnummer });
                                        break;
                                    case GeneralValidations.OrganisasjonsnummerValidation.InvalidDigitsControl:
                                        _validationResult.AddMessage("4398.3.6.4.2", new[] { form.ansvarligSoeker.organisasjonsnummer });
                                        break;
                                }
                            }
                        }

                        if (string.IsNullOrEmpty(form.ansvarligSoeker.navn))
                            _validationResult.AddMessage("4398.3.6.6", null);
                    }
                }
            }
        }
        internal void AnsvarsomraadeValidation(GjennomfoeringsplanType form)
        {
            if (Helpers.ObjectIsNullOrEmpty(form.gjennomfoeringsplan))
            {
                _validationResult.AddMessage("4398.1.7", null);
            }
            else
            {
                foreach (var ansvarsomraade in form.gjennomfoeringsplan)
                {


                    if (!_codeListService.IsCodelistValid("Funksjon", ansvarsomraade?.funksjon?.kodeverdi))
                    {
                        _validationResult.AddMessage("4398.1.7.1", new[] { ansvarsomraade?.funksjon?.kodeverdi });
                    }

                    if (string.IsNullOrEmpty(ansvarsomraade?.ansvarsomraade))
                    {
                        _validationResult.AddMessage("4398.1.7.4", null);
                    }

                    if (Helpers.ObjectIsNullOrEmpty(ansvarsomraade?.tiltaksklasse))
                    {
                        _validationResult.AddMessage("4398.6.7.5", null);

                    }
                    else
                    {
                        if (!_codeListService.IsCodelistValid("Tiltaksklasse", ansvarsomraade?.tiltaksklasse?.kodeverdi))
                        {
                            _validationResult.AddMessage("4398.1.7.5.1", new[] { ansvarsomraade?.tiltaksklasse?.kodeverdi });
                        }
                    }

                    bool[] dateTimeAnyHasValue = new[] { false };
                    bool[] areAnyPlanlagt = new[] { false }; ;
                    if (!Helpers.ObjectIsNullOrEmpty(ansvarsomraade))
                    {
                        dateTimeAnyHasValue = new[]
                        {
                            //DateTime?
                            ansvarsomraade.samsvarKontrollForeliggerVedFerdigattest.HasValue,
                            ansvarsomraade.samsvarKontrollForeliggerVedIgangsettingstillatelse.HasValue,
                            ansvarsomraade.samsvarKontrollForeliggerVedMidlertidigBrukstillatelse.HasValue,
                            ansvarsomraade.samsvarKontrollForeliggerVedRammetillatelse.HasValue,
                        };
                        areAnyPlanlagt = new[]
                        {
                            //Bool?
                            ansvarsomraade.samsvarKontrollPlanlagtVedFerdigattest.GetValueOrDefault(),
                            ansvarsomraade.samsvarKontrollPlanlagtVedIgangsettingstillatelse.GetValueOrDefault(),
                            ansvarsomraade.samsvarKontrollPlanlagtVedMidlertidigBrukstillatelse.GetValueOrDefault(),
                            ansvarsomraade.samsvarKontrollPlanlagtVedRammetillatelse.GetValueOrDefault(),
                        };
                    }

                    bool haveAnyDateTime = dateTimeAnyHasValue.Any(b => b);
                    bool haveAnyPlan = areAnyPlanlagt.Any(b => b);
                    if (!haveAnyDateTime && !haveAnyPlan)
                    {
                        _validationResult.AddMessage("4398.6.7.6", null);
                    }
                }
            }
        }

        internal void GeneralInfoValidation(GjennomfoeringsplanType form)
        {
            if (string.IsNullOrEmpty(form.versjon))
            {
                _validationResult.AddMessage("4398.6.8.1", null);
            }
        }

    }

}

