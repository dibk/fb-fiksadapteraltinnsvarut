﻿using System;
using DIBK.FBygg.Altinn.MapperCodelist;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Checklist.Prefill;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Matrikkel;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.PostalCode;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Validations;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.SvarUtForm
{
    public class FormValidator
    {
        public ValidationResult _validationResult;
        public List<string> _tiltakstyperISoeknad = new List<string>();
        public IMunicipalityValidator _municipalityValidator { get; set; }
        public IMatrikkelService _matrikkelService { get; set; }
        public IPostalCodeProvider _postalCodeProvider { get; set; }
        public IChecklistPrefillService _checklistPrefillService { get; set; }
        protected CodeListService _codeListService { get; set; }


        public FormValidator(string soknadType)
        {
            _municipalityValidator = new MunicipalityValidator();
            _matrikkelService = new MatrikkelService();
            _checklistPrefillService = new ChecklistPrefillService();
            _postalCodeProvider = new BringPostalCodeProvider();
            _codeListService = new CodeListService();

            _validationResult = new ValidationResult()
            {
                Soknadtype = soknadType,
            };
        }


        protected void GetChecklistAnswer<T>(T form)
        {
            var prefill = new List<ChecklistAnswer>();

            // Prefill from validationresult
            var prefillFromValidationResult = AsyncHelper.RunSync(async () => await _checklistPrefillService.GetPrefillChecklist(_validationResult, _validationResult.Soknadtype));
            if (prefillFromValidationResult != null)
            {
                prefill.AddRange(prefillFromValidationResult);
            }

            // Prefill from formData
            var formXmlString = SerializeToXmlString(form);
            var formJsonValue = EscapeSpecialCharacters(formXmlString);

            var prefillFromForm = AsyncHelper.RunSync(async () => await _checklistPrefillService.GetPrefillChecklist(formJsonValue, _validationResult.Soknadtype, _tiltakstyperISoeknad));
            if (prefillFromForm != null)
            {
                prefill.AddRange(prefillFromForm);
            }

            if (prefill.Any())
            {
                _validationResult.AddChecklistAnswers(prefill);
            }

        }



        private string SerializeToXmlString<T>(T xmlObject)
        {
            // Konverter XML-objektet til en XML-streng
            XmlSerializerNamespaces namespaces = new XmlSerializerNamespaces();
            namespaces.Add(string.Empty, string.Empty);

            XmlSerializer serializer = new XmlSerializer(xmlObject.GetType());
            using (StringWriter writer = new StringWriter())
            {
                serializer.Serialize(writer, xmlObject, namespaces);
                return writer.ToString();
            }
        }

        // Erstatt spesialtegn i en streng for å gjøre den gyldig i JSON
        private static string EscapeSpecialCharacters(string value)
        {
            value = value.Replace("\\", "\\\\")
                .Replace("\"", "\\\"")
                .Replace("/", "\\/")
                .Replace("\n", "\\n")
                .Replace("\t", "\\t")
                .Replace("\r", "\\r")
                .Replace("\b", "\\b")
                .Replace("\f", "\\f");
            return value;
        }
    }
}