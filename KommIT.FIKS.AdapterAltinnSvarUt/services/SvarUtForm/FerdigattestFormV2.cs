﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.SvarUtForm;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.SvarUt
{
    public class FerdigattestFormV2 : IAltinnForm, SvarUtForm
    {
        private const string DataFormatId = "5855";
        private const string DataFormatVersion = "43192";
        private const string SchemaFile = "ferdigattestV2.xsd";
        private const string Name = "Søknad om ferdigattest";
        private List<string> FormSetElements;

        public FerdigattestFormV2Validator Validator { get; set; }

        public FerdigattestFormV2()
        {
            Validator = new FerdigattestFormV2Validator();
        }

        public void SetFormSetElements(List<string> attachmentlist)
        {
            FormSetElements = attachmentlist;
        }

        public bool? ValidateAuthenticatedSubmitterVsFormData(string authenticatedSubmitter, ValidationResult validationResult)
        {
            bool isSubmitterValid = Helpers.ValidateEntityOfAltinnSubmitterDefault(authenticatedSubmitter, GetPropertyIdentifiers());

            if (!isSubmitterValid)
            {
                Helpers.RecordErrorAltinnSubmitter(validationResult);
            }
            return isSubmitterValid;
        }

        public void InitiateForm(string formDataAsXml)
        {

            form = Utils.SerializeUtil.DeserializeFromString<no.kxml.skjema.dibk.ferdigattestV2.FerdigattestType>(formDataAsXml);
        }

        private no.kxml.skjema.dibk.ferdigattestV2.FerdigattestType form;

        public PropertyIdentifiers GetPropertyIdentifiers()
        {
            PropertyIdentifiers propertyIdentifiers = new PropertyIdentifiers();

            propertyIdentifiers.Kommunenummer = form.eiendomByggested[0].eiendomsidentifikasjon.kommunenummer;
            propertyIdentifiers.Gaardsnummer = form.eiendomByggested[0].eiendomsidentifikasjon.gaardsnummer;
            propertyIdentifiers.Bruksnummer = form.eiendomByggested[0].eiendomsidentifikasjon.bruksnummer;
            propertyIdentifiers.Festenummer = form.eiendomByggested[0].eiendomsidentifikasjon.festenummer;
            propertyIdentifiers.Seksjonsnummer = form.eiendomByggested[0].eiendomsidentifikasjon.seksjonsnummer;
            propertyIdentifiers.Adresselinje1 = form.eiendomByggested[0].adresse.adresselinje1;
            propertyIdentifiers.Adresselinje2 = form.eiendomByggested[0].adresse.adresselinje2;
            propertyIdentifiers.Adresselinje3 = form.eiendomByggested[0].adresse.adresselinje3;
            propertyIdentifiers.Postnr = form.eiendomByggested[0].adresse.postnr;
            propertyIdentifiers.Poststed = form.eiendomByggested[0].adresse.poststed;
            propertyIdentifiers.Landkode = form.eiendomByggested[0].adresse.landkode;
            propertyIdentifiers.Bygningsnummer = form.eiendomByggested[0].bygningsnummer;
            propertyIdentifiers.Bolignummer = form.eiendomByggested[0].bolignummer;

            propertyIdentifiers.SoeknadSkjemaNavn = Name;
            propertyIdentifiers.FraSluttbrukersystem = form.fraSluttbrukersystem;

            propertyIdentifiers.KommunensSaksnummerAar = form.kommunensSaksnummer.saksaar;
            propertyIdentifiers.KommunensSaksnummerSekvensnummer = form.kommunensSaksnummer.sakssekvensnummer;

            propertyIdentifiers.AnsvarligSokerNavn = form.ansvarligSoeker.navn;
            propertyIdentifiers.AnsvarligSokerOrgnr = form.ansvarligSoeker.organisasjonsnummer;
            propertyIdentifiers.AnsvarligSokerFnr = form.ansvarligSoeker.foedselsnummer;

            if (form.ansvarligSoeker.adresse != null)
            {
                propertyIdentifiers.AnsvarligSokerAdresselinje1 = form.ansvarligSoeker.adresse.adresselinje1;
                propertyIdentifiers.AnsvarligSokerPostnr = form.ansvarligSoeker.adresse.postnr;
                propertyIdentifiers.AnsvarligSokerPoststed = form.ansvarligSoeker.adresse.poststed;
            }

            //Hvis det gjelder tiltak uten ansvarsrett så kan tiltakshaver være avsender
            if (String.IsNullOrEmpty(propertyIdentifiers.AnsvarligSokerNavn))
            {
                propertyIdentifiers.AnsvarligSokerNavn = form.tiltakshaver.navn;
                propertyIdentifiers.AnsvarligSokerOrgnr = form.tiltakshaver.organisasjonsnummer;
                propertyIdentifiers.AnsvarligSokerFnr = form.tiltakshaver.foedselsnummer;

                if (form.tiltakshaver.adresse != null)
                {
                    propertyIdentifiers.AnsvarligSokerAdresselinje1 = form.tiltakshaver.adresse.adresselinje1;
                    propertyIdentifiers.AnsvarligSokerPostnr = form.tiltakshaver.adresse.postnr;
                    propertyIdentifiers.AnsvarligSokerPoststed = form.tiltakshaver.adresse.poststed;
                }
            }
            return propertyIdentifiers;
        }

        public ValidationResult ValidateData()
        {
            return Validator.Validate(form, FormSetElements);
        }

        public string GetDataFormatId()
        {
            return DataFormatId;
        }

        public string GetDataFormatVersion()
        {
            return DataFormatVersion;
        }

        public string GetMunicipalityCode()
        {
            return GetPropertyIdentifiers().Kommunenummer;
        }

        public string GetSchemaFile()
        {
            return SchemaFile;
        }

        public string GetName()
        {
            return Name;
        }
    }
}