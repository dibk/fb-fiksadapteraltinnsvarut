using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using DIBK.FBygg.Altinn.MapperCodelist;
using KommIT.FIKS.AdapterAltinnSvarUt.Logger;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Checklist;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Matrikkel;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.PostalCode;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Validations;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;
using no.kxml.skjema.dibk.suppleringAvSoknad;
using Serilog;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.SvarUtForm
{
    public class SuppleringFormValidator
    {
        private ILogger _logger = Log.ForContext<SuppleringFormValidator>();

        CodeListService _codeListService;

        public ValidationResult _validationResult = new ValidationResult();
        public List<string> _tiltakstyperISoeknad = new List<string>();
        public IMunicipalityValidator _municipalityValidator { get; set; }
        public IMatrikkelService _matrikkelService { get; set; }
        public IPostalCodeProvider _postalCodeProvider { get; set; }

        internal ValidationResult GetResult() => _validationResult;
        private List<string> _tiltakstypesForMangelbesvarelseAndEttersendingVedlegRule;

        public SuppleringFormValidator()
        {
            var form = new SuppleringAvSoeknadType();
            var skjema = "SuppleringAvSoeknad";
            _codeListService = new CodeListService();
            _municipalityValidator = new MunicipalityValidator();
            _matrikkelService = new MatrikkelService();
            _validationResult = new ValidationResult()
            {
                //Soknadstype bare trenge hvis de kalles checklist
                //  Soknadtype = "SU",
            };
            _postalCodeProvider = new BringPostalCodeProvider();

            _validationResult.AddRule(skjema, "5721.1.1", null, "Innhold må være i henhold til informasjonsmodell/xsd for skjema.", null, "ERROR", null);
            _validationResult.AddRule(skjema, "5721.1.3", null, "Vedlegg må være i henhold til metadata for skjema. Vedleggstype eller underskjema '{0}' er ikke gyldig.", Helpers.GetFullClassPath(() => form), "ERROR", null);
            _validationResult.AddRule(skjema, "5721.1.3.1", null, "Systemet må fylle ut samme navn som er brukt i registrering for Altinn API i ‘fraSluttbrukersystem’.", Helpers.GetFullClassPath(() => form.metadata.fraSluttbrukersystem), "ERROR", null);

            //Eiendommens--> eiendomsidentifikasjon
            _validationResult.AddRule(skjema, "5721.1.4", null, "Du må oppgi hvilken eiendom/hvilke eiendommer byggesøknaden gjelder.", Helpers.GetFullClassPath(() => form.eiendomByggested), "ERROR", null);
            _validationResult.AddRule(skjema, "5721.1.4.1", null, "Kommunenummer må fylles ut for eiendom/byggested.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].eiendomsidentifikasjon.kommunenummer), "ERROR", null);
            _validationResult.AddRule(skjema, "5721.1.4.2", null, "Kommunenummeret '{0}' for eiendom/byggested finnes ikke i kodelisten. Du kan sjekke riktig kommunenummer på https://register.geonorge.no/sosi-kodelister/inndelinger/inndelingsbase/kommunenummer", Helpers.GetFullClassPath(() => form.eiendomByggested[0].eiendomsidentifikasjon.kommunenummer), "ERROR", null);
            _validationResult.AddRule(skjema, "5721.1.4.3", null, "Kommunenummeret '{0}' for eiendom/byggested har ugyldig status ({1}). Du kan sjekke status på https://register.geonorge.no/sosi-kodelister/inndelinger/inndelingsbase/kommunenummer", Helpers.GetFullClassPath(() => form.eiendomByggested[0].eiendomsidentifikasjon.kommunenummer), "ERROR", null); //
            _validationResult.AddRule(skjema, "5721.1.4.4", null, "Gårdsnummer må fylles ut for eiendom/byggested.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].eiendomsidentifikasjon.gaardsnummer), "ERROR", null);
            _validationResult.AddRule(skjema, "5721.1.4.4.1", null, "Bruksnummer må fylles ut for eiendom/byggested.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].eiendomsidentifikasjon.bruksnummer), "ERROR", null);
            _validationResult.AddRule(skjema, "5721.1.4.5", null, "Gårdsnummer '{0}' for eiendom/byggested må være '0' eller større.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].eiendomsidentifikasjon.gaardsnummer), "ERROR", null);
            _validationResult.AddRule(skjema, "5721.1.4.6", null, "Bruksnummer '{0}' for eiendom/byggested må være '0' eller større.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].eiendomsidentifikasjon.bruksnummer), "ERROR", null);
            _validationResult.AddRule(skjema, "5721.1.4.7", null, "Du har oppgitt følgende bygningsnummer for eiendom/byggested: ‘{0}'. Bygningsnummeret må være et tall.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].bygningsnummer), "ERROR", null);
            _validationResult.AddRule(skjema, "5721.1.4.8", null, "Bygningsnummer for eiendom/byggested må være større enn '0'.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].bygningsnummer), "ERROR", null);

            //Eiendommens --> Adress
            _validationResult.AddRule(skjema, "5721.1.4.9", null, "Postadresse for eiendom/byggested bør fylles ut.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].adresse), "WARNING", null);
            _validationResult.AddRule(skjema, "5721.1.4.9.1", null, "Adresselinje 1 for eiendom/byggested bør fylles ut.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].adresse.adresselinje1), "WARNING", null);
            _validationResult.AddRule(skjema, "5721.1.4.9.2", null, "Du bør oppgi gatenavn, husnummer og eventuell bokstav for eiendom/byggested slik at adressen kan valideres mot matrikkelen. Du kan sjekke riktig adresse på https://seeiendom.no", Helpers.GetFullClassPath(() => form.eiendomByggested[0].adresse.gatenavn), "WARNING", null); //
            _validationResult.AddRule(skjema, "5721.1.4.10", null, "Navnet på kommunen bør fylles ut for eiendom/byggested", Helpers.GetFullClassPath(() => form.eiendomByggested[0].kommunenavn), "WARNING", null);
            _validationResult.AddRule(skjema, "5721.1.4.11", null, "Når bruksenhetsnummer/bolignummer er fylt ut, må det følge riktig format (for eksempel H0101). Se https://www.kartverket.no/eiendom/adressering/bruksenhetsnummer/.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].bolignummer), "ERROR", null);

            //if (_matrikkelFlag)
            //{
            _validationResult.AddRule(skjema, "5721.1.4.12", null, "Når eiendomsidentifikasjon [{0}-{1}/{2}/{3}/{4}] er oppgitt for eiendom/byggested, bør den være gyldig i matrikkelen. Du kan sjekke riktig informasjon på https://seeiendom.no", Helpers.GetFullClassPath(() => form.eiendomByggested[0].eiendomsidentifikasjon), "WARNING", null);
            _validationResult.AddRule(skjema, "5721.1.4.13", null, "Eiendomsidentifikasjon [{0}-{1}/{2}/{3}/{4}] for eiendom/byggested ble ikke validert mot matrikkelen. Identifikasjonen kan være riktig, men en teknisk feil gjør at vi ikke kan bekrefte det.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].eiendomsidentifikasjon), "WARNING", null);
            _validationResult.AddRule(skjema, "5721.1.4.14", null, "Når bygningsnummer [{0}] er oppgitt for eiendom/byggested, bør det være gyldig i matrikkelen på aktuelt matrikkelnummer. Du kan sjekke riktig bygningsnummer på https://seeiendom.no", Helpers.GetFullClassPath(() => form.eiendomByggested[0].bygningsnummer), "WARNING", null);
            _validationResult.AddRule(skjema, "5721.1.4.15", null, "Gateadressen for eiendom/byggested ble ikke validert mot matrikkelen. Gateadressen kan være riktig, men en teknisk feil gjør at vi ikke kan bekrefte det. Du kan sjekke adressen på https://seeiendom.no", Helpers.GetFullClassPath(() => form.eiendomByggested[0].adresse.gatenavn), "WARNING", null);
            _validationResult.AddRule(skjema, "5721.1.4.15.1", null, "Du har oppgitt feil gateadresse for eiendom/byggested. Du kan sjekke riktig adresse på https://seeiendom.no", Helpers.GetFullClassPath(() => form.eiendomByggested[0].adresse.gatenavn), "WARNING", null);
            //}

            //Søker validering
            _validationResult.AddRule(skjema, "5721.1.5", null, "Du har ikke fylt ut informasjon om søker. Hvis du søker med ansvarsrett, må du fylle ut informasjon om ansvarlig søker. For tiltak uten ansvarsrett, må du fylle ut informasjon om tiltakshaver.", Helpers.GetFullClassPath(() => form), "ERROR", null);

            // Ansvarlig søker
            _validationResult.AddRule(skjema, "5721.1.5.1", null, "Partstypen for ansvarlig søker må fylles ut.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.partstype), "ERROR", null);
            _validationResult.AddRule(skjema, "5721.1.5.2", null, "'{0}' er en ugyldig kodeverdi for partstypen til ansvarlig søker. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/byggesoknad/partstype", Helpers.GetFullClassPath(() => form.ansvarligSoeker.partstype.kodeverdi), "ERROR", null);

            //Ansvarlig søker foedselsnummer
            _validationResult.AddRule(skjema, "5721.1.5.3", null, "Fødselsnummer må fylles ut når ansvarlig søker er en privatperson.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.foedselsnummer), "ERROR", Helpers.GetFullClassPath(() => form.ansvarligSoeker.partstype.kodeverdi));
            _validationResult.AddRule(skjema, "5721.1.5.3.1", null, "Fødselsnummeret til ansvarlig søker er ikke gyldig.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.foedselsnummer), "ERROR", Helpers.GetFullClassPath(() => form.ansvarligSoeker.partstype.kodeverdi));
            _validationResult.AddRule(skjema, "5721.1.5.3.2", null, "Fødselsnummeret til ansvarlig søker har ikke gyldig kontrollsiffer.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.foedselsnummer), "ERROR", Helpers.GetFullClassPath(() => form.ansvarligSoeker.partstype.kodeverdi));
            _validationResult.AddRule(skjema, "5721.1.5.3.3", null, "Fødselsnummeret til ansvarlig søker kan ikke dekrypteres.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.foedselsnummer), "ERROR", Helpers.GetFullClassPath(() => form.ansvarligSoeker.partstype.kodeverdi));
            //Ansvarlig søker organisasjonsnummer      
            _validationResult.AddRule(skjema, "5721.1.5.4", null, "Organisasjonsnummeret til ansvarlig søker må fylles ut.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.organisasjonsnummer), "ERROR", Helpers.GetFullClassPath(() => form.ansvarligSoeker.partstype.kodeverdi));
            _validationResult.AddRule(skjema, "5721.1.5.4.1", null, "Organisasjonsnummeret ('{0}') til ansvarlig søker har ikke gyldig kontrollsiffer.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.organisasjonsnummer), "ERROR", Helpers.GetFullClassPath(() => form.ansvarligSoeker.partstype.kodeverdi));
            _validationResult.AddRule(skjema, "5721.1.5.4.2", null, "Organisasjonsnummeret ('{0}') til ansvarlig søker er ikke gyldig.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.organisasjonsnummer), "ERROR", Helpers.GetFullClassPath(() => form.ansvarligSoeker.partstype.kodeverdi));
            _validationResult.AddRule(skjema, "5721.1.5.4.3", null, "Kontaktpersonen til ansvarlig søker bør fylles ut.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.kontaktperson), "WARNING", Helpers.GetFullClassPath(() => form.ansvarligSoeker.partstype.kodeverdi));
            _validationResult.AddRule(skjema, "5721.1.5.4.3.1", null, "Navnet til kontaktpersonen for ansvarlig søker bør fylles ut.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.kontaktperson.navn), "WARNING", Helpers.GetFullClassPath(() => form.ansvarligSoeker.partstype.kodeverdi));
            _validationResult.AddRule(skjema, "5721.1.5.4.3.2", null, "Telefonnummeret eller mobilnummeret til ansvarlig søkers kontaktperson bør fylles ut. ", Helpers.GetFullClassPath(() => form.ansvarligSoeker.kontaktperson), "WARNING", Helpers.GetFullClassPath(() => form.ansvarligSoeker.partstype.kodeverdi));
            _validationResult.AddRule(skjema, "5721.1.5.4.3.3", null, "Telefonnummeret til ansvarlig søkers kontaktperson må kun inneholde tall og '+'.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.kontaktperson.telefonnummer), "ERROR", Helpers.GetFullClassPath(() => form.ansvarligSoeker.partstype.kodeverdi));
            _validationResult.AddRule(skjema, "5721.1.5.4.3.4", null, "Mobilnummeret til ansvarlig søkers kontaktperson må kun inneholde tall og '+'.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.kontaktperson.mobilnummer), "ERROR", Helpers.GetFullClassPath(() => form.ansvarligSoeker.partstype.kodeverdi));
            _validationResult.AddRule(skjema, "5721.1.5.4.3.5", null, "E-postadressen til ansvarlig søkers kontaktperson bør fylles ut.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.kontaktperson.epost), "WARNING", Helpers.GetFullClassPath(() => form.ansvarligSoeker.partstype.kodeverdi));
            //Ansvarlig søker adresse
            _validationResult.AddRule(skjema, "5721.1.5.5", null, "Adressen til ansvarlig søker bør fylles ut.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.adresse), "WARNING", null);
            _validationResult.AddRule(skjema, "5721.1.5.5.1", null, "Adresselinje 1 bør fylles ut for ansvarlig søker.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.adresse.adresselinje1), "WARNING", null);
            _validationResult.AddRule(skjema, "5721.1.5.5.2", null, "Ugyldig landkode for ansvarlig søker.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.adresse.landkode), "WARNING", Helpers.GetFullClassPath(() => form.ansvarligSoeker.adresse.adresselinje1));
            _validationResult.AddRule(skjema, "5721.1.5.5.3", null, "Postnummeret til ansvarlig søker bør angis.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.adresse.postnr), "WARNING", Helpers.GetFullClassPath(() => form.ansvarligSoeker.adresse.adresselinje1));
            _validationResult.AddRule(skjema, "5721.1.5.5.4", null, "Postnummeret '{0}' til ansvarlig søker har ikke gyldig kontrollsiffer.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.adresse.postnr), "WARNING", Helpers.GetFullClassPath(() => form.ansvarligSoeker.adresse.adresselinje1));
            _validationResult.AddRule(skjema, "5721.1.5.5.5", null, "Postnummeret '{0}' til ansvarlig søker er ugyldig. Du kan sjekke riktig postnummer på http://adressesok.bring.no/", Helpers.GetFullClassPath(() => form.ansvarligSoeker.adresse.postnr), "WARNING", Helpers.GetFullClassPath(() => form.ansvarligSoeker.adresse.adresselinje1));
            _validationResult.AddRule(skjema, "5721.1.5.5.6", null, "Postnummeret '{0}' til ansvarlig søker stemmer ikke overens med poststedet '{1}'. Riktig postnummer er '{2}'. Du kan sjekke riktig poststed på http://adressesok.bring.no/", Helpers.GetFullClassPath(() => form.ansvarligSoeker.adresse.postnr), "WARNING", Helpers.GetFullClassPath(() => form.ansvarligSoeker.adresse.adresselinje1));
            _validationResult.AddRule(skjema, "5721.1.5.5.7", null, "Postnummeret '{0}' til ansvarlig søker ble ikke validert. Postnummeret kan være riktig, men en teknisk feil gjør at vi ikke kan bekrefte det.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.adresse.postnr), "WARNING", Helpers.GetFullClassPath(() => form.ansvarligSoeker.adresse.adresselinje1));
            _validationResult.AddRule(skjema, "5721.1.5.5.8", null, "Telefonnummeret eller mobilnummeret til ansvarlig søker bør fylles ut.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.mobilnummer), "WARNING", Helpers.GetFullClassPath(() => form.ansvarligSoeker.telefonnummer));
            _validationResult.AddRule(skjema, "5721.1.5.5.8.1", null, "Telefonnummeret til tiltakshaver må kun inneholde tall og '+'.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.telefonnummer), "ERROR", null);
            _validationResult.AddRule(skjema, "5721.1.5.5.8.2", null, "Mobilnummeret til tiltakshaver må kun inneholde tall og '+'.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.mobilnummer), "ERROR", null);

            _validationResult.AddRule(skjema, "5721.1.5.5.9", null, "Navnet til ansvarlig søker må fylles ut", Helpers.GetFullClassPath(() => form.ansvarligSoeker.navn), "ERROR", null);
            _validationResult.AddRule(skjema, "5721.1.5.5.10", null, "E-postadressen til ansvarlig søker bør fylles ut.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.epost), "WARNING", null);

            //Tiltakshaver
            _validationResult.AddRule(skjema, "5721.1.5.6", null, "Partstypen for tiltakshaver må fylles ut.", Helpers.GetFullClassPath(() => form.tiltakshaver.partstype), "ERROR", Helpers.GetFullClassPath(() => form.ansvarligSoeker));
            _validationResult.AddRule(skjema, "5721.1.5.6.1", null, "'{0}' er en ugyldig kodeverdi for partstypen til tiltakshaver. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/byggesoknad/partstype", Helpers.GetFullClassPath(() => form.tiltakshaver.partstype.kodeverdi), "ERROR", null);
            //Tiltakshaver foedselsnummer
            _validationResult.AddRule(skjema, "5721.1.5.6.2", null, "Fødselsnummer må fylles ut når tiltakshaver er en privatperson.", Helpers.GetFullClassPath(() => form.tiltakshaver.foedselsnummer), "ERROR", Helpers.GetFullClassPath(() => form.tiltakshaver.partstype.kodeverdi));
            _validationResult.AddRule(skjema, "5721.1.5.6.2.1", null, "Fødselsnummeret til tiltakshaver er ikke gyldig.", Helpers.GetFullClassPath(() => form.tiltakshaver.foedselsnummer), "ERROR", Helpers.GetFullClassPath(() => form.tiltakshaver.partstype.kodeverdi));
            _validationResult.AddRule(skjema, "5721.1.5.6.2.2", null, "Fødselsnummeret til tiltakshaver har ikke gyldig kontrollsiffer.", Helpers.GetFullClassPath(() => form.tiltakshaver.foedselsnummer), "ERROR", Helpers.GetFullClassPath(() => form.tiltakshaver.partstype.kodeverdi));
            _validationResult.AddRule(skjema, "5721.1.5.6.2.3", null, "Fødselsnummeret til tiltakshaver kan ikke dekrypteres.", Helpers.GetFullClassPath(() => form.tiltakshaver.foedselsnummer), "ERROR", Helpers.GetFullClassPath(() => form.tiltakshaver.partstype.kodeverdi));
            //Tiltakshaver organisasjonsnummer      
            _validationResult.AddRule(skjema, "5721.1.5.6.3", null, "Organisasjonsnummeret til tiltakshaver må fylles ut når tiltakshaver er en organisasjon.", Helpers.GetFullClassPath(() => form.tiltakshaver.organisasjonsnummer), "ERROR", Helpers.GetFullClassPath(() => form.tiltakshaver.partstype.kodeverdi));
            _validationResult.AddRule(skjema, "5721.1.5.6.3.1", null, "Organisasjonsnummeret ('{0}') til tiltakshaver har ikke gyldig kontrollsiffer.", Helpers.GetFullClassPath(() => form.tiltakshaver.organisasjonsnummer), "ERROR", Helpers.GetFullClassPath(() => form.tiltakshaver.partstype.kodeverdi));
            _validationResult.AddRule(skjema, "5721.1.5.6.3.2", null, "Organisasjonsnummeret ('{0}') til tiltakshaver er ikke gyldig.", Helpers.GetFullClassPath(() => form.tiltakshaver.organisasjonsnummer), "ERROR", Helpers.GetFullClassPath(() => form.tiltakshaver.partstype.kodeverdi));
            _validationResult.AddRule(skjema, "5721.1.5.6.3.3", null, "Kontaktperson bør fylles ut når tiltakshaver er en organisasjon.", Helpers.GetFullClassPath(() => form.tiltakshaver.kontaktperson), "WARNING", Helpers.GetFullClassPath(() => form.tiltakshaver.partstype.kodeverdi));
            _validationResult.AddRule(skjema, "5721.1.5.6.3.3.1", null, "Navnet til kontaktpersonen for tiltakshaver bør fylles ut.", Helpers.GetFullClassPath(() => form.tiltakshaver.kontaktperson.navn), "WARNING", Helpers.GetFullClassPath(() => form.tiltakshaver.partstype.kodeverdi));
            _validationResult.AddRule(skjema, "5721.1.5.6.3.3.2", null, "Telefonnummeret eller mobilnummeret til tiltakshavers kontaktperson bør fylles ut.", Helpers.GetFullClassPath(() => form.tiltakshaver.kontaktperson), "WARNING", Helpers.GetFullClassPath(() => form.tiltakshaver.partstype.kodeverdi));
            _validationResult.AddRule(skjema, "5721.1.5.6.3.3.3", null, "Telefonnummeret til tiltakshavers kontaktperson må kun inneholde tall og '+'.", Helpers.GetFullClassPath(() => form.tiltakshaver.kontaktperson.telefonnummer), "ERROR", Helpers.GetFullClassPath(() => form.tiltakshaver.partstype.kodeverdi));
            _validationResult.AddRule(skjema, "5721.1.5.6.3.3.4", null, "Mobilnummeret til tiltakshavers kontaktperson må kun inneholde tall og '+'.", Helpers.GetFullClassPath(() => form.tiltakshaver.kontaktperson.mobilnummer), "ERROR", Helpers.GetFullClassPath(() => form.tiltakshaver.partstype.kodeverdi));
            _validationResult.AddRule(skjema, "5721.1.5.6.3.3.5", null, "E-postadressen til til tiltakshavers kontaktperson bør fylles ut.", Helpers.GetFullClassPath(() => form.tiltakshaver.kontaktperson.epost), "WARNING", Helpers.GetFullClassPath(() => form.tiltakshaver.partstype.kodeverdi));
            //tiltakshaver adresse
            _validationResult.AddRule(skjema, "5721.1.5.6.4", null, "Adressen til tiltakshaver bør fylles ut.", Helpers.GetFullClassPath(() => form.tiltakshaver.adresse), "WARNING", null);
            _validationResult.AddRule(skjema, "5721.1.5.6.4.1", null, "Adresselinje 1 bør fylles ut for tiltakshaver.", Helpers.GetFullClassPath(() => form.tiltakshaver.adresse.adresselinje1), "WARNING", null);
            _validationResult.AddRule(skjema, "5721.1.5.6.4.2", null, "Ugyldig landkode for tiltakshaver.", Helpers.GetFullClassPath(() => form.tiltakshaver.adresse.landkode), "WARNING", Helpers.GetFullClassPath(() => form.tiltakshaver.adresse.adresselinje1));
            _validationResult.AddRule(skjema, "5721.1.5.6.4.3", null, "Postnummeret til tiltakshaver bør angis.", Helpers.GetFullClassPath(() => form.tiltakshaver.adresse.postnr), "WARNING", Helpers.GetFullClassPath(() => form.tiltakshaver.adresse.adresselinje1));
            _validationResult.AddRule(skjema, "5721.1.5.6.4.4", null, "Postnummeret '{0}' til tiltakshaver har ikke gyldig kontrollsiffer.", Helpers.GetFullClassPath(() => form.tiltakshaver.adresse.postnr), "WARNING", Helpers.GetFullClassPath(() => form.tiltakshaver.adresse.adresselinje1));
            _validationResult.AddRule(skjema, "5721.1.5.6.4.5", null, "Postnummeret '{0}' til tiltakshaver er ugyldig. Du kan sjekke riktig postnummer på http://adressesok.bring.no/", Helpers.GetFullClassPath(() => form.tiltakshaver.adresse.postnr), "WARNING", Helpers.GetFullClassPath(() => form.tiltakshaver.adresse.adresselinje1));
            _validationResult.AddRule(skjema, "5721.1.5.6.4.6", null, "Postnummeret '{0}' til tiltakshaver stemmer ikke overens med poststedet '{1}'. Riktig postnummer er '{2}'. Du kan sjekke riktig poststed på http://adressesok.bring.no/", Helpers.GetFullClassPath(() => form.tiltakshaver.adresse.postnr), "WARNING", Helpers.GetFullClassPath(() => form.tiltakshaver.adresse.adresselinje1));
            _validationResult.AddRule(skjema, "5721.1.5.6.4.7", null, "Postnummeret '{0}' til tiltakshaver ble ikke validert. Postnummeret kan være riktig, men en teknisk feil gjør at vi ikke kan bekrefte det.", Helpers.GetFullClassPath(() => form.tiltakshaver.adresse.postnr), "WARNING", Helpers.GetFullClassPath(() => form.tiltakshaver.adresse.adresselinje1));
            _validationResult.AddRule(skjema, "5721.1.5.6.5", null, "Telefonnummeret eller mobilnummeret til tiltakshaver må fylles ut.", Helpers.GetFullClassPath(() => form.tiltakshaver), "WARNING", Helpers.GetFullClassPath(() => form.tiltakshaver.telefonnummer));
            _validationResult.AddRule(skjema, "5721.1.5.6.5.1", null, "Telefonnummeret til ansvarlig søker må kun inneholde tall og '+'.", Helpers.GetFullClassPath(() => form.tiltakshaver.telefonnummer), "ERROR", null);
            _validationResult.AddRule(skjema, "5721.1.5.6.5.2", null, "Mobilnummeret til ansvarlig søker må kun inneholde tall og '+'.", Helpers.GetFullClassPath(() => form.tiltakshaver.mobilnummer), "ERROR", null);
            _validationResult.AddRule(skjema, "5721.1.5.6.6", null, "Navnet til tiltakshaver må fylles ut.", Helpers.GetFullClassPath(() => form.tiltakshaver.navn), "ERROR", null);
            _validationResult.AddRule(skjema, "5721.1.5.6.7", null, "E-postadressen til tiltakshaver bør fylles ut.", Helpers.GetFullClassPath(() => form.tiltakshaver.epost), "WARNING", null);
            //KommunensSaksnummerValidering - Alle
            _validationResult.AddRule(skjema, "5721.1.6", null, "Kommunens saksnummer må fylles ut.", Helpers.GetFullClassPath(() => form.kommunensSaksnummer), "ERROR", Helpers.GetFullClassPath(() => form.mangelbesvarelse));
            _validationResult.AddRule(skjema, "5721.1.6.1", null, "Du må oppgi kommunens saksnummer med saksår.", Helpers.GetFullClassPath(() => form.kommunensSaksnummer.saksaar), "ERROR", Helpers.GetFullClassPath(() => form.mangelbesvarelse));
            _validationResult.AddRule(skjema, "5721.1.6.2", null, "Du må oppgi kommunens saksnummer med sekvensnummer.", Helpers.GetFullClassPath(() => form.kommunensSaksnummer.sakssekvensnummer), "ERROR", Helpers.GetFullClassPath(() => form.mangelbesvarelse));
            //KommunensSaksnummerValidering - Alle
            _validationResult.AddRule(skjema, "5721.1.6.3", null, "Kommunens saksnummer bør fylles ut.", Helpers.GetFullClassPath(() => form.kommunensSaksnummer), "WARNING", Helpers.GetFullClassPath(() => form.mangelbesvarelse));
            _validationResult.AddRule(skjema, "5721.1.6.3.1", null, "Du bør oppgi kommunens saksnummer med saksår.", Helpers.GetFullClassPath(() => form.kommunensSaksnummer.saksaar), "WARNING", Helpers.GetFullClassPath(() => form.mangelbesvarelse));
            _validationResult.AddRule(skjema, "5721.1.6.3.2", null, "Du bør oppgi kommunens saksnummer med sekvensnummer.", Helpers.GetFullClassPath(() => form.kommunensSaksnummer.sakssekvensnummer), "WARNING", Helpers.GetFullClassPath(() => form.mangelbesvarelse));

            //EttersendingOgMangelbesvarelseValidering
            _validationResult.AddRule(skjema, "5721.1.7", null, "Du må fylle ut informasjon om mangelbesvarelse og/eller ettersending.", Helpers.GetFullClassPath(() => form), "ERROR", null);

            // Ettersending
            _validationResult.AddRule(skjema, "5721.1.8", null, "Du har valgt å ettersende informasjon. Da må du lage en tittel som beskriver det du ettersender.", Helpers.GetFullClassPath(() => form.ettersending[0].tittel), "ERROR", Helpers.GetFullClassPath(() => form.ettersending[0]));
            _validationResult.AddRule(skjema, "5721.1.8.1", null, "Du har valgt å ettersende informasjon. Da må du enten skrive en kommentar eller fylle ut informasjon om hvilke vedlegg/underskjema du skal ettersende.", Helpers.GetFullClassPath(() => form.ettersending[0]), "ERROR", null);

            //tema 
            _validationResult.AddRule(skjema, "5721.1.8.2", null, "'{0}' er en ugyldig kodeverdi for tema. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/ebyggesak/tema", Helpers.GetFullClassPath(() => form.ettersending[0].tema.kodeverdi), "ERROR", null);
            _validationResult.AddRule(skjema, "5721.1.8.3", null, "Når tema er valgt, må kodebeskrivelse fylles ut. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/ebyggesak/tema", Helpers.GetFullClassPath(() => form.ettersending[0].tema.kodebeskrivelse), "ERROR", Helpers.GetFullClassPath(() => form.ettersending[0].tema.kodeverdi));
            _validationResult.AddRule(skjema, "5721.1.8.4", null, "Kodebeskrivelsen '{0}' stemmer ikke med den valgte kodeverdien for tema. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/ebyggesak/tema", Helpers.GetFullClassPath(() => form.ettersending[0].tema.kodebeskrivelse), "WARNING", Helpers.GetFullClassPath(() => form.ettersending[0].tema.kodeverdi));
            //vedlegg
            _validationResult.AddRule(skjema, "5721.1.8.5", null, "Du har lagt ved vedlegget '{0}'. Da må du også velge vedleggstype.", Helpers.GetFullClassPath(() => form.ettersending[0].vedlegg[0].vedleggstype), "ERROR", Helpers.GetFullClassPath(() => form.ettersending[0].vedlegg[0].filnavn));
            _validationResult.AddRule(skjema, "5721.1.8.5.1", null, "Du har valgt å legge ved '{0}'. Da må du også fylle ut et filnavn.", Helpers.GetFullClassPath(() => form.ettersending[0].vedlegg[0].filnavn), "WARNING", Helpers.GetFullClassPath(() => form.ettersending[0].vedlegg[0].vedleggstype));
            _validationResult.AddRule(skjema, "5721.1.8.6", null, "Du må oppgi en kodeverdi for vedleggstype. Du kan sjekke gyldige vedlegg på https://register.geonorge.no/kodelister/byggesoknad/vedlegg-og-underskjema/vedlegg", Helpers.GetFullClassPath(() => form.ettersending[0].vedlegg[0].vedleggstype.kodeverdi), "ERROR", Helpers.GetFullClassPath(() => form.ettersending[0].vedlegg[0].filnavn));
            _validationResult.AddRule(skjema, "5721.1.8.6.1", null, "'{0}' er en ugyldig kodeverdi for vedleggstype. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/vedlegg-og-underskjema/vedlegg ", Helpers.GetFullClassPath(() => form.ettersending[0].vedlegg[0].vedleggstype.kodeverdi), "ERROR", Helpers.GetFullClassPath(() => form.ettersending[0].vedlegg[0].filnavn));
            _validationResult.AddRule(skjema, "5721.1.8.6.2", null, "Når vedleggstype er valgt, må kodebeskrivelse fylles ut. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/vedlegg-og-underskjema/vedlegg", Helpers.GetFullClassPath(() => form.ettersending[0].vedlegg[0].vedleggstype.kodebeskrivelse), "ERROR", Helpers.GetFullClassPath(() => form.ettersending[0].vedlegg[0].vedleggstype.kodeverdi));
            _validationResult.AddRule(skjema, "5721.1.8.6.3", null, "Kodebeskrivelsen '{0}' stemmer ikke med den valgte kodeverdien for vedleggstype. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/vedlegg-og-underskjema/vedlegg", Helpers.GetFullClassPath(() => form.ettersending[0].vedlegg[0].vedleggstype.kodebeskrivelse), "WARNING", Helpers.GetFullClassPath(() => form.ettersending[0].vedlegg[0].vedleggstype.kodeverdi));
            _validationResult.AddRule(skjema, "5721.1.8.6.4", null, "Du har lagt ved vedlegget '{0}'. Da bør du også fylle ut vedleggets dato.", Helpers.GetFullClassPath(() => form.ettersending[0].vedlegg[0].versjonsdato), "WARNING", Helpers.GetFullClassPath(() => form.ettersending[0].vedlegg[0].vedleggstype.kodeverdi));
            _validationResult.AddRule(skjema, "5721.1.8.6.5", null, "Du har lagt ved vedlegget '{0}'. Dersom dette er en ny versjon av et tidligere innsendt dokument, bør du fylle ut versjonsnummer.", Helpers.GetFullClassPath(() => form.ettersending[0].vedlegg[0].versjonsnummer), "WARNING", Helpers.GetFullClassPath(() => form.ettersending[0].vedlegg[0].vedleggstype.kodeverdi));
            //Underskjema
            _validationResult.AddRule(skjema, "5721.1.8.7", null, "Du må oppgi en kodeverdi for underskjema. Du kan sjekke gyldige underskjema på https://register.geonorge.no/kodelister/byggesoknad/vedlegg-og-underskjema/underskjema", Helpers.GetFullClassPath(() => form.ettersending[0].underskjema[0].kodeverdi), "ERROR", null);
            _validationResult.AddRule(skjema, "5721.1.8.7.1", null, "'{0}' er en ugyldig kodeverdi for underskjema. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/vedlegg-og-underskjema/underskjema", Helpers.GetFullClassPath(() => form.ettersending[0].underskjema[0].kodeverdi), "ERROR", null);
            _validationResult.AddRule(skjema, "5721.1.8.7.2", null, "Når underskjema er valgt, må kodebeskrivelse fylles ut. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/vedlegg-og-underskjema/underskjema", Helpers.GetFullClassPath(() => form.ettersending[0].underskjema[0].kodebeskrivelse), "ERROR", Helpers.GetFullClassPath(() => form.ettersending[0].underskjema[0].kodeverdi));
            _validationResult.AddRule(skjema, "5721.1.8.7.3", null, "Kodebeskrivelsen '{0}' stemmer ikke med den valgte kodeverdien for underskjema. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/vedlegg-og-underskjema/underskjema", Helpers.GetFullClassPath(() => form.ettersending[0].underskjema[0].kodebeskrivelse), "ERROR", Helpers.GetFullClassPath(() => form.ettersending[0].underskjema[0].kodeverdi));
            _validationResult.AddRule(skjema, "5721.1.8.8", null, "Du har valgt å ettersende vedlegg eller underskjema. Da må du legge ved minst én fil.", Helpers.GetFullClassPath(() => form.ettersending), "ERROR", "/Vedlegg");

            //Mangelbesvarelse
            _validationResult.AddRule(skjema, "5721.1.9", null, "Du har valgt å besvare en mangel fra kommunen. Da må du lage en tittel som beskriver det du svarer på.", Helpers.GetFullClassPath(() => form.mangelbesvarelse[0].tittel), "ERROR", Helpers.GetFullClassPath(() => form.mangelbesvarelse[0]));
            _validationResult.AddRule(skjema, "5721.1.9.1", null, "Du må velge om mangelen skal besvares senere.", Helpers.GetFullClassPath(() => form.mangelbesvarelse[0].erMangelBesvaresSenere), "ERROR", Helpers.GetFullClassPath(() => form.mangelbesvarelse[0]));
            _validationResult.AddRule(skjema, "5721.1.9.2", null, "Du har bedt om å få besvare mangelen senere. Da bør du skrive en kommentar til kommunen.", Helpers.GetFullClassPath(() => form.mangelbesvarelse[0].kommentar), "WARNING", Helpers.GetFullClassPath(() => form.mangelbesvarelse[0].erMangelBesvaresSenere));
            _validationResult.AddRule(skjema, "5721.1.9.2.1", null, "Du har valgt å besvare en mangel fra kommunen. Da må du enten skrive en kommentar eller fylle ut informasjon om hvilke vedlegg/underskjema du skal sende inn.", Helpers.GetFullClassPath(() => form.mangelbesvarelse[0]), "ERROR", Helpers.GetFullClassPath(() => form.mangelbesvarelse[0].erMangelBesvaresSenere));

            //Tema
            _validationResult.AddRule(skjema, "5721.1.9.4", null, "'{0}' er en ugyldig kodeverdi for tema. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/ebyggesak/tema", Helpers.GetFullClassPath(() => form.mangelbesvarelse[0].tema.kodeverdi), "ERROR", Helpers.GetFullClassPath(() => form.mangelbesvarelse[0].tema));
            _validationResult.AddRule(skjema, "5721.1.9.4.1", null, "Når tema er valgt, må kodebeskrivelse fylles ut. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/ebyggesak/tema", Helpers.GetFullClassPath(() => form.mangelbesvarelse[0].tema.kodebeskrivelse), "ERROR", Helpers.GetFullClassPath(() => form.mangelbesvarelse[0].tema.kodeverdi));
            _validationResult.AddRule(skjema, "5721.1.9.4.2", null, "Kodebeskrivelsen '{0}' stemmer ikke med den valgte kodeverdien for tema. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/ebyggesak/tema", Helpers.GetFullClassPath(() => form.mangelbesvarelse[0].tema.kodebeskrivelse), "WARNING", Helpers.GetFullClassPath(() => form.mangelbesvarelse[0].tema.kodeverdi));

            //Vedlegg & Underskjema
            _validationResult.AddRule(skjema, "5721.1.9.8", null, "Du har valgt å besvare mangelen med vedlegg eller underskjema. Da må du legge ved minst én fil.", Helpers.GetFullClassPath(() => form.mangelbesvarelse), "ERROR", "/Vedlegg");

            //vedlegg
            _validationResult.AddRule(skjema, "5721.1.9.5", null, "Du har lagt ved vedlegget '{0}'. Da må du også velge vedleggstype.", Helpers.GetFullClassPath(() => form.mangelbesvarelse[0].vedlegg[0].vedleggstype), "ERROR", Helpers.GetFullClassPath(() => form.mangelbesvarelse[0].vedlegg[0].filnavn));
            _validationResult.AddRule(skjema, "5721.1.9.5.1", null, "Du har valgt å legge ved '{0}'. Da må du også fylle ut et filnavn.", Helpers.GetFullClassPath(() => form.mangelbesvarelse[0].vedlegg[0].filnavn), "WARNING", Helpers.GetFullClassPath(() => form.mangelbesvarelse[0].vedlegg[0].vedleggstype));
            _validationResult.AddRule(skjema, "5721.1.9.6", null, "Du må oppgi en kodeverdi for vedleggstype. Du kan sjekke gyldige vedlegg på https://register.geonorge.no/kodelister/byggesoknad/vedlegg-og-underskjema/vedlegg", Helpers.GetFullClassPath(() => form.mangelbesvarelse[0].vedlegg[0].vedleggstype.kodeverdi), "ERROR", Helpers.GetFullClassPath(() => form.mangelbesvarelse[0].vedlegg[0].filnavn));
            _validationResult.AddRule(skjema, "5721.1.9.6.1", null, "'{0}' er en ugyldig kodeverdi for vedleggstype. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/vedlegg-og-underskjema/vedlegg ", Helpers.GetFullClassPath(() => form.mangelbesvarelse[0].vedlegg[0].vedleggstype.kodeverdi), "ERROR", Helpers.GetFullClassPath(() => form.mangelbesvarelse[0].vedlegg[0].filnavn));
            _validationResult.AddRule(skjema, "5721.1.9.6.2", null, "Når vedleggstype er valgt, må kodebeskrivelse fylles ut. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/vedlegg-og-underskjema/vedlegg", Helpers.GetFullClassPath(() => form.mangelbesvarelse[0].vedlegg[0].vedleggstype.kodebeskrivelse), "ERROR", Helpers.GetFullClassPath(() => form.mangelbesvarelse[0].vedlegg[0].vedleggstype.kodeverdi));
            _validationResult.AddRule(skjema, "5721.1.9.6.3", null, "Kodebeskrivelsen '{0}' stemmer ikke med den valgte kodeverdien for vedleggstype. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/vedlegg-og-underskjema/vedlegg", Helpers.GetFullClassPath(() => form.mangelbesvarelse[0].vedlegg[0].vedleggstype.kodebeskrivelse), "WARNING", Helpers.GetFullClassPath(() => form.mangelbesvarelse[0].vedlegg[0].vedleggstype.kodeverdi));
            _validationResult.AddRule(skjema, "5721.1.9.6.4", null, "Du har lagt ved vedlegget '{0}'. Da bør du også fylle ut vedleggets dato.", Helpers.GetFullClassPath(() => form.mangelbesvarelse[0].vedlegg[0].versjonsdato), "WARNING", Helpers.GetFullClassPath(() => form.mangelbesvarelse[0].vedlegg[0].vedleggstype.kodeverdi));
            _validationResult.AddRule(skjema, "5721.1.9.6.5", null, "Du har lagt ved vedlegget '{0}'. Dersom dette er en ny versjon av et tidligere innsendt dokument, bør du fylle ut versjonsnummer.", Helpers.GetFullClassPath(() => form.mangelbesvarelse[0].vedlegg[0].versjonsnummer), "WARNING", Helpers.GetFullClassPath(() => form.mangelbesvarelse[0].vedlegg[0].vedleggstype.kodeverdi));
            //Underskjema
            _validationResult.AddRule(skjema, "5721.1.9.7", null, "Du må oppgi en kodeverdi for underskjema. Du kan sjekke gyldige underskjema på https://register.geonorge.no/kodelister/byggesoknad/vedlegg-og-underskjema/underskjema", Helpers.GetFullClassPath(() => form.mangelbesvarelse[0].underskjema[0].kodeverdi), "ERROR", null);
            _validationResult.AddRule(skjema, "5721.1.9.7.1", null, "'{0}' er en ugyldig kodeverdi for underskjema. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/vedlegg-og-underskjema/underskjema", Helpers.GetFullClassPath(() => form.mangelbesvarelse[0].underskjema[0].kodeverdi), "ERROR", null);
            _validationResult.AddRule(skjema, "5721.1.9.7.2", null, "Når underskjema er valgt, må kodebeskrivelse fylles ut. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/vedlegg-og-underskjema/underskjema", Helpers.GetFullClassPath(() => form.mangelbesvarelse[0].underskjema[0].kodebeskrivelse), "ERROR", Helpers.GetFullClassPath(() => form.mangelbesvarelse[0].underskjema[0].kodeverdi));
            _validationResult.AddRule(skjema, "5721.1.9.7.3", null, "Kodebeskrivelsen '{0}' stemmer ikke med den valgte kodeverdien for underskjema. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/vedlegg-og-underskjema/underskjema", Helpers.GetFullClassPath(() => form.mangelbesvarelse[0].underskjema[0].kodebeskrivelse), "WARNING", Helpers.GetFullClassPath(() => form.mangelbesvarelse[0].underskjema[0].kodeverdi));

            _tiltakstypesForMangelbesvarelseAndEttersendingVedlegRule = new List<string>()
            {
                "Situasjonsplan",
                "TegningNyPlan",
                "TegningNyttSnitt",
                "TegningNyFasade",
                "Kart",
                "Avkjoerselsplan",
                "RedegjoerelseAndreNaturMiljoeforhold",
                "RedegjoerelseEstetikk",
                "RedegjoerelseUnntakTEK",
                "RedegjoerelseForurensetGrunn",
                "RedegjoerelseGrunnforhold",
                "RedegjoerelseSkredOgFlom",
                "UnderlagUtnytting",
                "Utomhusplan",
                "ByggesaksBIM",
            };
        }

        public ValidationResult Validate(SuppleringAvSoeknadType form, List<string> formSetElements)
        {
            Parallel.Invoke(
                () => { MetadataValidering(form); },
                () => { EiendomByggestedValidation(form); },
                () => { SoekerValidering(form); },
                () => { KommunensSaksnummerValidering(form); },
                () => { EttersendingOgMangelbesvarelseValidering(form); },
                () => { MangelbesvarelseValidering(form, formSetElements); },
                () => { EttersendingValidering(form, formSetElements); }
            );

            return _validationResult;
        }
        internal void MetadataValidering(SuppleringAvSoeknadType form)
        {
            if (string.IsNullOrEmpty(form.metadata?.fraSluttbrukersystem))
                _validationResult.AddMessage("5721.1.3.1", null);
        }

        internal void EiendomByggestedValidation(SuppleringAvSoeknadType form)
        {
            if (Helpers.ObjectIsNullOrEmpty(form.eiendomByggested))
            {
                _validationResult.AddMessage("5721.1.4", null);
            }
            else
            {
                foreach (EiendomType eiendomType in form.eiendomByggested)
                {

                    if (eiendomType.eiendomsidentifikasjon != null)
                    {
                        var kommunenummer = eiendomType.eiendomsidentifikasjon.kommunenummer;
                        var kommunenummerValideringStatus = _municipalityValidator.Validate_kommunenummerStatus(kommunenummer);
                        if (kommunenummerValideringStatus.Status != MunicipalityValidation.Ok)
                        {

                            switch (kommunenummerValideringStatus.Status)
                            {
                                case MunicipalityValidation.Empty:
                                    _validationResult.AddMessage("5721.1.4.1", null);
                                    break;
                                case MunicipalityValidation.Invalid:
                                    _validationResult.AddMessage("5721.1.4.2", new[] { kommunenummer });
                                    break;
                                case MunicipalityValidation.Expired:
                                    _validationResult.AddMessage("5721.1.4.3", new[] { kommunenummer, kommunenummerValideringStatus.Status.ToString() });
                                    break;
                            }
                        }
                        else
                        {
                            var gaardsnummer = eiendomType.eiendomsidentifikasjon.gaardsnummer;
                            var bruksnummer = eiendomType.eiendomsidentifikasjon.bruksnummer;

                            if (string.IsNullOrEmpty(gaardsnummer) || string.IsNullOrEmpty(bruksnummer))
                            {
                                if (string.IsNullOrEmpty(gaardsnummer))
                                    _validationResult.AddMessage("5721.1.4.4", null);

                                if (string.IsNullOrEmpty(bruksnummer))
                                    _validationResult.AddMessage("5721.1.4.4.1", null);
                            }
                            else
                            {
                                int gaardsnumerInt = 0;
                                int bruksnummerInt = 0;
                                if (int.TryParse(gaardsnummer, out gaardsnumerInt) && int.TryParse(bruksnummer, out bruksnummerInt))
                                {
                                    int festenummerInt = 0;
                                    int seksjonsnummerInt = 0;
                                    int.TryParse(eiendomType.eiendomsidentifikasjon.festenummer, out festenummerInt);
                                    int.TryParse(eiendomType.eiendomsidentifikasjon.seksjonsnummer, out seksjonsnummerInt);

                                    if (gaardsnumerInt < 0 || bruksnummerInt < 0)
                                    {
                                        if (gaardsnumerInt < 0)
                                            _validationResult.AddMessage("5721.1.4.5", new[] { eiendomType.eiendomsidentifikasjon.gaardsnummer });

                                        if (bruksnummerInt < 0)
                                            _validationResult.AddMessage("5721.1.4.6", new[] { eiendomType.eiendomsidentifikasjon.bruksnummer });

                                    }
                                    else
                                    {
                                        //## MATRIKKEL
                                        var matrikkelnrExist = _matrikkelService.MatrikkelnrExist(kommunenummer, gaardsnumerInt, bruksnummerInt, festenummerInt, seksjonsnummerInt);
                                        if (matrikkelnrExist.HasValue)
                                        {
                                            if (!matrikkelnrExist.Value)
                                            {
                                                _validationResult.AddMessage("5721.1.4.12", new[]
                                                {
                                                    kommunenummer,
                                                    eiendomType.eiendomsidentifikasjon.gaardsnummer,
                                                    eiendomType.eiendomsidentifikasjon.bruksnummer,
                                                    eiendomType.eiendomsidentifikasjon.festenummer,
                                                    eiendomType.eiendomsidentifikasjon.seksjonsnummer,
                                                });
                                            }
                                        }
                                        else
                                        {
                                            _validationResult.AddMessage("5721.1.4.13", new[]
                                            {
                                                kommunenummer,
                                                eiendomType.eiendomsidentifikasjon.gaardsnummer,
                                                eiendomType.eiendomsidentifikasjon.bruksnummer,
                                                eiendomType.eiendomsidentifikasjon.festenummer,
                                                eiendomType.eiendomsidentifikasjon.seksjonsnummer,
                                            });
                                        }

                                        if (!String.IsNullOrEmpty(eiendomType.bygningsnummer))
                                        {
                                            long bygningsnrLong = 0;
                                            if (!long.TryParse(eiendomType.bygningsnummer, out bygningsnrLong))
                                            {
                                                _validationResult.AddMessage("5721.1.4.7", new[] { eiendomType.bygningsnummer });
                                            }
                                            else
                                            {
                                                if (bygningsnrLong <= 0)
                                                {
                                                    _validationResult.AddMessage("5721.1.4.8", new[] { eiendomType.eiendomsidentifikasjon.gaardsnummer, eiendomType.eiendomsidentifikasjon.bruksnummer });
                                                }
                                                else
                                                {
                                                    ////## MATRIKKEL
                                                    var isBygningValid = _matrikkelService.IsBygningOnMatrikkelnr(bygningsnrLong, kommunenummer, gaardsnumerInt, bruksnummerInt, festenummerInt, seksjonsnummerInt);
                                                    if (!isBygningValid.GetValueOrDefault())
                                                    {
                                                        _validationResult.AddMessage("5721.1.4.14", new[] { eiendomType.bygningsnummer });
                                                    }
                                                }
                                            }
                                        }

                                        if (Helpers.ObjectIsNullOrEmpty(eiendomType.adresse))
                                        {
                                            _validationResult.AddMessage("5721.1.4.9", null);
                                        }
                                        else
                                        {
                                            if (string.IsNullOrEmpty(eiendomType.adresse?.adresselinje1))
                                            {
                                                _validationResult.AddMessage("5721.1.4.9.1", null);
                                            }
                                            else
                                            {
                                                if (String.IsNullOrEmpty(eiendomType.adresse.gatenavn) || String.IsNullOrEmpty(eiendomType.adresse.husnr))
                                                {
                                                    _validationResult.AddMessage("5721.1.4.9.2", null);
                                                }
                                                else
                                                {
                                                    //## MATRIKKEL
                                                    var isAdresseValid = _matrikkelService.IsVegadresseOnMatrikkelnr(eiendomType.adresse.gatenavn, eiendomType.adresse.husnr, eiendomType.adresse.bokstav, kommunenummer, gaardsnumerInt, bruksnummerInt, festenummerInt, seksjonsnummerInt);
                                                    if (isAdresseValid == null)
                                                    {
                                                        _validationResult.AddMessage("5721.1.4.15", null);
                                                    }
                                                    else if (!isAdresseValid.GetValueOrDefault())
                                                    {
                                                        _validationResult.AddMessage("5721.1.4.15.1", null);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if (String.IsNullOrEmpty(eiendomType.kommunenavn))
                    {
                        _validationResult.AddMessage("5721.1.4.10", null);
                    }

                    if (!String.IsNullOrEmpty(eiendomType.bolignummer))
                    {
                        if (GeneralValidations.Validate_bruksenhetsnummer(eiendomType.bolignummer).Status == ValidationStatus.Fail)
                        {
                            _validationResult.AddMessage("5721.1.4.11", null);
                        }
                    }
                }
            }
        }
        internal void SoekerValidering(SuppleringAvSoeknadType form)
        {
            if (Helpers.ObjectIsNullOrEmpty(form.ansvarligSoeker) && Helpers.ObjectIsNullOrEmpty(form.tiltakshaver))
            {
                _validationResult.AddMessage("5721.1.5", null);
            }
            else
            {
                if (!Helpers.ObjectIsNullOrEmpty(form.ansvarligSoeker))
                {
                    if (Helpers.ObjectIsNullOrEmpty(form.ansvarligSoeker.partstype))
                    {
                        _validationResult.AddMessage("5721.1.5.1", null);
                    }
                    else
                    {
                        if (!_codeListService.IsCodelistValid("Partstype", form.ansvarligSoeker.partstype.kodeverdi))
                        {
                            _validationResult.AddMessage("5721.1.5.2", new[] { form.ansvarligSoeker.partstype.kodeverdi });
                        }
                        else
                        {

                            if (form.ansvarligSoeker.partstype.kodeverdi == "Privatperson")
                            {
                                var foedselsnummerValidation = GeneralValidations.Validate_foedselsnummer(form.ansvarligSoeker.foedselsnummer);
                                switch (foedselsnummerValidation)
                                {
                                    case GeneralValidations.FoedselnumerValidation.Empty:
                                        _validationResult.AddMessage("5721.1.5.3", null);
                                        break;
                                    case GeneralValidations.FoedselnumerValidation.Invalid:
                                        _validationResult.AddMessage("5721.1.5.3.1", null);
                                        break;
                                    case GeneralValidations.FoedselnumerValidation.InvalidDigitsControl:
                                        _validationResult.AddMessage("5721.1.5.3.2", null);
                                        break;
                                    case GeneralValidations.FoedselnumerValidation.InvalidEncryption:
                                        _validationResult.AddMessage("5721.1.5.3.3", null);
                                        break;
                                }
                            }
                            else
                            {
                                var organisasjonsnummerValidation = GeneralValidations.Validate_OrgnummerEnum(form.ansvarligSoeker.organisasjonsnummer);
                                switch (organisasjonsnummerValidation)
                                {
                                    case GeneralValidations.OrganisasjonsnummerValidation.Empty:
                                        _validationResult.AddMessage("5721.1.5.4", null);
                                        break;
                                    case GeneralValidations.OrganisasjonsnummerValidation.InvalidDigitsControl:
                                        _validationResult.AddMessage("5721.1.5.4.1", new[] { form.ansvarligSoeker.organisasjonsnummer });
                                        break;
                                    case GeneralValidations.OrganisasjonsnummerValidation.Invalid:
                                        _validationResult.AddMessage("5721.1.5.4.2", new[] { form.ansvarligSoeker.organisasjonsnummer });
                                        break;
                                }

                                if (Helpers.ObjectIsNullOrEmpty(form.ansvarligSoeker.kontaktperson))
                                {
                                    _validationResult.AddMessage("5721.1.5.4.3", null);
                                }
                                else
                                {
                                    if (string.IsNullOrEmpty(form.ansvarligSoeker?.kontaktperson?.navn))
                                    {
                                        _validationResult.AddMessage("5721.1.5.4.3.1", null);
                                    }

                                    if (string.IsNullOrEmpty(form.ansvarligSoeker?.kontaktperson?.telefonnummer) && string.IsNullOrEmpty(form.ansvarligSoeker?.kontaktperson?.mobilnummer))
                                    {
                                        _validationResult.AddMessage("5721.1.5.4.3.2", null);
                                    }
                                    else
                                    {
                                        if (!string.IsNullOrEmpty(form.ansvarligSoeker?.kontaktperson?.telefonnummer))
                                        {
                                            var telefonNumber = form.ansvarligSoeker?.kontaktperson?.telefonnummer;
                                            var isValidTelefonNumber = telefonNumber.All(c => "+0123456789".Contains(c));
                                            if (!isValidTelefonNumber)
                                            {
                                                _validationResult.AddMessage("5721.1.5.4.3.3", null);
                                            }
                                        }
                                        if (!string.IsNullOrEmpty(form.ansvarligSoeker?.kontaktperson?.mobilnummer))
                                        {
                                            var mobilNummer = form.ansvarligSoeker?.kontaktperson?.mobilnummer;
                                            var isValidmobilnummer = mobilNummer.All(c => "+0123456789".Contains(c));
                                            if (!isValidmobilnummer)
                                            {
                                                _validationResult.AddMessage("5721.1.5.4.3.4", null);
                                            }
                                        }
                                    }

                                    if (string.IsNullOrEmpty(form.ansvarligSoeker.kontaktperson.epost))
                                        _validationResult.AddMessage("5721.1.5.4.3.5", null);
                                }
                            }
                        }
                    }
                    if (Helpers.ObjectIsNullOrEmpty(form.ansvarligSoeker.adresse))
                    {
                        _validationResult.AddMessage("5721.1.5.5", null);
                    }
                    else
                    {
                        if (String.IsNullOrEmpty(form.ansvarligSoeker.adresse.adresselinje1))
                        {
                            _validationResult.AddMessage("5721.1.5.5.1", null);
                        }
                        else
                        {
                            if (!CountryCodeHandler.IsCountryNorway(form.ansvarligSoeker.adresse.landkode))
                            {
                                if (!CountryCodeHandler.VerifyCountryCode(form.ansvarligSoeker.adresse.landkode))
                                {
                                    _validationResult.AddMessage("5721.1.5.5.2", null);
                                }
                            }
                            else
                            {
                                var postNr = form.ansvarligSoeker.adresse.postnr;
                                var landkode = form.ansvarligSoeker.adresse.landkode;

                                if (string.IsNullOrEmpty(postNr))
                                {
                                    _validationResult.AddMessage("5721.1.5.5.3", null);
                                }
                                else
                                {
                                    if (!GeneralValidations.postNumberMatch(form.ansvarligSoeker.adresse.postnr).Success)
                                    {
                                        _validationResult.AddMessage("5721.1.5.5.4", new[] { postNr });
                                    }
                                    else
                                    {
                                        var postnrValidation = _postalCodeProvider.ValidatePostnr(postNr, landkode);
                                        if (!Helpers.ObjectIsNullOrEmpty(postnrValidation))
                                        {
                                            if (!postnrValidation.Valid)
                                            {
                                                _validationResult.AddMessage("5721.1.5.5.5", null);
                                            }
                                            else
                                            {
                                                if (!postnrValidation.Result.Equals(form.ansvarligSoeker.adresse.poststed, StringComparison.CurrentCultureIgnoreCase))
                                                {
                                                    _validationResult.AddMessage("5721.1.5.5.6", new[] { postNr, form.ansvarligSoeker.adresse.poststed, postnrValidation.Result });
                                                }
                                            }
                                        }
                                        else
                                        {
                                            _validationResult.AddMessage("5721.1.5.5.7", null);
                                        }
                                    }
                                }
                            }
                        }
                    }



                    if (string.IsNullOrEmpty(form.ansvarligSoeker.telefonnummer) && string.IsNullOrEmpty(form.ansvarligSoeker.mobilnummer))
                    {
                        _validationResult.AddMessage("5721.1.5.5.8", null);

                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(form.ansvarligSoeker.telefonnummer))
                        {
                            var telefonNumber = form.ansvarligSoeker.telefonnummer;
                            var isValidTelefonNumber = telefonNumber.All(c => "+0123456789".Contains(c));
                            if (!isValidTelefonNumber)
                            {
                                _validationResult.AddMessage("5721.1.5.5.8.1", null);
                            }
                        }
                        if (!string.IsNullOrEmpty(form.ansvarligSoeker.mobilnummer))
                        {
                            var mobilNummer = form.ansvarligSoeker.mobilnummer;
                            var isValidmobilnummer = mobilNummer.All(c => "+0123456789".Contains(c));
                            if (!isValidmobilnummer)
                            {
                                _validationResult.AddMessage("5721.1.5.5.8.2", null);
                            }
                        }
                    }
                    if (string.IsNullOrEmpty(form.ansvarligSoeker.navn))
                        _validationResult.AddMessage("5721.1.5.5.9", null);
                    if (string.IsNullOrEmpty(form.ansvarligSoeker.epost))
                        _validationResult.AddMessage("5721.1.5.5.10", null);
                }
                else
                {
                    //**Validere tiltakshaver hvis ikke finnes ansvarligSoeker

                    if (Helpers.ObjectIsNullOrEmpty(form.tiltakshaver.partstype))
                    {
                        _validationResult.AddMessage("5721.1.5.6", null);
                    }
                    else
                    {
                        if (!_codeListService.IsCodelistValid("Partstype", form.tiltakshaver.partstype?.kodeverdi))
                        {
                            _validationResult.AddMessage("5721.1.5.6.1", new[] { form.tiltakshaver.partstype?.kodeverdi });
                        }
                        else
                        {
                            if (form.tiltakshaver.partstype?.kodeverdi == "Privatperson")
                            {
                                var foedselsnummerValidation = GeneralValidations.Validate_foedselsnummer(form.tiltakshaver.foedselsnummer);
                                switch (foedselsnummerValidation)
                                {
                                    case GeneralValidations.FoedselnumerValidation.Empty:
                                        _validationResult.AddMessage("5721.1.5.6.2", null);
                                        break;
                                    case GeneralValidations.FoedselnumerValidation.Invalid:
                                        _validationResult.AddMessage("5721.1.5.6.2.1", null);
                                        break;
                                    case GeneralValidations.FoedselnumerValidation.InvalidDigitsControl:
                                        _validationResult.AddMessage("5721.1.5.6.2.2", null);
                                        break;
                                    case GeneralValidations.FoedselnumerValidation.InvalidEncryption:
                                        _validationResult.AddMessage("5721.1.5.6.2.3", null);
                                        break;
                                }
                            }
                            else
                            {
                                var organisasjonsnummerValidation = GeneralValidations.Validate_OrgnummerEnum(form.tiltakshaver.organisasjonsnummer);
                                switch (organisasjonsnummerValidation)
                                {
                                    case GeneralValidations.OrganisasjonsnummerValidation.Empty:
                                        _validationResult.AddMessage("5721.1.5.6.3", null);
                                        break;
                                    case GeneralValidations.OrganisasjonsnummerValidation.Invalid:
                                        _validationResult.AddMessage("5721.1.5.6.3.1", new[] { form.tiltakshaver.organisasjonsnummer });
                                        break;
                                    case GeneralValidations.OrganisasjonsnummerValidation.InvalidDigitsControl:
                                        _validationResult.AddMessage("5721.1.5.6.3.2", new[] { form.tiltakshaver.organisasjonsnummer });
                                        break;
                                }

                                if (Helpers.ObjectIsNullOrEmpty(form.tiltakshaver.kontaktperson))
                                {
                                    _validationResult.AddMessage("5721.1.5.6.3.3", null);
                                }
                                else
                                {
                                    if (string.IsNullOrEmpty(form.tiltakshaver?.kontaktperson?.navn))
                                    {
                                        _validationResult.AddMessage("5721.1.5.6.3.3.1", null);
                                    }

                                    if (string.IsNullOrEmpty(form.tiltakshaver?.kontaktperson?.telefonnummer) && string.IsNullOrEmpty(form.tiltakshaver?.kontaktperson?.mobilnummer))
                                    {
                                        _validationResult.AddMessage("5721.1.5.6.3.3.2", null);
                                    }
                                    else
                                    {
                                        if (!string.IsNullOrEmpty(form.tiltakshaver?.kontaktperson?.telefonnummer))
                                        {
                                            var telefonNumber = form.tiltakshaver?.kontaktperson?.telefonnummer;
                                            var isValidTelefonNumber = telefonNumber.All(c => "+0123456789".Contains(c));
                                            if (!isValidTelefonNumber)
                                            {
                                                _validationResult.AddMessage("5721.1.5.6.3.3.3", null);
                                            }
                                        }
                                        if (!string.IsNullOrEmpty(form.tiltakshaver?.kontaktperson?.mobilnummer))
                                        {
                                            var mobilNummer = form.tiltakshaver?.kontaktperson?.mobilnummer;
                                            var isValidmobilnummer = mobilNummer.All(c => "+0123456789".Contains(c));
                                            if (!isValidmobilnummer)
                                            {
                                                _validationResult.AddMessage("5721.1.5.6.3.3.4", null);
                                            }
                                        }
                                    }

                                    if (string.IsNullOrEmpty(form.tiltakshaver.kontaktperson.epost))
                                        _validationResult.AddMessage("5721.1.5.6.3.3.5", null);
                                }
                            }
                        }
                    }
                    if (Helpers.ObjectIsNullOrEmpty(form.tiltakshaver.adresse))
                    {
                        _validationResult.AddMessage("5721.1.5.6.4", null);
                    }
                    else
                    {
                        if (String.IsNullOrEmpty(form.tiltakshaver.adresse.adresselinje1))
                        {
                            _validationResult.AddMessage("5721.1.5.6.4.1", null);
                        }
                        else
                        {
                            if (!CountryCodeHandler.IsCountryNorway(form.tiltakshaver.adresse.landkode))
                            {
                                if (!CountryCodeHandler.VerifyCountryCode(form.tiltakshaver.adresse.landkode))
                                {
                                    _validationResult.AddMessage("5721.1.5.6.4.2", null);
                                }
                            }
                            else
                            {
                                var postNr = form.tiltakshaver.adresse.postnr;
                                var landkode = form.tiltakshaver.adresse.landkode;

                                if (string.IsNullOrEmpty(postNr))
                                {
                                    _validationResult.AddMessage("5721.1.5.6.4.3", null);
                                }
                                else
                                {
                                    if (!GeneralValidations.postNumberMatch(form.tiltakshaver.adresse.postnr).Success)
                                    {
                                        _validationResult.AddMessage("5721.1.5.6.4.4", new[] { postNr });
                                    }
                                    else
                                    {
                                        var postnrValidation = _postalCodeProvider.ValidatePostnr(postNr, landkode);
                                        if (!Helpers.ObjectIsNullOrEmpty(postnrValidation))
                                        {
                                            if (!postnrValidation.Valid)
                                            {
                                                _validationResult.AddMessage("5721.1.5.6.4.5", null);
                                            }
                                            else
                                            {
                                                if (!postnrValidation.Result.Equals(form.tiltakshaver.adresse.poststed, StringComparison.CurrentCultureIgnoreCase))
                                                {
                                                    _validationResult.AddMessage("5721.1.5.6.4.6", new[] { postNr, form.tiltakshaver.adresse.poststed, postnrValidation.Result });
                                                }
                                            }
                                        }
                                        else
                                        {
                                            _validationResult.AddMessage("5721.1.5.6.4.7", null);
                                        }
                                    }
                                }
                            }
                        }
                    }


                    if (string.IsNullOrEmpty(form.tiltakshaver.telefonnummer) && string.IsNullOrEmpty(form.tiltakshaver.mobilnummer))
                    {
                        _validationResult.AddMessage("5721.1.5.6.5", null);

                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(form.tiltakshaver.telefonnummer))
                        {
                            var telefonNumber = form.tiltakshaver?.telefonnummer;
                            var isValidTelefonNumber = telefonNumber.All(c => "+0123456789".Contains(c));
                            if (!isValidTelefonNumber)
                            {
                                _validationResult.AddMessage("5721.1.5.6.5.1", null);
                            }
                        }
                        if (!string.IsNullOrEmpty(form.tiltakshaver.mobilnummer))
                        {
                            var mobilNummer = form.tiltakshaver.mobilnummer;
                            var isValidmobilnummer = mobilNummer.All(c => "+0123456789".Contains(c));
                            if (!isValidmobilnummer)
                            {
                                _validationResult.AddMessage("5721.1.5.6.5.2", null);
                            }
                        }
                    }

                    if (string.IsNullOrEmpty(form.tiltakshaver.navn))
                        _validationResult.AddMessage("5721.1.5.6.6", null);

                    if (string.IsNullOrEmpty(form.tiltakshaver.epost))
                        _validationResult.AddMessage("5721.1.5.6.7", null);

                }
            }
        }
        internal void KommunensSaksnummerValidering(SuppleringAvSoeknadType form)
        {
            if (!Helpers.ObjectIsNullOrEmpty(form.mangelbesvarelse))
            {
                if (Helpers.ObjectIsNullOrEmpty(form.kommunensSaksnummer))
                {
                    _validationResult.AddMessage("5721.1.6", null);
                }
                else
                {
                    if (string.IsNullOrEmpty(form.kommunensSaksnummer.saksaar))
                    {
                        _validationResult.AddMessage("5721.1.6.1", null);
                    }

                    if (string.IsNullOrEmpty(form.kommunensSaksnummer.sakssekvensnummer))
                    {
                        _validationResult.AddMessage("5721.1.6.2", null);
                    }
                }
            }
            else
            {
                if (Helpers.ObjectIsNullOrEmpty(form.kommunensSaksnummer))
                {
                    _validationResult.AddMessage("5721.1.6.3", null);
                }
                else
                {
                    if (string.IsNullOrEmpty(form.kommunensSaksnummer.saksaar))
                    {
                        _validationResult.AddMessage("5721.1.6.3.1", null);
                    }

                    if (string.IsNullOrEmpty(form.kommunensSaksnummer.sakssekvensnummer))
                    {
                        _validationResult.AddMessage("5721.1.6.3.2", null);
                    }
                }

            }
        }

        internal void EttersendingOgMangelbesvarelseValidering(SuppleringAvSoeknadType form)
        {
            if (Helpers.ObjectIsNullOrEmpty(form.ettersending) && Helpers.ObjectIsNullOrEmpty(form.mangelbesvarelse))
            {
                _validationResult.AddMessage("5721.1.7", null);
            }

            if (!Helpers.ObjectIsNullOrEmpty(form.ettersending))
            {

            }
        }
        internal void EttersendingValidering(SuppleringAvSoeknadType form, List<string> formSetElements)
        {
            if (!Helpers.ObjectIsNullOrEmpty(form.ettersending))
            {
                var haveAnyAttachment = form.ettersending.Any(e =>
                    !Helpers.ObjectIsNullOrEmpty(e.vedlegg) ||
                    !Helpers.ObjectIsNullOrEmpty(e.underskjema));

                if (haveAnyAttachment && (formSetElements == null || formSetElements.All(string.IsNullOrEmpty)))
                {
                    _validationResult.AddMessage("5721.1.8.8", null);
                }


                foreach (var ettersending in form.ettersending)
                {
                    if (string.IsNullOrEmpty(ettersending.tittel))
                        _validationResult.AddMessage("5721.1.8", null);

                    if (Helpers.ObjectIsNullOrEmpty(ettersending.vedlegg) && Helpers.ObjectIsNullOrEmpty(ettersending.underskjema) && string.IsNullOrEmpty(ettersending.kommentar))
                    {
                        _validationResult.AddMessage("5721.1.8.1", null);
                    }

                    if (!Helpers.ObjectIsNullOrEmpty(ettersending.tema))
                    {
                        if (!_codeListService.IsCodelistValid("tema", ettersending.tema.kodeverdi,
                            RegistryType.Ebyggesak))
                        {
                            _validationResult.AddMessage("5721.1.8.2", new[] { ettersending.tema.kodeverdi });
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(ettersending.tema?.kodebeskrivelse))
                            {
                                _validationResult.AddMessage("5721.1.8.3", null);
                            }
                            else
                            {
                                if (!_codeListService.IsCodelistLabelValid("tema", ettersending.tema.kodeverdi,
                                    ettersending.tema.kodebeskrivelse, RegistryType.Ebyggesak))
                                {
                                    _validationResult.AddMessage("5721.1.8.4",
                                        new[] { ettersending.tema.kodebeskrivelse });
                                }
                            }
                        }
                    }

                    if (!Helpers.ObjectIsNullOrEmpty(ettersending.vedlegg))
                    {
                        foreach (var vedlegg in ettersending.vedlegg)
                        {
                            if (!Helpers.ObjectIsNullOrEmpty(vedlegg))
                            {
                                if (Helpers.ObjectIsNullOrEmpty(vedlegg.vedleggstype))
                                {
                                    _validationResult.AddMessage("5721.1.8.5", new[] { vedlegg.filnavn });
                                }
                                else
                                {
                                    if (string.IsNullOrEmpty(vedlegg.filnavn))
                                    {
                                        _validationResult.AddMessage("5721.1.8.5.1", new[] { vedlegg.vedleggstype?.kodeverdi });
                                    }

                                    if (string.IsNullOrEmpty(vedlegg.vedleggstype.kodeverdi))
                                    {
                                        _validationResult.AddMessage("5721.1.8.6", null);
                                    }
                                    else
                                    {
                                        if (!_codeListService.IsCodelistValid("vedlegg-og-underskjema/vedlegg", vedlegg.vedleggstype.kodeverdi))
                                        {
                                            _validationResult.AddMessage("5721.1.8.6.1", new[] { vedlegg.vedleggstype.kodeverdi });
                                        }
                                        else
                                        {
                                            if (string.IsNullOrEmpty(vedlegg.vedleggstype?.kodebeskrivelse))
                                            {
                                                _validationResult.AddMessage("5721.1.8.6.2", null);
                                            }
                                            else
                                            {
                                                if (!_codeListService.IsCodelistLabelValid("vedlegg-og-underskjema/vedlegg", vedlegg.vedleggstype.kodeverdi, vedlegg.vedleggstype.kodebeskrivelse))
                                                {
                                                    _validationResult.AddMessage("5721.1.8.6.3",
                                                        new[] { vedlegg.vedleggstype?.kodebeskrivelse });
                                                }
                                                else
                                                {
                                                    if (_tiltakstypesForMangelbesvarelseAndEttersendingVedlegRule.Any(t => t.Equals(vedlegg.vedleggstype.kodeverdi)))
                                                    {
                                                        if (!vedlegg.versjonsdato.HasValue)
                                                            _validationResult.AddMessage("5721.1.8.6.4", new[] { vedlegg.vedleggstype?.kodebeskrivelse });
                                                        if (string.IsNullOrEmpty(vedlegg.versjonsnummer))
                                                            _validationResult.AddMessage("5721.1.8.6.5", new[] { vedlegg.vedleggstype?.kodebeskrivelse });
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if (!Helpers.ObjectIsNullOrEmpty(ettersending.underskjema))
                    {
                        foreach (var underskjema in ettersending.underskjema)
                        {
                            if (string.IsNullOrEmpty(underskjema.kodeverdi))
                            {
                                _validationResult.AddMessage("5721.1.8.7", null);
                            }
                            else
                            {
                                if (!_codeListService.IsCodelistValid("vedlegg-og-underskjema/underskjema", underskjema.kodeverdi))
                                {
                                    _validationResult.AddMessage("5721.1.8.7.1", new[] { underskjema.kodeverdi });
                                }
                                else
                                {
                                    if (string.IsNullOrEmpty(underskjema.kodebeskrivelse))
                                    {
                                        _validationResult.AddMessage("5721.1.8.7.2", null);
                                    }
                                    else
                                    {
                                        if (!_codeListService.IsCodelistLabelValid("vedlegg-og-underskjema/underskjema", underskjema.kodeverdi, underskjema.kodebeskrivelse))
                                        {
                                            _validationResult.AddMessage("5721.1.8.7.3", new[] { underskjema.kodebeskrivelse });
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        internal void MangelbesvarelseValidering(SuppleringAvSoeknadType form, List<string> formSetElements)
        {
            if (!Helpers.ObjectIsNullOrEmpty(form.mangelbesvarelse))
            {
                foreach (var mangelbesvarelse in form.mangelbesvarelse)
                {
                    if (string.IsNullOrEmpty(mangelbesvarelse.tittel))
                        _validationResult.AddMessage("5721.1.9", null);

                    if (!mangelbesvarelse.erMangelBesvaresSenere.HasValue)
                    {
                        _validationResult.AddMessage("5721.1.9.1", null);
                    }
                    else
                    {
                        if (mangelbesvarelse.erMangelBesvaresSenere.GetValueOrDefault())
                        {
                            if (string.IsNullOrEmpty(mangelbesvarelse.kommentar))
                            {
                                _validationResult.AddMessage("5721.1.9.2", null);
                            }
                        }
                        else
                        {
                            if (Helpers.ObjectIsNullOrEmpty(mangelbesvarelse.vedlegg) && Helpers.ObjectIsNullOrEmpty(mangelbesvarelse.underskjema) && string.IsNullOrEmpty(mangelbesvarelse.kommentar))
                            {
                                _validationResult.AddMessage("5721.1.9.2.1", null);
                            }
                        }
                    }

                    if (!Helpers.ObjectIsNullOrEmpty(mangelbesvarelse.tema))
                    {
                        if (!_codeListService.IsCodelistValid("tema", mangelbesvarelse.tema.kodeverdi, RegistryType.Ebyggesak))
                        {
                            _validationResult.AddMessage("5721.1.9.4", new[] { mangelbesvarelse.tema.kodeverdi });
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(mangelbesvarelse?.tema.kodebeskrivelse))
                            {
                                _validationResult.AddMessage("5721.1.9.4.1", null);
                            }
                            else
                            {
                                if (!_codeListService.IsCodelistLabelValid("tema", mangelbesvarelse.tema.kodeverdi, mangelbesvarelse.tema.kodebeskrivelse, RegistryType.Ebyggesak))
                                {
                                    _validationResult.AddMessage("5721.1.9.4.2", new[] { mangelbesvarelse.tema.kodebeskrivelse });
                                }
                            }
                        }
                    }

                    //Vedlegg
                    if (!Helpers.ObjectIsNullOrEmpty(mangelbesvarelse.vedlegg))
                    {
                        foreach (var vedlegg in mangelbesvarelse.vedlegg)
                        {
                            if (!Helpers.ObjectIsNullOrEmpty(vedlegg))
                            {
                                if (Helpers.ObjectIsNullOrEmpty(vedlegg.vedleggstype))
                                {
                                    _validationResult.AddMessage("5721.1.9.5", new[] { vedlegg.filnavn });
                                }
                                else
                                {
                                    if (string.IsNullOrEmpty(vedlegg.filnavn))
                                    {
                                        _validationResult.AddMessage("5721.1.9.5.1", new[] { vedlegg.vedleggstype?.kodeverdi });
                                    }

                                    if (string.IsNullOrEmpty(vedlegg.vedleggstype.kodeverdi))
                                    {
                                        _validationResult.AddMessage("5721.1.9.6", null);
                                    }
                                    else
                                    {
                                        if (!_codeListService.IsCodelistValid("vedlegg-og-underskjema/vedlegg", vedlegg.vedleggstype.kodeverdi))
                                        {
                                            _validationResult.AddMessage("5721.1.9.6.1", new[] { vedlegg.vedleggstype.kodeverdi });
                                        }
                                        else
                                        {
                                            if (string.IsNullOrEmpty(vedlegg.vedleggstype?.kodebeskrivelse))
                                            {
                                                _validationResult.AddMessage("5721.1.9.6.2", null);
                                            }
                                            else
                                            {
                                                if (!_codeListService.IsCodelistLabelValid("vedlegg-og-underskjema/vedlegg", vedlegg.vedleggstype.kodeverdi, vedlegg.vedleggstype.kodebeskrivelse))
                                                {
                                                    _validationResult.AddMessage("5721.1.9.6.3", new[] { vedlegg.vedleggstype?.kodebeskrivelse });
                                                }
                                                else
                                                {
                                                    if (_tiltakstypesForMangelbesvarelseAndEttersendingVedlegRule.Any(t => t.Equals(vedlegg.vedleggstype.kodeverdi)))
                                                    {
                                                        if (!vedlegg.versjonsdato.HasValue)
                                                            _validationResult.AddMessage("5721.1.9.6.4", new[] { vedlegg.vedleggstype?.kodebeskrivelse });
                                                        if (string.IsNullOrEmpty(vedlegg.versjonsnummer))
                                                            _validationResult.AddMessage("5721.1.9.6.5", new[] { vedlegg.vedleggstype?.kodebeskrivelse });
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    //Underskjema
                    if (!Helpers.ObjectIsNullOrEmpty(mangelbesvarelse.underskjema))
                    {
                        foreach (var underskjema in mangelbesvarelse.underskjema)
                        {
                            if (string.IsNullOrEmpty(underskjema.kodeverdi))
                            {
                                _validationResult.AddMessage("5721.1.9.7", null);
                            }
                            else
                            {
                                if (!_codeListService.IsCodelistValid("vedlegg-og-underskjema/underskjema", underskjema.kodeverdi))
                                {
                                    _validationResult.AddMessage("5721.1.9.7.1", new[] { underskjema.kodeverdi });
                                }
                                else
                                {
                                    if (string.IsNullOrEmpty(underskjema.kodebeskrivelse))
                                    {
                                        _validationResult.AddMessage("5721.1.9.7.2", null);
                                    }
                                    else
                                    {
                                        if (!_codeListService.IsCodelistLabelValid("vedlegg-og-underskjema/underskjema", underskjema.kodeverdi, underskjema.kodebeskrivelse))
                                        {
                                            _validationResult.AddMessage("5721.1.9.7.3", new[] { underskjema.kodebeskrivelse });
                                        }
                                    }
                                }
                            }
                        }
                    }

                }
                var haveAnyAttachment = form.mangelbesvarelse.Any(e =>
                    !Helpers.ObjectIsNullOrEmpty(e.vedlegg) ||
                    !Helpers.ObjectIsNullOrEmpty(e.underskjema));
                if (haveAnyAttachment && (formSetElements == null || formSetElements.All(e => string.IsNullOrEmpty(e))))
                {
                    _validationResult.AddMessage("5721.1.9.8", null);
                }

            }
        }

    }
}


