using DIBK.FBygg.Altinn.MapperCodelist;
using KommIT.FIKS.AdapterAltinnSvarUt.Resources;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Validations;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;
using no.kxml.skjema.dibk.endringavtillatelse;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.SvarUtForm
{
    public class EndringAvTillatelseFormValidator
    {
        private CodeListService _codeListService = new CodeListService();

        internal ValidationResult _validationResult;
        internal List<string> _tiltakstyperISoeknad = new List<string>();

        internal ValidationResult GetResult() => _validationResult;

        private readonly MunicipalityValidator _municipalityValidator;

        public EndringAvTillatelseFormValidator()
        {
            _municipalityValidator = new MunicipalityValidator();

            string _skjema = "EndringAvTillatelse";
            var form = new EndringAvTillatelseType();
            _validationResult = new ValidationResult()
            {
                Soknadtype = "ES"
            };

            //MinstEtTiltak - Alle
            _validationResult.AddRule(_skjema, "4402.1.1", "", "Minst en beskrivelse av tiltak må være registrert.", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.type), "ERROR", "");
            _validationResult.AddRule(_skjema, "4402.1.2.2", "", "Ugyldig kodeverdi({0}) i henhold til kodeliste for tiltakstype.", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.type[0].kodeverdi), "ERROR", "");
            _validationResult.AddRule(_skjema, "4402.1.2.15", "", "Når tiltakstype er valgt må kodebeskrivelse angis.", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.type[0].kodebeskrivelse), "ERROR", "");
            _validationResult.AddRule(_skjema, "4402.1.2.3", "", "Ugyldig kodebeskrivelse ({0}) i henhold til kodeliste for tiltakstype.", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.type[0].kodebeskrivelse), "WARNING", "");
            //--

            //MatrikkelInformasjonValidering - Bruk, Formaal
            _validationResult.AddRule(_skjema, "4402.1.2.4", "", "Matrikkelinformasjon bør fylles ut.", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.bruk), "WARNING", "");
            _validationResult.AddRule(_skjema, "4402.1.2.5", "", "Ugyldig kodeverdi({0}) i henhold til kodeliste for næringsgruppe.", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.bruk.naeringsgruppe.kodeverdi), "ERROR", "");
            _validationResult.AddRule(_skjema, "4402.1.2.6", "", "Ugyldig kodeverdi({0}) i henhold til kodeliste for bygningstype.", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.bruk.bygningstype.kodeverdi), "ERROR", "");
            _validationResult.AddRule(_skjema, "4402.1.2.16", "", "Når bygningstype er valgt må kodebeskrivelse angis.", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.bruk.bygningstype.kodebeskrivelse), "ERROR", "");
            _validationResult.AddRule(_skjema, "4402.1.2.7", "", "Ugyldig kodebeskrivelse({0}) i henhold til kodeliste for bygningstype.", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.bruk.bygningstype.kodebeskrivelse), "WARNING", "");
            _validationResult.AddRule(_skjema, "4402.1.2.8", "", "Ugyldig kodeverdi({0}) i henhold til kodeliste for tiltaksformål.", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.bruk.tiltaksformaal[0].kodeverdi), "ERROR", "");
            _validationResult.AddRule(_skjema, "4402.1.35", "", "Når tiltaksformål er registrert som 'annet' må beskrivelse av planlagt formål må fylles ut.", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.bruk.beskrivPlanlagtFormaal), "ERROR", "");
            //--
            //SoekerValidation - Alle
            _validationResult.AddRule(_skjema, "4402.1.2.12", "", "Ugyldig kodeverdi({0}) i henhold til kodeliste for tiltakshaver.", Helpers.GetFullClassPath(() => form.tiltakshaver.partstype.kodeverdi), "ERROR", "");
            _validationResult.AddRule(_skjema, "4402.1.53", "", ValidationError.TiltakshaverFoedselnummer, Helpers.GetFullClassPath(() => form.tiltakshaver.foedselsnummer), "ERROR", "");
            _validationResult.AddRule(_skjema, "4402.1.54", "", ValidationError.TiltakshaverOrganisasjonsnummer, Helpers.GetFullClassPath(() => form.tiltakshaver.organisasjonsnummer), "ERROR", "");
            _validationResult.AddRule(_skjema, "4402.1.55", "", ValidationError.TiltakshaverKontaktperson, Helpers.GetFullClassPath(() => form.tiltakshaver.kontaktperson), "WARNING", "");
            _validationResult.AddRule(_skjema, "4402.1.56", "", ValidationError.TiltakshaverPoststed, Helpers.GetFullClassPath(() => form.tiltakshaver.adresse.poststed), "ERROR", "");
            _validationResult.AddRule(_skjema, "4402.1.57", "", ValidationError.TiltakshaverPostnr, Helpers.GetFullClassPath(() => form.tiltakshaver.adresse.postnr), "ERROR", "");
            _validationResult.AddRule(_skjema, "4402.1.60", "", ValidationError.TiltakshaverTelefonEllerMobil, Helpers.GetFullClassPath(() => form.tiltakshaver.telefonnummer), "WARNING", "");
            _validationResult.AddRule(_skjema, "4402.1.2.14", "", "Ugyldig kodeverdi({0}) i henhold til kodeliste for ansvarlig søker.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.partstype.kodeverdi), "ERROR", "");
            _validationResult.AddRule(_skjema, "4402.1.58", "", ValidationError.AnsvarligSoekerFoedselsnummer, Helpers.GetFullClassPath(() => form.ansvarligSoeker.foedselsnummer), "ERROR", "");
            _validationResult.AddRule(_skjema, "4402.1.62", "", "Validering av organisasjonsnummer for ansvarlig søker: {0}.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.organisasjonsnummer), "ERROR", "");
            _validationResult.AddRule(_skjema, "4402.1.63", "", ValidationError.AnsvarligSoekerKontaktperson, Helpers.GetFullClassPath(() => form.ansvarligSoeker.kontaktperson), "WARNING", "");
            _validationResult.AddRule(_skjema, "4402.1.64", "", ValidationError.AnsvarligSoekerPoststed, Helpers.GetFullClassPath(() => form.ansvarligSoeker.adresse.poststed), "ERROR", "");
            _validationResult.AddRule(_skjema, "4402.1.52", "", ValidationError.AnsvarligSoekerPostnr, Helpers.GetFullClassPath(() => form.ansvarligSoeker.adresse.postnr), "ERROR", "");
            _validationResult.AddRule(_skjema, "4402.1.59", "", ValidationError.AnsvarligSoekerTelefonEllerMobil, Helpers.GetFullClassPath(() => form.ansvarligSoeker.telefonnummer), "WARNING", "");
            _validationResult.AddRule(_skjema, "4402.1.61", "", "Feltene navn, adresse og hvorvidt tiltakshaver og/eller ansvarlig søker er en person eller et foretak, må alle være med for enten ansvarlig søker eller tiltakshaver.", Helpers.GetFullClassPath(() => form), "ERROR", "");
            //MinstEnEndringValidering - Alle
            _validationResult.AddRule(_skjema, "4402.1.6", "1.67", "Minst én endring må være avkrysset i søknaden på en av: endring areal, endring plassering, endring formål, endring bruk, endring annet.", Helpers.GetFullClassPath(() => form), "ERROR", "");
            //EiendomByggestedValidation - Alle
            _validationResult.AddRule(_skjema, "4402.1.2", "", "Ugyldig kommunenummer ({0})", Helpers.GetFullClassPath(() => form.eiendomByggested[0].eiendomsidentifikasjon.kommunenummer), "ERROR", "");
            _validationResult.AddRule(_skjema, "4402.1.3", "", "Minst én eiendom må være definert.", Helpers.GetFullClassPath(() => form.eiendomByggested), "ERROR", "");
            _validationResult.AddRule(_skjema, "4402.1.7", "", "Kommunenummer må fylles ut for eiendom / byggested.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].eiendomsidentifikasjon.kommunenummer), "ERROR", "");
            _validationResult.AddRule(_skjema, "4402.1.14", "1.72", ValidationError.Matrikkelnummer, Helpers.GetFullClassPath(() => form.eiendomByggested[0].eiendomsidentifikasjon), "WARNING", "");
            _validationResult.AddRule(_skjema, "4402.1.15", "1.72", "Hvis bygningsnummer er oppgitt for byggested, bør den være gyldig i Matrikkelen på aktuelt matrikkelnummer.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].bygningsnummer), "WARNING", "");
            _validationResult.AddRule(_skjema, "4402.1.18", "1.72", "Hvis vegadresse er oppgitt for byggested, bør den være gyldig i Matrikkelen på aktuelt matrikkelnummer.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].adresse.gatenavn), "WARNING", "");
            _validationResult.AddRule(_skjema, "4402.1.65", "", "Adresse er angitt for byggested, men er ikke validert mot Matrikkelen", Helpers.GetFullClassPath(() => form.eiendomByggested[0].adresse.gatenavn), "WARNING", "");
            _validationResult.AddRule(_skjema, "4402.1.7.1", "", ValidationError.EiendomByggestedKommunenavn, Helpers.GetFullClassPath(() => form.eiendomByggested[0].kommunenavn), "WARNING", "");
            _validationResult.AddRule(_skjema, "4402.1.8", "", ValidationError.Bruksenhetsnummer, Helpers.GetFullClassPath(() => form.eiendomByggested[0].bolignummer), "ERROR", "");
            //VarslingValidering - Alle
            _validationResult.AddRule(_skjema, "4402.1.9", "2.1", "Det kreves nabovarsling for tiltaket. Underskjema 'opplysninger gitt i nabovarsel' (gjenpart nabovarsel) eller 'vedlegg nabovarsel' må derfor legges ved.", Helpers.GetFullClassPath((() => form.varsling.fritattFraNabovarsling)), "ERROR", "");
            _validationResult.AddRule(_skjema, "4402.1.78", "", "Informasjon om nabovarsling må fylles ut.", Helpers.GetFullClassPath(() => form.varsling), "ERROR", "");
            _validationResult.AddRule(_skjema, "4402.1.10", "2.2", "Når nabovarsling kreves for tiltaket skal vedlegg 'kvittering for nabovarsel' legges ved.", Helpers.GetFullClassPath(() => form.varsling.fritattFraNabovarsling), "ERROR", "");
            _validationResult.AddRule(_skjema, "4402.1.11", "2.6", "Når det foreligger merknader fra nabovarsling, må antall merknader fylles ut.", Helpers.GetFullClassPath(() => form.varsling.antallMerknader), "ERROR", Helpers.GetFullClassPath(() => form.varsling.foreliggerMerknader));
            _validationResult.AddRule(_skjema, "4402.1.12", "2.7", "Når det foreligger merknader fra nabovarsling, må vurdering av merknader fylles ut", Helpers.GetFullClassPath(() => form.varsling.vurderingAvMerknader), "ERROR", Helpers.GetFullClassPath(() => form.varsling.foreliggerMerknader));
            _validationResult.AddRule(_skjema, "4402.1.13", "2.7", "Det foreligger merknader fra nabovarslingen. Vedlegget ‘nabomerknader’ må derfor legges ved.", Helpers.GetFullClassPath((() => form.varsling.foreliggerMerknader)), "ERROR", "");
            // DispensasjonValidering - Dispensasjon
            _validationResult.AddRule(_skjema, "4402.1.17", null, "Det er valgt at det skal søkes om dispensasjon, men det mangler informasjon om dispensasjonen.", Helpers.GetFullClassPath(() => form.dispensasjon), "ERROR", Helpers.GetFullClassPath(() => form.endringSomKreverDispensasjon));
            _validationResult.AddRule(_skjema, "4402.1.17.1", null, "Det er valgt at det skal søkes om dispensasjon. Da må du velge en dispensasjonstype. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/byggesoknad/dispensasjonstype", Helpers.GetFullClassPath(() => form.dispensasjon[0].dispensasjonstype), "ERROR", Helpers.GetFullClassPath(() => form.endringSomKreverDispensasjon));
            _validationResult.AddRule(_skjema, "4402.1.2.1", null, "'{0}' er en ugyldig kodeverdi for dispensasjonstype. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/kodelister/byggesoknad/plantype", Helpers.GetFullClassPath(() => form.dispensasjon[0].dispensasjonstype.kodeverdi), "ERROR", Helpers.GetFullClassPath(() => form.endringSomKreverDispensasjon));

            _validationResult.AddRule(_skjema, "4402.1.16", null, "Det er valgt at det skal søkes om dispensasjon. Da må du legge inn beskrivelse og begrunnelse for dispensasjonen i tekstfeltene, eller i vedlegget 'Dispensasjonssøknad'.", Helpers.GetFullClassPath(() => form.dispensasjon[0].beskrivelse), "ERROR", "/vedlegg");
            _validationResult.AddRule(_skjema, "4402.1.16.1", null, "Det er valgt at det skal søkes om dispensasjon. Da må du også skrive en begrunnelse for dispensasjonen.", Helpers.GetFullClassPath(() => form.dispensasjon[0].begrunnelse), "ERROR", Helpers.GetFullClassPath(() => form.endringSomKreverDispensasjon));
            //ArealdisponeringValidering - Areal
            _validationResult.AddRule(_skjema, "4402.1.19", "3.3", "Informasjon om arealdisponering kreves for søknad om endring av areal.", Helpers.GetFullClassPath(() => form.rammebetingelser.arealdisponering), "ERROR", "");
            _validationResult.AddRule(_skjema, "4402.1.20", "3.3", "Du må oppgi beregningsregel for grad av utnytting fra gjeldende plan for valgte tiltakstyper, jfr. nasjonal sjekkliste punkt 3.3. Du kan sjekke riktig beregningsregel på https://register.geonorge.no/byggesoknad/beregningsregelgradavutnytting", Helpers.GetFullClassPath(() => form.rammebetingelser.gjeldendePlan[0].beregningsregelGradAvUtnytting), "ERROR", "");
            _validationResult.AddRule(_skjema, "4402.1.21", "", "Når beregningsregel for grad av utnytting er BYA, T-BRA eller BRA, kreves utfylte verdier for areal eksisterende bebyggelse, areal ny bebyggelse, areal bebyggelse som rives, parkeringsareal terreng og beregnet grad av utnytting.", Helpers.GetFullClassPath(() => form.rammebetingelser.gjeldendePlan[0].beregningsregelGradAvUtnytting.kodeverdi), "ERROR", "");
            _validationResult.AddRule(_skjema, "4402.1.22", "", "Utregningen av utnyttelsesgrad er ikke riktig for BYA, T-BRA eller BRA. Utfylt grad av utnytting er {0} og beregnet grad av utnytting er {1}. Utregning: areal eksisterende bebyggelse - areal bebyggelse som skal rives + areal bebyggelse ny + parkeringsareal terreng.", Helpers.GetFullClassPath(() => form.rammebetingelser.arealdisponering.beregnetGradAvUtnytting), "ERROR", "");
            _validationResult.AddRule(_skjema, "4402.1.23", "", "Når beregningsregel for grad av utnytting er %BYA, %BRA eller %TU, kreves utfylte verdier for areal eksisterende bebyggelse, areal ny bebyggelse, areal bebyggelse som rives, parkeringsareal terreng, tomteareal beregnet og beregnet grad av utnytting.", Helpers.GetFullClassPath(() => form.rammebetingelser.gjeldendePlan[0].beregningsregelGradAvUtnytting.kodeverdi), "ERROR", "");
            _validationResult.AddRule(_skjema, "4402.1.24", "", "Når beregningsregel for grad av utnytting er 'Annet', anbefales det å legge ved vedlegg 'Underlag for beregning av utnytting' (UnderlagUtnytting).", Helpers.GetFullClassPath(() => form.rammebetingelser.gjeldendePlan[0].beregningsregelGradAvUtnytting.kodeverdi), "WARNING", "");
            _validationResult.AddRule(_skjema, "4402.1.25.1", "", ValidationError.UtregningEksisterendeArealBebyggelse, Helpers.GetFullClassPath(() => form.rammebetingelser.arealdisponering.arealBebyggelseEksisterende), "WARNING", "");
            _validationResult.AddRule(_skjema, "4402.1.25.2", "", ValidationError.UtregningNyttArealBebyggelse, Helpers.GetFullClassPath(() => form.rammebetingelser.arealdisponering.arealBebyggelseNytt), "WARNING", "");
            _validationResult.AddRule(_skjema, "4402.1.25.3", "", ValidationError.UtregningeksisterendeArealBebyggelseSkalRives, Helpers.GetFullClassPath(() => form.rammebetingelser.arealdisponering.arealBebyggelseSomSkalRives), "WARNING", "");
            _validationResult.AddRule(_skjema, "4402.1.25.4", "", ValidationError.UtregningParkeringsarealTerreng, Helpers.GetFullClassPath(() => form.rammebetingelser.arealdisponering.parkeringsarealTerreng), "WARNING", "");
            _validationResult.AddRule(_skjema, "4402.1.25.5", "", "Utregning av utnyttelsesgrad kan ikke ha negative verdier for beregnetGradAvUtnytting.", Helpers.GetFullClassPath(() => form.rammebetingelser.arealdisponering.beregnetGradAvUtnytting), "WARNING", "");
            _validationResult.AddRule(_skjema, "4402.1.25.6", "", ValidationError.UtregningTomtearealBeregnet, Helpers.GetFullClassPath(() => form.rammebetingelser.arealdisponering.tomtearealBeregnet), "WARNING", "");
            _validationResult.AddRule(_skjema, "4402.1.26", "", ValidationError.UtregningTomtearelBeregnetZero, Helpers.GetFullClassPath(() => form.rammebetingelser.arealdisponering.tomtearealBeregnet), "WARNING", "");
            _validationResult.AddRule(_skjema, "4402.1.27", "", "Utregningen av utnyttelsesgrad er ikke riktig for %BYA, %BRA eller %TU. Utfylt grad av utnytting er {0} og beregnet grad av utnytting er {1}. Utregning ((areal eksisterende bebyggelse- areal bebyggelse som skal rives + areal bebyggelse ny + parkeringsareal terreng) / beregnet tomteareal) * 100.", Helpers.GetFullClassPath(() => form.rammebetingelser.arealdisponering.beregnetGradAvUtnytting), "ERROR", "");
            _validationResult.AddRule(_skjema, "4402.1.66", "", "koden {0} er ikke gyldig status, du kan sjekke status på https://register.geonorge.no/byggesoknad/beregningsregelgradavutnytting ", Helpers.GetFullClassPath(() => form.rammebetingelser.gjeldendePlan[0].beregningsregelGradAvUtnytting.kodeverdi), "ERROR", "");
            _validationResult.AddRule(_skjema, "4402.1.2.13", "", "Ugyldig kodeverdi({0}) i henhold til kodeliste for beregningsregel grad av utnytting.", Helpers.GetFullClassPath(() => form.rammebetingelser.gjeldendePlan[0].beregningsregelGradAvUtnytting.kodeverdi), "ERROR", "");
            _validationResult.AddRule(_skjema, "4402.1.1.3", "", "Når kodeverdi '{0}' er angitt bør kodebeskrivelsen også fylles ut.", Helpers.GetFullClassPath(() => form.rammebetingelser.gjeldendePlan[0].beregningsregelGradAvUtnytting.kodebeskrivelse), "WARNING", "");
            // AdkomstValidering - BRUK, Formaal
            _validationResult.AddRule(_skjema, "4402.1.30", "7.1", "Informasjon om adkomst kreves for valgte tiltakstyper jf. nasjonal sjekkliste pkt 7.1.", Helpers.GetFullClassPath(() => form.rammebetingelser.adkomst), "WARNING", "");
            _validationResult.AddRule(_skjema, "4402.1.31", "7.1", "Vedlegg 'avkjørselsplan' er anbefalt da det er valgt at tiltaket gir ny eller endret adkomst", Helpers.GetFullClassPath((() => form.rammebetingelser.adkomst.nyeEndretAdkomst)), "WARNING", "");
            _validationResult.AddRule(_skjema, "4402.1.2.9", "", "Ugyldig kodeverdi({0}) i henhold til kodeliste for vegtype.", Helpers.GetFullClassPath(() => form.rammebetingelser.adkomst.vegtype[0].kodeverdi), "ERROR", "");
            _validationResult.AddRule(_skjema, "4402.1.32", "7.1", "Når riksveg / fylkesveg er valgt må avkjøringstillatelse fylles ut.", Helpers.GetFullClassPath(() => form.rammebetingelser.adkomst.erTillatelseGittRiksFylkesveg), "ERROR", "");
            _validationResult.AddRule(_skjema, "4402.1.33", "7.1", "Når kommunal veg er valgt må avkjøringstillatelse fylles ut.", Helpers.GetFullClassPath(() => form.rammebetingelser.adkomst.erTillatelseGittKommunalVeg), "ERROR", "");
            _validationResult.AddRule(_skjema, "4402.1.34", "7.1", "Når privat veg er valgt, må du fylle ut om vegrett er sikret ved tinglyst erklæring, eller om det på annen måte er sikret vegforbindelse som kommunen godtar som tilfredsstillende.", Helpers.GetFullClassPath(() => form.rammebetingelser.adkomst.erTillatelseGittPrivatVeg), "ERROR", "");
            //KravTilByggegrunnValidering - BRUK, Formaal og Plassering
            _validationResult.AddRule(_skjema, "4402.1.36", "11.1", "Du må svare på om byggverket skal plasseres i et område med fare for flom eller stormflo, jfr. nasjonal sjekkliste punkt 11.1.", Helpers.GetFullClassPath(() => form.rammebetingelser.kravTilByggegrunn.flomutsattOmraade), "ERROR", null);
            _validationResult.AddRule(_skjema, "4402.1.1.1", "11.4", "Når det er valgt flomutsatt område må en av sannsynlighetene og konsekvensene velges.", Helpers.GetFullClassPath(() => form.rammebetingelser.kravTilByggegrunn.flomutsattOmraade), "ERROR", "");
            _validationResult.AddRule(_skjema, "4402.1.37", "11.2", "Det er valgt at byggverket skal plasseres i et område med fare for flom eller stormflo. Da må vedlegget 'Redegjørelse skred og flomfare' legges ved, jfr. nasjonal sjekkliste punkt 11.2.", Helpers.GetFullClassPath(() => form.rammebetingelser.kravTilByggegrunn.flomutsattOmraade), "WARNING", "/Vedlegg");

            _validationResult.AddRule(_skjema, "4402.1.1.2", "11.8", "Du må svare på om byggverket skal plasseres i et område med fare for skred, jfr. nasjonal sjekkliste punkt 11.8.", Helpers.GetFullClassPath(() => form.rammebetingelser.kravTilByggegrunn.skredutsattOmraade), "ERROR", null);
            _validationResult.AddRule(_skjema, "4402.1.1.2.1", "11.11", "Det er valgt at byggverket skal plasseres i et område med fare for skred. Da må sikkerhetsklassen fylles ut, jfr. nasjonal sjekkliste punkt 11.11.", Helpers.GetFullClassPath(() => form.rammebetingelser.kravTilByggegrunn.s1), "ERROR", Helpers.GetFullClassPath(() => form.rammebetingelser.kravTilByggegrunn.skredutsattOmraade));
            _validationResult.AddRule(_skjema, "4402.1.1.2.2", "11.9", "Det er valgt at byggverket skal plasseres i et område med fare for skred. Da må vedlegget 'Redegjørelse skred og flomfare' legges ved, jfr. nasjonal sjekkliste punkt 11.9.", Helpers.GetFullClassPath(() => form.rammebetingelser.kravTilByggegrunn), "ERROR", "/Vedlegg");

            //Just for endring av Plassering
            _validationResult.AddRule(_skjema, "4402.1.38", "10.1", "Du må svare på om byggverket skal plasseres i et område med fare for natur- og miljøforhold, jfr. nasjonal sjekkliste punkt 10.1.", Helpers.GetFullClassPath(() => form.rammebetingelser.kravTilByggegrunn.miljoeforhold), "ERROR", null);
            _validationResult.AddRule(_skjema, "4402.1.38.1", "10.1", "Det er valgt at byggverket skal plasseres i et område med fare for natur- og miljøforhold. Da bør vedlegget Redegjørelse andre natur og miljøforhold legges ved.", Helpers.GetFullClassPath((() => form.rammebetingelser.kravTilByggegrunn.miljoeforhold)), "WARNING", Helpers.GetFullClassPath(() => form.rammebetingelser.kravTilByggegrunn.miljoeforhold));
            //--
            //Just for Endring av Burk/Formål
            _validationResult.AddRule(_skjema, "4402.1.39", "13.1", "Beskriv planlagt formaal bør fylles ut'.", Helpers.GetFullClassPath((() => form.beskrivelseAvTiltak.bruk.beskrivPlanlagtFormaal)), "WARNING", Helpers.GetFullClassPath(() => form.rammebetingelser.kravTilByggegrunn.miljoeforhold));
            //--
            //PlasseringValidering - Plassering
            _validationResult.AddRule(_skjema, "4402.1.40", "1.33", "Informasjon om plassering og konflikt vann og avløp kreves for valgte tiltakstyper. jf. nasjonal sjekkliste pkt. 1.33.", Helpers.GetFullClassPath(() => form.rammebetingelser.plassering.konfliktVannOgAvloep), "WARNING", "");
            _validationResult.AddRule(_skjema, "4402.1.41", "1.34", "Informasjon om plassering og konflikt høyspent / kraftlinjer anbefales for valgte tiltakstyper.", Helpers.GetFullClassPath(() => form.rammebetingelser.plassering.konfliktHoeyspentkraftlinje), "WARNING", "");
            _validationResult.AddRule(_skjema, "4402.1.42", "1.34", "Vedlegg 'avklaring av plassering nær høyspentledning' (AvklaringHoyspent) mangler da det er valgt at høyspent kraftlinje kan være i konflikt med tiltaket.", Helpers.GetFullClassPath((() => form.rammebetingelser.plassering.konfliktHoeyspentkraftlinje)), "WARNING", "Vedlegg");
            _validationResult.AddRule(_skjema, "4402.1.43", "1.33", "Vedlegg 'avklaring av plassering nær VA - ledninger' (AvklaringVA) mangler da det er valgt at vann og avløpsledninger kan være i konflikt med tiltaket.", Helpers.GetFullClassPath(() => form.rammebetingelser.plassering.konfliktVannOgAvloep), "WARNING", "Vedlegg");
            _validationResult.AddRule(_skjema, "4402.1.44", "1.33", "Informasjon om plassering kreves for søknad om endring av plassering.", Helpers.GetFullClassPath(() => form.rammebetingelser.plassering), "ERROR", "");

            // ArbeidsplasserValidering - Alle
            _validationResult.AddRule(_skjema, "4402.1.46", "", "Når en velger at yrkesbygg berører arbeidsplasser må samtykke fra Arbeidstilsynet være på plass før igangsettingstillatelse kan gis. Det anbefales at enten vedlegg 'SamtykkeArbeidstilsynet' eller underskjema 'Søknad om Arbeidstilsynets samtykke' legges ved søknaden.'", Helpers.GetFullClassPath(() => form.rammebetingelser.generelleVilkaar.beroererArbeidsplasser), "WARNING", "Vedlegg");
            // GjeldendePlanValidering - Alle
            _validationResult.AddRule(_skjema, "4402.1.47", "3.1", "Informasjon om gjeldende plan kreves for valgte tiltakstyper jf. nasjonal sjekkliste pkt 3.1.", Helpers.GetFullClassPath(() => form.rammebetingelser.gjeldendePlan), "ERROR", "");
            _validationResult.AddRule(_skjema, "4402.1.2.10", "3.1", "Ugyldig kodeverdi({0}) i henhold til kodeliste for plantype, jfr. nasjonal sjekkliste punkt 3.1.", Helpers.GetFullClassPath(() => form.rammebetingelser.gjeldendePlan[0].plantype.kodeverdi), "ERROR", "");
            _validationResult.AddRule(_skjema, "4402.1.2.11", "3.1", "Når plantype er valgt må kodebeskrivelse angis, jfr. nasjonal sjekkliste punkt 3.1.", Helpers.GetFullClassPath(() => form.rammebetingelser.gjeldendePlan[0].plantype.kodebeskrivelse), "ERROR", "");
            _validationResult.AddRule(_skjema, "4402.1.2.17", "3.1", "Når bygningstype er valgt må gjeldende plan navn angis, jfr. nasjonal sjekkliste punkt 3.1.", Helpers.GetFullClassPath(() => form.rammebetingelser.gjeldendePlan[0].navn), "WARNING", "");
            //UavhengigKontrollValidering - Bruk, formaal
            _validationResult.AddRule(_skjema, "4402.1.45", "", "For søknad om endring av bruk/formål anbefales underskjema matrikkelopplysninger utfylt.", "Vedlegg", "WARNING", "");
            //UavhengigKontrollValidering - Alle
            _validationResult.AddRule(_skjema, "4402.1.48", "17.6", "Det bør oppgis om tiltaket utløser krav til uavhengig kontroll.", Helpers.GetFullClassPath(() => form.rammebetingelser.generelleVilkaar.paalagtUavhengigKontroll), "WARNING", "");
            //KommunensSaksnummerValidering - Alle
            _validationResult.AddRule(_skjema, "4402.1.67", "", "KommunensSaksnummer må fylles ut", Helpers.GetFullClassPath(() => form.kommunensSaksnummer), "ERROR", "");
            _validationResult.AddRule(_skjema, "4402.1.68", "", "KommunensSaksnummer saksaar må fylles ut", Helpers.GetFullClassPath(() => form.kommunensSaksnummer.saksaar), "ERROR", "");
            _validationResult.AddRule(_skjema, "4402.1.69", "", "KommunensSaksnummer sakssekvensnummer må fylles ut", Helpers.GetFullClassPath(() => form.kommunensSaksnummer.sakssekvensnummer), "ERROR", "");
            //VedleggValidering - Alle
            _validationResult.AddRule(_skjema, "4402.1.70", "17.11", "Vedlegg eller underskjema ‘Gjennomføringsplan’ skal følge med søknaden.", Helpers.GetFullClassPath((() => form.ansvarligSoeker.organisasjonsnummer)), "ERROR", "Vedlegg");
            _validationResult.AddRule(_skjema, "4402.1.70.1", "17.17", "Normalt skal gjennomføringsplan følge søknaden. Dersom selvbygger skal ha alt ansvar, skal det ikke sendes inn gjennomføringsplan.", Helpers.GetFullClassPath((() => form.ansvarligSoeker.organisasjonsnummer)), "WARNING", "Vedlegg");
            _validationResult.AddRule(_skjema, "4402.1.70.2", "17.17", "‘Søknad om ansvarsrett for selvbygger’ skal følge med søknaden når ansvarlig søker er en privatperson.", Helpers.GetFullClassPath((() => form)), "ERROR", "Vedlegg");
            // EndringAreal
            _validationResult.AddRule(_skjema, "4402.1.71", "", "'Vedlegg' Situasjonsplan er anbefalt da det er valgt endring av areal", Helpers.GetFullClassPath((() => form.endringAreal)), "WARNING", "Vedlegg");
            //*endringAreal og endringAnnet
            _validationResult.AddRule(_skjema, "4402.1.73", "", "'Vedlegg' ny fasade, ny plantegning og nytt snitt er anbefalt da der er valgt endring av areal eller annet. De kan være samlet på ett eller flere vedlegg.", Helpers.GetFullClassPath((() => form.endringAreal)), "WARNING", "Vedlegg");
            //endringPlassering
            _validationResult.AddRule(_skjema, "4402.1.74", "", "'Vedlegg' Situasjonsplan må sendes da det er valgt endring av plassering", Helpers.GetFullClassPath((() => form.endringPlassering)), "ERROR", "Vedlegg");
            _validationResult.AddRule(_skjema, "4402.1.76", "", "'Vedlegg' TegningNyFasade bør sendes da det er valgt endring av plassering", Helpers.GetFullClassPath((() => form.endringPlassering)), "ERROR", "Vedlegg");
            //Annet
            _validationResult.AddRule(_skjema, "4402.1.77", "", "Du bør beskrive hvilke endringer det søkes om ved å bruke tekstfeltet eller vedlegget ´Følgebrev´.", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.foelgebrev), "WARNING", Helpers.GetFullClassPath(() => form.endringAnnet));
        }

        public ValidationResult Validate(EndringAvTillatelseType form, List<string> formSetElements)
        {
            MinstEtTiltak(form); // Må først siden tiltakslisten bygges her
            //Add to validationResults all "tiltakstypes"
            _validationResult.tiltakstyperISoeknad = _tiltakstyperISoeknad;

            EiendomByggestedValidation(form);
            Parallel.Invoke(
                () => { KommunensSaksnummerValidering(form); },
                () => { MinstEnEndringValidering(form); },
                () => { FoelgebrevValidering(form, formSetElements); },
                () => { VarslingValidering(form, formSetElements); },
                () => { GjeldendePlanValidering(form, formSetElements); },
                () => { SoekerValidation(form); },
                () => { VedleggValidering(form, formSetElements); },
                () => { ArbeidsplasserValidering(form, formSetElements); },
                () => { UavhengigKontrollValidering(form); },
                () =>
                {
                    //Endring av formål/bruk, Plassering
                    if (IsEndringPlassering(form) || IsEndringBruk(form) || IsEndringFormaal(form))
                    {
                        KravTilByggegrunnValidering(form, formSetElements);
                    }
                },
                () =>
                {
                    //endringSomKreverDispensasjon
                    if (IsEndringSomKreverDispensasjon(form))
                    {
                        DispensasjonValidering(form, formSetElements);
                    }
                },
                () =>
                {
                    //Endring av areal
                    if (IsEndringAreal(form))
                    {
                        //TODO update validation like "GjeldendePlanValidering" and "ArealdisponeringValidering" in EtttrinnsoknadFormV3Validator.cs
                        ArealdisponeringValidering(form);
                    }
                },
                () =>
                {
                    //Endring av areal
                    if (IsEndringAreal(form))
                    {
                        //TODO update validation like "GjeldendePlanValidering" and "ArealdisponeringValidering" in EtttrinnsoknadFormV3Validator.cs
                        ArealdisponeringValidering(form);
                    }
                },
                () =>
                {
                    //Endring av plassering
                    if (IsEndringPlassering(form))
                    {
                        PlasseringValidering(form, formSetElements);
                    }
                },
                () =>
                {
                    //Endring av formål/bruk
                    if (IsEndringBruk(form) || IsEndringFormaal(form))
                    {
                        AdkomstValidering(form, formSetElements);
                        MatrikkelInformasjonValidering(form);
                        MatrikkelopplysningValidering(form, formSetElements);
                    }
                }
            );

            return _validationResult;
        }

        internal void FoelgebrevValidering(EndringAvTillatelseType form, List<string> formSetElements)
        {
            if (string.IsNullOrEmpty(form.beskrivelseAvTiltak?.foelgebrev) && !formSetElements.Any(attachments => attachments.Equals("Folgebrev")))
            {
                _validationResult.AddMessage("4402.1.77", null);
            }
        }

        internal void VedleggValidering(EndringAvTillatelseType form, List<string> formSetElements)
        {
            var gjennomforingsplans = new List<string>()
            {
                "Gjennomføringsplan",
                "Gjennomfoeringsplan",
                "GjennomføringsplanV4",
            };
            var ansvarsrettSelvbygger = new List<string>()
            {
                "AnsvarsrettSelvbygger",
                "Søknad om ansvarsrett for selvbygger"
            };

            bool isGjennomforingsplans = formSetElements?.Select(x => x)
                                  .Intersect(gjennomforingsplans)
                                  .Any() ?? false;
            bool isAnsvarsrettSelvbygger = formSetElements?.Select(x => x)
                                  .Intersect(ansvarsrettSelvbygger)
                                  .Any() ?? false;

            if (!isGjennomforingsplans && isAnsvarsrettSelvbygger)
            {
                _validationResult.AddMessage("4402.1.70.1", null);
            }

            if (!Helpers.ObjectIsNullOrEmpty(form.ansvarligSoeker) && form.ansvarligSoeker.partstype.kodeverdi == "Privatperson")
            {
                if (!isAnsvarsrettSelvbygger)
                {
                    _validationResult.AddMessage("4402.1.70.2", null);
                }
            }
            else
            {
                if (!Helpers.ObjectIsNullOrEmpty(form.ansvarligSoeker) && !isGjennomforingsplans && !isAnsvarsrettSelvbygger)
                {
                    _validationResult.AddMessage("4402.1.70", null);
                }
            }

            var tegningen = new[] {
                    "TegningNyFasade",
                    "TegningNyPlan" ,
                    "TegningNyttSnitt",
                };
            var tegningenCount = formSetElements.Count(vedlegg => tegningen.Any(vedlegg.Equals));

            if (IsEndringAreal(form) || IsEndringAnnet(form))
            {
                if ((!formSetElements.Any() || !formSetElements.Contains("Situasjonsplan")) && IsEndringAreal(form))
                {
                    _validationResult.AddMessage("4402.1.71", null);
                }

                if (tegningenCount != 3)
                {
                    _validationResult.AddMessage("4402.1.73", null);
                }
            }
            if (IsEndringPlassering(form))
            {
                if (!formSetElements.Any() || !formSetElements.Contains("Situasjonsplan"))
                {
                    _validationResult.AddMessage("4402.1.74", null);
                }
                if (!formSetElements.Any() || !formSetElements.Contains("TegningNyFasade"))
                {
                    _validationResult.AddMessage("4402.1.76", null);
                }
            }
        }

        internal void KommunensSaksnummerValidering(EndringAvTillatelseType form)
        {
            if (Helpers.ObjectIsNullOrEmpty(form.kommunensSaksnummer))
            {
                _validationResult.AddMessage("4402.1.67", null);
            }
            else
            {
                if (string.IsNullOrEmpty(form.kommunensSaksnummer.saksaar))
                {
                    _validationResult.AddMessage("4402.1.68", null);
                }

                if (string.IsNullOrEmpty(form.kommunensSaksnummer.sakssekvensnummer))
                {
                    _validationResult.AddMessage("4402.1.69", null);
                }
            }
        }

        internal void MinstEnEndringValidering(EndringAvTillatelseType form)
        {
            if (!IsEndringAreal(form) && !IsEndringPlassering(form) && !IsEndringFormaal(form) && !IsEndringBruk(form) && !IsEndringAnnet(form))
            {
                _validationResult.AddMessage("4402.1.6", null);
            }
        }

        internal void VarslingValidering(EndringAvTillatelseType form, List<string> formSetElements)
        {
            if (Helpers.ObjectIsNullOrEmpty(form.varsling))
            {
                _validationResult.AddMessage("4402.1.78", null);
            }
            else
            {
                if (!form.varsling.fritattFraNabovarsling.GetValueOrDefault())
                {
                    if (formSetElements == null)
                    {
                        _validationResult.AddMessage("4402.1.9", null);
                    }
                    else
                    {
                        if (!formSetElements.Contains("Gjenpart nabovarsel") && !formSetElements.Contains("GjenpartNabovarsel") && !formSetElements.Contains("Opplysninger gitt i nabovarsel") && !formSetElements.Contains("Nabovarsel"))
                        {
                            _validationResult.AddMessage("4402.1.9", null);
                        }
                        if (!formSetElements.Contains("KvitteringNabovarsel") && !formSetElements.Contains("Kvittering for nabovarsel"))
                        {
                            _validationResult.AddMessage("4402.1.10", null);
                        }
                    }

                    if (form.varsling.foreliggerMerknader.GetValueOrDefault())
                    {
                        if (String.IsNullOrEmpty(form.varsling.antallMerknader))
                        {
                            _validationResult.AddMessage("4402.1.11", null);
                        }
                        if (String.IsNullOrEmpty(form.varsling.vurderingAvMerknader))
                        {
                            _validationResult.AddMessage("4402.1.12", null);
                        }
                        if (formSetElements == null || !formSetElements.Contains("Nabomerknader"))
                        {
                            _validationResult.AddMessage("4402.1.13", null);
                        }
                    }
                }
            }
        }

        internal void ValiderEiendom(EiendomType eiendom)
        {
            if (eiendom.eiendomsidentifikasjon != null)
            {
                if (String.IsNullOrEmpty(eiendom.eiendomsidentifikasjon.kommunenummer))
                {
                    _validationResult.AddMessage("4402.1.7", null);
                }
                else
                {
                    GyldigKommuneNrEiendom(eiendom.eiendomsidentifikasjon.kommunenummer);

                    var isEiendomsidentifikasjonValid = GeneralValidations.Validate_Eiendomsidentifikasjon(
                        eiendom.eiendomsidentifikasjon.kommunenummer,
                        eiendom.eiendomsidentifikasjon.gaardsnummer,
                        eiendom.eiendomsidentifikasjon.bruksnummer,
                        eiendom.eiendomsidentifikasjon.festenummer,
                        eiendom.eiendomsidentifikasjon.seksjonsnummer);
                    if (isEiendomsidentifikasjonValid.HasValue && !isEiendomsidentifikasjonValid.Value)
                    {
                        _validationResult.AddMessage("4402.1.14", new[] { eiendom.eiendomsidentifikasjon.kommunenummer, eiendom.eiendomsidentifikasjon.gaardsnummer, eiendom.eiendomsidentifikasjon.bruksnummer, eiendom.eiendomsidentifikasjon.festenummer, eiendom.eiendomsidentifikasjon.seksjonsnummer });
                    }
                    else
                    {
                        if (!String.IsNullOrEmpty(eiendom.bygningsnummer))
                        {
                            var isBygningValid = GeneralValidations.Validate_Bygningsnr(eiendom.bygningsnummer,
                        eiendom.eiendomsidentifikasjon.kommunenummer,
                        eiendom.eiendomsidentifikasjon.gaardsnummer,
                        eiendom.eiendomsidentifikasjon.bruksnummer,
                        eiendom.eiendomsidentifikasjon.festenummer,
                        eiendom.eiendomsidentifikasjon.seksjonsnummer);

                            if (isBygningValid.HasValue && !isBygningValid.Value)
                            {
                                _validationResult.AddMessage("4402.1.15", null);
                            }
                        }

                        if (!string.IsNullOrEmpty(eiendom.adresse?.adresselinje1))
                        {
                            if (!String.IsNullOrEmpty(eiendom.adresse.gatenavn) && !String.IsNullOrEmpty(eiendom.adresse.husnr))
                            {
                                var isAdresseValid = GeneralValidations.Validate_Adresse(eiendom.adresse.gatenavn, eiendom.adresse.husnr, eiendom.adresse.bokstav,
                            eiendom.eiendomsidentifikasjon.kommunenummer,
                            eiendom.eiendomsidentifikasjon.gaardsnummer,
                            eiendom.eiendomsidentifikasjon.bruksnummer,
                            eiendom.eiendomsidentifikasjon.festenummer,
                            eiendom.eiendomsidentifikasjon.seksjonsnummer);

                                if (isAdresseValid.HasValue && !isAdresseValid.Value)
                                {
                                    _validationResult.AddMessage("4402.1.18", null);
                                }
                            }
                            else
                            {
                                _validationResult.AddMessage("4402.1.65", null);
                            }
                        }
                    }
                }
            }

            if (String.IsNullOrEmpty(eiendom.kommunenavn))
            {
                _validationResult.AddMessage("4402.1.7.1", null);
            }

            if (!String.IsNullOrEmpty(eiendom.bolignummer))
            {
                if (GeneralValidations.Validate_bruksenhetsnummer(eiendom.bolignummer).Status == ValidationStatus.Fail)
                {
                    _validationResult.AddMessage("4402.1.8", null);
                }
            }
        }

        internal void EiendomByggestedValidation(EndringAvTillatelseType form)
        {
            if (MinstEnEiendom(form))
            {
                Parallel.ForEach(form.eiendomByggested, (eiendom) =>
                {
                    ValiderEiendom(eiendom);
                });
            }
        }

        internal void DispensasjonValidering(EndringAvTillatelseType form, List<string> formSetElements)
        {
            if (Helpers.ObjectIsNullOrEmpty(form.dispensasjon) || !form.dispensasjon.Any())
            {
                _validationResult.AddMessage("4402.1.17", null);
            }
            else
            {
                foreach (var disp in form.dispensasjon)
                {
                    if (Helpers.ObjectIsNullOrEmpty(disp.dispensasjonstype))
                    {
                        _validationResult.AddMessage("4402.1.17.1", null);
                    }
                    else
                    {
                        if (!_codeListService.IsCodelistValid("dispensasjonstype", disp.dispensasjonstype.kodeverdi))
                        {
                            _validationResult.AddMessage("4402.1.2.1", new[] { disp.dispensasjonstype.kodeverdi });
                        }
                        else
                        {
                            if (!formSetElements.Any(f => f.Equals("Dispensasjonssoeknad")))
                            {
                                if (String.IsNullOrEmpty(disp.beskrivelse))
                                    _validationResult.AddMessage("4402.1.16", null);
                                if (String.IsNullOrEmpty(disp.begrunnelse))
                                    _validationResult.AddMessage("4402.1.16.1", null);
                            }
                        }
                    }
                }
            }
        }

        internal void MinstEtTiltak(EndringAvTillatelseType form)
        {
            if (form.beskrivelseAvTiltak != null)
            {
                if (Helpers.ObjectIsNullOrEmpty(form.beskrivelseAvTiltak.type))
                {
                    _validationResult.AddMessage("4402.1.1", null);
                }
                else
                {
                    foreach (var tiltakstype in form.beskrivelseAvTiltak.type)
                    {
                        if (!_codeListService.IsCodelistValid("tiltaktype", tiltakstype.kodeverdi))
                        {
                            _validationResult.AddMessage("4402.1.2.2", new[] { tiltakstype.kodeverdi });
                        }
                        else
                        {
                            if (!_codeListService.IsCodelistLabelValid("tiltaktype", tiltakstype.kodeverdi, tiltakstype.kodebeskrivelse))
                            {
                                if (String.IsNullOrEmpty(tiltakstype.kodebeskrivelse))
                                {
                                    _validationResult.AddMessage("4402.1.2.15", null);
                                }
                                else
                                {
                                    _validationResult.AddMessage("4402.1.2.3", new[] { tiltakstype.kodebeskrivelse });
                                }
                            }
                            // TODO shoud we add tiltakstype just if Kodeverdi and kodebeskrivelse are valid?
                            //else
                            //{
                            _tiltakstyperISoeknad.Add(tiltakstype.kodeverdi);
                            //}
                        }
                    }
                }
            }
        }

        //Endring av formål/bruk
        internal void MatrikkelopplysningValidering(EndringAvTillatelseType form, List<string> formSetElements)
        {
            if (formSetElements == null || !formSetElements.Contains("Matrikkelopplysninger"))
            {
                _validationResult.AddMessage("4402.1.45", null);
            }
        }

        internal void MatrikkelInformasjonValidering(EndringAvTillatelseType form)
        {
            if (Helpers.ObjectIsNullOrEmpty(form.beskrivelseAvTiltak?.bruk))
            {
                _validationResult.AddMessage("4402.1.2.4", null);
            }
            else
            {
                //if (!Helpers.ObjectIsNullOrEmpty(form.beskrivelseAvTiltak.bruk.anleggstype))
                //{
                //    if (!_codeListService.IsCodelistValid("anleggstype", form.beskrivelseAvTiltak.bruk.anleggstype.kodeverdi))
                //    {
                //        _validationResult.AddMessage("4402.1.2.4", new[] { form.beskrivelseAvTiltak.bruk.anleggstype.kodeverdi });
                //    }
                //}

                if (!Helpers.ObjectIsNullOrEmpty(form.beskrivelseAvTiltak.bruk.naeringsgruppe))
                {
                    if (!_codeListService.IsCodelistValid("naeringsgruppe", form.beskrivelseAvTiltak.bruk.naeringsgruppe.kodeverdi))
                    {
                        _validationResult.AddMessage("4402.1.2.5", new[] { form.beskrivelseAvTiltak.bruk.naeringsgruppe.kodeverdi });
                    }
                }

                if (!Helpers.ObjectIsNullOrEmpty(form.beskrivelseAvTiltak.bruk.bygningstype))
                {
                    if (!_codeListService.IsCodelistValid("bygningstype", form.beskrivelseAvTiltak.bruk.bygningstype.kodeverdi))
                    {
                        _validationResult.AddMessage("4402.1.2.6", new[] { form.beskrivelseAvTiltak.bruk.bygningstype.kodeverdi });
                    }
                    else
                    {
                        if (String.IsNullOrEmpty(form.beskrivelseAvTiltak.bruk.bygningstype?.kodebeskrivelse))
                        {
                            _validationResult.AddMessage("4402.1.2.16", null);
                        }
                        else if (!_codeListService.IsCodelistLabelValid("bygningstype", form.beskrivelseAvTiltak.bruk.bygningstype.kodeverdi, form.beskrivelseAvTiltak.bruk.bygningstype.kodebeskrivelse))
                        {
                            _validationResult.AddMessage("4402.1.2.7", new[] { form.beskrivelseAvTiltak.bruk.bygningstype.kodebeskrivelse });
                        }
                    }
                }
                if (!Helpers.ObjectIsNullOrEmpty(form.beskrivelseAvTiltak.bruk.tiltaksformaal))
                {
                    //tiltaksformaal
                    foreach (var tiltaksformaal in form.beskrivelseAvTiltak.bruk.tiltaksformaal)
                    {
                        if (!_codeListService.IsCodelistValid("tiltaksformal", tiltaksformaal.kodeverdi))
                        {
                            _validationResult.AddMessage("4402.1.2.8", new[] { tiltaksformaal.kodeverdi });
                        }
                    }
                }

                if (String.IsNullOrEmpty(form.beskrivelseAvTiltak.bruk.beskrivPlanlagtFormaal))
                {
                    var annetAny = form.beskrivelseAvTiltak.bruk.tiltaksformaal?.Any(t => t.kodeverdi == "Annet");
                    if (annetAny.GetValueOrDefault())
                    {
                        _validationResult.AddMessage("4402.1.35", null);
                    }
                    else
                    {
                        if (!IsEndringPlassering(form))
                        {
                            _validationResult.AddMessage("4402.1.39", null);
                        }
                    }
                }
            }
        }

        internal void GjeldendePlanValidering(EndringAvTillatelseType form, List<string> formSetElements)
        {
            if (Helpers.ObjectIsNullOrEmpty(form.rammebetingelser?.gjeldendePlan))
            {
                _validationResult.AddMessage("4402.1.47", null);
            }
            else
            {
                foreach (var gjeldendePlan in form.rammebetingelser.gjeldendePlan)
                {
                    if (!Helpers.ObjectIsNullOrEmpty(gjeldendePlan?.plantype))
                    {
                        var planTypeKodeverdi = gjeldendePlan.plantype.kodeverdi;

                        if (!_codeListService.IsCodelistValid("Plantype", planTypeKodeverdi))
                        {
                            _validationResult.AddMessage("4402.1.2.10", new[] { planTypeKodeverdi });
                        }
                        if (!String.IsNullOrEmpty(planTypeKodeverdi) && String.IsNullOrEmpty(gjeldendePlan.plantype.kodebeskrivelse))
                        {
                            _validationResult.AddMessage("4402.1.2.11", null);
                        }
                    }

                    if (string.IsNullOrEmpty(gjeldendePlan?.navn))
                    {
                        _validationResult.AddMessage("4402.1.2.17", null);
                    }
                    //TODO dette er under skjPunkt 3.3
                    if (IsEndringAreal(form))
                    {
                        if (Helpers.ObjectIsNullOrEmpty(gjeldendePlan?.beregningsregelGradAvUtnytting))
                        {
                            _validationResult.AddMessage("4402.1.20", null);
                        }
                        else
                        {
                            if (_codeListService.IsCodeValidInCodeList(CodelistNames.BeregningsregelGradAvUtnytting.ToString(), gjeldendePlan.beregningsregelGradAvUtnytting.kodeverdi))
                            {
                                if (gjeldendePlan.beregningsregelGradAvUtnytting.kodeverdi == "annetUdef")
                                {
                                    if (formSetElements == null || !formSetElements.Contains("UnderlagUtnytting"))
                                    {
                                        _validationResult.AddMessage("4402.1.24", null);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        // endring av areal
        internal void ArealdisponeringValidering(EndringAvTillatelseType form)
        {
            if (!Helpers.ObjectIsNullOrEmpty(form.rammebetingelser?.gjeldendePlan))
            {
                if (form.rammebetingelser.gjeldendePlan.All(p => p.beregningsregelGradAvUtnytting?.kodeverdi != "annetUdef")
                    && form.rammebetingelser.gjeldendePlan.All(p => p.beregningsregelGradAvUtnytting?.kodeverdi != "ingenKrav"))
                {
                    if (Helpers.ObjectIsNullOrEmpty(form.rammebetingelser.arealdisponering))
                    {
                        _validationResult.AddMessage("4402.1.19", null);
                    }
                    else
                    {
                        foreach (var gjeldendePlan in form.rammebetingelser.gjeldendePlan)
                        {
                            if (!Helpers.ObjectIsNullOrEmpty(gjeldendePlan.beregningsregelGradAvUtnytting))
                            {
                                if (_codeListService.IsCodeValidInCodeList(CodelistNames.BeregningsregelGradAvUtnytting.ToString(), gjeldendePlan.beregningsregelGradAvUtnytting.kodeverdi))
                                {
                                    if (gjeldendePlan.beregningsregelGradAvUtnytting.kodeverdi == "BYA"
                                        || gjeldendePlan.beregningsregelGradAvUtnytting.kodeverdi == "BRA"
                                        || gjeldendePlan.beregningsregelGradAvUtnytting.kodeverdi == "T-BRA")
                                    {
                                        //Krav om utfylte felter
                                        if (!form.rammebetingelser.arealdisponering.arealBebyggelseEksisterendeSpecified ||
                                            !form.rammebetingelser.arealdisponering.arealBebyggelseNyttSpecified ||
                                            !form.rammebetingelser.arealdisponering.arealBebyggelseSomSkalRivesSpecified ||
                                            !form.rammebetingelser.arealdisponering.parkeringsarealTerrengSpecified ||
                                            !form.rammebetingelser.arealdisponering.beregnetGradAvUtnyttingSpecified
                                        )
                                        {
                                            _validationResult.AddMessage("4402.1.21", null);
                                        }
                                        else
                                        {
                                            //Krav om at regnestykke er riktig
                                            double utfyltGradAvUtnytting = form.rammebetingelser.arealdisponering.beregnetGradAvUtnytting.Value;
                                            double beregnetGradAvUtnytting = form.rammebetingelser.arealdisponering.arealBebyggelseEksisterende.Value
                                                                             - form.rammebetingelser.arealdisponering.arealBebyggelseSomSkalRives
                                                                                 .Value
                                                                             + form.rammebetingelser.arealdisponering.arealBebyggelseNytt.Value
                                                                             + form.rammebetingelser.arealdisponering.parkeringsarealTerreng.Value;
                                            if (!ValidationHelper.UtfyltOgBeregnetArealErDetSamme(utfyltGradAvUtnytting, beregnetGradAvUtnytting))
                                            {
                                                _validationResult.AddMessage("4402.1.22", new[] { utfyltGradAvUtnytting.ToString("##.##"), beregnetGradAvUtnytting.ToString("##.##") });
                                            }
                                        }
                                    }
                                    if (gjeldendePlan.beregningsregelGradAvUtnytting.kodeverdi == "%BYA"
                                        || gjeldendePlan.beregningsregelGradAvUtnytting.kodeverdi == "%BRA"
                                        || gjeldendePlan.beregningsregelGradAvUtnytting.kodeverdi == "%TU")
                                    {
                                        //Krav om utfylte felter
                                        if (!form.rammebetingelser.arealdisponering
                                                .arealBebyggelseEksisterendeSpecified ||
                                            !form.rammebetingelser.arealdisponering.arealBebyggelseNyttSpecified ||
                                            !form.rammebetingelser.arealdisponering
                                                .arealBebyggelseSomSkalRivesSpecified ||
                                            !form.rammebetingelser.arealdisponering.parkeringsarealTerrengSpecified ||
                                            !form.rammebetingelser.arealdisponering.beregnetGradAvUtnyttingSpecified ||
                                            !form.rammebetingelser.arealdisponering.tomtearealBeregnetSpecified
                                        )
                                        {
                                            _validationResult.AddMessage("4402.1.23", null);
                                        }
                                        else
                                        {
                                            if (form.rammebetingelser.arealdisponering.arealBebyggelseEksisterende
                                                    .Value < 0 ||
                                                form.rammebetingelser.arealdisponering.arealBebyggelseNytt.Value < 0 ||
                                                form.rammebetingelser.arealdisponering.arealBebyggelseSomSkalRives
                                                    .Value < 0 ||
                                                form.rammebetingelser.arealdisponering.parkeringsarealTerreng.Value <
                                                0 ||
                                                form.rammebetingelser.arealdisponering.beregnetGradAvUtnytting.Value <
                                                0 ||
                                                form.rammebetingelser.arealdisponering.tomtearealBeregnet.Value < 0
                                            )
                                            {
                                                if (form.rammebetingelser.arealdisponering.arealBebyggelseEksisterende
                                                        .Value < 0)
                                                {
                                                    _validationResult.AddMessage("4402.1.25.1", null);
                                                }

                                                if (form.rammebetingelser.arealdisponering.arealBebyggelseNytt.Value <
                                                    0)
                                                {
                                                    _validationResult.AddMessage("4402.1.25.2", null);
                                                }

                                                if (form.rammebetingelser.arealdisponering.arealBebyggelseSomSkalRives
                                                        .Value < 0)
                                                {
                                                    _validationResult.AddMessage("4402.1.25.3", null);
                                                }

                                                if (form.rammebetingelser.arealdisponering.parkeringsarealTerreng.Value < 0)
                                                {
                                                    _validationResult.AddMessage("4402.1.25.4", null);
                                                }

                                                if (form.rammebetingelser.arealdisponering.beregnetGradAvUtnytting.Value < 0)
                                                {
                                                    _validationResult.AddMessage("4402.1.25.5", null);
                                                }
                                                if (form.rammebetingelser.arealdisponering.tomtearealBeregnet.Value < 0)
                                                {
                                                    _validationResult.AddMessage("4402.1.25.6", null);
                                                }
                                            }
                                            else
                                            {
                                                //Sjekke  devision by zero feil
                                                if (form.rammebetingelser.arealdisponering.tomtearealBeregnet.Value == 0)
                                                {
                                                    _validationResult.AddMessage("4402.1.26", null);
                                                }
                                                else
                                                {
                                                    double utfyltGradAvUtnytting = form.rammebetingelser.arealdisponering.beregnetGradAvUtnytting.Value;
                                                    double beregnetGradAvUtnytting = ValidationHelper.BeregnetGradAvUtnyttingArealProsent(
                                                        form.rammebetingelser.arealdisponering.arealBebyggelseEksisterende.Value,
                                                        form.rammebetingelser.arealdisponering.arealBebyggelseSomSkalRives.Value,
                                                        form.rammebetingelser.arealdisponering.arealBebyggelseNytt.Value,
                                                        form.rammebetingelser.arealdisponering.parkeringsarealTerreng.Value,
                                                        form.rammebetingelser.arealdisponering.tomtearealBeregnet.Value);

                                                    if (!ValidationHelper.UtfyltOgBeregnetArealErDetSamme(utfyltGradAvUtnytting, beregnetGradAvUtnytting))
                                                    {
                                                        _validationResult.AddMessage("4402.1.27", new[] { utfyltGradAvUtnytting.ToString("##.##"), beregnetGradAvUtnytting.ToString("##.##") });
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    if (!String.IsNullOrEmpty(gjeldendePlan.beregningsregelGradAvUtnytting.kodeverdi))
                                    {
                                        var codeStatus = _codeListService.CodeHaveStatus("beregningsregelGradAvUtnytting", gjeldendePlan.beregningsregelGradAvUtnytting.kodeverdi);
                                        if (string.IsNullOrEmpty(codeStatus))
                                        {
                                            _validationResult.AddMessage("4402.1.2.13", new[] { gjeldendePlan.beregningsregelGradAvUtnytting.kodeverdi });
                                        }
                                        else
                                        {
                                            if (codeStatus.Equals("erstattet", StringComparison.InvariantCultureIgnoreCase) || codeStatus.Equals("utgått", StringComparison.InvariantCultureIgnoreCase))
                                            {
                                                _validationResult.AddMessage("4402.1.66", new[] { gjeldendePlan.beregningsregelGradAvUtnytting.kodeverdi });
                                            }
                                        }
                                    }
                                }

                                if (string.IsNullOrEmpty(gjeldendePlan.beregningsregelGradAvUtnytting.kodebeskrivelse))
                                {
                                    _validationResult.AddMessage("4402.1.1.3", new[] { gjeldendePlan.beregningsregelGradAvUtnytting.kodeverdi });
                                }
                            }
                        }
                    }
                }
            }
        }

        // Endring av formål/bruk
        internal void AdkomstValidering(EndringAvTillatelseType form, List<string> formSetElements)
        {
            if (_tiltakstyperISoeknad.Any())
            {
                if (form.rammebetingelser.adkomst == null || form.rammebetingelser?.adkomst?.nyeEndretAdkomst.HasValue == false)
                {
                    _validationResult.AddMessage("4402.1.30", null);
                }
            }

            //Vegtype - liste
            if (form.rammebetingelser.adkomst != null)
            {
                if (form.rammebetingelser.adkomst.nyeEndretAdkomst.HasValue)
                {
                    if (form.rammebetingelser.adkomst.nyeEndretAdkomst.Value)
                    {
                        if (formSetElements == null || !formSetElements.Contains("Avkjoerselsplan"))
                        {
                            _validationResult.AddMessage("4402.1.31", null);
                        }

                        if (form.rammebetingelser.adkomst.vegtype != null)
                        {
                            //Avhengig av vegtype så må en svare på om tillatelse er gitt
                            foreach (var vegtype in form.rammebetingelser.adkomst.vegtype)
                            {
                                if (!_codeListService.IsCodelistValid("vegtype", vegtype.kodeverdi))
                                {
                                    _validationResult.AddMessage("4402.1.2.9", new[] { vegtype.kodeverdi });
                                }
                                else
                                {
                                    //sjekke om tillatelse er gitt for aktuell vegtype
                                    if (vegtype.kodeverdi == "RiksFylkesveg")
                                    {
                                        if (!form.rammebetingelser.adkomst.erTillatelseGittRiksFylkesveg.HasValue)
                                        {
                                            _validationResult.AddMessage("4402.1.32", null);
                                        }
                                    }
                                    if (vegtype.kodeverdi == "KommunalVeg")
                                    {
                                        if (!form.rammebetingelser.adkomst.erTillatelseGittKommunalVeg.HasValue)
                                        {
                                            _validationResult.AddMessage("4402.1.33", null);
                                        }
                                    }
                                    if (vegtype.kodeverdi == "PrivatVeg")
                                    {
                                        if (!form.rammebetingelser.adkomst.erTillatelseGittPrivatVeg.HasValue)
                                        {
                                            _validationResult.AddMessage("4402.1.34", null);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        internal void KravTilByggegrunnValidering(EndringAvTillatelseType form, List<string> formSetElements)
        {
            if (_tiltakstyperISoeknad.Any())
            {
                if (Helpers.ObjectIsNullOrEmpty(form.rammebetingelser?.kravTilByggegrunn) || !form.rammebetingelser.kravTilByggegrunn.flomutsattOmraade.HasValue)
                {
                    _validationResult.AddMessage("4402.1.36", null);
                }
                else
                {
                    if (form.rammebetingelser.kravTilByggegrunn.flomutsattOmraade.GetValueOrDefault())
                    {
                        if (!form.rammebetingelser.kravTilByggegrunn.f1.GetValueOrDefault() &&
                            !form.rammebetingelser.kravTilByggegrunn.f2.GetValueOrDefault() &&
                            !form.rammebetingelser.kravTilByggegrunn.f3.GetValueOrDefault())
                        {
                            _validationResult.AddMessage("4402.1.1.1", null);
                        }

                        if (formSetElements == null || !formSetElements.Contains("RedegjoerelseSkredOgFlom"))
                        {
                            _validationResult.AddMessage("4402.1.37", null);
                        }
                    }
                }
            }

            if (Helpers.ObjectIsNullOrEmpty(form.rammebetingelser?.kravTilByggegrunn) || !form.rammebetingelser.kravTilByggegrunn.skredutsattOmraade.HasValue)
            {
                _validationResult.AddMessage("4402.1.1.2", null);
            }
            else
            {
                if (form.rammebetingelser.kravTilByggegrunn.skredutsattOmraade.GetValueOrDefault())
                {
                    if (!form.rammebetingelser.kravTilByggegrunn.s1.GetValueOrDefault() &&
                        !form.rammebetingelser.kravTilByggegrunn.s2.GetValueOrDefault() &&
                        !form.rammebetingelser.kravTilByggegrunn.s3.GetValueOrDefault())
                    {
                        _validationResult.AddMessage("4402.1.1.2.1", null);
                    }
                    if (formSetElements == null || !formSetElements.Contains("RedegjoerelseSkredOgFlom"))
                    {
                        _validationResult.AddMessage("4402.1.1.2.2", null);
                    }
                }
            }

            if (IsEndringPlassering(form))
            {
                if (Helpers.ObjectIsNullOrEmpty(form.rammebetingelser?.kravTilByggegrunn) || !form.rammebetingelser.kravTilByggegrunn.miljoeforhold.HasValue)
                {
                    _validationResult.AddMessage("4402.1.38", null);
                }
                else
                {
                    if (form.rammebetingelser.kravTilByggegrunn.miljoeforhold.GetValueOrDefault())
                    {
                        if (formSetElements == null || !formSetElements.Contains("RedegjoerelseAndreNaturMiljoeforhold"))
                        {
                            _validationResult.AddMessage("4402.1.38.1", null);
                        }
                    }
                }
            }
        }

        internal void PlasseringValidering(EndringAvTillatelseType form, List<string> formSetElements)
        {
            if (form.rammebetingelser != null)
            {
                if (_tiltakstyperISoeknad.Any())
                {
                    if (form.rammebetingelser.plassering?.konfliktVannOgAvloep.HasValue == null || form.rammebetingelser.plassering?.konfliktVannOgAvloep == null)
                    {
                        _validationResult.AddMessage("4402.1.40", null);
                    }

                    if (form.rammebetingelser.plassering?.konfliktHoeyspentkraftlinje.HasValue == null || form.rammebetingelser.plassering?.konfliktHoeyspentkraftlinje == null)
                    {
                        _validationResult.AddMessage("4402.1.41", null);
                    }
                }

                //Hvis krav til plassering
                if (form.rammebetingelser.plassering != null)
                {
                    if (form.rammebetingelser.plassering.konfliktHoeyspentkraftlinje.GetValueOrDefault())
                    {
                        if (formSetElements == null || !formSetElements.Contains("AvklaringHoyspent"))
                        {
                            _validationResult.AddMessage("4402.1.42", null);
                        }
                    }

                    if (form.rammebetingelser.plassering.konfliktVannOgAvloep.GetValueOrDefault())
                    {
                        if (formSetElements == null || !formSetElements.Contains("AvklaringVA"))
                        {
                            _validationResult.AddMessage("4402.1.43", null);
                        }
                    }
                }
                else
                {
                    _validationResult.AddMessage("4402.1.44", null);
                }
            }
        }

        internal void ArbeidsplasserValidering(EndringAvTillatelseType form, List<string> formSetElements)
        {
            if (form.rammebetingelser.generelleVilkaar != null)
            {
                if (form.rammebetingelser.generelleVilkaar.beroererArbeidsplasser.GetValueOrDefault(false))
                {
                    if (formSetElements == null)
                    {
                        _validationResult.AddMessage("4402.1.46", null);
                    }
                    else if (!formSetElements.Contains("Søknad om Arbeidstilsynets samtykke") && !formSetElements.Contains("SamtykkeArbeidstilsynet") && !formSetElements.Contains("Søknad om arbeidstilsynets samtykke"))
                    {
                        _validationResult.AddMessage("4402.1.46", null);
                    }
                }
            }
        }

        internal void UavhengigKontrollValidering(EndringAvTillatelseType form)
        {
            if (form.rammebetingelser.generelleVilkaar != null)
            {
                if (!form.rammebetingelser.generelleVilkaar.paalagtUavhengigKontrollSpecified)
                {
                    _validationResult.AddMessage("4402.1.48", null);
                }
            }
        }

        internal void SoekerValidation(EndringAvTillatelseType form)
        {
            var ansvarlidSoekerHaveEnoughData = (!PartsTypeIsNullOrEmpry(form.ansvarligSoeker) && !Helpers.ObjectIsNullOrEmpty(form.ansvarligSoeker?.adresse) && !string.IsNullOrEmpty(form.ansvarligSoeker?.navn));
            var tiltakshaverHaveEnoughData = (!PartsTypeIsNullOrEmpry(form.tiltakshaver) && !Helpers.ObjectIsNullOrEmpty(form.tiltakshaver?.adresse) && !string.IsNullOrEmpty(form.tiltakshaver?.navn));

            if (tiltakshaverHaveEnoughData || ansvarlidSoekerHaveEnoughData)
            {
                var isTiltakshaverValidSoeker = IsValidSoeker(form.tiltakshaver);
                if (tiltakshaverHaveEnoughData && !ansvarlidSoekerHaveEnoughData)
                {
                    if (form.tiltakshaver != null)
                    {
                        if (form.tiltakshaver.partstype != null)
                        {
                            if (!_codeListService.IsCodelistValid("Partstype", form.tiltakshaver.partstype.kodeverdi))
                            {
                                _validationResult.AddMessage("4402.1.2.12", new[] { form.tiltakshaver.partstype.kodeverdi });
                            }
                            else
                            {
                                if (form.tiltakshaver.partstype.kodeverdi == "Privatperson")
                                {
                                    GeneralValidationResult generalValidationResult = GeneralValidations.Validate_fodselsnummer(form.tiltakshaver.foedselsnummer);

                                    if (generalValidationResult.Status != ValidationStatus.Ok)
                                    {
                                        _validationResult.AddMessage("4402.1.53", new[] { generalValidationResult.Message });
                                    }
                                }
                                else
                                {
                                    GeneralValidationResult generalValidationResult = GeneralValidations.Validate_orgnummer(form.tiltakshaver.organisasjonsnummer);

                                    if (generalValidationResult.Status != ValidationStatus.Ok)
                                    {
                                        _validationResult.AddMessage("4402.1.54", new[] { generalValidationResult.Message });
                                    }

                                    if (string.IsNullOrEmpty(form.tiltakshaver.kontaktperson))
                                    {
                                        _validationResult.AddMessage("4402.1.55", null);
                                    }
                                }
                            }
                        }
                    }
                    //trenger ikke fordi vi bekrefter at adress, Navn og PartsType er alti med for søker (å kjøres validering)
                    //else
                    //{
                    //    var rule = _validationResult.GetRule("4402.1.??");
                    //    _validationResult.AddError(rule.message, rule.id, $"/EndringAvTillatelse/{soeker}/partstype");
                    //}
                    if (form.tiltakshaver.adresse != null)
                    {
                        if (String.IsNullOrEmpty(form.tiltakshaver.adresse.poststed))
                        {
                            _validationResult.AddMessage("4402.1.56", null);
                        }
                        var generalValidationResult = GeneralValidations.Validate_postnummer(form.tiltakshaver.adresse.postnr, form.tiltakshaver.adresse.landkode, false);
                        if (generalValidationResult?.Status == ValidationStatus.Fail)
                        {
                            _validationResult.AddMessage("4402.1.57", new[] { generalValidationResult.Message });
                        }
                    }
                    //trenger ikke fordi vi bekrefter at adress, Navn og PartsType er alti med for å kjøres validering
                    //else
                    //{
                    //    var rule = _validationResult.GetRule("4402.1.??");
                    //    _validationResult.AddError(rule.message, rule.id, $"/EndringAvTillatelse/{soeker}/adresse");
                    //}

                    if (string.IsNullOrEmpty(form.tiltakshaver.mobilnummer))
                    {
                        if (string.IsNullOrEmpty(form.tiltakshaver.telefonnummer))
                        {
                            _validationResult.AddMessage("4402.1.60", null);
                        }
                    }
                }
                else
                {
                    if (!isTiltakshaverValidSoeker)
                    {
                        if (form.ansvarligSoeker != null)
                        {
                            if (form.ansvarligSoeker.partstype != null)
                            {
                                if (!_codeListService.IsCodelistValid("Partstype", form.ansvarligSoeker.partstype.kodeverdi))
                                {
                                    _validationResult.AddMessage("4402.1.2.14", new[] { form.ansvarligSoeker.partstype.kodeverdi });
                                }
                                else
                                {
                                    if (form.ansvarligSoeker.partstype.kodeverdi == "Privatperson")
                                    {
                                        GeneralValidationResult generalValidationResult = GeneralValidations.Validate_fodselsnummer(form.ansvarligSoeker.foedselsnummer);

                                        if (generalValidationResult.Status != ValidationStatus.Ok)
                                        {
                                            _validationResult.AddMessage("4402.1.58", new[] { generalValidationResult.Message });
                                        }
                                    }
                                    else
                                    {
                                        GeneralValidationResult generalValidationResult = GeneralValidations.Validate_orgnummer(form.ansvarligSoeker.organisasjonsnummer);

                                        if (generalValidationResult.Status != ValidationStatus.Ok)
                                        {
                                            _validationResult.AddMessage("4402.1.62", new[] { generalValidationResult.Message });
                                        }

                                        if (string.IsNullOrEmpty(form.ansvarligSoeker.kontaktperson))
                                        {
                                            _validationResult.AddMessage("4402.1.63", null);
                                        }
                                    }
                                }
                            }
                        }
                        //trenger ikke fordi vi bekrefter at adress, Navn og PartsType er alti med for søker (å kjøres validering)
                        //else
                        //{
                        //    var rule = _validationResult.GetRule("4402.1.??");
                        //    _validationResult.AddError(rule.message, rule.id, $"/EndringAvTillatelse/{soeker}/partstype");
                        //}
                        if (form.ansvarligSoeker.adresse != null)
                        {
                            if (String.IsNullOrEmpty(form.ansvarligSoeker.adresse.poststed))
                            {
                                _validationResult.AddMessage("4402.1.64", null);
                            }
                            var generalValidationResult = GeneralValidations.Validate_postnummer(form.ansvarligSoeker.adresse.postnr, form.ansvarligSoeker.adresse.landkode, false);
                            if (generalValidationResult?.Status == ValidationStatus.Fail)
                            {
                                _validationResult.AddMessage("4402.1.52", new[] { generalValidationResult.Message });
                            }
                        }
                        //trenger ikke fordi vi bekrefter at adress, Navn og PartsType er alti med for å kjøres validering
                        //else
                        //{
                        //    var rule = _validationResult.GetRule("4402.1.??");
                        //    _validationResult.AddError(rule.message, rule.id, $"/EndringAvTillatelse/{soeker}/adresse");
                        //}

                        if (string.IsNullOrEmpty(form.ansvarligSoeker.mobilnummer))
                        {
                            if (string.IsNullOrEmpty(form.ansvarligSoeker.telefonnummer))
                            {
                                _validationResult.AddMessage("4402.1.59", null);
                            }
                        }
                    }
                }
            }
            else
            {
                _validationResult.AddMessage("4402.1.61", null);
            }
        }

        private bool IsValidSoeker(PartType soekerPartsType)
        {
            if (soekerPartsType != null)
            {
                if (soekerPartsType.partstype != null)
                {
                    if (_codeListService.IsCodelistValid("Partstype", soekerPartsType.partstype.kodeverdi))
                    {
                        if (soekerPartsType.partstype.kodeverdi == "Privatperson")
                        {
                            GeneralValidationResult generalValidationResult = GeneralValidations.Validate_fodselsnummer(soekerPartsType.foedselsnummer);

                            if (generalValidationResult.Status == ValidationStatus.Ok)
                            {
                                if (IsAdressValid(soekerPartsType))
                                    return !string.IsNullOrEmpty(soekerPartsType.navn);
                            }
                        }
                        else
                        {
                            GeneralValidationResult generalValidationResult = GeneralValidations.Validate_orgnummer(soekerPartsType.organisasjonsnummer);
                            if (generalValidationResult.Status == ValidationStatus.Ok)
                            {
                                if (!string.IsNullOrEmpty(soekerPartsType.kontaktperson))
                                {
                                    if (IsAdressValid(soekerPartsType))
                                        return !string.IsNullOrEmpty(soekerPartsType.navn);
                                }
                            }
                        }
                    }
                }
            }

            return false;
        }

        private static bool IsAdressValid(PartType soekerPartsType)
        {
            if (soekerPartsType.adresse != null)
            {
                if (!String.IsNullOrEmpty(soekerPartsType.adresse.poststed))
                {
                    var generalValidationResult = GeneralValidations.Validate_postnummer(soekerPartsType.adresse.postnr,
                        soekerPartsType.adresse.landkode, false);
                    if (generalValidationResult?.Status == ValidationStatus.Ok)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        private bool PartsTypeIsNullOrEmpry(PartType soeker)
        {
            if (soeker != null)
            {
                return Helpers.ObjectIsNullOrEmpty(soeker) || string.IsNullOrEmpty(soeker.partstype?.kodeverdi);
            }

            return false;
        }

        public static double Round(double input, int places)
        {
            double multiplier = Math.Pow(10, Convert.ToDouble(places));
            return Math.Round(input * multiplier) / multiplier;
        }

        internal bool MinstEnEiendom(EndringAvTillatelseType form)
        {
            if (form.eiendomByggested == null || !form.eiendomByggested.Any())
            {
                _validationResult.AddMessage("4402.1.3", null);
            }

            return form.eiendomByggested != null && (form.eiendomByggested != null || form.eiendomByggested.Any());
        }

        internal bool GyldigKommuneNrEiendom(string kommunenr)
        {
            GeneralValidationResult generalValidationResult = _municipalityValidator.ValidateKommunenummer(kommunenr);
            if (generalValidationResult.Status != ValidationStatus.Ok)
            {
                _validationResult.AddMessage("4402.1.2", null);
                return false;
            }
            return true;
        }

        private static bool IsEndringAreal(EndringAvTillatelseType form)
        {
            var endringAreal = form.endringArealSpecified && form.endringAreal.GetValueOrDefault(false);
            return endringAreal;
        }

        private static bool IsEndringPlassering(EndringAvTillatelseType form)
        {
            var endringPlassering = form.endringPlasseringSpecified && form.endringPlassering.GetValueOrDefault(false);
            return endringPlassering;
        }

        private static bool IsEndringFormaal(EndringAvTillatelseType form)
        {
            var endringFormaal = form.endringFormaalSpecified && form.endringFormaal.GetValueOrDefault(false);
            return endringFormaal;
        }

        private static bool IsEndringBruk(EndringAvTillatelseType form)
        {
            var endringBruk = form.endringBrukSpecified && form.endringBruk.GetValueOrDefault(false);
            return endringBruk;
        }

        private static bool IsEndringAnnet(EndringAvTillatelseType form)
        {
            var endringAnnet = form.endringAnnetSpecified && form.endringAnnet.GetValueOrDefault(false);
            return endringAnnet;
        }

        private static bool IsEndringSomKreverDispensasjon(EndringAvTillatelseType form)
        {
            var endringSomKreverDispensasjon = form.endringSomKreverDispensasjonSpecified && form.endringSomKreverDispensasjon.GetValueOrDefault(false);
            return endringSomKreverDispensasjon;
        }
    }
}