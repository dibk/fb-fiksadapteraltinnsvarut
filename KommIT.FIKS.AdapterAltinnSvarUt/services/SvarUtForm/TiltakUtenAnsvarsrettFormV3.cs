﻿using System.Collections.Generic;
using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.SvarUtForm;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.SvarUt
{
    public class TiltakUtenAnsvarsrettFormV3 : IAltinnForm, SvarUtForm
    {
        private const string DataFormatId = "6742";
        private const string DataFormatVersion = "45753";
        private const string SchemaFile = "tiltakutenansvarsrett3.xsd";
        private const string Name = "Søknad om tillatelse til tiltak uten ansvarsrett";
        private List<string> FormSetElements;
        public void SetFormSetElements(List<string> attachmentlist)
        {
            FormSetElements = attachmentlist;
        }

        public bool? ValidateAuthenticatedSubmitterVsFormData(string authenticatedSubmitter, ValidationResult validationResult)
        {
           bool isSubmitterValid = Helpers.ValidateEntityOfAltinnSubmitterDefault(authenticatedSubmitter, GetPropertyIdentifiers());

            if (!isSubmitterValid)
            {
                Helpers.RecordErrorAltinnSubmitter(validationResult);
            }
            return isSubmitterValid;
        }

        public void InitiateForm(string formDataAsXml)
        {
            form = Utils.SerializeUtil.DeserializeFromString<no.kxml.skjema.dibk.tiltakutenansvarsrettV3.TiltakUtenAnsvarsrettType>(formDataAsXml);
        }

        private no.kxml.skjema.dibk.tiltakutenansvarsrettV3.TiltakUtenAnsvarsrettType form;

        public PropertyIdentifiers GetPropertyIdentifiers()
        {
            PropertyIdentifiers propertyIdentifiers = new PropertyIdentifiers();

            propertyIdentifiers.Kommunenummer = form.eiendomByggested[0].eiendomsidentifikasjon.kommunenummer;
            propertyIdentifiers.Gaardsnummer = form.eiendomByggested[0].eiendomsidentifikasjon.gaardsnummer;
            propertyIdentifiers.Bruksnummer = form.eiendomByggested[0].eiendomsidentifikasjon.bruksnummer;
            propertyIdentifiers.Festenummer = form.eiendomByggested[0].eiendomsidentifikasjon.festenummer;
            propertyIdentifiers.Seksjonsnummer = form.eiendomByggested[0].eiendomsidentifikasjon.seksjonsnummer;
            propertyIdentifiers.Adresselinje1 = form.eiendomByggested[0].adresse.adresselinje1;
            propertyIdentifiers.Adresselinje2 = form.eiendomByggested[0].adresse.adresselinje2;
            propertyIdentifiers.Adresselinje3 = form.eiendomByggested[0].adresse.adresselinje3;
            propertyIdentifiers.Postnr = form.eiendomByggested[0].adresse.postnr;
            propertyIdentifiers.Poststed = form.eiendomByggested[0].adresse.poststed;
            propertyIdentifiers.Landkode = form.eiendomByggested[0].adresse.landkode;
            propertyIdentifiers.Bygningsnummer = form.eiendomByggested[0].bygningsnummer;
            propertyIdentifiers.Bolignummer = form.eiendomByggested[0].bolignummer;
            propertyIdentifiers.TiltakType = form.beskrivelseAvTiltak.type.type.kodeverdi;

            propertyIdentifiers.SoeknadSkjemaNavn = Name;
            propertyIdentifiers.FraSluttbrukersystem = form.metadata.fraSluttbrukersystem;

            propertyIdentifiers.AnsvarligSokerNavn = form.tiltakshaver.navn;
            propertyIdentifiers.AnsvarligSokerOrgnr = form.tiltakshaver.organisasjonsnummer;
            propertyIdentifiers.AnsvarligSokerFnr = form.tiltakshaver.foedselsnummer;

            if (form.tiltakshaver.adresse != null)
            {
                propertyIdentifiers.AnsvarligSokerAdresselinje1 = form.tiltakshaver.adresse.adresselinje1;
                propertyIdentifiers.AnsvarligSokerPostnr = form.tiltakshaver.adresse.postnr;
                propertyIdentifiers.AnsvarligSokerPoststed = form.tiltakshaver.adresse.poststed;
            }

            return propertyIdentifiers;
        }

        public ValidationResult ValidateData()
        {

            return new TiltakUtenAnsvarsrettFormV3Validator().Validate(form, FormSetElements);
        }

        public string GetDataFormatId()
        {
            return DataFormatId;
        }

        public string GetDataFormatVersion()
        {
            return DataFormatVersion;
        }

        public string GetMunicipalityCode()
        {
            return GetPropertyIdentifiers().Kommunenummer;
        }

        public string GetSchemaFile()
        {
            return SchemaFile;
        }

        public string GetName()
        {
            return Name;
        }
    }
}