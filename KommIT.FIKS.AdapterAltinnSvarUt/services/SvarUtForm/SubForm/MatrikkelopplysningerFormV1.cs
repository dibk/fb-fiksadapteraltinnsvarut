﻿using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.SvarUtForm.SubForm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.SvarUt
{
    public class MatrikkelopplysningerFormV1 : IAltinnForm
    {
        private const string DataFormatId = "5625";
        private const string DataFormatVersion = "42144";
        private const string SchemaFile = "matrikkelregistrering01.xsd";
        private const string Name = "Matrikkelopplysninger";
        private List<string> FormSetElements;
        public void SetFormSetElements(List<string> attachmentlist)
        {
            FormSetElements = attachmentlist;
        }

        public bool? ValidateAuthenticatedSubmitterVsFormData(string authenticatedSubmitter, ValidationResult validationResult)
        {
            bool isSubmitterValid = Helpers.ValidateEntityOfAltinnSubmitterDefault(authenticatedSubmitter, GetPropertyIdentifiers());

            if (!isSubmitterValid)
            {
                Helpers.RecordErrorAltinnSubmitter(validationResult);
            }
            return isSubmitterValid;
        }

        public void InitiateForm(string formDataAsXml) {
            
            form = Utils.SerializeUtil.DeserializeFromString<no.kxml.skjema.dibk.matrikkelregistrering.MatrikkelregistreringType>(formDataAsXml);
        }

        private no.kxml.skjema.dibk.matrikkelregistrering.MatrikkelregistreringType form;

        public PropertyIdentifiers GetPropertyIdentifiers()
        {
            PropertyIdentifiers propertyIdentifiers = new PropertyIdentifiers();

            if (form.eiendomByggested != null && form.eiendomByggested.Any())
            {
                propertyIdentifiers.Kommunenummer = form.eiendomByggested.First().eiendomsidentifikasjon.kommunenummer;
                propertyIdentifiers.Gaardsnummer = form.eiendomByggested.First().eiendomsidentifikasjon.gaardsnummer;
                propertyIdentifiers.Bruksnummer = form.eiendomByggested.First().eiendomsidentifikasjon.bruksnummer;
                propertyIdentifiers.Festenummer = form.eiendomByggested.First().eiendomsidentifikasjon.festenummer;
                propertyIdentifiers.Seksjonsnummer = form.eiendomByggested.First().eiendomsidentifikasjon.seksjonsnummer;
            }

            if (form.eiendomByggested.First().adresse != null)
            {
                propertyIdentifiers.Adresselinje1 = form.eiendomByggested.First().adresse.adresselinje1 + " " + form.eiendomByggested.First().adresse.husnr + " " + form.eiendomByggested.First().adresse.bokstav;
            }

            propertyIdentifiers.Bygningsnummer = form.bygning.First().bygningsnummer;
            //propertyIdentifiers.Bolignummer = form.eiendomByggested.First().bolignummer;
            propertyIdentifiers.SoeknadSkjemaNavn = Name;
            propertyIdentifiers.FraSluttbrukersystem = "";//form.fraSluttbrukersystem;

            return propertyIdentifiers;
        }

        public ValidationResult ValidateData()
        {
            return new MatrikkelopplysningerFormV1Validator().Validate(form, FormSetElements);
        }

        public string GetDataFormatId()
        {
            return DataFormatId;
        }

        public string GetDataFormatVersion()
        {
            return DataFormatVersion;
        }

        public string GetMunicipalityCode()
        {
            return GetPropertyIdentifiers().Kommunenummer;
        }

        public string GetSchemaFile()
        {
            return SchemaFile;
        }

        public string GetName()
        {
            return Name;
        }
    }
}