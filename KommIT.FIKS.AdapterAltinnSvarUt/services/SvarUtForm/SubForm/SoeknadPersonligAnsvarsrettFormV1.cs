﻿using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Validations;
using no.kxml.skjema.dibk.soeknadpersonligansvarsrett;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.SvarUt
{
    public class SoeknadPersonligAnsvarsrettFormV1 : IAltinnForm
    {
        private const string DataFormatId = "5500";
        private const string DataFormatVersion = "41791";
        private const string SchemaFile = "soeknadpersonligansvarsrett09.xsd";
        private const string Name = "Søknad om ansvarsrett for selvbygger";

        public IMunicipalityValidator MunicipalityValidator { get; set; }

        public SoeknadPersonligAnsvarsrettFormV1()
        {
            MunicipalityValidator = new MunicipalityValidator();
        }
        private List<string> FormSetElements;
        public void SetFormSetElements(List<string> attachmentlist)
        {
            FormSetElements = attachmentlist;
        }

        public bool? ValidateAuthenticatedSubmitterVsFormData(string authenticatedSubmitter, ValidationResult validationResult)
        {
            bool isSubmitterValid = Helpers.ValidateEntityOfAltinnSubmitterDefault(authenticatedSubmitter, GetPropertyIdentifiers());

            if (!isSubmitterValid)
            {
                Helpers.RecordErrorAltinnSubmitter(validationResult);
            }
            return isSubmitterValid;
        }

        public void InitiateForm(string formDataAsXml) {
            
            form = Utils.SerializeUtil.DeserializeFromString<no.kxml.skjema.dibk.soeknadpersonligansvarsrett.SoeknadPersonligAnsvarsrettType>(formDataAsXml);
        }

        private no.kxml.skjema.dibk.soeknadpersonligansvarsrett.SoeknadPersonligAnsvarsrettType form;

        public PropertyIdentifiers GetPropertyIdentifiers()
        {
            PropertyIdentifiers propertyIdentifiers = new PropertyIdentifiers();

            propertyIdentifiers.Kommunenummer = form.eiendomByggested.First().eiendomsidentifikasjon.kommunenummer;
            propertyIdentifiers.Gaardsnummer = form.eiendomByggested.First().eiendomsidentifikasjon.gaardsnummer;
            propertyIdentifiers.Bruksnummer = form.eiendomByggested.First().eiendomsidentifikasjon.bruksnummer;
            propertyIdentifiers.Festenummer = form.eiendomByggested.First().eiendomsidentifikasjon.festenummer;
            propertyIdentifiers.Seksjonsnummer = form.eiendomByggested.First().eiendomsidentifikasjon.seksjonsnummer;
            propertyIdentifiers.Adresselinje1 = form.eiendomByggested.First().adresse.adresselinje1;
            propertyIdentifiers.Adresselinje2 = "";
            propertyIdentifiers.Adresselinje3 = "";
            propertyIdentifiers.Postnr = form.eiendomByggested.First().adresse.postnr;
            propertyIdentifiers.Poststed = form.eiendomByggested.First().adresse.poststed;
            propertyIdentifiers.Landkode = form.eiendomByggested.First().adresse.landkode;
            propertyIdentifiers.Bygningsnummer = form.eiendomByggested.First().bygningsnummer;
            propertyIdentifiers.Bolignummer = form.eiendomByggested.First().bolignummer;
            propertyIdentifiers.SoeknadSkjemaNavn = Name;
            propertyIdentifiers.FraSluttbrukersystem = "";// form.fraSluttbrukersystem;

            
            propertyIdentifiers.AnsvarligSokerNavn = form.tiltakshaver.navn;
            propertyIdentifiers.AnsvarligSokerOrgnr = form.tiltakshaver.organisasjonsnummer;
            propertyIdentifiers.AnsvarligSokerFnr = form.tiltakshaver.foedselsnummer;

            if (form.tiltakshaver.adresse != null)
            {
                propertyIdentifiers.AnsvarligSokerAdresselinje1 = form.tiltakshaver.adresse.adresselinje1;
                propertyIdentifiers.AnsvarligSokerPostnr = form.tiltakshaver.adresse.postnr;
                propertyIdentifiers.AnsvarligSokerPoststed = form.tiltakshaver.adresse.poststed;
            }
            return propertyIdentifiers;
        }

        public ValidationResult ValidateData()
        {
   
            ValidationResult valResult = new ValidationResult(0, 0);

            if (form.eiendomByggested == null || !form.eiendomByggested.Any())
                valResult.AddError("Minst en eiendom må være utfylt", "4701.1.4");
            else
            {
                foreach (var eiendom in form.eiendomByggested)
                {
                    
                    GeneralValidationResult generalValidationResult = MunicipalityValidator.ValidateKommunenummer(eiendom.eiendomsidentifikasjon.kommunenummer);
                    if (generalValidationResult.Status != ValidationStatus.Ok)
                    {
                        valResult.AddError(generalValidationResult.Message, "4701.1.2");
                    }
                }
            }

            if (String.IsNullOrEmpty(GetPropertyIdentifiers().AnsvarligSokerNavn))
            {
                valResult.AddError("Søkers navn må være utfylt", "4701.1.4");
            }
            if (String.IsNullOrEmpty(GetPropertyIdentifiers().AnsvarligSokerOrgnr) && String.IsNullOrEmpty(GetPropertyIdentifiers().AnsvarligSokerFnr))
            {
                valResult.AddError("Søkers organisasjonsnummer eller fødselsnummer må være utfylt.", "4701.1.5");
            }
            if (String.IsNullOrEmpty(GetPropertyIdentifiers().AnsvarligSokerPostnr))
            {
                valResult.AddError("Søkers postnummer må være utfylt", "4701.1.4");
            }
            

            return valResult;
        }
        

        public string GetDataFormatId()
        {
            return DataFormatId;
        }

        public string GetDataFormatVersion()
        {
            return DataFormatVersion;
        }

        public string GetMunicipalityCode()
        {
            return GetPropertyIdentifiers().Kommunenummer;
        }

        public string GetSchemaFile()
        {
            return SchemaFile;
        }

        public string GetName()
        {
            return Name;
        }
    }
}