﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.SvarUtForm.SubForm
{
    public class GjenpartnabovarselV2FormV1 : IAltinnForm
    {
        private const string DataFormatId = "5826";
        private const string DataFormatVersion = "42895";
        private const string SchemaFile = "gjenpartnabovarselV2.xsd";
        private const string Name = "Opplysninger gitt i nabovarsel";
        private List<string> FormSetElements;
        public void SetFormSetElements(List<string> attachmentlist)
        {
            FormSetElements = attachmentlist;
        }

        public bool? ValidateAuthenticatedSubmitterVsFormData(string authenticatedSubmitter, ValidationResult validationResult)
        {
            bool isSubmitterValid = Helpers.ValidateEntityOfAltinnSubmitterDefault(authenticatedSubmitter, GetPropertyIdentifiers());

            if (!isSubmitterValid)
            {
                Helpers.RecordErrorAltinnSubmitter(validationResult);
            }
            return isSubmitterValid;
        }

        public void InitiateForm(string formDataAsXml)
        {

            form = Utils.SerializeUtil.DeserializeFromString<no.kxml.skjema.dibk.gjenpartnabovarselV2.NabovarselType>(formDataAsXml);
        }

        private no.kxml.skjema.dibk.gjenpartnabovarselV2.NabovarselType form;

        public PropertyIdentifiers GetPropertyIdentifiers()
        {
            PropertyIdentifiers propertyIdentifiers = new PropertyIdentifiers();

            propertyIdentifiers.Kommunenummer = form.eiendomByggested.First().eiendomsidentifikasjon.kommunenummer;
            propertyIdentifiers.Gaardsnummer = form.eiendomByggested.First().eiendomsidentifikasjon.gaardsnummer;
            propertyIdentifiers.Bruksnummer = form.eiendomByggested.First().eiendomsidentifikasjon.bruksnummer;
            propertyIdentifiers.Festenummer = form.eiendomByggested.First().eiendomsidentifikasjon.festenummer;
            propertyIdentifiers.Seksjonsnummer = form.eiendomByggested.First().eiendomsidentifikasjon.seksjonsnummer;
            propertyIdentifiers.Adresselinje1 = form.eiendomByggested.First().adresse.adresselinje1;
            propertyIdentifiers.Adresselinje2 = "";
            propertyIdentifiers.Adresselinje3 = "";
            propertyIdentifiers.Postnr = form.eiendomByggested.First().adresse.postnr;
            propertyIdentifiers.Poststed = form.eiendomByggested.First().adresse.poststed;
            propertyIdentifiers.Landkode = form.eiendomByggested.First().adresse.landkode;
            propertyIdentifiers.Bygningsnummer = form.eiendomByggested.First().bygningsnummer;
            propertyIdentifiers.Bolignummer = form.eiendomByggested.First().bolignummer;
            propertyIdentifiers.SoeknadSkjemaNavn = Name;
            propertyIdentifiers.FraSluttbrukersystem = "";// form.fraSluttbrukersystem;


            propertyIdentifiers.AnsvarligSokerNavn = form.ansvarligSoeker.navn;
            propertyIdentifiers.AnsvarligSokerOrgnr = form.ansvarligSoeker.organisasjonsnummer;
            propertyIdentifiers.AnsvarligSokerFnr = form.ansvarligSoeker.foedselsnummer;

            if (form.ansvarligSoeker.adresse != null)
            {
                propertyIdentifiers.AnsvarligSokerAdresselinje1 = form.ansvarligSoeker.adresse.adresselinje1;
                propertyIdentifiers.AnsvarligSokerPostnr = form.ansvarligSoeker.adresse.postnr;
                propertyIdentifiers.AnsvarligSokerPoststed = form.ansvarligSoeker.adresse.poststed;
            }
            return propertyIdentifiers;
        }

        public ValidationResult ValidateData()
        {
            return new GjenpartnabovarselV2Validator().Validate(form, FormSetElements);
        }

        public string GetDataFormatId()
        {
            return DataFormatId;
        }

        public string GetDataFormatVersion()
        {
            return DataFormatVersion;
        }

        public string GetMunicipalityCode()
        {
            return GetPropertyIdentifiers().Kommunenummer;
        }

        public string GetSchemaFile()
        {
            return SchemaFile;
        }

        public string GetName()
        {
            return Name;
        }
    }
}
