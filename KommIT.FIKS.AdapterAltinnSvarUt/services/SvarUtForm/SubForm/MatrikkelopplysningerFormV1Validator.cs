﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DIBK.FBygg.Altinn.MapperCodelist;
using no.kxml.skjema.dibk.matrikkelregistrering;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.SvarUtForm.SubForm
{
    public class MatrikkelopplysningerFormV1Validator
    {
        private ValidationResult _validationResult = new ValidationResult();
        CodeListService _codeListService = new CodeListService();

        internal ValidationResult GetResult() => _validationResult;
        public ValidationResult Validate(MatrikkelregistreringType form, List<string> formSetElements)
        {
            _validationResult = new ValidationResult();

            EiendomValidering(form);

            BygningValidering(form, formSetElements);




            return _validationResult;
        }

        internal void BygningValidering(MatrikkelregistreringType form, List<string> formSetElements)
        {

            if (form.bygning == null || !form.bygning.Any())
                _validationResult.AddError("Minst en bygning må angis.", "4920.1.1", "/Matrikkelregistrering/bygning");
            else
            {

                foreach (var bygning in form.bygning)
                {
                    //Fra RS
                    //bygningstype
                    if (bygning.bygningstype == null)
                    {
                        _validationResult.AddError("Bygningstype skal angis.", "4920.1.4", "/Matrikkelregistrering/bygning[0]/bygningstype");
                    }
                    else if (String.IsNullOrEmpty(bygning.bygningstype.kodeverdi))
                    {
                        _validationResult.AddError("Bygningstype skal angis.", "4920.1.4", "/Matrikkelregistrering/bygning[0]/bygningstype/kodeverdi");
                    }
                    else if (!_codeListService.IsCodelistValid("Bygningstype", bygning.bygningstype.kodeverdi))
                    {
                        _validationResult.AddError($"Bygningstype har ugyldig kodeverdi {bygning.bygningstype.kodeverdi}.", "4920.1.2", "/Matrikkelregistrering/bygning[0]/bygningstype/kodeverdi");
                    }


                    //næringsgruppe
                    if (bygning.naeringsgruppe == null)
                    {
                        _validationResult.AddError("Næringsgruppe skal angis.", "4920.1.5", "/Matrikkelregistrering/bygning[0]/naeringsgruppe");
                    }
                    else if (String.IsNullOrEmpty(bygning.naeringsgruppe.kodeverdi))
                    {
                        _validationResult.AddError("Næringsgruppe skal angis.", "4920.1.5", "/Matrikkelregistrering/bygning[0]/naeringsgruppe/kodeverdi");
                    }
                    else if (!_codeListService.IsCodelistValid("naeringsgruppe", bygning.naeringsgruppe.kodeverdi))
                    {
                        _validationResult.AddError($"Næringsgruppe har ugyldig kodeverdi {bygning.naeringsgruppe.kodeverdi}.", "4920.1.2", "/Matrikkelregistrering/bygning[0]/naeringsgruppe/kodeverdi");
                    }

                    //bebygd areal
                    if (bygning.bebygdArealSpecified == false)
                    {
                        _validationResult.AddError("Bebygd areal skal angis.", "4920.1.6", "/Matrikkelregistrering/bygning[0]/bebygdAreal");
                    }

                    if (formSetElements == null)
                    {
                        _validationResult.AddError("Søknad og evt vedlegg må angis.", "4920.1.1", "/Vedlegg");
                    }
                    else
                    {
                        //Fra IG
                        if (formSetElements.Where(substring => substring.Contains("Søknad om tillatelse til tiltak uten ansvarsrett") || substring.Contains("Søknad om tillatelse i ett trinn") || substring.Contains("Søknad om igangsettingstillatelse")).Count() > 0)
                        {
                            //Minst en etasje
                            if (bygning.etasjer == null || !bygning.etasjer.Any())
                                _validationResult.AddWarning("Minst en etasje bør angis for bygning.", "4920.1.7", "/Matrikkelregistrering/bygning[0]/etasjer");
                            else
                            {

                                foreach (var etasje in bygning.etasjer)
                                {
                                    //Sjekke kodeverdi på etasjeplan
                                    if (String.IsNullOrEmpty(etasje.etasjeplan.kodeverdi))
                                    {
                                        _validationResult.AddError("Etasjeplan må angis for etasje.", "4920.1.1", "/Matrikkelregistrering/bygning[0]/etasjer[0]/etasjeplan/kodeverdi");
                                    }
                                    else if (!_codeListService.IsCodelistValid("Etasjeplan", etasje.etasjeplan.kodeverdi))
                                    {
                                        _validationResult.AddError($"Etasjeplan har ugyldig kodeverdi {etasje.etasjeplan.kodeverdi}.", "4920.1.2", "/Matrikkelregistrering/bygning[0]/etasjer[0]/etasjeplan/kodeverdi");
                                    }
                                }
                            }
                            //Minst en bruksenhet
                            if (bygning.bruksenheter == null || !bygning.bruksenheter.Any())
                                _validationResult.AddWarning("Minst en bruksenhet bør angis for bygning.", "4920.1.8", "/Matrikkelregistrering/bygning[0]/bruksenheter");
                            else
                            {
                                //Sjekke kodeverdi på etasjeplan
                                //Sjekke kodeverdi på bruksenhetstype
                                foreach (var bruksenhet in bygning.bruksenheter)
                                {
                                    //Sjekke kodeverdi på etasjeplan
                                    if (bruksenhet.bruksenhetsnummer != null)
                                    {
                                        if (bruksenhet.bruksenhetsnummer.etasjeplan != null)
                                        {
                                            if (String.IsNullOrEmpty(bruksenhet.bruksenhetsnummer.etasjeplan.kodeverdi))
                                            {
                                                _validationResult.AddError("Etasjeplan må angis for bruksenhet.", "4920.1.1", "/Matrikkelregistrering/bygning[0]/bruksenheter[0]/bruksenhetsnummer/etasjeplan/kodeverdi");
                                            }
                                            else if (!_codeListService.IsCodelistValid("Etasjeplan", bruksenhet.bruksenhetsnummer.etasjeplan.kodeverdi))
                                            {
                                                _validationResult.AddError($"Etasjeplan har ugyldig kodeverdi {bruksenhet.bruksenhetsnummer.etasjeplan.kodeverdi}.", "4920.1.2", "/Matrikkelregistrering/bygning[0]/bruksenheter[0]/bruksenhetsnummer/etasjeplan/kodeverdi");
                                            }
                                        }
                                        else
                                        {
                                            _validationResult.AddError("Etasjeplan må angis for bruksenhet.", "4920.1.1", "/Matrikkelregistrering/bygning[0]/bruksenheter[0]/bruksenhetsnummer/etasjeplan");
                                        }
                                    }
                                    else
                                    {
                                        _validationResult.AddError("Bruksenhetsnummer må angis for bruksenhet.", "4920.1.1", "/Matrikkelregistrering/bygning[0]/bruksenheter[0]/bruksenhetsnummer");
                                    }
                                    if (bruksenhet.bruksenhetstype != null)
                                    {
                                        if (!String.IsNullOrEmpty(bruksenhet.bruksenhetstype.kodeverdi))
                                        {
                                            if (!_codeListService.IsCodelistValid("Bruksenhetstype", bruksenhet.bruksenhetstype.kodeverdi))
                                            {
                                                _validationResult.AddError($"Bruksenhetstype har ugyldig kodeverdi {bruksenhet.bruksenhetstype.kodeverdi}.", "4920.1.2", "/Matrikkelregistrering/bygning[0]/bruksenheter[0]/bruksenhetstype/kodeverdi");
                                            }
                                        }

                                    }

                                }
                            }

                            //Vannforsyning
                            if (bygning.vannforsyning == null)
                            {
                                _validationResult.AddWarning("Vannforsyning bør angis for bygning.", "4920.1.9", "/Matrikkelregistrering/bygning[0]/vannforsyning");
                            }
                            else if (String.IsNullOrEmpty(bygning.vannforsyning.kodeverdi))
                            {
                                _validationResult.AddWarning("Vannforsyning bør angis for bygning.", "4920.1.9", "/Matrikkelregistrering/bygning[0]/vannforsyning/kodeverdi");
                            }
                            else if (!_codeListService.IsCodelistValid("vanntilknytning", bygning.vannforsyning.kodeverdi))
                            {
                                _validationResult.AddError($"Vannforsyning har ugyldig kodeverdi {bygning.vannforsyning.kodeverdi}.", "4920.1.2", "/Matrikkelregistrering/bygning[0]/vannforsyning/kodeverdi");
                            }

                            //Avløp
                            if (bygning.avlop == null)
                            {
                                _validationResult.AddWarning("Avløpstilknytning bør angis for bygning.", "4920.1.10", "/Matrikkelregistrering/bygning[0]/avlop");
                            }
                            else if (String.IsNullOrEmpty(bygning.avlop.kodeverdi))
                            {
                                _validationResult.AddWarning("Avløpstilknytning bør angis for bygning.", "4920.1.10", "/Matrikkelregistrering/bygning[0]/avlop/kodeverdi");
                            }
                            else if (!_codeListService.IsCodelistValid("avlopstilknytning", bygning.avlop.kodeverdi))
                            {
                                _validationResult.AddError($"Avløpstilknytning har ugyldig kodeverdi {bygning.avlop.kodeverdi}.", "4920.1.2", "/Matrikkelregistrering/bygning[0]/avlop/kodeverdi");
                            }

                            //Heis
                            if (bygning.harHeisSpecified == false)
                            {
                                _validationResult.AddWarning("Har heis bør angis for bygning.", "4920.1.11", "/Matrikkelregistrering/bygning[0]/harHeis");
                            }
                        }

                        //Fra FA
                        if (formSetElements.Where(substring => substring.Contains("Søknad om ferdigattest")).Count() > 0)
                        {

                            if (bygning.energiforsyning == null)
                            {
                                _validationResult.AddWarning("Energiforsyning bør angis.", "4920.1.12", "/Matrikkelregistrering/bygning[0]/energiforsyning");
                            }
                            else
                            {
                                //Energikilde
                                if (bygning.energiforsyning.energiforsyning == null || !bygning.energiforsyning.energiforsyning.Any())
                                {
                                    _validationResult.AddWarning("Energiforsyning/Energikilder bør angis.", "4920.1.12", "/Matrikkelregistrering/bygning[0]/energiforsyning/energiforsyning");
                                }
                                else
                                {
                                    foreach (var kode in bygning.energiforsyning.energiforsyning)
                                    {
                                        if (String.IsNullOrEmpty(kode.kodeverdi))
                                        {
                                            _validationResult.AddWarning("Energiforsyning/Energikilder bør angis.", "4920.1.12", "/Matrikkelregistrering/bygning[0]/energiforsyning/energiforsyning");
                                        }
                                        else if (!_codeListService.IsCodelistValid("energiforsyningtype", kode.kodeverdi))
                                        {
                                            _validationResult.AddError($"Energiforsyning/Energikilder har ugyldig kodeverdi {kode.kodeverdi}.", "4920.1.2", "/Matrikkelregistrering/bygning[0]/energiforsyning/energiforsyning[0]/kodeverdi");
                                        }
                                    }
                                }
                                //Oppvarmingstype
                                if (bygning.energiforsyning.varmefordeling == null || !bygning.energiforsyning.varmefordeling.Any())
                                {
                                    _validationResult.AddWarning("Varmefordeling/Oppvarmingstyper bør angis.", "4920.1.13", "/Matrikkelregistrering/bygning[0]/energiforsyning/varmefordeling");
                                }
                                else
                                {
                                    foreach (var kode in bygning.energiforsyning.varmefordeling)
                                    {
                                        if (String.IsNullOrEmpty(kode.kodeverdi))
                                        {
                                            _validationResult.AddWarning("Varmefordeling/Oppvarmingstyper bør angis.", "4920.1.12", "/Matrikkelregistrering/bygning[0]/energiforsyning/varmefordeling");
                                        }
                                        else if (!_codeListService.IsCodelistValid("Varmefordeling", kode.kodeverdi))
                                        {
                                            _validationResult.AddError($"Varmefordeling/Oppvarmingstyper har ugyldig kodeverdi {kode.kodeverdi}.", "4920.1.2", "/Matrikkelregistrering/bygning[0]/energiforsyning/varmefordeling[0]/kodeverdi");
                                        }
                                    }
                                }
                            }

                        }
                    }
                }

            }


        }

        internal void EiendomValidering(MatrikkelregistreringType form)
        {

            if (form.eiendomByggested == null || !form.eiendomByggested.Any())
                _validationResult.AddWarning("Minst en eiendom bør angis", "4920.1.3",
                    "/Matrikkelregistrering/eiendomsidentifikasjon");
            else
            {
                foreach (var eiendom in form.eiendomByggested)
                {
                    //TODO Sjekke kommunenr 4920.1.2

                }

            }


        }
    }
}