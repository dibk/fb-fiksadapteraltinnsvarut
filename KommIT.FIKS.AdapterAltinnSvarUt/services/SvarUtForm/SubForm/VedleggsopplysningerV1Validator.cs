﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DIBK.FBygg.Altinn.MapperCodelist;
using no.kxml.skjema.dibk.vedlegg;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.SvarUtForm.SubForm
{
    public class VedleggsopplysningerV1Validator
    {
        private ValidationResult _validationResult = new ValidationResult();

        internal ValidationResult GetResult() => _validationResult;
        public ValidationResult Validate(VedleggsopplysningerType form, List<string> formSetElements)
        {
            _validationResult = new ValidationResult();
            Vedleggstype(form);
            //IfcFileChecksum(form, formSetElements);


            return _validationResult;
        }

        internal void IfcFileChecksum(VedleggType form, List<string> formSetElements)
        {
            if (formSetElements != null)
            {
                if (string.IsNullOrEmpty(form.ifcValidationId))
                {
                    _validationResult.AddError("...ifcValidationId må fylles ut.", "5058.1.4", "/Vedlegg/ifcValidationId");
                }
                else
                {
                    if (formSetElements.Contains("valideringsstatus"))
                    {
                        int temp;
                        var enumerable = formSetElements.Where(x => int.TryParse(x, out temp));
                        var setElements = enumerable as string[] ?? enumerable.ToArray();

                        if (setElements.Any())
                        {
                            foreach (var formSetElement in setElements)
                            {
                                switch (formSetElement)
                                {
                                    case "401":
                                        _validationResult.AddError("...Filen er ikke godkjent...", "5058.1.*",
                                            "..");
                                        break;
                                    case "412":
                                        _validationResult.AddError("...Filen er endret etter validering...", "5058.1.*",
                                            "..");
                                        break;
                                    case "417":
                                        _validationResult.AddError("...Kan ikke finne validation Id...", "5058.1.*",
                                            "..");
                                        break;
                                    case "500":
                                        _validationResult.AddError("...Internal Server Error...", "5058.1.*",
                                            "..");
                                        break;
                                    case "200":
                                        break;
                                    default:
                                        _validationResult.AddError("...status eksisterer ikke...", "5058.1.*",
                                            "..");
                                        break;
                                }
                            }
                        }
                        else
                        {
                            _validationResult.AddError("...kreves validation kode.", "5058.1.*", "../ApiValidate/ApiValidate_GetValideringsStatus");
                        }
                    }
                }
            }
        }


        internal void Vedleggstype(VedleggsopplysningerType form)
        {
            foreach (var vedlegg in form.vedlegg)
            {
                if (vedlegg.vedleggstype.Contains("TegningEksisterendePlan")|| vedlegg.vedleggstype.Contains("TegningNyPlan") ||
                    vedlegg.vedleggstype.Contains("TegningEksisterendeSnitt") || vedlegg.vedleggstype.Contains("TegningNyttSnitt") ||
                    vedlegg.vedleggstype.Contains("TegningNyFasade") || vedlegg.vedleggstype.Contains("TegningEksisterendeFasade"))
                {
                    if (string.IsNullOrEmpty(vedlegg.tegningsnr)|| vedlegg.tegningsdato==null)
                        _validationResult.AddWarning("Når vedleggstype er tegninger bør tegningsnr og tegningsdato fylles ut.", "5058.1.5", "/vedleggsopplysninger/vedlegg/tegningsnr");
                }
                if (vedlegg.vedleggstype.Contains("ByggesaksBIM"))
                {
                    if (string.IsNullOrEmpty(vedlegg.ifcValidationId))
                    {
                        _validationResult.AddWarning("Når vedleggstype er ByggesaksBIM bør ifcValidationId fylles ut.", "5058.1.4", "/vedleggsopplysninger/vedlegg/ifcValidationId");
                    }
                }
            }
            
        }
    }
}