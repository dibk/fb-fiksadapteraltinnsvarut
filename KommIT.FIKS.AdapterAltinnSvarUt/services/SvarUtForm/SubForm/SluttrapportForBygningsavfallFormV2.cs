﻿using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;
using System.Collections.Generic;
using System.Linq;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.SvarUt
{
    public class SluttrapportForBygningsavfallFormV2 : IAltinnForm
    {
        private const string DataFormatId = "7063";
        private const string DataFormatVersion = "47177";
        private const string SchemaFile = "AvfallsplanV2.xsd";
        private const string Name = "Sluttrapport for bygningsavfall";
        private List<string> FormSetElements;
        public void SetFormSetElements(List<string> attachmentlist)
        {
            FormSetElements = attachmentlist;
        }

        public bool? ValidateAuthenticatedSubmitterVsFormData(string authenticatedSubmitter, ValidationResult validationResult)
        {
            bool isSubmitterValid = Helpers.ValidateEntityOfAltinnSubmitterDefault(authenticatedSubmitter, GetPropertyIdentifiers());

            if (!isSubmitterValid)
            {
                Helpers.RecordErrorAltinnSubmitter(validationResult);
            }
            return isSubmitterValid;
        }

        public void InitiateForm(string formDataAsXml) {
            
            form = Utils.SerializeUtil.DeserializeFromString<no.kxml.skjema.dibk.avfallsplan.AvfallsplanType>(formDataAsXml);
        }

        private no.kxml.skjema.dibk.avfallsplan.AvfallsplanType form;

        public PropertyIdentifiers GetPropertyIdentifiers()
        {
            PropertyIdentifiers propertyIdentifiers = new PropertyIdentifiers();

            propertyIdentifiers.Kommunenummer = form.eiendomByggested.First().eiendomsidentifikasjon.kommunenummer;
            propertyIdentifiers.Gaardsnummer = form.eiendomByggested.First().eiendomsidentifikasjon.gaardsnummer;
            propertyIdentifiers.Bruksnummer = form.eiendomByggested.First().eiendomsidentifikasjon.bruksnummer;
            propertyIdentifiers.Festenummer = form.eiendomByggested.First().eiendomsidentifikasjon.festenummer;
            propertyIdentifiers.Seksjonsnummer = form.eiendomByggested.First().eiendomsidentifikasjon.seksjonsnummer;
            propertyIdentifiers.Adresselinje1 = form.eiendomByggested.First().adresse.adresselinje1;
            propertyIdentifiers.Adresselinje2 = "";
            propertyIdentifiers.Adresselinje3 = "";
            propertyIdentifiers.Postnr = form.eiendomByggested.First().adresse.postnr;
            propertyIdentifiers.Poststed = form.eiendomByggested.First().adresse.poststed;
            propertyIdentifiers.Landkode = form.eiendomByggested.First().adresse.landkode;
            propertyIdentifiers.Bygningsnummer = form.eiendomByggested.First().bygningsnummer;
            propertyIdentifiers.Bolignummer = form.eiendomByggested.First().bolignummer;
            propertyIdentifiers.SoeknadSkjemaNavn = Name;
            propertyIdentifiers.FraSluttbrukersystem = "";//form.fraSluttbrukersystem;

            
            propertyIdentifiers.AnsvarligSokerNavn = form.ansvarligSoeker.navn;
            propertyIdentifiers.AnsvarligSokerOrgnr = form.ansvarligSoeker.organisasjonsnummer;
            propertyIdentifiers.AnsvarligSokerFnr = form.ansvarligSoeker.foedselsnummer;

            if (form.ansvarligSoeker.adresse != null)
            {
                propertyIdentifiers.AnsvarligSokerAdresselinje1 = form.ansvarligSoeker.adresse.adresselinje1;
                propertyIdentifiers.AnsvarligSokerPostnr = form.ansvarligSoeker.adresse.postnr;
                propertyIdentifiers.AnsvarligSokerPoststed = form.ansvarligSoeker.adresse.poststed;
            }
            return propertyIdentifiers;
        }

        public ValidationResult ValidateData()
        {
   
            ValidationResult valResult = new ValidationResult(0, 0);

          

            return valResult;
        }

        public string GetDataFormatId()
        {
            return DataFormatId;
        }

        public string GetDataFormatVersion()
        {
            return DataFormatVersion;
        }

        public string GetMunicipalityCode()
        {
            return GetPropertyIdentifiers().Kommunenummer;
        }

        public string GetSchemaFile()
        {
            return SchemaFile;
        }

        public string GetName()
        {
            return Name;
        }
    }
}