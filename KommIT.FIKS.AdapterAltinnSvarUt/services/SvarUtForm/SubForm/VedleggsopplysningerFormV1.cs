﻿using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.SvarUtForm.SubForm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.SvarUt
{
    public class VedleggsopplysningerFormV1 : IAltinnForm
    {
        private const string DataFormatId = "5797";
        private const string DataFormatVersion = "42813";
        private const string SchemaFile = "vedlegg.xsd";
        private const string Name = "Vedleggsopplysninger";
        private List<string> FormSetElements;
        public void SetFormSetElements(List<string> attachmentlist)
        {
            FormSetElements = attachmentlist;
        }

        public bool? ValidateAuthenticatedSubmitterVsFormData(string authenticatedSubmitter, ValidationResult validationResult)
        {
            bool isSubmitterValid = Helpers.ValidateEntityOfAltinnSubmitterDefault(authenticatedSubmitter, GetPropertyIdentifiers());

            if (!isSubmitterValid)
            {
                Helpers.RecordErrorAltinnSubmitter(validationResult);
            }
            return isSubmitterValid;
        }

        public void InitiateForm(string formDataAsXml) {
            
            form = Utils.SerializeUtil.DeserializeFromString<no.kxml.skjema.dibk.vedlegg.VedleggsopplysningerType>(formDataAsXml);
        }

        private no.kxml.skjema.dibk.vedlegg.VedleggsopplysningerType form;

        public PropertyIdentifiers GetPropertyIdentifiers()
        {
            PropertyIdentifiers propertyIdentifiers = new PropertyIdentifiers();

            //propertyIdentifiers.Kommunenummer = "";
            //propertyIdentifiers.Gaardsnummer = "";
            //propertyIdentifiers.Bruksnummer = "";
            //propertyIdentifiers.Festenummer = "";
            //propertyIdentifiers.Seksjonsnummer = "";
            //propertyIdentifiers.Adresselinje1 = "";
            //propertyIdentifiers.Adresselinje2 = "";
            //propertyIdentifiers.Adresselinje3 = "";
            //propertyIdentifiers.Postnr = "";
            //propertyIdentifiers.Poststed = "";
            //propertyIdentifiers.Landkode = form.eiendomByggested.First().adresse.landkode;
            //propertyIdentifiers.Bygningsnummer = form.eiendomByggested.First().bygningsnummer;
            //propertyIdentifiers.Bolignummer = form.eiendomByggested.First().bolignummer;
            propertyIdentifiers.SoeknadSkjemaNavn = Name;
            //propertyIdentifiers.FraSluttbrukersystem = form.fraSluttbrukersystem;


            //propertyIdentifiers.AnsvarligSokerNavn = form.ansvarligSoeker.navn;
            //propertyIdentifiers.AnsvarligSokerOrgnr = form.ansvarligSoeker.organisasjonsnummer;
            //propertyIdentifiers.AnsvarligSokerFnr = form.ansvarligSoeker.foedselsnummer;

           
            return propertyIdentifiers;
        }

        public ValidationResult ValidateData()
        {

            return new VedleggsopplysningerV1Validator().Validate(form, FormSetElements);
        }

        public string GetDataFormatId()
        {
            return DataFormatId;
        }

        public string GetDataFormatVersion()
        {
            return DataFormatVersion;
        }

        public string GetMunicipalityCode()
        {
            return GetPropertyIdentifiers().Kommunenummer;
        }

        public string GetSchemaFile()
        {
            return SchemaFile;
        }

        public string GetName()
        {
            return Name;
        }
    }
}