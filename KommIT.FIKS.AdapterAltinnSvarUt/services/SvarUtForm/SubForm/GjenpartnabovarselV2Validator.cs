﻿using System;
using System.Collections.Generic;
using System.Linq;
using DIBK.FBygg.Altinn.MapperCodelist;
using KommIT.FIKS.AdapterAltinnSvarUt.Resources;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Validations;
using no.kxml.skjema.dibk.gjenpartnabovarselV2;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.SvarUtForm.SubForm
{
    public class GjenpartnabovarselV2Validator
    {
        readonly CodeListService _codeListService = new CodeListService();

        private ValidationResult _validationResult = new ValidationResult();
        private List<string> _tiltakstyperISoeknad = new List<string>();

        public IMunicipalityValidator MunicipalityValidator { get; set; }
        public GjenpartnabovarselV2Validator()
        {
            MunicipalityValidator = new MunicipalityValidator();
        }
        internal ValidationResult GetResult() => _validationResult;
        public ValidationResult Validate(NabovarselType form, List<string> formSetElements)
        {
            _validationResult = new ValidationResult();
            _tiltakstyperISoeknad = new List<string>();

            EiendomByggestedValidation(form);
            MinstEtTiltak(form);
            DispensasjonValidering(form);

            return _validationResult;
        }

        internal void EiendomByggestedValidation(NabovarselType form)
        {
            if (MinstEnEiendom(form))
            {
                foreach (var eiendom in form.eiendomByggested)
                {
                    if (eiendom.eiendomsidentifikasjon != null) GyldigKommuneNrEiendom(eiendom.eiendomsidentifikasjon.kommunenummer);

                    if (String.IsNullOrEmpty(eiendom.kommunenavn))
                    {
                        _validationResult.AddWarning("Kommunenavn bør fylles ut for eiendom/byggested", "4816.1.1", "/Nabovarsel/eiendomByggested/eiendom[0]/kommunenavn");
                    }

                    if (!String.IsNullOrEmpty(eiendom.bolignummer))
                    {
                        if (GeneralValidations.Validate_bruksenhetsnummer(eiendom.bolignummer).Status == ValidationStatus.Fail)
                        {
                            _validationResult.AddError("Når bruksenhetsnummer(bolignummer) er fylt ut må det følge riktig format (^[HKLU]\\d{4}$) (feks H0101) se https://www.kartverket.no/eiendom/adressering/bruksenhetsnummer/", "4816.1.6", "/Nabovarsel/eiendomByggested/eiendom[0]/bolignummer");
                        }
                    }
                }

            }
        }

        internal void GyldigKommuneNrEiendom(string kommunenr)
        {
            GeneralValidationResult generalValidationResult = MunicipalityValidator.ValidateKommunenummerExists(kommunenr);

            if (generalValidationResult.Status != ValidationStatus.Ok)
            {
                _validationResult.AddError($"{generalValidationResult.Message}", "4816.1.2",
                    "/Nabovarsel/eiendomByggested/eiendom/eiendomsidentifikasjon/kommunenummer");
            }

        }
        internal bool MinstEnEiendom(NabovarselType form)
        {
            if (form.eiendomByggested == null || !form.eiendomByggested.Any())
                _validationResult.AddError(ValidationError.MinstEnEiendom, "4816.1.1",
                    "/Nabovarsel/eiendomByggested/eiendom");

            return (form.eiendomByggested != null || form.eiendomByggested.Any());
        }

        internal void MinstEtTiltak(NabovarselType form)
        {
            if (!form.beskrivelseAvTiltak.Any())
            {
                _validationResult.AddError("Minst en beskrivelse av tiltak må være registrert", "4816.1.1", "/Nabovarsel/beskrivelseAvTiltak");
            }
            else
            {
                if (form.beskrivelseAvTiltak[0].type == null)
                {
                    _validationResult.AddError("Minst en tiltakstype må være registrert", "4816.1.1", "/Nabovarsel/beskrivelseAvTiltak[0]/type");
                }
                else
                {
                    Dictionary<string, CodelistFormat> codeData = _codeListService.GetCodelist("tiltaktype", RegistryType.KodelisteByggesoknad);
                    Dictionary<string, CodelistFormat> codeDataTiltaksformaal = _codeListService.GetCodelist("tiltaksformal", RegistryType.KodelisteByggesoknad);

                    foreach (var beskTiltak in form.beskrivelseAvTiltak)
                    {
                        foreach (var tiltakstype in beskTiltak.type)
                        {
                            if (!String.IsNullOrEmpty(tiltakstype.kodeverdi))
                            {
                                if (!codeData.ContainsKey(tiltakstype.kodeverdi))
                                {
                                    _validationResult.AddError("Ugyldig kodeverdi i henhold til kodeliste for tiltakstype", "4816.1.2", "/Nabovarsel/beskrivelseAvTiltak[0]/type[0]/kodeverdi");
                                }
                                else _tiltakstyperISoeknad.Add(tiltakstype.kodeverdi);
                            }
                        }

                        if (beskTiltak.bruk != null)
                        {
                            //beskTiltak.bruk.tiltaksformaal
                            if (beskTiltak.bruk.tiltaksformaal.Any())
                            {
                                //Kontroller alle kodeverdier i tiltaksformaal
                                foreach (var tiltaksformaal in beskTiltak.bruk.tiltaksformaal)
                                {
                                    if (!String.IsNullOrEmpty(tiltaksformaal.kodeverdi))
                                    {
                                        if (!codeDataTiltaksformaal.ContainsKey(tiltaksformaal.kodeverdi))
                                        {
                                            _validationResult.AddError("Ugyldig kodeverdi i henhold til kodeliste for tiltaksformål", "4816.1.2", "/Nabovarsel/beskrivelseAvTiltak[0]/bruk/tiltaksformaal[0]/kodeverdi");
                                        }
                                        else
                                        {
                                            if (tiltaksformaal.kodeverdi == "Annet")
                                            {
                                                if (String.IsNullOrEmpty(beskTiltak.bruk.beskrivPlanlagtFormaal))
                                                {
                                                    _validationResult.AddError("Når tiltaksformål er registrert som Annet må beskrivelse av planlagt formål fylles ut", "4816.1.5", "/Nabovarsel/beskrivelseAvTiltak[0]/bruk/beskrivPlanlagtFormaal");
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            
                        }
                    }
                }
            }
        }

        internal void DispensasjonValidering(NabovarselType form)
        {

            if (form.dispensasjon != null)
            {
                Dictionary<string, CodelistFormat> codeData = _codeListService.GetCodelist("dispensasjonstype", RegistryType.KodelisteByggesoknad);

                foreach (var dispensasjon in form.dispensasjon)
                {
                    if (String.IsNullOrEmpty(dispensasjon.begrunnelse) || dispensasjon.dispensasjonstype == null)
                        _validationResult.AddError("Når dispensasjon er angitt må dispensasjonstype og begrunnelse fylles ut.", "4816.1.4", "/Nabovarsel/dispensasjon/dispensasjonstype[0]");

                    if (String.IsNullOrEmpty(dispensasjon.beskrivelse))

                        _validationResult.AddWarning("Når dispensasjon er angitt bør beskrivelse fylles ut.", "4816.2.7", "/Nabovarsel/dispensasjon[0]/beskrivelse");


                    if (dispensasjon.dispensasjonstype != null)
                    {
                        if (!String.IsNullOrEmpty(dispensasjon.dispensasjonstype.kodeverdi))
                        {
                            if (!codeData.ContainsKey(dispensasjon.dispensasjonstype.kodeverdi))
                            {
                                _validationResult.AddError("Ugyldig kodeverdi(" + dispensasjon.dispensasjonstype.kodeverdi + ") i henhold til kodeliste for dispensasjonstype", "4816.1.2", "/Nabovarsel/dispensasjon[0]/dispensasjonstype/kodeverdi");
                            }
                        } 
                    }
                } 
            }
        }
      
    }
}