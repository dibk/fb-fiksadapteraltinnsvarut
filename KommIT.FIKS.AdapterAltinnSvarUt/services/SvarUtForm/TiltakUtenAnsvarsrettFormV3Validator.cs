using DIBK.FBygg.Altinn.MapperCodelist;
using KommIT.FIKS.AdapterAltinnSvarUt.Logger;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Validations;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;
using no.kxml.skjema.dibk.tiltakutenansvarsrettV3;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.SvarUtForm
{
    public class TiltakUtenAnsvarsrettFormV3Validator : FormValidator
    {
        public TiltakUtenAnsvarsrettFormV3Validator() : base("TA")
        {
            var form = new TiltakUtenAnsvarsrettType();
            var skjema = "TiltakUtenAnsvarsrett";

            _validationResult.AddRule(skjema, "4373.0.1", null, "En teknisk feil gjør at vi ikke kan bekrefte informasjonen du har oppgitt. Vi anbefaler at du vurderer om følgende punkt gjelder for tiltakstypen(e) du har valgt: '{0}'. Du finner punktene i de nasjonale sjekklistene for byggesak på https://sjekkliste-bygg.ft.dibk.no/checklist/TA", null, "ERROR", null);
            _validationResult.AddRule(skjema, "4373.1.1", null, "Innhold må være i henhold til informasjonsmodell/xsd for skjema.", null, "ERROR", null);
            _validationResult.AddRule(skjema, "4373.1.3", null, "Vedlegg må være i henhold til metadata for skjema. Vedleggstype eller underskjema '{0}' er ikke gyldig.", Helpers.GetFullClassPath(() => form), "ERROR", null);

            //**beskrivelseAvTiltak.type
            _validationResult.AddRule(skjema, "4373.2.3", null, "Minst én beskrivelse av tiltak må være registrert.", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak), "ERROR", null);
            _validationResult.AddRule(skjema, "4373.2.3.1", null, "Du må velge en tiltakstype.", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.type.type), "ERROR", null);
            _validationResult.AddRule(skjema, "4373.2.3.2", null, "'{0}' er en ugyldig kodeverdi for tiltakstype. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/byggesoknad/tiltakstyperutenansvarsrett", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.type.type.kodeverdi), "ERROR", null);
            _validationResult.AddRule(skjema, "4373.2.3.3", null, "Når tiltakstype er valgt, må kodebeskrivelse angis. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/byggesoknad/tiltakstyperutenansvarsrett", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.type.type.kodebeskrivelse), "ERROR", null);
            _validationResult.AddRule(skjema, "4373.2.3.3.1", null, "Kodebeskrivelsen '{0}' stemmer ikke med den valgte kodeverdien. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/byggesoknad/tiltakstyperutenansvarsrett", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.type.type.kodebeskrivelse), "ERROR", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.type.type.kodeverdi));
            _validationResult.AddRule(skjema, "4373.2.3.6", null, "Du må beskrive hva du søker om i tekstfeltet 'følgebrev', eller legge ved vedlegget 'Folgebrev'.", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.foelgebrev), "ERROR", null);
            _validationResult.AddRule(skjema, "4373.2.3.7", null, "Du har valgt tiltakstype 'Annet'. Da må beskrivelse fylles ut.", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.type.beskrivelse), "ERROR", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.bruk.tiltaksformaal[0].kodeverdi));

            //**beskrivelseAvTiltak.bruk
            _validationResult.AddRule(skjema, "4373.2.3.4", null, "'{0}' er en ugyldig kodeverdi for tiltaksformål. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/byggesoknad/tiltaksformal", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.bruk.tiltaksformaal[0].kodeverdi), "WARNING", null);
            _validationResult.AddRule(skjema, "4373.3.3.4.1", null, "Når tiltaksformål er valgt bør kodebeskrivelse angis. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/byggesoknad/tiltaksformal", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.bruk.tiltaksformaal[0].kodebeskrivelse), "WARNING", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.bruk.tiltaksformaal[0].kodeverdi));
            _validationResult.AddRule(skjema, "4373.3.3.4.2", null, "Kodebeskrivelsen '{0}' stemmer ikke med den valgte kodeverdien. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/byggesoknad/tiltaksformal", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.bruk.tiltaksformaal[0].kodebeskrivelse), "WARNING", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.bruk.tiltaksformaal[0].kodeverdi));
            _validationResult.AddRule(skjema, "4373.2.3.5", null, "Når tiltaksformål er registrert som ‘Annet’ må beskrivelse av planlagt formål fylles ut.", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.bruk.beskrivPlanlagtFormaal), "ERROR", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.bruk.tiltaksformaal[0].kodeverdi));

            //Eiendommens--> eiendomsidentifikasjon
            _validationResult.AddRule(skjema, "4373.2.4", null, "Du må oppgi hvilken eiendom/hvilke eiendommer byggesøknaden gjelder.", Helpers.GetFullClassPath(() => form.eiendomByggested), "ERROR", null);
            _validationResult.AddRule(skjema, "4373.2.4.1", "1.2", "Kommunenummer må fylles ut for eiendom/byggested.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].eiendomsidentifikasjon.kommunenummer), "ERROR", null);
            _validationResult.AddRule(skjema, "4373.2.4.2", "1.2", "Kommunenummeret '{0}' for eiendom/byggested finnes ikke i kodelisten. Du kan sjekke riktig kommunenummer på https://register.geonorge.no/sosi-kodelister/inndelinger/inndelingsbase/kommunenummer", Helpers.GetFullClassPath(() => form.eiendomByggested[0].eiendomsidentifikasjon.kommunenummer), "ERROR", null);
            _validationResult.AddRule(skjema, "4373.2.4.3", "1.2", "Kommunenummeret '{0}' for eiendom/byggested har ugyldig status ({1}). Du kan sjekke status på https://register.geonorge.no/sosi-kodelister/inndelinger/inndelingsbase/kommunenummer", Helpers.GetFullClassPath(() => form.eiendomByggested[0].eiendomsidentifikasjon.kommunenummer), "ERROR", null);
            _validationResult.AddRule(skjema, "4373.2.4.4", "1.3", "Gårdsnummer må fylles ut for eiendom/byggested.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].eiendomsidentifikasjon.gaardsnummer), "ERROR", null);
            _validationResult.AddRule(skjema, "4373.2.4.4.1", "1.4", "Bruksnummer må fylles ut for eiendom/byggested.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].eiendomsidentifikasjon.bruksnummer), "ERROR", null);
            _validationResult.AddRule(skjema, "4373.3.4.5", "1.3", "Gårdsnummer '{0}' for eiendom/byggested må være '0' eller større.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].eiendomsidentifikasjon.gaardsnummer), "ERROR", null);
            _validationResult.AddRule(skjema, "4373.3.4.6", "1.4", "Bruksnummer '{0}' for eiendom/byggested må være '0' eller større.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].eiendomsidentifikasjon.gaardsnummer), "ERROR", null);
            _validationResult.AddRule(skjema, "4373.2.4.7", "1.6", "Hvis bygningsnummer {0} er oppgitt for eiendom/byggested, må det være et tall.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].bygningsnummer), "ERROR", null);
            _validationResult.AddRule(skjema, "4373.2.4.8", "1.6", "Bygningsnummer for eiendom/byggested må være større enn '0'.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].bygningsnummer), "ERROR", null);
            //Eiendommens --> Adress
            _validationResult.AddRule(skjema, "4373.2.4.9", "1.8", "Postadresse for eiendom/byggested bør fylles ut.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].adresse), "WARNING", null);
            _validationResult.AddRule(skjema, "4373.2.4.9.1", "1.8", "Adresselinje 1 for eiendom/byggested bør fylles ut.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].adresse.adresselinje1), "WARNING", null);
            _validationResult.AddRule(skjema, "4373.2.4.9.2", null, "Du bør også oppgi gatenavn, husnummer og eventuell bokstav for eiendom/byggested. Du kan sjekke riktig adresse på https://seeiendom.no", Helpers.GetFullClassPath(() => form.eiendomByggested[0].adresse.gatenavn), "WARNING", null);
            _validationResult.AddRule(skjema, "4373.2.4.10", null, "Kommunenavn bør fylles ut for eiendom/byggested.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].kommunenavn), "WARNING", null);
            _validationResult.AddRule(skjema, "4373.2.4.11", null, "Når bruksenhetsnummer/bolignummer er fylt ut for eiendom/byggested, må det følge riktig format (for eksempel H0101). Se https://www.kartverket.no/eiendom/adressering/bruksenhetsnummer/", Helpers.GetFullClassPath(() => form.eiendomByggested[0].bolignummer), "ERROR", null);

            //if (_matrikkelFlag)
            //{
            _validationResult.AddRule(skjema, "4373.2.4.12", "1.72", "Når eiendomsidentifikasjon [{0}-{1}/{2}/{3}/{4}] er oppgitt for eiendom/byggested, bør den være gyldig i matrikkelen. Du kan sjekke riktig informasjon på https://seeiendom.no", Helpers.GetFullClassPath(() => form.eiendomByggested[0].eiendomsidentifikasjon), "WARNING", null);
            _validationResult.AddRule(skjema, "4373.2.4.13", "1.72", "Eiendomsidentifikasjon [{0}-{1}/{2}/{3}/{4}] for eiendom/byggested ble ikke validert mot matrikkelen. Identifikasjonen kan være riktig, men en teknisk feil gjør at vi ikke kan bekrefte det.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].eiendomsidentifikasjon), "WARNING", null);
            _validationResult.AddRule(skjema, "4373.2.4.14", "1.6", "Når bygningsnummer [{0}] er oppgitt for eiendom/byggested, bør det være gyldig i matrikkelen på aktuelt matrikkelnummer. Du kan sjekke riktig bygningsnummer på https://seeiendom.no", Helpers.GetFullClassPath(() => form.eiendomByggested[0].bygningsnummer), "WARNING", null);
            _validationResult.AddRule(skjema, "4373.2.4.15", "1.8", "Gateadressen for eiendom/byggested ble ikke validert mot matrikkelen. Gateadressen kan være riktig, men en teknisk feil gjør at vi ikke kan bekrefte det.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].adresse.gatenavn), "WARNING", null);
            _validationResult.AddRule(skjema, "4373.2.4.15.1", "1.8", "Du har oppgitt feil gateadresse for eiendom/byggested. Du kan sjekke riktig adresse på https://seeiendom.no", Helpers.GetFullClassPath(() => form.eiendomByggested[0].adresse.gatenavn), "WARNING", null);
            //}

            // Tiltakshaver
            _validationResult.AddRule(skjema, "4373.2.5.1", "1.91", "Informasjon om tiltakshaver må fylles ut, jfr. nasjonal sjekkliste punkt 1.91.", Helpers.GetFullClassPath(() => form.tiltakshaver.partstype), "ERROR", null);
            _validationResult.AddRule(skjema, "4373.2.5.2", null, "Kodeverdien for 'partstype' for tiltakshaver må fylles ut.", Helpers.GetFullClassPath(() => form.tiltakshaver.partstype.kodeverdi), "ERROR", null);
            _validationResult.AddRule(skjema, "4373.2.5.3", null, "'{0}' er en ugyldig kodeverdi for partstype for tiltakshaver. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/byggesoknad/partstype", Helpers.GetFullClassPath(() => form.tiltakshaver.partstype.kodeverdi), "ERROR", null);
            _validationResult.AddRule(skjema, "4373.2.5.4", null, "Fødselsnummer må angis når tiltakshaver er privatperson.", Helpers.GetFullClassPath(() => form.tiltakshaver.foedselsnummer), "ERROR", null);
            _validationResult.AddRule(skjema, "4373.2.5.4.1", null, "Fødselsnummeret til tiltakshaver kan ikke dekrypteres.", Helpers.GetFullClassPath(() => form.tiltakshaver.foedselsnummer), "ERROR", null);
            _validationResult.AddRule(skjema, "4373.2.5.4.2", null, "Fødselsnummeret til tiltakshaver er ikke gyldig.", Helpers.GetFullClassPath(() => form.tiltakshaver.foedselsnummer), "ERROR", null);
            _validationResult.AddRule(skjema, "4373.2.5.4.3", null, "Fødselsnummeret til tiltakshaver har ikke gyldig kontrollsiffer.", Helpers.GetFullClassPath(() => form.tiltakshaver.foedselsnummer), "ERROR", null);
            _validationResult.AddRule(skjema, "4373.2.5.5", null, "Organisasjonsnummeret til tiltakshaver må fylles ut når tiltakshaver er en organisasjon.", Helpers.GetFullClassPath(() => form.tiltakshaver.organisasjonsnummer), "ERROR", null);
            _validationResult.AddRule(skjema, "4373.2.5.5.1", null, "Organisasjonsnummeret ('{0}') til tiltakshaver har ikke gyldig kontrollsiffer.", Helpers.GetFullClassPath(() => form.tiltakshaver.organisasjonsnummer), "ERROR", null);
            _validationResult.AddRule(skjema, "4373.2.5.5.2", null, "Organisasjonsnummeret ('{0}') til tiltakshaver er ikke gyldig.", Helpers.GetFullClassPath(() => form.tiltakshaver.organisasjonsnummer), "ERROR", null);
            _validationResult.AddRule(skjema, "4373.2.5.6", null, "Adressen til tiltakshaver må fylles ut.", Helpers.GetFullClassPath(() => form.tiltakshaver.adresse), "ERROR", null);
            _validationResult.AddRule(skjema, "4373.2.5.6.1", null, "Adresselinje 1 må fylles ut for tiltakshaver.", Helpers.GetFullClassPath(() => form.tiltakshaver.adresse.adresselinje1), "ERROR", null);
            _validationResult.AddRule(skjema, "4373.2.5.6.2", null, "Ugyldig landkode for tiltakshaver.", Helpers.GetFullClassPath(() => form.tiltakshaver.adresse.landkode), "WARNING", null);
            _validationResult.AddRule(skjema, "4373.2.5.6.3", null, "Postnummeret til tiltakshaver må angis.", Helpers.GetFullClassPath(() => form.tiltakshaver.adresse.postnr), "ERROR", null);
            _validationResult.AddRule(skjema, "4373.2.5.6.4", null, "Postnummeret '{0}' til tiltakshaver har ikke gyldig kontrollsiffer.", Helpers.GetFullClassPath(() => form.tiltakshaver.adresse.postnr), "ERROR", null);
            _validationResult.AddRule(skjema, "4373.2.5.6.5", null, "Postnummeret '{0}' til tiltakshaver er ugyldig. Du kan sjekke riktig postnummer på http://adressesok.bring.no/", Helpers.GetFullClassPath(() => form.tiltakshaver.adresse.postnr), "ERROR", null);
            _validationResult.AddRule(skjema, "4373.2.5.6.6", null, "Postnummeret '{0}' til tiltakshaver stemmer ikke overens med poststedet '{1}'. Postnummeret er fra '{2}'. Du kan sjekke riktig postnummer/poststed på http://adressesok.bring.no/", Helpers.GetFullClassPath(() => form.tiltakshaver.adresse.postnr), "ERROR", null);
            _validationResult.AddRule(skjema, "4373.2.5.6.7", null, "Postnummeret til tiltakshaver ble ikke validert. Postnummeret kan være riktig, men en teknisk feil gjør at vi ikke kan bekrefte det.", Helpers.GetFullClassPath(() => form.tiltakshaver.adresse.postnr), "WARNING", null);
            _validationResult.AddRule(skjema, "4373.2.5.7", "1.91", "Navnet til tiltakshaver må fylles ut, jfr. nasjonal sjekkliste punkt 1.91.", Helpers.GetFullClassPath(() => form.tiltakshaver.navn), "ERROR", null);
            _validationResult.AddRule(skjema, "4373.2.5.8", null, "Telefonnummeret eller mobilnummeret til tiltakshaver må fylles ut.", Helpers.GetFullClassPath(() => form.tiltakshaver.mobilnummer), "ERROR", null);

            _validationResult.AddRule(skjema, "4373.3.5.9", null, "Telefonnummeret til tiltakshaver må kun inneholde tall og '+'.", Helpers.GetFullClassPath(() => form.tiltakshaver.telefonnummer), "ERROR", null);
            _validationResult.AddRule(skjema, "4373.3.5.10", null, "Mobilnummeret til tiltakshaver må kun inneholde tall og '+'.", Helpers.GetFullClassPath(() => form.tiltakshaver.mobilnummer), "ERROR", null);
            _validationResult.AddRule(skjema, "4373.2.5.11", null, "E-postadressen til tiltakshaver må fylles ut.", Helpers.GetFullClassPath(() => form.tiltakshaver.epost), "ERROR", null);

            _validationResult.AddRule(skjema, "4373.3.5.12", null, "Kontaktperson bør fylles ut når tiltakshaver er en organisasjon.", Helpers.GetFullClassPath(() => form.tiltakshaver.kontaktperson), "WARNING", null);
            _validationResult.AddRule(skjema, "4373.3.5.12.1", null, "Navnet til tiltakshavers kontaktperson bør fylles ut når tiltakshaver er en organisasjon.", Helpers.GetFullClassPath(() => form.tiltakshaver.kontaktperson.navn), "WARNING", null);
            _validationResult.AddRule(skjema, "4373.3.5.12.2", null, "Telefonnummer eller mobilnummer til tiltakshavers kontaktperson bør fylles ut når tiltakshaver er en organisasjon.", Helpers.GetFullClassPath(() => form.tiltakshaver.kontaktperson.navn), "WARNING", null);
            _validationResult.AddRule(skjema, "4373.3.5.12.3", null, "Telefonnummeret til tiltakshavers kontaktperson må kun inneholde tall og '+'.", Helpers.GetFullClassPath(() => form.tiltakshaver.kontaktperson.telefonnummer), "ERROR", null);
            _validationResult.AddRule(skjema, "4373.3.5.12.4", null, "Mobilnummeret til tiltakshavers kontaktperson må kun inneholde tall og '+'.", Helpers.GetFullClassPath(() => form.tiltakshaver.kontaktperson.mobilnummer), "ERROR", null);
            _validationResult.AddRule(skjema, "4373.3.5.12.5", null, "E-postadressen til tiltakshavers kontaktperson bør fylles ut når tiltakshaver er en organisasjon.", Helpers.GetFullClassPath(() => form.tiltakshaver.kontaktperson.navn), "WARNING", null);
            // fraSluttbrukersystem
            _validationResult.AddRule(skjema, "4373.2.6", null, "Systemet må fylle ut samme navn som er brukt i registrering for Altinn API i 'fraSluttbrukersystem'.", Helpers.GetFullClassPath(() => form.metadata.fraSluttbrukersystem), "ERROR", null);

            // varsling
            _validationResult.AddRule(skjema, "4373.2.7", null, "Informasjon om varsling mangler for tiltaket.", Helpers.GetFullClassPath(() => form.varsling), "ERROR", null);
            _validationResult.AddRule(skjema, "4373.2.7.1", "2.12", "Du må svare på om tiltaket er fritatt fra nabovarsling.", Helpers.GetFullClassPath(() => form.varsling.fritattFraNabovarsling), "ERROR", null);
            _validationResult.AddRule(skjema, "4373.2.7.2", "2.3", "Når nabovarsling kreves for tiltaket, må underskjema 'Opplysninger gitt i nabovarsel' (Gjenpart nabovarsel) eller vedlegg 'Nabovarsel' legges ved.", Helpers.GetFullClassPath(() => form.varsling.fritattFraNabovarsling), "ERROR", "/Vedlegg");
            _validationResult.AddRule(skjema, "4373.2.7.3", "2.2", "Når nabovarsling kreves for tiltaket, må vedlegget 'Kvittering for nabovarsel' legges ved.", Helpers.GetFullClassPath(() => form.varsling.fritattFraNabovarsling), "ERROR", "/Vedlegg");
            _validationResult.AddRule(skjema, "4373.2.7.4", "2.6", "Når nabovarsel er påkrevd for tiltaket, må du svare på om det foreligger merknader.", Helpers.GetFullClassPath(() => form.varsling.foreliggerMerknader), "ERROR", null);
            _validationResult.AddRule(skjema, "4373.2.7.4.1", "2.6", "Du har valgt at det foreligger merknader fra nabovarsel. Da må antallet merknader fylles ut.", Helpers.GetFullClassPath(() => form.varsling.antallMerknader), "ERROR", Helpers.GetFullClassPath(() => form.varsling.foreliggerMerknader));
            _validationResult.AddRule(skjema, "4373.2.7.4.2", "2.7", "Du har valgt at det foreligger merknader fra nabovarsling. Da må du skrive din vurdering av merknadene i tekstboksen eller legge ved vedlegget 'Kommentar til nabomerknader'.", Helpers.GetFullClassPath(() => form.varsling.foreliggerMerknader), "ERROR", "/Vedlegg");
            _validationResult.AddRule(skjema, "4373.2.7.4.3", "2.7", "Du har valgt at det foreligger merknader fra nabovarsling. Da må vedlegget 'Nabomerknader' legges ved.", Helpers.GetFullClassPath(() => form.varsling.foreliggerMerknader), "ERROR", "/Vedlegg");

            // rammebetingelser.plan.gjeldendePlan
            _validationResult.AddRule(skjema, "4373.2.8.4", "3.1", "Informasjon om gjeldende plan kreves for valgte tiltakstyper, jfr. nasjonal sjekkliste punkt 3.1.", Helpers.GetFullClassPath(() => form.rammebetingelser.plan.gjeldendePlan), "ERROR", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.type));
            _validationResult.AddRule(skjema, "4373.2.8.4.1", "3.1", "Du må oppgi en plantype for gjeldende plan. Du kan sjekke gyldige plantyper på https://register.geonorge.no/byggesoknad/plantype", Helpers.GetFullClassPath(() => form.rammebetingelser.plan.gjeldendePlan.plantype.kodeverdi), "ERROR", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.type));
            _validationResult.AddRule(skjema, "4373.2.8.4.1.1", "3.1", "'{0}' er en ugyldig kodeverdi for plantype. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/byggesoknad/plantype", Helpers.GetFullClassPath(() => form.rammebetingelser.plan.gjeldendePlan.plantype.kodeverdi), "ERROR", null);
            _validationResult.AddRule(skjema, "4373.3.8.4.2", "3.1", "Når plantype er valgt, må kodebeskrivelse fylles ut. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/byggesoknad/plantype", Helpers.GetFullClassPath(() => form.rammebetingelser.plan.gjeldendePlan.plantype.kodebeskrivelse), "ERROR", Helpers.GetFullClassPath(() => form.rammebetingelser.plan.gjeldendePlan.plantype.kodeverdi));
            _validationResult.AddRule(skjema, "4373.3.8.4.2.1", "3.1", "Kodebeskrivelsen '{0}' stemmer ikke med den valgte kodeverdien for plantype. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/byggesoknad/plantype", Helpers.GetFullClassPath(() => form.rammebetingelser.plan.gjeldendePlan.plantype.kodebeskrivelse), "WARNING", Helpers.GetFullClassPath(() => form.rammebetingelser.plan.gjeldendePlan.plantype.kodeverdi));

            // rammebetingelser.plan.gjeldendePlan.beregningsregelGradAvUtnytting
            _validationResult.AddRule(skjema, "4373.2.8.4.3", "3.3", "Du må oppgi beregningsregel for grad av utnytting fra gjeldende plan for valgte tiltakstyper, jfr. nasjonal sjekkliste punkt 3.3. Du kan sjekke riktig beregningsregel på https://register.geonorge.no/byggesoknad/beregningsregelgradavutnytting", Helpers.GetFullClassPath(() => form.rammebetingelser.plan.gjeldendePlan.beregningsregelGradAvUtnytting), "ERROR", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.type));
            _validationResult.AddRule(skjema, "4373.2.8.4.3.1", "3.3", "'{0}' er en ugyldig kodeverdi for beregningsregel grad av utnytting. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/byggesoknad/beregningsregelgradavutnytting", Helpers.GetFullClassPath(() => form.rammebetingelser.plan.gjeldendePlan.beregningsregelGradAvUtnytting.kodeverdi), "ERROR", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.type));
            _validationResult.AddRule(skjema, "4373.2.8.4.3.2", "3.3", "Kodebeskrivelsen for beregningsregel grad av utnytting ('{0}') bør fylles ut. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/byggesoknad/beregningsregelgradavutnytting", Helpers.GetFullClassPath(() => form.rammebetingelser.plan.gjeldendePlan.beregningsregelGradAvUtnytting.kodebeskrivelse), "WARNING", Helpers.GetFullClassPath(() => form.rammebetingelser.plan.gjeldendePlan.beregningsregelGradAvUtnytting.kodeverdi));
            _validationResult.AddRule(skjema, "4373.2.8.4.3.3", "3.3", "Når beregningsregel for grad av utnytting er 'Annet', må du legge ved vedlegg 'Underlag for beregning av utnytting' (UnderlagUtnytting).", Helpers.GetFullClassPath(() => form.rammebetingelser.plan.gjeldendePlan.beregningsregelGradAvUtnytting.kodeverdi), "ERROR", "/Vedlegg");

            _validationResult.AddRule(skjema, "4373.2.8.5", "3.1", "Reguleringsformål bør fylles ut.", Helpers.GetFullClassPath(() => form.rammebetingelser.plan.gjeldendePlan.formaal), "WARNING", null);
            _validationResult.AddRule(skjema, "4373.2.8.6", "3.1", "Navnet på gjeldende plan bør fylles ut.", Helpers.GetFullClassPath(() => form.rammebetingelser.plan.gjeldendePlan.navn), "WARNING", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.type));

            // rammebetingelser.plassering
            _validationResult.AddRule(skjema, "4373.2.9", "14.7", "Det er valgt at strømførende linje/kabel eller nettstasjon/transformator kan være i konflikt med tiltaket. Da må vedlegget ‘Avklaring av plassering nær høyspentledning’ (AvklaringHoyspent) fra legges ved.", Helpers.GetFullClassPath(() => form.rammebetingelser.plassering.konfliktHoeyspentkraftlinje), "ERROR", "/Vedlegg");
            _validationResult.AddRule(skjema, "4373.2.9.1", "14.18", "Det er valgt at vann- og avløpsledninger kan være i konflikt med tiltaket. Da må vedlegget 'Avklaring av plassering nær VA-ledninger' (AvklaringVA) legges ved.", Helpers.GetFullClassPath(() => form.rammebetingelser.plassering.konfliktVannOgAvloep), "ERROR", "/Vedlegg");
            _validationResult.AddRule(skjema, "4373.3.9.1.1", "14.18", "Du bør svare på om vann- og avløpsledninger kan være i konflikt med tiltaket, jfr. nasjonal sjekkliste punkt 14.18.", Helpers.GetFullClassPath(() => form.rammebetingelser.plassering.konfliktVannOgAvloep), "WARNING", null);
            _validationResult.AddRule(skjema, "4373.3.9.2", null, "Du må svare på om tiltaket er bekreftet innenfor byggegrensen.", Helpers.GetFullClassPath(() => form.rammebetingelser.plassering.bekreftetInnenforByggegrense), "ERROR", null);
            _validationResult.AddRule(skjema, "4373.3.9.2.1", "1.30", "Du har svart at tiltaket ikke er bekreftet innenfor byggegrensen. Da må du fylle ut minste avstand til nabogrense, jfr. nasjonal sjekkliste punkt 1.30.", Helpers.GetFullClassPath(() => form.rammebetingelser.plassering.minsteAvstandNabogrense), "ERROR", Helpers.GetFullClassPath(() => form.rammebetingelser.plassering.bekreftetInnenforByggegrense));
            _validationResult.AddRule(skjema, "4373.3.9.2.2", "1.32", "Du har svart at tiltaket ikke er bekreftet innenfor byggegrensen. Da må du fylle ut minste avstand til midten av vei, jfr. nasjonal sjekkliste punkt 1.32.", Helpers.GetFullClassPath(() => form.rammebetingelser.plassering.minsteAvstandTilMidtenAvVei), "ERROR", Helpers.GetFullClassPath(() => form.rammebetingelser.plassering.bekreftetInnenforByggegrense));
            _validationResult.AddRule(skjema, "4373.3.9.2.3", "1.31", "Du har svart at tiltaket ikke er bekreftet innenfor byggegrensen. Da må du fylle ut minste avstand til annen bygning, jfr. nasjonal sjekkliste punkt 1.31.", Helpers.GetFullClassPath(() => form.rammebetingelser.plassering.minsteAvstandTilAnnenBygning), "ERROR", Helpers.GetFullClassPath(() => form.rammebetingelser.plassering.bekreftetInnenforByggegrense));
            _validationResult.AddRule(skjema, "4373.3.9.3", "14.17", "Du bør svare på om det er strømførende linje/kabel eller nettstasjon/transformator i, over eller i nærheten av tiltaket, jfr. nasjonal sjekkliste punkt 14.17.", Helpers.GetFullClassPath(() => form.rammebetingelser.plassering.konfliktHoeyspentkraftlinje), "WARNING", null);

            // soekesOmDispensasjon
            _validationResult.AddRule(skjema, "4373.2.10", null, "Det er valgt at det skal søkes om dispensasjon. Da må du velge en dispensasjonstype.", Helpers.GetFullClassPath(() => form.dispensasjon[0].dispensasjonstype), "ERROR", null);
            _validationResult.AddRule(skjema, "4373.2.10.1", null, "Det er valgt at det skal søkes om dispensasjon. Da må du legge inn beskrivelse og begrunnelse for dispensasjonen i tekstfeltene, eller i vedlegget 'Dispensasjonssøknad'.", Helpers.GetFullClassPath(() => form.dispensasjon[0].beskrivelse), "ERROR", "/Vedlegg");
            _validationResult.AddRule(skjema, "4373.2.10.2", null, "Det er valgt at det skal søkes om dispensasjon. Da må du også skrive en begrunnelse for dispensasjonen.", Helpers.GetFullClassPath(() => form.dispensasjon[0].begrunnelse), "ERROR", null);
            _validationResult.AddRule(skjema, "4373.2.10.3", null, "'{0}' er en ugyldig kodeverdi for dispensasjonstype. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/byggesoknad/dispensasjonstype", Helpers.GetFullClassPath(() => form.dispensasjon[0].beskrivelse), "ERROR", null);

            // rammebetingelser.kravTilByggegrunn
            _validationResult.AddRule(skjema, "4373.3.11.1", "10.1", "Du bør svare på om byggverket skal plasseres i et område med fare for natur- og miljøforhold, jfr. nasjonal sjekkliste punkt 10.1.", Helpers.GetFullClassPath(() => form.rammebetingelser.kravTilByggegrunn.miljoeforhold), "WARNING", null);
            _validationResult.AddRule(skjema, "4373.2.11.1.1", "10.1", "Det er valgt at byggverket skal plasseres i et område med fare for natur- og miljøforhold. Da skal vedlegget 'Redegjørelse andre natur- og miljøforhold' legges ved.", Helpers.GetFullClassPath(() => form.rammebetingelser.kravTilByggegrunn.miljoeforhold), "ERROR", "/Vedlegg");
            _validationResult.AddRule(skjema, "4373.3.11.2", "11.8", "Du bør svare på om byggverket skal plasseres i et område med fare for skred, jfr. nasjonal sjekkliste punkt 11.8.", Helpers.GetFullClassPath(() => form.rammebetingelser.kravTilByggegrunn.skredutsattOmraade), "WARNING", null);
            _validationResult.AddRule(skjema, "4373.2.11.2.1", "11.8", "Det er valgt at byggverket skal plasseres i et område med fare for skred. Da skal vedlegget ‘Redegjørelse skred og flomfare’ legges ved, jfr. nasjonal sjekkliste punkt 11.8.", Helpers.GetFullClassPath(() => form.rammebetingelser.kravTilByggegrunn.skredutsattOmraade), "ERROR", "/Vedlegg");
            _validationResult.AddRule(skjema, "4373.3.11.3", "11.1", "Du bør svare på om byggverket skal plasseres i et område med fare for flom eller stormflo, jfr. nasjonal sjekkliste punkt 11.1.", Helpers.GetFullClassPath(() => form.rammebetingelser.kravTilByggegrunn.flomutsattOmraade), "WARNING", null);
            _validationResult.AddRule(skjema, "4373.2.11.3.1", "11.1", "Det er valgt at byggverket skal plasseres i et område med fare for flom eller stormflo. Da skal vedlegget 'Redegjørelse skred og flomfare' legges ved.", Helpers.GetFullClassPath(() => form.rammebetingelser.kravTilByggegrunn.flomutsattOmraade), "ERROR", "/Vedlegg");

            //rammebetingelser.adkomst
            _validationResult.AddRule(skjema, "4373.2.12", "7.1", "Informasjon om adkomst kreves for valgte tiltakstyper, jfr. nasjonal sjekkliste punkt 7.1.", Helpers.GetFullClassPath(() => form.rammebetingelser.adkomst), "ERROR", null);
            _validationResult.AddRule(skjema, "4373.2.12.1", "7.1", "Du må svare på om tiltaket medfører ny eller endret adkomst, jfr. nasjonal sjekkliste punkt 7.1.", Helpers.GetFullClassPath(() => form.rammebetingelser.adkomst.nyeEndretAdkomst), "ERROR", null);
            _validationResult.AddRule(skjema, "4373.2.12.2", null, "Det er valgt at tiltaket gir ny eller endret adkomst. Da bør vedlegget ‘Avkjørselsplan’ legges ved.", Helpers.GetFullClassPath(() => form.rammebetingelser.adkomst.nyeEndretAdkomst), "WARNING", "/Vedlegg");
            _validationResult.AddRule(skjema, "4373.2.12.3", null, "'{0}' er en ugyldig kodeverdi for vegtype. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/byggesoknad/vegtype", Helpers.GetFullClassPath(() => form.rammebetingelser.adkomst.vegrett[0].vegtype.kodeverdi), "ERROR", null);
            _validationResult.AddRule(skjema, "4373.2.12.4", null, "Når vegtypen er '{0}', må du svare på om avkjøringstillatelse er gitt.", Helpers.GetFullClassPath(() => form.rammebetingelser.adkomst.vegrett[0].erTillatelseGitt), "ERROR", null);

            //rammebetingelser.generelleVilkaar.oppfyllesVilkaarFor3Ukersfrist
            _validationResult.AddRule(skjema, "4373.2.13", null, "Vilkår for 3 ukers saksbehandling er ikke oppfylt, jfr. § 21-7 tredje ledd. Søknaden må være uten ytterligere behov for tillatelse.", Helpers.GetFullClassPath(() => form.rammebetingelser.generelleVilkaar.behovForTillatelse), "ERROR", Helpers.GetFullClassPath(() => form.rammebetingelser.generelleVilkaar.oppfyllesVilkaarFor3Ukersfrist));
            _validationResult.AddRule(skjema, "4373.2.13.1", null, "Søknaden må være uten dispensasjoner for å oppfylle vilkårene for 3 ukers saksbehandling.", Helpers.GetFullClassPath(() => form.dispensasjon), "ERROR", Helpers.GetFullClassPath(() => form.rammebetingelser.generelleVilkaar.oppfyllesVilkaarFor3Ukersfrist));

            //Vedlegg validering
            _validationResult.AddRule(skjema, "4373.2.14", "1.80", "Minst én tegning med en av vedleggstypene  ‘TegningEksisterendeSnitt’, ‘TegningNyttSnitt’, ‘TegningEksisterendePlan’, ‘TegningNyPlan’, ‘TegningEksisterendeFasade’ eller ‘TegningNyFasade', må være vedlagt søknaden.", Helpers.GetFullClassPath(() => form), "ERROR", "/Vedlegg");
            _validationResult.AddRule(skjema, "4373.2.14.1", "1.79", "Vedleggene ‘TegningEksisterendePlan’ og ‘TegningNyPlan’ bør være vedlagt søknaden for valgte tiltakstyper, jfr. nasjonal sjekkliste punkt 1.79.", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.type.type.kodeverdi), "WARNING", "/Vedlegg");
            _validationResult.AddRule(skjema, "4373.2.14.2", "1.80", "Vedleggene ‘TegningEksisterendeSnitt’ og ‘TegningNyttSnitt’ bør være vedlagt søknaden for valgte tiltakstyper, jfr. nasjonal sjekkliste punkt 1.80.", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.type.type.kodeverdi), "WARNING", "/Vedlegg");
            _validationResult.AddRule(skjema, "4373.2.14.3", "1.81", "Vedleggene ‘TegningEksisterendeFasade’ og ‘TegningNyFasade’ bør være vedlagt søknaden for valgte tiltakstyper, jfr. nasjonal sjekkliste punkt 1.81.", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.type.type.kodeverdi), "WARNING", "/Vedlegg");
            _validationResult.AddRule(skjema, "4373.2.14.4", "1.73", "Vedlegg ‘Situasjonsplan’ mangler for valgte tiltakstyper i søknad, jfr. nasjonal sjekkliste punkt 1.73.", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.type.type.kodeverdi), "ERROR", "/Vedlegg");
            //_validationResult.AddRule(_skjema, "4373.3.14.5", null, "Vedlegg UttalelseKulturminnemyndighet mangler når du velge 'eroererTidligere1850'.", Helpers.GetFullClassPath(() => form.rammebetingelser.generelleVilkaar.beroererTidligere1850), "ERROR", "/Vedlegg");

            //rammebetingelser.vannforsyning
            _validationResult.AddRule(skjema, "4373.2.15", "9.1", "Informasjon om vannforsyning kreves for valgte tiltakstyper, jfr. nasjonal sjekkliste punkt 9.1.", Helpers.GetFullClassPath(() => form.rammebetingelser.vannforsyning.tilknytningstype), "ERROR", null);
            _validationResult.AddRule(skjema, "4373.2.15.1", null, "'{0}' er en ugyldig kodeverdi for vanntilknytning. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/byggesoknad/vanntilknytning", Helpers.GetFullClassPath(() => form.rammebetingelser.vannforsyning.tilknytningstype[0].kodeverdi), "ERROR", null);
            _validationResult.AddRule(skjema, "4373.2.15.2", null, "Når 'annen privat vannforsyning' er angitt, må beskrivelse fylles ut.", Helpers.GetFullClassPath(() => form.rammebetingelser.vannforsyning.beskrivelse), "ERROR", Helpers.GetFullClassPath(() => form.rammebetingelser.vannforsyning.tilknytningstype[0].kodeverdi));
            _validationResult.AddRule(skjema, "4373.2.15.3", "9.6", "Når vannforsyning krysser annens grunn, må du svare på om det foreligger tinglyst erklæring, jfr. nasjonal sjekkliste punkt 9.6.", Helpers.GetFullClassPath(() => form.rammebetingelser.vannforsyning.tinglystErklaering), "ERROR", Helpers.GetFullClassPath(() => form.rammebetingelser.vannforsyning.krysserVannforsyningAnnensGrunn));
            //rammebetingelser.avloep
            _validationResult.AddRule(skjema, "4373.2.16", "8.1", "Informasjon om avløp kreves for valgte tiltakstyper, jfr. nasjonal sjekkliste punkt 8.1.", Helpers.GetFullClassPath(() => form.rammebetingelser.avloep.tilknytningstype), "ERROR", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.type.type.kodeverdi));
            _validationResult.AddRule(skjema, "4373.2.16.1", null, "'{0}' er en ugyldig kodeverdi for avløpstilknytning. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/byggesoknad/avlopstilknytning", Helpers.GetFullClassPath(() => form.rammebetingelser.avloep.tilknytningstype.kodeverdi), "ERROR", null);
            _validationResult.AddRule(skjema, "4373.2.16.2", null, "Når avløpstilknytning er valgt må kodebeskrivelse angis. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/byggesoknad/avlopstilknytning", Helpers.GetFullClassPath(() => form.rammebetingelser.avloep.tilknytningstype.kodebeskrivelse), "ERROR", null);
            _validationResult.AddRule(skjema, "4373.2.16.3", null, "Når 'privat avløpsanlegg' er valgt, må du fylle ut om det skal installeres vannklosett.", Helpers.GetFullClassPath(() => form.rammebetingelser.avloep.installereVannklosett), "ERROR", null);
            _validationResult.AddRule(skjema, "4373.2.16.4", null, "Når 'privat avløpsanlegg' er valgt, må du fylle ut om det foreligger utslippstillatelse.", Helpers.GetFullClassPath(() => form.rammebetingelser.avloep.utslippstillatelse), "ERROR", null);
            _validationResult.AddRule(skjema, "4373.3.16.5", "8.7", "Du må svare på om takvann/overvann blir ført til terreng eller overvannsledning, jfr. nasjonal sjekkliste punkt 8.7.", Helpers.GetFullClassPath(() => form.rammebetingelser.avloep.overvannTerreng), "ERROR", null);

            //generelleVilkaar
            _validationResult.AddRule(skjema, "4373.3.17.1", "5.13", "Du bør svare på om tiltaket berører byggverk oppført før 1850, jfr. nasjonal sjekkliste punkt 5.13.", Helpers.GetFullClassPath(() => form.rammebetingelser.generelleVilkaar.beroererTidligere1850), "WARNING", null);
            _validationResult.AddRule(skjema, "4373.3.17.2", "1.1", "Du må svare på om søknaden og alle relevante dokumenter er skrevet eller oversatt til norsk, svensk eller dansk.", Helpers.GetFullClassPath(() => form.rammebetingelser.generelleVilkaar.norskSvenskDansk), "ERROR", null);

            //rammebetingelser.arealdisponering
            _validationResult.AddRule(skjema, "4373.3.18.1.1", "3.3", "Informasjon om arealdisponering kreves for valgte tiltakstyper, jfr. nasjonal sjekkliste punkt 3.3.", Helpers.GetFullClassPath(() => form.rammebetingelser.arealdisponering), "ERROR", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak.type.type.kodeverdi));
            _validationResult.AddRule(skjema, "4373.3.18.1", "3.3", "Når beregningsregel for grad av utnytting er '{0}' kreves utfylte verdier for areal eksisterende bebyggelse, areal ny bebyggelse, areal bebyggelse som rives, parkeringsareal terreng og beregnet grad av utnytting.", Helpers.GetFullClassPath(() => form.rammebetingelser.arealdisponering), "ERROR", Helpers.GetFullClassPath(() => form.rammebetingelser.plan.gjeldendePlan.beregningsregelGradAvUtnytting.kodeverdi));
            _validationResult.AddRule(skjema, "4373.3.18.2", null, "Utregning av utnyttelsesgrad kan ikke ha negative verdier for 'arealBebyggelseEksisterende'.", Helpers.GetFullClassPath(() => form.rammebetingelser.arealdisponering.arealBebyggelseEksisterende), "WARNING", null);
            _validationResult.AddRule(skjema, "4373.3.18.3", null, "Utregning av utnyttelsesgrad kan ikke ha negative verdier for 'arealBebyggelseNytt'", Helpers.GetFullClassPath(() => form.rammebetingelser.arealdisponering.arealBebyggelseNytt), "WARNING", null);
            _validationResult.AddRule(skjema, "4373.3.18.4", null, "Utregning av utnyttelsesgrad kan ikke ha negative verdier for 'arealBebyggelseSomSkalRives'.", Helpers.GetFullClassPath(() => form.rammebetingelser.arealdisponering.arealBebyggelseSomSkalRives), "WARNING", null);
            _validationResult.AddRule(skjema, "4373.3.18.5", null, "Utregning av utnyttelsesgrad kan ikke ha negative verdier for 'parkeringsarealTerreng'.", Helpers.GetFullClassPath(() => form.rammebetingelser.arealdisponering.parkeringsarealTerreng), "WARNING", null);
            _validationResult.AddRule(skjema, "4373.3.18.6", null, "Utregning av utnyttelsesgrad kan ikke ha negative verdier for 'beregnetGradAvUtnytting'.", Helpers.GetFullClassPath(() => form.rammebetingelser.arealdisponering.beregnetGradAvUtnytting), "WARNING", null);
            _validationResult.AddRule(skjema, "4373.3.18.7", null, "Utregningen av utnyttelsesgrad er ikke riktig for '{0}'. Utfylt grad av utnytting er '{1}' og beregnet grad av utnytting er '{2}'. Utregningen er: 'arealBebyggelseEksisterende' - 'arealBebyggelseSomSkalRives' + 'arealBebyggelseNytt' + 'parkeringsarealTerreng'.", Helpers.GetFullClassPath(() => form.rammebetingelser.arealdisponering), "ERROR", Helpers.GetFullClassPath(() => form.rammebetingelser.plan.gjeldendePlan.beregningsregelGradAvUtnytting.kodeverdi));
            _validationResult.AddRule(skjema, "4373.3.18.8", null, "Når beregningsregel for grad av utnytting er angitt i prosent, må tomtearealet beregnes.", Helpers.GetFullClassPath(() => form.rammebetingelser.arealdisponering.tomtearealBeregnet), "ERROR", Helpers.GetFullClassPath(() => form.rammebetingelser.plan.gjeldendePlan.beregningsregelGradAvUtnytting.kodeverdi));
            _validationResult.AddRule(skjema, "4373.3.18.9", null, "Utregning av utnyttelsesgrad kan ikke ha 0 for 'tomtearealBeregnet'.", Helpers.GetFullClassPath(() => form.rammebetingelser.arealdisponering.tomtearealBeregnet), "WARNING", null);
            _validationResult.AddRule(skjema, "4373.3.18.10", null, "Utregning av utnyttelsesgrad kan ikke ha negative verdier for 'tomtearealBeregnet'.", Helpers.GetFullClassPath(() => form.rammebetingelser.arealdisponering.tomtearealBeregnet), "WARNING", null);
            _validationResult.AddRule(skjema, "4373.3.18.11", null, "Utregningen av utnyttelsesgrad er ikke riktig for '{0}'. Utfylt grad av utnytting er '{1}' og beregnet grad av utnytting er '{2}'. Utregning  av persent (arealBebyggelseEksisterende - arealBebyggelseSomSkalRives + arealBebyggelseNytt + parkeringsarealTerreng)/tomtearealBeregnet.", Helpers.GetFullClassPath(() => form.rammebetingelser.arealdisponering), "ERROR", Helpers.GetFullClassPath(() => form.rammebetingelser.plan.gjeldendePlan.beregningsregelGradAvUtnytting.kodeverdi));
        }

        internal ValidationResult GetResult() => _validationResult;

        public ValidationResult Validate(TiltakUtenAnsvarsrettType form, List<string> formSetElements)
        {
            MinstEtTiltak(form, formSetElements);
            //Add to validationResults all "tiltakstypes"
            _validationResult.tiltakstyperISoeknad = _tiltakstyperISoeknad;

            Parallel.Invoke(
                () => { EiendomByggestedValidation(form); },
                () => { TiltakshaverValidation(form); },
                () => { MetadataValidering(form); },
                () => { VarslingValidering(form, formSetElements); },
                () => { GjeldendePlanValidering(form, formSetElements); },
                () => { PlasseringValidering(form, formSetElements); },
                () => { DispensasjonValidering(form, formSetElements); },
                () => { KravTilByggegrunnValidering(form, formSetElements); },
                () => { AdkomstValidering(form, formSetElements); },
                () => { OppfyllesVilkaarFor3UkersfristValidering(form, formSetElements); },
                () => { VedleggValidaring(form, formSetElements); },
                () => { VannforsyningValidering(form); },
                () => { GenerelleVilkaarValidering(form); },
                () => { ArealdisponeringValidering(form); },
                () => { AvloepValidering(form); }
            );

            GetChecklistAnswer(form);

            return _validationResult;
        }

        internal void GenerelleVilkaarValidering(TiltakUtenAnsvarsrettType form)
        {
            if (Helpers.ObjectIsNullOrEmpty(form.rammebetingelser?.generelleVilkaar) || !form.rammebetingelser.generelleVilkaar.beroererTidligere1850.HasValue)
                _validationResult.AddMessage("4373.3.17.1", null);

            if (Helpers.ObjectIsNullOrEmpty(form.rammebetingelser?.generelleVilkaar) || !form.rammebetingelser.generelleVilkaar.norskSvenskDansk.HasValue)
                _validationResult.AddMessage("4373.3.17.2", null);
        }

        internal void MinstEtTiltak(TiltakUtenAnsvarsrettType form, List<string> formSetElements)
        {
            if (form.beskrivelseAvTiltak != null)
            {
                if (Helpers.ObjectIsNullOrEmpty(form.beskrivelseAvTiltak))
                {
                    _validationResult.AddMessage("4373.2.3", null);
                }
                else
                {
                    if (Helpers.ObjectIsNullOrEmpty(form.beskrivelseAvTiltak.type?.type))
                    {
                        _validationResult.AddMessage("4373.2.3.1", null);
                    }
                    else
                    {
                        var tiltakstype = form.beskrivelseAvTiltak.type.type;
                        if (!_codeListService.IsCodeValidInCodeList("tiltakstyperutenansvarsrett", tiltakstype.kodeverdi))
                        {
                            _validationResult.AddMessage("4373.2.3.2", new[] { tiltakstype.kodeverdi });
                        }
                        else
                        {
                            //Add tiltak type to list
                            _tiltakstyperISoeknad.Add(tiltakstype.kodeverdi);

                            if (String.IsNullOrEmpty(tiltakstype.kodebeskrivelse))
                            {
                                _validationResult.AddMessage("4373.2.3.3", null);
                            }
                            else
                            {
                                if (!_codeListService.IsCodelistLabelValid("tiltakstyperutenansvarsrett", tiltakstype.kodeverdi, tiltakstype.kodebeskrivelse))
                                {
                                    _validationResult.AddMessage("4373.2.3.3.1", new[] { tiltakstype.kodebeskrivelse });
                                }
                                else
                                {
                                    if (tiltakstype.kodeverdi == "annet" && String.IsNullOrEmpty(form.beskrivelseAvTiltak.type.beskrivelse))
                                    {
                                        _validationResult.AddMessage("4373.2.3.7", null);
                                    }
                                }
                            }
                        }

                        if (!formSetElements.Any(f => f.Equals("Folgebrev")))
                            if (string.IsNullOrEmpty(form.beskrivelseAvTiltak?.foelgebrev))
                            {
                                _validationResult.AddMessage("4373.2.3.6", null);
                            }

                        if (!Helpers.ObjectIsNullOrEmpty(form.beskrivelseAvTiltak.bruk?.tiltaksformaal))
                        {
                            //tiltaksformaal
                            foreach (var tiltaksformaal in form.beskrivelseAvTiltak.bruk.tiltaksformaal)
                            {
                                if (!String.IsNullOrEmpty(tiltaksformaal.kodeverdi))
                                {
                                    if (!_codeListService.IsCodeValidInCodeList("tiltaksformal", tiltaksformaal.kodeverdi))
                                    {
                                        _validationResult.AddMessage("4373.2.3.4", new[] { tiltaksformaal.kodeverdi });
                                    }
                                    else
                                    {
                                        if (string.IsNullOrEmpty(tiltaksformaal.kodebeskrivelse))
                                        {
                                            _validationResult.AddMessage("4373.3.3.4.1", new[] { tiltaksformaal.kodeverdi });
                                        }
                                        else
                                        {
                                            if (!_codeListService.IsCodelistLabelValid("tiltaksformal", tiltaksformaal.kodeverdi, tiltaksformaal.kodebeskrivelse))
                                                _validationResult.AddMessage("4373.3.3.4.2", new[] { tiltaksformaal.kodeverdi });
                                        }

                                        if (tiltaksformaal.kodeverdi == "Annet")
                                        {
                                            if (String.IsNullOrEmpty(form.beskrivelseAvTiltak.bruk.beskrivPlanlagtFormaal))
                                            {
                                                _validationResult.AddMessage("4373.2.3.5", null);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        internal void EiendomByggestedValidation(TiltakUtenAnsvarsrettType form)
        {
            if (Helpers.ObjectIsNullOrEmpty(form.eiendomByggested))
            {
                _validationResult.AddMessage("4373.2.4", null);
            }
            else
            {
                foreach (EiendomType eiendomType in form.eiendomByggested)
                {
                    if (eiendomType.eiendomsidentifikasjon != null)
                    {
                        var kommunenummer = eiendomType.eiendomsidentifikasjon.kommunenummer;
                        var kommunenummerValideringStatus = _municipalityValidator.Validate_kommunenummerStatus(kommunenummer);
                        if (kommunenummerValideringStatus.Status != MunicipalityValidation.Ok)
                        {
                            switch (kommunenummerValideringStatus.Status)
                            {
                                case MunicipalityValidation.Empty:
                                    _validationResult.AddMessage("4373.2.4.1", null);
                                    break;

                                case MunicipalityValidation.Invalid:
                                    _validationResult.AddMessage("4373.2.4.2", new[] { kommunenummer });
                                    break;

                                case MunicipalityValidation.Expired:
                                    _validationResult.AddMessage("4373.2.4.3", new[] { kommunenummer, kommunenummerValideringStatus.Status.ToString() });
                                    break;
                            }
                        }
                        else
                        {
                            var gaardsnummer = eiendomType.eiendomsidentifikasjon.gaardsnummer;
                            var bruksnummer = eiendomType.eiendomsidentifikasjon.bruksnummer;
                            if (string.IsNullOrEmpty(gaardsnummer) || string.IsNullOrEmpty(bruksnummer))
                            {
                                if (string.IsNullOrEmpty(gaardsnummer))
                                    _validationResult.AddMessage("4373.2.4.4", null);

                                if (string.IsNullOrEmpty(bruksnummer))
                                    _validationResult.AddMessage("4373.2.4.4.1", null);
                            }
                            else
                            {
                                int gaardsnumerInt = 0;
                                int bruksnummerInt = 0;

                                if (int.TryParse(gaardsnummer, out gaardsnumerInt) && int.TryParse(bruksnummer, out bruksnummerInt))
                                {
                                    int festenummerInt = 0;
                                    int seksjonsnummerInt = 0;
                                    int.TryParse(eiendomType.eiendomsidentifikasjon.festenummer, out festenummerInt);
                                    int.TryParse(eiendomType.eiendomsidentifikasjon.seksjonsnummer, out seksjonsnummerInt);

                                    if (gaardsnumerInt < 0 || bruksnummerInt < 0)
                                    {
                                        if (gaardsnumerInt < 0)
                                            _validationResult.AddMessage("4373.3.4.5", new[] { eiendomType.eiendomsidentifikasjon.gaardsnummer });

                                        if (bruksnummerInt < 0)
                                            _validationResult.AddMessage("4373.3.4.6", new[] { eiendomType.eiendomsidentifikasjon.bruksnummer });
                                    }
                                    else
                                    {
                                        //## MATRIKKEL
                                        var matrikkelnrExist = _matrikkelService.MatrikkelnrExist(kommunenummer, gaardsnumerInt, bruksnummerInt, festenummerInt, seksjonsnummerInt);
                                        if (matrikkelnrExist.HasValue)
                                        {
                                            if (!matrikkelnrExist.Value)
                                            {
                                                _validationResult.AddMessage("4373.2.4.12", new[]
                                                {
                                                    kommunenummer,
                                                    eiendomType.eiendomsidentifikasjon.gaardsnummer,
                                                    eiendomType.eiendomsidentifikasjon.bruksnummer,
                                                    eiendomType.eiendomsidentifikasjon.festenummer,
                                                    eiendomType.eiendomsidentifikasjon.seksjonsnummer,
                                                });
                                            }
                                        }
                                        else
                                        {
                                            _validationResult.AddMessage("4373.2.4.13", new[]
                                            {
                                                kommunenummer,
                                                eiendomType.eiendomsidentifikasjon.gaardsnummer,
                                                eiendomType.eiendomsidentifikasjon.bruksnummer,
                                                eiendomType.eiendomsidentifikasjon.festenummer,
                                                eiendomType.eiendomsidentifikasjon.seksjonsnummer,
                                            });
                                        }

                                        if (!String.IsNullOrEmpty(eiendomType.bygningsnummer))
                                        {
                                            long bygningsnrLong = 0;
                                            if (!long.TryParse(eiendomType.bygningsnummer, out bygningsnrLong))
                                            {
                                                _validationResult.AddMessage("4373.2.4.7", new[] { eiendomType.eiendomsidentifikasjon.gaardsnummer, eiendomType.eiendomsidentifikasjon.bruksnummer });
                                            }
                                            else
                                            {
                                                if (bygningsnrLong <= 0)
                                                {
                                                    _validationResult.AddMessage("4373.2.4.8", new[] { eiendomType.eiendomsidentifikasjon.gaardsnummer, eiendomType.eiendomsidentifikasjon.bruksnummer });
                                                }
                                                else
                                                {
                                                    ////## MATRIKKEL
                                                    var isBygningValid = _matrikkelService.IsBygningOnMatrikkelnr(bygningsnrLong, kommunenummer, gaardsnumerInt, bruksnummerInt, festenummerInt, seksjonsnummerInt);
                                                    if (!isBygningValid.GetValueOrDefault())
                                                    {
                                                        _validationResult.AddMessage("4373.2.4.14", new[]
                                                        {
                                                            kommunenummer,
                                                            eiendomType.eiendomsidentifikasjon.gaardsnummer,
                                                            eiendomType.eiendomsidentifikasjon.bruksnummer,
                                                            eiendomType.eiendomsidentifikasjon.festenummer,
                                                            eiendomType.eiendomsidentifikasjon.seksjonsnummer,
                                                        });
                                                    }
                                                }
                                            }
                                        }

                                        if (Helpers.ObjectIsNullOrEmpty(eiendomType.adresse))
                                        {
                                            _validationResult.AddMessage("4373.2.4.9", null);
                                        }
                                        else
                                        {
                                            if (string.IsNullOrEmpty(eiendomType.adresse?.adresselinje1))
                                            {
                                                _validationResult.AddMessage("4373.2.4.9.1", null);
                                            }
                                            else
                                            {
                                                if (String.IsNullOrEmpty(eiendomType.adresse.gatenavn) || String.IsNullOrEmpty(eiendomType.adresse.husnr))
                                                {
                                                    _validationResult.AddMessage("4373.2.4.9.2", null);
                                                }
                                                else
                                                {
                                                    //## MATRIKKEL
                                                    var isAdresseValid = _matrikkelService.IsVegadresseOnMatrikkelnr(eiendomType.adresse.gatenavn, eiendomType.adresse.husnr, eiendomType.adresse.bokstav, kommunenummer, gaardsnumerInt, bruksnummerInt, festenummerInt, seksjonsnummerInt);
                                                    if (isAdresseValid == null)
                                                    {
                                                        _validationResult.AddMessage("4373.2.4.15", null);
                                                    }
                                                    else if (!isAdresseValid.GetValueOrDefault())
                                                    {
                                                        _validationResult.AddMessage("4373.2.4.15.1", null);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if (String.IsNullOrEmpty(eiendomType.kommunenavn))
                    {
                        _validationResult.AddMessage("4373.2.4.10", null);
                    }

                    if (!String.IsNullOrEmpty(eiendomType.bolignummer))
                    {
                        if (GeneralValidations.Validate_bruksenhetsnummer(eiendomType.bolignummer).Status == ValidationStatus.Fail)
                        {
                            _validationResult.AddMessage("4373.2.4.11", null);
                        }
                    }
                }
            }
        }

        internal void TiltakshaverValidation(TiltakUtenAnsvarsrettType form)
        {
            using (var logger = new PerfTimerLogger())
            {
                if (Helpers.ObjectIsNullOrEmpty(form.tiltakshaver?.partstype))
                {
                    _validationResult.AddMessage("4373.2.5.1", null);
                }
                else
                {
                    if (Helpers.ObjectIsNullOrEmpty(form.tiltakshaver?.partstype?.kodeverdi))
                    {
                        _validationResult.AddMessage("4373.2.5.2", null);
                    }
                    else
                    {
                        if (!_codeListService.IsCodelistValid("Partstype", form.tiltakshaver.partstype?.kodeverdi))
                        {
                            _validationResult.AddMessage("4373.2.5.3", new[] { form.tiltakshaver.partstype?.kodeverdi });
                        }
                        else
                        {
                            if (form.tiltakshaver.partstype?.kodeverdi == "Privatperson")
                            {
                                var foedselsnummerValidation = GeneralValidations.Validate_foedselsnummer(form.tiltakshaver.foedselsnummer);
                                switch (foedselsnummerValidation)
                                {
                                    case GeneralValidations.FoedselnumerValidation.Empty:
                                        _validationResult.AddMessage("4373.2.5.4", null);
                                        break;

                                    case GeneralValidations.FoedselnumerValidation.InvalidEncryption:
                                        _validationResult.AddMessage("4373.2.5.4.1", null);
                                        break;

                                    case GeneralValidations.FoedselnumerValidation.Invalid:
                                        _validationResult.AddMessage("4373.2.5.4.2", null);
                                        break;

                                    case GeneralValidations.FoedselnumerValidation.InvalidDigitsControl:
                                        _validationResult.AddMessage("4373.2.5.4.3", null);
                                        break;
                                }
                            }
                            else
                            {
                                var organisasjonsnummerValidation = GeneralValidations.Validate_OrgnummerEnum(form.tiltakshaver.organisasjonsnummer);
                                switch (organisasjonsnummerValidation)
                                {
                                    case GeneralValidations.OrganisasjonsnummerValidation.Empty:
                                        _validationResult.AddMessage("4373.2.5.5", null);
                                        break;

                                    case GeneralValidations.OrganisasjonsnummerValidation.InvalidDigitsControl:
                                        _validationResult.AddMessage("4373.2.5.5.1", new[] { form.tiltakshaver?.organisasjonsnummer });
                                        break;

                                    case GeneralValidations.OrganisasjonsnummerValidation.Invalid:
                                        _validationResult.AddMessage("4373.2.5.5.2", new[] { form.tiltakshaver?.organisasjonsnummer });
                                        break;
                                }

                                if (Helpers.ObjectIsNullOrEmpty(form.tiltakshaver.kontaktperson))
                                {
                                    _validationResult.AddMessage("4373.3.5.12", null);
                                }
                                else
                                {
                                    if (string.IsNullOrEmpty(form.tiltakshaver.kontaktperson.navn))
                                        _validationResult.AddMessage("4373.3.5.12.1", null);
                                    if (string.IsNullOrEmpty(form.tiltakshaver.kontaktperson.telefonnummer) && string.IsNullOrEmpty(form.tiltakshaver.kontaktperson.mobilnummer))
                                    {
                                        _validationResult.AddMessage("4373.3.5.12.2", null);
                                    }
                                    else
                                    {
                                        if (!string.IsNullOrEmpty(form.tiltakshaver.kontaktperson.telefonnummer))
                                        {
                                            var telefonNumber = form.tiltakshaver.kontaktperson.telefonnummer;
                                            var isValidTelefonNumber = telefonNumber.All(c => "+0123456789".Contains(c));
                                            if (!isValidTelefonNumber)
                                            {
                                                _validationResult.AddMessage("4373.3.5.12.3", null);
                                            }
                                        }

                                        if (!string.IsNullOrEmpty(form.tiltakshaver.kontaktperson.mobilnummer))
                                        {
                                            var mobilNummer = form.tiltakshaver.kontaktperson.mobilnummer;
                                            var isValidmobilnummer = mobilNummer.All(c => "+0123456789".Contains(c));
                                            if (!isValidmobilnummer)
                                            {
                                                _validationResult.AddMessage("4373.3.5.12.4", null);
                                            }
                                        }
                                    }

                                    if (string.IsNullOrEmpty(form.tiltakshaver.kontaktperson.epost))
                                        _validationResult.AddMessage("4373.3.5.12.5", null);
                                }
                            }

                            if (Helpers.ObjectIsNullOrEmpty(form.tiltakshaver?.adresse))
                            {
                                _validationResult.AddMessage("4373.2.5.6", null);
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(form.tiltakshaver.adresse.adresselinje1))
                                {
                                    _validationResult.AddMessage("4373.2.5.6.1", null);
                                }
                                else
                                {
                                    if (!CountryCodeHandler.IsCountryNorway(form.tiltakshaver.adresse.landkode))
                                    {
                                        if (!CountryCodeHandler.VerifyCountryCode(form.tiltakshaver.adresse.landkode))
                                        {
                                            _validationResult.AddMessage("4373.2.5.6.2", null);
                                        }
                                    }
                                    else
                                    {
                                        var postNr = form.tiltakshaver.adresse.postnr;
                                        var landkode = form.tiltakshaver.adresse.landkode;

                                        if (string.IsNullOrEmpty(form.tiltakshaver.adresse.postnr))
                                        {
                                            _validationResult.AddMessage("4373.2.5.6.3", null);
                                        }
                                        else
                                        {
                                            if (!GeneralValidations.postNumberMatch(form.tiltakshaver.adresse.postnr).Success)
                                            {
                                                _validationResult.AddMessage("4373.2.5.6.4", new[] { postNr });
                                            }
                                            else
                                            {
                                                var postnrValidation = _postalCodeProvider.ValidatePostnr(postNr, landkode);
                                                if (!Helpers.ObjectIsNullOrEmpty(postnrValidation))
                                                {
                                                    if (!postnrValidation.Valid)
                                                    {
                                                        _validationResult.AddMessage("4373.2.5.6.5", new[] { postNr });
                                                    }
                                                    else
                                                    {
                                                        if (!postnrValidation.Result.Equals(form.tiltakshaver.adresse.poststed, StringComparison.CurrentCultureIgnoreCase))
                                                        {
                                                            _validationResult.AddMessage("4373.2.5.6.6", new[] { postNr, form.tiltakshaver.adresse.poststed, postnrValidation.Result });
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    _validationResult.AddMessage("4373.2.5.6.7", null);
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            if (string.IsNullOrEmpty(form.tiltakshaver?.navn))
                                _validationResult.AddMessage("4373.2.5.7", null);

                            if (string.IsNullOrEmpty(form.tiltakshaver?.telefonnummer) && string.IsNullOrEmpty(form.tiltakshaver?.mobilnummer))
                            {
                                _validationResult.AddMessage("4373.2.5.8", null);
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(form.tiltakshaver?.telefonnummer))
                                {
                                    var telefonNumber = form.tiltakshaver.telefonnummer;
                                    var isValidTelefonNumber = telefonNumber.All(c => "+0123456789".Contains(c));
                                    if (!isValidTelefonNumber)
                                    {
                                        _validationResult.AddMessage("4373.3.5.9", null);
                                    }
                                }

                                if (!string.IsNullOrEmpty(form.tiltakshaver?.mobilnummer))
                                {
                                    var mobilNummer = form.tiltakshaver.mobilnummer;
                                    var isValidmobilnummer = mobilNummer.All(c => "+0123456789".Contains(c));
                                    if (!isValidmobilnummer)
                                    {
                                        _validationResult.AddMessage("4373.3.5.10", null);
                                    }
                                }
                            }

                            if (string.IsNullOrEmpty(form.tiltakshaver?.epost))
                                _validationResult.AddMessage("4373.2.5.11", null);
                        }
                    }
                }
            }
        }

        internal void MetadataValidering(TiltakUtenAnsvarsrettType form)
        {
            if (string.IsNullOrEmpty(form.metadata?.fraSluttbrukersystem))
                _validationResult.AddMessage("4373.2.6", null);
        }

        internal void VarslingValidering(TiltakUtenAnsvarsrettType form, List<string> formSetElements)
        {
            if (Helpers.ObjectIsNullOrEmpty(form.varsling))
            {
                _validationResult.AddMessage("4373.2.7", null);
            }
            else
            {
                if (!form.varsling.fritattFraNabovarslingSpecified || !form.varsling.fritattFraNabovarsling.HasValue)
                {
                    _validationResult.AddMessage("4373.2.7.1", null);
                }
                else
                {
                    if (!form.varsling.fritattFraNabovarsling.GetValueOrDefault())
                    {
                        if (formSetElements == null || !formSetElements.Contains("Gjenpart nabovarsel") && !formSetElements.Contains("Opplysninger gitt i nabovarsel") && !formSetElements.Contains("Nabovarsel") && !formSetElements.Contains("GjenpartNabovarsel"))
                        {
                            _validationResult.AddMessage("4373.2.7.2", null);
                        }

                        if (formSetElements == null || !formSetElements.Contains("KvitteringNabovarsel") && !formSetElements.Contains("Kvittering for nabovarsel"))
                        {
                            _validationResult.AddMessage("4373.2.7.3", null);
                        }

                        if (!form.varsling.foreliggerMerknader.HasValue || !form.varsling.foreliggerMerknaderSpecified)
                        {
                            _validationResult.AddMessage("4373.2.7.4", null);
                        }
                        else
                        {
                            if (form.varsling.foreliggerMerknader.GetValueOrDefault())
                            {
                                if (String.IsNullOrEmpty(form.varsling.antallMerknader))
                                {
                                    _validationResult.AddMessage("4373.2.7.4.1", null);
                                }

                                if (formSetElements == null || !formSetElements.Contains("KommentarNabomerknader") && string.IsNullOrEmpty(form.varsling.vurderingAvMerknader))
                                {
                                    _validationResult.AddMessage("4373.2.7.4.2", null);
                                }

                                if (formSetElements == null || !formSetElements.Contains("Nabomerknader"))
                                {
                                    _validationResult.AddMessage("4373.2.7.4.3", null);
                                }
                            }
                        }
                    }
                }
            }
        }

        internal void GjeldendePlanValidering(TiltakUtenAnsvarsrettType form, List<string> formSetElements)
        {
            if (Helpers.ObjectIsNullOrEmpty(form?.rammebetingelser?.plan?.gjeldendePlan?.plantype))
            {
                _validationResult.AddMessage("4373.2.8.4", null);
            }
            else
            {
                var planType = form.rammebetingelser.plan.gjeldendePlan.plantype;
                if (Helpers.ObjectIsNullOrEmpty(form.rammebetingelser.plan.gjeldendePlan.plantype.kodeverdi))
                {
                    _validationResult.AddMessage("4373.2.8.4.1", null);
                }
                else
                {
                    if (!_codeListService.IsCodeValidInCodeList("plantype", planType.kodeverdi))
                    {
                        _validationResult.AddMessage("4373.2.8.4.1.1", new[] { planType.kodeverdi });
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(planType.kodebeskrivelse))
                        {
                            _validationResult.AddMessage("4373.3.8.4.2", null);
                        }
                        else
                        {
                            if (!_codeListService.IsCodelistLabelValid("Plantype", planType.kodeverdi, planType.kodebeskrivelse))
                            {
                                _validationResult.AddMessage("4373.3.8.4.2.1", new[] { planType.kodebeskrivelse });
                            }
                        }
                    }
                }

                if (string.IsNullOrEmpty(form.rammebetingelser?.plan?.gjeldendePlan?.beregningsregelGradAvUtnytting?.kodeverdi))
                {
                    _validationResult.AddMessage("4373.2.8.4.3", null);
                }
                else
                {
                    var beregningsregelGradAvUtnyttingKodeverdi = form.rammebetingelser.plan.gjeldendePlan.beregningsregelGradAvUtnytting.kodeverdi;
                    if (!_codeListService.IsCodelistValid("beregningsregelGradAvUtnytting", beregningsregelGradAvUtnyttingKodeverdi))
                    {
                        _validationResult.AddMessage("4373.2.8.4.3.1", new[] { beregningsregelGradAvUtnyttingKodeverdi });
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(form.rammebetingelser.plan.gjeldendePlan.beregningsregelGradAvUtnytting.kodebeskrivelse))
                        {
                            _validationResult.AddMessage("4373.2.8.4.3.2", new[] { beregningsregelGradAvUtnyttingKodeverdi });
                        }

                        if (beregningsregelGradAvUtnyttingKodeverdi == "annetUdef")
                        {
                            if (formSetElements == null || !formSetElements.Any(f => f.Equals("UnderlagUtnytting")))
                            {
                                _validationResult.AddMessage("4373.2.8.4.3.3", null);
                            }
                        }
                    }
                }
            }

            if (string.IsNullOrEmpty(form.rammebetingelser?.plan?.gjeldendePlan?.formaal))
            {
                _validationResult.AddMessage("4373.2.8.5", null);
            }

            if (string.IsNullOrEmpty(form.rammebetingelser?.plan?.gjeldendePlan?.navn))
            {
                _validationResult.AddMessage("4373.2.8.6", null);
            }
        }

        internal void PlasseringValidering(TiltakUtenAnsvarsrettType form, List<string> formSetElements)
        {
            //Hvis krav til plassering
            if (!Helpers.ObjectIsNullOrEmpty(form.rammebetingelser?.plassering))
            {
                if (form.rammebetingelser.plassering.konfliktHoeyspentkraftlinjeSpecified && form.rammebetingelser.plassering.konfliktHoeyspentkraftlinje == true)
                {
                    if (formSetElements == null || !formSetElements.Contains("AvklaringHoyspent"))
                    {
                        _validationResult.AddMessage("4373.2.9", null);
                    }
                }

                if (!form.rammebetingelser.plassering.konfliktVannOgAvloepSpecified || !form.rammebetingelser.plassering.konfliktVannOgAvloep.HasValue)
                {
                    _validationResult.AddMessage("4373.3.9.1.1", null);
                }
                else
                {
                    if (form.rammebetingelser.plassering.konfliktVannOgAvloep.GetValueOrDefault())
                    {
                        if (formSetElements == null || !formSetElements.Contains("AvklaringVA"))
                            _validationResult.AddMessage("4373.2.9.1", null);
                    }
                }

                if (!form.rammebetingelser.plassering.bekreftetInnenforByggegrenseSpecified || !form.rammebetingelser.plassering.bekreftetInnenforByggegrense.HasValue)
                    _validationResult.AddMessage("4373.3.9.2", null);
                else
                {
                    if (!form.rammebetingelser.plassering.bekreftetInnenforByggegrense.GetValueOrDefault())
                    {
                        if (!form.rammebetingelser.plassering.minsteAvstandNabogrense.HasValue)
                            _validationResult.AddMessage("4373.3.9.2.1", null);

                        if (!form.rammebetingelser.plassering.minsteAvstandTilAnnenBygning.HasValue)
                            _validationResult.AddMessage("4373.3.9.2.3", null);

                        if (!form.rammebetingelser.plassering.minsteAvstandTilMidtenAvVei.HasValue)
                            _validationResult.AddMessage("4373.3.9.2.2", null);
                    }
                }

                if (!form.rammebetingelser.plassering.konfliktHoeyspentkraftlinjeSpecified || !form.rammebetingelser.plassering.konfliktHoeyspentkraftlinje.HasValue)
                    _validationResult.AddMessage("4373.3.9.3", null);
            }
        }

        internal void DispensasjonValidering(TiltakUtenAnsvarsrettType form, List<string> formSetElements)
        {
            if (!Helpers.ObjectIsNullOrEmpty(form.dispensasjon))
            {
                Dictionary<string, CodelistFormat> codeData = _codeListService.GetCodelist("dispensasjonstype", RegistryType.KodelisteByggesoknad);

                foreach (var disp in form.dispensasjon)
                {
                    if (Helpers.ObjectIsNullOrEmpty(disp.dispensasjonstype))
                    {
                        _validationResult.AddMessage("4373.2.10", null);
                    }
                    else
                    {
                        if (!_codeListService.IsCodeValidInCodeList("dispensasjonstype", disp.dispensasjonstype.kodeverdi))
                        {
                            _validationResult.AddMessage("4373.2.10.3", new[] { disp.dispensasjonstype.kodeverdi });
                        }
                        else
                        {
                            if (!formSetElements.Any(f => f.Equals("Dispensasjonssoeknad")))
                            {
                                if (String.IsNullOrEmpty(disp.beskrivelse))
                                    _validationResult.AddMessage("4373.2.10.1", null);
                                if (String.IsNullOrEmpty(disp.begrunnelse))
                                    _validationResult.AddMessage("4373.2.10.2", null);
                            }
                        }
                    }
                }
            }
        }

        internal void KravTilByggegrunnValidering(TiltakUtenAnsvarsrettType form, List<string> formSetElements)
        {
            if (Helpers.ObjectIsNullOrEmpty(form.rammebetingelser?.kravTilByggegrunn) || !form.rammebetingelser.kravTilByggegrunn.miljoeforhold.HasValue)
            {
                _validationResult.AddMessage("4373.3.11.1", null);
            }
            else
            {
                if (form.rammebetingelser != null && form.rammebetingelser.kravTilByggegrunn.miljoeforhold.GetValueOrDefault())
                {
                    if (formSetElements == null || !formSetElements.Contains("RedegjoerelseAndreNaturMiljoeforhold"))
                    {
                        _validationResult.AddMessage("4373.2.11.1.1", null);
                    }
                }
            }

            if (Helpers.ObjectIsNullOrEmpty(form.rammebetingelser?.kravTilByggegrunn) || !form.rammebetingelser.kravTilByggegrunn.skredutsattOmraade.HasValue)
            {
                _validationResult.AddMessage("4373.3.11.2", null);
            }
            else
            {
                if (form.rammebetingelser != null && form.rammebetingelser.kravTilByggegrunn.skredutsattOmraade.GetValueOrDefault())
                {
                    if (formSetElements == null || !formSetElements.Contains("RedegjoerelseSkredOgFlom"))
                    {
                        _validationResult.AddMessage("4373.2.11.2.1", null);
                    }
                }
            }

            if (Helpers.ObjectIsNullOrEmpty(form.rammebetingelser?.kravTilByggegrunn) || !form.rammebetingelser.kravTilByggegrunn.flomutsattOmraade.HasValue)
            {
                _validationResult.AddMessage("4373.3.11.3", null);
            }
            else
            {
                if (form.rammebetingelser != null && form.rammebetingelser.kravTilByggegrunn.flomutsattOmraade.GetValueOrDefault())
                {
                    if (formSetElements == null || !formSetElements.Contains("RedegjoerelseSkredOgFlom"))
                    {
                        _validationResult.AddMessage("4373.2.11.3.1", null);
                    }
                }
            }
        }

        internal void AdkomstValidering(TiltakUtenAnsvarsrettType form, List<string> formSetElements)
        {
            //Hvis tiltaksstype krever adkomst - ref sjekkpunkt 7.1
            using (var logger = new PerfTimerLogger())
            {
                if (form.rammebetingelser?.adkomst == null)
                {
                    _validationResult.AddMessage("4373.2.12", null);
                }
                else
                {
                    if (form.rammebetingelser.adkomst.nyeEndretAdkomst.HasValue == false)
                    {
                        _validationResult.AddMessage("4373.2.12.1", null);
                    }
                }

                //Vegtype - liste
                if (!Helpers.ObjectIsNullOrEmpty(form.rammebetingelser?.adkomst))
                {
                    if (form.rammebetingelser.adkomst.nyeEndretAdkomst.GetValueOrDefault())
                    {
                        //Vedleggssjekk
                        if (formSetElements == null || !formSetElements.Contains("Avkjoerselsplan"))
                        {
                            _validationResult.AddMessage("4373.2.12.2", null);
                        }

                        if (form.rammebetingelser.adkomst.vegrett != null)
                        {
                            //Avhengig av vegtype så må en svare på om tillatelse er gitt
                            Dictionary<string, CodelistFormat> codeData = _codeListService.GetCodelist("vegtype", RegistryType.KodelisteByggesoknad);

                            foreach (var vegtype in form.rammebetingelser.adkomst.vegrett)
                            {
                                if (vegtype.vegtype != null)
                                {
                                    if (!String.IsNullOrEmpty(vegtype.vegtype.kodeverdi))
                                    {
                                        if (!codeData.ContainsKey(vegtype.vegtype.kodeverdi))
                                        {
                                            _validationResult.AddMessage("4373.2.12.3", new[] { vegtype.vegtype.kodeverdi });
                                        }

                                        //sjekke om tillatelse er gitt for aktuell vegtype
                                        if (!vegtype.erTillatelseGitt.HasValue)
                                        {
                                            _validationResult.AddMessage("4373.2.12.4", new[] { vegtype.vegtype.kodeverdi });
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        internal void OppfyllesVilkaarFor3UkersfristValidering(TiltakUtenAnsvarsrettType form, List<string> formSetElements)
        {
            if (!Helpers.ObjectIsNullOrEmpty(form.rammebetingelser?.generelleVilkaar))
            {
                if (form.rammebetingelser.generelleVilkaar.oppfyllesVilkaarFor3Ukersfrist.GetValueOrDefault())
                {
                    if (form.rammebetingelser.generelleVilkaar.behovForTillatelse.GetValueOrDefault())
                        _validationResult.AddMessage("4373.2.13", null);

                    if (!Helpers.ObjectIsNullOrEmpty(form.dispensasjon) && form.dispensasjon.Any())
                        _validationResult.AddMessage("4373.2.13.1", null);
                }
            }
        }

        internal void VedleggValidaring(TiltakUtenAnsvarsrettType form, List<string> formSetElements)
        {
            using (var logger = new PerfTimerLogger())
            {
                var tengningen = new List<string>()
                {
                    "TegningEksisterendeSnitt",
                    "TegningNyttSnitt",
                    "TegningEksisterendePlan",
                    "TegningNyPlan",
                    "TegningEksisterendeFasade",
                    "TegningNyFasade"
                };

                //https://stackoverflow.com/a/11092955
                bool hasDrawing = formSetElements?.Select(x => x)
                    .Intersect(tengningen)
                    .Any() ?? false;
                //Minst en tegning
                if (!hasDrawing)
                {
                    _validationResult.AddMessage("4373.2.14", null);
                }
                else
                {
                    // Plantegning Validering
                    if (formSetElements == null || !formSetElements.Contains("TegningEksisterendePlan") || !formSetElements.Contains("TegningNyPlan"))
                    {
                        _validationResult.AddMessage("4373.2.14.1", null);
                    }

                    // snittegninger validering
                    if (formSetElements == null || !formSetElements.Contains("TegningEksisterendeSnitt") || !formSetElements.Contains("TegningNyttSnitt"))
                    {
                        _validationResult.AddMessage("4373.2.14.2", null);
                    }

                    //FasadetegningerValidering
                    if (formSetElements == null || !formSetElements.Contains("TegningEksisterendeFasade") || !formSetElements.Contains("TegningNyFasade"))
                    {
                        _validationResult.AddMessage("4373.2.14.3", null);
                    }
                }

                // Situasjonsplan
                if (formSetElements == null || !formSetElements.Contains("Situasjonsplan"))
                {
                    _validationResult.AddMessage("4373.2.14.4", null);
                }
            }
        }

        internal void VannforsyningValidering(TiltakUtenAnsvarsrettType form)
        {
            //Hvis tiltakstype krever vannforsyning
            using (var logger = new PerfTimerLogger())
            {
                if (Helpers.ObjectIsNullOrEmpty(form.rammebetingelser?.vannforsyning?.tilknytningstype))
                {
                    _validationResult.AddMessage("4373.2.15", null);
                }

                if (!Helpers.ObjectIsNullOrEmpty(form.rammebetingelser?.vannforsyning?.tilknytningstype))
                {
                    Dictionary<string, CodelistFormat> codeData = _codeListService.GetCodelist("vanntilknytning", RegistryType.KodelisteByggesoknad);

                    foreach (var tilknytningstype in form.rammebetingelser.vannforsyning.tilknytningstype)
                    {
                        if (!String.IsNullOrEmpty(tilknytningstype.kodeverdi))
                        {
                            if (!codeData.ContainsKey(tilknytningstype.kodeverdi))
                            {
                                _validationResult.AddMessage("4373.2.15.1", new[] { tilknytningstype.kodeverdi });
                            }
                            else
                            {
                                if (tilknytningstype.kodeverdi == "AnnenPrivatInnlagt")
                                {
                                    if (String.IsNullOrEmpty(form.rammebetingelser.vannforsyning.beskrivelse))
                                    {
                                        _validationResult.AddMessage("4373.2.15.2", null);
                                    }
                                }
                                if (tilknytningstype.kodeverdi != "BerorerIkkeVannverk" && tilknytningstype.kodeverdi != "AnnenPrivatIkkeInnlagt")
                                {
                                    if (form.rammebetingelser.vannforsyning.krysserVannforsyningAnnensGrunn.GetValueOrDefault())
                                    {
                                        if (form.rammebetingelser.vannforsyning.tinglystErklaeringSpecified == false)
                                            _validationResult.AddMessage("4373.2.15.3", null);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        internal void AvloepValidering(TiltakUtenAnsvarsrettType form)
        {
            //Hvis tiltakstype krever avlop
            using (var logger = new PerfTimerLogger())
            {
                if (form.rammebetingelser?.avloep?.tilknytningstype == null)
                {
                    _validationResult.AddMessage("4373.2.16", null);
                }

                if (form.rammebetingelser?.avloep?.tilknytningstype != null)
                {
                    if (!String.IsNullOrEmpty(form.rammebetingelser.avloep.tilknytningstype.kodeverdi))
                    {
                        if (!_codeListService.IsCodeValidInCodeList("avlopstilknytning", form.rammebetingelser.avloep.tilknytningstype.kodeverdi))
                        {
                            _validationResult.AddMessage("4373.2.16.1", new[] { form.rammebetingelser.avloep.tilknytningstype.kodeverdi });
                        }
                        else
                        {
                            if (String.IsNullOrEmpty(form.rammebetingelser.avloep.tilknytningstype.kodebeskrivelse))
                            {
                                _validationResult.AddMessage("4373.2.16.2", null);
                            }

                            if (form.rammebetingelser.avloep.tilknytningstype.kodeverdi == "PrivatKloakk")
                            {
                                if (!form.rammebetingelser.avloep.installereVannklosett.HasValue)
                                {
                                    _validationResult.AddMessage("4373.2.16.3", null);
                                }

                                if (!form.rammebetingelser.avloep.utslippstillatelse.HasValue)
                                {
                                    _validationResult.AddMessage("4373.2.16.4", null);
                                }
                            }
                        }
                    }
                }

                if (Helpers.ObjectIsNullOrEmpty(form.rammebetingelser?.avloep) || !form.rammebetingelser.avloep.overvannTerreng.HasValue)
                {
                    _validationResult.AddMessage("4373.3.16.5", null);
                }
            }
        }

        internal void ArealdisponeringValidering(TiltakUtenAnsvarsrettType form)
        {
            if (!Helpers.ObjectIsNullOrEmpty(form.rammebetingelser?.plan?.gjeldendePlan?.beregningsregelGradAvUtnytting))
            {
                var beregnGradUtnyttingKodeverdi = form.rammebetingelser?.plan?.gjeldendePlan?.beregningsregelGradAvUtnytting?.kodeverdi;

                if (beregnGradUtnyttingKodeverdi != null && !beregnGradUtnyttingKodeverdi.Equals("annetUdef") && !beregnGradUtnyttingKodeverdi.Equals("ingenKrav"))
                {
                    if (Helpers.ObjectIsNullOrEmpty(form.rammebetingelser.arealdisponering))
                    {
                        _validationResult.AddMessage("4373.3.18.1.1", null);
                    }
                    else
                    {
                        double utfyltGradAvUtnytting;
                        double beregnetGradAvUtnytting;
                        if (Helpers.ObjectIsNullOrEmpty(form.rammebetingelser?.arealdisponering) ||
                            !form.rammebetingelser.arealdisponering.arealBebyggelseEksisterendeSpecified ||
                            !form.rammebetingelser.arealdisponering.arealBebyggelseNyttSpecified ||
                            !form.rammebetingelser.arealdisponering.arealBebyggelseSomSkalRivesSpecified ||
                            !form.rammebetingelser.arealdisponering.parkeringsarealTerrengSpecified ||
                            !form.rammebetingelser.arealdisponering.beregnetGradAvUtnyttingSpecified)
                        {
                            _validationResult.AddMessage("4373.3.18.1", new[] { beregnGradUtnyttingKodeverdi });
                        }
                        else
                        {
                            //Krav om at regnestykke er riktig
                            //Sjekke på negative verdier
                            if (form.rammebetingelser.arealdisponering.arealBebyggelseEksisterende.Value < 0 ||
                                form.rammebetingelser.arealdisponering.arealBebyggelseNytt.Value < 0 ||
                                form.rammebetingelser.arealdisponering.arealBebyggelseSomSkalRives.Value < 0 ||
                                form.rammebetingelser.arealdisponering.parkeringsarealTerreng.Value < 0 ||
                                form.rammebetingelser.arealdisponering.beregnetGradAvUtnytting.Value < 0
                            )
                            {
                                if (form.rammebetingelser.arealdisponering.arealBebyggelseEksisterende.Value < 0)
                                    _validationResult.AddMessage("4373.3.18.2", null);

                                if (form.rammebetingelser.arealdisponering.arealBebyggelseNytt.Value < 0)
                                    _validationResult.AddMessage("4373.3.18.3", null);

                                if (form.rammebetingelser.arealdisponering.arealBebyggelseSomSkalRives.Value < 0)
                                    _validationResult.AddMessage("4373.3.18.4", null);

                                if (form.rammebetingelser.arealdisponering.parkeringsarealTerreng.Value < 0)
                                    _validationResult.AddMessage("4373.3.18.5", null);

                                if (form.rammebetingelser.arealdisponering.beregnetGradAvUtnytting.Value < 0)
                                    _validationResult.AddMessage("4373.3.18.6", null);
                            }
                            else
                            {
                                utfyltGradAvUtnytting = form.rammebetingelser.arealdisponering.beregnetGradAvUtnytting.Value;
                                beregnetGradAvUtnytting = ValidationHelper.BeregnetGradAvUtnyttingAreal(
                                    form.rammebetingelser.arealdisponering.arealBebyggelseEksisterende.Value,
                                    form.rammebetingelser.arealdisponering.arealBebyggelseSomSkalRives.Value,
                                    form.rammebetingelser.arealdisponering.arealBebyggelseNytt.Value,
                                    form.rammebetingelser.arealdisponering.parkeringsarealTerreng.Value);

                                switch (beregnGradUtnyttingKodeverdi)
                                {
                                    case "BYA":
                                    case "BRA":
                                    case "T-BRA":
                                        if (!ValidationHelper.UtfyltOgBeregnetArealErDetSamme(utfyltGradAvUtnytting, beregnetGradAvUtnytting))
                                            _validationResult.AddMessage("4373.3.18.7", new[] { beregnGradUtnyttingKodeverdi, utfyltGradAvUtnytting.ToString("##.##"), beregnetGradAvUtnytting.ToString("##.##") });

                                        break;

                                    case "%BYA":
                                    case "%BRA":
                                    case "%TU":
                                        if (!form.rammebetingelser.arealdisponering.tomtearealBeregnetSpecified || !form.rammebetingelser.arealdisponering.tomtearealBeregnet.HasValue)
                                        {
                                            _validationResult.AddMessage("4373.3.18.8", null);
                                        }
                                        else
                                        {
                                            double tomtearealBeregnet = (double)form.rammebetingelser.arealdisponering.tomtearealBeregnet;

                                            if (tomtearealBeregnet == 0)
                                            {
                                                _validationResult.AddMessage("4373.3.18.9", null);
                                            }
                                            else if (tomtearealBeregnet < 0)
                                            {
                                                _validationResult.AddMessage("4373.3.18.10", null);
                                            }
                                            else
                                            {
                                                var porcentGradAvUtnyttingBeregnt = beregnetGradAvUtnytting / tomtearealBeregnet * 100;
                                                if (!ValidationHelper.UtfyltOgBeregnetArealErDetSamme(utfyltGradAvUtnytting, porcentGradAvUtnyttingBeregnt))
                                                {
                                                    _validationResult.AddMessage("4373.3.18.11", new[] { beregnGradUtnyttingKodeverdi, utfyltGradAvUtnytting.ToString("##.##"), porcentGradAvUtnyttingBeregnt.ToString("##.##") });
                                                }
                                            }
                                        }

                                        break;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}