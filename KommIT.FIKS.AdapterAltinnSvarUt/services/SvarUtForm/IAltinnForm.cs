﻿using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using System.Collections.Generic;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services
{
    public interface IAltinnForm
    {
        string GetDataFormatId();
        string GetDataFormatVersion();
        string GetSchemaFile();
        /// <summary>
        /// Gets name of form
        /// </summary>
        string GetName();
        /// <summary>
        /// Gets municipality code
        /// </summary>
        string GetMunicipalityCode();
        void InitiateForm(string formDataAsXml);
        PropertyIdentifiers GetPropertyIdentifiers();
        ValidationResult ValidateData();

        /// <summary>
        /// Sets subforms and attachmenttypes for validationrules
        /// </summary>
        /// <param name="attachmentlist"></param>
        void SetFormSetElements(List<string> attachmentlist);


        /// <summary>
        /// Validates that the entity that submitted the form through Altinn
        /// is also the entity that is suppose to submit the form by the
        /// data given in the form data (XML)
        /// </summary>
        /// <param name="authenticatedSubmitter">Org num or fnr</param>
        /// <param name="validationResult">Validation results, will be added to if not null</param>
        /// <returns>bool true if the submitter validates, false if not</returns>
        bool? ValidateAuthenticatedSubmitterVsFormData(string authenticatedSubmitter, ValidationResult validationResult);

    }
    
}