﻿namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.SvarUt
{
    public interface SvarUtForm
    {

        string GetMunicipalityCode();

    }
    
}