﻿using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.SvarUtForm;
using System.Collections.Generic;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.SvarUt
{
    public class EtttrinnsoknadFormV3 : IAltinnForm, SvarUtForm
    {
        private const string DataFormatId = "6740";
        private const string DataFormatVersion = "45751";
        private const string SchemaFile = "etttrinnsoknad3.xsd";
        private const string Name = "Søknad om tillatelse i ett trinn";
        private List<string> FormSetElements;
        public void SetFormSetElements(List<string> attachmentlist)
        {
            FormSetElements = attachmentlist;
        }

        public bool? ValidateAuthenticatedSubmitterVsFormData(string authenticatedSubmitter, ValidationResult validationResult)
        {
            bool isSubmitterValid = Helpers.ValidateEntityOfAltinnSubmitterDefault(authenticatedSubmitter, GetPropertyIdentifiers());

            if (!isSubmitterValid)
            {
                Helpers.RecordErrorAltinnSubmitter(validationResult);
            }
            return isSubmitterValid;
        }

        public void InitiateForm(string formDataAsXml)
        {

            form = Utils.SerializeUtil.DeserializeFromString<no.kxml.skjema.dibk.etttrinnsoknadV3.EttTrinnType>(formDataAsXml);
        }

        private no.kxml.skjema.dibk.etttrinnsoknadV3.EttTrinnType form;

        public PropertyIdentifiers GetPropertyIdentifiers()
        {
            PropertyIdentifiers propertyIdentifiers = new PropertyIdentifiers();

            propertyIdentifiers.Kommunenummer = form.eiendomByggested[0].eiendomsidentifikasjon.kommunenummer;
            propertyIdentifiers.Gaardsnummer = form.eiendomByggested[0].eiendomsidentifikasjon.gaardsnummer;
            propertyIdentifiers.Bruksnummer = form.eiendomByggested[0].eiendomsidentifikasjon.bruksnummer;
            propertyIdentifiers.Festenummer = form.eiendomByggested[0].eiendomsidentifikasjon.festenummer;
            propertyIdentifiers.Seksjonsnummer = form.eiendomByggested[0].eiendomsidentifikasjon.seksjonsnummer;
            propertyIdentifiers.Adresselinje1 = form.eiendomByggested[0].adresse.adresselinje1;
            propertyIdentifiers.Adresselinje2 = form.eiendomByggested[0].adresse.adresselinje2;
            propertyIdentifiers.Adresselinje3 = form.eiendomByggested[0].adresse.adresselinje3;
            propertyIdentifiers.Postnr = form.eiendomByggested[0].adresse.postnr;
            propertyIdentifiers.Poststed = form.eiendomByggested[0].adresse.poststed;
            propertyIdentifiers.Landkode = form.eiendomByggested[0].adresse.landkode;
            propertyIdentifiers.Bygningsnummer = form.eiendomByggested[0].bygningsnummer;
            propertyIdentifiers.Bolignummer = form.eiendomByggested[0].bolignummer;
            propertyIdentifiers.TiltakType = form.beskrivelseAvTiltak.type[0].kodeverdi;

            propertyIdentifiers.SoeknadSkjemaNavn = Name;
            propertyIdentifiers.FraSluttbrukersystem = form.metadata.fraSluttbrukersystem;

            propertyIdentifiers.AnsvarligSokerNavn = form.ansvarligSoeker.navn;
            propertyIdentifiers.AnsvarligSokerOrgnr = form.ansvarligSoeker.organisasjonsnummer;
            propertyIdentifiers.AnsvarligSokerFnr = form.ansvarligSoeker.foedselsnummer;

            if (form.ansvarligSoeker.adresse != null)
            {
                propertyIdentifiers.AnsvarligSokerAdresselinje1 = form.ansvarligSoeker.adresse.adresselinje1;
                propertyIdentifiers.AnsvarligSokerPostnr = form.ansvarligSoeker.adresse.postnr;
                propertyIdentifiers.AnsvarligSokerPoststed = form.ansvarligSoeker.adresse.poststed;
            }
            return propertyIdentifiers;
        }

        public ValidationResult ValidateData()
        {

            return new EtttrinnsoknadFormV3Validator().Validate(form, FormSetElements);
        }

        public string GetDataFormatId()
        {
            return DataFormatId;
        }

        public string GetDataFormatVersion()
        {
            return DataFormatVersion;
        }

        public string GetMunicipalityCode()
        {
            return GetPropertyIdentifiers().Kommunenummer;
        }

        public string GetSchemaFile()
        {
            return SchemaFile;
        }

        public string GetName()
        {
            return Name;
        }
    }
}