﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using DIBK.FBygg.Altinn.MapperCodelist;
using KommIT.FIKS.AdapterAltinnSvarUt.Logger;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Matrikkel;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Validations;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;
using no.kxml.skjema.dibk.arbeidstilsynetsSamtykke;
using Serilog;


namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.SvarUtForm
{
    public class ArbeidstilsynetsSamtykkeFormV1Validation
    {
        private ILogger _logger = Log.ForContext<ArbeidstilsynetsSamtykkeFormV1Validation>();

        private readonly string _skjema = "ArbeidstilsynetsSamtykkeType";
        private ValidationResult _validationResult;

        CodeListService _codeListService = new CodeListService();

        public IMunicipalityValidator MunicipalityValidator { get; set; }


        public ArbeidstilsynetsSamtykkeFormV1Validation()
        {
            MunicipalityValidator = new MunicipalityValidator();

            var form = new ArbeidstilsynetsSamtykkeType();
            _validationResult = new ValidationResult();

            _validationResult.AddRule(_skjema, "4845.1.1", "", "Innhold må være ihht informasjonsmodell/xsd for skjema.", "", "ERROR", "");
            _validationResult.AddRule(_skjema, "4845.1.3", "", "Må fylle ut fraSluttbrukersystem med samme navn som brukt i registrering for Altinn API.", Helpers.GetFullClassPath(() => form.fraSluttbrukersystem), "ERROR", "");
            //_validationResult.AddRule("4845.1.2", "Koder må være ihht publiserte kodelister i XSD skjema.", "", "ERROR", "");

            //Eiendommens--> eiendomsidentifikasjon
            _validationResult.AddRule(_skjema, "4845.1.4", "", "Du må oppgi hvilken eiendom/hvilke eiendommer byggesøknaden gjelder.", Helpers.GetFullClassPath(() => form.eiendomByggested), "ERROR", "");
            _validationResult.AddRule(_skjema, "4845.1.4.1", "", "Kommunenummer må fylles ut for eiendom / byggested.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].eiendomsidentifikasjon.kommunenummer), "ERROR", "");
            _validationResult.AddRule(_skjema, "4845.1.4.2", "", "Kommunenummer for eiendom/byggested finnes ikke i kodeliste: https://register.geonorge.no/sosi-kodelister/inndelinger/inndelingsbase/kommunenummer", Helpers.GetFullClassPath(() => form.eiendomByggested[0].eiendomsidentifikasjon.kommunenummer), "ERROR", "");
            _validationResult.AddRule(_skjema, "4845.1.4.3", "", "Kommunenummer for eiendom/byggested har ugyldig status ({0}). Du kan sjekke status på https://register.geonorge.no/sosi-kodelister/inndelinger/inndelingsbase/kommunenummer", Helpers.GetFullClassPath(() => form.eiendomByggested[0].eiendomsidentifikasjon.kommunenummer), "ERROR", "");//
            _validationResult.AddRule(_skjema, "4845.1.4.4", "", "Gårdsnummer og bruksnummer må fylles ut for eiendom/byggested.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].eiendomsidentifikasjon), "ERROR", "");
            _validationResult.AddRule(_skjema, "4845.1.4.5", "", "Gårdsnummer '{0}' og bruksnummer '{1}' for eiendom/byggested må være tall.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].eiendomsidentifikasjon.gaardsnummer), "ERROR", "");
            _validationResult.AddRule(_skjema, "4845.1.4.6", "1.3", "Gårdsnummer '{0}' for eiendom/byggested må være '0' eller større.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].eiendomsidentifikasjon.gaardsnummer), "ERROR", null);
            _validationResult.AddRule(_skjema, "4845.1.4.6.1", "1.4", "Bruksnummer '{0}' for eiendom/byggested må være '0' eller større.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].eiendomsidentifikasjon.bruksnummer), "ERROR", null);


            _validationResult.AddRule(_skjema, "4845.1.4.7", "", "Hvis bygningsnummer {0} er oppgitt for eiendom/byggested, må det være et tall.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].bygningsnummer), "ERROR", "");
            _validationResult.AddRule(_skjema, "4845.1.4.8", "", "Bygningsnummer for eiendom/byggested må være større enn '0'.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].bygningsnummer), "ERROR", "");
            //Eiendommens --> Adress
            _validationResult.AddRule(_skjema, "4845.1.4.9", "", "Postadresse for eiendom/byggested bør fylles ut.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].adresse), "WARNING", "");
            _validationResult.AddRule(_skjema, "4845.1.4.9.1", "", "Adresselinje 1 for eiendom/byggested bør fylles ut.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].adresse.adresselinje1), "WARNING", "");
            _validationResult.AddRule(_skjema, "4845.1.4.9.2", "", "Du bør også oppgi gatenavn, husnummer og eventuell bokstav for eiendom/byggested. Du kan sjekke riktig adresse på https://seeiendom.no", Helpers.GetFullClassPath(() => form.eiendomByggested[0].adresse.gatenavn), "WARNING", "");//
            _validationResult.AddRule(_skjema, "4845.1.4.10", "", "Kommunenavn bør fylles ut for eiendom/byggested.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].kommunenavn), "WARNING", "");
            _validationResult.AddRule(_skjema, "4845.1.4.11", "", "Når bruksenhetsnummer/'bolignummer' er fylt ut, må det følge riktig format (f. eks. H0101). Se https://www.kartverket.no/eiendom/adressering/bruksenhetsnummer/.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].bolignummer), "WARNING", "");

            //if (_matrikkelFlag)
            //{
            _validationResult.AddRule(_skjema, "4845.1.4.12", "", "Hvis Eiendomsidentifikasjon [{0}-{1}/{2}/{3}/{4}] er oppgitt for eiendom/byggested, bør den være gyldig i matrikkelen. Du kan sjekke riktig informasjon på https://seeiendom.no", Helpers.GetFullClassPath(() => form.eiendomByggested[0].eiendomsidentifikasjon), "WARNING", "");
            _validationResult.AddRule(_skjema, "4845.1.4.13", "", "Eiendomsidentifikasjon [{0}-{1}/{2}/{3}/{4}] for eiendom/byggested ble ikke bekreftet mot matrikkelen.", Helpers.GetFullClassPath(() => form.eiendomByggested[0].eiendomsidentifikasjon), "WARNING", "");
            _validationResult.AddRule(_skjema, "4845.1.4.14", "", "Hvis bygningsnummer [{0}] er oppgitt for eiendom/byggested, bør den være gyldig i matrikkelen på aktuelt matrikkelnummer. Du kan sjekke riktig bygningsnummer på https://seeiendom.no", Helpers.GetFullClassPath(() => form.eiendomByggested[0].bygningsnummer), "WARNING", "");
            _validationResult.AddRule(_skjema, "4845.1.4.15", "", "Gateadresse for eiendom/byggested ble ikke bekreftet mot matrikkelen. Du kan sjekke adresse på https://seeiendom.no", Helpers.GetFullClassPath(() => form.eiendomByggested[0].adresse.gatenavn), "WARNING", "");
            _validationResult.AddRule(_skjema, "4845.1.4.15.1", "", "Du har oppgitt feil gateadresse for eiendom/byggested. Du kan sjekke riktig adresse på https://seeiendom.no", Helpers.GetFullClassPath(() => form.eiendomByggested[0].adresse.gatenavn), "ERROR", "");
            //}

            //Arbeidsplasser
            _validationResult.AddRule(_skjema, "4845.1.5", "", "Arbeidsplasser må fylles ut ", Helpers.GetFullClassPath(() => form.arbeidsplasser), "ERROR", "");
            _validationResult.AddRule(_skjema, "4845.1.5.1", "", "Det må velges enten 'eksisterende' eller 'fremtidige' eller begge deler.", Helpers.GetFullClassPath(() => form.arbeidsplasser.framtidige), "ERROR", "");
            _validationResult.AddRule(_skjema, "4845.1.5.2", "", "Det må velges enten 'faste' eller 'midlertidige' eller begge deler", Helpers.GetFullClassPath(() => form.arbeidsplasser.midlertidige), "ERROR", "");
            _validationResult.AddRule(_skjema, "4845.1.5.3", "", "Er tiltaket knyttet til utleiebygg så skal antall virksomheter angis.", Helpers.GetFullClassPath(() => form.arbeidsplasser.antallVirksomheter), "ERROR", Helpers.GetFullClassPath(() => form.arbeidsplasser.utleieBygg));
            _validationResult.AddRule(_skjema, "4845.1.5.4", "", "Det skal angis hvormange ansatte som bygget dimensjoneres for.", Helpers.GetFullClassPath(() => form.arbeidsplasser.antallAnsatte), "ERROR", "");
            _validationResult.AddRule(_skjema, "4845.1.5.5", "", "Enten skal arbeidsplasser beskrives i søknaden eller det skal være lagt ved vedlegg 2: 'Beskrivelse av type arbeid / prosesser'.", Helpers.GetFullClassPath(() => form.arbeidsplasser.beskrivelse), "ERROR", "");

            //Tiltakshaver            
            _validationResult.AddRule(_skjema, "4845.1.6", "", "Informasjon om tiltakshaver må fylles ut.", Helpers.GetFullClassPath(() => form.tiltakshaver), "ERROR", "");
            _validationResult.AddRule(_skjema, "4845.1.6.1", "", "Kodeverdien for 'partstype' for tiltakshaver må fylles ut.", Helpers.GetFullClassPath(() => form.tiltakshaver.partstype.kodeverdi), "ERROR", "");
            _validationResult.AddRule(_skjema, "4845.1.6.2", "", "Ugyldig kodeverdi '{0}' i henhold til kodeliste for 'partstype' for tiltakshaver. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/byggesoknad/partstype", Helpers.GetFullClassPath(() => form.tiltakshaver.partstype.kodeverdi), "ERROR", "");
            _validationResult.AddRule(_skjema, "4845.1.6.3", "", "Fødselsnummer må angis når tiltakshaver er privatperson.", Helpers.GetFullClassPath(() => form.tiltakshaver.foedselsnummer), "ERROR", "");
            _validationResult.AddRule(_skjema, "4845.1.6.3.1", "", "Tiltakshaver Fødselsnummer kan ikke dekrypteres", Helpers.GetFullClassPath(() => form.tiltakshaver.foedselsnummer), "ERROR", "");
            _validationResult.AddRule(_skjema, "4845.1.6.3.2", "", "Tiltakshaver Fødselsnummeret er ikke gyldig.", Helpers.GetFullClassPath(() => form.tiltakshaver.foedselsnummer), "ERROR", "");
            _validationResult.AddRule(_skjema, "4845.1.6.3.3", "", "Tiltakshaver Fødselsnummeret har ikke gyldig kontrollsiffer.", Helpers.GetFullClassPath(() => form.tiltakshaver.foedselsnummer), "ERROR", "");
            _validationResult.AddRule(_skjema, "4845.1.6.4", "", "Organisasjonsnummer for tiltakshaver må fylles ut.", Helpers.GetFullClassPath(() => form.tiltakshaver.organisasjonsnummer), "ERROR", "");
            _validationResult.AddRule(_skjema, "4845.1.6.5", "", "Organisasjonsnummeret ('{0}') for tiltakshaver har ikke gyldig kontrollsiffer.", Helpers.GetFullClassPath(() => form.tiltakshaver.organisasjonsnummer), "ERROR", "");
            _validationResult.AddRule(_skjema, "4845.1.6.6", "", "Organisasjonsnummeret ('{0}') for tiltakshaver er ikke gyldig.", Helpers.GetFullClassPath(() => form.tiltakshaver.organisasjonsnummer), "ERROR", "");
            _validationResult.AddRule(_skjema, "4845.1.6.7", "", "Adresse bør fylles ut for tiltakshaver.", Helpers.GetFullClassPath(() => form.tiltakshaver.adresse), "WARNING", "");
            _validationResult.AddRule(_skjema, "4845.1.6.8", "", "Adresselinje 1 bør fylles ut for tiltakshaver.", Helpers.GetFullClassPath(() => form.tiltakshaver.adresse.adresselinje1), "WARNING", "");
            _validationResult.AddRule(_skjema, "4845.1.6.9", "", "Ugyldig landkode for tiltakshaver.", Helpers.GetFullClassPath(() => form.tiltakshaver.adresse.landkode), "WARNING", "");
            _validationResult.AddRule(_skjema, "4845.1.6.10", "", "Postnummer for tiltakshaver bør angis.", Helpers.GetFullClassPath(() => form.tiltakshaver.adresse.postnr), "WARNING", "");
            _validationResult.AddRule(_skjema, "4845.1.6.11", "", "Postnummeret '{0}' for tiltakshaver har ikke gyldig kontrollsiffer.", Helpers.GetFullClassPath(() => form.tiltakshaver.adresse.postnr), "WARNING", "");
            _validationResult.AddRule(_skjema, "4845.1.6.12", "", "Postnummeret '{0}' for tiltakshaver er ugyldig. Du kan sjekke riktig postnummer på http://adressesok.bring.no/", Helpers.GetFullClassPath(() => form.tiltakshaver.adresse.postnr), "WARNING", "");
            _validationResult.AddRule(_skjema, "4845.1.6.13", "", "Postnummeret '{0}' for tiltakshaver stemmer ikke overens med poststedet '{1}'. Riktig postnummer er '{2}'. Du kan sjekke riktig poststed på http://adressesok.bring.no/", Helpers.GetFullClassPath(() => form.tiltakshaver.adresse.postnr), "WARNING", "");
            _validationResult.AddRule(_skjema, "4845.1.6.14", "", "Postnummeret til tiltakshaver ble ikke validert.", Helpers.GetFullClassPath(() => form.tiltakshaver.adresse.postnr), "WARNING", "");

            _validationResult.AddRule(_skjema, "4845.1.6.15", "", "Navnet til tiltakshaver må fylles ut.", Helpers.GetFullClassPath(() => form.tiltakshaver.navn), "ERROR", "");
            _validationResult.AddRule(_skjema, "4845.1.6.16", "", "Navnet til kontaktperson for tiltakshaver bør fylles ut.", Helpers.GetFullClassPath(() => form.tiltakshaver.kontaktperson.navn), "WARNING", "");
            _validationResult.AddRule(_skjema, "4845.1.6.17", "", "Mobilnummer eller telefonnummer for tiltakshaver bør fylles ut.", Helpers.GetFullClassPath(() => form.tiltakshaver.mobilnummer), "WARNING", "");
            _validationResult.AddRule(_skjema, "4845.1.6.18", "", "E-postadresse for tiltakshaver bør fylles ut.", Helpers.GetFullClassPath(() => form.tiltakshaver.epost), "WARNING", "");

            //Faktura
            _validationResult.AddRule(_skjema, "4845.1.7", "", "Fakturainformasjon må fylles ut.", Helpers.GetFullClassPath(() => form.fakturamottaker), "ERROR", "");
            _validationResult.AddRule(_skjema, "4845.1.7.1", "", "Det må angis om det ønskes faktura som EHF eller på papir.", Helpers.GetFullClassPath(() => form.fakturamottaker), "ERROR", "");
            _validationResult.AddRule(_skjema, "4845.1.7.2", "", "Organisasjonsnummer skal fylles ut for fakturamottaker når man velger papirfaktura.", Helpers.GetFullClassPath(() => form.fakturamottaker.organisasjonsnummer), "ERROR", Helpers.GetFullClassPath(() => form.fakturamottaker.ehfFaktura));
            _validationResult.AddRule(_skjema, "4845.1.7.2.2", "", "Organisasjonsnummeret ('{0}') for fakturamottaker har ikke gyldig kontrollsiffer.", Helpers.GetFullClassPath(() => form.fakturamottaker.organisasjonsnummer), "ERROR", Helpers.GetFullClassPath(() => form.fakturamottaker.ehfFaktura));
            _validationResult.AddRule(_skjema, "4845.1.7.2.1", "", "Organisasjonsnummeret ('{0}') for fakturamottaker er ikke gyldig.", Helpers.GetFullClassPath(() => form.fakturamottaker.organisasjonsnummer), "ERROR", Helpers.GetFullClassPath(() => form.fakturamottaker.ehfFaktura));
            _validationResult.AddRule(_skjema, "4845.1.7.3", "", "Navn skal fylles ut for fakturamottaker når man velger papirfaktura.", Helpers.GetFullClassPath(() => form.fakturamottaker.navn), "ERROR", "");
            //adress
            _validationResult.AddRule(_skjema, "4845.1.7.4", "", "Adresse skal fylles ut for fakturamottaker.", Helpers.GetFullClassPath(() => form.fakturamottaker.adresse), "ERROR", "");
            _validationResult.AddRule(_skjema, "4845.1.7.4.1", "", "Adresselinje 1 skal fylles ut for fakturamottaker.", Helpers.GetFullClassPath(() => form.fakturamottaker.adresse.adresselinje1), "ERROR", "");
            _validationResult.AddRule(_skjema, "4845.1.7.4.2", "", "Postnummer for fakturamottaker.", Helpers.GetFullClassPath(() => form.fakturamottaker.adresse.postnr), "ERROR", "");
            _validationResult.AddRule(_skjema, "4845.1.7.4.3", "", "Poststed for fakturamottaker.", Helpers.GetFullClassPath(() => form.fakturamottaker.adresse.poststed), "ERROR", "");
            _validationResult.AddRule(_skjema, "4845.1.7.4.4", "", "Landkode for fakturamottaker.", Helpers.GetFullClassPath(() => form.fakturamottaker.adresse.landkode), "ERROR", "");

            _validationResult.AddRule(_skjema, "4845.1.7.5", "", "Du bør legg inn referanse opplysninger for å sikre riktig fakturering", Helpers.GetFullClassPath(() => form.fakturamottaker.bestillerReferanse), "WARNING", "");
            _validationResult.AddRule(_skjema, "4845.1.7.6", "", "Du bør legg inn referanse opplysninger for å sikre riktig fakturering", Helpers.GetFullClassPath(() => form.fakturamottaker.fakturareferanser), "WARNING", "");


            //Bygningstype
            _validationResult.AddRule(_skjema, "4845.1.8", "", "Minst en beskrivelse av tiltak skal være registrert", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak), "ERROR", "");
            _validationResult.AddRule(_skjema, "4845.1.8.1", "", "Bygningstype skal fylles ut.", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak[0].bruk.bygningstype.kodeverdi), "ERROR", "");
            _validationResult.AddRule(_skjema, "4845.1.8.2", "", "Ugyldig kodeverdi i henhold til kodeliste for bygningstype. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/arbeidstilsynet/bygningstyper-for-arbeidstilsynet.", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak[0].bruk.bygningstype.kodeverdi), "ERROR", "");
            _validationResult.AddRule(_skjema, "4845.1.8.3", "", "Kodebeskrivelsen '{0}' stemmer ikke med den valgte kodeverdien {1}. Du kan sjekke riktig kodebeskrivelse på  https://register.geonorge.no/arbeidstilsynet/bygningstyper-for-arbeidstilsynet.", Helpers.GetFullClassPath(() => form.beskrivelseAvTiltak[0].bruk.bygningstype.kodebeskrivelse), "WARNING", "");

            //Sakstnumer Validering
            _validationResult.AddRule(_skjema, "4845.1.9", "", "Arbeidstilsynets sakssekvensnummer bør fylles ut.", Helpers.GetFullClassPath(() => form.arbeidstilsynetsSaksnummer.sakssekvensnummer), "WARNING", "");
            _validationResult.AddRule(_skjema, "4845.1.9.1", "", "Arbeidstilsynets saks år bør fylles ut.", Helpers.GetFullClassPath(() => form.arbeidstilsynetsSaksnummer.saksaar), "WARNING", "");


            //Vedleggs Validering
            _validationResult.AddRule(_skjema, "4845.1.10.1", "", "Du bør sende med vedlegget som inneholder signatur fra Arbeidsmiljøutvalg.", "", "WARNING", "Vedlegg");
            _validationResult.AddRule(_skjema, "4845.1.10.2", "", "Du bør sende med vedlegget som inneholder signatur fra Arbeidstakernes representant.", "", "WARNING", "Vedlegg");
            _validationResult.AddRule(_skjema, "4845.1.10.3", "", "Du bør sende med vedlegget som inneholder signatur fra Verneombud.", "", "WARNING", "Vedlegg");
            _validationResult.AddRule(_skjema, "4845.1.10.4", "", "Du bør sende med vedlegget som inneholder signatur fra Leietaker - arbeidsgiver.", "", "WARNING", "Vedlegg");
            _validationResult.AddRule(_skjema, "4845.1.10.5", "", "Du bør sende med vedlegget som inneholder signatur fra Bedriftshelsetjeneste.", "", "WARNING", "Vedlegg");


        }
        internal ValidationResult GetResult() => _validationResult;

        public ValidationResult Validate(ArbeidstilsynetsSamtykkeType form, List<string> formSetElements)
        {
            _logger.Debug("Starts validating form {dataFormatId} - {dataFormatVersion}", form.dataFormatId, form.dataFormatVersion);

            Parallel.Invoke(
                () => { FraSluttbrukersystemErUtfylt(form); },
                () => { EiendomByggestedValidering(form); },
                () => { ArbeidsplasserValidering(form, formSetElements); },
                () => { TiltakshaverValidation(form); },
                () => { FakturamottakerValidation(form); },
                () => { BygningstypeValidation(form); },
                () => { ArbeidstilsynetsSaksnummerValidering(form); },
                () => { VedleggsValidering(form, formSetElements); }
                );

            return _validationResult;
        }

        internal void ArbeidstilsynetsSaksnummerValidering(ArbeidstilsynetsSamtykkeType form)
        {
            if (!Helpers.ObjectIsNullOrEmpty(form.arbeidstilsynetsSaksnummer))
            {
                int sakssekvensnummer = 0;
                if (int.TryParse(form.arbeidstilsynetsSaksnummer.sakssekvensnummer, out sakssekvensnummer))
                {
                    if (sakssekvensnummer == 0)
                    {
                        _validationResult.AddMessage("4845.1.9", null);
                    }
                }
                else
                {
                    _validationResult.AddMessage("4845.1.9", null);
                }

                int saksaar = 0;
                if (int.TryParse(form.arbeidstilsynetsSaksnummer.saksaar, out saksaar))
                {
                    if (saksaar == 0)
                    {
                        _validationResult.AddMessage("4845.1.9.1", null);
                    }
                }
                else
                {
                    _validationResult.AddMessage("4845.1.9.1", null);
                }
            }
        }

        internal void FraSluttbrukersystemErUtfylt(ArbeidstilsynetsSamtykkeType form)
        {
            if (String.IsNullOrEmpty(form.fraSluttbrukersystem))
            {
                _validationResult.AddMessage("4845.1.3", null);
            }
        }

        internal void EiendomByggestedValidering(ArbeidstilsynetsSamtykkeType form)
        {
            if (Helpers.ObjectIsNullOrEmpty(form.eiendomByggested))
            {
                _validationResult.AddMessage("4845.1.4", Helpers.GetFullClassPath(() => form.eiendomByggested), _skjema);
            }
            else
            {
                foreach (EiendomType eiendomType in form.eiendomByggested)
                {
                    if (eiendomType.eiendomsidentifikasjon != null)
                    {
                        var kommunenummer = eiendomType.eiendomsidentifikasjon.kommunenummer;
                        var kommunenummerValideringStatus = MunicipalityValidator.Validate_kommunenummerStatus(kommunenummer);
                        if (kommunenummerValideringStatus.Status != MunicipalityValidation.Ok)
                        {

                            switch (kommunenummerValideringStatus.Status)
                            {
                                case MunicipalityValidation.Empty:
                                    _validationResult.AddMessage("4845.1.4.1", null);
                                    break;
                                case MunicipalityValidation.Invalid:
                                    _validationResult.AddMessage("4845.1.4.2", new[] { kommunenummer });
                                    break;
                                case MunicipalityValidation.Expired:
                                    _validationResult.AddMessage("4845.1.4.3", null);
                                    break;
                            }
                        }
                        else
                        {
                            var gaardsnummer = eiendomType.eiendomsidentifikasjon.gaardsnummer;
                            var bruksnummer = eiendomType.eiendomsidentifikasjon.bruksnummer;
                            if (string.IsNullOrEmpty(gaardsnummer) || string.IsNullOrEmpty(bruksnummer))
                            {
                                _validationResult.AddMessage("4845.1.4.4", null);
                            }
                            else
                            {
                                int gaardsnumerInt = 0;
                                int bruksnummerInt = 0;
                                if (!int.TryParse(gaardsnummer, out gaardsnumerInt) || !int.TryParse(bruksnummer, out bruksnummerInt))
                                {
                                    _validationResult.AddMessage("4845.1.4.5", new[] { eiendomType.eiendomsidentifikasjon.gaardsnummer, eiendomType.eiendomsidentifikasjon.bruksnummer });
                                }
                                else
                                {
                                    int festenummerInt = 0;
                                    int seksjonsnummerInt = 0;
                                    int.TryParse(eiendomType.eiendomsidentifikasjon.festenummer, out festenummerInt);
                                    int.TryParse(eiendomType.eiendomsidentifikasjon.seksjonsnummer, out seksjonsnummerInt);

                                    if (gaardsnumerInt <= 0 || bruksnummerInt <= 0)
                                    {
                                        if (gaardsnumerInt < 0)
                                            _validationResult.AddMessage("4845.1.4.6", new[] { eiendomType.eiendomsidentifikasjon.gaardsnummer });

                                        if (bruksnummerInt < 0)
                                            _validationResult.AddMessage("4845.1.4.6.1", new[] { eiendomType.eiendomsidentifikasjon.bruksnummer });

                                    }
                                    else
                                    {
                                        //## MATRIKKEL
                                        var matrikkelnrExist = new MatrikkelService().MatrikkelnrExist(kommunenummer, gaardsnumerInt, bruksnummerInt, festenummerInt, seksjonsnummerInt);
                                        if (matrikkelnrExist.HasValue)
                                        {
                                            if (!matrikkelnrExist.Value)
                                            {
                                                _validationResult.AddMessage("4845.1.4.12", new[]
                                                { kommunenummer,
                                                           eiendomType.eiendomsidentifikasjon.gaardsnummer,
                                                           eiendomType.eiendomsidentifikasjon.bruksnummer,
                                                           eiendomType.eiendomsidentifikasjon.festenummer,
                                                           eiendomType.eiendomsidentifikasjon.seksjonsnummer,});
                                            }
                                        }
                                        else
                                        {
                                            _validationResult.AddMessage("4845.1.4.13", new[]
                                            {
                                            kommunenummer,
                                            eiendomType.eiendomsidentifikasjon.gaardsnummer,
                                            eiendomType.eiendomsidentifikasjon.bruksnummer,
                                            eiendomType.eiendomsidentifikasjon.festenummer,
                                            eiendomType.eiendomsidentifikasjon.seksjonsnummer,
                                        });
                                        }

                                        if (!String.IsNullOrEmpty(eiendomType.bygningsnummer))
                                        {
                                            long bygningsnrLong = 0;
                                            if (!long.TryParse(eiendomType.bygningsnummer, out bygningsnrLong))
                                            {
                                                _validationResult.AddMessage("4845.1.4.7", new[] { eiendomType.eiendomsidentifikasjon.gaardsnummer, eiendomType.eiendomsidentifikasjon.bruksnummer });
                                            }
                                            else
                                            {
                                                if (bygningsnrLong <= 0)
                                                {
                                                    _validationResult.AddMessage("4845.1.4.8", new[] { eiendomType.eiendomsidentifikasjon.gaardsnummer, eiendomType.eiendomsidentifikasjon.bruksnummer });
                                                }
                                                else
                                                {
                                                    ////## MATRIKKEL
                                                    var isBygningValid = new MatrikkelService().IsBygningOnMatrikkelnr(bygningsnrLong, kommunenummer, gaardsnumerInt, bruksnummerInt, festenummerInt, seksjonsnummerInt);
                                                    if (isBygningValid.HasValue && !isBygningValid.Value)
                                                    {
                                                        _validationResult.AddMessage("4845.1.4.14", new[]
                                                        {
                                                                    kommunenummer,
                                                                    eiendomType.eiendomsidentifikasjon.gaardsnummer,
                                                                    eiendomType.eiendomsidentifikasjon.bruksnummer,
                                                                    eiendomType.eiendomsidentifikasjon.festenummer,
                                                                    eiendomType.eiendomsidentifikasjon.seksjonsnummer,});
                                                    }
                                                }
                                            }
                                        }

                                        if (Helpers.ObjectIsNullOrEmpty(eiendomType.adresse))
                                        {
                                            _validationResult.AddMessage("4845.1.4.9", null);
                                        }
                                        else
                                        {
                                            if (string.IsNullOrEmpty(eiendomType.adresse?.adresselinje1))
                                            {
                                                _validationResult.AddMessage("4845.1.4.9.1", null);
                                            }
                                            else
                                            {
                                                if (String.IsNullOrEmpty(eiendomType.adresse.gatenavn) || String.IsNullOrEmpty(eiendomType.adresse.husnr))
                                                {
                                                    _validationResult.AddMessage("4845.1.4.9.2", null);
                                                }
                                                else
                                                {
                                                    //## MATRIKKEL
                                                    var isAdresseValid = new MatrikkelService().IsVegadresseOnMatrikkelnr(eiendomType.adresse.gatenavn, eiendomType.adresse.husnr, eiendomType.adresse.bokstav, kommunenummer, gaardsnumerInt, bruksnummerInt, festenummerInt, seksjonsnummerInt);
                                                    if (isAdresseValid == null)
                                                    {
                                                        _validationResult.AddMessage("4845.1.4.15", null);
                                                    }
                                                    else if (!isAdresseValid.GetValueOrDefault())
                                                    {
                                                        _validationResult.AddMessage("4845.1.4.15.1", null);
                                                    }
                                                }
                                            }
                                        }

                                    }
                                }
                            }
                        }
                    }

                    if (String.IsNullOrEmpty(eiendomType.kommunenavn))
                    {
                        _validationResult.AddMessage("4845.1.4.10", Helpers.GetFullClassPath(() => eiendomType.kommunenavn), _skjema);
                    }

                    if (!String.IsNullOrEmpty(eiendomType.bolignummer))
                    {
                        if (GeneralValidations.Validate_bruksenhetsnummer(eiendomType.bolignummer).Status == ValidationStatus.Fail)
                        {
                            _validationResult.AddMessage("4845.1.4.11", Helpers.GetFullClassPath(() => eiendomType.bolignummer), _skjema);
                        }
                    }
                }
            }
        }
        internal void ArbeidsplasserValidering(ArbeidstilsynetsSamtykkeType form, List<string> formSetElements)
        {
            if (Helpers.ObjectIsNullOrEmpty(form.arbeidsplasser))
            {
                _validationResult.AddMessage("4845.1.5", null);
            }
            else
            {
                if (!form.arbeidsplasser.eksisterende.GetValueOrDefault(false) && !form.arbeidsplasser.framtidige.GetValueOrDefault(false))
                {
                    _validationResult.AddMessage("4845.1.5.1", null);
                }
                else
                {
                    if (!form.arbeidsplasser.faste.GetValueOrDefault(false) && !form.arbeidsplasser.midlertidige.GetValueOrDefault(false))
                    {
                        _validationResult.AddMessage("4845.1.5.2", null);
                    }

                    if (form.arbeidsplasser.utleieBygg.GetValueOrDefault(false))
                    {
                        int antallVirksomheter;
                        int.TryParse(form.arbeidsplasser.antallVirksomheter, out antallVirksomheter);
                        if (antallVirksomheter <= 0)
                        {
                            _validationResult.AddMessage("4845.1.5.3", null);

                        }
                    }
                    int antallAnsatte;
                    int.TryParse(form.arbeidsplasser.antallAnsatte, out antallAnsatte);
                    if (antallAnsatte <= 0)
                    {
                        _validationResult.AddMessage("4845.1.5.4", null);

                    }

                    if (string.IsNullOrEmpty(form.arbeidsplasser.beskrivelse))
                    {
                        if (!formSetElements.Contains("BeskrivelseTypeArbeidProsess"))
                        {
                            _validationResult.AddMessage("4845.1.5.5", null);
                        }
                    }
                }
            }


        }

        internal void TiltakshaverValidation(ArbeidstilsynetsSamtykkeType form)
        {
            using (var logger = new PerfTimerLogger())
            {
                var tiltakshaver = form.tiltakshaver;
                if (Helpers.ObjectIsNullOrEmpty(tiltakshaver))
                {
                    _validationResult.AddMessage("4845.1.6", null);
                }
                else
                {
                    if (Helpers.ObjectIsNullOrEmpty(tiltakshaver.partstype?.kodeverdi))
                    {
                        _validationResult.AddMessage("4845.1.6.1", null);
                    }
                    else
                    {
                        if (!_codeListService.IsCodelistValid("Partstype", tiltakshaver.partstype?.kodeverdi))
                        {
                            _validationResult.AddMessage("4845.1.6.2", new[] { tiltakshaver.partstype?.kodeverdi });
                        }
                        else
                        {
                            if (tiltakshaver.partstype?.kodeverdi == "Privatperson")
                            {
                                var foedselsnummerValidation = GeneralValidations.Validate_foedselsnummer(tiltakshaver.foedselsnummer);
                                switch (foedselsnummerValidation)
                                {
                                    case GeneralValidations.FoedselnumerValidation.Empty:
                                        _validationResult.AddMessage("4845.1.6.3", null);
                                        break;
                                    case GeneralValidations.FoedselnumerValidation.InvalidEncryption:
                                        _validationResult.AddMessage("4845.1.6.3.1", null);
                                        break;
                                    case GeneralValidations.FoedselnumerValidation.Invalid:
                                        _validationResult.AddMessage("4845.1.6.3.2", null);
                                        break;
                                    case GeneralValidations.FoedselnumerValidation.InvalidDigitsControl:
                                        _validationResult.AddMessage("4845.1.6.3.3", null);
                                        break;
                                }
                            }
                            else
                            {
                                var organisasjonsnummerValidation = GeneralValidations.Validate_OrgnummerEnum(tiltakshaver.organisasjonsnummer);
                                switch (organisasjonsnummerValidation)
                                {
                                    case GeneralValidations.OrganisasjonsnummerValidation.Empty:
                                        _validationResult.AddMessage("4845.1.6.4", null);
                                        break;
                                    case GeneralValidations.OrganisasjonsnummerValidation.InvalidDigitsControl:
                                        _validationResult.AddMessage("4845.1.6.5", new []{ tiltakshaver.organisasjonsnummer });
                                        break;
                                    case GeneralValidations.OrganisasjonsnummerValidation.Invalid:
                                        _validationResult.AddMessage("4845.1.6.6", new []{ tiltakshaver.organisasjonsnummer });
                                        break;
                                }

                                if (Helpers.ObjectIsNullOrEmpty(tiltakshaver.adresse))
                                {
                                    _validationResult.AddMessage("4845.1.6.7", null);
                                }
                                else
                                {
                                    if (String.IsNullOrEmpty(tiltakshaver.adresse.adresselinje1))
                                    {
                                        _validationResult.AddMessage("4845.1.6.8", null);
                                    }
                                    else
                                    {
                                        if (!CountryCodeHandler.IsCountryNorway(tiltakshaver.adresse.landkode))
                                        {
                                            if (!CountryCodeHandler.VerifyCountryCode(tiltakshaver.adresse.landkode))
                                            {
                                                _validationResult.AddMessage("4845.1.6.9", null);
                                            }
                                        }
                                        else
                                        {
                                            var postNr = tiltakshaver.adresse.postnr;
                                            var landkode = tiltakshaver.adresse.landkode;

                                            if (string.IsNullOrEmpty(tiltakshaver.adresse.postnr))
                                            {
                                                _validationResult.AddMessage("4845.1.6.10", null);
                                            }
                                            else
                                            {
                                                if (!GeneralValidations.postNumberMatch(tiltakshaver.adresse.postnr).Success)
                                                {
                                                    _validationResult.AddMessage("4845.1.6.11", new[] { postNr });
                                                }
                                                else
                                                {
                                                    var postnrValidation = new PostalCode.BringPostalCodeProvider().ValidatePostnr(postNr, landkode);
                                                    if (postnrValidation != null)
                                                    {
                                                        if (!postnrValidation.Valid)
                                                        {
                                                            _validationResult.AddMessage("4845.1.6.12", null);
                                                        }
                                                        else
                                                        {
                                                            if (!postnrValidation.Result.Equals(tiltakshaver.adresse.poststed, StringComparison.CurrentCultureIgnoreCase))
                                                            {
                                                                _validationResult.AddMessage("4845.1.6.13", new[] { postNr, tiltakshaver.adresse.poststed, postnrValidation.Result });
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        _validationResult.AddMessage("4845.1.6.14", null);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                if (string.IsNullOrEmpty(tiltakshaver.kontaktperson?.navn))
                                    _validationResult.AddMessage("4845.1.6.16", null);

                                if (String.IsNullOrEmpty(tiltakshaver.navn))
                                    _validationResult.AddMessage("4845.1.6.15", null);

                                if (string.IsNullOrEmpty(tiltakshaver.mobilnummer) && string.IsNullOrEmpty(tiltakshaver.telefonnummer))
                                    _validationResult.AddMessage("4845.1.6.17", null);

                                if (string.IsNullOrEmpty(tiltakshaver.epost))
                                    _validationResult.AddMessage("4845.1.6.18", null);
                            }


                        }
                    }
                }
            }
        }

        internal void FakturamottakerValidation(ArbeidstilsynetsSamtykkeType form)
        {
            if (Helpers.ObjectIsNullOrEmpty(form.fakturamottaker))
            {
                _validationResult.AddMessage("4845.1.7", null);
            }
            else
            {
                var booleans = new[]
                {
                    form.fakturamottaker.ehfFaktura.GetValueOrDefault(),
                    form.fakturamottaker.fakturaPapir.GetValueOrDefault(),
                };
                var trueCount = booleans.Count(c => c);
                if (trueCount != 1)
                {
                    _validationResult.AddMessage("4845.1.7.1", null);
                }
                else
                {
                    // Must have org num if electronic billing
                    if (form.fakturamottaker?.ehfFaktura == true)
                    {
                        var organisasjonsnummerValidation = GeneralValidations.Validate_OrgnummerEnum(form.fakturamottaker?.organisasjonsnummer);
                        switch (organisasjonsnummerValidation)
                        {
                            case GeneralValidations.OrganisasjonsnummerValidation.Empty:
                                _validationResult.AddMessage("4845.1.7.2", null);
                                break;
                            case GeneralValidations.OrganisasjonsnummerValidation.InvalidDigitsControl:
                                _validationResult.AddMessage("4845.1.7.2.2", new[] { form.fakturamottaker?.organisasjonsnummer });
                                break;
                            case GeneralValidations.OrganisasjonsnummerValidation.Invalid:
                                _validationResult.AddMessage("4845.1.7.2.1", new []{ form.fakturamottaker?.organisasjonsnummer });
                                break;
                        }
                    }

                    if (form.fakturamottaker.fakturaPapir.GetValueOrDefault())
                    {
                        if (string.IsNullOrEmpty(form.fakturamottaker?.navn))
                            _validationResult.AddMessage("4845.1.7.3", null);
                    }

                    if (Helpers.ObjectIsNullOrEmpty(form.fakturamottaker?.adresse))
                    {
                        _validationResult.AddMessage("4845.1.7.4", null);
                    }
                    else
                    {
                        if (String.IsNullOrEmpty(form.fakturamottaker.adresse.adresselinje1))
                            _validationResult.AddMessage("4845.1.7.4.1", null);
                        if (string.IsNullOrEmpty(form.fakturamottaker.adresse.postnr))
                            _validationResult.AddMessage("4845.1.7.4.2", null);
                        if (string.IsNullOrEmpty(form.fakturamottaker.adresse.poststed))
                            _validationResult.AddMessage("4845.1.7.4.3", null);
                        if (string.IsNullOrEmpty(form.fakturamottaker?.adresse.landkode))
                            _validationResult.AddMessage("4845.1.7.4.4", null);
                    }

                    if (string.IsNullOrEmpty(form.fakturamottaker.bestillerReferanse))
                        _validationResult.AddMessage("4845.1.7.5", null);
                    if (string.IsNullOrEmpty(form.fakturamottaker.fakturareferanser))
                        _validationResult.AddMessage("4845.1.7.6", null);
                }
            }
        }

        internal void BygningstypeValidation(ArbeidstilsynetsSamtykkeType form)
        {
            if (Helpers.ObjectIsNullOrEmpty(form.beskrivelseAvTiltak))
            {
                _validationResult.AddMessage("4845.1.8", null);
            }
            else
            {
                if (form.beskrivelseAvTiltak.All(t => string.IsNullOrEmpty(t.bruk?.bygningstype?.kodeverdi)))
                {
                    _validationResult.AddMessage("4845.1.8.1", null);
                }
                else
                {
                    if (!form.beskrivelseAvTiltak.All(t => _codeListService.IsCodelistValid("bygningstyper-for-arbeidstilsynet", t.bruk?.bygningstype?.kodeverdi, RegistryType.Arbeidstilsynet)))
                    {
                        _validationResult.AddMessage("4845.1.8.2", null);
                    }
                    else
                    {
                        foreach (TiltakType tiltakType in form.beskrivelseAvTiltak)
                        {
                            if (!_codeListService.IsCodelistLabelValid("bygningstyper-for-arbeidstilsynet", tiltakType.bruk?.bygningstype?.kodeverdi, tiltakType.bruk?.bygningstype?.kodebeskrivelse, RegistryType.Arbeidstilsynet))
                            {
                                _validationResult.AddMessage("4845.1.8.3", new[] { tiltakType.bruk?.bygningstype?.kodebeskrivelse, tiltakType.bruk?.bygningstype?.kodeverdi });
                            }
                        }
                    }
                }
            }
        }


        internal void VedleggsValidering(ArbeidstilsynetsSamtykkeType form, List<string> formSetElements)
        {

            if (!formSetElements.Contains("Arbeidsmiljoutvalg"))
            {
                _validationResult.AddMessage("4845.1.10.1", null);
            }

            if (!formSetElements.Contains("ArbeidstakersRepresentant"))
            {
                _validationResult.AddMessage("4845.1.10.2", null);
            }

            if (!formSetElements.Contains("Verneombud"))
            {
                _validationResult.AddMessage("4845.1.10.3", null);
            }

            if (!formSetElements.Contains("LeietakerArbeidsgiver"))
            {
                _validationResult.AddMessage("4845.1.10.4", null);
            }

            if (!formSetElements.Contains("Bedriftshelsetjeneste"))
            {
                _validationResult.AddMessage("4845.1.10.5", null);
            }
        }

    }
}
