﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using DIBK.FBygg.Altinn.MapperCodelist;
using KommIT.FIKS.AdapterAltinnSvarUt.Resources;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.PostalCode;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Validations;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;
using no.kxml.skjema.dibk.midlertidigbrukstillatelseV2;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.SvarUtForm
{
    public class MidlertidigBrukstillatelseFormV2Validator
    {
        CodeListService _codeListService = new CodeListService();

        private readonly ValidationResult _validationResult;
        private List<string> _tiltakstyperISoeknad = new List<string>();

        internal ValidationResult GetResult() => _validationResult;
        public IMunicipalityValidator MunicipalityValidator { get; set; }
        public IPostalCodeProvider PostalCodeProvider { get; set; }

        public MidlertidigBrukstillatelseFormV2Validator()
        {
            MunicipalityValidator = new MunicipalityValidator();
            PostalCodeProvider = new BringPostalCodeProvider();

            var skjema = "MidlertidigBrukstillatelse";
            _validationResult = new ValidationResult();
            var form = new MidlertidigBrukstillatelseType();
            //AnsvarValidering
            _validationResult.AddRule(skjema, "4399.2.8", null, "Gjennomføringsplan (eller Søknad om ansvarsrett for selvbygger) skal følge med søknaden", "/Vedlegg", "WARNING", null);
            //DatoferdigattestValidering
            _validationResult.AddRule(skjema, "4399.2.10", null, "Dato for søknad om ferdigattest må angis", Helpers.GetFullClassPath(() => form.datoFerdigattest), "ERROR", null);

            //SoknadsDelerAvTiltakenValidering
            _validationResult.AddRule(skjema, "4399.2.11", null, "Når det søkes om deler av tiltaket må beskrivelse av del fylles ut", Helpers.GetFullClassPath(() => form.delAvTiltaket), "ERROR", null);
            _validationResult.AddRule(skjema, "4399.2.12", null, "Når det søkes om deler av tiltaket må resterende del fylles ut", Helpers.GetFullClassPath(() => form.gjenstaaendeUtenfor), "ERROR", null);

            //EiendomByggestedValidation
            _validationResult.AddRule(skjema, "4399.1.1", null, "kommunenummer må fylles ut for eiendom/byggested", Helpers.GetFullClassPath(() => form.eiendomByggested[0].eiendomsidentifikasjon.kommunenummer), "ERROR", null);
            _validationResult.AddRule(skjema, "4399.1.1.1", null, "Kommunenummeret '{0}' for eiendom/byggested finnes ikke i kodelisten. Du kan sjekke riktig kommunenummer på https://register.geonorge.no/sosi-kodelister/inndelinger/inndelingsbase/kommunenummer", Helpers.GetFullClassPath(() => form.eiendomByggested[0].eiendomsidentifikasjon.kommunenummer), "ERROR", null);
            _validationResult.AddRule(skjema, "4399.1.1.2", null, "Kommunenummeret '{0}' for eiendom/byggested har ugyldig status ({1}). Du kan sjekke status på https://register.geonorge.no/sosi-kodelister/inndelinger/inndelingsbase/kommunenummer", Helpers.GetFullClassPath(() => form.eiendomByggested[0].eiendomsidentifikasjon.kommunenummer), "ERROR", null);
            _validationResult.AddRule(skjema, "4399.1.1.3", null, "Kommunenavn bør fylles ut for eiendom/byggested", Helpers.GetFullClassPath(() => form.eiendomByggested[0].kommunenavn), "WARNING", null);
            _validationResult.AddRule(skjema, "4399.2.17", null, "Hvis bruksenhetsnummer(bolignummer) er fylt ut må det følge riktig format (^[HKLU]\\d{4}$) (feks H0101) se https://www.kartverket.no/eiendom/adressering/bruksenhetsnummer/", Helpers.GetFullClassPath(() => form.eiendomByggested[0].bygningsnummer), "ERROR", null);

            //HarTilstrekkeligSikkerhetValidering
            _validationResult.AddRule(skjema, "4399.2.13", null, "Når tiltaket ikke har tilstrekkelig sikkerhet for brukstillatelse må utført innen, type arbeider og bekreftelse innen fylles ut.", Helpers.GetFullClassPath(() => form), "ERROR", Helpers.GetFullClassPath(() => form.harTilstrekkeligSikkerhet));
            _validationResult.AddRule(skjema, "4399.2.14", null, "Følgende arbeider vil bli utført innen kan være maks om 14 dager", Helpers.GetFullClassPath(() => form.utfoertInnen), "ERROR", null);
            _validationResult.AddRule(skjema, "4399.2.15", null, "Bekreftelse på at disse arbeidene er utført vil være kommunen i hende innen kan være maks om 14 dager.", Helpers.GetFullClassPath(() => form.bekreftelseInnen), "ERROR", null);

            //KommunensSaksnummerValidator
            _validationResult.AddRule(skjema, "4399.2.16", null, "Kommunens saksnummer må være fylt ut.", Helpers.GetFullClassPath(() => form.kommunensSaksnummer), "ERROR", null);

            //søker validering
            _validationResult.AddRule(skjema, "4399.1.1.4", null, "Du har ikke fylt ut informasjon om søker. Hvis du søker med ansvarsrett, må du fylle ut informasjon om ansvarlig søker. For tiltak uten ansvarsrett, må du fylle ut informasjon om tiltakshaver.", Helpers.GetFullClassPath(() => form), "ERROR", null);

            //Tiltakshaver
            _validationResult.AddRule(skjema, "4399.2.5.1", null, "Kodeverdien for 'partstype' til tiltakshaver må fylles ut.", Helpers.GetFullClassPath(() => form.tiltakshaver.partstype.kodeverdi), "ERROR", null);
            _validationResult.AddRule(skjema, "4399.2.5.1.1", null, "'{0}' er en ugyldig kodeverdi for partstypen til tiltakshaver. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/byggesoknad/partstype", Helpers.GetFullClassPath(() => form.tiltakshaver.partstype.kodeverdi), "ERROR", null);
            //Tiltakshaver foedselsnummer
            _validationResult.AddRule(skjema, "4399.2.5.2", null, "Fødselsnummer må fylles ut når tiltakshaver er en privatperson.", Helpers.GetFullClassPath(() => form.tiltakshaver.foedselsnummer), "ERROR", null);
            _validationResult.AddRule(skjema, "4399.2.5.2.1", null, "Fødselsnummeret til tiltakshaver er ikke gyldig.", Helpers.GetFullClassPath(() => form.tiltakshaver.foedselsnummer), "ERROR", null);
            _validationResult.AddRule(skjema, "4399.2.5.2.2", null, "Fødselsnummeret til tiltakshaver har ikke gyldig kontrollsiffer.", Helpers.GetFullClassPath(() => form.tiltakshaver.foedselsnummer), "ERROR", null);
            _validationResult.AddRule(skjema, "4399.2.5.2.3", null, "Fødselsnummeret til tiltakshaver kan ikke dekrypteres.", Helpers.GetFullClassPath(() => form.tiltakshaver.foedselsnummer), "ERROR", null);
            //Tiltakshaver organisasjonsnummer      
            _validationResult.AddRule(skjema, "4399.2.5.3", null, "Organisasjonsnummeret til tiltakshaver må fylles ut når tiltakshaver er en organisasjon.", Helpers.GetFullClassPath(() => form.tiltakshaver.organisasjonsnummer), "ERROR", null);
            _validationResult.AddRule(skjema, "4399.2.5.3.1", null, "Organisasjonsnummeret ('{0}') til tiltakshaver har ikke gyldig kontrollsiffer.", Helpers.GetFullClassPath(() => form.tiltakshaver.organisasjonsnummer), "ERROR", null);
            _validationResult.AddRule(skjema, "4399.2.5.3.2", null, "Organisasjonsnummeret ('{0}') til tiltakshaver er ikke gyldig.", Helpers.GetFullClassPath(() => form.tiltakshaver.organisasjonsnummer), "ERROR", null);
            _validationResult.AddRule(skjema, "4399.2.5.3.3", null, "Kontaktperson bør fylles ut når tiltakshaver er en organisasjon.", Helpers.GetFullClassPath(() => form.tiltakshaver.kontaktperson), "WARNING", null);
            //tiltakshaver adresse
            _validationResult.AddRule(skjema, "4399.2.5.4", null, "Adressen til tiltakshaver bør fylles ut.", Helpers.GetFullClassPath(() => form.tiltakshaver.adresse), "WARNING", null);
            _validationResult.AddRule(skjema, "4399.2.5.5", null, "Telefonnummeret eller mobilnummeret til tiltakshaver må fylles ut.", Helpers.GetFullClassPath(() => form.tiltakshaver.mobilnummer), "WARNING", Helpers.GetFullClassPath(() => form.tiltakshaver.telefonnummer));
            _validationResult.AddRule(skjema, "4399.2.5.6", null, "Navnet til tiltakshaver må fylles ut.", Helpers.GetFullClassPath(() => form.tiltakshaver.navn), "ERROR", null);

            //Ansvarlig søker
            _validationResult.AddRule(skjema, "4399.1.1.5", null, "Kodeverdien for 'partstype' til ansvarlig søker må fylles ut.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.partstype.kodeverdi), "ERROR", null);
            _validationResult.AddRule(skjema, "4399.1.2", null, "'{0}' er en ugyldig kodeverdi for partstypen til ansvarlig søker. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/byggesoknad/partstype", Helpers.GetFullClassPath(() => form.ansvarligSoeker.partstype.kodeverdi), "ERROR", null);

            //Ansvarlig søker foedselsnummer
            _validationResult.AddRule(skjema, "4399.2.4", null, "Fødselsnummer må fylles ut når ansvarlig søker er en privatperson.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.foedselsnummer), "ERROR", null);
            _validationResult.AddRule(skjema, "4399.2.4.1", null, "Fødselsnummeret til ansvarlig søker er ikke gyldig.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.foedselsnummer), "ERROR", null);
            _validationResult.AddRule(skjema, "4399.2.4.2", null, "Fødselsnummeret til ansvarlig søker har ikke gyldig kontrollsiffer.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.foedselsnummer), "ERROR", null);
            _validationResult.AddRule(skjema, "4399.2.4.3", null, "Fødselsnummeret til ansvarlig søker kan ikke dekrypteres.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.foedselsnummer), "ERROR", null);
            //Ansvarlig søker organisasjonsnummer      
            _validationResult.AddRule(skjema, "4399.2.4.5", null, "Organisasjonsnummeret til ansvarlig søker må fylles ut.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.organisasjonsnummer), "ERROR", null);
            _validationResult.AddRule(skjema, "4399.2.4.6", null, "Organisasjonsnummeret ('{0}') til ansvarlig søker har ikke gyldig kontrollsiffer.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.organisasjonsnummer), "ERROR", null);
            _validationResult.AddRule(skjema, "4399.2.4.7", null, "Organisasjonsnummeret ('{0}') til ansvarlig søker er ikke gyldig.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.organisasjonsnummer), "ERROR", null);
            _validationResult.AddRule(skjema, "4399.2.18", null, "Kontaktpersonen til ansvarlig søker bør fylles ut.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.kontaktperson), "WARNING", null);
            //Ansvarlig søker adresse
            _validationResult.AddRule(skjema, "4399.2.4.8", null, "Adressen til ansvarlig søker bør fylles ut.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.adresse), "WARNING", null);
            _validationResult.AddRule(skjema, "4399.2.4.8.1", null, "Adresselinje 1 bør fylles ut for ansvarlig søker.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.adresse.adresselinje1), "WARNING", null);
            _validationResult.AddRule(skjema, "4399.2.4.8.2", null, "Ugyldig landkode for ansvarlig søker.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.adresse.landkode), "WARNING", null);
            _validationResult.AddRule(skjema, "4399.2.4.10", null, "Postnummeret til ansvarlig søker bør angis.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.adresse.postnr), "WARNING", null);
            _validationResult.AddRule(skjema, "4399.2.4.11", null, "Postnummeret '{0}' til ansvarlig søker har ikke gyldig kontrollsiffer.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.adresse.postnr), "WARNING", null);
            _validationResult.AddRule(skjema, "4399.2.4.10.1", null, "Postnummeret '{0}' til ansvarlig søker er ugyldig. Du kan sjekke riktig postnummer på http://adressesok.bring.no/", Helpers.GetFullClassPath(() => form.ansvarligSoeker.adresse.postnr), "WARNING", null);
            _validationResult.AddRule(skjema, "4399.2.4.9", null, "Postnummeret '{0}' til ansvarlig søker stemmer ikke overens med poststedet '{1}'. Riktig postnummer er '{2}'. Du kan sjekke riktig poststed på http://adressesok.bring.no/", Helpers.GetFullClassPath(() => form.ansvarligSoeker.adresse.postnr), "WARNING", null);
            _validationResult.AddRule(skjema, "4399.2.4.10.3", null, "Postnummeret '{0}' til ansvarlig søker ble ikke validert. Postnummeret kan være riktig, men en teknisk feil gjør at vi ikke kan bekrefte det.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.adresse.postnr), "WARNING", null);
            _validationResult.AddRule(skjema, "4399.2.19", null, "Telefonnummeret eller mobilnummeret til ansvarlig søker bør fylles ut.", Helpers.GetFullClassPath(() => form.ansvarligSoeker.mobilnummer), "WARNING", Helpers.GetFullClassPath(() => form.ansvarligSoeker.telefonnummer));
        }

        public ValidationResult Validate(MidlertidigBrukstillatelseType form, List<string> formSetElements)
        {
            Parallel.Invoke(
                () => { AnsvarValidering(form,formSetElements); },
                () => { DatoferdigattestValidering(form); },
                () => { SoknadsDelerAvTiltakenValidering(form); },
                () => { EiendomByggestedValidation(form); },
                () => { HarTilstrekkeligSikkerhetValidering(form); },
                () => { KommunensSaksnummerValidator(form); },
                () => { SoekerValidering(form); }
                );

            return _validationResult;
        }

        internal void SoekerValidering(MidlertidigBrukstillatelseType form)
        {
            if (Helpers.ObjectIsNullOrEmpty(form.ansvarligSoeker) && Helpers.ObjectIsNullOrEmpty(form.tiltakshaver))
            {
                _validationResult.AddMessage("4399.1.1.4", null);
            }
            else
            {
                if (Helpers.ObjectIsNullOrEmpty(form.ansvarligSoeker))
                {
                    if (Helpers.ObjectIsNullOrEmpty(form.tiltakshaver.partstype))
                    {
                        _validationResult.AddMessage("4399.2.5.1", null);
                    }
                    else
                    {
                        if (!_codeListService.IsCodelistValid("Partstype", form.tiltakshaver.partstype?.kodeverdi))
                        {
                            _validationResult.AddMessage("4399.2.5.1.1", new[] { form.tiltakshaver.partstype?.kodeverdi });
                        }
                        else
                        {
                            if (form.tiltakshaver.partstype?.kodeverdi == "Privatperson")
                            {
                                var foedselsnummerValidation = GeneralValidations.Validate_foedselsnummer(form.tiltakshaver.foedselsnummer);
                                switch (foedselsnummerValidation)
                                {
                                    case GeneralValidations.FoedselnumerValidation.Empty:
                                        _validationResult.AddMessage("4399.2.5.2", null);
                                        break;
                                    case GeneralValidations.FoedselnumerValidation.Invalid:
                                        _validationResult.AddMessage("4399.2.5.2.1", null);
                                        break;
                                    case GeneralValidations.FoedselnumerValidation.InvalidDigitsControl:
                                        _validationResult.AddMessage("4399.2.5.2.2", null);
                                        break;
                                    case GeneralValidations.FoedselnumerValidation.InvalidEncryption:
                                        _validationResult.AddMessage("4399.2.5.2.3", null);
                                        break;
                                }
                            }
                            else
                            {
                                var organisasjonsnummerValidation = GeneralValidations.Validate_OrgnummerEnum(form.tiltakshaver.organisasjonsnummer);
                                switch (organisasjonsnummerValidation)
                                {
                                    case GeneralValidations.OrganisasjonsnummerValidation.Empty:
                                        _validationResult.AddMessage("4399.2.5.3", null);
                                        break;
                                    case GeneralValidations.OrganisasjonsnummerValidation.Invalid:
                                        _validationResult.AddMessage("4399.2.5.3.1", new[] { form.tiltakshaver.organisasjonsnummer });
                                        break;
                                    case GeneralValidations.OrganisasjonsnummerValidation.InvalidDigitsControl:
                                        _validationResult.AddMessage("4399.2.5.3.2", new[] { form.tiltakshaver.organisasjonsnummer });
                                        break;
                                }
                                if (Helpers.ObjectIsNullOrEmpty(form.tiltakshaver.kontaktperson))
                                {
                                    _validationResult.AddMessage("4399.2.5.3.3", null);
                                }
                            }
                        }

                    }
                    if (Helpers.ObjectIsNullOrEmpty(form.tiltakshaver.adresse))
                        _validationResult.AddMessage("4399.2.5.4", null);

                    if (string.IsNullOrEmpty(form.tiltakshaver.telefonnummer) && string.IsNullOrEmpty(form.tiltakshaver.mobilnummer))
                        _validationResult.AddMessage("4399.2.5.5", null);

                    if (string.IsNullOrEmpty(form.tiltakshaver.navn))
                        _validationResult.AddMessage("4399.2.5.6", null);

                }
                else
                {
                    if (form.ansvarligSoeker.partstype == null)
                    {
                        _validationResult.AddMessage("4399.1.1.5", null);
                    }
                    else
                    {
                        if (!_codeListService.IsCodelistValid("Partstype", form.ansvarligSoeker.partstype.kodeverdi))
                        {
                            _validationResult.AddMessage("4399.1.2", new[] { form.ansvarligSoeker.partstype.kodeverdi });
                        }
                        else
                        {

                            if (form.ansvarligSoeker.partstype.kodeverdi == "Privatperson")
                            {
                                var foedselsnummerValidation = GeneralValidations.Validate_foedselsnummer(form.ansvarligSoeker.foedselsnummer);
                                switch (foedselsnummerValidation)
                                {
                                    case GeneralValidations.FoedselnumerValidation.Empty:
                                        _validationResult.AddMessage("4399.2.4", null);
                                        break;
                                    case GeneralValidations.FoedselnumerValidation.Invalid:
                                        _validationResult.AddMessage("4399.2.4.1", null);
                                        break;
                                    case GeneralValidations.FoedselnumerValidation.InvalidDigitsControl:
                                        _validationResult.AddMessage("4399.2.4.2", null);
                                        break;
                                    case GeneralValidations.FoedselnumerValidation.InvalidEncryption:
                                        _validationResult.AddMessage("4399.2.4.3", null);
                                        break;
                                }
                            }
                            else
                            {
                                var organisasjonsnummerValidation = GeneralValidations.Validate_OrgnummerEnum(form.ansvarligSoeker.organisasjonsnummer);
                                switch (organisasjonsnummerValidation)
                                {
                                    case GeneralValidations.OrganisasjonsnummerValidation.Empty:
                                        _validationResult.AddMessage("4399.2.4.5", null);
                                        break;
                                    case GeneralValidations.OrganisasjonsnummerValidation.InvalidDigitsControl:
                                        _validationResult.AddMessage("4399.2.4.6", new[] { form.ansvarligSoeker.organisasjonsnummer });
                                        break;
                                    case GeneralValidations.OrganisasjonsnummerValidation.Invalid:
                                        _validationResult.AddMessage("4399.2.4.7", new[] { form.ansvarligSoeker.organisasjonsnummer });
                                        break;
                                }

                                if (string.IsNullOrEmpty(form.ansvarligSoeker.kontaktperson))
                                {
                                    _validationResult.AddMessage("4399.2.18", null);
                                }
                            }
                        }
                    }
                    if (Helpers.ObjectIsNullOrEmpty(form.ansvarligSoeker.adresse))
                    {
                        _validationResult.AddMessage("4399.2.4.8", null);
                    }
                    else
                    {
                        if (String.IsNullOrEmpty(form.ansvarligSoeker.adresse.adresselinje1))
                        {
                            _validationResult.AddMessage("4399.2.4.8.1", null);
                        }
                        else
                        {
                            if (!CountryCodeHandler.IsCountryNorway(form.ansvarligSoeker.adresse.landkode))
                            {
                                if (!CountryCodeHandler.VerifyCountryCode(form.ansvarligSoeker.adresse.landkode))
                                {
                                    _validationResult.AddMessage("4399.2.4.8.2", null);
                                }
                            }
                            else
                            {
                                var postNr = form.ansvarligSoeker.adresse.postnr;
                                var landkode = form.ansvarligSoeker.adresse.landkode;

                                if (string.IsNullOrEmpty(form.ansvarligSoeker.adresse.postnr))
                                {
                                    _validationResult.AddMessage("4399.2.4.10", null);
                                }
                                else
                                {
                                    if (!GeneralValidations.postNumberMatch(form.ansvarligSoeker.adresse.postnr).Success)
                                    {
                                        _validationResult.AddMessage("4399.2.4.11", new[] { postNr });
                                    }
                                    else
                                    {
                                        var postnrValidation = PostalCodeProvider.ValidatePostnr(postNr, landkode);
                                        if (!Helpers.ObjectIsNullOrEmpty(postnrValidation))
                                        {
                                            if (!postnrValidation.Valid)
                                            {
                                                _validationResult.AddMessage("4399.2.4.10.1", null);
                                            }
                                            else
                                            {
                                                if (!postnrValidation.Result.Equals(form.ansvarligSoeker.adresse.poststed, StringComparison.CurrentCultureIgnoreCase))
                                                {
                                                    _validationResult.AddMessage("4399.2.4.9", new[] { postNr, form.ansvarligSoeker.adresse.poststed, postnrValidation.Result });
                                                }
                                            }
                                        }
                                        else
                                        {
                                            _validationResult.AddMessage("4399.2.4.10.3", null);
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if (string.IsNullOrEmpty(form.ansvarligSoeker.mobilnummer) && string.IsNullOrEmpty(form.ansvarligSoeker.telefonnummer))
                    {
                        _validationResult.AddMessage("4399.2.19", null);
                    }
                }
            }
        }

        internal void AnsvarValidering(MidlertidigBrukstillatelseType form, List<string> formSetElements)
        {
            var tengningen = new List<string>()
            {
                "Søknad om ansvarsrett for selvbygger",
                "Gjennomføringsplan",
                "Gjennomfoeringsplan",
                "GjennomføringsplanV4"
            };

            bool hasPlan = formSetElements?.Select(x => x)
                .Intersect(tengningen)
                .Any() ?? false;

            if (!hasPlan && !Helpers.ObjectIsNullOrEmpty(form.ansvarligSoeker))
            {
                _validationResult.AddMessage("4399.2.8", null);
            }
        }

        internal void DatoferdigattestValidering(MidlertidigBrukstillatelseType form)
        {
            if (DateTime.Equals(new DateTime(01, 01, 01), form.datoFerdigattest))
            {
                _validationResult.AddMessage("4399.2.10", null);
            }
        }

        internal void SoknadsDelerAvTiltakenValidering(MidlertidigBrukstillatelseType form)
        {
            if (!form.gjelderHeleTiltaket)
            {

                if (string.IsNullOrEmpty(form.delAvTiltaket))
                {
                    _validationResult.AddMessage("4399.2.11", null);
                }
                if (string.IsNullOrEmpty(form.gjenstaaendeUtenfor))
                {
                    _validationResult.AddMessage("4399.2.12", null);
                }
            }
        }

        internal void HarTilstrekkeligSikkerhetValidering(MidlertidigBrukstillatelseType form)
        {
            //TODO change 
            if (form.harTilstrekkeligSikkerhetSpecified)
            {
                if (form.harTilstrekkeligSikkerhet == false)
                {
                    if (!TiltaketIkkeHarTilstrekkelig(form))
                    {
                        _validationResult.AddMessage("4399.2.13", null);
                    }
                    if (form.utfoertInnenSpecified)
                    {
                        if (form.utfoertInnen != null)
                        {
                            if ((form.utfoertInnen.Value - DateTime.Now).TotalDays > 14)
                                _validationResult.AddMessage("4399.2.14", null);
                        }
                    }

                    if (form.bekreftelseInnenSpecified)
                    {
                        if (form.bekreftelseInnen != null)
                        {
                            if ((form.bekreftelseInnen.Value - DateTime.Now).TotalDays > 14)
                                _validationResult.AddMessage("4399.2.15", null);
                        }
                    }
                }
            }

        }

        internal bool TiltaketIkkeHarTilstrekkelig(MidlertidigBrukstillatelseType form)
        {
            if (string.IsNullOrEmpty(form.typeArbeider)) return false;
            if (string.IsNullOrEmpty(form.utfoertInnen.ToString())) return false;
            if (string.IsNullOrEmpty(form.bekreftelseInnen.ToString())) return false;
            return true;
        }


        internal void EiendomByggestedValidation(MidlertidigBrukstillatelseType form)
        {
            if (MinstEnEiendom(form))
            {
                foreach (var eiendom in form.eiendomByggested)
                {
                    if (eiendom.eiendomsidentifikasjon != null)
                    {
                        string kommunenummer = eiendom.eiendomsidentifikasjon.kommunenummer;
                        var kommunenummerValideringStatus = MunicipalityValidator.Validate_kommunenummerStatus(kommunenummer);
                        if (kommunenummerValideringStatus.Status != MunicipalityValidation.Ok)
                        {
                            switch (kommunenummerValideringStatus.Status)
                            {
                                case MunicipalityValidation.Empty:
                                    _validationResult.AddMessage("4399.1.1", null);
                                    break;
                                case MunicipalityValidation.Invalid:
                                    _validationResult.AddMessage("4399.1.1.1", new[] { kommunenummer });
                                    break;
                                case MunicipalityValidation.Expired:
                                    _validationResult.AddMessage("4399.1.1.2", new[] { kommunenummer, kommunenummerValideringStatus.Status.ToString() });
                                    break;
                            }
                        }
                    }

                    if (String.IsNullOrEmpty(eiendom.kommunenavn))
                    {
                        _validationResult.AddMessage("4399.1.1.3", null);
                    }

                    if (!String.IsNullOrEmpty(eiendom.bolignummer))
                    {
                        if (GeneralValidations.Validate_bruksenhetsnummer(eiendom.bolignummer).Status == ValidationStatus.Fail)
                        {
                            _validationResult.AddMessage("4399.2.17", null);
                        }
                    }
                }

            }
        }

        internal bool MinstEnEiendom(MidlertidigBrukstillatelseType form)
        {
            if (form.eiendomByggested == null || !form.eiendomByggested.Any())
                _validationResult.AddError(ValidationError.MinstEnEiendom, "4399.1.1",
                    "/MidlertidigBrukstillatelse/eiendomByggested/eiendom");

            return form.eiendomByggested != null && (form.eiendomByggested != null || form.eiendomByggested.Any());
        }

        internal void GyldigKommuneNrEiendom(string kommunenummer)
        {
            GeneralValidationResult generalValidationResult = MunicipalityValidator.ValidateKommunenummer(kommunenummer);
            if (generalValidationResult.Status != ValidationStatus.Ok)
            {
                _validationResult.AddError($"{generalValidationResult.Message}", "4399.1.1", "/MidlertidigBrukstillatelse/eiendomByggested/eiendom[]/eiendomsidentifikasjon/kommunenummer");
            }
            var kommunenummerValideringStatus = MunicipalityValidator.Validate_kommunenummerStatus(kommunenummer);
            if (kommunenummerValideringStatus.Status != MunicipalityValidation.Ok)
            {
                switch (kommunenummerValideringStatus.Status)
                {
                    case MunicipalityValidation.Empty:
                        _validationResult.AddMessage("4399.1.1", null);
                        break;
                    case MunicipalityValidation.Invalid:
                        _validationResult.AddMessage("4399.1.1.1", new[] { kommunenummer });
                        break;
                    case MunicipalityValidation.Expired:
                        _validationResult.AddMessage("4399.1.1.2", new[] { kommunenummer, kommunenummerValideringStatus.Status.ToString() });
                        break;
                }
            }



        }

        internal void KommunensSaksnummerValidator(MidlertidigBrukstillatelseType form)
        {
            if (form.kommunensSaksnummer == null || string.IsNullOrEmpty(form.kommunensSaksnummer.saksaar) || string.IsNullOrEmpty(form.kommunensSaksnummer.sakssekvensnummer))
            {
                _validationResult.AddMessage("4399.2.16", null);
            }
        }
    }

}