using iTextSharp.text;
using iTextSharp.text.pdf;
using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Altinn;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Notification;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.NotificationForm.Nabovarsel;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Svarut.REST;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Svarut.StatusService;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.UserNotification;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Validations;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.WS.AltinnCorrespondenceAgency;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.WS.AltinnPreFill;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;
using Serilog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Threading;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Prefill
{
    public class SendPrefillFormServiceV2
    {
        private readonly IAltinnPrefillService _altinnPrefillService;
        private readonly ILogEntryService _logEntryService;
        private readonly IFormMetadataService _formMetadataService;
        private static IAltinnCorrespondenceService _corresponcanceService;
        private static string _altinnServer;

        private readonly SvarUtRestService _svarUtService;
        private readonly SvarUtRestBuilderPrint _svarUtRestPrintBuilder;

        private readonly UserNotificationHandler _userNotificationHandler;

        private readonly ISyncRecordsOfCombinedDistributions _syncRecordsOfCombinedDistributions;
        private readonly ISvarUtStatusService _svarUtStatusService;
        private readonly ApplicationDbContext _dbContext;
        private readonly ILogger _log = Log.ForContext<SendPrefillFormServiceV2>();

        public SendPrefillFormServiceV2(IAltinnCorrespondenceService corresponcanceService,
                                        IAltinnPrefillService altinnPrefillService,
                                        ILogEntryService logEntryService,
                                        IFormMetadataService formMetadataService,
                                        UserNotificationHandler userNotificationHandler,
                                        ISyncRecordsOfCombinedDistributions syncRecordsOfCombinedDistributions,
                                        ISvarUtStatusService svarUtStatusService,
                                        ApplicationDbContext dbContext,
                                        SvarUtRestService svarUtService,
                                        SvarUtRestBuilderPrint svarUtRestPrintBuilder
            )
        {
            _altinnPrefillService = altinnPrefillService;
            _logEntryService = logEntryService;
            _formMetadataService = formMetadataService;
            _corresponcanceService = corresponcanceService;
            _altinnServer = ConfigurationManager.AppSettings["AltinnServer"];
            _svarUtService = svarUtService;
            _svarUtRestPrintBuilder = svarUtRestPrintBuilder;
            _userNotificationHandler = userNotificationHandler;
            _syncRecordsOfCombinedDistributions = syncRecordsOfCombinedDistributions;
            _svarUtStatusService = svarUtStatusService;
            _dbContext = dbContext;
        }

        private bool SendNotification(FormData distributionServiceFormData, string reportee, string reporteePresentation, IPrefillForm prefillFormData, Guid distributionId, string prefillFormId, string archivereference, int dueInNumberOfDays)
        {
            bool isNotificationSuccessful = false;

            try
            {
                _logEntryService.Save(new LogEntry(distributionServiceFormData.ArchiveReference, $"Dist id {prefillFormData.GetPrefillKey()} - correspondence", LogEntry.Info, LogEntry.InternalMsg));

                string messageServiceCode = ConfigurationManager.AppSettings["MessageServiceCode"]; ;
                int messageServiceEditionCode = Convert.ToInt32(ConfigurationManager.AppSettings["MessageServiceCodeEdition"]);
                WS.AltinnCorrespondenceAgency.ReceiptExternal correspondenceResponse = null;

                EmailNotification notificationTexts = null;
                string emailcontent = "";
                string smscontent = "";

                CorrespondanceBuilder correspondanceBuilder = new CorrespondanceBuilder();

                if (dueInNumberOfDays != 0)
                {
                    correspondanceBuilder.SetUpCorrespondence(messageServiceCode,
                        messageServiceEditionCode.ToString(),
                        reportee,
                        archivereference,
                        DateTime.Now.AddDays(dueInNumberOfDays));
                }
                else
                {
                    correspondanceBuilder.SetUpCorrespondence(messageServiceCode,
                        messageServiceEditionCode.ToString(),
                        reportee,
                        archivereference);
                }

                //correspondanceBuilder.SetMessageSender(prefillFormData.GetPrefillMessageSender());
                correspondanceBuilder.AddContent(prefillFormData.GetPrefillNotificationTitle(), prefillFormData.GetPrefillNotificationSummary(), prefillFormData.GetPrefillNotificationBody().Replace("ARXXXXXX", distributionServiceFormData.ArchiveReference));
                correspondanceBuilder.AddReplyLink($"{_altinnServer}/Pages/ServiceEngine/Dispatcher/Dispatcher.aspx?ReporteeElementID={prefillFormId}", prefillFormData.GetPrefillLinkText());

                if (distributionServiceFormData.Attachments != null)
                {
                    //TODO support attachment larger than 30MB
                    //Sortering på vedlegg etter gruppe i blankett
                    var sortedAttachments = new AttachmentSorter().GenerateSortedListOfAttachments(distributionServiceFormData.Attachments);

                    foreach (var attachment in sortedAttachments)
                        correspondanceBuilder.AddBinaryAttachment(attachment.Attachment.FileName, AttachmentSetting.GetAttachmentTypeFriendlyName(attachment.Attachment.AttachmentTypeName), attachment.Attachment.AttachmentData, attachment.Attachment.ArchiveReference);
                }

                if (prefillFormData.DoEmailNotification())
                {
                    notificationTexts = prefillFormData.GetEmailNotification();

                    // Replace generic link with link to actual form in Altinn
                    emailcontent = notificationTexts.EmailContent.Replace("<a href='https://www.altinn.no/'>", $"<a href = '{_altinnServer}/Pages/ServiceEngine/Dispatcher/Dispatcher.aspx?ReporteeElementID={prefillFormId}'>");
                    smscontent = notificationTexts.SmsContent.Replace("<a href='https://www.altinn.no/'>", $"{_altinnServer}/Pages/ServiceEngine/Dispatcher/Dispatcher.aspx?ReporteeElementID={prefillFormId}");

                    // Replace dummy AR reference with actual AR
                    emailcontent = MessageFormatter.ReplaceStaticArchiveReferenceWithActual(emailcontent, archivereference);
                    smscontent = MessageFormatter.ReplaceStaticArchiveReferenceWithActual(smscontent, archivereference);
                }

                if (prefillFormData.DoEmailNotification() && (prefillFormData.GetNotificationChannel() == NotificationEnums.NotificationChannel.Correspondence || prefillFormData.GetNotificationChannel() == NotificationEnums.NotificationChannel.CorrespondenceWithPrefillEndpointValidation))
                {
                    var notificationTemplate = prefillFormData.GetAltinnNotificationTemplate();
                    correspondanceBuilder.AddEmailAndSmsNotification(prefillFormData.GetNotificationType(), Resources.TextStrings.DistributionFromEmail, notificationTexts?.Email, notificationTexts?.EmailSubject, emailcontent, notificationTemplate, smscontent);
                }

                InsertCorrespondenceV2 correspondence = correspondanceBuilder.Build();

                try
                {
                    correspondenceResponse = _corresponcanceService.SendNotification(prefillFormData.GetPrefillKey(), correspondence);
                }
                catch (Exception e)
                {
                    _log.Error(e, "An error occurred when sending notification for {prefillKey}", prefillFormData.GetPrefillKey());

                    correspondenceResponse = null;
                    string innerException = "";
                    if (e.InnerException != null)
                        innerException = e.InnerException.Message;
                    _logEntryService.Save(new LogEntry(distributionServiceFormData.ArchiveReference, $"Dist id {prefillFormData.GetPrefillKey()} - Altinn correspondence WS avvik: {e.Message}, {innerException}", LogEntry.Error, LogEntry.InternalMsg));
                }

                // - Get reference id to message in Altinn -- START
                try
                {
                    var correspondenceStatusResult = _corresponcanceService.GetCorrespondenceStatus(distributionId.ToString(), messageServiceCode, messageServiceEditionCode);
                    var corrId = correspondenceStatusResult.CorrespondenceStatusInformation.CorrespondenceStatusDetailsList.FirstOrDefault()?.CorrespondenceID;
                    _formMetadataService.SavePostDistributionReferenceId(distributionId, corrId.ToString());
                }
                catch (Exception e)
                {
                    _log.Error(e, "An error occurred when retrieving correspondence status or storing status for {prefillKey}", prefillFormData.GetPrefillKey());
                    _logEntryService.Save(new LogEntry(distributionServiceFormData.ArchiveReference, $"Dist id {prefillFormData.GetPrefillKey()} - Feil ved henting/lagring av distribusjonsmetadata: {e.Message}, {e.ToString()}", LogEntry.Error, LogEntry.InternalMsg));
                }
                // - Get reference id to message in Altinn -- END

                if (correspondenceResponse == null)
                {
                    _logEntryService.Save(new LogEntry(distributionServiceFormData.ArchiveReference, $"Feil ved utsedning av melding til Altinn bruker. Preutfylt skjema ligger nå uten tilhørende melding for tjenesten", LogEntry.Error, LogEntry.ExternalMsg));

                    _formMetadataService.SaveDistributionFormStatusNotifiedError(
                        distributionId,
                        System.DateTime.Now, // TODO: LastChanged bug
                        "Unntak ved Altinn utsendelse av melding, preutfylt skjema ligger uten melding");
                }
                else if (correspondenceResponse.ReceiptStatusCode != WS.AltinnCorrespondenceAgency.ReceiptStatusEnum.OK)
                {
                    _logEntryService.Save(new LogEntry(distributionServiceFormData.ArchiveReference, $"Dist id {prefillFormData.GetPrefillKey()} - Feil ved Altinn utsendelse av melding til {reporteePresentation} " + correspondenceResponse.ReceiptStatusCode, LogEntry.Error, LogEntry.ExternalMsg));
                    _logEntryService.Save(new LogEntry(distributionServiceFormData.ArchiveReference, $"Dist id {prefillFormData.GetPrefillKey()} - Distribusjon/correspondence Altinn feil for bruker {reportee}: {correspondenceResponse.ReceiptText}, {correspondenceResponse.ReceiptHistory}", LogEntry.Error, LogEntry.InternalMsg));

                    _formMetadataService.SaveDistributionFormStatusNotifiedError(
                        distributionId,
                        System.DateTime.Now, // TODO: LastChanged bug
                        "Feil ved Altinn utsendelse av melding, preutfylt skjema ligger uten melding");
                }
                else
                {
                    isNotificationSuccessful = true;
                    _logEntryService.Save(new LogEntry(distributionServiceFormData.ArchiveReference, $"Altinn kvittering for preutfylling til {reporteePresentation} er OK", "Info"));
                    _logEntryService.Save(new LogEntry(distributionServiceFormData.ArchiveReference, $"Dist id {prefillFormData.GetPrefillKey()} - Distribusjon/correspondence komplett", LogEntry.Info, LogEntry.InternalMsg));

                    if (prefillFormData.DoEmailNotification() && prefillFormData.ConfigureSecondNotification())
                    {
                        // TODO: Override returned datetime if faulty format
                        //   correspondenceResponse.LastChanged, incorrect time format returned
                        _userNotificationHandler.AddUserNotification(distributionServiceFormData.ArchiveReference,
                                                                                prefillFormData.GetPrefillKey(),
                                                                                //correspondenceResponse.LastChanged,
                                                                                System.DateTime.Now,
                                                                                notificationTexts.Email,
                                                                                notificationTexts.SmsContent,
                                                                                notificationTexts.EmailSubject,
                                                                                emailcontent);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return isNotificationSuccessful;
        }

        private DistributionForm GetDistributionForm(string archiveReference, string prefillKey, string prefillOurReference, string formName)
        {
            DistributionForm dForm = null;
            dForm = _formMetadataService.GetDistributionFormByOurReference(archiveReference, new List<string>() { prefillOurReference }).FirstOrDefault();

            if (dForm == null)
                dForm = _formMetadataService.InsertDistributionForm(archiveReference, prefillKey, prefillOurReference, formName);

            return dForm;
        }

        /// <summary>
        /// Improving performance on long running jobs. DbContext should be short lived...
        /// All services using dbContext performs a save in their respective methods
        /// </summary>
        private void ResetDbContext()
        {
            var changedEntriesCopy = _dbContext.ChangeTracker.Entries()
                .Where(e => e.State == System.Data.Entity.EntityState.Added ||
                            e.State == System.Data.Entity.EntityState.Modified ||
                            e.State == System.Data.Entity.EntityState.Deleted ||
                            e.State == System.Data.Entity.EntityState.Unchanged)
                .ToList();
            _log.Debug("Detaches #{DetachedDbEntries}", changedEntriesCopy.Count());
            foreach (var entry in changedEntriesCopy)
                entry.State = System.Data.Entity.EntityState.Detached;
        }

        public void SendPrefillForm(FormData distributionServiceFormData, IPrefillForm prefillFormData, string archivereferance, IAltinnForm altinnForm)
        {
            //!! ---- Performance improvement ---- !!!
            ResetDbContext();

            var stopWatch = new Stopwatch();
            try
            {
                string decryptedReportee;
                string presentationReportee;
                WS.AltinnPreFill.ReceiptExternal receiptExternal = null;

                DistributionForm dForm = GetDistributionForm(archivereferance, prefillFormData.GetPrefillKey(), prefillFormData.GetPrefillOurReference(), altinnForm.GetName());
                dForm.ErrorMessage = null;

                _logEntryService.Save(new LogEntry(archivereferance, $"Starter distribusjon med søknadsystemsreferanse {prefillFormData.GetPrefillOurReference()}", LogEntry.Info, LogEntry.ExternalMsg));

                // Add "dummy" distributions for combined nabovarsel distributions
                // ToojDo: Make this a seperate function very soon
                if (altinnForm is INabovarselSvar)
                {
                    var neighborReply = (INabovarselSvar)altinnForm;
                    var neighborReferenceIdList = neighborReply.GetSluttbrukersystemVaarReferanse();
                    if (neighborReferenceIdList != null && neighborReferenceIdList.Count > 1)
                    {
                        // Add reference ID to the first distribution
                        dForm.DistributionReference = dForm.Id;
                        _formMetadataService.SaveDistributionForm(dForm);

                        for (int i = 1; i < neighborReferenceIdList.Count; i++)
                        {
                            DistributionForm dFormDummy = GetDistributionForm(archivereferance, prefillFormData.GetPrefillKey(), neighborReferenceIdList[i], altinnForm.GetName());
                            //DistributionForm dFormDummy = _formMetadataService.InsertDistributionForm(archivereferance, prefillFormData.GetPrefillKey(), neighborReferenceIdList[i], altinnForm.GetName());
                            dFormDummy.DistributionReference = dForm.Id;
                            dFormDummy.ErrorMessage = null;
                            _formMetadataService.SaveDistributionForm(dFormDummy);
                            _logEntryService.Save(new LogEntry(archivereferance, $"Distribusjon med søknadsystemsreferanse {prefillFormData.GetPrefillOurReference()} kombinert med {neighborReferenceIdList[i]}", LogEntry.Info, LogEntry.ExternalMsg));
                            _logEntryService.Save(new LogEntry(archivereferance, $"Dist id {prefillFormData.GetPrefillKey()} kombinert med {dFormDummy.Id.ToString()}", "Info", true));
                        }
                    }
                }

                prefillFormData.SetPrefillKey(dForm.Id.ToString());
                _logEntryService.Save(new LogEntry(archivereferance, $"Dist id {prefillFormData.GetPrefillKey()} - Distribusjon av {altinnForm.GetName()} til tjeneste {prefillFormData.GetPrefillServiceCode()}/{prefillFormData.GetPrefillServiceEditionCode()}", "Info", true));

                if (String.IsNullOrEmpty(prefillFormData.GetPrefillSendToReporteeId()))
                {
                    _logEntryService.Save(new LogEntry(archivereferance, "Fant ikke personnummer/organisasjonsnummer", LogEntry.Error, LogEntry.ExternalMsg));
                    _logEntryService.Save(new LogEntry(archivereferance, $"Dist id {prefillFormData.GetPrefillKey()} - Fant ikke personnummer/organisasjonsnummer i prefillFormData.GetPrefillSendToReporteeId()", LogEntry.Error, LogEntry.InternalMsg));

                    dForm.DistributionStatus = DistributionStatus.error;
                    _formMetadataService.SaveDistributionForm(dForm);
                    _syncRecordsOfCombinedDistributions.Sync(altinnForm, dForm);

                    // Returning out of distribution loop to process the next entry
                    return;
                }

                if (prefillFormData.GetPrefillSendToReporteeId().Length > 11)
                {
                    _logEntryService.Save(new LogEntry(archivereferance, $"Dist id {prefillFormData.GetPrefillKey()} - Dekrypterer fødselsnummer", LogEntry.Info, LogEntry.InternalMsg));
                    decryptedReportee = Helpers.GetDecryptedFnr(prefillFormData.GetPrefillSendToReporteeId());
                    presentationReportee = "kryptert personnummer";
                }
                else
                {
                    decryptedReportee = prefillFormData.GetPrefillSendToReporteeId();
                    presentationReportee = decryptedReportee;
                }

                PrefillFormTaskBuilder prefillFormTaskBuilder = new PrefillFormTaskBuilder();

                prefillFormTaskBuilder.SetupPrefillFormTask(prefillFormData.GetPrefillServiceCode(), Convert.ToInt32(prefillFormData.GetPrefillServiceEditionCode()), decryptedReportee, dForm.Id.ToString(), dForm.Id.ToString(), dForm.Id.ToString(), 14);
                prefillFormTaskBuilder.AddPrefillForm(altinnForm.GetDataFormatId(), Convert.ToInt32(altinnForm.GetDataFormatVersion()), prefillFormData.GetFormXML(), dForm.Id.ToString());

                if (prefillFormData.DoEmailNotification())
                {
                    if (prefillFormData.GetNotificationChannel() == NotificationEnums.NotificationChannel.Prefill)
                    {
                        var enotification = prefillFormData.GetEmailNotification();
                        var notificationTemplate = prefillFormData.GetAltinnNotificationTemplate();
                        string emailcontent = MessageFormatter.ReplaceStaticArchiveReferenceWithActual(enotification.EmailContent, archivereferance);
                        prefillFormTaskBuilder.AddEmailAndSmsNotification(Resources.TextStrings.DistributionFromEmail, enotification.Email, enotification.EmailSubject, emailcontent, notificationTemplate, enotification.SmsContent);
                    }

                    if (prefillFormData.GetNotificationChannel() == NotificationEnums.NotificationChannel.CorrespondenceWithPrefillEndpointValidation)
                    {
                        prefillFormTaskBuilder.ValidateNotificationEndpointsWithPrefillInstantiation();
                    }
                }

                PrefillFormTask preFillData = prefillFormTaskBuilder.Build();
                DateTime? dueDate = null;
                if (prefillFormData.GetPrefillNotificationDueDays() > 0)
                    dueDate = DateTime.Now.AddDays(prefillFormData.GetPrefillNotificationDueDays());

                stopWatch.Start();
                try
                {
                    _logEntryService.Save(new LogEntry(archivereferance, $"Dist id {prefillFormData.GetPrefillKey()} - Sender prefill kall til Altinn", LogEntry.Info, true));
                    receiptExternal = _altinnPrefillService.SubmitAndInstantiatePrefilledForm(preFillData, dueDate);
                }
                catch (Exception e)
                {
                    _log.Error(e, "An error occurred when submitting prefill form {prefillKey}", prefillFormData.GetPrefillKey());
                    receiptExternal = null;
                    string innerExcpetion = "";
                    if (e.InnerException != null)
                        innerExcpetion = e.InnerException.Message;
                    _logEntryService.Save(new LogEntry(archivereferance, $"Dist id {prefillFormData.GetPrefillKey()} - unntak i WS Prefill: {e.Message}, {innerExcpetion}", LogEntry.Error, true));
                }
                stopWatch.Stop();

                if (receiptExternal == null)
                {
                    _logEntryService.Save(new LogEntry(archivereferance, $"Unntak ved Altinn utsendelse av {altinnForm.GetName()} til {presentationReportee}", LogEntry.Error, LogEntry.ExternalMsg));
                    LogEntry logSsnOrg = LogEntry.NewErrorInternal(archivereferance, $"Dist id {prefillFormData.GetPrefillKey()} - Unntak ved Altinn prefill av {altinnForm.GetName()} til >>>{prefillFormData.GetPrefillOurReference()}<<<", ProcessLabel.AltinnDistributionPostProcess, stopWatch);
                    _logEntryService.Save(logSsnOrg);
                    _formMetadataService.SaveDistributionFormStatusSubmittedPrefilledError(dForm.Id, DateTime.Now, "Unntak i Altinn prefill Web Service utsendelse");
                    _syncRecordsOfCombinedDistributions.Sync(altinnForm, dForm);
                }
                else if (receiptExternal.ReceiptStatusCode != WS.AltinnPreFill.ReceiptStatusEnum.OK)
                {
                    //if reportee isReservable --> todo - støttes ikke i Altinn Prefill
                    var nabovarsel = distributionServiceFormData.Mainform as INabovarselDistribution;
                    if (nabovarsel != null && nabovarsel.PrintIsEnabled())
                    {
                        if (receiptExternal.ReceiptText.Contains("Reportee is reserved against electronic communication"))
                        {
                            _logEntryService.Save(new LogEntry(archivereferance, $"Dist id {prefillFormData.GetPrefillKey()} - Nabovarsel print pga. reservasjon", LogEntry.Info, LogEntry.InternalMsg));
                        }
                        else if (
                            receiptExternal.ReceiptText.Contains("One or more notification endpoint addresses is missing.") ||
                            receiptExternal.ReceiptText.Contains("Unable to properly identify notification receivers") ||
                            receiptExternal.ReceiptText.Contains("no official (kofuvi) notification endpoints found")
                        )
                        {
                            // :  <a:ReceiptText>One or more notification endpoint addresses is missing. Altinn does not have an address to fill the missing address Reportee: 01024100991</a:ReceiptText>
                            _logEntryService.Save(new LogEntry(archivereferance, $"Dist id {prefillFormData.GetPrefillKey()} - Nabovarsel print pga. manglende endepunkter", LogEntry.Info, LogEntry.InternalMsg));
                        }
                        SendNabovarselThroughPrintAndMailService(distributionServiceFormData, prefillFormData, archivereferance, altinnForm, presentationReportee, decryptedReportee, dForm);
                    }
                    else
                    {
                        _logEntryService.Save(new LogEntry(archivereferance, $"Feil ved Altinn utsendelse av {altinnForm.GetName()} til {presentationReportee}"));
                        LogEntry logSsnOrg = LogEntry.NewErrorInternal(archivereferance, $"Dist id {prefillFormData.GetPrefillKey()} - Feil ved Altinn prefill til >>>{prefillFormData.GetPrefillOurReference()}<<<: {receiptExternal.ReceiptStatusCode} {receiptExternal.ReceiptText}", ProcessLabel.AltinnDistributionPostProcess, stopWatch);
                        _logEntryService.Save(logSsnOrg);

                        _formMetadataService.SaveDistributionFormStatusSubmittedPrefilledError(dForm.Id, receiptExternal.LastChanged, receiptExternal.ReceiptText);
                        _syncRecordsOfCombinedDistributions.Sync(altinnForm, dForm);
                    }
                }
                else
                {
                    _formMetadataService.SaveDistributionFormStatusSubmittedPrefilled(dForm.Id, DistributionStatus.submittedPrefilled, receiptExternal.ReceiptId.ToString(), receiptExternal.LastChanged);
                    _syncRecordsOfCombinedDistributions.Sync(altinnForm, dForm);
                    _logEntryService.Save(new LogEntry(archivereferance, $"Dist id {prefillFormData.GetPrefillKey()} - Klar til å sende correspondence", LogEntry.Info, LogEntry.InternalMsg));

                    string workflowReferenceId = "";
                    if (receiptExternal.References.Where(r => r.ReferenceTypeName == WS.AltinnPreFill.ReferenceType.WorkFlowReference).FirstOrDefault() != null) workflowReferenceId = receiptExternal.References.Where(r => r.ReferenceTypeName == WS.AltinnPreFill.ReferenceType.WorkFlowReference).First().ReferenceValue;

                    LogEntry logEntry;
                    if (SendNotification(distributionServiceFormData, decryptedReportee, presentationReportee, prefillFormData, dForm.Id, workflowReferenceId, distributionServiceFormData.ArchiveReference, CheckDueDate(altinnForm)))
                    {
                        logEntry = LogEntry.NewInfo(archivereferance, $"Altinn {altinnForm.GetName()} laget til ({presentationReportee}), Altinn kvitteringsid {receiptExternal.ReceiptId}", ProcessLabel.AltinnSubmitPrefill, stopWatch);
                    }
                    else
                    {
                        logEntry = LogEntry.NewInfo(archivereferance, $"Altinn {altinnForm.GetName()} pefill OK men correspondence feilet til ({presentationReportee}), Altinn prefill kvitteringsid {receiptExternal.ReceiptId}", ProcessLabel.AltinnSubmitPrefill, stopWatch);
                    }
                    _logEntryService.Save(logEntry);
                    _log.Information(logEntry.ToString());

                    _logEntryService.Save(new LogEntry(archivereferance, $"Dist id {prefillFormData.GetPrefillKey()} - Distribusjon behandling ferdig", LogEntry.Info, LogEntry.InternalMsg));
                }
            }
            catch (FaultException ex)
            {
                stopWatch.Stop();

                var logEntry = LogEntry.NewError(archivereferance, $"Feil ved Altinn preutfylling av {altinnForm.GetName()} " + ex.Message, ProcessLabel.AltinnSubmitPrefill, stopWatch);
                _logEntryService.Save(logEntry);
                _log.Error(ex, logEntry.ToString());

                _logEntryService.Save(new LogEntry(archivereferance, $"Dist id {prefillFormData.GetPrefillKey()} - Generellt unntak i Prefill: {ex.Message}", LogEntry.Error, LogEntry.InternalMsg));
            }
        }

        public void CloseClients()
        {
            _altinnPrefillService.CloseClient();
            _corresponcanceService.CloseClient();
        }

        internal void SendNabovarselThroughPrintAndMailService(FormData distributionServiceFormData, IPrefillForm prefillFormData, string archivereferance, IAltinnForm altinnForm, string presentationReportee, string decryptedReportee, DistributionForm dForm)
        {
            try
            {
                // Neighbor addresses
                var neighborPrintData = (INabovarselSvar)prefillFormData;
                Address neighbour = neighborPrintData.GetNeighborMailingAddress();
                Address applicant = neighborPrintData.GetApplicantMailingAddress();

                AttachmentMetadata[] sortedAttachments = new AttachmentSorter().GenerateSortedListOfAttachments(distributionServiceFormData.Attachments);
                List<Attachment> attachmentList = new List<Attachment>();
                List<AttachmentForPrintService> attachmentForPrint = ProcessAttachmentsForPrintMailPackage(sortedAttachments.Select(att => att.Attachment).ToList());

                var coverLetterPdfBytes = GeneratePrintCoverLetter(applicant.Name, prefillFormData.GetPrefillNotificationBody(true), attachmentForPrint);
                attachmentList.Add(new Attachment(archivereferance, coverLetterPdfBytes, "application/pdf", "FølgebrevNabovarsel", "folgebrevNabovarsel.pdf", false));
                attachmentList.AddRange(attachmentForPrint.Select(afp => afp.Attachment));

                //*********** Validate if data is valid for print ***********
                if (GeneralValidations.IsAddressValidForPrint(neighbour))
                {
                    // Send to print
                    ; var svarUtReferenceId = _svarUtService.SendShipment(_svarUtRestPrintBuilder.BuildForsendelseForPrint("Nabovarsel", neighbour, applicant, prefillFormData.GetPrefillNotificationTitle(), attachmentList, distributionServiceFormData), false);
                    // Check status, update db
                    CheckStatusOfPrintService(svarUtReferenceId);

                    _logEntryService.Save(new LogEntry(archivereferance, $"Altinn {altinnForm.GetName()} laget til ({presentationReportee}),  kvitteringsid {svarUtReferenceId}"));
                    _formMetadataService.SaveDistributionFormStatusSubmittedPrefilledPrinted(dForm.Id, DistributionStatus.submittedPrefilled, svarUtReferenceId, DateTime.Now);
                    _syncRecordsOfCombinedDistributions.Sync(altinnForm, dForm);
                }
                else
                {
                    _logEntryService.Save(new LogEntry(archivereferance, $"Feil ved Altinn utsendelse av {altinnForm.GetName()} til {presentationReportee}: Mottaker sine addresse data er ikke tilstrekkelige for å sende til print", "Error", true));
                    _formMetadataService.SaveDistributionFormStatusSubmittedPrefilledError(dForm.Id, DateTime.Now, "Send manuelt");
                    _syncRecordsOfCombinedDistributions.Sync(altinnForm, dForm);
                }
            }
            catch (Exception e)
            {
                _log.Error(e, "An error occurred when performing correspondence for print");
                _logEntryService.Save(new LogEntry(archivereferance, $"Feil ved Altinn utsendelse av {altinnForm.GetName()} til {presentationReportee}: {e.Message}", "Error", true));
                _formMetadataService.SaveDistributionFormStatusSubmittedPrefilledError(dForm.Id, DateTime.Now, "Send manuelt");
                _syncRecordsOfCombinedDistributions.Sync(altinnForm, dForm);
            }
        }

        private byte[] GeneratePrintCoverLetter(string applicantName, string textBody, List<AttachmentForPrintService> sortedAttachments)
        {
            var vedleggList = new List<string>();
            bool oneOrMoreAttachmentsHaveBeenModified = false;
            foreach (var attachment in sortedAttachments)
            {
                if (attachment.IsFileModifiedForPrintService)
                {
                    vedleggList.Add($"{attachment.ModifiedAttachmentTypeName}*");
                    oneOrMoreAttachmentsHaveBeenModified = true;
                }
                else
                {
                    vedleggList.Add($"{attachment.ModifiedAttachmentTypeName}");
                }
            }
            return new AltinnNabovarselCoverLetterService().MakeAltinnNabovarselCoverLetter(textBody, vedleggList, applicantName, oneOrMoreAttachmentsHaveBeenModified);
        }

        private List<AttachmentForPrintService> ProcessAttachmentsForPrintMailPackage(List<Attachment> rawAttachments)
        {
            // Todo: convert more attachments to PDF
            //  IMAGES: jpeg, jpg, tif, tiff, png, jpeg,
            //  DOCUMENTS: doc, docx, pdf

            List<AttachmentForPrintService> attachments = new List<AttachmentForPrintService>();
            List<String> attachmentsNotSupported = new List<string>();

            for (int i = 0; i < rawAttachments.Count; i++)
            {
                if (isFileAPdf(rawAttachments[i].AttachmentData))
                {
                    Attachment temp = rawAttachments[i];
                    temp.AttachmentType = "application/pdf";
                    attachments.Add(new AttachmentForPrintService(temp, AttachmentSetting.GetAttachmentTypeFriendlyName(rawAttachments[i].AttachmentTypeName), false));
                }
                else if (IsAttachmentASupportedImage(rawAttachments[i]))
                {
                    Attachment temp = rawAttachments[i];
                    temp.AttachmentData = ConvertImageToPdfBytes(rawAttachments[i].AttachmentData);
                    temp.AttachmentType = "application/pdf";
                    temp.FileName = $"{temp.FileName}.pdf";
                    attachments.Add(new AttachmentForPrintService(temp, AttachmentSetting.GetAttachmentTypeFriendlyName(rawAttachments[i].AttachmentTypeName), true));
                }
                else
                {
                    attachmentsNotSupported.Add($"{rawAttachments[i].AttachmentTypeName} vedlegg {rawAttachments[i].FileName}");
                }
            }

            if (attachmentsNotSupported.Count > 0)
            {
                throw new Exception($"{String.Join(", ", attachmentsNotSupported.ToArray())} er ikke støttet");
            }

            return attachments;
        }

        internal bool IsAttachmentASupportedImage(Attachment attachment)
        {
            // IMAGES: jpeg, jpg, tif, tiff, png, jpeg,
            // MIME: "image/tiff", "image/jpeg", "image/png"
            List<string> supportedMimeTypes = new List<string>() { "image/tiff", "image/jpeg", "image/png" };
            List<string> supportedExtensionList = new List<string>() { ".tiff", ".tif", ".jpeg", ".jpg", ".png" };
            string fileNameExtension = Path.GetExtension(attachment.FileName);

            if (supportedMimeTypes.Contains(attachment.AttachmentType.ToLower()) || (fileNameExtension != null) && supportedExtensionList.Contains(fileNameExtension.ToLower()))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        internal void CheckStatusOfPrintService(string trackingId)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();

            while (!_svarUtStatusService.GetShipmentStatus(trackingId).Contains(Dibk.Ftpb.Integration.SvarUt.Models.Status.SendtPrint.ToString()))
            {
                if (sw.ElapsedMilliseconds > 3 * 60 * 1000)
                {
                    throw new Exception($"Timeout ved utsending av Nabovarsel {_svarUtStatusService.GetShipmentStatus(trackingId)}");
                }
                Thread.Sleep(10 * 1000);
            }
        }

        internal bool isFileAPdf(byte[] checkForPdfBytes)
        {
            if (checkForPdfBytes[0] == Convert.ToByte('%')
                && checkForPdfBytes[1] == Convert.ToByte('P')
                && checkForPdfBytes[2] == Convert.ToByte('D')
                && checkForPdfBytes[3] == Convert.ToByte('F'))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        internal byte[] ConvertImageToPdfBytes(byte[] imageBytes)
        {
            // ToDo: https://stackoverflow.com/questions/2370427/itextsharp-set-document-landscape-horizontal-a4

            Document document = new Document(PageSize.A4, 50, 38, 38, 38);
            byte[] generatedPdfBytes = null;

            // Read image
            var image = Image.GetInstance(imageBytes);

            if (image.Width > image.Height && image.Width > (document.PageSize.Width - document.LeftMargin - document.RightMargin))
            {
                document = new Document(PageSize.A4.Rotate());
            }

            using (var outPdfStreams = new MemoryStream())
            {
                PdfWriter.GetInstance(document, outPdfStreams);
                document.Open();

                float maxWidth = document.PageSize.Width - document.LeftMargin - document.RightMargin;
                float maxHeight = document.PageSize.Height - document.TopMargin - document.BottomMargin;
                if (image.Height > maxHeight || image.Width > maxWidth)
                    image.ScaleToFit(maxWidth, maxHeight);

                document.Add(image);
                document.Close();

                generatedPdfBytes = outPdfStreams.ToArray();
            }

            return generatedPdfBytes;
        }

        private int CheckDueDate(IAltinnForm altinnForm)
        {
            int replyDays = 0;

            var notificationForm = altinnForm as INotificationForm;

            if (notificationForm != null)
            {
                replyDays = notificationForm.GetReplyDueDays();
            }

            return replyDays;
        }
    }
}