﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Altinn;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Encryption;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;
using no.kxml.skjema.dibk.gjenpartnabovarselV2;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Prefill
{
    public static class GjenpartNabovarselDecoder
    {
        public static List<NaboGjenboer> GetListOfNeighborSsn(Subform gjennomfoeringsplan)
        {
            var naboGjenboerTypes = new List<NaboGjenboer>();

            var gjenpartnabovarselV2 = SerializeUtil.DeserializeFromString<NabovarselType>(gjennomfoeringsplan.FormDataXml);
            var ssnList = gjenpartnabovarselV2?.naboeier?.Select(nb => nb?.foedselsnummer).ToList();

            if (gjenpartnabovarselV2?.naboeier != null)
                foreach (NaboGjenboerType naboGjenboerType in gjenpartnabovarselV2.naboeier)
                {
                    naboGjenboerTypes.Add(new NaboGjenboer()
                    {
                        encryptedFoedselsnummer = naboGjenboerType.foedselsnummer
                    });
                }

            return naboGjenboerTypes;
        }

        public static Naboeier GetNaboeier(List<NaboGjenboer> naboeierSsnList)
        {
            var naboeier = new Naboeier();

            var naboGjenboerWithSsn = new List<NaboGjenboer>();
            foreach (var gjenboerType in naboeierSsnList)
            {
                var encryptedText = gjenboerType.encryptedFoedselsnummer;

                string fodselsnummer = null;
                
                if (encryptedText?.Length > 11)
                {
                    try { fodselsnummer = Decryption.Instance.DecryptText(encryptedText); }
                    catch
                    {}
                }

                // vi bare legger ved de som er kryptert. De andre ligger da rett i XMLen
                if (string.IsNullOrEmpty(fodselsnummer))
                    continue;

                gjenboerType.foedselsnummer = fodselsnummer;
                naboGjenboerWithSsn.Add(gjenboerType);
            }

            naboeier.naboeier = naboGjenboerWithSsn.ToArray();
            return naboeier;
        }

        public static Attachment GetGjenpartNabovarselAttachement(string archiveReference, Naboeier naboeier)
        {
            if (string.IsNullOrEmpty(archiveReference) || Helpers.ObjectIsNullOrEmpty(naboeier))
                return null;

            var xmlfile = SerializeUtil.Serialize(naboeier);
            byte[] xmlBytes = Encoding.ASCII.GetBytes(xmlfile);

            var newAttachment = new Attachment(archiveReference, xmlBytes, "application/xml", Dibk.Ftpb.Common.Constants.DataTypes.OpplysningerGittINabovarselDekrypteringsNoekkel, "Opplysninger gitt i nabovarsel-maskinlesbar_nøkkel-UNNTATT-OFFENTLIGHET.xml", false);

            return newAttachment;
        }
    }
}