using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using iTextSharp.tool.xml.css;
using iTextSharp.tool.xml.html;
using iTextSharp.tool.xml.parser;
using iTextSharp.tool.xml.pipeline.css;
using iTextSharp.tool.xml.pipeline.end;
using iTextSharp.tool.xml.pipeline.html;
using KommIT.FIKS.AdapterAltinnSvarUt.Extensions;
using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using Image = iTextSharp.text.Image;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Prefill
{
    public class DistributionPlanReceiptService : IDistributionReceiptService
    {
        public Attachment MakeReceipt(List<KvitteringForNabovarsel> kvitteringForNabovarsels, List<DistributionForm> distribusjonsdata, string prosjektnavn, string forslagsstiller, string kontaktperson, List<Attachment> attachments)
        {
            Attachment rapport = null;
            //Nok data? Må sendt til navn stå i klartekst eller er referanser nok? Både XML og pdf?

            if (distribusjonsdata.Any())
            {
                rapport = new Attachment();
                rapport.AttachmentTypeName = "Kvittering";
                rapport.AttachmentType = "PDF";
                rapport.FileName = "Kvittering.pdf";

                //Get html embedded resource file
                string htmlTemplate;
                var assembly = Assembly.GetExecutingAssembly();
                var HtmlResourceName = "KommIT.FIKS.AdapterAltinnSvarUt.Services.Prefill.KvitteringForNabovarselPlan.Html";

                using (Stream stream = assembly.GetManifestResourceStream(HtmlResourceName))
                using (StreamReader reader = new StreamReader(stream))
                {
                    htmlTemplate = reader.ReadToEnd();
                }
                string LogoResourceName = "KommIT.FIKS.AdapterAltinnSvarUt.Content.images.dibk_logo.png";

                // Add Header til Html
                htmlTemplate = htmlTemplate.Replace("<logoPath/>", string.Concat("data:imagestream/", LogoResourceName));

                // Add Eiendom til Html
                var prosjektinfo = AddProsjektInfoToHtml(distribusjonsdata, prosjektnavn, forslagsstiller, kontaktperson);
                htmlTemplate = htmlTemplate.Replace("<prosjektinfo/>", prosjektinfo.ToString());

                // Add Naboer til Html
                var naboer = AddNaboerToHtml(kvitteringForNabovarsels, distribusjonsdata);
                htmlTemplate = htmlTemplate.Replace("<naboer/>", naboer.ToString());

                //Add vedlegg to Html
                var vedlegg = AddAttachmentsToHtml(attachments);
                htmlTemplate = htmlTemplate.Replace("<vedlegg/>", vedlegg.ToString());


                using (var memoryStream = CreateFromHtml(htmlTemplate))
                {
                    if (memoryStream != null)
                    {
                        var bytesStream = new byte[memoryStream.Length];
                        memoryStream.Read(bytesStream, 0, bytesStream.Length);

                        rapport.AttachmentData = bytesStream;
                    }
                }
            }
            return rapport;
        }

        private StringBuilder AddProsjektInfoToHtml(List<DistributionForm> distribusjonsdata, string planNavn, string forslagsstiller, string kontaktperson)
        {
            StringBuilder strBuilder = new StringBuilder();

            if (!planNavn.IsNullOrEmpty())
            {
                strBuilder.Append($"<b>Navn på plan: </b>{planNavn.ToHtmlEncodedString()}<br/>");
            }
            strBuilder.Append($"<b>Forslagsstiller: </b>{forslagsstiller.ToHtmlEncodedString()}<br/>");
            strBuilder.Append($"<b>Kontaktperson: </b>{kontaktperson.ToHtmlEncodedString()}<br/><br/>");
            strBuilder.Append("<b>Nabovarsel sendt via: </b> Fellestjenester Bygg<br/>");
            strBuilder.Append("<b>Altinnreferanse: </b>" + distribusjonsdata.First().InitialArchiveReference);
            return strBuilder;
        }

        private StringBuilder AddNaboerToHtml(List<KvitteringForNabovarsel> kvitteringForNabovarsels, List<DistributionForm> distribusjonsdata)
        {
            StringBuilder strBuilder = new StringBuilder();
            int numberOfDistributionSendt = distribusjonsdata.Count(d => d.DistributionStatus != DistributionStatus.error);
            var numberOfFailedDistributions = distribusjonsdata.Count(d => d.DistributionStatus == DistributionStatus.error);

            strBuilder.Append("<div class='PaddingTop Paddingbottom'>");
            strBuilder.Append("<b>Antall mottakere som har fått varsel om oppstart av reguleringsplanarbeid</b>");
            strBuilder.Append("</div>");
            
            strBuilder.Append("<div>");
            strBuilder.Append("<span>" + numberOfDistributionSendt+ "</span>");
            strBuilder.Append("</div>"); 
            

            if (numberOfFailedDistributions > 0)
            {
                var distributionFormsWithErrors = distribusjonsdata.Where(n =>  n.DistributionStatus == DistributionStatus.error).ToList();
                if (!distributionFormsWithErrors.IsNullOrEmpty())
                {
                    strBuilder.Append("<div class='PaddingTop Paddingbottom'>");
                    strBuilder.Append("<b>Følgende mottakere kunne IKKE varsles digitalt </b>");
                    strBuilder.Append("</div>");

                    foreach (var varsel in distributionFormsWithErrors)
                    {
                        var nabo = kvitteringForNabovarsels.Find(k => k.Hovedinnsendingsnummer == varsel.Id.ToString());
                        strBuilder.Append("<div class='PaddingBottom'>");
                        strBuilder.Append("<span>" + nabo.NaboNavn.ToHtmlEncodedString() + "</span>");
                        strBuilder.Append("</div>");
                    }
                }
            }

            return strBuilder;
        }


        private StringBuilder AddAttachmentsToHtml(List<Attachment> attachments)
        {
            StringBuilder strBuilder = new StringBuilder();

            strBuilder.Append("<div class='SubHeadingUnderline PaddingTop Paddingbottom'>Følgende vedlegg er sendt med varselet:</div>");
            strBuilder.Append("<div class='Paragraf'>");
            strBuilder.Append("<table id='tabell'>");
            strBuilder.Append("<thead>");
            strBuilder.Append("<tr>");
            strBuilder.Append("<td><b>Vedleggstype:</b></td>");
            strBuilder.Append("<td><b>Filnavn:</b></td>");
            strBuilder.Append("</tr>");
            strBuilder.Append("</thead>");
            strBuilder.Append("<tbody>");

            foreach (var vedlegg in attachments)
            {
                strBuilder.Append("<tr>");
                strBuilder.Append("<td>" + vedlegg.AttachmentTypeName + "</td>");
                strBuilder.Append("<td>" + vedlegg.FileName.ToHtmlEncodedString() + "</td>");
                strBuilder.Append("</tr>");
            }

            strBuilder.Append("</tbody>");
            strBuilder.Append("</table>");
            strBuilder.Append("</div>");

            return strBuilder;
        }



        public Stream CreateFromHtml(string html)
        {
            var stream = new MemoryStream();

            using (var doc = new Document(PageSize.A4))
            {
                using (var ms = new MemoryStream())
                {
                    using (var writer = PdfWriter.GetInstance(doc, ms))
                    {
                        writer.CloseStream = false;
                        doc.Open();

                        var tagProcessors = (DefaultTagProcessorFactory)Tags.GetHtmlTagProcessorFactory();
                        tagProcessors.RemoveProcessor(HTML.Tag.IMG);
                        tagProcessors.AddProcessor(HTML.Tag.IMG, new CustomImageTagProcessor());

                        var cssFiles = new CssFilesImpl();
                        cssFiles.Add(XMLWorkerHelper.GetInstance().GetDefaultCSS());
                        var cssResolver = new StyleAttrCSSResolver(cssFiles);

                        var charset = Encoding.UTF8;
                        var context = new HtmlPipelineContext(new CssAppliersImpl(new XMLWorkerFontProvider()));
                        context.SetAcceptUnknown(true).AutoBookmark(true).SetTagFactory(tagProcessors);
                        var htmlPipeline = new HtmlPipeline(context, new PdfWriterPipeline(doc, writer));
                        var cssPipeline = new CssResolverPipeline(cssResolver, htmlPipeline);
                        var worker = new XMLWorker(cssPipeline, true);
                        var xmlParser = new XMLParser(true, worker, charset);

                        using (var sr = new StringReader(html))
                        {
                            xmlParser.Parse(sr);
                            doc.Close();
                            ms.Position = 0;
                            ms.CopyTo(stream);
                            stream.Position = 0;
                        }
                    }
                }
            }

            return stream;
        }
        public class CustomImageTagProcessor : iTextSharp.tool.xml.html.Image
        {
            public override IList<IElement> End(IWorkerContext ctx, Tag tag, IList<IElement> currentContent)
            {
                var src = String.Empty;

                if (!tag.Attributes.TryGetValue(HTML.Attribute.SRC, out src))
                    return new List<IElement>(1);

                if (String.IsNullOrWhiteSpace(src))
                    return new List<IElement>(1);

                if (src.StartsWith("data:imagestream/", StringComparison.InvariantCultureIgnoreCase))
                {
                    var name = src.Substring(src.IndexOf("/", StringComparison.InvariantCultureIgnoreCase) + 1);

                    using (var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(name))
                    {
                        return CreateElementList(ctx, tag, Image.GetInstance(stream));
                    }
                }

                return base.End(ctx, tag, currentContent);
            }

            protected IList<IElement> CreateElementList(IWorkerContext ctx, Tag tag, Image image)
            {
                var htmlPipelineContext = GetHtmlPipelineContext(ctx);
                var result = new List<IElement>();
                var element = GetCssAppliers().Apply(new Chunk((Image)GetCssAppliers().Apply(image, tag, htmlPipelineContext), 0, 0, true), tag, htmlPipelineContext);
                result.Add(element);

                return result;
            }
        }
    }

}