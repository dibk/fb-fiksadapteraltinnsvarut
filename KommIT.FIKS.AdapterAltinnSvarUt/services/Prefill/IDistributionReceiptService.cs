﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KommIT.FIKS.AdapterAltinnSvarUt.Models;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Prefill
{
    public interface IDistributionReceiptService
    {
        Attachment MakeReceipt(List<KvitteringForNabovarsel> kvitteringForNabovarsels,
            List<DistributionForm> distribusjonsdata, string prosjektnavn, string ansvarligSoeker, string kontaktperson, 
            List<Attachment> attachments);
    }

}
