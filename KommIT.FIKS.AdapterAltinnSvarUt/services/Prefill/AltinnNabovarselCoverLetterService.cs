﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using iTextSharp.tool.xml.css;
using iTextSharp.tool.xml.html;
using iTextSharp.tool.xml.parser;
using iTextSharp.tool.xml.pipeline.css;
using iTextSharp.tool.xml.pipeline.end;
using iTextSharp.tool.xml.pipeline.html;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Prefill
{
    public class AltinnNabovarselCoverLetterService
    {
        public byte[] MakeAltinnNabovarselCoverLetter(string bodyText, List<string> vedleggs, string applicantName, bool oneOrMoreAttachmentsHaveBeenModified)
        {
            byte[] bytesStream = { new byte() };

            //Get html embedded resource file
            string htmlTemplate;
            var assembly = Assembly.GetExecutingAssembly();
            var HtmlResourceName = "KommIT.FIKS.AdapterAltinnSvarUt.Services.Prefill.NabovarselCoverLetterService.html";

            using (Stream stream = assembly.GetManifestResourceStream(HtmlResourceName))
            using (StreamReader reader = new StreamReader(stream))
            {
                htmlTemplate = reader.ReadToEnd();
            }
            string LogoResourceName = "KommIT.FIKS.AdapterAltinnSvarUt.Content.images.dibk_logo.png";

            // Add Header til Html
            htmlTemplate = htmlTemplate.Replace("<logoPath/>", string.Concat("data:imagestream/", LogoResourceName));
            // Add textToNabovarselCoverLetter til Html
            htmlTemplate = htmlTemplate.Replace("<textToNabovarselCoverLetter/>", bodyText);
            // Add vedlegg til Html

            if (vedleggs.Any())
            {
                var vedleggHtml = $"<div class='SubHeading'><b>Liste med vedlegg</b></div>";
                vedleggHtml = vedleggHtml + $"<div class='Paragraf'>";
                vedleggHtml = vedleggHtml + $"<ul>";
                foreach (var vedlegg in vedleggs)
                {
                    var name = vedlegg.Split('*');
                    var happyName = AttachmentSetting.GetAttachmentTypeFriendlyName(name.First());
                    if (!string.IsNullOrEmpty(happyName))
                    {
                        if (name.Length > 1)
                            happyName = happyName + " *";
                        vedleggHtml = vedleggHtml + $"<li>{happyName}</li>";

                    }
                    //vedleggHtml = vedleggHtml + $"<div><li>{vedlegg}</li></div>";
                }
                vedleggHtml = vedleggHtml + $"</ul>";
                vedleggHtml = vedleggHtml + $"</div>";

                if (oneOrMoreAttachmentsHaveBeenModified)
                {
                    vedleggHtml = vedleggHtml + $"<div class='Paragraf PaddingTop'>";
                    vedleggHtml = vedleggHtml +$"Vedlegg som er merket med * kan ha blitt justert for å gjøre dem utskriftsvennlige. Dersom vedleggene er vanskelige å lese, kan du ta kontakt med <b>{applicantName}</b> på kontaktinformasjonen oppgitt ovenfor.";
                    vedleggHtml = vedleggHtml + $"</div>";
                }

                htmlTemplate = htmlTemplate.Replace("<vedlegg/>", vedleggHtml);

            }
            //htmlTemplate = htmlTemplate.Replace("<naboer/>", naboer.ToString());

            using (var memoryStream = CreateFromHtml(htmlTemplate))
            {
                if (memoryStream != null)
                {
                    bytesStream = new byte[memoryStream.Length];
                    memoryStream.Read(bytesStream, 0, bytesStream.Length);
                }
            }
            return bytesStream;
        }




        public Stream CreateFromHtml(string html)
        {
            var stream = new MemoryStream();

            using (var doc = new Document(PageSize.A4, 50, 38, 38, 38))
            {
                using (var ms = new MemoryStream())
                {
                    using (var writer = PdfWriter.GetInstance(doc, ms))
                    {
                        writer.CloseStream = false;
                        doc.Open();

                        var tagProcessors = (DefaultTagProcessorFactory)Tags.GetHtmlTagProcessorFactory();
                        tagProcessors.RemoveProcessor(HTML.Tag.IMG);
                        tagProcessors.AddProcessor(HTML.Tag.IMG, new DistributionReceiptService.CustomImageTagProcessor());

                        var cssFiles = new CssFilesImpl();
                        cssFiles.Add(XMLWorkerHelper.GetInstance().GetDefaultCSS());
                        var cssResolver = new StyleAttrCSSResolver(cssFiles);

                        var charset = Encoding.UTF8;
                        var context = new HtmlPipelineContext(new CssAppliersImpl(new XMLWorkerFontProvider()));
                        context.SetAcceptUnknown(true).AutoBookmark(true).SetTagFactory(tagProcessors);
                        var htmlPipeline = new HtmlPipeline(context, new PdfWriterPipeline(doc, writer));
                        var cssPipeline = new CssResolverPipeline(cssResolver, htmlPipeline);
                        var worker = new XMLWorker(cssPipeline, true);
                        var xmlParser = new XMLParser(true, worker, charset);

                        using (var sr = new StringReader(html))
                        {
                            xmlParser.Parse(sr);
                            doc.Close();
                            ms.Position = 0;
                            ms.CopyTo(stream);
                            stream.Position = 0;
                        }
                    }
                }
            }

            return stream;
        }
    }
}