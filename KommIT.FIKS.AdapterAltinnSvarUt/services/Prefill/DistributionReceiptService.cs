using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using iTextSharp.tool.xml.css;
using iTextSharp.tool.xml.html;
using iTextSharp.tool.xml.parser;
using iTextSharp.tool.xml.pipeline.css;
using iTextSharp.tool.xml.pipeline.end;
using iTextSharp.tool.xml.pipeline.html;
using KommIT.FIKS.AdapterAltinnSvarUt.Extensions;
using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using Image = iTextSharp.text.Image;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Prefill
{
    public class DistributionReceiptService : IDistributionReceiptService
    {
        public Attachment MakeReceipt(List<KvitteringForNabovarsel> kvitteringForNabovarsels, List<DistributionForm> distribusjonsdata, string prosjektnavn, string ansvarligSoeker, string kontaktperson, List<Attachment> attachments)
        {
            Attachment rapport = null;
            //Nok data? Må sendt til navn stå i klartekst eller er referanser nok? Både XML og pdf?

            if (distribusjonsdata.Any())
            {
                rapport = new Attachment();
                rapport.AttachmentTypeName = "KvitteringNabovarsel";
                rapport.AttachmentType = "PDF";
                rapport.FileName = "Nabovarsel.pdf";

                //Get html embedded resource file
                string htmlTemplate;
                var assembly = Assembly.GetExecutingAssembly();
                var HtmlResourceName = "KommIT.FIKS.AdapterAltinnSvarUt.Services.Prefill.KvitteringForNabovarsel.Html";

                using (Stream stream = assembly.GetManifestResourceStream(HtmlResourceName))
                using (StreamReader reader = new StreamReader(stream))
                {
                    htmlTemplate = reader.ReadToEnd();
                }
                string LogoResourceName = "KommIT.FIKS.AdapterAltinnSvarUt.Content.images.dibk_logo.png";

                // Add Header til Html
                htmlTemplate = htmlTemplate.Replace("<logoPath/>", string.Concat("data:imagestream/", LogoResourceName));

                // Add Eiendom til Html
                var prosjektinfo = AddProsjektInfoToHtml(distribusjonsdata, prosjektnavn, ansvarligSoeker);
                htmlTemplate = htmlTemplate.Replace("<prosjektinfo/>", prosjektinfo.ToString());

                // Eiendommer
                var eiendommer = AddEiendommerToHtml(kvitteringForNabovarsels.FirstOrDefault()?.Eiendommer);
                htmlTemplate = htmlTemplate.Replace("<eiendommer/>", eiendommer.ToString());

                // Add Naboer til Html
                var naboer = AddNaboerToHtml(kvitteringForNabovarsels, distribusjonsdata);
                htmlTemplate = htmlTemplate.Replace("<naboer/>", naboer.ToString());

                //Add vedlegg to Html
                var vedlegg = AddAttachmentsToHtml(attachments);
                htmlTemplate = htmlTemplate.Replace("<vedlegg/>", vedlegg.ToString());


                using (var memoryStream = CreateFromHtml(htmlTemplate))
                {
                    if (memoryStream != null)
                    {
                        var bytesStream = new byte[memoryStream.Length];
                        memoryStream.Read(bytesStream, 0, bytesStream.Length);

                        rapport.AttachmentData = bytesStream;
                    }
                }
            }
            return rapport;
        }

        private StringBuilder AddProsjektInfoToHtml(List<DistributionForm> distribusjonsdata, string prosjektnavn, string ansvarligSoeker)
        {
            StringBuilder strBuilder = new StringBuilder();

            strBuilder.Append("<br/>");
            if (!prosjektnavn.IsNullOrEmpty())
            {
                strBuilder.Append($"<div><b>Prosjekt: </b>{prosjektnavn.ToHtmlEncodedString()}</div>");
            }
            strBuilder.Append($"<div><b>Søker: </b>{ansvarligSoeker}</div>");
            strBuilder.Append("<div><b>Altinnreferanse: </b>" + distribusjonsdata.First().InitialArchiveReference + "</div><br/>");


            return strBuilder;
        }

        private StringBuilder AddEiendommerToHtml(IEnumerable<Eiendom> eiendomByggested)
        {
            StringBuilder strBuilder = new StringBuilder();
            foreach (var eiendom in eiendomByggested)
            {
                strBuilder.Append("<div><span><b>Adresse: </b></span><span>" + eiendom.EiendomsAdress + ", " + eiendom.EiendomsPostnr + " " + eiendom.EiendomsPoststed + "</span></div>");
                strBuilder.Append("<div><span><b>Kommune: </b></span><span>" + eiendom.Kommunenavn + "</span></div>");
                strBuilder.Append("<br/>");
                strBuilder.Append("<table id='tabell'>");
                strBuilder.Append("<thead>");
                strBuilder.Append("<tr>");
                strBuilder.Append("<td><b>Gårdsnr.:</b></td>");
                strBuilder.Append("<td><b>Bruksnr.:</b></td>");
                strBuilder.Append("<td><b>Festenr.:</b></td>");
                strBuilder.Append("<td><b>Seksjonsnr.:</b></td>");
                strBuilder.Append("</tr>");
                strBuilder.Append("</thead>");

                strBuilder.Append("<tbody>");
                strBuilder.Append("<tr>");
                strBuilder.Append("<td>" + eiendom.Gaardsnummer + "</td>");
                strBuilder.Append("<td>" + eiendom.Bruksnumme + "</td>");
                
                if (!eiendom.Festenummer.IsNullOrEmpty())
                {
                    strBuilder.Append("<td>" + eiendom.Festenummer + "</td>");
                }
                if (!eiendom.Seksjonsnummer.IsNullOrEmpty())
                {
                    strBuilder.Append("<td>" + eiendom.Seksjonsnummer + "</td>");
                }
                strBuilder.Append("</tr>");
                strBuilder.Append("</tbody>");
                strBuilder.Append("</table>");
            }
            return strBuilder;
        }

        private StringBuilder AddNaboerToHtml(List<KvitteringForNabovarsel> kvitteringForNabovarsels, List<DistributionForm> distribusjonsdata)
        {
            StringBuilder strBuilder = new StringBuilder();
            var navoError = distribusjonsdata.Any(n => n.DistributionStatus == DistributionStatus.error);
            if (navoError)
            {
                strBuilder.Append("<div class='SubHeadingRed Paddingbottom'>");
                strBuilder.Append("<b>Nabovarslingen mislyktes for én eller flere naboer. Se gjennom listen for å finne ut hvilke(n) nabo(er) det gjelder.</b>");
                strBuilder.Append("<br/>");
                strBuilder.Append("</div>");
                strBuilder.Append(
                    "<div class='ParagrafRed'>Digital nabovarsling vil for eksempel mislykkes dersom naboen ikke kan nås digitalt og ikke har gyldig postadresse, dersom hjemmelshaveren er død og det ikke er foretatt eierskifte, eller dersom eiendommen er registrert på et foretak med ugyldig organisasjonsnummer.");
                strBuilder.Append("<br/>");
                strBuilder.Append("</div>");
                strBuilder.Append(
                    "<div class='ParagrafRed'>Det er opp til avsender å undersøke om naboen kan nås på en annen måte. Nabovarselet kan enten sendes som rekommandert postsending, overleveres personlig mot signatur fra naboen eller sendes på e-post hvor naboen bekrefter å ha mottatt e-posten. Unntak fra nabovarsel kan gis dersom det ikke er mulig å få tak i eieren. Det er kommunen som avgjør om nabovarslingen er tilstrekkelig i hvert tilfelle. Du kan lese mer om nabovarsling på <a href:'#'>www.dibk.no/nabovarsel</a>.");
              
                //"Unntak fra nabovarsling kan gis dersom det ikke er mulig å få tak i eieren av en eiendom. Du kan lese mer om nabovarsling på <a href:'#'>www.dibk.no/nabovarsel</a>.</div>");
                strBuilder.Append("</div>");
            }

            strBuilder.Append("<br/>");
            strBuilder.Append("<br/>");
            strBuilder.Append("<div class='SubHeadingUnderline PaddingTop'><span>Følgende naboer har fått sending av nabovarsel med tilhørende vedlegg:</span></div>");
            foreach (var naboen in kvitteringForNabovarsels)
            {
                // Get form submition date
                DateTime? date = null;
                var distributionForm = distribusjonsdata.Where(d => d.Id.ToString() == naboen.Hovedinnsendingsnummer);

                var distributionForms = distributionForm as DistributionForm[] ?? distributionForm.ToArray();

                if (distributionForms.Any())
                {
                    Thread.CurrentThread.CurrentCulture = new CultureInfo("no");
                    date = distributionForms[0].SubmitAndInstantiatePrefilled;

                }
                if (date != null)
                {
                    strBuilder.Append("<div class='PaddingTop'><span><b>Eier/fester av naboeiendom: </b></span><span>" + naboen.NaboNavn.ToHtmlEncodedString() + "</span></div>");
                    //if (!naboen.NaboAdress.IsNullOrEmpty())
                    //{
                    //    strBuilder.Append("<div><span><b>Adresse: </b></span><span>" + naboen.NaboAdress + ", " + naboen.NaboPostnr + " " + naboen.NaboPoststed + "</span></div>");
                    //}

                    //eiendom informasjon tabell
                    strBuilder.Append("<div class='Paragraf'>");
                    strBuilder.Append("<table id='tabell'>");
                    strBuilder.Append("<thead>");
                    strBuilder.Append("<tr>");
                    strBuilder.Append("<td><b>Adresse:</b></td>");
                    strBuilder.Append("<td><b>Gårdsnr.:</b></td>");
                    strBuilder.Append("<td><b>Bruksnr.:</b></td>");
                    strBuilder.Append("<td><b>Festenr.:</b></td>");
                    strBuilder.Append("<td><b>Seksjonsnr.:</b></td>");

                    strBuilder.Append("</tr>");
                    strBuilder.Append("</thead>");
                    strBuilder.Append("<tbody>");

                    foreach (var naboeiendom in naboen.GjelderNaboeiendommer)
                    {
                        // Adresse streng
                        StringBuilder addrStrBuilder = new StringBuilder();
                        if (!naboeiendom.EiendomsAdress.IsNullOrWhiteSpace())
                        {
                            addrStrBuilder.Append(naboeiendom.EiendomsAdress);
                            if (!naboeiendom.EiendomsPostnr.IsNullOrWhiteSpace() || !naboeiendom.EiendomsPoststed.IsNullOrWhiteSpace())
                            {
                                addrStrBuilder.Append(", ");
                            }
                        }

                        if (!naboeiendom.EiendomsPostnr.IsNullOrWhiteSpace() || !naboeiendom.EiendomsPoststed.IsNullOrWhiteSpace())
                        {
                            addrStrBuilder.Append(naboeiendom.EiendomsPostnr);
                            addrStrBuilder.Append(" ");
                            addrStrBuilder.Append(naboeiendom.EiendomsPoststed);
                        }


                        strBuilder.Append("<tr>");
                        strBuilder.Append("<td>" + addrStrBuilder.ToString() + "</td>");
                        strBuilder.Append("<td>" + naboeiendom.Gaardsnummer + "</td>");
                        strBuilder.Append("<td>" + naboeiendom.Bruksnumme + "</td>");
                        strBuilder.Append("<td>" + naboeiendom.Festenummer + "</td>");
                        strBuilder.Append("<td>" + naboeiendom.Seksjonsnummer + "</td>");
                        strBuilder.Append("</tr>");
                    }

                    strBuilder.Append("</tbody>");
                    strBuilder.Append("</table>");
                    strBuilder.Append("</div>");

                    // Get date from DB
                    strBuilder.Append("<div class='Paragraf'>");
                    if (distributionForms[0].DistributionStatus == DistributionStatus.error)
                    {
                        strBuilder.Append("<div class='ParagrafRed'><b>Det skjedde en feil ved digital varsling av denne naboen.</b>");
                        strBuilder.Append("</div>");
                    }
                    else
                    {
                        strBuilder.Append("<div><b>Nabovarsel sendt via: </b> Fellestjenester Bygg</div>");
                        strBuilder.Append("<div class='Paddingbottom'><b>Nabovarsel sendt: </b>" + date + "</div>");
                    }

                    strBuilder.Append("</div>");
                    //strBuilder.Append("</div>");
                }
                else
                {
                    //something
                }
            }
            return strBuilder;
        }


        private StringBuilder AddAttachmentsToHtml(List<Attachment> attachments)
        {
            StringBuilder strBuilder = new StringBuilder();

            strBuilder.Append("<div class='SubHeadingUnderline PaddingTop Paddingbottom'>Følgende vedlegg er sendt med nabovarselet:</div>");
            strBuilder.Append("<div class='Paragraf'>");
            strBuilder.Append("<table id='tabell'>");
            strBuilder.Append("<thead>");
            strBuilder.Append("<tr>");
            strBuilder.Append("<td><b>Vedleggstype:</b></td>");
            strBuilder.Append("<td><b>Filnavn:</b></td>");
            strBuilder.Append("</tr>");
            strBuilder.Append("</thead>");
            strBuilder.Append("<tbody>");

            foreach (var vedlegg in attachments)
            {
                strBuilder.Append("<tr>");
                strBuilder.Append("<td>" + vedlegg.AttachmentTypeName + "</td>");
                strBuilder.Append("<td>" + vedlegg.FileName.ToHtmlEncodedString() + "</td>");
                strBuilder.Append("</tr>");
            }

            strBuilder.Append("</tbody>");
            strBuilder.Append("</table>");
            strBuilder.Append("</div>");

            return strBuilder;
        }



        public Stream CreateFromHtml(string html)
        {
            var stream = new MemoryStream();

            using (var doc = new Document(PageSize.A4))
            {
                using (var ms = new MemoryStream())
                {
                    using (var writer = PdfWriter.GetInstance(doc, ms))
                    {
                        writer.CloseStream = false;
                        doc.Open();

                        var tagProcessors = (DefaultTagProcessorFactory)Tags.GetHtmlTagProcessorFactory();
                        tagProcessors.RemoveProcessor(HTML.Tag.IMG);
                        tagProcessors.AddProcessor(HTML.Tag.IMG, new CustomImageTagProcessor());

                        var cssFiles = new CssFilesImpl();
                        cssFiles.Add(XMLWorkerHelper.GetInstance().GetDefaultCSS());
                        var cssResolver = new StyleAttrCSSResolver(cssFiles);

                        var charset = Encoding.UTF8;
                        var context = new HtmlPipelineContext(new CssAppliersImpl(new XMLWorkerFontProvider()));
                        context.SetAcceptUnknown(true).AutoBookmark(true).SetTagFactory(tagProcessors);
                        var htmlPipeline = new HtmlPipeline(context, new PdfWriterPipeline(doc, writer));
                        var cssPipeline = new CssResolverPipeline(cssResolver, htmlPipeline);
                        var worker = new XMLWorker(cssPipeline, true);
                        var xmlParser = new XMLParser(true, worker, charset);
                        
                        using (var sr = new StringReader(html))
                        {
                            xmlParser.Parse(sr);
                            doc.Close();
                            ms.Position = 0;
                            ms.CopyTo(stream);
                            stream.Position = 0;
                        }
                    }
                }
            }

            return stream;
        }
        public class CustomImageTagProcessor : iTextSharp.tool.xml.html.Image
        {
            public override IList<IElement> End(IWorkerContext ctx, Tag tag, IList<IElement> currentContent)
            {
                var src = String.Empty;

                if (!tag.Attributes.TryGetValue(HTML.Attribute.SRC, out src))
                    return new List<IElement>(1);

                if (String.IsNullOrWhiteSpace(src))
                    return new List<IElement>(1);

                if (src.StartsWith("data:imagestream/", StringComparison.InvariantCultureIgnoreCase))
                {
                    var name = src.Substring(src.IndexOf("/", StringComparison.InvariantCultureIgnoreCase) + 1);

                    using (var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(name))
                    {
                        return CreateElementList(ctx, tag, Image.GetInstance(stream));
                    }
                }

                return base.End(ctx, tag, currentContent);
            }

            protected IList<IElement> CreateElementList(IWorkerContext ctx, Tag tag, Image image)
            {
                var htmlPipelineContext = GetHtmlPipelineContext(ctx);
                var result = new List<IElement>();
                var element = GetCssAppliers().Apply(new Chunk((Image)GetCssAppliers().Apply(image, tag, htmlPipelineContext), 0, 0, true), tag, htmlPipelineContext);
                result.Add(element);

                return result;
            }
        }
    }

}