﻿using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using System.Collections.Generic;
using System.Linq;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Prefill
{
    public class AttachmentSorter
    {
        public AttachmentMetadata[] GenerateSortedListOfAttachments(List<Attachment> attachmentMasterList)
        {
            AttachmentMetadata[] dok = new AttachmentMetadata[attachmentMasterList.Count];
            int dokidx = 0;

            foreach (var att in attachmentMasterList)
            {
                dok[dokidx] = new AttachmentMetadata
                {
                    Gruppe = AttachmentSetting.vedleggBlankettgruppe.FirstOrDefault(s => s.Key == att.AttachmentTypeName).Value,
                    Attachment = att
                };
                dokidx++;
            }

            return dok.OrderBy(b => b.Gruppe).ToArray();
        }
    }

    public class AttachmentMetadata
    {
        public string Gruppe { get; set; }

        public Attachment Attachment { get; set; }
    }
}