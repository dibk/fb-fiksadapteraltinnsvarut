using KommIT.FIKS.AdapterAltinnSvarUt.Extensions;
using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Models.ApiModels;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Altinn;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Azure;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Notification;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.NotificationForm.Nabovarsel;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.NotificationForm.NabovarselPlan;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.WS.AltinnCorrespondenceAgency;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;
using Serilog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Prefill
{
    public class FormDistributionServiceV2 : IFormDistributionServiceV2
    {
        private readonly ILogEntryService _logEntryService;
        private readonly IFormMetadataService _formMetadataService;
        private readonly SendPrefillFormServiceV2 _sendPrefillFormServiceV2;
        private static IAltinnCorrespondenceService _corresponcanceService;
        private readonly IFileStorage _fileStorage;
        private readonly IFileDownloadStatusService _fileDownloadStatusService;
        private readonly ILogger _logger = Log.ForContext<FormDistributionServiceV2>();


        public FormDistributionServiceV2(IAltinnCorrespondenceService corresponcanceService, ILogEntryService logEntryService, IFormMetadataService formMetadataService, SendPrefillFormServiceV2 sendPrefillFormServiceV2, IFileStorage fileStorage, IFileDownloadStatusService fileDownloadStatusService)
        {
            _sendPrefillFormServiceV2 = sendPrefillFormServiceV2;
            _logEntryService = logEntryService;
            _formMetadataService = formMetadataService;
            _corresponcanceService = corresponcanceService;
            _fileStorage = fileStorage;
            _fileDownloadStatusService = fileDownloadStatusService;
        }

        public void Distribute(FormData distributionServiceFormData)
        {
            _formMetadataService.SaveFormDataToFormMetadataLog(distributionServiceFormData);

            if (distributionServiceFormData.Mainform is INabovarselDistribution)
            {
                var nvForm = (INabovarselDistribution)distributionServiceFormData.Mainform;
                nvForm.CopyMainPdfToFormsAttchments(distributionServiceFormData);
            }

            if (distributionServiceFormData.Mainform is IDistributionForm)
            {
                _logEntryService.Save(new LogEntry(distributionServiceFormData.ArchiveReference, $"Behandler distribusjonsskjema {distributionServiceFormData.Mainform.GetName()}", "Info"));
                var form = (IDistributionForm)distributionServiceFormData.Mainform;

                var prefillFormsForDistribution = form.GetPrefillForms();

                _logEntryService.Save(LogEntry.NewInfo(distributionServiceFormData.ArchiveReference, $"Det er {prefillFormsForDistribution.Count} distribusjoner i innsendingen", ProcessLabel.AltinnDistributionLoop));

                var distrLogCounter = 0;
                foreach (var item in prefillFormsForDistribution)
                {
                    distrLogCounter++;

                    if (item is IPrefillForm)
                    {
                        IPrefillForm prefill = (IPrefillForm)item;

                        //Make reentrant (idempotent)
                        if (!_formMetadataService.IsDistributionSent(prefill.GetPrefillOurReference(), distributionServiceFormData.ArchiveReference))
                        {
                            _logger.Debug("Distributes #{DistributionNumber} of {NumberOfDistributions}", distrLogCounter, prefillFormsForDistribution.Count);
                            _sendPrefillFormServiceV2.SendPrefillForm(distributionServiceFormData, prefill, distributionServiceFormData.ArchiveReference, item);
                        }
                        else
                        {
                            _logEntryService.Save(LogEntry.NewInfo(distributionServiceFormData.ArchiveReference, $"Distribusjon med referanse {prefill.GetPrefillOurReference()} er allerede sendt."));
                            _logger.Warning("Skips #{DistributionNumber} of {NumberOfDistributions} with reference {PrefillOurReference} - Already been distributed", distrLogCounter, prefillFormsForDistribution.Count, prefill.GetPrefillOurReference());
                        }
                    }
                    else
                    {
                        throw new ArgumentException($"Unknown Prefillform type {item.GetName()} sent for distribution.");
                    }
                }

                _sendPrefillFormServiceV2.CloseClients();

                if (distributionServiceFormData.Mainform is INabovarselDistribution)
                {
                    var distribusjonsdata = _formMetadataService.GetDistributionForm(distributionServiceFormData.ArchiveReference);
                    var kvitteringForNabovarsels = CorrectReferencesInKvitteringForNabovarsel(prefillFormsForDistribution, distribusjonsdata);

                    var nabovarselDistributionForm = distributionServiceFormData.Mainform as INabovarselDistribution;
                    string ansvarligSokerNavn = distributionServiceFormData.Mainform.GetPropertyIdentifiers().AnsvarligSokerNavn;
                    var mainFormFilename = Resources.TextStrings.NabovarselMainformFilename;
                    string kontaktpersonReceipt = "Kontaktperson";

                    //Lage en rapport fra databasen - Get DistributionForm from arkivreferanse distribusjon GUI/ID
                    var receitService = nabovarselDistributionForm.GetDistributionReceiptService();
                    Attachment reciept = receitService.MakeReceipt(kvitteringForNabovarsels, distribusjonsdata, nabovarselDistributionForm.GetProjectName(), ansvarligSokerNavn, kontaktpersonReceipt, distributionServiceFormData.Attachments);

                    //Sende til søker
                    if (reciept != null)
                    {
                        // Find out if the ansvarlig soker is defined by an org number or folselsnummer
                        string reporteeToUse;
                        string orgNum = distributionServiceFormData.Mainform.GetPropertyIdentifiers().AnsvarligSokerOrgnr;
                        string fnrNum = distributionServiceFormData.Mainform.GetPropertyIdentifiers().AnsvarligSokerFnr;

                        if (!orgNum.IsNullOrWhiteSpace())
                        {
                            reporteeToUse = orgNum;
                        }
                        else if (!fnrNum.IsNullOrWhiteSpace() && fnrNum.Length == 11)
                        {
                            reporteeToUse = fnrNum;
                        }
                        else if (!fnrNum.IsNullOrWhiteSpace() && fnrNum.Length > 11)
                        {
                            reporteeToUse = Helpers.GetDecryptedFnr(fnrNum);
                        }
                        else
                        {
                            _logEntryService.Save(new LogEntry(distributionServiceFormData.ArchiveReference, "Finner ikke fødselnummer eller organisasjonsnummer for søker", "Error"));
                            reporteeToUse = orgNum;
                        }

                        SaveNotificationToBlob(reciept, distributionServiceFormData.MainFormPdf, distributionServiceFormData.ArchiveReference, mainFormFilename);

                        string projectNameText = nabovarselDistributionForm.GetProjectName() == null ? "" : $", {nabovarselDistributionForm.GetProjectName()}";
                        string adresse = distributionServiceFormData.Mainform.GetPropertyIdentifiers().Adresselinje1;

                        string title;
                        string kontaktperson;
                        string summary = "";


                        string body = "";

                        title = $"Kvittering for nabovarsel, {adresse}{projectNameText}";
                        kontaktperson = string.IsNullOrWhiteSpace(nabovarselDistributionForm.GetContactPerson()) ? "" : $"Kontaktperson for tiltaket er: <br> {nabovarselDistributionForm.GetContactPerson()}";
                        summary = $"Trykk på vedleggene under for å laste ned nabovarsel og kvittering med liste over hvilke naboer som har blitt varslet. ";
                        body = $"Både nabovarselet og kvitteringen skal sendes til kommunen sammen med byggesøknaden. <br><br>" +
                                      $"Naboene har 14 dager til å svare på nabovarselet. Du må sende eventuelle merknader, sammen med dine kommentarer til hver merknad, til kommunen sammen med byggesøknaden. " +
                                      $"Du kan også velge å endre byggeprosjektet og nabovarsle på nytt.<br><br>" +
                                      $"Kvitteringen gjelder byggeplaner i {adresse} <br><br> {kontaktperson}<br><br>" +
                                      $"Hvis du har spørsmål om kvitteringen, kan du oppgi følgende Altinnreferanse: " +
                                      distributionServiceFormData.ArchiveReference;

                        SendNotification(reciept, distributionServiceFormData.MainFormPdf, title,
                            summary, reporteeToUse, distributionServiceFormData.ArchiveReference, body, mainFormFilename);
                    }
                    else
                        throw new ArgumentException($"Unable to make kvittering for nabovarsel.");
                }

            }
            else
            {
                throw new ArgumentException($"Unknown form type {distributionServiceFormData.Mainform.GetName()} sent for distribution.");
            }

        }

        private void SaveNotificationToBlob(Attachment kvittering, Attachment nabovarsel, string archivereference, string filename)
        {
            Guid fileDownloadStatusGuid = Guid.Empty;
            var containerName = string.Empty;
            var fm = _formMetadataService.GetFormMetadata(archivereference);

            if (!Guid.TryParse(fm.DistributionRecieptLink, out fileDownloadStatusGuid)) //Dersom distribusjonen allerede har ein container
                fileDownloadStatusGuid = Guid.NewGuid();

            containerName = fileDownloadStatusGuid.ToString();

            try
            {
                _fileStorage.EstablishFolder(containerName, archivereference);

                var xmlUri = _fileStorage.AddFileAsByteArrayToFolder(containerName, Resources.TextStrings.KvitteringNabovarselFilename,
                    kvittering.AttachmentData, Resources.TextStrings.MimeAppPdf);

                _fileDownloadStatusService.Add(new FileDownloadStatus(archivereference, fileDownloadStatusGuid, Resources.TextStrings.FormNameNabovarsel,
                    FilTyperForNedlasting.KvitteringNabovarsel, Resources.TextStrings.KvitteringNabovarselFilename, xmlUri, Resources.TextStrings.MimeAppPdf));

                xmlUri = _fileStorage.AddFileAsByteArrayToFolder(containerName, filename,
                    nabovarsel.AttachmentData, Resources.TextStrings.MimeAppPdf);

                _fileDownloadStatusService.Add(new FileDownloadStatus(archivereference, fileDownloadStatusGuid, Resources.TextStrings.FormNameNabovarsel,
                    FilTyperForNedlasting.Nabovarsel, filename, xmlUri, Resources.TextStrings.MimeAppPdf));

                _formMetadataService.SaveDistributionRecieptLink(archivereference, containerName.ToString());
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "An error occured while saving receipt report to storage for guid {ContainerName}", containerName);
                _logEntryService.Save(new LogEntry(archivereference, $"Problem ved lagring av kvitteringsrapport", "Info"));
            }
        }

        public List<KvitteringForNabovarsel> CorrectReferencesInKvitteringForNabovarsel(List<IAltinnForm> prefillFormsForDistribution, List<DistributionForm> distrForms)
        {
            // Forsikring om at 'KvitteringNabovarsel' element i lista har 'hovedinnsendingsnummer'
            // Denne setter hovedinnseingsnummer direkte fra databasen for alle distribusjonene (tilsvarende FIXen vi kjører i etterkant)

            var kvitteringer = new List<KvitteringForNabovarsel>();


            foreach (var item in prefillFormsForDistribution)
            {
                var svar = item as INabovarselSvar;
                var kvittering = svar.GetkvitteringForNabovarsel();

                kvittering.Hovedinnsendingsnummer = distrForms.Where(d => d.ExternalSystemReference == svar.GetSluttbrukersystemVaarReferanse().FirstOrDefault())?.FirstOrDefault()?.Id.ToString();

                kvitteringer.Add(kvittering);
            }

            return kvitteringer;
        }


        private void SendNotification(Attachment kvittering, Attachment nabovarsel, string title, string summary, string sendToReporteeId, string archivereference, string body, string filename)
        {
            try
            {
                string messageServiceCode = ConfigurationManager.AppSettings["MessageServiceCode"]; ;
                int messageServiceEditionCode = Convert.ToInt32(ConfigurationManager.AppSettings["MessageServiceCodeEdition"]);

                CorrespondanceBuilder correspondanceBuilder = new CorrespondanceBuilder();
                correspondanceBuilder.SetUpCorrespondence(messageServiceCode,
                                                            messageServiceEditionCode.ToString(),
                                                            sendToReporteeId,
                                                            archivereference);

                correspondanceBuilder.AddContent(title, summary, body);


                correspondanceBuilder.AddBinaryAttachment(kvittering.FileName, kvittering.AttachmentTypeName, kvittering.AttachmentData, kvittering.ArchiveReference);
                correspondanceBuilder.AddBinaryAttachment(filename, nabovarsel.AttachmentTypeName, nabovarsel.AttachmentData, nabovarsel.ArchiveReference);

                InsertCorrespondenceV2 correspondence = correspondanceBuilder.Build();

                WS.AltinnCorrespondenceAgency.ReceiptExternal correspondenceResponse = _corresponcanceService.SendNotification(archivereference, correspondence);

                if (correspondenceResponse.ReceiptStatusCode != WS.AltinnCorrespondenceAgency.ReceiptStatusEnum.OK)
                {
                    _logEntryService.Save(new LogEntry(archivereference, $"Unntak ved Altinn utsendelse av melding", "Error"));
                    _logEntryService.Save(new LogEntry(archivereference, $"Unntak ved Altinn utsendelse av melding til {sendToReporteeId} " + correspondenceResponse.ReceiptStatusCode, "Error", true));
                }
                else
                {
                    _logEntryService.Save(new LogEntry(archivereference, $"Altinn kvittering for preutfylling er OK", "Info"));

                }

            }
            catch (Exception)
            {
                throw;
            }
        }
    }

}
