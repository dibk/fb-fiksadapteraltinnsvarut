﻿using KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.SampleData;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services
{
    public interface IPrefillForm
    {
        string GetPrefillSendToReporteeId();

        string GetPrefillMessageSender();
        string GetFormXML();
        string GetPrefillServiceCode();
        string GetPrefillServiceEditionCode();

        string GetPrefillNotificationTitle();

        string GetPrefillNotificationSummary();
        string GetPrefillNotificationBody(bool? pdf=false, string replyLink = "");
        string GetPrefillOurReference();

        int GetPrefillNotificationDueDays();

        void SetPrefillKey(string key);

        string GetPrefillLinkText();

        string GetPrefillKey();

        /// <summary>
        /// Sjekker om det er mulig å sende epostvarsel via altinn
        /// </summary>
        /// <returns></returns>
        bool DoEmailNotification();

        void SetEmailNotification(string email, string subject, string emailcontent, string smscontent);

        EmailNotification GetEmailNotification();

        NotificationEnums.NotificationChannel GetNotificationChannel();

        NotificationEnums.NotificationCarrier GetNotificationType();

        // ny metode som returnerer en string => varslingsmal. 

        string GetAltinnNotificationTemplate();

        bool ConfigureSecondNotification();

        string GetArchiveReferenceOfDistribution();
        void SetArchiveReferenceOfDistribution(string ar);

        string GetAltinnReferenceForPrefillFrom();
        void SetAltinnReferenceForPrefillFrom(string altinnReference);


    }
}