﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KommIT.FIKS.AdapterAltinnSvarUt.Models;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services
{
    public interface IApplicationLogService
    {
            void Save(ApplicationLog applicationLog);
            List<ApplicationLog> GetAllLogEntries(int getNumEntries = 100);
            List<ApplicationLog> GetLogEntriesByProcessLabelId(string processLabelId);
            List<ApplicationLog> GetLogEntriesBySequenceTag(string sequenceTag);
    }
}
