﻿using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Altinn;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Svarut.StatusService;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services
{
    public interface IDistributionStatusService
    {
        string GetStatusFor(string archiveReference);
    }
    public class DistributionStatusApiService : IDistributionStatusService
    {
        private readonly IAltinnCorrespondenceService _altinnCorrespondenceService;
        private readonly IFormMetadataService _formMetadataService;
        private readonly IServiceCodeProvider _serviceCodeProvider;
        private readonly ISvarUtStatusService _svarUtStatusService;

        public DistributionStatusApiService(IAltinnCorrespondenceService altinnCorrespondenceService, IFormMetadataService formMetadataService, IServiceCodeProvider serviceCodeProvider, ISvarUtStatusService svarUtStatusService)
        {
            _altinnCorrespondenceService = altinnCorrespondenceService;
            _formMetadataService = formMetadataService;
            _serviceCodeProvider = serviceCodeProvider;
            _svarUtStatusService = svarUtStatusService;
        }

        public string GetStatusFor(string archiveReference)
        {
            var retVal = string.Empty;

            //Get formmetadata info using the archiveReference
            var formMetadata = _formMetadataService.GetFormMetadata(archiveReference);

            if (formMetadata != null)
            {
                if (_serviceCodeProvider.DistributionServiceCodes.Contains(formMetadata.ServiceCode?.Trim()))
                {
                    var distributionForms = _formMetadataService.GetDistributionForm(archiveReference);

                    var formStatusResult = new FormStatus() { ArchiveReference = formMetadata.ArchiveReference, Application = formMetadata.Application, FormType = formMetadata.FormType, Status = formMetadata.Status, NumberOfValidationErrors = formMetadata.ValidationErrors, NumberOfValidationWarnings = formMetadata.ValidationWarnings };

                    // Main distributions will initiate retrieval of info from Altinn: 
                    // Rules:
                    //                  Id == DistributionReference
                    //                  DistributionRefernece == Guid.Empty
                    //
                    // Child distribution forms
                    // Rules:
                    //                  DistributionReference != Guid.Empty && Id <> (DistributionReference)
                    //

                    //Filter and create structure of distribution forms
                    //Identify "child distributions"
                    var childDistributions = distributionForms.Where(s => s.DistributionReference != Guid.Empty && s.Id != s.DistributionReference).OrderBy(i1 => i1.ExternalSystemReference).ToList();

                    foreach (var item in childDistributions)
                    { distributionForms.Remove(item); }

                    var parllelOptions = new ParallelOptions() { MaxDegreeOfParallelism = 20 };

                    // Get status from Altinn 
                    Parallel.ForEach(distributionForms, parllelOptions, item =>
                    {
                        var altinnCorrespondenceResult = _altinnCorrespondenceService.GetCorrespondenceStatus(item.Id.ToString(), _serviceCodeProvider.MessageServiceCode, _serviceCodeProvider.MessageServiceCodeEdition);

                        if (altinnCorrespondenceResult != null)
                        {
                            var correspondenceResult = new Correspondance() { DistributionStatus = item.DistributionStatus, DistributionType = item.DistributionType, ErrorMessage = TextObfuscation.ObfuscateSSN(item.ErrorMessage), Id = item.Id, RecieptSent = item.RecieptSent, Signed = item.Signed };

                            correspondenceResult.Distribution = new Distribution() { ExternalSystemReference = item.ExternalSystemReference };

                            if (item.Printed)
                            {
                                correspondenceResult.Distribution.SvarutShipmentStatus = GetSvarUtData(item);
                            }

                            foreach (var childItem in childDistributions.Where(i => i.DistributionReference == item.Id).ToList())
                            {
                                correspondenceResult.ChildDistributions.Add(new ChildDistribution() { ExternalSystemReference = childItem.ExternalSystemReference, Id = childItem.Id });
                            }

                            //Identify if reportee is person and anonymize. Do we have identification code/logic somewhere??
                            foreach (var corrStatusDetails in altinnCorrespondenceResult.CorrespondenceStatusInformation.CorrespondenceStatusDetailsList)
                            {
                                corrStatusDetails.Reportee = TextObfuscation.ObfuscateSSN(corrStatusDetails.Reportee);

                                var statusChanges = (from statusChange in corrStatusDetails.StatusChanges
                                                     select new StatusChange() { StatusDate = statusChange.StatusDate, StatusType = statusChange.StatusType.ToString() }).ToList();

                                var correspondanceNotifications = (from notification in corrStatusDetails.Notifications
                                                                   select new CorrespondanceNotification() { Recepient = notification.Recipient, SentDate = notification.SentDate, TransportType = notification.TransportType.ToString() }).ToList();

                                var correspondanceDetail = new CorrespondenceDetails(corrStatusDetails.Reportee, statusChanges, correspondanceNotifications);
                                correspondenceResult.CorrespondenceDetails.Add(correspondanceDetail);
                            }

                            formStatusResult.Correspondances.Add(correspondenceResult);
                        }
                    });

                    retVal = JsonConvert.SerializeObject(formStatusResult, Formatting.Indented, new MyStringEnumConverter());
                }
                else
                {
                    //Report that the archiveReference isn't a distribution
                    retVal = JsonConvert.SerializeObject(new FormStatus() { ArchiveReference = archiveReference, Status = "This form isn't part of a distribution" }, Formatting.Indented, new MyStringEnumConverter());
                }
            }

            return retVal;
        }

        private SvarUtShipmenStatus GetSvarUtData(DistributionForm printedDistribution)
        {
            var retVal = new SvarUtShipmenStatus() { SvarUtId = printedDistribution.SubmitAndInstantiatePrefilledFormTaskReceiptId, Printed = printedDistribution.Printed };

            try
            {
                // get status
                retVal.ShipmentStatus = _svarUtStatusService.GetShipmentStatus(printedDistribution.SubmitAndInstantiatePrefilledFormTaskReceiptId);

                // get "forsendelses historikk"
                var svarUtHistoricalData = _svarUtStatusService.GetShipmentHistoricalLog(printedDistribution.SubmitAndInstantiatePrefilledFormTaskReceiptId);

                foreach (var item in svarUtHistoricalData)
                {
                    //Identifies SSNs and anonymizes the message
                    var eventMessage = TextObfuscation.ObfuscateSSN(item.Item1);
                    retVal.SvarUtShipmentHistoricalEvents.Add(new SvarUtShipmentHistoricalEvent() { Event = eventMessage, EventDate = item.Item2 });
                }
            }
            catch (Exception ex)
            {
                retVal.ShipmentStatus = ex.Message;
            }

            return retVal;
        }
    }

    public class MyStringEnumConverter : Newtonsoft.Json.Converters.StringEnumConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var type = value.GetType();

            writer.WriteValue(Enum.GetName(type, value));
            return;
        }
    }

    public class FormStatus
    {
        public string ArchiveReference { get; set; }
        public string FormType { get; set; }
        public string Application { get; set; }
        public string Status { get; set; }
        public int NumberOfValidationErrors { get; set; }
        public int NumberOfValidationWarnings { get; set; }
        public System.Collections.Concurrent.ConcurrentBag<Correspondance> Correspondances { get; set; }
        public FormStatus() { Correspondances = new System.Collections.Concurrent.ConcurrentBag<Correspondance>(); }
    }

    public class Correspondance
    {
        public Guid Id { get; set; }
        public string DistributionType { get; set; }
        public DateTime? Signed { get; set; }
        public DateTime? RecieptSent { get; set; }
        public DistributionStatus DistributionStatus { get; set; }
        public string ErrorMessage { get; set; }
        public Distribution Distribution { get; set; }
        public List<ChildDistribution> ChildDistributions { get; set; }
        public List<CorrespondenceDetails> CorrespondenceDetails { get; set; }
        public Correspondance() { CorrespondenceDetails = new List<CorrespondenceDetails>(); ChildDistributions = new List<ChildDistribution>(); }
    }

    public class Distribution
    {
        public string ExternalSystemReference { get; set; }
        public SvarUtShipmenStatus SvarutShipmentStatus { get; set; }
    }

    public class ChildDistribution
    {
        public string ExternalSystemReference { get; set; }
        public Guid Id { get; set; }
    }

    public class CorrespondenceDetails
    {
        public string Reportee { get; set; }
        public List<StatusChange> StatusChanges { get; set; }
        public List<CorrespondanceNotification> CorrespondanceNotifications { get; set; }
        public CorrespondenceDetails(string reportee, List<StatusChange> statusChanges, List<CorrespondanceNotification> correspondanceNotifications)
        {
            this.Reportee = reportee;
            this.StatusChanges = statusChanges;
            this.CorrespondanceNotifications = correspondanceNotifications;
        }
    }

    public class SvarUtShipmenStatus
    {
        public string ShipmentStatus { get; set; }
        public bool Printed { get; set; }
        public string SvarUtId { get; set; }
        public List<SvarUtShipmentHistoricalEvent> SvarUtShipmentHistoricalEvents { get; set; }
        public SvarUtShipmenStatus() { SvarUtShipmentHistoricalEvents = new List<SvarUtShipmentHistoricalEvent>(); }
    }

    public class SvarUtShipmentHistoricalEvent
    {
        public string EventDate { get; set; }
        public string Event { get; set; }
    }

    public class StatusChange
    {
        public DateTime StatusDate { get; set; }
        public string StatusType { get; set; }
    }
    public class CorrespondanceNotification
    {
        public string Recepient { get; set; }
        public DateTime? SentDate { get; set; }
        public string TransportType { get; set; }
    }
}