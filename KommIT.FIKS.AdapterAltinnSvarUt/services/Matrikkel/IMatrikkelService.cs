﻿namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Matrikkel
{
    public interface IMatrikkelService
    {
        /// <summary>
        /// Kontrollerer om bruksenheten er angitt med riktig matrikkelnr, adresse og bygning.
        /// </summary>
        /// <param name="bygningsnr"></param>
        /// <param name="etasjeplan"></param>
        /// <param name="etasjenr"></param>
        /// <param name="bruksenhetlopenr"></param>
        /// <param name="adresse"></param>
        /// <param name="kommunenr"></param>
        /// <param name="gardsnr"></param>
        /// <param name="bruksnr"></param>
        /// <param name="festenr"></param>
        /// <param name="seksjonsnr"></param>
        /// <returns></returns>
        bool? IsBruksenhetOnMatrikkelnrAndAdresse(long bygningsnr, string etasjeplan, int etasjenr, int bruksenhetlopenr, string adresse, string kommunenr, int gardsnr, int bruksnr, int festenr, int seksjonsnr);
        /// <summary>
        /// Kontrollerer at bygningen finnes på eiendommen
        /// </summary>
        /// <param name="bygningsnr"></param>
        /// <param name="kommunenr"></param>
        /// <param name="gardsnr"></param>
        /// <param name="bruksnr"></param>
        /// <param name="festenr"></param>
        /// <param name="seksjonsnr"></param>
        /// <returns></returns>
        bool? IsBygningOnMatrikkelnr(long bygningsnr, string kommunenr, int gardsnr, int bruksnr, int festenr, int seksjonsnr);
        /// <summary>
        /// Kontrollerer om vegadressen finnes på eiendommen
        /// </summary>
        /// <param name="adressenavn"></param>
        /// <param name="husnummer"></param>
        /// <param name="bokstav"></param>
        /// <param name="kommunenr"></param>
        /// <param name="gardsnr"></param>
        /// <param name="bruksnr"></param>
        /// <param name="festenr"></param>
        /// <param name="seksjonsnr"></param>
        /// <returns></returns>
        bool? IsVegadresseOnMatrikkelnr(string adressenavn, string husnummer, string bokstav, string kommunenr, int gardsnr, int bruksnr, int festenr, int seksjonsnr);
        /// <summary>
        /// Kontrollerer om matrikkelnr finnes
        /// </summary>
        /// <param name="kommunenr"></param>
        /// <param name="gardsnr"></param>
        /// <param name="bruksnr"></param>
        /// <param name="festenr"></param>
        /// <param name="seksjonsnr"></param>
        /// <returns></returns>
        bool? MatrikkelnrExist(string kommunenr, int gardsnr, int bruksnr, int festenr, int seksjonsnr);
    }
}