﻿using KommIT.FIKS.AdapterAltinnSvarUt.Utils;
using Serilog;
using System;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Matrikkel
{
    public class MatrikkelService : IMatrikkelService
    {
        private readonly string _username;
        private readonly string _password;
        private readonly bool _isFeatureEnabled;

        //Funksjoner for å validere Eiendom/Byggested strukturen i byggesøknader
        //Eks:
        // <eiendomByggested>
        //  <eiendom>
        //    <eiendomsidentifikasjon>
        //      <kommunenummer>9999</kommunenummer>
        //      <gaardsnummer>892</gaardsnummer>
        //      <bruksnummer>933</bruksnummer>
        //      <festenummer>0</festenummer>
        //      <seksjonsnummer>0</seksjonsnummer>
        //    </eiendomsidentifikasjon>
        //    <adresse>
        //      <adresselinje1>Nygate 33</adresselinje1>
        //      <adresselinje2 xsi:nil="true" />
        //      <adresselinje3 xsi:nil="true" />
        //      <postnr>3825</postnr>
        //      <poststed>Lunde</poststed>
        //      <landkode xsi:nil="true" />
        //      <gatenavn xsi:nil="true" />
        //      <husnr xsi:nil="true" />
        //      <bokstav xsi:nil="true" />
        //    </adresse>
        //    <bygningsnummer>1234567896</bygningsnummer>
        //    <bolignummer>H0101</bolignummer>
        //    <kommunenavn>Nome</kommunenavn>
        //  </eiendom>
        //</eiendomByggested>
        public MatrikkelService()
        {
            _username = System.Configuration.ConfigurationManager.AppSettings["Matrikkel.Username"];
            _password = System.Configuration.ConfigurationManager.AppSettings["Matrikkel.Password"];
            _isFeatureEnabled = Feature.IsFeatureEnabled(FeatureFlags.MatrikkelValidering);
        }
        /// <summary>
        /// Sjekker om matrikkelnr som er angitt finnes i sentral matrikkel. Kan ikke brukes på "delingssaker"(hvilke tiltakstyper som kan bruke denne må sjekkes før kall til denne)
        /// </summary>
        /// <param name="kommunenr"></param>
        /// <param name="gardsnr"></param>
        /// <param name="bruksnr"></param>
        /// <param name="festenr"></param>
        /// <param name="seksjonsnr"></param>
        /// <returns></returns>
        public bool? MatrikkelnrExist(string kommunenr, int gardsnr, int bruksnr, int festenr, int seksjonsnr)
        {

            if (!_isFeatureEnabled)
                return null;

            var client = new matrikkelenhetservice.MatrikkelenhetServiceClient();
            client.ClientCredentials.UserName.UserName = _username;
            client.ClientCredentials.UserName.Password = _password;

            bool? returnValue;

            try
            {
                var result = client.findMatrikkelenhet(new matrikkelenhetservice.MatrikkelenhetIdent() { kommuneIdent = new matrikkelenhetservice.KommuneIdent() { kommunenummer = kommunenr }, gardsnummer = gardsnr, gardsnummerSpecified = true, bruksnummer = bruksnr, bruksnummerSpecified = true, festenummer = festenr, festenummerSpecified = true, seksjonsnummer = seksjonsnr, seksjonsnummerSpecified = true }, new matrikkelenhetservice.MatrikkelContext() { klientIdentifikasjon = "DibkFellestjenesterBygg", brukOriginaleKoordinater = true, locale = "no_NO_B", koordinatsystemKodeId = new matrikkelenhetservice.KoordinatsystemKodeId() { value = 10 }, systemVersion = "3.13", snapshotVersion = new matrikkelenhetservice.Timestamp() { timestamp = new DateTime(9999, 1, 1) } });
                client.Close();
                //TODO logging
                returnValue = true;
            }
            catch (Exception ex)
            {
                client.Abort();
                //TODO logging
                // Example of property that does not exist: "Matrikkelenhet med matrikkelnummer 1 / 88 finnes ikke"
                string msg = ex.Message;

                if (msg.Contains("finnes ikke"))
                {
                    returnValue = false;
                }
                else
                {
                    LogException(ex, "MatrikkelEnhetService.findMatrikkelenhet");
                    returnValue = null;
                }
            }
            return returnValue;
        }

        public bool? IsVegadresseOnMatrikkelnr(string adressenavn, string husnummer, string bokstav, string kommunenr, int gardsnr, int bruksnr, int festenr, int seksjonsnr)
        {

            if (!_isFeatureEnabled)
                return null;

            //Kan vi verifisere adresselinje eller må vi da kreve adressekode, husnr, bokstav?
            var client = new adresseservice.AdresseServiceClient();
            client.ClientCredentials.UserName.UserName = _username;
            client.ClientCredentials.UserName.Password = _password;

            Stopwatch sp = Stopwatch.StartNew();

            try
            {
                var result = client.findAdresser(new adresseservice.AdressesokModel() { kommunenummer = kommunenr, adressetype = adresseservice.Adressetype.VEGADRESSE, adressetypeSpecified = true, adressenavn = adressenavn, husnummer = husnummer, gardsnummer = gardsnr.ToString(), bruksnummer = bruksnr.ToString(), festenummer = festenr, festenummerSpecified = true, seksjonsnummer = seksjonsnr, seksjonsnummerSpecified = true }, new adresseservice.MatrikkelContext() { klientIdentifikasjon = "DibkFellestjenesterBygg", brukOriginaleKoordinater = true, locale = "no_NO_B", koordinatsystemKodeId = new adresseservice.KoordinatsystemKodeId() { value = 10 }, systemVersion = "3.13", snapshotVersion = new adresseservice.Timestamp() { timestamp = new DateTime(9999, 1, 1) } });
                client.Close();
                //TODO check result is empty array
                if (Helpers.ObjectIsNullOrEmpty(result))
                    return null;
                
                if (result.Count() > 0)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                client.Abort();
                LogException(ex, "MatrikkelAddresseService.findAdresser");

                string inputData = $"Adressenavn - {adressenavn} : husnummer - {husnummer} : bokstav - {bokstav} : kommunenr - {kommunenr} : gardsnr - {gardsnr} : bruksnr - {bruksnr} : festenr - {festenr} : seksjonsnr - {seksjonsnr}";
                LogExceptionInputData(ex, "MatrikkelAddresseService.findAdresser", inputData, sp.ElapsedMilliseconds.ToString());

                return null;
            }
        }

        //Når bygningsnr er angitt så kan det sjekkes at denne er tilknyttet riktig eiendom
        public bool? IsBygningOnMatrikkelnr(long bygningsnr, string kommunenr, int gardsnr, int bruksnr, int festenr, int seksjonsnr)
        {
            if (!_isFeatureEnabled)
                return null;

            var clientB = new bygningservice.BygningServiceClient();
            clientB.ClientCredentials.UserName.UserName = _username;
            clientB.ClientCredentials.UserName.Password = _password;

            try
            {
                var result = clientB.findBygningInfoObjekter(new bygningservice.ByggsokModel() { bygningsnummer = bygningsnr, bygningsnummerSpecified = true, gardsnummer = gardsnr.ToString(), bruksnummer = bruksnr.ToString(), festenummer = festenr, festenummerSpecified = true, seksjonsnummer = seksjonsnr, seksjonsnummerSpecified = true }, new bygningservice.MatrikkelContext() { klientIdentifikasjon = "DibkFellestjenesterBygg", brukOriginaleKoordinater = true, locale = "no_NO_B", koordinatsystemKodeId = new bygningservice.KoordinatsystemKodeId() { value = 10 }, systemVersion = "3.13", snapshotVersion = new bygningservice.Timestamp() { timestamp = new DateTime(9999, 1, 1) } });
                clientB.Close();

                foreach (var bygg in result)
                {
                    if (bygg.bygningsnr == bygningsnr)
                    {
                        return true;
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                clientB.Abort();
                LogException(ex, "MatrikkelBygningService.findBygningInfoObjekter");
                return null;
            }
        }

        //Sjekke bruksenhetsnr på bygning + adresse + seksjonsnr på eiendom er riktig
        //finn bruksenhet på bygning
        public bool? IsBruksenhetOnMatrikkelnrAndAdresse(long bygningsnr, string etasjeplan, int etasjenr, int bruksenhetlopenr, string adresse, string kommunenr, int gardsnr, int bruksnr, int festenr, int seksjonsnr)
        {
            if (!_isFeatureEnabled)
                return null;

            var client = new adresseservice.AdresseServiceClient();
            client.ClientCredentials.UserName.UserName = _username;
            client.ClientCredentials.UserName.Password = _password;

            bool found = false;
            try
            {
                var result = client.findAdresseInfoObjekter(new adresseservice.AdressesokModel() { kommunenummer = kommunenr, bygningsnummer = bygningsnr, bygningsnummerSpecified = true, gardsnummer = gardsnr.ToString(), bruksnummer = bruksnr.ToString(), festenummer = festenr, festenummerSpecified = true, seksjonsnummer = seksjonsnr, seksjonsnummerSpecified = true }, new adresseservice.MatrikkelContext() { klientIdentifikasjon = "DibkFellestjenesterBygg", brukOriginaleKoordinater = true, locale = "no_NO_B", koordinatsystemKodeId = new adresseservice.KoordinatsystemKodeId() { value = 10 }, systemVersion = "3.13", snapshotVersion = new adresseservice.Timestamp() { timestamp = new DateTime(9999, 1, 1) } });
                client.Close();
                if (result.Count() > 0)
                {
                    foreach (var adr in result)
                    {
                        foreach (var item in adr.bruksenheter)
                        {
                            if (item.etasjeplanKodeId.value.ToString() == etasjeplan && item.etasjenr == etasjenr && item.lopenr == bruksenhetlopenr)
                            {
                                found = true;
                            }
                        }
                    }
                    return found;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                client.Abort();
                LogException(ex, "MatrikkelAdresseService.findAdresseInfoObjekter");
                return null;
            }
        }

        private void LogException(Exception exception, string resourceName)
        {
            var serviceFaultException = exception as FaultException<bygningservice.ServiceFaultInfo>;
            if (serviceFaultException != null)
            {
                Log.Error(serviceFaultException, "Error ({exceptionType}) occurred when communicating with Matrikkel - {externalResource}. FaultMessage: {faultMessage} - FaultAction: {faultAction} - FaultCode: {faultCode} -  FaultReason: {faultReason} - MatrikkelDetailCategory: {matrikkelDetailCategory} - MatrikkelDetailCause: {matrikkelDetailCause} - MatrikkelDetailClassname: {matrikkelDetailClassname} - MatrikkelDetailMessage: {matrikkelDetailMessage} - MatrikkelDetailStacktraceText: {matrikkelDetailStacktraceText}",
                      serviceFaultException.GetType().ToString(),
                      resourceName,
                      serviceFaultException.Message, serviceFaultException.Action, serviceFaultException.Code, serviceFaultException.Reason,
                      serviceFaultException.Detail.category,
                      serviceFaultException.Detail.exceptionDetail.cause,
                      serviceFaultException.Detail.exceptionDetail.className,
                      serviceFaultException.Detail.exceptionDetail.message,
                      serviceFaultException.Detail.stackTraceText);

                return;
            }

            var faultException = exception as FaultException;
            if (faultException != null)
            {
                Log.Error(faultException, "Error ({exceptionType}) occurred when communicating with Matrikkel - {externalResource}. FaultMessage: {faultMessage} - FaultAction: {faultAction} - FaultCode: {faultCode} - FaultReason: {faultReason}",
                    faultException.GetType().ToString(),
                    resourceName,
                    faultException.Message, faultException.Action, faultException.Code, faultException.Reason);

                return;
            }

            // Generic exception logging
            Log.Error(exception, "Error ({exceptionType}) occurred when communicating with Matrikkel - {externalResource} - ExceptionMessage: {exceptionMessage}", exception.GetType().ToString(), resourceName, exception.Message);
        }

        
        private void LogExceptionInputData(Exception exception, string resourceName, string inputData, string milliseconds)
        {
            var serviceFaultException = exception as FaultException<bygningservice.ServiceFaultInfo>;
            if (serviceFaultException != null)
            {
                Log.Error(serviceFaultException, "Error ({exceptionType}) occurred when communicating with Matrikkel - {externalResource}. InputData: {inputData} - Duration: {milliseconds}",
                      serviceFaultException.GetType().ToString(),
                      resourceName,
                      inputData,
                      milliseconds);

                return;
            }

            var faultException = exception as FaultException;
            if (faultException != null)
            {
                Log.Error(faultException,
                    "Error ({exceptionType}) occurred when communicating with Matrikkel - {externalResource}. InputData: {inputData} - Duration: {milliseconds}",
                    faultException.GetType().ToString(),
                    resourceName,
                    inputData,
                    milliseconds);

                return;
            }

            // Generic exception logging
            Log.Error(exception, "Error ({exceptionType}) occurred when communicating with Matrikkel - {externalResource} - InputData: {inputData}  - Duration: {milliseconds}", exception.GetType().ToString(), resourceName, inputData, milliseconds);
        }

    }
}