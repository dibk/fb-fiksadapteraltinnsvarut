using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.SearchEngine;
using Serilog;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services
{
    public class FormMetadataService : IFormMetadataService
    {
        private readonly ILogger _log = Log.ForContext<FormMetadataService>();

        private readonly ApplicationDbContext _dbContext;
        private readonly ISearchEngineIndexer _searchEngineIndexer;

        public FormMetadataService(ApplicationDbContext dbContext, ISearchEngineIndexer searchEngineIndexer)
        {
            _dbContext = dbContext;
            _searchEngineIndexer = searchEngineIndexer;
        }

        public void UpdateFormMetadata(FormMetadata formMetadata)
        {
            Update(formMetadata);
        }

        private void Update(FormMetadata formMetadata)
        {
            try
            {
                _dbContext.Entry(formMetadata).State = EntityState.Modified;
                _dbContext.SaveChanges();
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException ex)
            {
                _log.Error(ex, "Entity validation error when saving form metadata. {ValidationErrors}", string.Join(", ", ex.EntityValidationErrors));
                throw;
            }

            _log.Debug("Finished saving metadata to database.");

            _searchEngineIndexer.Add(formMetadata);

            _log.Debug("Finished with metadata update method.");
        }

        public void SaveSvarUtShippingInformation(string archiveReference, string forsendelsesId)
        {
            FormMetadata metadata = GetFormMetadata(archiveReference);
            metadata.SvarUtForsendelsesId = forsendelsesId;
            metadata.SvarUtShippingTimestamp = DateTime.Now;
            Update(metadata);
        }

        public void SaveDistributionRecieptLink(string archiveReference, string distributionRecieptLink)
        {
            FormMetadata metadata = GetFormMetadata(archiveReference);
            metadata.DistributionRecieptLink = distributionRecieptLink;
            Update(metadata);
        }

        public string GetForsendelsesIdByArchiveReference(string archivereference)
        {
            var queryResult = from s in _dbContext.FormMetadata
                              where s.ArchiveReference == archivereference
                              select s.SvarUtForsendelsesId;

            return queryResult.FirstOrDefault();
        }

        public FormMetadata GetFormMetadata(string archivereference)
        {
            var queryResult = from s in _dbContext.FormMetadata
                              where s.ArchiveReference == archivereference
                              select s;

            return queryResult.FirstOrDefault();
        }

        public Boolean IsFormInQueue(string archivereference)
        {
            var queryResult = from s in _dbContext.FormMetadata
                              where s.ArchiveReference == archivereference
                              select s;

            Boolean inQueue = false;
            if (queryResult.Count() > 0) inQueue = true;

            return inQueue;
        }

        public List<FormMetadata> GetFormMetadataListByCaseId(int saksaar, long saksseksvensnummer)
        {
            var queryResult = from s in _dbContext.FormMetadata
                              where s.MunicipalityArchiveCaseYear == saksaar && s.MunicipalityArchiveCaseSequence == saksseksvensnummer
                              select s;

            return queryResult.ToList();
        }

        public List<FormMetadata> GetFormMetadataList(int skip, int topLogEntries)
        {
            var queryResult = (from m in _dbContext.FormMetadata
                               orderby m.ArchiveTimestamp descending
                               select m).Skip(skip).Take(topLogEntries);

            return queryResult.ToList();
        }

        public int CountFormMetadataList()
        {
            return _dbContext.FormMetadata.Count();
        }

        public List<FormMetadata> GetFormMetadataList(int skip, int topLogEntries, string filter)
        {
            IQueryable<FormMetadata> queryResult;

            if (string.IsNullOrEmpty(filter))
            {
                queryResult = (from m in _dbContext.FormMetadata
                               orderby m.ArchiveTimestamp descending
                               select m).Skip(skip).Take(topLogEntries);
            }
            else
            {
                //Search performance improvement, most likely the filter is the archive reference
                queryResult = (from m in _dbContext.FormMetadata
                               orderby m.ArchiveTimestamp descending
                               where m.ArchiveReference == filter
                               select m).Skip(skip).Take(topLogEntries);

                if (queryResult.Count() == 0)
                    queryResult = (from m in _dbContext.FormMetadata
                                   orderby m.ArchiveTimestamp descending
                                   where m.Application.Contains(filter) || m.ArchiveReference.Contains(filter) || m.MunicipalityCode.Contains(filter) || m.SenderSystem.Contains(filter)
                                   select m).Skip(skip).Take(topLogEntries);
            }
            return queryResult.ToList();
        }

        public void SaveArchiveReference(string archiveReference, string senderSystem)
        {
            if (!MetadataExists(archiveReference))
            {
                var metadata = new FormMetadata(archiveReference);
                metadata.SenderSystem = senderSystem;
                metadata.Status = "I kø";

                _dbContext.FormMetadata.Add(metadata);
                _dbContext.SaveChanges();
            }
        }

        public bool SaveFormMetadata(FormMetadata formMetadata)
        {
            try
            {
                if (!MetadataExists(formMetadata.ArchiveReference))
                {
                    _dbContext.FormMetadata.Add(formMetadata);
                    _dbContext.SaveChanges();
                    return true;
                }
                return false;
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException ex)
            {
                _log.Error(ex, "Entity validation error when saving form metadata. {ValidationErrors}", string.Join(", ", ex.EntityValidationErrors));
                throw;
            }
        }

        public void UpdateValidationResultToFormMetadata(string archiveReference, string status, int errors, int warnings)
        {
            FormMetadata metadata = GetFormMetadata(archiveReference);
            metadata.Status = status;
            metadata.ValidationErrors = errors;
            metadata.ValidationWarnings = warnings;

            Update(metadata);
        }

        public void UpdateStatusToFormMetadata(string archiveReference, string status)
        {
            FormMetadata metadata = GetFormMetadata(archiveReference);
            metadata.Status = status;

            Update(metadata);
        }

        public void UpdateServiceCodeToFormMetadata(string archiveReference, string servicecode, string serviceeditioncode)
        {
            FormMetadata metadata = GetFormMetadata(archiveReference);
            metadata.ServiceCode = servicecode;
            metadata.ServiceEditionCode = Convert.ToInt32(serviceeditioncode);

            Update(metadata);
        }

        public void SaveFormDataToFormMetadataLog(FormData formData)
        {
            FormMetadata metadata = GetFormMetadata(formData.ArchiveReference);

            if (formData.Mainform != null)
            {
                if (formData.Mainform.GetPropertyIdentifiers() != null)
                {
                    metadata.FormType = formData.Mainform.GetName();
                    metadata.Application = formData.Mainform.GetPropertyIdentifiers().FraSluttbrukersystem;

                    //More log data if present
                    metadata.ApplicantName = formData.Mainform.GetPropertyIdentifiers().AnsvarligSokerNavn;
                    metadata.ApplicantAddress = formData.Mainform.GetPropertyIdentifiers().AnsvarligSokerAdresselinje1;
                    metadata.PropertyFirstKnr = formData.Mainform.GetPropertyIdentifiers().Kommunenummer;
                    metadata.PropertyFirstGardsnr = formData.Mainform.GetPropertyIdentifiers().Gaardsnummer;
                    metadata.PropertyFirstBruksnr = formData.Mainform.GetPropertyIdentifiers().Bruksnummer;
                    metadata.PropertyFirstFestenr = formData.Mainform.GetPropertyIdentifiers().Festenummer;
                    metadata.PropertyFirstSeksjonsnr = formData.Mainform.GetPropertyIdentifiers().Seksjonsnummer;
                    metadata.PropertyFirstAddress = formData.Mainform.GetPropertyIdentifiers().Adresselinje1;
                    metadata.FirstActionType = formData.Mainform.GetPropertyIdentifiers().TiltakType;
                }
            }

            if (formData.ArchiveTimestamp != null)
                metadata.ArchiveTimestamp = formData.ArchiveTimestamp;

            if (!string.IsNullOrWhiteSpace(formData.SvarUtDocumentTitle))
                metadata.SvarUtDocumentTitle = formData.SvarUtDocumentTitle;

            Update(metadata);
        }

        public void SaveReceiver(IReceivingAuthority receivingAuthority, string archiveReference)
        {
            FormMetadata metadata = GetFormMetadata(archiveReference);
            metadata.MunicipalityCode = receivingAuthority.Municipality.GetMunicipalityCode();
            metadata.SendTo = receivingAuthority.ToString();
            Update(metadata);
        }

        public void SaveMunicipalityArchiveCase(string archiveReference, int municipalityArchiveCaseYear, long municipalityArchiveCaseSequence)
        {
            FormMetadata metadata = GetFormMetadata(archiveReference);
            metadata.MunicipalityArchiveCaseYear = municipalityArchiveCaseYear;
            metadata.MunicipalityArchiveCaseSequence = municipalityArchiveCaseSequence;
            Update(metadata);
        }

        public void SaveDecision(Decision dc)
        {
            _dbContext.Decisions.Add(dc);
            _dbContext.SaveChanges();
        }

        public void SaveDeviationLetter(DeviationLetter deviationLetter)
        {
            _dbContext.DeviationLetter.Add(deviationLetter);
            _dbContext.SaveChanges();
        }

        public bool MetadataExists(string altinnArkivreferanse)
        {
            return _dbContext.FormMetadata.Any(f => f.ArchiveReference == altinnArkivreferanse);
        }

        public List<Decision> GetDecisionListByCaseId(int municipalityArchiveCaseYear, long municipalityArchiveCaseSequence)
        {
            var queryResult = from s in _dbContext.Decisions
                              where s.MunicipalityArchiveCaseYear == municipalityArchiveCaseYear && s.MunicipalityArchiveCaseSequence == municipalityArchiveCaseSequence
                              select s;

            return queryResult.ToList();
        }

        public Decision GetDecision(string archivereference)
        {
            var queryResult = from s in _dbContext.Decisions
                              where s.ArchiveReference == archivereference
                              select s;

            return queryResult.OrderBy(t => t.DecisionDate).FirstOrDefault();
        }

        public int RemoveMetadata(string archivereference)
        {
            //var metadata = new FormMetadata(archivereference);
            FormMetadata metadata = GetFormMetadata(archivereference);
            _dbContext.FormMetadata.Remove(metadata);
            _dbContext.SaveChanges();

            return _dbContext.SaveChanges();
        }

        public DistributionForm InsertDistributionForm(string archiveReference, string initialExternalSystemReference, string externalSystemReference, string distributionType)
        {
            var metadata = new DistributionForm();
            metadata.Id = Guid.NewGuid();
            metadata.InitialArchiveReference = archiveReference;
            metadata.InitialExternalSystemReference = initialExternalSystemReference;
            metadata.ExternalSystemReference = externalSystemReference;
            metadata.DistributionType = distributionType;
            metadata.DistributionReference = Guid.Empty;
            _dbContext.DistributionForms.Add(metadata);
            _dbContext.SaveChanges();
            return metadata;
        }

        public void InsertDistributionForm(DistributionForm distributionForm)
        {
            _dbContext.DistributionForms.Add(distributionForm);
            _dbContext.SaveChanges();
            _searchEngineIndexer.Add(distributionForm);
        }

        public void InsertDistributionForms(IEnumerable<DistributionForm> distributionForms)
        {
            _dbContext.DistributionForms.AddRange(distributionForms);
            _dbContext.SaveChanges();
            AsyncHelper.RunSync(async () => await _searchEngineIndexer.AddManyAsync(distributionForms));
        }

        public void SaveDistributionForm(DistributionForm df)
        {
            _dbContext.Entry(df).State = EntityState.Modified;
            _dbContext.SaveChanges();

            _searchEngineIndexer.Add(df);
        }

        public List<DistributionForm> GetDistributionForm(string archivereference)
        {
            var queryResult = from s in _dbContext.DistributionForms
                              where s.InitialArchiveReference == archivereference
                              select s;

            return queryResult.ToList();
        }

        public List<DistributionForm> GetDistributionFormsForDistributionReference(Guid distributionReference)
        {
            var queryResult = from s in _dbContext.DistributionForms
                              where s.DistributionReference == distributionReference
                              select s;

            return queryResult.ToList();
        }

        public DistributionForm GetDistributionFormByGuid(Guid internalReference)
        {
            var queryResult = from s in _dbContext.DistributionForms
                              where s.Id == internalReference
                              select s;

            return queryResult.FirstOrDefault();
        }

        public List<DistributionForm> GetDistributionFormByOurReference(string archiveReference, List<string> sluttbrukersystemVaarReferanse)
        {
            var query = _dbContext.DistributionForms.Where(d =>
                                    d.InitialArchiveReference.Equals(archiveReference, StringComparison.InvariantCultureIgnoreCase)
                        && sluttbrukersystemVaarReferanse.Contains(d.ExternalSystemReference));
            return query.ToList();
        }

        public string GetMostRecentDistributionForsendelsesIdNotSigned()
        {
            var queryResult = from s in _dbContext.DistributionForms
                              where s.DistributionStatus == DistributionStatus.submittedPrefilled && s.SignedArchiveReference == null
                              orderby s.SubmitAndInstantiatePrefilled descending
                              select s.Id.ToString();

            return queryResult.FirstOrDefault();
        }

        public void SaveDistributionFormStatusSubmittedPrefilled(Guid id,
            DistributionStatus distributionStatus,
            string submitAndInstantiatePrefilledFormTaskReceiptId,
            DateTime submitAndInstantiatePrefilled)
        {
            var record = GetDistributionFormByGuid(id);
            record.DistributionStatus = distributionStatus;
            record.SubmitAndInstantiatePrefilledFormTaskReceiptId = submitAndInstantiatePrefilledFormTaskReceiptId;
            record.SubmitAndInstantiatePrefilled = submitAndInstantiatePrefilled;
            SaveDistributionForm(record);
        }

        public void SaveDistributionFormStatusSubmittedPrefilledPrinted(Guid id,
           DistributionStatus distributionStatus,
           string submitAndInstantiatePrefilledFormTaskReceiptId,
           DateTime submitAndInstantiatePrefilled)
        {
            var record = GetDistributionFormByGuid(id);
            record.DistributionStatus = distributionStatus;
            record.SubmitAndInstantiatePrefilledFormTaskReceiptId = submitAndInstantiatePrefilledFormTaskReceiptId;
            record.SubmitAndInstantiatePrefilled = submitAndInstantiatePrefilled;
            record.Printed = true;
            SaveDistributionForm(record);
        }

        public void SaveDistributionFormStatusSubmittedPrefilledError(Guid id, DateTime submitAndInstantiatePrefilled, string errorMessage)
        {
            var record = GetDistributionFormByGuid(id);
            record.DistributionStatus = DistributionStatus.error;
            record.SubmitAndInstantiatePrefilled = submitAndInstantiatePrefilled;
            record.ErrorMessage = errorMessage;
            SaveDistributionForm(record);
        }

        public bool SaveDistributionFormStatusSigned(Guid id, DistributionStatus distributionStatus, DateTime signedDateTime, string signedArchiveReference)
        {
            var record = GetDistributionFormByGuid(id);

            if (record != null)
            {
                record.DistributionStatus = distributionStatus;
                record.Signed = signedDateTime;
                record.SignedArchiveReference = signedArchiveReference;
                SaveDistributionForm(record);
                return true;
            }
            else
            {
                return false;
            }
        }

        public void SaveDistributionFormStatusNotified(Guid id, DistributionStatus distributionStatus, DateTime notifiedDateTime, string recieptSentArchiveReference)
        {
            var record = GetDistributionFormByGuid(id);
            record.DistributionStatus = distributionStatus;
            record.RecieptSent = notifiedDateTime;
            record.RecieptSentArchiveReference = recieptSentArchiveReference;
            SaveDistributionForm(record);
        }

        public void SaveDistributionFormStatusNotifiedError(Guid id, DateTime notifiedDateTime, string errorMessage)
        {
            var record = GetDistributionFormByGuid(id);
            record.DistributionStatus = DistributionStatus.error;
            record.RecieptSent = notifiedDateTime;
            record.ErrorMessage = errorMessage;
            SaveDistributionForm(record);
        }

        public bool IsDistributionSent(string ourReference, string archiveReference)
        {
            if (string.IsNullOrEmpty(ourReference?.Trim()))
                return false;

            var df = _dbContext.DistributionForms.FirstOrDefault(d => d.InitialArchiveReference.Equals(archiveReference) && d.ExternalSystemReference.Equals(ourReference));
            /*
                Sent scenario:
                Scenarie 1. DistributionStatus = SubmittedPrefilled og SubmitAndInstatntiatePrefilledFormTaskReceiptId har verdi er det sent ut skjema og varsel
                Scenario 2. Printed
            */
            if (df != null
                && (
                     (!DistributionHasError(df) && !string.IsNullOrEmpty(df.SubmitAndInstantiatePrefilledFormTaskReceiptId))
                  || df.Printed)
                  )
            {
                return true;
            }

            /*
                Failed scenario:
                Scenario 1. DistributionStatus = Error og ErrorMessage I disse tilfellene vil det være mulig å prøve å sende på nytt.
            */
            if (df != null && DistributionHasError(df))
            {
                _log.Debug("{ArchiveReference} distributionReference {OurReference} can be retried for errormessage: {ErrorMessage}", archiveReference, ourReference, df.ErrorMessage);
                return false;
            }
            return false;
        }

        private bool DistributionHasError(DistributionForm df)
        {
            return df.DistributionStatus == DistributionStatus.error && !string.IsNullOrEmpty(df.ErrorMessage);
        }

        public void SaveFormAttachmentMetadata(List<FormAttachmentMetadata> formAttachmentMetadata)
        {
            if (formAttachmentMetadata?.Count() == 0)
                return;

            bool addedItem = false;
            foreach (var item in formAttachmentMetadata)
            {
                var existingItem = _dbContext.FormAttachmentMetadatas.FirstOrDefault(p => p.ArchiveReference == item.ArchiveReference
                                                                    && p.AttachmentType == item.AttachmentType
                                                                    && p.FileName == item.FileName
                                                                    && p.Size == item.Size);
                if (existingItem == null)
                {
                    item.ArchiveReference = item.ArchiveReference.ToUpper();
                    _dbContext.FormAttachmentMetadatas.Add(item);
                    addedItem = true;
                }
            }

            if (addedItem)
                _dbContext.SaveChanges();
        }

        /// <summary>
        /// Adds or updates PostDistributionMetaData record
        /// </summary>
        /// <param name="distributionFormId"></param>
        /// <param name="referenceId"></param>
        /// <returns></returns>
        public PostDistributionMetaData SavePostDistributionReferenceId(Guid distributionFormId, string referenceId)
        {
            var record = (from s in _dbContext.PostDistributionMetaData where s.Id == distributionFormId select s).FirstOrDefault();

            if (record == null)
            {
                record = new PostDistributionMetaData();
                record.Id = distributionFormId;
                _dbContext.PostDistributionMetaData.Add(record);
            }
            else
                _dbContext.Entry(record).State = EntityState.Modified;

            record.ReferenceId = referenceId;
            record.DateChanged = DateTime.Now;
            _dbContext.SaveChanges();

            return record;
        }

        public PostDistributionMetaData GetPostDistributionMetaData(Guid distributionFormId)
        {
            var record = (from s in _dbContext.PostDistributionMetaData where s.Id == distributionFormId select s).FirstOrDefault();
            return record;
        }

        public List<PostDistributionMetaData> GetPostDistributionMetaDatas(List<Guid> distributionFormIds)
        {
            var records = (from s in _dbContext.PostDistributionMetaData where distributionFormIds.Contains(s.Id) select s).ToList();
            return records;
        }

        public void AddFtId(string archiveReference, string ftId)
        {
            if (!string.IsNullOrEmpty(archiveReference) && !string.IsNullOrEmpty(ftId))
            {
                var result = _dbContext.FormWorkflowLog.Where(f => f.ArchiveReference.Equals(archiveReference)).FirstOrDefault();
                if (result == null)
                {
                    _dbContext.FormWorkflowLog.Add(new FormWorkflowLog() { ArchiveReference = archiveReference, FtId = ftId });
                    _dbContext.SaveChanges();
                }
            }
        }

        public int CountDistributionFormsByStatus(string archivereference, IEnumerable<DistributionStatus> distributionStatuses)
        {
            if (distributionStatuses == null || distributionStatuses.Count() == 0)
                throw new ArgumentException("At least one distribution status must be part of the filter");

            var result = UniqueDistributions(archivereference).Where(d => distributionStatuses.Contains(d.DistributionStatus)).Count();

            return result;
        }

        public int CountUniqueDistributions(string archivereference)
        {
            var result = UniqueDistributions(archivereference).Count();

            return result;
        }

        private IQueryable<DistributionForm> UniqueDistributions(string archivereference)
        {
            if (string.IsNullOrEmpty(archivereference))
                throw new ArgumentException("Archivereference is required");

            var result = _dbContext.DistributionForms.Where(d => d.InitialArchiveReference.Equals(archivereference) && (d.DistributionReference == new Guid() || d.Id == d.DistributionReference));

            return result;
        }

        public void UpdateReferencedDistributions(Guid id, DistributionForm updatedDistributionForm)
        {
            var forms = GetDistributionFormsForDistributionReference(id).Where(d => d.InitialArchiveReference == updatedDistributionForm.InitialArchiveReference);

            foreach (DistributionForm form in forms)
            {
                form.DistributionStatus = updatedDistributionForm.DistributionStatus;
                form.Signed = updatedDistributionForm.Signed;
                form.SignedArchiveReference = updatedDistributionForm.SignedArchiveReference;
                form.SubmitAndInstantiatePrefilledFormTaskReceiptId = updatedDistributionForm.SubmitAndInstantiatePrefilledFormTaskReceiptId;
                form.RecieptSent = updatedDistributionForm.RecieptSent;
                form.RecieptSentArchiveReference = updatedDistributionForm.RecieptSentArchiveReference;
                form.ErrorMessage = updatedDistributionForm.ErrorMessage;
                form.Printed = updatedDistributionForm.Printed;

                SaveDistributionForm(form);
            }
        }
    }
}