﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KommIT.FIKS.AdapterAltinnSvarUt.Models;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services
{
    public class DeviationService : IDeviationService
    {
        private readonly ApplicationDbContext _dbContext;

        public DeviationService(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void SaveDeviations(List<Deviation> deviations)
        {
            foreach (var deviation in deviations)
            {
                _dbContext.Deviations.Add(deviation);
            }
            _dbContext.SaveChanges();
        }
    }
}