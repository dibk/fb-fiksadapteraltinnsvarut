﻿using Dibk.Ftpb.Common.Datamodels;
using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Models.ApiModels;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Altinn;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Metadata;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services
{
    public class FormValidationServiceV2
    {
        private ValidationResult _validationResult;

        private bool AttachmenttypesAndFormsNameIsMainForm(string name)
        {
            var hovedskjema = new[]
            {
                "GjenpartNabovarsel",
                "Matrikkelopplysninger",
                "Melding om endring",
                "Opphoer av ansvarsrett",
                "Avfallsplan",
                "Søknad om Arbeidstilsynets samtykke",
                "Søknad om ansvarsrett for selvbygger",
                "Søknad om endring av gitt tillatelse eller godkjenning",
                "Søknad om ferdigattest",
                "Søknad om igangsettingstillatelse",
                "Søknad om midlertidig brukstillatelse",
                "Søknad om rammetillatelse",
                "Søknad om tillatelse i ett trinn",
                "Søknad om endring av tillatelse",
                "Gjennomføringsplan", "GjennomføringsplanV4"
            };
            return hovedskjema.Contains(name);
        }

        public ValidationResult ValidateFormStructure(string dataFormatId, string dataFormatVersion, string formDataAsXml)
        {
            ValidationResult valResult = new ValidationResult(0, 0);
            string xsdSchema = "";

            var instances = AltinnFormTypeResolver.GetForms();

            foreach (var item in instances)
            {
                if (item.GetDataFormatId() == dataFormatId && item.GetDataFormatVersion() == dataFormatVersion)
                {
                    xsdSchema = item.GetSchemaFile();
                }
            }
            if (string.IsNullOrEmpty(xsdSchema))
                valResult.AddError($"Finner ikke XSD for dataFormatId {dataFormatId} og dataFormatVersion {dataFormatVersion}", "");
            else
            {
                var xmlValidator = new XmlValidator();
                using (var xsdStream = XsdResolver.GetXsd(xsdSchema))
                {
                    using (var xmlStream = new MemoryStream(Encoding.UTF8.GetBytes(formDataAsXml)))
                    {
                        valResult = xmlValidator.Validate(xmlStream, xsdStream);
                    }
                }
            }
            return valResult;
        }

        public ValidationResult ValidateFormData(string dataFormatId, string dataFormatVersion, string formDataAsXml, List<string> attachmenttypesAndForms)
        {
            if (_validationResult == null)
                _validationResult = new ValidationResult(0, 0);

            var instances = AltinnFormTypeResolver.GetForms();

            if (!string.IsNullOrEmpty(formDataAsXml))
            {
                foreach (var item in instances)
                {
                    if (item.GetDataFormatId() == dataFormatId && item.GetDataFormatVersion() == dataFormatVersion)
                    {
                        item.InitiateForm(formDataAsXml);
                        if (attachmenttypesAndForms == null)
                            attachmenttypesAndForms = new List<string>();

                        item.SetFormSetElements(attachmenttypesAndForms);
                        _validationResult = item.ValidateData();
                    }
                }
            }
            return _validationResult;
        }

        public ValidationResult ValidateAttachmentsAndSubForms(string dataFormatId, string dataFormatVersion, List<ValidateAttachment> attachmenttypesAndForms)
        {
            //servicecode og edition for dataformatid og version
            var altinMetadata = new AltinnMetadataServices().GetAltinMetadata(dataFormatId, dataFormatVersion);

            // Validere altinn attachment rules
            ValidateFormAttachmentsAndSubForms(altinMetadata, attachmenttypesAndForms);

            return _validationResult;
        }

        public ValidationResult ValidateFormDataAndAttachments(string dataFormatId, string dataFormatVersion, string formDataAsXml, List<ValidateAttachment> attachmenttypesAndForms)
        {
            //servicecode og edition for dataformatid og version
            var altinMetadata = new AltinnMetadataServices().GetAltinMetadata(dataFormatId, dataFormatVersion);
            var attachmentNames = new List<string>();
            if (attachmenttypesAndForms != null && attachmenttypesAndForms.Any())
            {
                attachmentNames = attachmenttypesAndForms.Select(attach => attach.Name).ToList();

                // Validate altinn attachment - Name, type, size
                ValidateFormAttachmentsAndSubForms(altinMetadata, attachmenttypesAndForms);
            }

            //Validate FormData
            ValidateFormData(dataFormatId, dataFormatVersion, formDataAsXml, attachmentNames);

            return _validationResult;
        }

        public ValidationResult ValidateFormAttachmentsAndSubForms(AltinnMetadata altinnMetadata, List<ValidateAttachment> attachmenttypesAndForms)
        {
            if (_validationResult == null)
                _validationResult = new ValidationResult(0, 0);

            var attachments = new List<ValidateAttachment>();
            var subForms = new List<ValidateAttachment>();
            var mainForms = new List<ValidateAttachment>();
            var unknowns = new List<ValidateAttachment>();

            // Split up the different forms and attachment into seperate lists
            if (attachmenttypesAndForms != null && attachmenttypesAndForms.Any())
            {
                foreach (var item in attachmenttypesAndForms)
                {
                    if (altinnMetadata.SubFormsMetaData.Exists(form => form.FormName == item.Name))
                    {
                        subForms.Add(item);
                    }
                    else if (altinnMetadata.AttachmentRules.Exists(form => form.AttachmentTypeName == item.Name))
                    {
                        attachments.Add(item);
                    }
                    else if (AttachmenttypesAndFormsNameIsMainForm(item.Name))
                    {
                        // Mainforms are not validated
                        mainForms.Add(item);
                    }
                    else
                    {
                        // These are the extras we have received
                        unknowns.Add(item);
                    }
                }

                ValidateAttachments(altinnMetadata, attachments, _validationResult);
                ReportUnknownForms(altinnMetadata, unknowns, _validationResult);
            }

            return _validationResult;
        }

        private void ReportUnknownForms(AltinnMetadata altinnMetadata, List<ValidateAttachment> unknownForms, ValidationResult validationResult)
        {
            foreach (var item in unknownForms)
            {
                _validationResult.AddError(
                    $"Vedlegg må være i henhold til metadata for skjema. Vedleggstype eller underskjema '{item.Name}' er ikke gyldig.",
                    $"{altinnMetadata.ServiceCode}.{altinnMetadata.ServiceEditionCode}.3");
            }
        }

        private void ValidateAttachments(AltinnMetadata altinnMetadata, List<ValidateAttachment> attachment, ValidationResult validationResult)
        {
            Dictionary<string, int> vedleggCountDictionary = new Dictionary<string, int>();

            foreach (var item in attachment)
            {
                // Check if the item is in
                if (altinnMetadata.AttachmentRules != null)
                {
                    //Check if the attachment type is valid in the schema
                    var vedleggstype =
                        altinnMetadata.AttachmentRules.FirstOrDefault(a => a.AttachmentTypeName == item.Name);
                    if (vedleggstype == null)
                    {
                        _validationResult.AddError(
                            $"Vedlegg må være i henhold til metadata for skjema. Vedleggstype '{item.Name}' for fil '{item.Filename}' er ikke gyldig.",
                            $"{altinnMetadata.ServiceCode}.{altinnMetadata.ServiceEditionCode}.3");
                        continue;
                    }

                    if(string.IsNullOrEmpty(item.Filename))
                    {
                        _validationResult.AddError($"Filnavnet for Vedleggstype {item.Name} er tomt.", $"{altinnMetadata.ServiceCode}.{altinnMetadata.ServiceEditionCode}.3");
                        continue;
                    }

                    //Check File Name for invalid characters
                    var fileName =
                        item.Filename?.Substring(0, item.Filename.LastIndexOf(".", StringComparison.Ordinal));

                    if (!string.IsNullOrEmpty(fileName))
                    {
                        // check if file name contains invalid chars
                        var invalidCharacters = Path.GetInvalidFileNameChars();

                        char[] fileNameArray = { };
                        fileNameArray = fileName.ToCharArray();

                        bool arrayHaveAnyInvalidCharacter = fileNameArray.Any(st => invalidCharacters.Contains(st));

                        if (arrayHaveAnyInvalidCharacter)
                        {
                            var invalidChars = string.Empty;
                            if (fileNameArray.Any())
                            {
                                var invalidsCharacters = fileNameArray.Where(st => invalidCharacters.Contains(st));
                                invalidChars =
                                    $". Filnavn skal ikke inneholde '{String.Join(",", invalidsCharacters.Select(chr => chr.ToString()))}'";
                            }

                            _validationResult.AddError(
                                $"Ugyldg filnavn '{item.Filename}' for Vedleggstype {item.Name}{invalidChars}.",
                                $"{altinnMetadata.ServiceCode}.{altinnMetadata.ServiceEditionCode}.3");
                            continue;
                        }
                    }
                    else
                    {
                        _validationResult.AddError($"Ugyldg filnavn '{item.Filename}' for Vedleggstype {item.Name}.",
                            $"{altinnMetadata.ServiceCode}.{altinnMetadata.ServiceEditionCode}.3");
                    }

                    // Check if file type is allowed
                    string extension = Path.GetExtension(item.Filename)?.Substring(1);

                    if (!string.IsNullOrEmpty(extension))
                    {
                        if (!vedleggstype.AllowedFileTypes.Contains(extension.ToLower()) &&
                            vedleggstype.AllowedFileTypes != "*.*")
                        {
                            _validationResult.AddError(
                                $"Vedlegg må være i henhold til metadata for skjema. Filtype for fil {item.Filename} er ikke gyldig.",
                                $"{altinnMetadata.ServiceCode}.{altinnMetadata.ServiceEditionCode}.3");
                        }
                    }

                    //Check file sizes
                    if (item.FileSize > vedleggstype.MaxFileSize)
                    {
                        _validationResult.AddError(
                            $"Vedlegg må være i henhold til metadata for skjema. Filstørrelse for fil {item.Filename} er ikke gyldig.",
                            $"{altinnMetadata.ServiceCode}.{altinnMetadata.ServiceEditionCode}.3");
                    }

                    //Count number of vedleggstyper
                    if (vedleggCountDictionary.ContainsKey(item.Name))
                    {
                        vedleggCountDictionary[item.Name] = vedleggCountDictionary[item.Name] + 1;
                    }
                    else
                    {
                        vedleggCountDictionary.Add(item.Name, 1);
                    }
                }
            }

            //Sjekke om antall vedleggstyper er gyldig
            foreach (var vedlegg in vedleggCountDictionary)
            {
                var vedleggstype = altinnMetadata.AttachmentRules.FirstOrDefault(a => a.AttachmentTypeName == vedlegg.Key);

                if (vedleggstype != null)
                {
                    if (vedlegg.Value > vedleggstype.MaxAttachmentCount)
                    {
                        _validationResult.AddError(
                            "Vedlegg må være i henhold til metadata for skjema. Vedleggstype " + vedlegg.Key +
                            " har for mange filer.",
                            altinnMetadata.ServiceCode + "." + altinnMetadata.ServiceEditionCode + ".3");
                    }

                    if (vedlegg.Value < vedleggstype.MinAttachmentCount)
                    {
                        _validationResult.AddError(
                            "Vedlegg må være i henhold til metadata for skjema. Vedleggstype " + vedlegg.Key +
                            " har for lite filer.",
                            altinnMetadata.ServiceCode + "." + altinnMetadata.ServiceEditionCode + ".3");
                    }
                }
            }
        }
    }
}