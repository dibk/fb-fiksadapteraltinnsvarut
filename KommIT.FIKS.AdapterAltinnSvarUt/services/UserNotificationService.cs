using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using KommIT.FIKS.AdapterAltinnSvarUt.Models;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services
{
    public class UserNotificationService : IUserNotificationService
    {
        private readonly ApplicationDbContext _dbContext;

        public UserNotificationService(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void DeleteEntries(DateTime expired)
        {
            var query = _dbContext.Set<Models.UserNotification>().Where(f =>
                f.FirstNotification < expired && f.Status == UserNotificationStatus.Sent);
            foreach (var expiredNotification in query)
            {
                _dbContext.UserNotifications.Remove(expiredNotification);
            }
        }

        public void AddUserNotification(Models.UserNotification userNotification)
        {
            userNotification.Status = UserNotificationStatus.Pending;
            _dbContext.UserNotifications.Add(userNotification);
            _dbContext.SaveChanges();
        }

        public List<Models.UserNotification> GetActiveNotifications()
        {
            DateTime currentTime = DateTime.Now;
            DateTime currentTimeMaxLookBack = currentTime.AddDays(-12);

            return _dbContext.Set<Models.UserNotification>().Where(f =>
                f.SecondNotification < currentTime &&
                f.SecondNotification > currentTimeMaxLookBack &&
                f.Status == UserNotificationStatus.Pending).ToList();
        }

        public void UpdateStatus(UserNotificationStatus userNotificationStatus, string messageReference)
        {
            var query = _dbContext.Set<Models.UserNotification>().FirstOrDefault(f => f.MessageReference == messageReference);

            if (query != null)
            {
                query.Status = userNotificationStatus;
                _dbContext.Entry(query).State = EntityState.Modified;
                _dbContext.SaveChanges();
            }
        }


        public void UpdateRecord(Models.UserNotification userNotification)
        {
            if (userNotification != null)
            {
                _dbContext.Entry(userNotification).State = EntityState.Modified;
                _dbContext.SaveChanges();
            }
        }


    }
}
