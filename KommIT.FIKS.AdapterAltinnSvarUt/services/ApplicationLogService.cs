﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.SearchEngine;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services
{
    public class ApplicationLogService : IApplicationLogService
    {
        private ApplicationDbContext _dbContext;
        private IApplicationUserService _applicationUserService;
        private readonly ISearchEngineIndexer _searchEngineIndexer;


        public ApplicationLogService(ApplicationDbContext dbContext, IApplicationUserService applicationUserService, ISearchEngineIndexer searchEngineIndexer)
        {
            _dbContext = dbContext;
            _applicationUserService = applicationUserService;
            _searchEngineIndexer = searchEngineIndexer;
        }


        public void Save(ApplicationLog applicationLog)
        {
            applicationLog.Timestamp = DateTime.Now;

            _dbContext.ApplicationLog.Add(applicationLog);
            _dbContext.SaveChanges();

            _searchEngineIndexer.Add(applicationLog);
        }

        public List<ApplicationLog> GetLogEntriesByProcessLabelId(string processLabelId)
        {
            var queryResult = from m in _dbContext.ApplicationLog
                              where m.ProcessLabelId == processLabelId
                              orderby m.Timestamp descending
                              select m;

            return queryResult.ToList();
        }

        public List<ApplicationLog> GetLogEntriesBySequenceTag(string sequenceTag)
        {
            var queryResult = from m in _dbContext.ApplicationLog
                              where m.SequenceTag == sequenceTag
                              orderby m.Timestamp descending
                              select m;

            return queryResult.ToList();
        }

        public List<ApplicationLog> GetAllLogEntries(int getNumEntries = 100)
        {
            var queryResult = from l in _dbContext.ApplicationLog
                              orderby l.Timestamp descending
                              select l;
            var temp = queryResult.Take(getNumEntries); // Could give timeout
            return temp.ToList();
        }
    }
}