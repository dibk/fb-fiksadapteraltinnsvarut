﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Prefill;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services
{
    public interface INabovarselDistribution
    {
        void CopyMainPdfToFormsAttchments(FormData formData);

        string GetProjectName();
        string GetContactPerson();
        bool PrintIsEnabled();
        IDistributionReceiptService GetDistributionReceiptService();

    }
}
