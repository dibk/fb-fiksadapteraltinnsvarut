﻿using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Models.ApiModels;
using KommIT.FIKS.AdapterAltinnSvarUt.Models.Signatur;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Encryption.TextEncyption;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services
{
    public class TiltakshaverSamtykkeService : ITiltakshaverSamtykkeService
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly ITextEncryptionService _textEncryptionService;

        public TiltakshaverSamtykkeService(ApplicationDbContext dbContext, ITextEncryptionService textEncryptionService)
        {
            _dbContext = dbContext;
            _textEncryptionService = textEncryptionService;
        }

        public Guid AddNew(SamtykkeTiltakshaverNy ny)
        {
            var samtykke = new TiltakshaversSamtykke
            {
                Id = Guid.NewGuid(),
                TiltakshaverNavn = ny.Tiltakshaver,
                TiltakshaverOrgnr = ny.TiltakshaverOrgnr,
                TiltakshaverPartstypeKode = ny.TiltakshaverPartstypeKode,
                AnsvarligSoeker = ny.AnsvarligSoeker,
                AnsvarligSoekerOrganisasjonsnummer = ny.AnsvarligSoekerOrganisasjonsnummer,
                Prosjektnavn = ny.Prosjektnavn,
                FraSluttbrukerSystem = ny.FraSluttbrukerSystem,
                DateCreated = DateTime.Now,
                Byggested = new Collection<TiltakshaversSamtykkeByggested>()
            };
            if (ny.Byggested != null && ny.Byggested.Any())
            {
                foreach (var item in ny.Byggested)
                {
                    samtykke.Byggested.Add(new TiltakshaversSamtykkeByggested(item));
                }
            }

            if (ny.Tiltaktypes != null && ny.Tiltaktypes.Any())
            {
                samtykke.Tiltaktypes = new List<TiltakshaversSamtykkeTiltaktype>();
                foreach (var tiltaktype in ny.Tiltaktypes)
                {
                    TiltakshaversSamtykkeTiltaktype tiltakstype = GetTiltakstype(tiltaktype) ?? new TiltakshaversSamtykkeTiltaktype(tiltaktype);

                    samtykke.Tiltaktypes.Add(tiltakstype);
                }
            }

            samtykke.Status = TiltakshaverSamtykkeConstants.Opprettet;

            _dbContext.TiltakshaversSamtykkes.Add(samtykke);
            _dbContext.SaveChanges();

            return samtykke.Id;
        }

        public TiltakshaversSamtykkeTiltaktype GetTiltakstype(SamtykkeTiltakshaverNyTiltaktype nyTiltaktype)
        {
            return _dbContext.TiltakshaversSamtykkeTiltaktypes.SingleOrDefault(t => t.kodeverdi == nyTiltaktype.kodeverdi);
        }

        public int PrepareVedleggRow(Guid samtykkeId)
        {
            var rowplaceholderId = Guid.NewGuid();
            var placeholderName = $"Placeholder-{rowplaceholderId}";
            var samtykke = GetTiltakshaversSamtykke(samtykkeId);
            var placeholderVedlegg = new TiltakshaversSamtykkeVedlegg()
            {
                Navn = placeholderName
            };
            samtykke.Vedlegg.Add(placeholderVedlegg);
            _dbContext.Entry(samtykke).State = EntityState.Modified;
            _dbContext.SaveChanges();

            samtykke = GetTiltakshaversSamtykke(samtykkeId);
            var vedlegg = samtykke.Vedlegg.First(p => p.Navn.Equals(placeholderName, StringComparison.OrdinalIgnoreCase));

            return vedlegg.VedleggId;
        }

        public void AddVedlegg(string samtykkeId, int vedleggId, string navn, string vedleggstype, string storageUri, long fileSize) =>
            AddVedlegg(ParseSamtykkeId(samtykkeId), vedleggId, navn, vedleggstype, storageUri, fileSize);

        public void AddVedlegg(Guid samtykkeId, int vedleggId, string navn, string vedleggstype, string storageUri, long fileSize)
        {
            var samtykke = GetTiltakshaversSamtykke(samtykkeId);

            var vedlegg = samtykke.Vedlegg.First(p => p.VedleggId == vedleggId);
            vedlegg.Navn = navn;
            vedlegg.Vedleggstype = vedleggstype;
            vedlegg.Referanse = storageUri;
            vedlegg.Storrelse = fileSize.ToString();

            _dbContext.Entry(samtykke).State = EntityState.Modified;
            _dbContext.SaveChanges();
        }

        public bool DeleteVedlegg(Guid samtykkeId, int vedleggId)
        {
            var samtykke = GetTiltakshaversSamtykke(samtykkeId);

            var vedlegg = samtykke.Vedlegg.First(p => p.VedleggId == vedleggId);
            samtykke.Vedlegg.Remove(vedlegg);
            _dbContext.Entry(vedlegg).State = EntityState.Deleted;
            _dbContext.SaveChanges();

            return true;
        }

        public TiltakshaversSamtykkeVedlegg GetVedlegg(string samtykkeId, int vedleggId) => GetVedlegg(ParseSamtykkeId(samtykkeId), vedleggId);

        public TiltakshaversSamtykkeVedlegg GetVedlegg(Guid samtykkeId, int vedleggId)
        {
            var samtykke = GetTiltakshaversSamtykke(samtykkeId);
            return samtykke.Vedlegg.FirstOrDefault(p => p.VedleggId == vedleggId);
        }

        public TiltakshaversSamtykke GetTiltakshaversSamtykke(string samtykkeId) => GetTiltakshaversSamtykke(ParseSamtykkeId(samtykkeId));

        public TiltakshaversSamtykke GetTiltakshaversSamtykke(Guid samtykkeId)
        {
            return _dbContext.TiltakshaversSamtykkes.Include(bygg => bygg.Byggested)
                                                  .Include(ved => ved.Vedlegg)
                                                  .Include(tiltak => tiltak.Tiltaktypes)
                                                  .SingleOrDefault(s => s.Id == samtykkeId);
        }

        public List<string> GetCandidatesForDeletion(int deleteAfterDaysLimit = 365)
        {
            var candidateIds = new List<string>();

            var obsoletionDate = DateTime.UtcNow.Subtract(new TimeSpan(deleteAfterDaysLimit, 0, 0, 0));
            candidateIds = _dbContext.TiltakshaversSamtykkes.Where(s => s.DateCreated < obsoletionDate && s.Status != TiltakshaverSamtykkeConstants.Slettet)
                                                          .Select(s => s.Id.ToString())
                                                          .ToList();

            return candidateIds;
        }

        public void MarkAsDeleted(string samtykkeId)
        {
            var samtykke = GetTiltakshaversSamtykke(samtykkeId);

            var deletedText = "(ANONYMISERT)";
            samtykke.Status = TiltakshaverSamtykkeConstants.Slettet;
            samtykke.TiltakshaverNavn = deletedText;
            samtykke.SignertTiltakshaverFnr = deletedText;
            samtykke.SignertTiltakshaverOrgnr = deletedText;
            samtykke.AnsvarligSoekerOrganisasjonsnummer = deletedText;
            samtykke.TiltakshaverOrgnr = deletedText;
            samtykke.Prosjektnavn = deletedText;
            samtykke.DateDeleted = DateTime.UtcNow;


            if (samtykke.Byggested.Count() > 0)
                _dbContext.TiltakshaversSamtykkeByggesteds.RemoveRange(samtykke.Byggested);

            if (samtykke.Vedlegg.Count() > 0)
                _dbContext.TiltakshaversSamtykkeVedleggs.RemoveRange(samtykke.Vedlegg);

            if (samtykke.Tiltaktypes.Count() > 0)
                samtykke.Tiltaktypes.Clear();
                //_dbContext.TiltakshaversSamtykkeTiltaktypes.RemoveRange(samtykke.Tiltaktypes);            

            _dbContext.SaveChanges();
        }

        public void SamtykkeGodkjent(string samtykkeId, string altinnArkivreferanse, string tiltakshaversOrganisasjonsnummer) =>
            SamtykkeGodkjent(ParseSamtykkeId(samtykkeId), altinnArkivreferanse, tiltakshaversOrganisasjonsnummer);

        public void SamtykkeGodkjent(Guid samtykkeId, string altinnArkivreferanse, string tiltakshaversOrganisasjonsnummer)
        {
            var samtykke = GetTiltakshaversSamtykke(samtykkeId);
            samtykke.AltinnArkivreferanse = altinnArkivreferanse;
            samtykke.SignertTiltakshaverOrgnr = tiltakshaversOrganisasjonsnummer;
            samtykke.Status = TiltakshaverSamtykkeConstants.Signert;

            _dbContext.Entry(samtykke).State = EntityState.Modified;
            _dbContext.SaveChanges();
        }

        public void SamtykkeSignert(string id) => SamtykkeSignert(ParseSamtykkeId(id));

        public void SamtykkeSignert(Guid samtykkeId)
        {
            var samtykke = GetTiltakshaversSamtykke(samtykkeId);
            samtykke.Status = TiltakshaverSamtykkeConstants.Signert;

            _dbContext.Entry(samtykke).State = EntityState.Modified;
            _dbContext.SaveChanges();
        }

        public void ArchiveSamtykke(string samtykkeId) => ArchiveSamtykke(ParseSamtykkeId(samtykkeId));

        public void ArchiveSamtykke(Guid samtykkeId)
        {
            var samtykke = GetTiltakshaversSamtykke(samtykkeId);
            if (samtykke != null)
            {
                // Vi må ha en oversikt over vedleggene som er signert av Tiltakshaver
                //if (samtykke.Vedlegg != null && samtykke.Vedlegg.Any())
                //    samtykke.Vedlegg.Clear();

                samtykke.Status = TiltakshaverSamtykkeConstants.Arkivert;
                _dbContext.Entry(samtykke).State = EntityState.Modified;
                _dbContext.SaveChanges();
            }
        }

        public bool SamtykkeExists(Guid id)
        {
            return _dbContext.TiltakshaversSamtykkes.Any(s => s.Id == id);
        }

        public bool SamtykkeExists(string samtykkeId) => SamtykkeExists(ParseSamtykkeId(samtykkeId));

        public string GetSamtykkeTiltakshaverId(string archiveReference)
        {
            var samtykke = _dbContext.TiltakshaversSamtykkes.SingleOrDefault(s => s.AltinnArkivreferanse.Equals(archiveReference));
            return samtykke?.Id.ToString(); ;
        }

        public void SaveReporteeToSamtykke(string archiverReference, string reportee)
        {
            var samtykke = _dbContext.TiltakshaversSamtykkes.SingleOrDefault(s => s.AltinnArkivreferanse.Equals(archiverReference));
            if (samtykke != null)
            {
                if (Utils.TextObfuscation.IsSSN(reportee))
                {
                    var result = _textEncryptionService.Encrypt(reportee);
                    samtykke.SignertTiltakshaverFnr = result;
                }
                else
                {
                    samtykke.SignertTiltakshaverOrgnr = reportee;
                }

                _dbContext.Entry(samtykke).State = EntityState.Modified;
                _dbContext.SaveChanges();
            }
            else
                throw new ArgumentException($"Samtykke for archiveReference {archiverReference} not found in database table SamtykkeTiltakshaver");
        }

        public string GetReporteePersonalNumberFromSamtykke(Guid id)
        {
            string ssn;

            var samtykke = _dbContext.TiltakshaversSamtykkes.SingleOrDefault(s => s.Id.Equals(id));
            if (samtykke != null && !string.IsNullOrWhiteSpace(samtykke.SignertTiltakshaverFnr))
            {
                ssn = _textEncryptionService.Decrypt(samtykke.SignertTiltakshaverFnr);
            }
            else
                throw new ArgumentException($"Samtykke for ID {id} not found in database table SamtykkeTiltakshaver");

            return ssn;
        }

        public void UpdateVedleggStatus(string id, bool uploaded) => UpdateVedleggStatus(ParseSamtykkeId(id), uploaded);

        public void UpdateVedleggStatus(Guid id, bool uploaded)
        {
            var samtykke = GetTiltakshaversSamtykke(id);

            foreach (var vedlegg in samtykke.Vedlegg)
            {
                vedlegg.Uploaded = uploaded;
            }

            _dbContext.Entry(samtykke).State = EntityState.Modified;
            _dbContext.SaveChanges();
        }

        bool ITiltakshaverSamtykkeService.PersonalNumberExistForTiltakshaverId(Guid id)
        {
            bool output = false;
            var samtykke = _dbContext.TiltakshaversSamtykkes.SingleOrDefault(s => s.Id.Equals(id));
            if (samtykke != null && !string.IsNullOrWhiteSpace(samtykke.SignertTiltakshaverFnr))
            {
                output = true;
            }
            return output;
        }

        private Guid ParseSamtykkeId(string samtykkeId)
        {
            Guid parsedSamtykkeId;
            if (!Guid.TryParse(samtykkeId, out parsedSamtykkeId))
                throw new ArgumentException("Unable to parse samtykkeId to Guid");

            return parsedSamtykkeId;
        }
    }
}