﻿using KommIT.FIKS.AdapterAltinnSvarUt.Services.Checklist;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services
{
    public class ValidationResult
    {
        /// <summary>
        /// Number of errors
        /// </summary>
        public int Errors;

        /// <summary>
        /// Number of warnings
        /// </summary>
        public int Warnings;

        /// <summary>
        /// Liste over tiltakstyper i Søknad for å filtrere regler som ikke gjelder for tiltak
        /// </summary>
        public List<string> tiltakstyperISoeknad = new List<string>();

        public List<string> _checklistErrors = new List<string>();

        /// <summary>
        /// Type søknad for å bruke jf. sjekklistene for å hente Tiltakstype
        /// </summary>
        public string Soknadtype;

        internal ChecklistUtilities _checklistUtilities;

        public List<ValidationMessage> messages { get { return _messages.ToList(); } }
        private ConcurrentBag<ValidationMessage> _messages;
        public List<ValidationRule> rulesChecked { get { return _rulesChecked.ToList(); } }
        private ConcurrentBag<ValidationRule> _rulesChecked;
        public List<ChecklistAnswer> prefillChecklist { get { return _prefillChecklist.ToList(); } }
        private ConcurrentBag<ChecklistAnswer> _prefillChecklist;

        public ValidationResult()
        {
            Warnings = 0;
            Errors = 0;
            _messages = new ConcurrentBag<ValidationMessage>();
            _rulesChecked = new ConcurrentBag<ValidationRule>();
            _prefillChecklist = new ConcurrentBag<ChecklistAnswer>();
            _checklistUtilities = new ChecklistUtilities();
        }

        public ValidationResult(int warningCount, int errorCount)
        {
            Warnings = warningCount;
            Errors = errorCount;
            _messages = new ConcurrentBag<ValidationMessage>();
            _rulesChecked = new ConcurrentBag<ValidationRule>();
            _prefillChecklist = new ConcurrentBag<ChecklistAnswer>();
        }

        public void AddRule(string skjema, string id, string checklistReference, string message, string xpath, string messagetype, string preCondition)
        {
            var xpathField = ReplaceFormWithSkjema(xpath, skjema);
            var preConditionField = ReplaceFormWithSkjema(preCondition, skjema);

            _rulesChecked.Add(new ValidationRule
            {
                id = id,
                messagetype = messagetype,
                message = message,
                checklistReference = checklistReference,
                preCondition = preConditionField,
                xpathField = xpathField
            });
        }

        internal string ReplaceFormWithSkjema(string xpathField, string skjema)
        {
            string skjemaRefpath = "";
            if (!string.IsNullOrEmpty(xpathField))
            {
                if (xpathField.Contains("form"))
                {
                    var regex = new Regex(Regex.Escape("form"));
                    skjemaRefpath = regex.Replace(xpathField, xpathField.Contains("form/") ? $"{skjema}" : $"{skjema}/", 1);
                    //skjemaRefpath = xpathField?.Replace("form", xpathField.Contains("form/") ? $"{skjema}" : $"{skjema}/",1);
                }
                else
                {
                    skjemaRefpath = xpathField;
                }
            }

            return skjemaRefpath;
        }

        public void AddRule(string id, string message, string preCondition, string messagetype, string checklistReference)
        {
            _rulesChecked.Add(new ValidationRule { id = id, messagetype = messagetype, message = message, checklistReference = checklistReference, preCondition = preCondition });
        }

        public ValidationRule GetRule(string id)
        {
            return _rulesChecked?.FirstOrDefault(r => r.id == id);// .Find(r => r.id == id);
        }

        //----
        public void AddMessage(string ruleId, string xpathField = null, string skjema = null, string[] ruleMessagesParameters = null)
        {
            var rule = GetRule(ruleId);
            if (rule == null)
            {
                _messages.Add(new ValidationMessage { messagetype = "WARNING", message = "Meldingskode ikke funnet", reference = ruleId, xpathField = "" });
                return;
            }

            var message = ruleMessagesParameters != null ? string.Format(rule.message, ruleMessagesParameters) : rule.message;
            var skjemaRefpath = string.Empty;
            if (!string.IsNullOrEmpty(xpathField))
            {
                if (xpathField.Contains("form"))
                {
                    skjemaRefpath = xpathField?.Replace("form/", skjema + "/");
                }
                else
                {
                    skjemaRefpath = xpathField;
                }
            }

            if (rule.messagetype.Trim().Equals("ERROR", StringComparison.InvariantCultureIgnoreCase))
            {
                Interlocked.Increment(ref Errors);
                _messages.Add(new ValidationMessage { messagetype = "ERROR", message = message, reference = ruleId, xpathField = skjemaRefpath });
            }
            if (rule.messagetype.Trim().Equals("WARNING", StringComparison.InvariantCultureIgnoreCase))
            {
                Interlocked.Increment(ref Warnings);
                _messages.Add(new ValidationMessage { messagetype = "WARNING", message = message, reference = ruleId, xpathField = skjemaRefpath });
            }
        }

        public void UpdateValidationMessage(string ruleId, string checklistReference)
        {
            if (string.IsNullOrEmpty(ruleId) || string.IsNullOrEmpty(checklistReference))
                return;

            ValidationRule rule = null;
            string ruleNumber = null;
            if (ruleId.Contains("."))
            {
                var ruleNumbers = ruleId.Split('.').ToList();
                ruleNumber = string.Concat(ruleNumbers[0], ".", "0", ".", "1");
                rule = GetRule(ruleNumber);
            }

            _checklistErrors.Add(checklistReference);

            if (string.IsNullOrEmpty(ruleNumber))
                return;

            string message;
            var points = string.Join(", ", _checklistErrors);

            if (rule == null)
            {
                message = $"En teknisk feil gjør at vi ikke kan bekrefte informasjonen du har oppgitt. Vi anbefaler derfor at du vurderer om regel nummer ('{points}') i de nasjonale sjekklistene for byggesak gjelder for tiltakstypen(e) du har valgt: https://sjekkliste-bygg.ft.dibk.no/checklist/{Soknadtype}";
            }
            else
            {
                message = string.Format(rule.message, points);
            }

            if (_messages.Any(m => m.reference == ruleNumber))
            {
                _messages.First(m => m.reference == ruleNumber).message = message;
            }
            else
            {
                _messages.Add(new ValidationMessage { messagetype = "WARNING", message = message, reference = ruleNumber, xpathField = "" });
            }
        }

        public void AddMessage(string ruleId, string[] ruleMessagesParameters = null)
        {
            var rule = GetRule(ruleId);
            if (rule == null)
            {
                _messages.Add(new ValidationMessage { messagetype = "WARNING", message = "Meldingskode ikke funnet", reference = ruleId, xpathField = "" });
                return;
            }

            bool? isRuleRelevatForApplication = null;
            if (!string.IsNullOrEmpty(rule.checklistReference) && !string.IsNullOrEmpty(Soknadtype) && tiltakstyperISoeknad != null && tiltakstyperISoeknad.Any())
            {
                isRuleRelevatForApplication = _checklistUtilities.IsRuleRelevantForApplication(rule.checklistReference, Soknadtype, tiltakstyperISoeknad);
                if (isRuleRelevatForApplication == null)
                {
                    UpdateValidationMessage(ruleId, rule.checklistReference);
                }
            }

            if (isRuleRelevatForApplication.GetValueOrDefault() || isRuleRelevatForApplication == null)
            {
                var messageWithParameters = ruleMessagesParameters != null ? string.Format(rule.message, ruleMessagesParameters) : rule.message;

                //replace check list reference in message when it fiends check list reference in the rule and reference in text message
                string message;
                var checkListTextToReplace = "<SjkPkt>";
                if (messageWithParameters.Contains(checkListTextToReplace))
                {
                    if (string.IsNullOrEmpty(rule.checklistReference))
                        message = messageWithParameters.Replace(checkListTextToReplace, "");
                    else
                        message = messageWithParameters.Replace(checkListTextToReplace, $", jfr. nasjonal sjekkliste punkt {rule.checklistReference}");
                }
                else
                {
                    message = messageWithParameters;
                }

                if (rule.messagetype.Trim().Equals("ERROR", StringComparison.InvariantCultureIgnoreCase))
                {
                    Interlocked.Increment(ref Errors);
                    _messages.Add(new ValidationMessage { messagetype = "ERROR", message = message, reference = ruleId, xpathField = rule.xpathField });
                }
                if (rule.messagetype.Trim().Equals("WARNING", StringComparison.InvariantCultureIgnoreCase))
                {
                    Interlocked.Increment(ref Warnings);
                    _messages.Add(new ValidationMessage { messagetype = "WARNING", message = message, reference = ruleId, xpathField = rule.xpathField });
                }
            }
        }

        public void AddError(string message, string reference, string xpathField)
        {
            Interlocked.Increment(ref Errors);
            _messages.Add(new ValidationMessage { messagetype = "ERROR", message = message, reference = reference, xpathField = xpathField });
        }

        public void AddError(string message, string reference)
        {
            Interlocked.Increment(ref Errors);
            _messages.Add(new ValidationMessage { messagetype = "ERROR", message = message, reference = reference });
        }

        public void AddWarning(string message, string reference)
        {
            Interlocked.Increment(ref Warnings);
            _messages.Add(new ValidationMessage { messagetype = "WARNING", message = message, reference = reference });
        }

        public void AddWarning(string message, string reference, string xpathField)
        {
            Interlocked.Increment(ref Warnings);
            _messages.Add(new ValidationMessage { messagetype = "WARNING", message = message, reference = reference, xpathField = xpathField });
        }

        public void AddChecklistAnswers(IEnumerable<ChecklistAnswer> checklistAnswers)
        {
            checklistAnswers = checklistAnswers.OrderByDescending(c =>
            {
                var parts = c.checklistReference.Split('.').Select(int.Parse).ToList();
                return parts;
            }, new ChecklistReferenceComparer());

            foreach (ChecklistAnswer answer in checklistAnswers)
                _prefillChecklist.Add(answer);
        }

        public bool HasErrors()
        {
            return !IsOk();
        }

        public bool IsOk()
        {
            return Errors == 0;
        }

        public override string ToString()
        {
            var toStr = string.Empty;

            foreach (var msg in messages)
                toStr = toStr + $"{msg.messagetype}: {msg.message}, iht. {msg.reference}; ";
            return toStr;
        }

    }

    public class ValidationMessage
    {
        public string message;
        public string messagetype;
        public string reference;
        public string xpathField;
    }

    public class ValidationRule
    {
        public string id;
        public string message;
        public string messagetype;
        public string preCondition;
        public string checklistReference;
        public string xpathField;
    }

    public class ChecklistAnswer
    {
        public string checklistReference;
        public string checklistQuestion;
        public bool yesNo;

        //public ChecklistOutcome checklistOutcome;

        public List<string> supportingDataValidationRuleId;
        public string supportingDataXpathField;
        public List<string> supportingDataValues;
    }

    public class ChecklistReferenceComparer : IComparer<List<int>>
    {
        public int Compare(List<int> x, List<int> y)
        {
            int minLength = Math.Min(x.Count, y.Count);

            for (int i = 0; i < minLength; i++)
            {
                if (x[i] != y[i])
                    return x[i].CompareTo(y[i]);
            }

            return x.Count.CompareTo(y.Count);
        }
    }

}