﻿using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using System;
using System.Collections.Generic;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Repositories
{
    public interface IFormMetadataRepository
    {
        void Add(SvarUtForsendelsesStatus shipmentStatus);
        void Complete();
        IEnumerable<FormMetadata> GetFormMetadataBy(DateTime newerThanDateTime, IEnumerable<string> filteredStatuses);
    }
}