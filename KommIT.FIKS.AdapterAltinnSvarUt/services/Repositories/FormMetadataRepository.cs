﻿using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.SearchEngine;
using System;
using System.Collections.Generic;
using System.Linq;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Repositories
{
    public class FormMetadataRepository : IFormMetadataRepository
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly ISearchEngineIndexer _searchEngineIndexer;

        public FormMetadataRepository(ApplicationDbContext dbContext, ISearchEngineIndexer searchEngineIndexer)
        {
            _dbContext = dbContext;
            _searchEngineIndexer = searchEngineIndexer;
        }

        public IEnumerable<FormMetadata> GetFormMetadataBy(DateTime newerThanDateTime, IEnumerable<string> filteredStatuses)
        {
            return _dbContext.FormMetadata.Where(
                f => f.ArchiveTimestamp >= newerThanDateTime
                && f.SvarUtForsendelsesId != null
                && (f.SvarUtForsendelsesStatus == null
                    || !filteredStatuses.Contains(f.SvarUtForsendelsesStatus.ForsendelseStatus)))
                .ToList();
        }

        public void Add(SvarUtForsendelsesStatus shipmentStatus)
        {
            _dbContext.SvarUtShipmentStatuses.Add(shipmentStatus);
        }

        public void Complete()
        {
            var changes = _dbContext.ChangeTracker.Entries()
                .Where(e => e.State == System.Data.Entity.EntityState.Modified || e.State == System.Data.Entity.EntityState.Added)
                .Select(x => x.Entity).ToList();

            _dbContext.SaveChanges();

            var changedSvarUtForsendelseStatuses = changes.Where(t => t is SvarUtForsendelsesStatus).Cast<SvarUtForsendelsesStatus>().ToList();
            AsyncHelper.RunSync(async () => await _searchEngineIndexer.AddManyAsync(changedSvarUtForsendelseStatuses)); //Syncing to Elasticsearch
        }
    }
}