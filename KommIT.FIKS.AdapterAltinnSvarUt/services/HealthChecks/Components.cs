﻿using System.Collections.Generic;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.HealthChecks
{
    public class Components
    {
        public static readonly Dictionary<string, string> _components = new Dictionary<string, string>()
        {
            { "altinn-correspondence", "dsqbc5kg16yw" },
            { "altinn-download-stream", "yxvg13q0l0yx" },
            { "altinn-notification", "hcxmch9ny38v" },
            { "altinn-prefill", "54bnmc5vxs8y" },
            { "bring-postal-code", "5nc4rhbrlhsk" },
            { "db-connection", "rnqhg08c7yz7"},
            { "geonorge-arbeidstilsynet", "jgz3rydbnmnb" },
            { "geonorge-ebyggesak", "8pk4ncf6xcz9"},
            { "geonorge-main-code-list", "z3rbk6w8z12m" },
            { "geonorge-plansak", "w09sj7lzljwr" },
            { "matrikkel_bygning", "tvvf0vjkf8z4" },
            { "matrikkel_enhet", "pjzd548wr2nq" },
            { "matrikkel_vegadresse", "c7h8jpy66d2c"},
            { "svarut-forsendelse", "9gv6lxj9hx0n" },
            {"db-ef-connection", "fdf45hnkrt5b" }
        };
    }
}