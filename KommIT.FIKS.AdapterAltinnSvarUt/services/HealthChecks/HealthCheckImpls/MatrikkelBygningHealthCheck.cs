﻿using KommIT.FIKS.AdapterAltinnSvarUt.bygningservice;
using System;
using Serilog;
using System.ServiceModel;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.HealthChecks.HealthCheckImpls
{
    public class MatrikkelBygningHealthCheck : IHealthCheck
    {
        private const string _description = "Matrikkel bygning";
        private const string _name = "matrikkel_bygning";
        private readonly ILogger _logger;

        public MatrikkelBygningHealthCheck(ILogger logger)
        {
            _logger = logger.ForContext<MatrikkelBygningHealthCheck>();
        }
        public HealthCheckResult CheckStatus()
        {
            var hcStatus = new HealthCheckResult(_name, _description);
            try
            {
                using (var client = new BygningServiceClient())
                {
                    hcStatus.Address = client.Endpoint.Address.Uri.AbsoluteUri;
                    client.ClientCredentials.UserName.UserName = System.Configuration.ConfigurationManager.AppSettings["Matrikkel.Username"];
                    client.ClientCredentials.UserName.Password = System.Configuration.ConfigurationManager.AppSettings["Matrikkel.Password"];
                    client.Endpoint.Binding.SendTimeout = new TimeSpan(0, 0, MatrikkelHealthCheckConfig.TimeoutSeconds);

                    try
                    {
                        var result = client.findBygningInfoObjekter(new ByggsokModel()
                        {
                            bygningsnummer = 165711807,
                            bygningsnummerSpecified = true,
                            gardsnummer = "49",
                            bruksnummer = "130",
                            festenummer = 0,
                            festenummerSpecified = true,
                            seksjonsnummer = 0,
                            seksjonsnummerSpecified = true
                        }, new MatrikkelContext()
                        {
                            klientIdentifikasjon = "DibkFellestjenesterBygg",
                            brukOriginaleKoordinater = true,
                            locale = "no_NO_B",
                            koordinatsystemKodeId = new KoordinatsystemKodeId() { value = 10 },
                            systemVersion = "3.13",
                            snapshotVersion = new Timestamp() { timestamp = new DateTime(9999, 1, 1) }
                        });

                        hcStatus.CheckStatus = HealthCheckStatus.Healthy;
                        client.Close();
                    }
                    catch (Exception)
                    { 
                        client.Abort();
                        throw;
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Health check error");
                hcStatus.CheckStatus = HealthCheckStatus.Unhealthy;
                hcStatus.ExceptionMessage = ex.Message;
            }

            return hcStatus;
        }
    }
}