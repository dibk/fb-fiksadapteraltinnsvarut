﻿using KommIT.FIKS.AdapterAltinnSvarUt.Services.WS.AltinnCorrespondenceAgency;
using Serilog;
using System;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.HealthChecks.HealthCheckImpls
{
    public class AltinnCorrespondenceHealthCheck : IHealthCheck
    {
        private readonly ILogger _logger;
        private const string _description = "Altinn Correspondence API";
        private const string _name = "altinn-correspondence";

        public AltinnCorrespondenceHealthCheck(ILogger logger)
        {
            _logger = logger.ForContext<AltinnCorrespondenceHealthCheck>();
        }
        public HealthCheckResult CheckStatus()
        {
            var hcStatus = new HealthCheckResult(_name, _description);
            try
            {
                using (var client = new CorrespondenceAgencyExternalBasicClient())
                {
                    hcStatus.Address = client.Endpoint.Address.Uri.AbsoluteUri;
                    try
                    {
                        client.Test();
                        hcStatus.CheckStatus = HealthCheckStatus.Healthy;
                        client.Close();
                    }
                    catch (Exception ex)
                    {
                        client.Abort();
                        throw;
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Health check error");
                hcStatus.CheckStatus = HealthCheckStatus.Unhealthy;
                hcStatus.ExceptionMessage = ex.Message;
            }
            return hcStatus;
        }
    }
}