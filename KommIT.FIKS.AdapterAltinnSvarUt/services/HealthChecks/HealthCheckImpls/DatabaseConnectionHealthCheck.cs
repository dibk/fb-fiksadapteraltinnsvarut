﻿using Microsoft.Data.SqlClient;
using Serilog;
using System;
using System.Configuration;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.HealthChecks.HealthCheckImpls
{
    public class DatabaseConnectionHealthCheck : IHealthCheck
    {
        private readonly ILogger _logger;
        private readonly string _connectionString;
        private const string _description = "Database connection";
        private const string _name = "db-connection";
        private const string _sql = "select '1'";

        public DatabaseConnectionHealthCheck(ILogger logger)
        {
            this._logger = logger.ForContext<DatabaseEntityFrameworkHealthCheck>();
            _connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        }

        public HealthCheckResult CheckStatus()
        {
            var hcStatus = new HealthCheckResult(_name, _description);
            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandText = _sql;
                        _ = command.ExecuteScalar();
                    }

                    hcStatus.CheckStatus = HealthCheckStatus.Healthy;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Health check error");
                hcStatus.CheckStatus = HealthCheckStatus.Unhealthy;
                hcStatus.ExceptionMessage = ex.Message;
            }
            return hcStatus;
        }
    }
}