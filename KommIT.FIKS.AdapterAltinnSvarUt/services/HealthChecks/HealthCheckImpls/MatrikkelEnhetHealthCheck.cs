﻿using KommIT.FIKS.AdapterAltinnSvarUt.matrikkelenhetservice;
using Serilog;
using System;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.HealthChecks.HealthCheckImpls
{
    public class MatrikkelEnhetHealthCheck : IHealthCheck
    {
        private const string _description = "Matrikkel finn matrikkelenhet";
        private const string _name = "matrikkel_enhet";
        private readonly ILogger _logger;

        public MatrikkelEnhetHealthCheck(ILogger logger)
        {
            _logger = logger.ForContext<MatrikkelBygningHealthCheck>();
        }

        public HealthCheckResult CheckStatus()
        {
            var hcStatus = new HealthCheckResult(_name, _description);
            try
            {
                using (var client = new MatrikkelenhetServiceClient())
                {
                    hcStatus.Address = client.Endpoint.Address.Uri.AbsoluteUri;
                    client.ClientCredentials.UserName.UserName = System.Configuration.ConfigurationManager.AppSettings["Matrikkel.Username"];
                    client.ClientCredentials.UserName.Password = System.Configuration.ConfigurationManager.AppSettings["Matrikkel.Password"];
                    client.Endpoint.Binding.SendTimeout = new TimeSpan(0, 0, MatrikkelHealthCheckConfig.TimeoutSeconds);
                    try
                    {
                        var result = client.findMatrikkelenhet(new MatrikkelenhetIdent()
                        {
                            kommuneIdent = new KommuneIdent()
                            { kommunenummer = "0101" },
                            gardsnummer = 1,
                            gardsnummerSpecified = true,
                            bruksnummer = 2,
                            bruksnummerSpecified = true,
                            festenummer = 0,
                            festenummerSpecified = true,
                            seksjonsnummer = 0,
                            seksjonsnummerSpecified = true
                        }, new MatrikkelContext()
                        {
                            klientIdentifikasjon = "DibkFellestjenesterBygg",
                            brukOriginaleKoordinater = true,
                            locale = "no_NO_B",
                            koordinatsystemKodeId = new KoordinatsystemKodeId() { value = 32 },
                            systemVersion = "3.13",
                            snapshotVersion = new Timestamp() { timestamp = new DateTime(9999, 1, 1) }
                        });

                        hcStatus.CheckStatus = HealthCheckStatus.Healthy;
                        client.Close();
                    }
                    catch (Exception)
                    {
                        client.Abort();
                        throw;
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Health check error");
                hcStatus.CheckStatus = HealthCheckStatus.Unhealthy;
                hcStatus.ExceptionMessage = ex.Message;
            }
            return hcStatus;
        }
    }
}