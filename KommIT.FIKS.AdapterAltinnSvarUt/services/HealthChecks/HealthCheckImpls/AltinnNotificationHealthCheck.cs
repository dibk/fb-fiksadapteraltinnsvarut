﻿using KommIT.FIKS.AdapterAltinnSvarUt.Services.WS.AltinnNotification;
using Serilog;
using System;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.HealthChecks.HealthCheckImpls
{
    public class AltinnNotificationeHealthCheck : IHealthCheck
    {
        private readonly ILogger _logger;
        private const string _description = "Altinn Notification API";
        private const string _name = "altinn-notification";

        public AltinnNotificationeHealthCheck(ILogger logger)
        {
            _logger = logger.ForContext<AltinnNotificationeHealthCheck>();
        }
        public HealthCheckResult CheckStatus()
        {
            var hcStatus = new HealthCheckResult(_name, _description);
            try
            {                
                using (var client = new NotificationAgencyExternalBasicClient())
                {
                    hcStatus.Address = client.Endpoint.Address.Uri.AbsoluteUri;
                    try
                    {
                        client.Test();
                        hcStatus.CheckStatus = HealthCheckStatus.Healthy;
                        client.Close();
                    }
                    catch (Exception)
                    {
                        client.Abort();
                        throw;
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Health check error");
                hcStatus.CheckStatus = HealthCheckStatus.Unhealthy;
                hcStatus.ExceptionMessage = ex.Message;
            }
            return hcStatus;
        }
    }
}