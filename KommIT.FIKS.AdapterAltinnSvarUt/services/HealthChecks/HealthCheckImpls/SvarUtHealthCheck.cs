﻿using KommIT.FIKS.AdapterAltinnSvarUt.Services.Svarut.REST;
using Serilog;
using System;
using System.Configuration;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.HealthChecks.HealthCheckImpls
{
    public class SvarUtHealthCheck : IHealthCheck
    {
        private const string _description = "SvarUt Forsendelse";
        private const string _name = "svarut-forsendelse";
        private readonly ILogger _logger = Log.ForContext<SvarUtHealthCheck>();
        private readonly SvarUtClientFactory _svarUtClientFactory;

        public SvarUtHealthCheck(SvarUtClientFactory svarUtClientFactory)
        {
            _svarUtClientFactory = svarUtClientFactory;
        }

        public HealthCheckResult CheckStatus()
        {
            var hcStatus = new HealthCheckResult(_name, _description);
            try
            {
                hcStatus.Address = $"{ConfigurationManager.AppSettings["SvarUt:BaseUri"]}/tjenester/api/forsendelse/v1/";
                var result = AsyncHelper.RunSync(async () => await _svarUtClientFactory.CreateSvarUtClient().GetForsendelsesTyperAsync());                
                hcStatus.CheckStatus = HealthCheckStatus.Healthy;
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Health check error");
                hcStatus.CheckStatus = HealthCheckStatus.Unhealthy;
                hcStatus.ExceptionMessage = ex.Message;
            }
            return hcStatus;
        }
    }
}