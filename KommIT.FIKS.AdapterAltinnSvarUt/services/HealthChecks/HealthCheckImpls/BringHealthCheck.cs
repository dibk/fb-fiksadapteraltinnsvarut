﻿using KommIT.FIKS.AdapterAltinnSvarUt.Services.Caching;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.PostalCode;
using System;
using Serilog;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.HealthChecks.HealthCheckImpls
{
    public class BringHealthCheck : IHealthCheck
    {
        private const string _description = "Bring postal code API";
        private const string _name = "bring-postal-code";
        private readonly ILogger _logger;

        public BringHealthCheck(ILogger logger)
        {
            _logger = logger.ForContext<BringHealthCheck>();
        }
        public HealthCheckResult CheckStatus()
        {
            var hcStatus = new HealthCheckResult(_name, _description);

            try
            {
            using (var postalCodeProvider = new BringPostalCodeProvider(new NullCache()))
            {
                    hcStatus.Address = postalCodeProvider.URL;
                postalCodeProvider.ValidatePostnr("3802", "NO");
                    hcStatus.CheckStatus = HealthCheckStatus.Healthy;
                }                

            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Health check error");
                hcStatus.CheckStatus = HealthCheckStatus.Unhealthy;
                hcStatus.ExceptionMessage = ex.Message;
            }

            return hcStatus;
        }
    }
}