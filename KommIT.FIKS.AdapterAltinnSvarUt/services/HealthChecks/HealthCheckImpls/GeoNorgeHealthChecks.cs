﻿using DIBK.FBygg.Altinn.MapperCodelist;
using System;
using Serilog;
using System.Configuration;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.HealthChecks.HealthCheckImpls
{
    public abstract class GeoNorgeHealthCheck : IHealthCheck
    {
        private readonly ICodeListDownloader _codeListDownloader;
        private readonly ILogger _logger;
        protected abstract string _description { get; }// = "GeoNorge main code list registry";
        protected abstract string _name { get; }// = "geonorge-main-code-list-registry";
        protected abstract string _codelistUrl { get; }
        public GeoNorgeHealthCheck(ICodeListDownloader codeListDownloader, ILogger logger)
        {
            _codeListDownloader = codeListDownloader;
            _logger = logger;
        }
        public HealthCheckResult CheckStatus()
        {

            var hcStatus = new HealthCheckResult(_name, _description);
            hcStatus.Address = _codelistUrl;
            try
            {
                var result = _codeListDownloader.DownloadCodeList(_codelistUrl);
                if (!string.IsNullOrEmpty(result))
                    hcStatus.CheckStatus = HealthCheckStatus.Healthy;
                else
                    hcStatus.CheckStatus = HealthCheckStatus.Warning;
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Health check error");
                hcStatus.CheckStatus = HealthCheckStatus.Unhealthy;
                hcStatus.ExceptionMessage = ex.Message;
            }

            return hcStatus;
        }

    }

    public class GeoNorgeMainCodeListHealthCheck : GeoNorgeHealthCheck
    {
        public GeoNorgeMainCodeListHealthCheck(ICodeListDownloader codeListDownloader, ILogger logger) : base(codeListDownloader, logger) { }
        protected override string _description => "GeoNorge main code list registry"; 
        protected override string _name  => "geonorge-main-code-list"; 
        protected override string _codelistUrl => ConfigurationManager.AppSettings["GeonorgeRegistryByggesoknadCodeListsUrl"];
    }

    //public class GeonorgeSosiCodeListHealthCheck : GeoNorgeHealthCheck
    //{
    //    protected override string _description => "Geonorge sosi code lists";
    //    protected override string _name => "geonorge-sosi";
    //    protected override string _codelistUrl => ConfigurationManager.AppSettings["GeonorgeRegistryMunicipalityApiUrl"];
    //    public GeonorgeSosiCodeListHealthCheck(ICodeListDownloader codeListDownloader, ILogger logger) : base(codeListDownloader, logger) { }
    //}

    public class GeonorgePlansakCodeListHealthCheck : GeoNorgeHealthCheck
    {
        protected override string _description => "Geonorge plansak code lists";
        protected override string _name => "geonorge-plansak";
        protected override string _codelistUrl => ConfigurationManager.AppSettings["GeonorgeRegistryNabovarselPlanCodeListUrl"];
        public GeonorgePlansakCodeListHealthCheck(ICodeListDownloader codeListDownloader, ILogger logger) : base(codeListDownloader, logger) { }
    }

    public class GeonorgeEbyggesakCodeListHealthCheck : GeoNorgeHealthCheck
    {
        public GeonorgeEbyggesakCodeListHealthCheck(ICodeListDownloader codeListDownloader, ILogger logger) : base(codeListDownloader, logger) { }
        protected override string _description => "Geonorge ebyggesak code lists";
        protected override string _name => "geonorge-ebyggesak";
        protected override string _codelistUrl => ConfigurationManager.AppSettings["GeonorgeRegistryEbyggesakCodeListUrl"];
    }

    public class GeonorgeArbeidstilsynetHealthCheck : GeoNorgeHealthCheck
    {
        public GeonorgeArbeidstilsynetHealthCheck(ICodeListDownloader codeListDownloader, ILogger logger) : base(codeListDownloader, logger) { }
        protected override string _description => "Geonorge arbeidstilsynet code lists"; 
        protected override string _name => "geonorge-arbeidstilsynet";
        protected override string _codelistUrl => ConfigurationManager.AppSettings["GeonorgeRegistryArbeidstilsynetCodeListUrl"];
    }
}