﻿using KommIT.FIKS.AdapterAltinnSvarUt.Services.WS.DownloadStreamAttachment;
using Serilog;
using System;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.HealthChecks.HealthCheckImpls
{
    public class AltinnDownloadStreamHealthCheck : IHealthCheck
    {
        private readonly ILogger _logger;
        private const string _description = "Altinn Download Stream API";
        private const string _name = "altinn-download-stream";

        public AltinnDownloadStreamHealthCheck(ILogger logger)
        {
            _logger = logger.ForContext<AltinnDownloadStreamHealthCheck>();
        }
        public HealthCheckResult CheckStatus()
        {
            var hcStatus = new HealthCheckResult(_name, _description);
            try
            {
                using (var client = new ServiceOwnerArchiveExternalStreamedBasicClient())
                {
                    hcStatus.Address = client.Endpoint.Address.Uri.AbsoluteUri;
                    try
                    {
                        client.Test();
                        hcStatus.CheckStatus = HealthCheckStatus.Healthy;
                        client.Close();
                    }
                    catch (Exception ex)
                    {
                        client.Abort();
                        throw;
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Health check error");
                hcStatus.CheckStatus = HealthCheckStatus.Unhealthy;
                hcStatus.ExceptionMessage = ex.Message;
            }
            return hcStatus;
        }
    }
}