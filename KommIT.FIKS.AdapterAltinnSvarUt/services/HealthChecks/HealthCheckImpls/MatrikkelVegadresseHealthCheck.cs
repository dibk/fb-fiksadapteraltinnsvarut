﻿using KommIT.FIKS.AdapterAltinnSvarUt.adresseservice;
using Serilog;
using System;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.HealthChecks.HealthCheckImpls
{
    public class MatrikkelVegadresseHealthCheck : IHealthCheck
    {
        private const string _description = "Matrikkel finn adresser";
        private const string _name = "matrikkel_vegadresse";
        private readonly ILogger _logger;

        public MatrikkelVegadresseHealthCheck(ILogger logger)
        {
            _logger = logger.ForContext<MatrikkelVegadresseHealthCheck>();
        }

        public HealthCheckResult CheckStatus()
        {
            var hcStatus = new HealthCheckResult(_name, _description);
            try
            {
                using (var client = new AdresseServiceClient())
                {
                    hcStatus.Address = client.Endpoint.Address.Uri.AbsoluteUri;
                    client.ClientCredentials.UserName.UserName = System.Configuration.ConfigurationManager.AppSettings["Matrikkel.Username"];
                    client.ClientCredentials.UserName.Password = System.Configuration.ConfigurationManager.AppSettings["Matrikkel.Password"];
                    client.Endpoint.Binding.SendTimeout = new TimeSpan(0, 0, MatrikkelHealthCheckConfig.TimeoutSeconds);

                    try
                    {
                        var result = client.findAdresser(new AdressesokModel()
                        {
                            kommunenummer = "0821",
                            adressetype = Adressetype.VEGADRESSE,
                            adressetypeSpecified = true,
                            adressenavn = "Svingen",
                            husnummer = "25",
                            husnummertype = Husnummertype.LIKE_NUMMER,
                            husnummertypeSpecified = true,
                            gardsnummer = "49",
                            bruksnummer = "130",
                            festenummer = 0,
                            festenummerSpecified = true,
                            seksjonsnummer = 0,
                            seksjonsnummerSpecified = true
                        }, new MatrikkelContext()
                        {
                            klientIdentifikasjon = "DibkFellestjenesterBygg",
                            brukOriginaleKoordinater = true,
                            locale = "no_NO_B",
                            koordinatsystemKodeId = new KoordinatsystemKodeId() { value = 32 },
                            systemVersion = "3.13",
                            snapshotVersion = new Timestamp() { timestamp = new DateTime(9999, 1, 1) }
                        });

                        hcStatus.CheckStatus = HealthCheckStatus.Healthy;
                        client.Close();
                    }
                    catch (Exception)
                    {
                        client.Abort();
                        throw;
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Health check error");
                hcStatus.CheckStatus = HealthCheckStatus.Unhealthy;
                hcStatus.ExceptionMessage = ex.Message;
            }
            return hcStatus;
        }
    }
}