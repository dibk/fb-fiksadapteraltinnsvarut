﻿using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using Serilog;
using System;
using System.Linq;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.HealthChecks.HealthCheckImpls
{
    public class DatabaseEntityFrameworkHealthCheck : IHealthCheck
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly ILogger _logger;
        private const string _description = "Database EF connection";
        private const string _name = "db-ef-connection";

        public DatabaseEntityFrameworkHealthCheck(ApplicationDbContext dbContext, ILogger logger)
        {
            this._dbContext = dbContext;
            this._logger = logger.ForContext<DatabaseEntityFrameworkHealthCheck>();
        }

        public HealthCheckResult CheckStatus()
        {
            var hcStatus = new HealthCheckResult(_name, _description);
            try
            {
                hcStatus.Address = _dbContext.Database.Connection.DataSource;
                var dbResult = _dbContext.Database.SqlQuery<string>("select 'test'", "").SingleOrDefault();

                hcStatus.CheckStatus = HealthCheckStatus.Healthy;
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Health check error");
                hcStatus.CheckStatus = HealthCheckStatus.Unhealthy;
                hcStatus.ExceptionMessage = ex.Message;
            }
            return hcStatus;
        }
    }
}