﻿namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.HealthChecks
{
    public enum HealthCheckStatus
    {
        Unknown,
        Unhealthy,
        Healthy,
        Warning
    }
}