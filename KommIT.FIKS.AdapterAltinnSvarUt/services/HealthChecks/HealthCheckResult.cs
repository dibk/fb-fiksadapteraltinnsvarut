﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.HealthChecks
{
    public class HealthCheckResult
    {
        [JsonConverter(typeof(StringEnumConverter))]
        public HealthCheckStatus CheckStatus { get; set; }
        public string Name { get;  }
        public string Description { get;  }
        public string Address { get; set; }
        public long ElapsedMilliseconds { get; set; }
        public string ExceptionMessage { get; set; }
        public string ComponentId { get; set; }

        public HealthCheckResult(string name, string description)
        {
            Name = name;
            Description = description;
        }
    }
}