using KommIT.FIKS.AdapterAltinnSvarUt.Services.Caching;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.HealthChecks.HealthCheckImpls;
using Serilog;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.HealthChecks
{
    public interface IHealthService
    {
        List<HealthCheckResult> PerformHealthChecks();
    }
    public class HealthService : IHealthService
    {
        private readonly ILogger _logger;
        private readonly IEnumerable<IHealthCheck> _healthChecks;
        private ConcurrentBag<HealthCheckResult> _healthCheckResults;
        public HealthService(IEnumerable<IHealthCheck> healthChecks, ILogger logger)
        {
            _healthCheckResults = new ConcurrentBag<HealthCheckResult>();
            _logger = logger.ForContext<HealthService>();
            _healthChecks = healthChecks;
        }

        public List<HealthCheckResult> PerformHealthChecks()
        {
            var cachedHealthData = MemoryCacheService.Instance.GetObject("HealthData");
            if (cachedHealthData != null) 
                return (List<HealthCheckResult>) cachedHealthData;
            
            Parallel.ForEach(_healthChecks, (healthCheck) =>
            {
                var sw = new Stopwatch();
                sw.Start();
                var result = healthCheck.CheckStatus();
                sw.Stop();

                result.ElapsedMilliseconds = sw.ElapsedMilliseconds;
                result.ComponentId = Components._components[result.Name];
                _healthCheckResults.Add(result);
            });

            foreach (var item in _healthCheckResults)
            {
                _logger.Information("Health check for: {healthCheckedComponent} - {healthStatus} - {ElapsedMilliseconds}", item.Name, item.CheckStatus.ToString(), item.ElapsedMilliseconds);
            }
            var l = _healthCheckResults.OrderByDescending(h => h.CheckStatus.ToString()).ThenBy(h => h.Name).ToList();
            MemoryCacheService.Instance.AddOrReplaceCachedItem("HealthData", l, DateTimeOffset.FromUnixTimeSeconds(600));
            return l;
        }
    }
}