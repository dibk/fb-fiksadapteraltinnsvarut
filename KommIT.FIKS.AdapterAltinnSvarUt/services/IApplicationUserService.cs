﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Models.UserAccess;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services
{
    public interface IApplicationUserService
    {
        string GetUserClaims(string type);
        bool IsAdmin();
        bool AccessDetailsPage(ApplicationUser applicationUser, Municipality municipality);
    }
}
