﻿using KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.SampleData;
using no.kxml.skjema.dibk.tiltakutenansvarsrettV3;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.Builders
{
    public class TiltakUtenAnsvarsrettV3Builder
    {
        private readonly TiltakUtenAnsvarsrettType _form;

        public TiltakUtenAnsvarsrettV3Builder()
        {
            _form = new TiltakUtenAnsvarsrettType();
        }

        public TiltakUtenAnsvarsrettType Build()
        {
            return _form;
        }


        internal TiltakUtenAnsvarsrettV3Builder EiendomByggested(string knr = null, string gnr = null, string bnr = null, string fnr = null, string snr = null, string kommunenavn = null, string bolignummer = null, string bygningsnummer = null
            , string adresselinje1 = null, string gatenavn = null, string husnr = null, string postnr = null, string poststed = null)
        {
            var eiendomByggested = new EiendomType();
            eiendomByggested.bolignummer = bolignummer;
            eiendomByggested.kommunenavn = kommunenavn;
            eiendomByggested.eiendomsidentifikasjon = new MatrikkelnummerType()
            {
                kommunenummer = knr,
                gaardsnummer = gnr,
                bruksnummer = bnr,
                festenummer = fnr,
                seksjonsnummer = snr
            };

            eiendomByggested.bygningsnummer = bygningsnummer;
            if (!string.IsNullOrEmpty(adresselinje1) || !string.IsNullOrEmpty(gatenavn) || !string.IsNullOrEmpty(husnr))
            {
                eiendomByggested.adresse = new EiendommensAdresseType()
                {
                    adresselinje1 = adresselinje1,
                    gatenavn = gatenavn,
                    husnr = husnr,
                    postnr = postnr,
                    poststed = poststed
                };
            }

            List<EiendomType> eiendomTypes;
            if (_form.eiendomByggested == null)
            {
                eiendomTypes = new List<EiendomType>() { eiendomByggested };
            }
            else
            {
                eiendomTypes = _form.eiendomByggested.ToList();
                eiendomTypes.Add(eiendomByggested);
            }
            _form.eiendomByggested = eiendomTypes.ToArray();

            return this;
        }

        public TiltakUtenAnsvarsrettV3Builder BeskrivelseAvTiltak(string foelgebrev = null, string tiltakTypeKodeverdi = null, string tiltakTypeKodebeskrivelse = null, string typeBeskrivelse = null)
        {

            _form.beskrivelseAvTiltak = _form.beskrivelseAvTiltak ?? new TiltakType();
            _form.beskrivelseAvTiltak.type = _form.beskrivelseAvTiltak.type ?? new TypeTiltakType();

            _form.beskrivelseAvTiltak.foelgebrev = foelgebrev;
            if (!string.IsNullOrEmpty(tiltakTypeKodeverdi) || !string.IsNullOrEmpty(tiltakTypeKodebeskrivelse))
            {
                _form.beskrivelseAvTiltak.type.type = new KodeType()
                {
                    kodeverdi = tiltakTypeKodeverdi,
                    kodebeskrivelse = tiltakTypeKodebeskrivelse
                };
            }

            _form.beskrivelseAvTiltak.type.beskrivelse = typeBeskrivelse;

            return this;
        }
        public TiltakUtenAnsvarsrettV3Builder BeskrivelseAvTiltakBruk(string naeringsgruppeKodeverdi = null, string naeringsgruppeKodebeskrivelse = null, string bygningstypeKodeverdi = null, string bygningstypeKodebeskrivelse = null, string tiltaksformaalKodeverdi = null, string tiltaksformaalKodebeskrivelse = null
            , string beskrivPlanlagtFormaal = null)
        {
            _form.beskrivelseAvTiltak = _form.beskrivelseAvTiltak ?? new TiltakType();

            if (!string.IsNullOrEmpty(naeringsgruppeKodeverdi) || !string.IsNullOrEmpty(naeringsgruppeKodebeskrivelse))
            {
                _form.beskrivelseAvTiltak.bruk = _form.beskrivelseAvTiltak.bruk ?? new FormaalType();

                _form.beskrivelseAvTiltak.bruk.naeringsgruppe = new KodeType()
                {
                    kodeverdi = naeringsgruppeKodeverdi,
                    kodebeskrivelse = naeringsgruppeKodebeskrivelse
                };
            }

            if (!string.IsNullOrEmpty(bygningstypeKodeverdi) || !string.IsNullOrEmpty(bygningstypeKodebeskrivelse))
            {
                _form.beskrivelseAvTiltak.bruk = _form.beskrivelseAvTiltak.bruk ?? new FormaalType();
                _form.beskrivelseAvTiltak.bruk.bygningstype = new KodeType()
                {
                    kodeverdi = bygningstypeKodeverdi,
                    kodebeskrivelse = bygningstypeKodebeskrivelse
                };
            }
            if (!string.IsNullOrEmpty(tiltaksformaalKodeverdi) || !string.IsNullOrEmpty(tiltaksformaalKodebeskrivelse))
            {
                _form.beskrivelseAvTiltak.bruk = _form.beskrivelseAvTiltak.bruk ?? new FormaalType();
                var bygningstypeKodeTypes = _form.beskrivelseAvTiltak.bruk.tiltaksformaal?.ToList() ?? new List<KodeType>();
                bygningstypeKodeTypes.Add(new KodeType()
                {
                    kodeverdi = tiltaksformaalKodeverdi,
                    kodebeskrivelse = tiltaksformaalKodebeskrivelse
                });

                _form.beskrivelseAvTiltak.bruk.tiltaksformaal = bygningstypeKodeTypes.ToArray();
            }
            if (!string.IsNullOrEmpty(beskrivPlanlagtFormaal))
            {
                _form.beskrivelseAvTiltak.bruk = _form.beskrivelseAvTiltak.bruk ?? new FormaalType();
                _form.beskrivelseAvTiltak.bruk.beskrivPlanlagtFormaal = beskrivPlanlagtFormaal;
            }

            return this;
        }

        // Tiltakshaver
     public TiltakUtenAnsvarsrettV3Builder Tiltakshaver(string organisasjonsnummer = null, string foedselsnummer = null, string partsTypeKodeverdi = null, string partsTypekodebeskrivelse = null, string navn = null, string adressLine = null, string postnr = null, string landkode = null
                , string poststed = null, string telefonnummer = null, string mobilnummer = null, string epost = null, string kontaktpersonNav = null, string kontaktpersonTel = null, string kontaktpersonEpost = null)
            {
                _form.tiltakshaver = new PartType();

            if (!string.IsNullOrEmpty(partsTypeKodeverdi)|| !string.IsNullOrEmpty(partsTypekodebeskrivelse))
            {
                _form.tiltakshaver.partstype = new KodeType
                {
                    kodeverdi = partsTypeKodeverdi,
                    kodebeskrivelse = partsTypekodebeskrivelse
                };
            }
            _form.tiltakshaver.foedselsnummer = foedselsnummer;
            _form.tiltakshaver.organisasjonsnummer = organisasjonsnummer;
            _form.tiltakshaver.navn = navn;
            if (!string.IsNullOrEmpty(adressLine) || !string.IsNullOrEmpty(postnr) || !string.IsNullOrEmpty(landkode) || !string.IsNullOrEmpty(poststed))
            {
                _form.tiltakshaver.adresse = new EnkelAdresseType
                {
                    adresselinje1 = adressLine,
                    postnr = postnr,
                    poststed = poststed,
                    landkode = landkode
                };
            }
            _form.tiltakshaver.telefonnummer = telefonnummer;
            _form.tiltakshaver.mobilnummer = mobilnummer;
            _form.tiltakshaver.epost = epost;

            if (!string.IsNullOrEmpty(kontaktpersonNav) || !string.IsNullOrEmpty(kontaktpersonTel) || !string.IsNullOrEmpty(kontaktpersonEpost))
            {
                _form.tiltakshaver.kontaktperson = new KontaktpersonType()
                {
                    navn = kontaktpersonNav,
                    telefonnummer = kontaktpersonTel,
                    epost = kontaktpersonEpost
                };

            }
            return this;
        }
        //Dispensasjon
        public TiltakUtenAnsvarsrettV3Builder Dispensasjon(string kodeverdi = null, string kodebeskrivelse = null, string begrunnelse = null, string beskrivelse = null)
        {

            var dispensasjons = _form.dispensasjon?.ToList() ?? new List<DispensasjonType>();

            var dispensajon = new DispensasjonType() { beskrivelse = beskrivelse, begrunnelse = begrunnelse };


            if (!string.IsNullOrEmpty(kodeverdi) || !string.IsNullOrEmpty(kodebeskrivelse))
            {
                dispensajon.dispensasjonstype = new KodeType()
                {
                    kodeverdi = kodeverdi,
                    kodebeskrivelse = kodebeskrivelse
                };

            }
            dispensasjons.Add(dispensajon);

            _form.dispensasjon = dispensasjons.ToArray();

            return this;
        }
        //KommunensSaksnummer
        public TiltakUtenAnsvarsrettV3Builder KommunensSaksnummer(string saksaar = null, string sakssekvensnummer = null)
        {
            _form.kommunensSaksnummer = new SaksnummerType()
            {
                saksaar = saksaar,
                sakssekvensnummer = sakssekvensnummer
            };
            return this;
        }
        // TiltakUtenAnsvarsrettType.VarslingType
        public TiltakUtenAnsvarsrettV3Builder Varsling(bool? foreliggerMerknader = null, string antallMerknader = null, bool? fritattFraNabovarsling = null, string vurderingAvMerknader = null, string soeknadensHjemmeside = null, string soeknadSeesKontaktperson = null)
        {
            _form.varsling = new VarslingType()
            {
                antallMerknader = antallMerknader,
                vurderingAvMerknader = vurderingAvMerknader,
                soeknadensHjemmeside = soeknadensHjemmeside,
                soeknadSeesKontaktperson = soeknadSeesKontaktperson
            };
            if (foreliggerMerknader.HasValue)
            {
                _form.varsling.foreliggerMerknaderSpecified = true;
                _form.varsling.foreliggerMerknader = foreliggerMerknader.Value;
            }
            if (fritattFraNabovarsling.HasValue)
            {
                _form.varsling.fritattFraNabovarslingSpecified = true;
                _form.varsling.fritattFraNabovarsling = fritattFraNabovarsling.Value;
            }
            return this;
        }

        //Metadata
        public TiltakUtenAnsvarsrettV3Builder Metadata(string fraSluttbrukersystem = null, string ftbId = null, string prosjektnavn = null, string sluttbrukersystemUrl = null)
        {
            _form.metadata = new MetadataType()
            {
                fraSluttbrukersystem = fraSluttbrukersystem,
                ftbId = ftbId,
                prosjektnavn = prosjektnavn,
                sluttbrukersystemUrl = sluttbrukersystemUrl
            };
            return this;
        }
        // TiltakUtenAnsvarsrettType.RammerType.Adkomst
        public TiltakUtenAnsvarsrettV3Builder RammebetingelserAdkomst(bool? nyeEndretAdkomst = null, string vegTypeKodeverdi = null, string vegkodebeskrivelse = null, bool? erTillatelseGitt = null)
        {

            _form.rammebetingelser = _form.rammebetingelser ?? new RammerType();
            _form.rammebetingelser.adkomst = _form.rammebetingelser.adkomst ?? new VegType();


            if (nyeEndretAdkomst.HasValue)
            {
                _form.rammebetingelser.adkomst.nyeEndretAdkomst = nyeEndretAdkomst.Value;
                _form.rammebetingelser.adkomst.nyeEndretAdkomstSpecified = true;
            }

            //vegrett
            if (!string.IsNullOrEmpty(vegTypeKodeverdi) || !string.IsNullOrEmpty(vegkodebeskrivelse) || erTillatelseGitt.HasValue)
            {
                var vegrettType = new VegrettType();
                var vegrettTypes = _form.rammebetingelser.adkomst.vegrett?.ToList() ?? new List<VegrettType>();

                if (!string.IsNullOrEmpty(vegTypeKodeverdi) || !string.IsNullOrEmpty(vegkodebeskrivelse))
                {
                    vegrettType.vegtype = new KodeType
                    {
                        kodeverdi = vegTypeKodeverdi,
                        kodebeskrivelse = vegkodebeskrivelse
                    };
                }
                if (erTillatelseGitt.HasValue)
                {
                    vegrettType.erTillatelseGittSpecified = true;
                    vegrettType.erTillatelseGitt = erTillatelseGitt.Value;
                }
                vegrettTypes.Add(vegrettType);

                _form.rammebetingelser.adkomst.vegrett = vegrettTypes.ToArray();
            }

            return this;
        }
        public TiltakUtenAnsvarsrettV3Builder RammebetingelserArealdisponering(double? tomtearealByggeomraade = null, double? tomtearealSomTrekkesFra = null, double? tomtearealSomLeggesTil = null, double? tomtearealBeregnet = null, double? beregnetMaksByggeareal = null,
    double? arealBebyggelseEksisterende = null, double? arealBebyggelseSomSkalRives = null, double? arealBebyggelseNytt = null, double? parkeringsarealTerreng = null, double? arealSumByggesak = null, double? beregnetGradAvUtnytting = null)
        {
            if (_form.rammebetingelser == null)
                _form.rammebetingelser = new RammerType();

            _form.rammebetingelser.arealdisponering = new ArealdisponeringType();

            if (tomtearealByggeomraade.HasValue)
            {
                _form.rammebetingelser.arealdisponering.tomtearealByggeomraadeSpecified = true;
                _form.rammebetingelser.arealdisponering.tomtearealByggeomraade = tomtearealByggeomraade;
            }
            if (tomtearealSomTrekkesFra.HasValue)
            {
                _form.rammebetingelser.arealdisponering.tomtearealSomTrekkesFraSpecified = true;
                _form.rammebetingelser.arealdisponering.tomtearealSomTrekkesFra = tomtearealSomTrekkesFra;
            }
            if (tomtearealSomLeggesTil.HasValue)
            {
                _form.rammebetingelser.arealdisponering.tomtearealSomLeggesTilSpecified = true;
                _form.rammebetingelser.arealdisponering.tomtearealSomLeggesTil = tomtearealSomLeggesTil;
            }
            if (beregnetMaksByggeareal.HasValue)
            {
                _form.rammebetingelser.arealdisponering.beregnetMaksByggearealSpecified = true;
                _form.rammebetingelser.arealdisponering.beregnetMaksByggeareal = beregnetMaksByggeareal;
            }

            //
            if (arealBebyggelseEksisterende.HasValue)
            {
                _form.rammebetingelser.arealdisponering.arealBebyggelseEksisterendeSpecified = true;
                _form.rammebetingelser.arealdisponering.arealBebyggelseEksisterende = arealBebyggelseEksisterende;
            }
            //
            if (arealBebyggelseSomSkalRives.HasValue)
            {
                _form.rammebetingelser.arealdisponering.arealBebyggelseSomSkalRivesSpecified = true;
                _form.rammebetingelser.arealdisponering.arealBebyggelseSomSkalRives = arealBebyggelseSomSkalRives;
            }

            if (arealBebyggelseNytt.HasValue)
            {
                _form.rammebetingelser.arealdisponering.arealBebyggelseNyttSpecified = true;
                _form.rammebetingelser.arealdisponering.arealBebyggelseNytt = arealBebyggelseNytt;
            }

            if (arealSumByggesak.HasValue)
            {
                _form.rammebetingelser.arealdisponering.arealSumByggesakSpecified = true;
                _form.rammebetingelser.arealdisponering.arealSumByggesak = arealSumByggesak;
            }
            if (beregnetGradAvUtnytting.HasValue)
            {
                _form.rammebetingelser.arealdisponering.beregnetGradAvUtnyttingSpecified = true;
                _form.rammebetingelser.arealdisponering.beregnetGradAvUtnytting = beregnetGradAvUtnytting;
            }

            if (parkeringsarealTerreng.HasValue)
            {
                _form.rammebetingelser.arealdisponering.parkeringsarealTerrengSpecified = true;
                _form.rammebetingelser.arealdisponering.parkeringsarealTerreng = parkeringsarealTerreng;
            }
            if (tomtearealBeregnet.HasValue)
            {
                _form.rammebetingelser.arealdisponering.tomtearealBeregnetSpecified = true;
                _form.rammebetingelser.arealdisponering.tomtearealBeregnet = tomtearealBeregnet;
            }

            return this;
        }

        // TiltakUtenAnsvarsrettType.RammerType.GenerelleVilkaar
        public TiltakUtenAnsvarsrettV3Builder RammebetingerlserGenerelleVilkaar(bool? oppfyllesVilkaarFor3Ukersfrist = null, bool? beroererTidligere1850 = null, bool? behovForTillatelse = null, bool? norskSvenskDansk = null)
        {
            if (_form.rammebetingelser == null)
            {
                _form.rammebetingelser = new RammerType();
            }

            _form.rammebetingelser.generelleVilkaar = new GenerelleVilkaarType();

            if (oppfyllesVilkaarFor3Ukersfrist.HasValue)
            {
                _form.rammebetingelser.generelleVilkaar.oppfyllesVilkaarFor3UkersfristSpecified = true;
                _form.rammebetingelser.generelleVilkaar.oppfyllesVilkaarFor3Ukersfrist = oppfyllesVilkaarFor3Ukersfrist.Value;
            }
            if (beroererTidligere1850.HasValue)
            {
                _form.rammebetingelser.generelleVilkaar.beroererTidligere1850Specified = true;
                _form.rammebetingelser.generelleVilkaar.beroererTidligere1850 = beroererTidligere1850.Value;
            }
            if (behovForTillatelse.HasValue)
            {
                _form.rammebetingelser.generelleVilkaar.behovForTillatelseSpecified = true;
                _form.rammebetingelser.generelleVilkaar.behovForTillatelse = behovForTillatelse.Value;
            }

            if (norskSvenskDansk.HasValue)
            {
                _form.rammebetingelser.generelleVilkaar.norskSvenskDanskSpecified = true;
                _form.rammebetingelser.generelleVilkaar.norskSvenskDansk = norskSvenskDansk.Value;
            }

            return this;
        }

        //rammebetingelser.plan.gjeldendePlan
        public TiltakUtenAnsvarsrettV3Builder RammebetingelserPlanGeldendePlan(double? utnyttingsgrad = null, string plantypeKodeverdi = null, string plantypeKodebeskrivelse = null,
           string navm = null, string formaal = null, string beregningsregelgradavutnyttingkodeverdi = null, string beregningsregelgradavutnyttingkodebeskrivelse = null,
              string andreRelevanteKrav = null)
        {
            _form.rammebetingelser = _form.rammebetingelser ?? new RammerType();
            _form.rammebetingelser.plan = _form.rammebetingelser.plan ?? new PlanType();

            // gjeldendePlan
            if (!string.IsNullOrEmpty(beregningsregelgradavutnyttingkodebeskrivelse) || !string.IsNullOrEmpty(beregningsregelgradavutnyttingkodeverdi))
            {
                _form.rammebetingelser.plan.gjeldendePlan = _form.rammebetingelser.plan.gjeldendePlan ?? new GjeldendePlanType();

                _form.rammebetingelser.plan.gjeldendePlan.beregningsregelGradAvUtnytting = new KodeType()
                {
                    kodeverdi = beregningsregelgradavutnyttingkodeverdi,
                    kodebeskrivelse = beregningsregelgradavutnyttingkodebeskrivelse
                };
            }

            if (!string.IsNullOrEmpty(plantypeKodeverdi) || !string.IsNullOrEmpty(plantypeKodebeskrivelse))
            {
                _form.rammebetingelser.plan.gjeldendePlan = _form.rammebetingelser.plan.gjeldendePlan ?? new GjeldendePlanType();

                _form.rammebetingelser.plan.gjeldendePlan.plantype = new KodeType()
                {
                    kodeverdi = plantypeKodeverdi,
                    kodebeskrivelse = plantypeKodebeskrivelse
                };
            }

            if (utnyttingsgrad.HasValue)
            {
                _form.rammebetingelser.plan.gjeldendePlan = _form.rammebetingelser.plan.gjeldendePlan ?? new GjeldendePlanType();

                _form.rammebetingelser.plan.gjeldendePlan.utnyttingsgradSpecified = true;
                _form.rammebetingelser.plan.gjeldendePlan.utnyttingsgrad = utnyttingsgrad.Value;
            }

            if (!string.IsNullOrEmpty(navm) || !string.IsNullOrEmpty(formaal))
            {
                _form.rammebetingelser.plan.gjeldendePlan = _form.rammebetingelser.plan.gjeldendePlan ?? new GjeldendePlanType();

                _form.rammebetingelser.plan.gjeldendePlan.navn = navm;
                _form.rammebetingelser.plan.gjeldendePlan.formaal = formaal;
            }

            _form.rammebetingelser.plan.andreRelevanteKrav = andreRelevanteKrav;
            return this;
        }
        //rammebetingelser.plan.andrePlaner
        public TiltakUtenAnsvarsrettV3Builder RammebetingelserPlanAndrePlaner(string andrePlanerKodeverdi = null, string andrePlanerkodebeskrivelse = null, string navn = null)
        {

            _form.rammebetingelser = _form.rammebetingelser ?? new RammerType();
            _form.rammebetingelser.plan = _form.rammebetingelser.plan ?? new PlanType();

            var andreplaner = new AndrePlanerType();
            if (!string.IsNullOrEmpty(andrePlanerKodeverdi) || !string.IsNullOrEmpty(andrePlanerkodebeskrivelse))
            {
                andreplaner.plantype = new KodeType()
                {
                    kodeverdi = andrePlanerKodeverdi,
                    kodebeskrivelse = andrePlanerkodebeskrivelse
                };
            }

            andreplaner.navn = navn;

            var andrePlans = new List<AndrePlanerType>();
            if (_form.rammebetingelser.plan.andrePlaner != null)
            {
                andrePlans = _form.rammebetingelser.plan.andrePlaner.ToList();
            }
            andrePlans.Add(andreplaner);
            _form.rammebetingelser.plan.andrePlaner = andrePlans.ToArray();
            return this;
        }
        //TiltakUtenAnsvarsrettType.RammerType.vannforsyning
        public TiltakUtenAnsvarsrettV3Builder RammebetingelserVannforsyningType(string tilknytningstypeKodeverdi = null, string tilknytningstypeKodebeskrivelse = null, string beskrivelse = null, bool? krysserVannforsyningAnnensGrunn = null, bool? tinglystErklaering = null)
        {
            if (_form.rammebetingelser == null)
            {
                _form.rammebetingelser = new RammerType();
            }

            _form.rammebetingelser.vannforsyning = new VannforsyningType()
            {
                beskrivelse = beskrivelse,
            };
            if (krysserVannforsyningAnnensGrunn.HasValue)
            {
                _form.rammebetingelser.vannforsyning.krysserVannforsyningAnnensGrunnSpecified = true;
                _form.rammebetingelser.vannforsyning.krysserVannforsyningAnnensGrunn = krysserVannforsyningAnnensGrunn.Value;
            }
            if (tinglystErklaering.HasValue)
            {
                _form.rammebetingelser.vannforsyning.tinglystErklaeringSpecified = true;
                _form.rammebetingelser.vannforsyning.tinglystErklaering = tinglystErklaering.Value;
            }

            if (!string.IsNullOrEmpty(tilknytningstypeKodeverdi))
            {
                _form.rammebetingelser.vannforsyning.tilknytningstype = new[]
                {
                    new KodeType
                    {
                        kodeverdi = tilknytningstypeKodeverdi,
                        kodebeskrivelse = tilknytningstypeKodebeskrivelse
                    }
                };
            }
            return this;
        }
        // rammebetingelser.Avloep
        public TiltakUtenAnsvarsrettV3Builder RammebetingelserAvloep(string tilknytninstypeKodeverdi, string tilknytninstypeKodebeskrivelse, bool? installereVannklosett, bool? utslippstillatelse
        , bool? krysserAvloepAnnensGrunn = null, bool? tinglystErklaering = null, bool? overvannTerreng = null, bool? overvannAvloepssystem = null)
        {
            if (_form.rammebetingelser == null)
            {
                _form.rammebetingelser = new RammerType();
            }

            _form.rammebetingelser.avloep = new AvloepType();
            if (!string.IsNullOrEmpty(tilknytninstypeKodeverdi) || !string.IsNullOrEmpty(tilknytninstypeKodebeskrivelse))
            {
                _form.rammebetingelser.avloep.tilknytningstype = new KodeType
                {
                    kodeverdi = tilknytninstypeKodeverdi,
                    kodebeskrivelse = tilknytninstypeKodebeskrivelse
                };
            }
            if (installereVannklosett.HasValue)
            {
                _form.rammebetingelser.avloep.installereVannklosettSpecified = true;
                _form.rammebetingelser.avloep.installereVannklosett = installereVannklosett.Value;
            }

            if (overvannAvloepssystem.HasValue)
            {
                _form.rammebetingelser.avloep.overvannAvloepssystemSpecified = true;
                _form.rammebetingelser.avloep.overvannAvloepssystem = overvannAvloepssystem.Value;
            }
            if (tinglystErklaering.HasValue)
            {
                _form.rammebetingelser.avloep.tinglystErklaeringSpecified = true;
                _form.rammebetingelser.avloep.tinglystErklaering = tinglystErklaering.Value;
            }

            if (utslippstillatelse.HasValue)
            {
                _form.rammebetingelser.avloep.utslippstillatelseSpecified = true;
                _form.rammebetingelser.avloep.utslippstillatelse = utslippstillatelse.Value;
            }
            if (krysserAvloepAnnensGrunn.HasValue)
            {
                _form.rammebetingelser.avloep.krysserAvloepAnnensGrunnSpecified = true;
                _form.rammebetingelser.avloep.krysserAvloepAnnensGrunn = krysserAvloepAnnensGrunn.Value;
            }
            if (overvannTerreng.HasValue)
            {
                _form.rammebetingelser.avloep.overvannTerrengSpecified = true;
                _form.rammebetingelser.avloep.overvannTerreng = overvannTerreng.Value;
            }
            return this;
        }

        //rammebetingelser.kravTilByggegrunn
        public TiltakUtenAnsvarsrettV3Builder RammebetingelserKravTilByggegrunn(bool? flomutsattOmraade = null, bool? skredutsattOmraade = null, bool? miljoeforhold = null)
        {
            if (_form.rammebetingelser == null)
            {
                _form.rammebetingelser = new RammerType();
            }

            _form.rammebetingelser.kravTilByggegrunn = new KravTilByggegrunnType();


            if (flomutsattOmraade.HasValue)
            {
                _form.rammebetingelser.kravTilByggegrunn.flomutsattOmraadeSpecified = true;
                _form.rammebetingelser.kravTilByggegrunn.flomutsattOmraade = flomutsattOmraade.Value;
            }
            if (skredutsattOmraade.HasValue)
            {
                _form.rammebetingelser.kravTilByggegrunn.skredutsattOmraadeSpecified = true;
                _form.rammebetingelser.kravTilByggegrunn.skredutsattOmraade = skredutsattOmraade.Value;
            }
            if (miljoeforhold.HasValue)
            {
                _form.rammebetingelser.kravTilByggegrunn.miljoeforholdSpecified = true;
                _form.rammebetingelser.kravTilByggegrunn.miljoeforhold = miljoeforhold.Value;
            }

            return this;
        }
       
        // TiltakUtenAnsvarsrettType.RammerType.plassering
        public TiltakUtenAnsvarsrettV3Builder RammebetingelserPlassering(bool? konfliktHoeyspentkraftlinje=null, double? minsteAvstandNabogrense = null, bool? konfliktVannOgAvloep=null,
            double? minsteAvstandTilAnnenBygning = null, double? minsteAvstandTilMidtenAvVei = null, bool? bekreftetInnenforByggegrense = null)
        {
            if (_form.rammebetingelser == null)
            {
                _form.rammebetingelser = new RammerType();
            }

            _form.rammebetingelser.plassering = new PlasseringType();

            if (konfliktHoeyspentkraftlinje.HasValue)
            {
                _form.rammebetingelser.plassering.konfliktHoeyspentkraftlinjeSpecified = true;
                _form.rammebetingelser.plassering.konfliktHoeyspentkraftlinje = konfliktHoeyspentkraftlinje.Value;
            }

            if (konfliktVannOgAvloep.HasValue)
            {
                _form.rammebetingelser.plassering.konfliktVannOgAvloepSpecified = true;
                _form.rammebetingelser.plassering.konfliktVannOgAvloep = konfliktVannOgAvloep.Value;
            }
            if (minsteAvstandNabogrense.HasValue)
            {
                _form.rammebetingelser.plassering.minsteAvstandNabogrenseSpecified = true;
                _form.rammebetingelser.plassering.minsteAvstandNabogrense = minsteAvstandNabogrense.Value;
            }
            if (minsteAvstandTilAnnenBygning.HasValue)
            {
                _form.rammebetingelser.plassering.minsteAvstandTilAnnenBygningSpecified = true;
                _form.rammebetingelser.plassering.minsteAvstandTilAnnenBygning = minsteAvstandTilAnnenBygning.Value;
            }
            if (minsteAvstandTilMidtenAvVei.HasValue)
            {
                _form.rammebetingelser.plassering.minsteAvstandTilMidtenAvVeiSpecified = true;
                _form.rammebetingelser.plassering.minsteAvstandTilMidtenAvVei = minsteAvstandTilMidtenAvVei.Value;
            }
            if (bekreftetInnenforByggegrense.HasValue)
            {
                _form.rammebetingelser.plassering.bekreftetInnenforByggegrenseSpecified = true;
                _form.rammebetingelser.plassering.bekreftetInnenforByggegrense = bekreftetInnenforByggegrense.Value;
            }
            return this;
        }
        //Signatur
        public TiltakUtenAnsvarsrettV3Builder Signatur(DateTime? signaturdato = null, string signertAv = null, string signertPaaVegneAv = null)
        {
            _form.signatur = new SignaturType();
            _form.signatur.signaturdato = signaturdato;
            _form.signatur.signertAv = signertAv;
            _form.signatur.signertPaaVegneAv = signertPaaVegneAv;
            return this;
        }
        //Parse
        internal TiltakUtenAnsvarsrettV3Builder EiendomByggested(SampleEiendomData.Eiendom eiendom)
        {
            EiendomByggested(eiendom.EiendomsIdentifikasjon.Kommunenummer, eiendom.EiendomsIdentifikasjon.Gaardsnummer, eiendom.EiendomsIdentifikasjon.Bruksnummer, eiendom.EiendomsIdentifikasjon.Festenummer, eiendom.EiendomsIdentifikasjon.Seksjonsnummer,
                eiendom.Kommunenavn, eiendom.Bolignummer, eiendom.Bygningsnummer, eiendom.Adresse.adresselinje1, eiendom.Adresse.gatenavn, eiendom.Adresse.husnr, eiendom.Adresse.postnr, eiendom.Adresse.poststed);
            return this;
        }
        internal TiltakUtenAnsvarsrettV3Builder Tiltakshaver(SampleAktoerData.Aktoer tiltakshaver)
        {
            Tiltakshaver(tiltakshaver.organisasjonsnummer, tiltakshaver.foedselsnummer, tiltakshaver.partsTypeKodeverdi,tiltakshaver.partsTypeKodebeskrivelse, tiltakshaver.navn, tiltakshaver.Adresse.adresselinje1
                , tiltakshaver.Adresse.postnr, tiltakshaver.Adresse.landkode, tiltakshaver.Adresse.poststed, tiltakshaver.telefonnummer, tiltakshaver.mobilnummer, tiltakshaver.Epost, tiltakshaver.kontaktperson?.navn, tiltakshaver.kontaktperson?.telefonnummer, tiltakshaver.kontaktperson?.Epost);
            return this;
        }
    }
}