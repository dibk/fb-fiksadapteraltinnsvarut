﻿using no.kxml.skjema.dibk.nabovarselPlan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.Builders
{
    public class DistribusjonNabovarselPlanBuilder
    {
        private readonly NabovarselPlanType _form;

        public DistribusjonNabovarselPlanBuilder()
        {
            _form = new NabovarselPlanType();
        }

        public NabovarselPlanType Build()
        {
            return _form;
        }

        public DistribusjonNabovarselPlanBuilder Forslagsstiller(string organisasjonsnummer = null, string foedselsnummer = null, string partsTypeKodeverdi = null, string partsTypeKodebeskrivelse = null, string navn = null, string adressLine = null, string postnr = null, string landkode = null
            , string poststed = null, string telefonnummer = null, string mobilnummer = null, string epost = null, string kontaktpersonNavn = null, string KontaktPersonEpost = null, string kontaktPersonMobilnummer = null)
        {
            _form.forslagsstiller = new PartType(); 

            if (!string.IsNullOrEmpty(partsTypeKodeverdi))
            {
                _form.forslagsstiller.partstype = new KodeType
                {
                    kodeverdi = partsTypeKodeverdi,
                    kodebeskrivelse = ""
                };
            }

            _form.forslagsstiller.navn = navn;

            if (!string.IsNullOrEmpty(adressLine) || !string.IsNullOrEmpty(postnr) || !string.IsNullOrEmpty(landkode) || !string.IsNullOrEmpty(poststed))
            {
                _form.forslagsstiller.adresse = new EnkelAdresseType()
                {
                    adresselinje1 = adressLine,
                    postnr = postnr,
                    landkode = landkode,
                    poststed = poststed
                };
            }

            _form.forslagsstiller.organisasjonsnummer = organisasjonsnummer; //"974760673" - 974760673 BRREG, Altinn brukerservice
            if (!string.IsNullOrEmpty(kontaktpersonNavn) || !string.IsNullOrEmpty(KontaktPersonEpost) || !string.IsNullOrEmpty(kontaktPersonMobilnummer))
            {
                _form.forslagsstiller.kontaktperson = new KontaktpersonType()
                {
                    navn = kontaktpersonNavn,
                    epost = KontaktPersonEpost,
                    mobilnummer = kontaktPersonMobilnummer
                };
            }
            //_form.forslagsstiller.kontaktperson = kontaktperson;
            if (!string.IsNullOrEmpty(foedselsnummer))
            {
                _form.forslagsstiller.foedselsnummer = foedselsnummer;
            }
            _form.forslagsstiller.telefon = telefonnummer;
            _form.forslagsstiller.epost = epost;

            return this;
        }
        public DistribusjonNabovarselPlanBuilder PlanKonsulent(string organisasjonsnummer, string foedselsnummer, string partsTypeKodeverdi = null, string partsTypekodebeskrivelse = null, string navn = null, string adressLine = null, string postnr = null, string landkode = null
    , string poststed = null, string telefonnummer = null, string mobilnummer = null, string epost = null, string kontaktpersonNav = null, string kontaktpersonTel = null, string kontaktpersonEpost = null)
        {
            _form.planKonsulent = new PartType();

            if (!string.IsNullOrEmpty(partsTypeKodeverdi) || !string.IsNullOrEmpty(partsTypekodebeskrivelse))
            {
                _form.planKonsulent.partstype = new KodeType
                {
                    kodeverdi = partsTypeKodeverdi,
                    kodebeskrivelse = partsTypekodebeskrivelse
                };
            }
            _form.planKonsulent.foedselsnummer = foedselsnummer;
            _form.planKonsulent.organisasjonsnummer = organisasjonsnummer;
            _form.planKonsulent.navn = navn;
            if (!string.IsNullOrEmpty(adressLine) || !string.IsNullOrEmpty(postnr) || !string.IsNullOrEmpty(landkode) || !string.IsNullOrEmpty(poststed))
            {
                _form.planKonsulent.adresse = new EnkelAdresseType
                {
                    adresselinje1 = adressLine,
                    postnr = postnr,
                    poststed = poststed,
                    landkode = landkode
                };
            }
            _form.planKonsulent.telefon = telefonnummer;
            _form.planKonsulent.epost = epost;

            if (!string.IsNullOrEmpty(kontaktpersonNav) || !string.IsNullOrEmpty(kontaktpersonTel) || !string.IsNullOrEmpty(kontaktpersonEpost))
            {
                _form.planKonsulent.kontaktperson = new KontaktpersonType()
                {
                    navn = kontaktpersonNav,
                    telefonnummer = kontaktpersonTel,
                    epost = kontaktpersonEpost
                };
            }
            return this;
        }

        public DistribusjonNabovarselPlanBuilder BeroerteParter(string organisasjonsnummer = null, string foedselsnummer = null, string partsTypeKodeverdi = null, string partsTypeKodebeskrivelse = null, string navn = null, string adressLine = null, string postnr = null, string landkode = null
            , string poststed = null, string telefonnummer = null, string mobilnummer = null, string epost = null, string kontaktpersonNavn = null, string KontaktPersonEpost = null, string kontaktPersonMobilnummer = null)
        {
            var beroerteParter = new BeroertPartType();

            if (!string.IsNullOrEmpty(partsTypeKodeverdi))
            {
                beroerteParter.partstype = new KodeType
                {
                    kodeverdi = partsTypeKodeverdi,
                    kodebeskrivelse = ""
                };
            }

            beroerteParter.navn = navn;

            if (!string.IsNullOrEmpty(adressLine) || !string.IsNullOrEmpty(postnr) || !string.IsNullOrEmpty(landkode) || !string.IsNullOrEmpty(poststed))
            {
                beroerteParter.adresse = new EnkelAdresseType()
                {
                    adresselinje1 = adressLine,
                    postnr = postnr,
                    landkode = landkode,
                    poststed = poststed
                };
            }

            beroerteParter.organisasjonsnummer = organisasjonsnummer; //"974760673" - 974760673 BRREG, Altinn brukerservice
            if (!string.IsNullOrEmpty(kontaktpersonNavn) || !string.IsNullOrEmpty(KontaktPersonEpost) || !string.IsNullOrEmpty(kontaktPersonMobilnummer))
            {
                beroerteParter.kontaktperson = new KontaktpersonType()
                {
                    navn = kontaktpersonNavn,
                    epost = KontaktPersonEpost,
                    mobilnummer = kontaktPersonMobilnummer
                };
            }
            //beroerteParter.kontaktperson = kontaktperson;
            if (!string.IsNullOrEmpty(foedselsnummer))
            {
                beroerteParter.foedselsnummer = foedselsnummer;
            }
            beroerteParter.telefon = telefonnummer;
            beroerteParter.epost = epost;

            AddBeroerteParter(beroerteParter);

            return this;
        }


        private void AddBeroerteParter(BeroertPartType beroertPart)
        {
            if (_form.beroerteParter == null)
            {
                _form.beroerteParter = new[] { beroertPart };
            }
            else
            {
                var beroerpartList = _form.beroerteParter.ToList();
                beroerpartList.Add(beroertPart);
                _form.beroerteParter = beroerpartList.ToArray();
            }
        }

        public DistribusjonNabovarselPlanBuilder Kommunenavn(string kommunenavn)
        {
            _form.kommunenavn = kommunenavn;
            return this;
        }

        public DistribusjonNabovarselPlanBuilder Planforslag(string planTypeKodeverdi = null, string planTypeKodeBeskrivelse = null, string plannavn = null, string arealplanId = null, string planhensikt = null, DateTime? fristForInnspill = null, bool? kravTilKonsekvensUtredning = null)
        {
            _form.planforslag = new PlanType();
            _form.planforslag.plannavn = plannavn;
            _form.planforslag.arealplanId = arealplanId;
            _form.planforslag.planHensikt = planhensikt;
            if (fristForInnspill != null)
            {
                _form.planforslag.fristForInnspill = fristForInnspill;
                _form.planforslag.fristForInnspillSpecified = true;
            }

            if (!string.IsNullOrEmpty(planTypeKodeverdi) || !string.IsNullOrEmpty(planTypeKodeBeskrivelse))
            {
                _form.planforslag.plantype = new KodeType()
                {
                    kodeverdi = planTypeKodeverdi,
                    kodebeskrivelse = planTypeKodeBeskrivelse
                };
            }
            _form.planforslag.kravKonsekvensUtredning = kravTilKonsekvensUtredning;
            return this;
        }

    }
}