﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.SampleData;
using no.kxml.skjema.dibk.rammesoknadV3;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.Builders
{
    public class RammetillatelseV3Builder
    {
        private readonly RammetillatelseType _form;

        public RammetillatelseV3Builder()
        {
            _form = new RammetillatelseType();
        }

        public RammetillatelseType Build()
        {
            return _form;
        }

        // Tiltakshaver
        public RammetillatelseV3Builder Tiltakshaver(string organisasjonsnummer = null, string foedselsnummer = null, string partsTypeKodeverdi = null, string partsTypekodebeskrivelse = null, string navn = null, string adressLine = null, string postnr = null, string landkode = null
            , string poststed = null, string telefonnummer = null, string mobilnummer = null, string epost = null, string kontaktpersonNav = null, string kontaktpersonTel = null, string kontaktpersonEpost = null, string kontaktpersonMob = null)
        {
            _form.tiltakshaver = new PartType();

            if (!string.IsNullOrEmpty(partsTypeKodeverdi)|| !string.IsNullOrEmpty(partsTypekodebeskrivelse))
            {
                _form.tiltakshaver.partstype = new KodeType
                {
                    kodeverdi = partsTypeKodeverdi,
                    kodebeskrivelse = partsTypekodebeskrivelse
                };
            }
            _form.tiltakshaver.foedselsnummer = foedselsnummer;
            _form.tiltakshaver.organisasjonsnummer = organisasjonsnummer;
            _form.tiltakshaver.navn = navn;
            if (!string.IsNullOrEmpty(adressLine) || !string.IsNullOrEmpty(postnr) || !string.IsNullOrEmpty(landkode) || !string.IsNullOrEmpty(poststed))
            {
                _form.tiltakshaver.adresse = new EnkelAdresseType
                {
                    adresselinje1 = adressLine,
                    postnr = postnr,
                    poststed = poststed,
                    landkode = landkode
                };
            }
            _form.tiltakshaver.telefonnummer = telefonnummer;
            _form.tiltakshaver.mobilnummer = mobilnummer;
            _form.tiltakshaver.epost = epost;

            if (!string.IsNullOrEmpty(kontaktpersonNav) || !string.IsNullOrEmpty(kontaktpersonTel) || !string.IsNullOrEmpty(kontaktpersonMob) || !string.IsNullOrEmpty(kontaktpersonEpost))
            {
                _form.tiltakshaver.kontaktperson = new KontaktpersonType()
                {
                    navn = kontaktpersonNav,
                    telefonnummer = kontaktpersonTel,
                    mobilnummer = kontaktpersonMob,
                    epost = kontaktpersonEpost
                };

            }

            return this;
        }
        // AnsvarligSoeker
        public RammetillatelseV3Builder AnsvarligSoeker(string organisasjonsnummer = null, string foedselsnummer = null, string partsTypeKodeverdi = null, string partsTypeKodebeskrivelse = null, string navn = null, string adressLine = null, string postnr = null, string landkode = null
            , string poststed = null, string telefonnummer = null, string mobilnummer = null, string epost = null, string kontaktpersonNav = null, string kontaktpersonTel = null, string kontaktpersonEpost = null)
        {
            _form.ansvarligSoeker = new PartType();

            if (!string.IsNullOrEmpty(partsTypeKodeverdi))
            {
                _form.ansvarligSoeker.partstype = new KodeType
                {
                    kodeverdi = partsTypeKodeverdi,
                    kodebeskrivelse = partsTypeKodebeskrivelse
                };
            }
            _form.ansvarligSoeker.foedselsnummer = foedselsnummer;
            _form.ansvarligSoeker.organisasjonsnummer = organisasjonsnummer;
            _form.ansvarligSoeker.navn = navn;

            if (!string.IsNullOrEmpty(adressLine) || !string.IsNullOrEmpty(postnr) || !string.IsNullOrEmpty(landkode) || !string.IsNullOrEmpty(poststed))
            {
                _form.ansvarligSoeker.adresse = new EnkelAdresseType
                {
                    adresselinje1 = adressLine,
                    postnr = postnr,
                    landkode = landkode,
                    poststed = poststed
                };
            }
            _form.ansvarligSoeker.telefonnummer = telefonnummer;
            _form.ansvarligSoeker.mobilnummer = mobilnummer;
            _form.ansvarligSoeker.epost = epost;

            if (!string.IsNullOrEmpty(kontaktpersonNav) || !string.IsNullOrEmpty(kontaktpersonTel) || !string.IsNullOrEmpty(kontaktpersonEpost))
            {
                _form.ansvarligSoeker.kontaktperson = new KontaktpersonType()
                {
                    navn = kontaktpersonNav,
                    telefonnummer = kontaktpersonTel,
                    epost = kontaktpersonEpost
                };

            }

            return this;
        }
        // rammebetingelser.Avloep
        public RammetillatelseV3Builder RammebetingelserAvloep(string tilknytninstypeKodeverdi = null, string tilknytninstypeKodebeskrivelse = null, bool? installereVannklosett = null, bool? utslippstillatelse = null
        , bool? krysserAvloepAnnensGrunn = null, bool? tinglystErklaering = null, bool? overvannTerreng = null, bool? overvannAvloepssystem = null)
        {
            if (_form.rammebetingelser == null)
            {
                _form.rammebetingelser = new RammerType();
            }

            _form.rammebetingelser.avloep = new AvloepType();
            if (!string.IsNullOrEmpty(tilknytninstypeKodeverdi) || !string.IsNullOrEmpty(tilknytninstypeKodebeskrivelse))
            {
                _form.rammebetingelser.avloep.tilknytningstype = new KodeType
                {
                    kodeverdi = tilknytninstypeKodeverdi,
                    kodebeskrivelse = tilknytninstypeKodebeskrivelse
                };
            }
            if (installereVannklosett.HasValue)
            {
                _form.rammebetingelser.avloep.installereVannklosettSpecified = true;
                _form.rammebetingelser.avloep.installereVannklosett = installereVannklosett.Value;
            }

            if (overvannAvloepssystem.HasValue)
            {
                _form.rammebetingelser.avloep.overvannAvloepssystemSpecified = true;
                _form.rammebetingelser.avloep.overvannAvloepssystem = overvannAvloepssystem.Value;
            }
            if (tinglystErklaering.HasValue)
            {
                _form.rammebetingelser.avloep.tinglystErklaeringSpecified = true;
                _form.rammebetingelser.avloep.tinglystErklaering = tinglystErklaering.Value;
            }

            if (utslippstillatelse.HasValue)
            {
                _form.rammebetingelser.avloep.utslippstillatelseSpecified = true;
                _form.rammebetingelser.avloep.utslippstillatelse = utslippstillatelse.Value;
            }
            if (krysserAvloepAnnensGrunn.HasValue)
            {
                _form.rammebetingelser.avloep.krysserAvloepAnnensGrunnSpecified = true;
                _form.rammebetingelser.avloep.krysserAvloepAnnensGrunn = krysserAvloepAnnensGrunn.Value;
            }
            if (overvannTerreng.HasValue)
            {
                _form.rammebetingelser.avloep.overvannTerrengSpecified = true;
                _form.rammebetingelser.avloep.overvannTerreng = overvannTerreng.Value;
            }
            return this;
        }

        //rammebetingelser.kravTilByggegrunn
        public RammetillatelseV3Builder RammebetingelserKravTilByggegrunn(bool? flomutsattOmraade = null, bool? f1 = null, bool? f2 = null, bool? f3 = null, bool? skredutsattOmraade = null, bool? s1 = null, bool? s2 = null, bool? s3 = null, bool? miljoeforhold = null)
        {

            if (_form.rammebetingelser == null)
                _form.rammebetingelser = new RammerType();

            _form.rammebetingelser.kravTilByggegrunn = new KravTilByggegrunnType();
            if (flomutsattOmraade.HasValue)
            {
                _form.rammebetingelser.kravTilByggegrunn.flomutsattOmraadeSpecified = true;
                _form.rammebetingelser.kravTilByggegrunn.flomutsattOmraade = flomutsattOmraade.Value;
            }
            if (f1.HasValue)
            {
                _form.rammebetingelser.kravTilByggegrunn.f1Specified = true;
                _form.rammebetingelser.kravTilByggegrunn.f1 = f1.Value;
            }
            if (f2.HasValue)
            {
                _form.rammebetingelser.kravTilByggegrunn.f2Specified = true;
                _form.rammebetingelser.kravTilByggegrunn.f2 = f2.Value;
            }
            if (f3.HasValue)
            {
                _form.rammebetingelser.kravTilByggegrunn.f3Specified = true;
                _form.rammebetingelser.kravTilByggegrunn.f3 = f3.Value;
            }
            if (skredutsattOmraade.HasValue)
            {
                _form.rammebetingelser.kravTilByggegrunn.skredutsattOmraadeSpecified = true;
                _form.rammebetingelser.kravTilByggegrunn.skredutsattOmraade = skredutsattOmraade.Value;
            }
            if (s1.HasValue)
            {
                _form.rammebetingelser.kravTilByggegrunn.s1Specified = true;
                _form.rammebetingelser.kravTilByggegrunn.s1 = s1.Value;
            }
            if (s2.HasValue)
            {
                _form.rammebetingelser.kravTilByggegrunn.s3Specified = true;
                _form.rammebetingelser.kravTilByggegrunn.s3 = s3.Value;
            }
            if (s3.HasValue)
            {
                _form.rammebetingelser.kravTilByggegrunn.s3Specified = true;
                _form.rammebetingelser.kravTilByggegrunn.s3 = s3.Value;
            }
            if (miljoeforhold.HasValue)
            {
                _form.rammebetingelser.kravTilByggegrunn.miljoeforholdSpecified = true;
                _form.rammebetingelser.kravTilByggegrunn.miljoeforhold = miljoeforhold.Value;
            }
            return this;
        }

        // rammebetingelser.Arealdisponering
        public RammetillatelseV3Builder RammebetingelserArealdisponering(double? tomtearealByggeomraade = null, double? tomtearealSomTrekkesFra = null, double? tomtearealSomLeggesTil = null, double? tomtearealBeregnet = null, double? beregnetMaksByggeareal = null,
           double? arealBebyggelseEksisterende = null, double? arealBebyggelseSomSkalRives = null, double? arealBebyggelseNytt = null, double? parkeringsarealTerreng = null, double? arealSumByggesak = null, double? beregnetGradAvUtnytting = null)
        {
            if (_form.rammebetingelser == null)
                _form.rammebetingelser = new RammerType();

            _form.rammebetingelser.arealdisponering = new ArealdisponeringType();

            if (tomtearealByggeomraade.HasValue)
            {
                _form.rammebetingelser.arealdisponering.tomtearealByggeomraadeSpecified = true;
                _form.rammebetingelser.arealdisponering.tomtearealByggeomraade = tomtearealByggeomraade;
            }
            if (tomtearealSomTrekkesFra.HasValue)
            {
                _form.rammebetingelser.arealdisponering.tomtearealSomTrekkesFraSpecified = true;
                _form.rammebetingelser.arealdisponering.tomtearealSomTrekkesFra = tomtearealSomTrekkesFra;
            }
            if (tomtearealSomLeggesTil.HasValue)
            {
                _form.rammebetingelser.arealdisponering.tomtearealSomLeggesTilSpecified = true;
                _form.rammebetingelser.arealdisponering.tomtearealSomLeggesTil = tomtearealSomLeggesTil;
            }
            if (tomtearealBeregnet.HasValue)
            {
                _form.rammebetingelser.arealdisponering.tomtearealBeregnetSpecified = true;
                _form.rammebetingelser.arealdisponering.tomtearealBeregnet = tomtearealBeregnet;
            }

            if (beregnetMaksByggeareal.HasValue)
            {
                _form.rammebetingelser.arealdisponering.beregnetMaksByggearealSpecified = true;
                _form.rammebetingelser.arealdisponering.beregnetMaksByggeareal = beregnetMaksByggeareal;
            }

            //
            if (arealBebyggelseEksisterende.HasValue)
            {
                _form.rammebetingelser.arealdisponering.arealBebyggelseEksisterendeSpecified = true;
                _form.rammebetingelser.arealdisponering.arealBebyggelseEksisterende = arealBebyggelseEksisterende;
            }
            //
            if (arealBebyggelseSomSkalRives.HasValue)
            {
                _form.rammebetingelser.arealdisponering.arealBebyggelseSomSkalRivesSpecified = true;
                _form.rammebetingelser.arealdisponering.arealBebyggelseSomSkalRives = arealBebyggelseSomSkalRives;
            }

            if (arealBebyggelseNytt.HasValue)
            {
                _form.rammebetingelser.arealdisponering.arealBebyggelseNyttSpecified = true;
                _form.rammebetingelser.arealdisponering.arealBebyggelseNytt = arealBebyggelseNytt;
            }
            if (parkeringsarealTerreng.HasValue)
            {
                _form.rammebetingelser.arealdisponering.parkeringsarealTerrengSpecified = true;
                _form.rammebetingelser.arealdisponering.parkeringsarealTerreng = parkeringsarealTerreng;
            }
            if (arealSumByggesak.HasValue)
            {
                _form.rammebetingelser.arealdisponering.arealSumByggesakSpecified = true;
                _form.rammebetingelser.arealdisponering.arealSumByggesak = arealSumByggesak;
            }
            if (beregnetGradAvUtnytting.HasValue)
            {
                _form.rammebetingelser.arealdisponering.beregnetGradAvUtnyttingSpecified = true;
                _form.rammebetingelser.arealdisponering.beregnetGradAvUtnytting = beregnetGradAvUtnytting;
            }



            return this;
        }
        public RammetillatelseV3Builder RammebetingelserPlanGeldendePlan(double? utnyttingsgrad = null, string plantypeKodeverdi = null, string plantypeKodebeskrivelse = null,
            string navm = null, string formaal = null, string beregningsregelgradavutnyttingkodeverdi = null, string beregningsregelgradavutnyttingkodebeskrivelse = null,
               string andreRelevanteKrav = null)
        {
            _form.rammebetingelser = _form.rammebetingelser ?? new RammerType();
            _form.rammebetingelser.plan = _form.rammebetingelser.plan ?? new PlanType();

            // gjeldendePlan
            if (!string.IsNullOrEmpty(beregningsregelgradavutnyttingkodebeskrivelse) || !string.IsNullOrEmpty(beregningsregelgradavutnyttingkodeverdi))
            {
                _form.rammebetingelser.plan.gjeldendePlan = _form.rammebetingelser.plan.gjeldendePlan ?? new GjeldendePlanType();

                _form.rammebetingelser.plan.gjeldendePlan.beregningsregelGradAvUtnytting = new KodeType()
                {
                    kodeverdi = beregningsregelgradavutnyttingkodeverdi,
                    kodebeskrivelse = beregningsregelgradavutnyttingkodebeskrivelse
                };
            }

            if (!string.IsNullOrEmpty(plantypeKodeverdi) || !string.IsNullOrEmpty(plantypeKodebeskrivelse))
            {
                _form.rammebetingelser.plan.gjeldendePlan = _form.rammebetingelser.plan.gjeldendePlan ?? new GjeldendePlanType();

                _form.rammebetingelser.plan.gjeldendePlan.plantype = new KodeType()
                {
                    kodeverdi = plantypeKodeverdi,
                    kodebeskrivelse = plantypeKodebeskrivelse
                };
            }

            if (utnyttingsgrad.HasValue)
            {
                _form.rammebetingelser.plan.gjeldendePlan = _form.rammebetingelser.plan.gjeldendePlan ?? new GjeldendePlanType();

                _form.rammebetingelser.plan.gjeldendePlan.utnyttingsgradSpecified = true;
                _form.rammebetingelser.plan.gjeldendePlan.utnyttingsgrad = utnyttingsgrad.Value;
            }

            if (!string.IsNullOrEmpty(navm) || !string.IsNullOrEmpty(formaal))
            {
                _form.rammebetingelser.plan.gjeldendePlan = _form.rammebetingelser.plan.gjeldendePlan ?? new GjeldendePlanType();

                _form.rammebetingelser.plan.gjeldendePlan.navn = navm;
                _form.rammebetingelser.plan.gjeldendePlan.formaal = formaal;
            }

            _form.rammebetingelser.plan.andreRelevanteKrav = andreRelevanteKrav;


            return this;
        }

        public RammetillatelseV3Builder RammebetingelserPlanAndrePlaner(string andrePlanerKodeverdi = null, string andrePlanerkodebeskrivelse = null, string navn = null)
        {

            _form.rammebetingelser = _form.rammebetingelser ?? new RammerType();
            _form.rammebetingelser.plan = _form.rammebetingelser.plan ?? new PlanType();

            var andreplaner = new AndrePlanerType();
            if (!string.IsNullOrEmpty(andrePlanerKodeverdi) || !string.IsNullOrEmpty(andrePlanerkodebeskrivelse))
            {
                andreplaner.plantype = new KodeType()
                {
                    kodeverdi = andrePlanerKodeverdi,
                    kodebeskrivelse = andrePlanerkodebeskrivelse
                };
            }

            andreplaner.navn = navn;

            var andrePlans = new List<AndrePlanerType>();
            if (_form.rammebetingelser.plan.andrePlaner != null)
            {
                andrePlans = _form.rammebetingelser.plan.andrePlaner.ToList();
            }
            andrePlans.Add(andreplaner);
            _form.rammebetingelser.plan.andrePlaner = andrePlans.ToArray();
            return this;
        }

        // Rammetillatelse.Adkomst
        public RammetillatelseV3Builder RammetillatelseAdkomst(bool? nyeEndretAdkomst = null, string vegTypeKodeverdi = null, string vegTypeKodebeskrivelse = null, bool? erTillatelseGittRiksFylkesveg = null, bool? erTillatelseGittPrivatVeg = null, bool? erTillatelseGittKommunalVeg = null)
        {

            if (_form.rammebetingelser == null)
                _form.rammebetingelser = new RammerType();

            _form.rammebetingelser.adkomst = new VegType();

            if (nyeEndretAdkomst.HasValue)
            {
                _form.rammebetingelser.adkomst.nyeEndretAdkomstSpecified = true;
                _form.rammebetingelser.adkomst.nyeEndretAdkomst = nyeEndretAdkomst.Value;
            }

            if (!string.IsNullOrEmpty(vegTypeKodeverdi) || !string.IsNullOrEmpty(vegTypeKodebeskrivelse))
            {
                _form.rammebetingelser.adkomst.vegtype = new[]
                {
                    new KodeType
                    {
                        kodeverdi = vegTypeKodeverdi,
                        kodebeskrivelse = vegTypeKodebeskrivelse
                    }
                };
            }
            if (erTillatelseGittRiksFylkesveg.HasValue)
            {
                _form.rammebetingelser.adkomst.erTillatelseGittRiksFylkesveg = erTillatelseGittRiksFylkesveg.Value;
                _form.rammebetingelser.adkomst.erTillatelseGittRiksFylkesvegSpecified = true;
            }

            if (erTillatelseGittKommunalVeg.HasValue)
            {
                _form.rammebetingelser.adkomst.erTillatelseGittKommunalVegSpecified = true;
                _form.rammebetingelser.adkomst.erTillatelseGittKommunalVeg = erTillatelseGittKommunalVeg.Value;
            }
            if (erTillatelseGittPrivatVeg.HasValue)
            {
                _form.rammebetingelser.adkomst.erTillatelseGittPrivatVegSpecified = true;
                _form.rammebetingelser.adkomst.erTillatelseGittPrivatVeg = erTillatelseGittPrivatVeg.Value;
            }
            return this;
        }
        // Rammetillatelse.plassering
        public RammetillatelseV3Builder RammetillatelsePlassering(bool? konfliktHoeyspentkraftlinje = null, bool? konfliktVannOgAvloep = null)
        {

            if (_form.rammebetingelser == null)
                _form.rammebetingelser = new RammerType();

            _form.rammebetingelser.plassering = new PlasseringType();

            if (konfliktHoeyspentkraftlinje.HasValue)
            {
                _form.rammebetingelser.plassering.konfliktHoeyspentkraftlinjeSpecified = true;
                _form.rammebetingelser.plassering.konfliktHoeyspentkraftlinje = konfliktHoeyspentkraftlinje.Value;
            }

            if (konfliktVannOgAvloep.HasValue)
            {
                _form.rammebetingelser.plassering.konfliktVannOgAvloepSpecified = true;
                _form.rammebetingelser.plassering.konfliktVannOgAvloep = konfliktVannOgAvloep.Value;
            }
            return this;
        }
        // Rammetillatelse.Loefteinnretninger
        public RammetillatelseV3Builder RammetillatelseLoefteinnretninger(bool? erLoefteinnretningIBygning = null, bool? planleggesLoefteinnretningIBygning = null, bool? planleggesHeis = null, bool? planleggesTrappeheis = null,
            bool? planleggesRulletrapp = null, bool? planleggesLoefteplattform = null)
        {
            if (_form.rammebetingelser == null)
                _form.rammebetingelser = new RammerType();

            _form.rammebetingelser.loefteinnretninger = new LoefteinnretningerType();

            if (erLoefteinnretningIBygning.HasValue)
            {
                _form.rammebetingelser.loefteinnretninger.erLoefteinnretningIBygningSpecified = true;
                _form.rammebetingelser.loefteinnretninger.erLoefteinnretningIBygning = erLoefteinnretningIBygning.Value;
            }
            if (planleggesLoefteinnretningIBygning.HasValue)
            {
                _form.rammebetingelser.loefteinnretninger.planleggesLoefteinnretningIBygning = planleggesLoefteinnretningIBygning.Value;
                _form.rammebetingelser.loefteinnretninger.planleggesLoefteinnretningIBygningSpecified = true;

            }

            if (planleggesTrappeheis.HasValue)
            {
                _form.rammebetingelser.loefteinnretninger.planleggesTrappeheisSpecified = true;
                _form.rammebetingelser.loefteinnretninger.planleggesTrappeheis = planleggesTrappeheis.Value;
            }
            if (planleggesHeis.HasValue)
            {
                _form.rammebetingelser.loefteinnretninger.planleggesHeisSpecified = true;
                _form.rammebetingelser.loefteinnretninger.planleggesHeis = planleggesHeis.Value;

            }

            if (planleggesRulletrapp.HasValue)
            {
                _form.rammebetingelser.loefteinnretninger.planleggesRulletrapp = true;
                _form.rammebetingelser.loefteinnretninger.planleggesRulletrappSpecified = planleggesRulletrapp.Value;
            }

            if (planleggesLoefteplattform.HasValue)
            {
                _form.rammebetingelser.loefteinnretninger.planleggesLoefteplattform = true;
                _form.rammebetingelser.loefteinnretninger.planleggesLoefteplattformSpecified = planleggesLoefteplattform.Value;

            }
            return this;
        }
        // Rammetillatelse.GenerelleVilkaar
        public RammetillatelseV3Builder RammebetingerlserGenerelleVilkaar(bool? beroererTidligere1850 = null, bool? forhaandskonferanseAvholdt = null, bool? paalagtUavhengigKontroll = null, bool? beroererArbeidsplasser = null,
            bool? utarbeideAvfallsplan = null, bool? behovForTillatelse = null, bool? norskSvenskDansk = null)
        {
            if (_form.rammebetingelser == null)
            {
                _form.rammebetingelser = new RammerType();
            }

            _form.rammebetingelser.generelleVilkaar = new GenerelleVilkaarType();

            if (beroererTidligere1850.HasValue)
            {
                _form.rammebetingelser.generelleVilkaar.beroererTidligere1850Specified = true;
                _form.rammebetingelser.generelleVilkaar.beroererTidligere1850 = beroererTidligere1850.Value;
            }

            if (forhaandskonferanseAvholdt.HasValue)
            {
                _form.rammebetingelser.generelleVilkaar.forhaandskonferanseAvholdtSpecified = true;
                _form.rammebetingelser.generelleVilkaar.forhaandskonferanseAvholdt = forhaandskonferanseAvholdt.Value;
            }
            if (paalagtUavhengigKontroll.HasValue)
            {
                _form.rammebetingelser.generelleVilkaar.paalagtUavhengigKontrollSpecified = true;
                _form.rammebetingelser.generelleVilkaar.paalagtUavhengigKontroll = paalagtUavhengigKontroll.Value;
            }
            if (beroererArbeidsplasser.HasValue)
            {
                _form.rammebetingelser.generelleVilkaar.beroererArbeidsplasserSpecified = true;
                _form.rammebetingelser.generelleVilkaar.beroererArbeidsplasser = beroererArbeidsplasser.Value;
            }
            if (utarbeideAvfallsplan.HasValue)
            {
                _form.rammebetingelser.generelleVilkaar.utarbeideAvfallsplanSpecified = true;
                _form.rammebetingelser.generelleVilkaar.utarbeideAvfallsplan = utarbeideAvfallsplan.Value;
            }
            if (behovForTillatelse.HasValue)
            {
                _form.rammebetingelser.generelleVilkaar.behovForTillatelseSpecified = true;
                _form.rammebetingelser.generelleVilkaar.behovForTillatelse = behovForTillatelse.Value;
            }
            if (norskSvenskDansk.HasValue)
            {
                _form.rammebetingelser.generelleVilkaar.norskSvenskDanskSpecified = true;
                _form.rammebetingelser.generelleVilkaar.norskSvenskDansk = norskSvenskDansk.Value;
            }
            return this;
        }



        public RammetillatelseV3Builder RammebetingelserVannforsyningType(string tilknytningstypeKodeverdi = null, string tilknytningstypeKodebeskrivelse = null, string beskrivelse = null, bool? tinglystErklaering = null, bool? krysserVannforsyningAnnensGrunn = null)
        {
            if (_form.rammebetingelser == null)
            {
                _form.rammebetingelser = new RammerType();
            }

            _form.rammebetingelser.vannforsyning = new VannforsyningType();

            if (!string.IsNullOrEmpty(tilknytningstypeKodeverdi) || !string.IsNullOrEmpty(tilknytningstypeKodebeskrivelse))
            {
                _form.rammebetingelser.vannforsyning.tilknytningstype =
                    new KodeType
                    {
                        kodeverdi = tilknytningstypeKodeverdi,
                        kodebeskrivelse = tilknytningstypeKodebeskrivelse
                    };
            }

            _form.rammebetingelser.vannforsyning.beskrivelse = beskrivelse;

            if (krysserVannforsyningAnnensGrunn.HasValue)
            {
                _form.rammebetingelser.vannforsyning.krysserVannforsyningAnnensGrunnSpecified = true;
                _form.rammebetingelser.vannforsyning.krysserVannforsyningAnnensGrunn = krysserVannforsyningAnnensGrunn.Value;
            }

            if (tinglystErklaering.HasValue)
            {
                _form.rammebetingelser.vannforsyning.tinglystErklaeringSpecified = true;
                _form.rammebetingelser.vannforsyning.tinglystErklaering = tinglystErklaering.Value;
            }

            return this;
        }

        // eiendomByggested
        public RammetillatelseV3Builder EiendomByggested(string knr = null, string gnr = null, string bnr = null, string fnr = null, string snr = null, string kommunenavn = null, string bolignummer = null, string bygningsnummer = null
            , string adresselinje1 = null, string gatenavn = null, string husnr = null, string postnr = null, string poststed = null)
        {
            var eiendomByggested = new EiendomType();
            eiendomByggested.bolignummer = bolignummer;
            eiendomByggested.kommunenavn = kommunenavn;
            eiendomByggested.eiendomsidentifikasjon = new MatrikkelnummerType()
            {
                kommunenummer = knr,
                gaardsnummer = gnr,
                bruksnummer = bnr,
                festenummer = fnr,
                seksjonsnummer = snr
            };

            eiendomByggested.bygningsnummer = bygningsnummer;
            if (!string.IsNullOrEmpty(adresselinje1) || !string.IsNullOrEmpty(gatenavn) || !string.IsNullOrEmpty(husnr))
            {
                eiendomByggested.adresse = new EiendommensAdresseType()
                {
                    adresselinje1 = adresselinje1,
                    gatenavn = gatenavn,
                    husnr = husnr,
                    postnr = postnr,
                    poststed = poststed
                };
            }

            var eiendomTypes = _form.eiendomByggested?.ToList() ?? new List<EiendomType>();
            eiendomTypes.Add(eiendomByggested);

            _form.eiendomByggested = eiendomTypes.ToArray();


            return this;
        }

        public RammetillatelseV3Builder BeskrivelseAvTiltakBruk(string anleggstypeKodeverdi = null, string anleggstypeKodebeskrivelse = null, string naeringsgruppeKodeverdi = null, string naeringsgruppeKodebeskrivelse = null,
            string bygningstypeKodeverdi = null, string bygningstypeKodebeskrivelse = null, string tiltaksformaalKodeverdi = null, string tiltaksformaalKodebeskrivelse = null,
            string beskrivPlanlagtFormaal = null)
        {
            _form.beskrivelseAvTiltak = _form.beskrivelseAvTiltak ?? new TiltakType();
            _form.beskrivelseAvTiltak.bruk = _form.beskrivelseAvTiltak.bruk ?? new FormaalType();

            //add Bruk to BeskrivelseAvTiltak
            if (!string.IsNullOrEmpty(anleggstypeKodeverdi) || !string.IsNullOrEmpty(anleggstypeKodebeskrivelse))
            {
                _form.beskrivelseAvTiltak.bruk.anleggstype = new KodeType()
                {
                    kodeverdi = anleggstypeKodeverdi,
                    kodebeskrivelse = anleggstypeKodebeskrivelse
                };
            }

            if (!string.IsNullOrEmpty(naeringsgruppeKodeverdi) || !string.IsNullOrEmpty(naeringsgruppeKodebeskrivelse))
            {
                _form.beskrivelseAvTiltak.bruk.naeringsgruppe = new KodeType()
                {
                    kodeverdi = naeringsgruppeKodeverdi,
                    kodebeskrivelse = naeringsgruppeKodebeskrivelse
                };
            }
            if (!string.IsNullOrEmpty(bygningstypeKodeverdi) || !string.IsNullOrEmpty(bygningstypeKodebeskrivelse))
            {
                _form.beskrivelseAvTiltak.bruk.bygningstype = new KodeType()
                {
                    kodeverdi = bygningstypeKodeverdi,
                    kodebeskrivelse = bygningstypeKodebeskrivelse
                };
            }
            if (!string.IsNullOrEmpty(tiltaksformaalKodeverdi) || !string.IsNullOrEmpty(tiltaksformaalKodebeskrivelse))
            {
                var kodetypes = _form.beskrivelseAvTiltak.bruk.tiltaksformaal?.ToList() ?? new List<KodeType>();
                kodetypes.Add(new KodeType()
                {
                    kodeverdi = tiltaksformaalKodeverdi,
                    kodebeskrivelse = tiltaksformaalKodebeskrivelse
                });
                _form.beskrivelseAvTiltak.bruk.tiltaksformaal = kodetypes.ToArray();
            }

            _form.beskrivelseAvTiltak.bruk.beskrivPlanlagtFormaal = beskrivPlanlagtFormaal;

            return this;
        }
        public RammetillatelseV3Builder BeskrivelseAvTiltakType(string tiltakTypeKodeverdi = null, string tiltakTypeKodebeskrivelse = null)
        {

            _form.beskrivelseAvTiltak = _form.beskrivelseAvTiltak ?? new TiltakType();
            _form.beskrivelseAvTiltak.bruk = _form.beskrivelseAvTiltak.bruk ?? new FormaalType();

            if (!string.IsNullOrEmpty(tiltakTypeKodeverdi) || !string.IsNullOrEmpty(tiltakTypeKodebeskrivelse))
            {
                var kodetypes = _form.beskrivelseAvTiltak.type?.ToList() ?? new List<KodeType>();
                kodetypes.Add(new KodeType()
                {
                    kodeverdi = tiltakTypeKodeverdi,
                    kodebeskrivelse = tiltakTypeKodebeskrivelse
                });
                _form.beskrivelseAvTiltak.type = kodetypes.ToArray();
            }

            return this;
        }
        public RammetillatelseV3Builder BeskrivelseAvTiltakFoelgebrev(string foelgebrev = null)
        {
            _form.beskrivelseAvTiltak = _form.beskrivelseAvTiltak ?? new TiltakType();
            _form.beskrivelseAvTiltak.foelgebrev = foelgebrev;
            return this;
        }
        // Varsling
        public RammetillatelseV3Builder Varsling(bool? fritattFraNabovarsling = null, bool? foreliggerMerknader = null, string antallMerknader = null, string vurderingAvMerknader = null, string soeknadensHjemmeside = null, string soeknadSeesKontaktperson = null)
        {
            _form.varsling = new VarslingType()
            {
                antallMerknader = antallMerknader,
                vurderingAvMerknader = vurderingAvMerknader,
                soeknadensHjemmeside = soeknadensHjemmeside,
                soeknadSeesKontaktperson = soeknadSeesKontaktperson
            };
            if (foreliggerMerknader.HasValue)
            {
                _form.varsling.foreliggerMerknaderSpecified = true;
                _form.varsling.foreliggerMerknader = foreliggerMerknader.Value;
            }
            if (fritattFraNabovarsling.HasValue)
            {
                _form.varsling.fritattFraNabovarslingSpecified = true;
                _form.varsling.fritattFraNabovarsling = fritattFraNabovarsling.Value;
            }
            return this;
        }

        //Dispensasjon
        public RammetillatelseV3Builder Dispensasjon(string kodeverdi = null, string kodebeskrivelse = null, string begrunnelse = null, string beskrivelse = null)
        {

            var dispensasjons = _form.dispensasjon?.ToList() ?? new List<DispensasjonType>();

            var dispensajon = new DispensasjonType() { beskrivelse = beskrivelse, begrunnelse = begrunnelse };


            if (!string.IsNullOrEmpty(kodeverdi) || !string.IsNullOrEmpty(kodebeskrivelse))
            {
                dispensajon.dispensasjonstype = new KodeType()
                {
                    kodeverdi = kodeverdi,
                    kodebeskrivelse = kodebeskrivelse
                };

            }
            dispensasjons.Add(dispensajon);

            _form.dispensasjon = dispensasjons.ToArray();

            return this;
        }

        //Signatur
        public RammetillatelseV3Builder Signatur(DateTime? signaturdato = null, string signertAv = null, string signertPaaVegneAv = null)
        {
            _form.signatur = new SignaturType();
            _form.signatur.signaturdato = signaturdato;
            _form.signatur.signertAv = signertAv;
            _form.signatur.signertPaaVegneAv = signertPaaVegneAv;
            return this;
        }

        //Metadata
        public RammetillatelseV3Builder Metadata(string fraSluttbrukersystem = null, string ftbId = null, string prosjektnavn = null, string sluttbrukersystemUrl = null, string samtykkeTilByggeplaner = null,bool? klartForSigneringFraSluttbrukersystem=null)
        {
            _form.metadata = new MetadataType();
            _form.metadata.fraSluttbrukersystem = fraSluttbrukersystem;
            _form.metadata.ftbId = ftbId;
            _form.metadata.prosjektnavn = prosjektnavn;
            _form.metadata.sluttbrukersystemUrl = sluttbrukersystemUrl;
            _form.metadata.samtykkeTilByggeplaner = samtykkeTilByggeplaner;

            if (klartForSigneringFraSluttbrukersystem.HasValue)
            {
                _form.metadata.klartForSigneringFraSluttbrukersystem = klartForSigneringFraSluttbrukersystem; 
                _form.metadata.klartForSigneringFraSluttbrukersystemSpecified = true; 
            }

            return this;
        }


        //Facturamottaker
        public RammetillatelseV3Builder Fakturamottaker(string organisasjonsnummer = null, string bestillerReferanse = null, string fakturareferanser = null,
            string navn = null, string prosjektnummer = null, bool? ehfFaktura = null, bool? fakturaPapir = null, string adresselinje1 = null, string postnr = null, string poststed = null, string landkode = null)
        {
            _form.fakturamottaker = new FakturamottakerType();
            _form.fakturamottaker.organisasjonsnummer = organisasjonsnummer;
            _form.fakturamottaker.bestillerReferanse = bestillerReferanse;
            _form.fakturamottaker.fakturareferanser = fakturareferanser;
            _form.fakturamottaker.navn = navn;
            _form.fakturamottaker.prosjektnummer = prosjektnummer;
            if (ehfFaktura.HasValue)
            {
                _form.fakturamottaker.ehfFakturaSpecified = true;
                _form.fakturamottaker.ehfFaktura = ehfFaktura;

            }
            if (fakturaPapir.HasValue)
            {
                _form.fakturamottaker.fakturaPapirSpecified = true;
                _form.fakturamottaker.fakturaPapir = fakturaPapir;

            }

            if (!string.IsNullOrEmpty(adresselinje1) || !string.IsNullOrEmpty(postnr) || !string.IsNullOrEmpty(poststed))
            {
                _form.fakturamottaker.adresse = new EnkelAdresseType()
                {
                    adresselinje1 = adresselinje1,
                    postnr = postnr,
                    poststed = poststed,
                    landkode = landkode
                };
            }
            return this;
        }
        //ansvarsrettSoeker
        public RammetillatelseV3Builder AnsvarsrettSoeker(bool? harSentralGodkjenning = null, string tiltaksklasseKodeverdi = null, string tiltaksklasseKodebeskrivelse = null,
            bool? erklaeringAnsvar = null)
        {
            _form.ansvarsrettSoeker = new AnsvarsrettSoekerType();

            if (harSentralGodkjenning.HasValue)
            {
                _form.ansvarsrettSoeker.harSentralGodkjenningSpecified = true;
                _form.ansvarsrettSoeker.harSentralGodkjenning = harSentralGodkjenning;
            }
            if (!string.IsNullOrEmpty(tiltaksklasseKodeverdi) || !string.IsNullOrEmpty(tiltaksklasseKodebeskrivelse))
            {
                _form.ansvarsrettSoeker.tiltaksklasse = new KodeType()
                {
                    kodeverdi = tiltaksklasseKodeverdi,
                    kodebeskrivelse = tiltaksklasseKodebeskrivelse
                };
            }
            if (erklaeringAnsvar.HasValue)
            {
                _form.ansvarsrettSoeker.erklaeringAnsvarSpecified = true;
                _form.ansvarsrettSoeker.erklaeringAnsvar = erklaeringAnsvar;
            }

            return this;
        }

        //Parse
        internal RammetillatelseV3Builder AnsvarligSoeker(SampleAktoerData.Aktoer ansvarligAktoer)
        {
            AnsvarligSoeker(ansvarligAktoer.organisasjonsnummer, ansvarligAktoer.foedselsnummer, ansvarligAktoer.partsTypeKodeverdi, ansvarligAktoer.partsTypeKodebeskrivelse, ansvarligAktoer.navn, ansvarligAktoer.Adresse.adresselinje1
                , ansvarligAktoer.Adresse.postnr, ansvarligAktoer.Adresse.landkode, ansvarligAktoer.Adresse.poststed, ansvarligAktoer.telefonnummer, ansvarligAktoer.mobilnummer, ansvarligAktoer.Epost, ansvarligAktoer.kontaktperson?.navn, ansvarligAktoer.kontaktperson?.telefonnummer, ansvarligAktoer.kontaktperson?.Epost);
            return this;
        }
        internal RammetillatelseV3Builder Tiltakshaver(SampleAktoerData.Aktoer tiltakshaver)
        {
            Tiltakshaver(tiltakshaver.organisasjonsnummer, tiltakshaver.foedselsnummer, tiltakshaver.partsTypeKodeverdi, tiltakshaver.partsTypeKodebeskrivelse, tiltakshaver.navn, tiltakshaver.Adresse.adresselinje1
                , tiltakshaver.Adresse.postnr, tiltakshaver.Adresse.landkode, tiltakshaver.Adresse.poststed, tiltakshaver.telefonnummer, tiltakshaver.mobilnummer, tiltakshaver.Epost, tiltakshaver.kontaktperson?.navn, tiltakshaver.kontaktperson?.telefonnummer, tiltakshaver.kontaktperson?.Epost, tiltakshaver.kontaktperson?.mobilnummer);
            return this;
        }
        internal RammetillatelseV3Builder EiendomByggested(SampleEiendomData.Eiendom eiendom)
        {
            EiendomByggested(eiendom.EiendomsIdentifikasjon.Kommunenummer, eiendom.EiendomsIdentifikasjon.Gaardsnummer, eiendom.EiendomsIdentifikasjon.Bruksnummer, eiendom.EiendomsIdentifikasjon.Festenummer, eiendom.EiendomsIdentifikasjon.Seksjonsnummer,
                eiendom.Kommunenavn, eiendom.Bolignummer, eiendom.Bygningsnummer, eiendom.Adresse.adresselinje1, eiendom.Adresse.gatenavn, eiendom.Adresse.husnr, eiendom.Adresse.postnr, eiendom.Adresse.poststed);
            return this;
        }

    }
}