﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.SampleData;
using no.kxml.skjema.dibk.endringavtillatelse;


namespace KommIT.FIKS.AdapterAltinnSvarUt.Tests.Services.Builders
{

    public class EndringAvTillatelseBuilder
    {
        private readonly EndringAvTillatelseType _form;

        public EndringAvTillatelseBuilder()
        {
            _form = new EndringAvTillatelseType();
        }

        public EndringAvTillatelseType Build()
        {
            return _form;
        }

        public EndringAvTillatelseBuilder EndringAvTillatelse(bool? endringAreal = null, bool? endringPlassering = null, bool? endringFormaal = null, bool? endringBruk = null, bool? endringSomKreverDispensasjon = null, bool? endringAnnet = null)
        {
            if (endringAreal.HasValue)
            {
                _form.endringArealSpecified = true;
                _form.endringAreal = endringAreal;
            }

            if (endringPlassering.HasValue)
            {
                _form.endringPlasseringSpecified = true;
                _form.endringPlassering = endringPlassering;
            }

            if (endringFormaal.HasValue)
            {
                _form.endringFormaalSpecified = true;
                _form.endringFormaal = endringFormaal;
            }

            if (endringBruk.HasValue)
            {
                _form.endringBrukSpecified = true;
                _form.endringBruk = endringBruk;
            }
            if (endringSomKreverDispensasjon.HasValue)
            {
                _form.endringSomKreverDispensasjonSpecified = true;
                _form.endringSomKreverDispensasjon = endringBruk;
            }
            if (endringAnnet.HasValue)
            {
                _form.endringAnnetSpecified = true;
                _form.endringAnnet = endringAnnet;
            }
            return this;
        }

        public EndringAvTillatelseBuilder FraSluttbrukersystem(string fraSluttbrukersystem)
        {
            _form.fraSluttbrukersystem = fraSluttbrukersystem;
            return this;
        }

        //endringSomKreverDispensasjon
        public EndringAvTillatelseBuilder Dispensasjon(string beskrivelse = null, string begrunnelse = null, string kodeverdi = null, string kodebeskrivelse = null)
        {

            var dispensajon = new DispensasjonType() { beskrivelse = beskrivelse, begrunnelse = begrunnelse };

            if (!string.IsNullOrEmpty(kodeverdi) || !string.IsNullOrEmpty(kodebeskrivelse))
            {
                dispensajon.dispensasjonstype = new KodeType()
                {
                    kodeverdi = kodeverdi,
                    kodebeskrivelse = kodebeskrivelse
                };

            }

            if (_form.dispensasjon == null)
            {
                _form.dispensasjon = new[] { dispensajon };
            }
            else
            {
                var dispensasjons = _form.dispensasjon.ToList();
                dispensasjons.Add(dispensajon);
                _form.dispensasjon = dispensasjons.ToArray();
            }
            return this;
        }

        // beskrivelseAvTiltak
        public EndringAvTillatelseBuilder BeskrivelseAvTiltak(string tiltakTypeKodeverdi = null, string tiltakTypeKodebeskrivelse = null, string beskrivPlanlagtFormaal = null, string tiltaksformaalKodeverdi = null, string tiltaksformaalKodebeskrivelse = null,
            string bygningstypeKodeverdi = null, string bygningstypeKodebeskrivelse = null, string naeringsgruppeKodeverdi = null, string naeringsgruppeKodebeskrivelse = null, string anleggstypeKodeverdi = null, string anleggstypeKodebeskrivelse = null, string foelgebrev = null)
        {
            _form.beskrivelseAvTiltak = new TiltakType()
            {
                foelgebrev = foelgebrev,
            };

            if (!string.IsNullOrEmpty(tiltakTypeKodeverdi) || !string.IsNullOrEmpty(tiltakTypeKodebeskrivelse))
            {
                _form.beskrivelseAvTiltak.type = new[]
                {
                    new KodeType()
                    {
                        kodeverdi = tiltakTypeKodeverdi,
                        kodebeskrivelse = tiltakTypeKodebeskrivelse
                    }
                };
            }

            if (!string.IsNullOrEmpty(tiltaksformaalKodeverdi) || !string.IsNullOrEmpty(tiltaksformaalKodebeskrivelse) || !string.IsNullOrEmpty(bygningstypeKodeverdi) || !string.IsNullOrEmpty(bygningstypeKodebeskrivelse) ||
                !string.IsNullOrEmpty(naeringsgruppeKodeverdi) || !string.IsNullOrEmpty(naeringsgruppeKodebeskrivelse) || !string.IsNullOrEmpty(anleggstypeKodeverdi) || !string.IsNullOrEmpty(anleggstypeKodebeskrivelse))
            {
                _form.beskrivelseAvTiltak.bruk = new FormaalType();
                if (!string.IsNullOrEmpty(tiltaksformaalKodeverdi) || !string.IsNullOrEmpty(tiltaksformaalKodebeskrivelse))
                {
                    BeskrivelseAvTiltakBrukTiltaksformaal(tiltaksformaalKodeverdi, tiltaksformaalKodebeskrivelse);
                };
                if (!string.IsNullOrEmpty(bygningstypeKodeverdi) || !string.IsNullOrEmpty(bygningstypeKodebeskrivelse))
                {
                    _form.beskrivelseAvTiltak.bruk.bygningstype =
                        new KodeType()
                        {
                            kodeverdi = bygningstypeKodeverdi,
                            kodebeskrivelse = bygningstypeKodebeskrivelse
                        };
                };
                if (!string.IsNullOrEmpty(naeringsgruppeKodeverdi) || !string.IsNullOrEmpty(naeringsgruppeKodebeskrivelse))
                {
                    _form.beskrivelseAvTiltak.bruk.naeringsgruppe =
                        new KodeType()
                        {
                            kodeverdi = naeringsgruppeKodeverdi,
                            kodebeskrivelse = naeringsgruppeKodebeskrivelse
                        };
                };
                if (!string.IsNullOrEmpty(anleggstypeKodeverdi) || !string.IsNullOrEmpty(anleggstypeKodebeskrivelse))
                {
                    _form.beskrivelseAvTiltak.bruk.anleggstype =
                        new KodeType()
                        {
                            kodeverdi = anleggstypeKodeverdi,
                            kodebeskrivelse = anleggstypeKodebeskrivelse
                        };
                };
                _form.beskrivelseAvTiltak.bruk.beskrivPlanlagtFormaal = beskrivPlanlagtFormaal;

            };

            return this;
        }

        public EndringAvTillatelseBuilder BeskrivelseAvTiltakBrukTiltaksformaal(string kodeverdi = null, string kodebeskrivelse = null)
        {
            if (_form.beskrivelseAvTiltak == null)
            {
                _form.beskrivelseAvTiltak = new TiltakType();
            }
            else if (_form.beskrivelseAvTiltak.bruk == null)
            {
                _form.beskrivelseAvTiltak.bruk = new FormaalType();
            }
            var tiltakformaal = new KodeType()
            {
                kodeverdi = kodeverdi,
                kodebeskrivelse = kodebeskrivelse
            };

            if (_form.beskrivelseAvTiltak.bruk.tiltaksformaal == null)
            {
                _form.beskrivelseAvTiltak.bruk.tiltaksformaal = new KodeType[] { tiltakformaal };
            }
            else
            {
                var tiltakformaals = _form.beskrivelseAvTiltak.bruk.tiltaksformaal.ToList();
                tiltakformaals.Add(tiltakformaal);
                _form.beskrivelseAvTiltak.bruk.tiltaksformaal = tiltakformaals.ToArray();
            }

            return this;
        }

        // EndringAvTillatelseType.RammerType.Adkomst
        public EndringAvTillatelseBuilder RammebetingelserAdkomst(string vegTypeKodeverdi, string vegTypeKodebeskrivelse, bool? nyeEndretAdkomst,
            bool? erTillatelseGittPrivatVeg,
            bool? erTillatelseGittKommunalVeg, bool? erTillatelseGittRiksFylkesveg)
        {
            var adkomst = new VegType();

            adkomst.vegtype = AddVegType(vegTypeKodeverdi, vegTypeKodebeskrivelse);

            if (nyeEndretAdkomst.HasValue)
            {
                adkomst.nyeEndretAdkomst = nyeEndretAdkomst.Value;
                adkomst.nyeEndretAdkomstSpecified = true;
            }

            if (erTillatelseGittPrivatVeg.HasValue)
            {
                adkomst.erTillatelseGittPrivatVegSpecified = true;
                adkomst.erTillatelseGittPrivatVeg = erTillatelseGittPrivatVeg.Value;
            }

            if (erTillatelseGittKommunalVeg.HasValue)
            {
                adkomst.erTillatelseGittKommunalVeg = erTillatelseGittKommunalVeg.Value;
                adkomst.erTillatelseGittKommunalVegSpecified = true;
            }

            if (erTillatelseGittRiksFylkesveg.HasValue)
            {
                adkomst.erTillatelseGittRiksFylkesveg = erTillatelseGittRiksFylkesveg.Value;
                adkomst.erTillatelseGittRiksFylkesvegSpecified = true;
            }
            AddRammebetingelserToFrom(adkomst, null, null, null, null, null);
            return this;
        }


        // EndringAvTillatelseType.RammerType.Arealdisponering
        public EndringAvTillatelseBuilder RammebetingelserArealdisponering(
            double? arealBebyggelseEksisterende, double? arealBebyggelseNytt, double? arealBebyggelseSomSkalRives,
            double? parkeringsarealTerreng,
            double? beregnetGradAvUtnytting, double? tomtearealBeregnet, double? arealSumByggesak, double? tomtearealByggeomraade, double? tomtearealSomTrekkesFra, double? tomtearealSomLeggesTil, double? beregnetMaksByggeareal)
        {
            // Arealdisponering

            var arealdisponering = new ArealdisponeringType();

            if (tomtearealByggeomraade.HasValue)
            {
                arealdisponering.tomtearealByggeomraadeSpecified = true;
                arealdisponering.tomtearealByggeomraade = tomtearealByggeomraade.Value;
            }
            if (tomtearealSomTrekkesFra.HasValue)
            {
                arealdisponering.tomtearealSomTrekkesFraSpecified = true;
                arealdisponering.tomtearealSomTrekkesFra = tomtearealSomTrekkesFra.Value;
            }
            if (tomtearealSomLeggesTil.HasValue)
            {
                arealdisponering.tomtearealSomLeggesTilSpecified = true;
                arealdisponering.tomtearealSomLeggesTil = tomtearealSomLeggesTil.Value;
            }
            if (beregnetMaksByggeareal.HasValue)
            {
                arealdisponering.beregnetMaksByggearealSpecified = true;
                arealdisponering.beregnetMaksByggeareal = beregnetMaksByggeareal.Value;
            }
            if (arealBebyggelseEksisterende.HasValue)
            {
                arealdisponering.arealBebyggelseEksisterendeSpecified = true;
                arealdisponering.arealBebyggelseEksisterende = arealBebyggelseEksisterende;
            }

            if (arealBebyggelseSomSkalRives.HasValue)
            {
                arealdisponering.arealBebyggelseSomSkalRivesSpecified = true;
                arealdisponering.arealBebyggelseSomSkalRives = arealBebyggelseSomSkalRives;
            }

            if (arealBebyggelseNytt.HasValue)
            {
                arealdisponering.arealBebyggelseNyttSpecified = true;
                arealdisponering.arealBebyggelseNytt = arealBebyggelseNytt;
            }

            if (arealSumByggesak.HasValue)
            {
                arealdisponering.arealSumByggesakSpecified = true;
                arealdisponering.arealSumByggesak = arealSumByggesak;
            }

            if (beregnetGradAvUtnytting.HasValue)
            {
                arealdisponering.beregnetGradAvUtnyttingSpecified = true;
                arealdisponering.beregnetGradAvUtnytting = beregnetGradAvUtnytting;
            }

            if (parkeringsarealTerreng.HasValue)
            {
                arealdisponering.parkeringsarealTerrengSpecified = true;
                arealdisponering.parkeringsarealTerreng = parkeringsarealTerreng;
            }

            if (tomtearealBeregnet.HasValue)
            {
                arealdisponering.tomtearealBeregnetSpecified = true;
                arealdisponering.tomtearealBeregnet = tomtearealBeregnet;
            }

            //if (gjeldendePlan)
            //{
            //    _form.rammebetingelser.gjeldendePlan = new[]
            //    {
            //        new PlanType()
            //        {
            //            utnyttingsgrad = 23.0,
            //            plantype = new KodeType
            //            {
            //                kodeverdi = "RP",
            //                kodebeskrivelse = "Reguleringsplan"
            //            },
            //            navn = "Naustgrendanabbevannet",
            //            formaal = "Fritidsbegyggelse",
            //            andreRelevanteKrav = "andre relevante krav her",
            //        }
            //        };
            //    if (!string.IsNullOrEmpty(beregningsregelGradAvUtnyttingKode))
            //    {
            //        _form.rammebetingelser.gjeldendePlan[0].beregningsregelGradAvUtnytting = new KodeType
            //        {
            //            kodeverdi = beregningsregelGradAvUtnyttingKode,
            //            kodebeskrivelse = beregningsregelGradAvUtnyttingKodeBeskrivelse
            //        };
            //    };
            //}

            AddRammebetingelserToFrom(null, arealdisponering, null, null, null, null);
            return this;
        }

        public EndringAvTillatelseBuilder EndringAvTillatelseRammerType()
        {
            AddRammebetingelserToFrom(null, null, null, null, null, null);
            return this;
        }

        // EndringAvTillatelseType.RammerType.GenerelleVilkaar
        public EndringAvTillatelseBuilder RammebetingerlserGenerelleVilkaar(bool? paalagtUavhengigKontroll,
            bool? beroererArbeidsplasser, bool? utarbeideAvfallsplan, bool? behovForTillatelse)
        {
            var generelleVilkaar = new GenerelleVilkaarType();

            if (behovForTillatelse.HasValue)
            {
                generelleVilkaar.behovForTillatelseSpecified = true;
                generelleVilkaar.behovForTillatelse = behovForTillatelse.Value;
            }

            if (paalagtUavhengigKontroll.HasValue)
            {
                generelleVilkaar.paalagtUavhengigKontrollSpecified = true;
                generelleVilkaar.paalagtUavhengigKontroll = paalagtUavhengigKontroll;
            }

            if (beroererArbeidsplasser.HasValue)
            {
                generelleVilkaar.beroererArbeidsplasserSpecified = true;
                generelleVilkaar.beroererArbeidsplasser = beroererArbeidsplasser.Value;
            }

            if (utarbeideAvfallsplan.HasValue)
            {
                generelleVilkaar.utarbeideAvfallsplanSpecified = true;
                generelleVilkaar.utarbeideAvfallsplan = utarbeideAvfallsplan;
            }

            AddRammebetingelserToFrom(null, null, generelleVilkaar, null, null, null);
            return this;
        }

        // EndringAvTillatelseType.RammerType.kravTilByggegrunn
        public EndringAvTillatelseBuilder RammebetingelserKravTilByggegrunn(bool? flomutsattOmraade,
            bool? skredutsattOmraade, bool? miljoeforhold, bool? f1, bool? f2, bool? f3, bool? s1, bool? s2, bool? s3)
        {
            var kravTilByggegrunn = new KravTilByggegrunnType();

            if (flomutsattOmraade.HasValue)
            {
                kravTilByggegrunn.flomutsattOmraadeSpecified = true;
                kravTilByggegrunn.flomutsattOmraade = flomutsattOmraade.Value;
            }

            if (skredutsattOmraade.HasValue)
            {
                kravTilByggegrunn.skredutsattOmraadeSpecified = true;
                kravTilByggegrunn.skredutsattOmraade = skredutsattOmraade.Value;
            }

            if (miljoeforhold.HasValue)
            {
                kravTilByggegrunn.miljoeforholdSpecified = true;
                kravTilByggegrunn.miljoeforhold = miljoeforhold.Value;
            }

            if (f1.HasValue)
            {
                kravTilByggegrunn.f1Specified = true;
                kravTilByggegrunn.f1 = f1.Value;
            }
            if (f2.HasValue)
            {
                kravTilByggegrunn.f2Specified = true;
                kravTilByggegrunn.f2 = f2.Value;
            }
            if (f3.HasValue)
            {
                kravTilByggegrunn.f3Specified = true;
                kravTilByggegrunn.f3 = f3.Value;
            }
            if (s1.HasValue)
            {
                kravTilByggegrunn.s1Specified = true;
                kravTilByggegrunn.s1 = s1.Value;
            }
            if (s2.HasValue)
            {
                kravTilByggegrunn.s2Specified = true;
                kravTilByggegrunn.s2 = s2.Value;
            }
            if (s3.HasValue)
            {
                kravTilByggegrunn.s3Specified = true;
                kravTilByggegrunn.s3 = s3.Value;
            }
            AddRammebetingelserToFrom(null, null, null, null, kravTilByggegrunn, null);
            return this;
        }

        private bool? AddBooleanValueTokravTilByggegrunn(bool? boolean)
        {
            if (boolean.HasValue)
            {
                if (_form.rammebetingelser.kravTilByggegrunn == null)
                    _form.rammebetingelser.kravTilByggegrunn = new KravTilByggegrunnType();
                return boolean.Value;
            }
            return null;
        }

        // EndringAvTillatelseType.RammerType.plassering
        public EndringAvTillatelseBuilder rammebetingelserPlassering(bool? konfliktHoeyspentkraftlinje, bool? konfliktVannOgAvloep)
        {
            var plassering = new PlasseringType();
            if (konfliktHoeyspentkraftlinje.HasValue)
            {
                plassering.konfliktHoeyspentkraftlinjeSpecified = true;
                plassering.konfliktHoeyspentkraftlinje = konfliktHoeyspentkraftlinje.Value;
            }
            if (konfliktVannOgAvloep.HasValue)
            {
                plassering.konfliktVannOgAvloepSpecified = true;
                plassering.konfliktVannOgAvloep = konfliktVannOgAvloep.Value;
            }

            AddRammebetingelserToFrom(null, null, null, null, null, plassering);
            return this;
        }

        // EndringAvTillatelseType.VarslingType
        public EndringAvTillatelseBuilder Varsling(bool? fritattFraNabovarsling, bool? foreliggerMerknader, string vurderingAvMerknader = null, string antallMerknader = null, string varslingsMetodeKode = null, string varslingsMetodeBeskrivelse = null)
        {
            _form.varsling = new VarslingType();
            if (foreliggerMerknader.HasValue)
            {
                _form.varsling.foreliggerMerknaderSpecified = true;
                _form.varsling.foreliggerMerknader = foreliggerMerknader.Value;
            }

            if (fritattFraNabovarsling.HasValue)
            {
                _form.varsling.fritattFraNabovarslingSpecified = true;
                _form.varsling.fritattFraNabovarsling = fritattFraNabovarsling.Value;
            }

            if (!string.IsNullOrEmpty(antallMerknader) || !string.IsNullOrEmpty(vurderingAvMerknader))
            {
                _form.varsling.antallMerknader = antallMerknader;
                _form.varsling.vurderingAvMerknader = vurderingAvMerknader;
                _form.varsling.soeknadensHjemmeside = "http://www.dibk.no";
                _form.varsling.soeknadSeesKontaktperson = "Hilde";
            }
            if (!string.IsNullOrEmpty(varslingsMetodeKode) || !string.IsNullOrEmpty(varslingsMetodeBeskrivelse))
            {
                _form.varsling.varslingsMetode = new KodeType()
                {
                    kodeverdi = varslingsMetodeKode,
                    kodebeskrivelse = varslingsMetodeBeskrivelse
                };
            }

            return this;
        }

        public EndringAvTillatelseBuilder AnsvarligSoeker(string organisasjonsnummer, string foedselsnummer, string partsTypeKodeverdi = null, string partsTypeKodebeskrivelse = null, string kontaktperson = null, string navn = null, string adressLine = null, string postnr = null, string landkode = null
            , string poststed = null, string telefonnummer = null, string mobilnummer = null)
        {
            _form.ansvarligSoeker = new PartType();

            if (!string.IsNullOrEmpty(partsTypeKodeverdi))
            {
                _form.ansvarligSoeker.partstype = new KodeType
                {
                    kodeverdi = partsTypeKodeverdi,
                    kodebeskrivelse = ""
                };
            }

            _form.ansvarligSoeker.navn = navn;

            if (!string.IsNullOrEmpty(adressLine) || !string.IsNullOrEmpty(postnr) || !string.IsNullOrEmpty(landkode) || !string.IsNullOrEmpty(poststed))
            {
                _form.ansvarligSoeker.adresse = new EnkelAdresseType
                {
                    adresselinje1 = adressLine,
                    postnr = postnr,
                    landkode = landkode,
                    poststed = poststed
                };
            }

            _form.ansvarligSoeker.organisasjonsnummer = organisasjonsnummer; //"974760673" - 974760673 BRREG, Altinn brukerservice
            _form.ansvarligSoeker.kontaktperson = kontaktperson;
            _form.ansvarligSoeker.foedselsnummer = foedselsnummer;
            _form.ansvarligSoeker.telefonnummer = telefonnummer;
            _form.ansvarligSoeker.mobilnummer = mobilnummer;

            return this;
        }
        public EndringAvTillatelseBuilder Tiltakshaver(string organisasjonsnummer, string foedselsnummer, string partsTypeKodeverdi = null, string partsTypeKodebeskrivelse = null, string kontaktperson = null, string navn = null, string adressLine = null, string postnr = null, string landkode = null
             , string poststed = null, string telefonnummer = null, string mobilnummer = null)
        {
            _form.tiltakshaver = new PartType();

            if (!string.IsNullOrEmpty(partsTypeKodeverdi) || !string.IsNullOrEmpty(partsTypeKodebeskrivelse))
            {
                _form.tiltakshaver.partstype = new KodeType
                {
                    kodeverdi = partsTypeKodeverdi,
                    kodebeskrivelse = partsTypeKodebeskrivelse
                };
            }

            _form.tiltakshaver.navn = navn;

            if (!string.IsNullOrEmpty(adressLine) || !string.IsNullOrEmpty(postnr) || !string.IsNullOrEmpty(landkode))
            {
                _form.tiltakshaver.adresse = new EnkelAdresseType
                {
                    adresselinje1 = adressLine,
                    postnr = postnr,
                    landkode = landkode,
                    poststed = poststed
                };
            }

            _form.tiltakshaver.organisasjonsnummer = organisasjonsnummer; //"974760673" - 974760673 BRREG, Altinn brukerservice
            _form.tiltakshaver.kontaktperson = kontaktperson;
            _form.tiltakshaver.foedselsnummer = foedselsnummer;
            _form.tiltakshaver.telefonnummer = telefonnummer;
            _form.tiltakshaver.mobilnummer = mobilnummer;

            return this;
        }

        internal EndringAvTillatelseBuilder EiendomByggested(string knr = null, string gnr = null, string bnr = null, string fnr = null, string snr = null, string kommunenavn = null, string bolignummer = null, string bygningsnummer = null
            , string adresselinje1 = null, string gatenavn = null, string husnr = null, string postnr = null, string poststed = null)
        {
            var eiendomByggested = new EiendomType();
            eiendomByggested.bolignummer = bolignummer;
            eiendomByggested.kommunenavn = kommunenavn;
            eiendomByggested.eiendomsidentifikasjon = new MatrikkelnummerType()
            {
                kommunenummer = knr,
                gaardsnummer = gnr,
                bruksnummer = bnr,
                festenummer = fnr,
                seksjonsnummer = snr
            };

            eiendomByggested.bygningsnummer = bygningsnummer;
            if (!string.IsNullOrEmpty(adresselinje1) || !string.IsNullOrEmpty(gatenavn) || !string.IsNullOrEmpty(husnr))
            {
                eiendomByggested.adresse = new EiendommensAdresseType()
                {
                    adresselinje1 = adresselinje1,
                    gatenavn = gatenavn,
                    husnr = husnr,
                    postnr = postnr,
                    poststed = poststed
                };
            }

            AddEiendomToFrom(eiendomByggested);
            return this;
        }
        public EndringAvTillatelseBuilder RammebetingelserGjeldendePlan(string plantypekodeverdi = null, string plantypekodebeskrivelse = null, string beregningsregelGradAvUtnyttingKode = null,
            string beregningsregelGradAvUtnyttingKodeBeskrivelse = null, string navn = null, string andreRelevanteKrav = null, string formaal = null, double? utnyttingsgrad = null)
        {
            var gjeldendePlan = new PlanType()
            {
                navn = navn,
                andreRelevanteKrav = andreRelevanteKrav,
                formaal = formaal,
            };

            if (utnyttingsgrad.HasValue)
            {
                gjeldendePlan.utnyttingsgradSpecified = true;
                gjeldendePlan.utnyttingsgrad = utnyttingsgrad.Value;

            }
            if (!string.IsNullOrEmpty(plantypekodeverdi) || !string.IsNullOrEmpty(plantypekodebeskrivelse))
                gjeldendePlan.plantype = AddKodeType(plantypekodeverdi, plantypekodebeskrivelse);

            if (!string.IsNullOrEmpty(beregningsregelGradAvUtnyttingKode) || !string.IsNullOrEmpty(beregningsregelGradAvUtnyttingKodeBeskrivelse))
                gjeldendePlan.beregningsregelGradAvUtnytting = AddKodeType(beregningsregelGradAvUtnyttingKode, beregningsregelGradAvUtnyttingKodeBeskrivelse);


            AddRammebetingelserToFrom(null, null, null, gjeldendePlan);
            return this;
        }

        private static KodeType AddKodeType(string kodeverdi, string kodebeskrivelse)
        {
            if (!string.IsNullOrEmpty(kodeverdi) || !string.IsNullOrEmpty(kodebeskrivelse))
            {
                return new KodeType() { kodeverdi = kodeverdi, kodebeskrivelse = kodebeskrivelse };

            }
            return null;
        }
        private KodeType[] AddVegType(string kodeverdi, string kodebeskrivelse)
        {
            if (!string.IsNullOrEmpty(kodeverdi) || !string.IsNullOrEmpty(kodebeskrivelse))
            {
                KodeType[] kodeTypes;
                if (_form.rammebetingelser?.adkomst?.vegtype == null)
                {
                    kodeTypes = new[] { AddKodeType(kodeverdi, kodebeskrivelse) };
                }
                else
                {
                    var vegTypes = _form.rammebetingelser?.adkomst?.vegtype.ToList();
                    vegTypes.Add(AddKodeType(kodeverdi, kodebeskrivelse));
                    kodeTypes = vegTypes.ToArray();
                }
                return kodeTypes;
            }

            return null;
        }

        private void AddRammebetingelserToFrom(VegType adkomst = null, ArealdisponeringType arealdisponering = null, GenerelleVilkaarType generelleVilkaar = null, PlanType gjeldendePlan = null,
            KravTilByggegrunnType kravTilByggegrunn = null, PlasseringType plassering = null)
        {
            if (_form.rammebetingelser == null)
            {
                _form.rammebetingelser = new RammerType();
            }

            if (adkomst != null)
                _form.rammebetingelser.adkomst = adkomst;
            if (arealdisponering != null)
                _form.rammebetingelser.arealdisponering = arealdisponering;
            if (generelleVilkaar != null)
                _form.rammebetingelser.generelleVilkaar = generelleVilkaar;
            if (gjeldendePlan != null)
                _form.rammebetingelser.gjeldendePlan = AddGjeldendePlan(gjeldendePlan);
            if (kravTilByggegrunn != null)
                _form.rammebetingelser.kravTilByggegrunn = kravTilByggegrunn;
            if (plassering != null)
                _form.rammebetingelser.plassering = plassering;
        }
        private PlanType[] AddGjeldendePlan(PlanType gjeldendePlan)
        {

            PlanType[] planTypes;
            if (_form.rammebetingelser?.gjeldendePlan == null)
            {
                planTypes = new[] { gjeldendePlan };
            }
            else
            {
                var planTypesTemp = _form.rammebetingelser?.gjeldendePlan.ToList();
                planTypesTemp.Add(gjeldendePlan);
                planTypes = planTypesTemp.ToArray();
            }
            return planTypes;
        }
        private void AddEiendomToFrom(EiendomType eiendomByggested)
        {
            if (_form.eiendomByggested == null)
            {
                _form.eiendomByggested = new[] { eiendomByggested };
            }
            else
            {
                var eiendomTypes = _form.eiendomByggested.ToList();
                eiendomTypes.Add(eiendomByggested);
                _form.eiendomByggested = eiendomTypes.ToArray();
            }
        }

        public EndringAvTillatelseBuilder Rammebetingelser()
        {
            AddRammebetingelserToFrom();
            return this;
        }

        public EndringAvTillatelseBuilder kommunensSaksnummer(string sakssekvensnummer = null, string saksaar = null)
        {
            _form.kommunensSaksnummer = new SaksnummerType()
            {
                sakssekvensnummer = sakssekvensnummer,
                saksaar = saksaar
            };
            return this;
        }
        public EndringAvTillatelseBuilder Foelgebrev(string foelgebrev)
        {
            if (_form.beskrivelseAvTiltak == null)
            {
                _form.beskrivelseAvTiltak = new TiltakType();
            }
            _form.beskrivelseAvTiltak.foelgebrev = foelgebrev;
            return this;
        }
        internal EndringAvTillatelseBuilder AnsvarligSoeker(SampleAktoerData.Aktoer ansvarligSoeker)
        {
            AnsvarligSoeker(ansvarligSoeker.organisasjonsnummer, ansvarligSoeker.foedselsnummer, ansvarligSoeker.partsTypeKodeverdi, ansvarligSoeker.partsTypeKodebeskrivelse, ansvarligSoeker.kontaktperson?.navn, ansvarligSoeker.navn, ansvarligSoeker.Adresse?.adresselinje1,
                ansvarligSoeker.Adresse?.postnr, ansvarligSoeker.Adresse?.landkode, ansvarligSoeker.Adresse?.poststed, ansvarligSoeker.telefonnummer, ansvarligSoeker.mobilnummer);
            return this;
        }
        internal EndringAvTillatelseBuilder Tiltakshaver(SampleAktoerData.Aktoer tiltakshaver)
        {
            Tiltakshaver(tiltakshaver.organisasjonsnummer, tiltakshaver.foedselsnummer, tiltakshaver.partsTypeKodeverdi, tiltakshaver.partsTypeKodebeskrivelse, tiltakshaver.kontaktperson?.navn, tiltakshaver.navn, tiltakshaver.Adresse?.adresselinje1,
                tiltakshaver.Adresse?.postnr, tiltakshaver.Adresse?.landkode, tiltakshaver.Adresse?.poststed, tiltakshaver.telefonnummer, tiltakshaver.mobilnummer);
            return this;
        }
        internal EndringAvTillatelseBuilder EiendomByggested(SampleEiendomData.Eiendom eiendom)
        {
            EiendomByggested(eiendom.EiendomsIdentifikasjon.Kommunenummer, eiendom.EiendomsIdentifikasjon.Gaardsnummer, eiendom.EiendomsIdentifikasjon.Bruksnummer, eiendom.EiendomsIdentifikasjon.Festenummer, eiendom.EiendomsIdentifikasjon.Seksjonsnummer,
                eiendom.Kommunenavn, eiendom.Bolignummer, eiendom.Bygningsnummer, eiendom.Adresse.adresselinje1, eiendom.Adresse.gatenavn, eiendom.Adresse.husnr, eiendom.Adresse.postnr, eiendom.Adresse.poststed);
            return this;
        }

        public EndringAvTillatelseBuilder ProsjektInfo(string prosjektnavn)
        {
            _form.prosjektnavn = prosjektnavn;
            return this;
        }
    }
}

