﻿using System;
using System.Linq;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.SampleData;
using no.kxml.skjema.dibk.samsvarserklaeringDirekteOpprettet;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.Builders
{
    public class SamsvarserklaeringDirekteBuilder
    {
        private readonly no.kxml.skjema.dibk.samsvarserklaeringDirekteOpprettet.SamsvarserklaeringDirekteType _form;

        public SamsvarserklaeringDirekteBuilder()
        {
            _form = new SamsvarserklaeringDirekteType();
        }

        public SamsvarserklaeringDirekteType Build()
        {
            return _form;
        }


        public SamsvarserklaeringDirekteBuilder ProsjektData(string fraStluttbrukersystem = null, string prosjektnr = null, string hovedinnsendingsnummer = null, string prosjektnavn = null, bool? erTek10 = null)
        {
            _form.fraSluttbrukersystem = fraStluttbrukersystem;
            _form.prosjektnr = prosjektnr;
            _form.hovedinnsendingsnummer = hovedinnsendingsnummer;
            _form.prosjektnavn = prosjektnavn;


            if (erTek10.HasValue)
            {
                _form.erTEK10 = erTek10.Value;
            }
            else
            {
                //predefined 
                _form.erTEK10 = false;

            }

            return this;
        }

        public SamsvarserklaeringDirekteBuilder KommunensSaksnummer(string saksaar, string sakssekvensnummer)
        {
            _form.kommunensSaksnummer = new SaksnummerType() { saksaar = saksaar, sakssekvensnummer = sakssekvensnummer };

            return this;
        }

        internal SamsvarserklaeringDirekteBuilder Ansvarsrett(string funksjonKodeverdi = null, string funksjonKodebeskrivelse = null,
            bool? okForFerdigattestPRO = null, bool? okForRammetillatelsePRO = null, bool? okForIgangsettingPRO = null, bool? okForMidlertidigBrukstillatelsePRO = null,
            bool? OkForFerdigattestUTF = null, bool? okMidlertidigBrukstillatelseUTF = null, bool? harTilstrekkeligSikkerhetUTF = null, string gjenstaaendeUtenforUTF = null, string gjenstaaendeInnenforUTF = null,
            string typeArbeiderUTF = null, DateTime? utfoertInnenUTF = null, string beskrivelseAvAnsvarsomraadet = null, bool? ansvarsomraadetAvsluttet = null)
        {

            _form.ansvarsrett = new AnsvarsomraadeType();
            if (funksjonKodeverdi != null)
            {
                _form.ansvarsrett.funksjon = new KodeType()
                {
                    kodeverdi = funksjonKodeverdi,
                    kodebeskrivelse = funksjonKodebeskrivelse
                };
            }

            if (okForFerdigattestPRO.HasValue || okForRammetillatelsePRO.HasValue || okForIgangsettingPRO.HasValue || okForMidlertidigBrukstillatelsePRO.HasValue)
            {
                _form.ansvarsrett.prosjekterende = new ProsjekterendeType();
                if (okForFerdigattestPRO.HasValue)
                {
                    _form.ansvarsrett.prosjekterende.okForFerdigattestSpecified = true;
                    _form.ansvarsrett.prosjekterende.okForFerdigattest = okForFerdigattestPRO.Value;
                }
                if (okForRammetillatelsePRO.HasValue)
                {
                    _form.ansvarsrett.prosjekterende.okForRammetillatelseSpecified = true;
                    _form.ansvarsrett.prosjekterende.okForRammetillatelse = okForRammetillatelsePRO.Value;
                }
                if (okForIgangsettingPRO.HasValue)
                {
                    _form.ansvarsrett.prosjekterende.okForIgangsettingSpecified = true;
                    _form.ansvarsrett.prosjekterende.okForIgangsetting = okForIgangsettingPRO.Value;
                }
                if (okForMidlertidigBrukstillatelsePRO.HasValue)
                {
                    _form.ansvarsrett.prosjekterende.okForMidlertidigBrukstillatelseSpecified = true;
                    _form.ansvarsrett.prosjekterende.okForMidlertidigBrukstillatelse = okForMidlertidigBrukstillatelsePRO.Value;
                }

            }

            if (OkForFerdigattestUTF.HasValue || okMidlertidigBrukstillatelseUTF.HasValue || !string.IsNullOrWhiteSpace(gjenstaaendeUtenforUTF) || !string.IsNullOrWhiteSpace(gjenstaaendeInnenforUTF) || !string.IsNullOrWhiteSpace(typeArbeiderUTF)
                || harTilstrekkeligSikkerhetUTF.HasValue || utfoertInnenUTF.HasValue)
            {
                _form.ansvarsrett.utfoerende = new UtfoerendeType();
                if (OkForFerdigattestUTF.HasValue)
                {
                    _form.ansvarsrett.utfoerende.okForFerdigattestSpecified = true;
                    _form.ansvarsrett.utfoerende.okForFerdigattest = OkForFerdigattestUTF;
                }
                if (okMidlertidigBrukstillatelseUTF.HasValue)
                {
                    _form.ansvarsrett.utfoerende.okMidlertidigBrukstillatelseSpecified = true;
                    _form.ansvarsrett.utfoerende.okMidlertidigBrukstillatelse = okMidlertidigBrukstillatelseUTF;
                }
                if (!string.IsNullOrWhiteSpace(gjenstaaendeUtenforUTF) || !string.IsNullOrWhiteSpace(gjenstaaendeInnenforUTF))
                {
                    _form.ansvarsrett.utfoerende.midlertidigBrukstillatelseGjenstaaende = new DelsoeknadMidlertidigBrukstillatelseType()
                    {
                        gjenstaaendeUtenfor = gjenstaaendeUtenforUTF,
                        gjenstaaendeInnenfor = gjenstaaendeInnenforUTF
                    };
                }

                if (harTilstrekkeligSikkerhetUTF.HasValue)
                {
                    _form.ansvarsrett.utfoerende.harTilstrekkeligSikkerhetSpecified = true;
                    _form.ansvarsrett.utfoerende.harTilstrekkeligSikkerhet = harTilstrekkeligSikkerhetUTF.Value;
                }
                _form.ansvarsrett.utfoerende.typeArbeider = typeArbeiderUTF;
                if (utfoertInnenUTF.HasValue)
                {
                    _form.ansvarsrett.utfoerende.utfoertInnenSpecified = true;
                    _form.ansvarsrett.utfoerende.utfoertInnen = utfoertInnenUTF;
                }
            }

            _form.ansvarsrett.beskrivelseAvAnsvarsomraadet = beskrivelseAvAnsvarsomraadet;
            if (ansvarsomraadetAvsluttet.HasValue)
            {
                _form.ansvarsrett.ansvarsomraadetAvsluttetSpecified = true; 
                _form.ansvarsrett.ansvarsomraadetAvsluttet = ansvarsomraadetAvsluttet.Value; 
            }

            return this;
        }

        public SamsvarserklaeringDirekteBuilder Foretak(string organisasjonsnummer, string partsTypeKodeverdi = null, string partsTypeKodebeskrivelse = null, string kontaktperson = null, string navn = null, string adressLine = null, string postnr = null, string landkode = null
            , string poststed = null, string telefonnummer = null, string mobilnummer = null, string epost = null)
        {
            _form.foretak = new PartType();

            if (!string.IsNullOrEmpty(partsTypeKodeverdi) || !string.IsNullOrEmpty(partsTypeKodebeskrivelse))
            {
                _form.foretak.partstype = new KodeType
                {
                    kodeverdi = partsTypeKodeverdi,
                    kodebeskrivelse = partsTypeKodebeskrivelse
                };
            }

            _form.foretak.navn = navn;

            if (!string.IsNullOrEmpty(adressLine) || !string.IsNullOrEmpty(postnr) || !string.IsNullOrEmpty(landkode))
            {
                _form.foretak.adresse = new EnkelAdresseType
                {
                    adresselinje1 = adressLine,
                    postnr = postnr,
                    landkode = landkode,
                    poststed = poststed
                };
            }

            if (!string.IsNullOrEmpty(kontaktperson))
            {
                _form.foretak.kontaktperson = new KontaktpersonType()
                {
                    navn = kontaktperson
                };
            }

            _form.foretak.organisasjonsnummer = organisasjonsnummer; //"974760673" - 974760673 BRREG, Altinn brukerservice
            _form.foretak.telefonnummer = telefonnummer;
            _form.foretak.mobilnummer = mobilnummer;
            _form.foretak.epost = epost;

            return this;
        }

        public SamsvarserklaeringDirekteBuilder AnsvarligSoeker(string organisasjonsnummer, string partsTypeKodeverdi = null, string partsTypeKodebeskrivelse = null, string kontaktperson = null, string navn = null, string adressLine = null, string postnr = null, string landkode = null
            , string poststed = null, string telefonnummer = null, string mobilnummer = null, string epost = null)
        {
            _form.ansvarligSoeker = new PartType();

            if (!string.IsNullOrEmpty(partsTypeKodeverdi) || !string.IsNullOrEmpty(partsTypeKodebeskrivelse))
            {
                _form.ansvarligSoeker.partstype = new KodeType
                {
                    kodeverdi = partsTypeKodeverdi,
                    kodebeskrivelse = partsTypeKodebeskrivelse
                };
            }

            _form.ansvarligSoeker.navn = navn;

            if (!string.IsNullOrEmpty(adressLine) || !string.IsNullOrEmpty(postnr) || !string.IsNullOrEmpty(landkode))
            {
                _form.ansvarligSoeker.adresse = new EnkelAdresseType
                {
                    adresselinje1 = adressLine,
                    postnr = postnr,
                    landkode = landkode,
                    poststed = poststed
                };
            }

            if (!string.IsNullOrEmpty(kontaktperson))
            {
                _form.ansvarligSoeker.kontaktperson = new KontaktpersonType()
                {
                    navn = kontaktperson
                };
            }

            _form.ansvarligSoeker.organisasjonsnummer = organisasjonsnummer; //"974760673" - 974760673 BRREG, Altinn brukerservice
            _form.ansvarligSoeker.telefonnummer = telefonnummer;
            _form.ansvarligSoeker.mobilnummer = mobilnummer;
            _form.ansvarligSoeker.epost = epost;

            return this;
        }

        public SamsvarserklaeringDirekteBuilder ErklaeringUtforelse(bool? erklaeringUtfoerelse)
        {
            _form.erklaeringUtfoerelse = erklaeringUtfoerelse;
            _form.erklaeringUtfoerelseSpecified = true;
            return this;
        }

        public SamsvarserklaeringDirekteBuilder ErklaeringProsjektering(bool? erklaeringProsjektering)
        {
            _form.erklaeringProsjektering = erklaeringProsjektering;
            _form.erklaeringProsjekteringSpecified = true;
            return this;
        }

        public SamsvarserklaeringDirekteBuilder Bekreftelser(bool? erklaeringKontroll = null, bool? erklaeringUtfoerelse = null)
        {
            if (erklaeringKontroll.HasValue)
            {
                _form.erklaeringProsjekteringSpecified = true;
                _form.erklaeringProsjektering = erklaeringKontroll.Value;
            }
            if (erklaeringUtfoerelse.HasValue)
            {
                _form.erklaeringUtfoerelseSpecified = true;
                _form.erklaeringUtfoerelse = erklaeringUtfoerelse.Value;
            }

            return this;
        }
        internal SamsvarserklaeringDirekteBuilder EiendomByggested(string knr = null, string gnr = null, string bnr = null, string fnr = null, string snr = null, string kommunenavn = null, string bolignummer = null, string bygningsnummer = null
            , string adresselinje1 = null, string gatenavn = null, string husnr = null, string postnr = null, string poststed = null)
        {
            var eiendomByggested = new EiendomType();
            eiendomByggested.bolignummer = bolignummer;
            eiendomByggested.kommunenavn = kommunenavn;
            eiendomByggested.eiendomsidentifikasjon = new MatrikkelnummerType()
            {
                kommunenummer = knr,
                gaardsnummer = gnr,
                bruksnummer = bnr,
                festenummer = fnr,
                seksjonsnummer = snr
            };

            eiendomByggested.bygningsnummer = bygningsnummer;
            if (!string.IsNullOrEmpty(adresselinje1) || !string.IsNullOrEmpty(gatenavn) || !string.IsNullOrEmpty(husnr))
            {
                eiendomByggested.adresse = new EiendommensAdresseType()
                {
                    adresselinje1 = adresselinje1,
                    gatenavn = gatenavn,
                    husnr = husnr,
                    postnr = postnr,
                    poststed = poststed
                };
            }

            AddEiendomToFrom(eiendomByggested);
            return this;
        }


        //
        private void AddEiendomToFrom(EiendomType eiendomByggested)
        {
            if (_form.eiendomByggested == null)
            {
                _form.eiendomByggested = new[] { eiendomByggested };
            }
            else
            {
                var eiendomTypes = _form.eiendomByggested.ToList();
                eiendomTypes.Add(eiendomByggested);
                _form.eiendomByggested = eiendomTypes.ToArray();
            }
        }

        //Parse
        internal SamsvarserklaeringDirekteBuilder EiendomByggested(SampleEiendomData.Eiendom eiendom)
        {
            EiendomByggested(eiendom.EiendomsIdentifikasjon.Kommunenummer, eiendom.EiendomsIdentifikasjon.Gaardsnummer, eiendom.EiendomsIdentifikasjon.Bruksnummer, eiendom.EiendomsIdentifikasjon.Festenummer, eiendom.EiendomsIdentifikasjon.Seksjonsnummer,
                eiendom.Kommunenavn, eiendom.Bolignummer, eiendom.Bygningsnummer, eiendom.Adresse.adresselinje1, eiendom.Adresse.gatenavn, eiendom.Adresse.husnr, eiendom.Adresse.postnr, eiendom.Adresse.poststed);
            return this;
        }

        internal SamsvarserklaeringDirekteBuilder AnsvarligSoeker(SampleAktoerData.Aktoer ansvarligAktoer)
        {
            AnsvarligSoeker(ansvarligAktoer.organisasjonsnummer, ansvarligAktoer.partsTypeKodeverdi, ansvarligAktoer.partsTypeKodebeskrivelse, ansvarligAktoer.kontaktperson?.navn, ansvarligAktoer.navn, ansvarligAktoer.Adresse.adresselinje1,
                ansvarligAktoer.Adresse.postnr, ansvarligAktoer.Adresse.landkode, ansvarligAktoer.Adresse.poststed, ansvarligAktoer.telefonnummer, ansvarligAktoer.mobilnummer, ansvarligAktoer.Epost);
            return this;
        }
        internal SamsvarserklaeringDirekteBuilder Foretak(SampleAktoerData.Aktoer foretak)
        {
            Foretak(foretak.organisasjonsnummer, foretak.partsTypeKodeverdi, foretak.partsTypeKodebeskrivelse, foretak.kontaktperson?.navn, foretak.navn, foretak.Adresse.adresselinje1,
                foretak.Adresse.postnr, foretak.Adresse.landkode, foretak.Adresse.poststed, foretak.telefonnummer, foretak.mobilnummer, foretak.Epost);
            return this;
        }

        public SamsvarserklaeringDirekteBuilder FraSluttbrukersystem(string fraSluttbrukersystem = null)
        {
            _form.fraSluttbrukersystem = fraSluttbrukersystem;
            return this;
        }
    }
}
