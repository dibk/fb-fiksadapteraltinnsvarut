﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KommIT.FIKS.AdapterAltinnSvarUt.Models;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.Builders
{
    class KvitteringForNabovarselBuilder
    {
        public KvitteringForNabovarsel Naboe1(List<Eiendom> eiendom, Guid guid)
        {
            var kvitteringForNabovarsel = new KvitteringForNabovarsel()
            {
                Hovedinnsendingsnummer = guid.ToString(),
                NaboNavn = "Tor Hammer",
                
                GjelderNaboeiendommer = new List<Eiendom>
                {
                    new Eiendom()
                    {
                        EiendomsAdress = "Storgata 1", 
                        EiendomsPostnr = "7003",
                        EiendomsPoststed = "Trondheim",
                        Gaardsnummer = "109",
                        Bruksnumme = "1",
                        Festenummer = "",
                        Seksjonsnummer = "1",
                        Kommunenavn = ""
                    }
                },
                NaboAdress = "Roald Dalhs gate 4",
                NaboPostnr = "3701",
                NaboPoststed = "Skien",
            };
            if (eiendom.Any())
            {
                kvitteringForNabovarsel.Eiendommer = eiendom.ToList();
            }

            return kvitteringForNabovarsel;
        }
        public KvitteringForNabovarsel Naboe2(List<Eiendom> eiendom, Guid guid)
        {
            var kvitteringForNabovarsel = new KvitteringForNabovarsel()
            {
                Hovedinnsendingsnummer = guid.ToString(),
                NaboNavn = "Vigdis Vater",
               
                GjelderNaboeiendommer = new List<Eiendom>
                {
                    new Eiendom()
                    {
                        EiendomsAdress = "Storgata 3",
                        EiendomsPostnr = "7003",
                        EiendomsPoststed = "Trondheim",
                        Gaardsnummer = "109",
                        Bruksnumme = "1",
                        Festenummer = "",
                        Seksjonsnummer = "1",
                        Kommunenavn = "Trondheim"
                    }

                },

                NaboAdress = "Storgata 3",
                NaboPostnr = "7003",
                NaboPoststed = "Trondheim",
            };
            if (eiendom.Any())
            {
                kvitteringForNabovarsel.Eiendommer = eiendom.ToList();
            }

            return kvitteringForNabovarsel;
        }
        public KvitteringForNabovarsel Naboe3(List<Eiendom> eiendom, Guid guid)
        {
            var kvitteringForNabovarsel = new KvitteringForNabovarsel()
            {
                Hovedinnsendingsnummer = guid.ToString(),
                NaboNavn = "Tom Tømrer",
              
                GjelderNaboeiendommer = new List<Eiendom>
                {
                    new Eiendom()
                    {
                        EiendomsAdress = "Storgata 3",
                        EiendomsPostnr = "7003",
                        EiendomsPoststed = "Trondheim",
                        Gaardsnummer = "109",
                        Bruksnumme = "1",
                        Festenummer = "",
                        Seksjonsnummer = "1",
                        Kommunenavn = ""
                    }

                },

                NaboAdress = "Storgata 3",
                NaboPostnr = "7003",
                NaboPoststed = "Trondheim",
            };
            if (eiendom.Any())
            {
                kvitteringForNabovarsel.Eiendommer = eiendom.ToList();
            }

            return kvitteringForNabovarsel;
        }
        public KvitteringForNabovarsel Naboe4(List<Eiendom> eiendom, Guid guid)
        {
            var kvitteringForNabovarsel = new KvitteringForNabovarsel()
            {
                Hovedinnsendingsnummer = guid.ToString(),
                NaboNavn = "SÆBØVÅGEN OG LONGYEARBYEN",
               
                GjelderNaboeiendommer = new List<Eiendom>
                {
                    new Eiendom()
                    {
                        EiendomsAdress = "Storgata 3",
                        EiendomsPostnr = "7003",
                        EiendomsPoststed = "Trondheim",
                        Gaardsnummer = "109",
                        Bruksnumme = "2",
                        Festenummer = "5001",
                        Seksjonsnummer = "1",
                        Kommunenavn = "Trondheim"
                    }

                },

                NaboAdress = "Foretningsgata 234",
                NaboPostnr = "9520",
                NaboPoststed = "KAUTOKEINO",
            };
            if (eiendom.Any())
            {
                kvitteringForNabovarsel.Eiendommer = eiendom.ToList();
            }

            return kvitteringForNabovarsel;
        }

        public KvitteringForNabovarsel Naboe5(List<Eiendom> eiendom, Guid guid)
        {
            var kvitteringForNabovarsel = new KvitteringForNabovarsel()
            {
                Hovedinnsendingsnummer = guid.ToString(),
                NaboNavn = "VIGGO VRANG &",

                GjelderNaboeiendommer = new List<Eiendom>
                {
                    new Eiendom()
                    {
                        EiendomsAdress = "Storgata 3",
                        EiendomsPostnr = "7003",
                        EiendomsPoststed = "Trondheim",
                        Gaardsnummer = "109",
                        Bruksnumme = "2",
                        Festenummer = "5001",
                        Seksjonsnummer = "1",
                        Kommunenavn = "Trondheim"
                    }

                },

                NaboAdress = "Foretningsgata 234",
                NaboPostnr = "9520",
                NaboPoststed = "KAUTOKEINO",
            };
            if (eiendom.Any())
            {
                kvitteringForNabovarsel.Eiendommer = eiendom.ToList();
            }

            return kvitteringForNabovarsel;
        }

        public Eiendom Endommer1()
        {
            return new Eiendom()
            {
                Gaardsnummer = "842",
                Bruksnumme = "162",
                Festenummer = "1",
                Seksjonsnummer = "1",
                Kommunenavn = "Bø i Telemark",
                EiendomsAdress = "Storgata 19",
                EiendomsPostnr = "3800",
                EiendomsPoststed = "Bø i Telemark",
            };
        }
        public Eiendom Eiendommer2()
        {
            return new Eiendom()
            {
                Gaardsnummer = "842",
                Bruksnumme = "162",
                Festenummer = "1",
                Seksjonsnummer = "1",
                Kommunenavn = "Trondheim",
                EiendomsAdress = "Storgata 19",
                EiendomsPostnr = "7003",
                EiendomsPoststed = "Trondheim",
            };
        }

        public List<KvitteringForNabovarsel> Kvitterings(Guid[] naboGuids)
        {
            var EndommerList= new List<Eiendom>(){ Eiendommer2(), Endommer1()};
            var kvitteringList = new List<KvitteringForNabovarsel>(){Naboe1(EndommerList, naboGuids[0]),Naboe2(EndommerList, naboGuids[1]),Naboe3(EndommerList, naboGuids[2]),Naboe4(EndommerList, naboGuids[3]) };
            return kvitteringList;
        }

        public List<KvitteringForNabovarsel> NabovarselKvitteringer(Guid[] naboGuids)
        {
            var EndommerList = new List<Eiendom>() { Eiendommer2(), Endommer1() };
            var kvitteringList = new List<KvitteringForNabovarsel>() { Naboe1(EndommerList, naboGuids[0]), Naboe2(EndommerList, naboGuids[1]), Naboe3(EndommerList, naboGuids[2]), Naboe4(EndommerList, naboGuids[3]), Naboe4(EndommerList, naboGuids[4]) };
            return kvitteringList;
        }

    }
}
