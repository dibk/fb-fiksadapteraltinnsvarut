﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using no.kxml.skjema.dibk.matrikkelregistrering;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.Builders
{
    class MatrikkelopplysningerV1Builder
    {
        private readonly MatrikkelregistreringType _form;

        public MatrikkelopplysningerV1Builder()
        {
            _form = new MatrikkelregistreringType();
        }

        public MatrikkelregistreringType Build()
        {
            return _form;
        }

        // eiendom
        public MatrikkelopplysningerV1Builder EiendomType(string knr, string gnr, string bnr, string fnr, string snr, bool addNewEiendom = true)
        {
            if (!addNewEiendom) return this;
            _form.eiendomByggested = new no.kxml.skjema.dibk.matrikkelregistrering.EiendomType[]
            {
                               new EiendomType
                    {
                                   eiendomsidentifikasjon = new MatrikkelnummerType(){
                        kommunenummer = knr,
                        gaardsnummer = gnr,
                        bruksnummer = bnr,
                        festenummer = fnr,
                        seksjonsnummer = snr
                    }
                }

            };

            return this;
        }

        // adresse
        public void AdresseType(string postnr, string gatenavn, string husnr, string bokstav)
        {
            //TODO hva med matrikkeladresser?
            if (_form.eiendomByggested.First().adresse == null)
            {

                _form.eiendomByggested.First().adresse = new EiendommensAdresseType
                {       
                        postnr = postnr,
                        gatenavn = gatenavn,
                        husnr = husnr,
                        bokstav = bokstav

                };
            }
        }
        public MatrikkelopplysningerV1Builder Bygning(string bygningstypeKodeverdi, string naeringsgruppeKodeverdi = "X", double? bebygdAreal = 1020, string vannforsyningkodeverdi = "AnnenPrivatInnlagt", string avlopkodeverdi = "PrivatKloakk", bool? harheis = true)
        {
            _form.bygning = new[] { new BygningType() };

            if (!string.IsNullOrEmpty(bygningstypeKodeverdi))
            {
                _form.bygning[0].bygningstype = new KodeType()
                {
                    kodeverdi = bygningstypeKodeverdi,
                    kodebeskrivelse = "161"
                };
            }
            if (!string.IsNullOrEmpty(naeringsgruppeKodeverdi))
            {
                _form.bygning[0].naeringsgruppe = new KodeType()
                {
                    kodeverdi = naeringsgruppeKodeverdi,
                    kodebeskrivelse = "Næringsgruppe for bolig"
                };
            }
            if (bebygdAreal != null)
            {
                _form.bygning[0].bebygdArealSpecified = true;
                _form.bygning[0].bebygdAreal = bebygdAreal;
            }

            if (!string.IsNullOrEmpty(vannforsyningkodeverdi))
            {
                _form.bygning[0].vannforsyning = new KodeType()
                {
                    kodeverdi = vannforsyningkodeverdi,
                    kodebeskrivelse = "Annen privat vannforsyning, innlagt vann"
                };
            }
            if (!string.IsNullOrEmpty(avlopkodeverdi))
            {
                _form.bygning[0].avlop = new KodeType()
                {
                    kodeverdi = avlopkodeverdi,
                    kodebeskrivelse = "Privat avløpsanlegg"
                };
            }

            if (harheis.HasValue)
            {
                _form.bygning[0].harHeisSpecified = true;
                _form.bygning[0].harHeis = harheis.Value;
            }
            return this;
        }

        public MatrikkelopplysningerV1Builder BygningEnergiforsyning(string energiforsyningkodeverdi = "elektrisitet", string varmefordelingkodeverdi = "ovnKaminPeis")
        {
            _form.bygning[0].energiforsyning = new EnergiforsyningType();
            if (!string.IsNullOrEmpty(energiforsyningkodeverdi))
            {
                _form.bygning[0].energiforsyning.energiforsyning = new[]
                {
                    new KodeType()
                    {
                        kodeverdi = energiforsyningkodeverdi,
                        kodebeskrivelse = "Elektrisitet"
                    }
                };
            }
            if (!string.IsNullOrEmpty(varmefordelingkodeverdi))
            {
                _form.bygning[0].energiforsyning.varmefordeling = new[]
                {
                    new KodeType()
                    {
                        kodeverdi = varmefordelingkodeverdi,
                        kodebeskrivelse = "Ovn/kamin/peis"
                    }
                };
            }
            return this;
        }
        public MatrikkelopplysningerV1Builder BygningBruksenheter(string bruksenhetsnummerEtasjeplan = "H")
        {
            _form.bygning[0].bruksenheter = new[] { new BruksenhetType() };

            if (!string.IsNullOrEmpty(bruksenhetsnummerEtasjeplan))
            {
                _form.bygning[0].bruksenheter[0].bruksenhetsnummer = new BruksenhetsnummerType()
                {
                    etasjeplan = new KodeType()
                    {
                        kodeverdi = bruksenhetsnummerEtasjeplan,
                        kodebeskrivelse = "H"
                    }
                };
            }


            return this;
        }
        public MatrikkelopplysningerV1Builder BygningEtasjer(string etasjeplanKodeverdi)
        {
            _form.bygning[0].etasjer = new[] { new EtasjeType() };
            if (!string.IsNullOrEmpty(etasjeplanKodeverdi))
            {
                _form.bygning[0].etasjer[0].etasjeplan = new KodeType()
                {
                    kodeverdi = etasjeplanKodeverdi,
                    kodebeskrivelse = ""
                };
            }
            return this;
        }

    }
}
