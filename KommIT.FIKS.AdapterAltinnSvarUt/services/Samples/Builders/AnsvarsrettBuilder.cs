﻿using no.kxml.skjema.dibk.ansvarsrettDirekteOpprettet;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.Builders
{
    public class AnsvarsrettBuilder
    {
        private readonly ErklaeringAnsvarsrettType _form = new ErklaeringAnsvarsrettType();

        public AnsvarsrettBuilder()
        {
            _form.ansvarsrett = new AnsvarsrettType()
            {
            };
        }
        public ErklaeringAnsvarsrettType Build()
        {
            return _form;
        }

       public AnsvarsrettBuilder AnsvarsrettAndAnsvarsomraader()
        {
            _form.ansvarsrett.ansvarsomraader = new[]
            {
                new AnsvarsomraadeType()
                {
                    funksjon = new KodeType()
                    {
                        kodeverdi = "KONTROLL",
                        kodebeskrivelse = "Ansvarlig kontroll"
                    },
                    beskrivelseAvAnsvarsomraade = "Overordnet ansvar for kontroll",
                    tiltaksklasse = new KodeType()
                    {
                        kodeverdi = "2",
                        kodebeskrivelse = "Tiltaksklasse 2"
                    },
                    samsvarKontrollVedRammetillatelse = false,
                    samsvarKontrollVedIgangsettingstillatelse = true,
                    samsvarKontrollVedMidlertidigBrukstillatelse = true,
                    samsvarKontrollVedFerdigattest = true,
                    vaarReferanse = System.Guid.NewGuid().ToString()
                }
            };

            return this;
        }

        public AnsvarsrettBuilder EiendomByggested()
        {
            _form.eiendomByggested = new[]
            {
                new EiendomType()
                {
                    eiendomsidentifikasjon = new MatrikkelnummerType()
                    {
                        kommunenummer = "123",
                        gaardsnummer = "123",
                        bruksnummer = "123"
                    }
                }
            };

            return this;
        }
        
        public AnsvarsrettBuilder Foretak(string orgNr)
        {
            _form.ansvarsrett.foretak = new ForetakType()
            {
                kontaktperson = new KontaktpersonType()
                {
                    navn = "Jens Jensen",
                    telefonnummer = "99994444",
                    mobilnummer = "44449999",
                    epost = "jens@kontaktperson.no"
                },
                harSentralGodkjenning = true,
                partstype = new KodeType()
                {
                    kodeverdi = "Foretak",
                    kodebeskrivelse = "Foretak"
                },
                organisasjonsnummer = orgNr,
                navn = "Nordmann Bygg og Anlegg AS",
                adresse = new EnkelAdresseType
                {
                    adresselinje1 = "Bøgata 16",
                    postnr = "3802",
                    poststed = "Bø i Telemark",
                    landkode = "NO"
                },
                telefonnummer = "12345678",
                mobilnummer = "98765432",
                epost = "ola@byggmestern-ola.no",
                signaturdato = new System.DateTime(2016, 12, 05),
                signaturdatoSpecified = true,
            };

            return this;
        }

        public AnsvarsrettBuilder ToAnsvarligeSoekere()
        {
            _form.ansvarsrett.ansvarsomraader = new[]
            {
                new AnsvarsomraadeType()
                {
                    funksjon = new KodeType()
                    {
                        kodeverdi = "SØK",
                        kodebeskrivelse = "Ansvarlig søker"
                    },
                    beskrivelseAvAnsvarsomraade = "Andre",
                    tiltaksklasse = new KodeType()
                    {
                        kodeverdi = "2",
                        kodebeskrivelse = "Tiltaksklasse 2"
                    },
                    samsvarKontrollVedRammetillatelse = false,
                    samsvarKontrollVedIgangsettingstillatelse = false,
                    samsvarKontrollVedMidlertidigBrukstillatelse = false,
                    samsvarKontrollVedFerdigattest = false
                },
                new AnsvarsomraadeType()
                {
                    funksjon = new KodeType()
                    {
                        kodeverdi = "SØK",
                        kodebeskrivelse = "Ansvarlig søker"
                    },
                    beskrivelseAvAnsvarsomraade = "Andre",
                    tiltaksklasse = new KodeType()
                    {
                        kodeverdi = "2",
                        kodebeskrivelse = "Tiltaksklasse 2"
                    },
                    samsvarKontrollVedRammetillatelse = false,
                    samsvarKontrollVedIgangsettingstillatelse = false,
                    samsvarKontrollVedMidlertidigBrukstillatelse = false,
                    samsvarKontrollVedFerdigattest = false
                }
            };

            return this;
        }


        public AnsvarsrettBuilder TiltaksklasseMedGodkjenteKodeverdier()
        {
            _form.ansvarsrett.ansvarsomraader = new[]
            {
                new AnsvarsomraadeType()
                {
                    funksjon = new KodeType()
                    {
                        kodeverdi = "KONTROLL",
                        kodebeskrivelse = "Ansvarlig kontroll"
                    },
                    beskrivelseAvAnsvarsomraade = "Overordnet ansvar for kontroll",
                    tiltaksklasse = new KodeType()
                    {
                        kodeverdi = "1",
                        kodebeskrivelse = "Tiltaksklasse 1"
                    },
                    samsvarKontrollVedRammetillatelse = false,
                    samsvarKontrollVedIgangsettingstillatelse = true,
                    samsvarKontrollVedMidlertidigBrukstillatelse = true,
                    samsvarKontrollVedFerdigattest = true,
                },
                new AnsvarsomraadeType()
                {
                    funksjon = new KodeType()
                    {
                        kodeverdi = "KONTROLL",
                        kodebeskrivelse = "Ansvarlig kontroll"
                    },
                    beskrivelseAvAnsvarsomraade = "Overordnet ansvar for kontroll",
                    tiltaksklasse = new KodeType()
                    {
                        kodeverdi = "2",
                        kodebeskrivelse = "Tiltaksklasse 2"
                    },
                    samsvarKontrollVedRammetillatelse = false,
                    samsvarKontrollVedIgangsettingstillatelse = true,
                    samsvarKontrollVedMidlertidigBrukstillatelse = true,
                    samsvarKontrollVedFerdigattest = true,
                },
                new AnsvarsomraadeType()
                {
                    funksjon = new KodeType()
                    {
                        kodeverdi = "KONTROLL",
                        kodebeskrivelse = "Ansvarlig kontroll"
                    },
                    beskrivelseAvAnsvarsomraade = "Overordnet ansvar for kontroll",
                    tiltaksklasse = new KodeType()
                    {
                        kodeverdi = "3",
                        kodebeskrivelse = "Tiltaksklasse 3"
                    },
                    samsvarKontrollVedRammetillatelse = false,
                    samsvarKontrollVedIgangsettingstillatelse = true,
                    samsvarKontrollVedMidlertidigBrukstillatelse = true,
                    samsvarKontrollVedFerdigattest = true,
                }
            };

            return this;
        }

        public AnsvarsrettBuilder TiltaksklasseMedIkkeGodkjenteKodeverdier()
        {
            _form.ansvarsrett.ansvarsomraader = new[]
            {
                new AnsvarsomraadeType()
                {
                    funksjon = new KodeType()
                    {
                        kodeverdi = "KONTROLL",
                        kodebeskrivelse = "Ansvarlig kontroll"
                    },
                    beskrivelseAvAnsvarsomraade = "Overordnet ansvar for kontroll",
                    tiltaksklasse = new KodeType()
                    {
                        kodeverdi = "a",
                        kodebeskrivelse = "Tiltaksklasse a"
                    },
                    samsvarKontrollVedRammetillatelse = false,
                    samsvarKontrollVedIgangsettingstillatelse = true,
                    samsvarKontrollVedMidlertidigBrukstillatelse = true,
                    samsvarKontrollVedFerdigattest = true,
                }
            };

            return this;
        }
    }
}