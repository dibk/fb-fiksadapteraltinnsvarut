﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.SampleData;
using no.kxml.skjema.dibk.arbeidstilsynetsSamtykke;
//using no.kxml.skjema.dibk.rammesoknadV2;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.Builders
{
    public class ArbeidstilsynetsSamtykkeBuilder
    {
        private readonly ArbeidstilsynetsSamtykkeType _form;

        public ArbeidstilsynetsSamtykkeBuilder()
        {
            _form = new ArbeidstilsynetsSamtykkeType();
        }

        public ArbeidstilsynetsSamtykkeType Build()
        {
            return _form;
        }

        public ArbeidstilsynetsSamtykkeBuilder ProsjektInfo(string prosjektnavn = null, string hovedinnsendingsnummer = null, string arbeidstilsynetsSaksnummer = null, string arbeidstilsynetsSaksaar = null, string kommunensSaksnummer = null, string kommunensSaksaar = null)
        {

            _form.prosjektnavn = prosjektnavn;
            if (!string.IsNullOrEmpty(kommunensSaksnummer) || !string.IsNullOrEmpty(kommunensSaksaar))
            {
                _form.kommunensSaksnummer = new SaksnummerType()
                {
                    sakssekvensnummer = kommunensSaksnummer,
                    saksaar = kommunensSaksaar
                };
            }

            if (!string.IsNullOrEmpty(arbeidstilsynetsSaksnummer) || !string.IsNullOrEmpty(arbeidstilsynetsSaksaar))
            {
                //_form.arbeidstilsynetsSaksnummer = arbeidstilsynetsSaksnummer; 
                _form.arbeidstilsynetsSaksnummer = new SaksnummerType()
                {
                    sakssekvensnummer = arbeidstilsynetsSaksnummer,
                    saksaar = arbeidstilsynetsSaksaar
                };
            }

            _form.hovedinnsendingsnummer = hovedinnsendingsnummer;

            return this;
        }

        public ArbeidstilsynetsSamtykkeBuilder FraSluttbrukersystemErUtfylt(string fraSluttbrukersystem)
        {
            _form.fraSluttbrukersystem = fraSluttbrukersystem;
            return this;
        }

        internal ArbeidstilsynetsSamtykkeBuilder EiendomByggested(string knr = null, string gnr = null, string bnr = null, string fnr = null, string snr = null, string kommunenavn = null, string bolignummer = null, string bygningsnummer = null
            , string adresselinje1 = null, string gatenavn = null, string husnr = null, string postnr = null, string poststed = null)
        {
            var eiendomByggested = new EiendomType();
            eiendomByggested.bolignummer = bolignummer;
            eiendomByggested.bygningsnummer = bygningsnummer;
            eiendomByggested.kommunenavn = kommunenavn;

            //eiendomByggested.kommunenavn = kommunenavn;
            eiendomByggested.eiendomsidentifikasjon = new MatrikkelnummerType()
            {
                kommunenummer = knr,
                gaardsnummer = gnr,
                bruksnummer = bnr,
                festenummer = fnr,
                seksjonsnummer = snr
            };

            if (!string.IsNullOrEmpty(adresselinje1) || !string.IsNullOrEmpty(gatenavn) || !string.IsNullOrEmpty(husnr))
            {
                eiendomByggested.adresse = new EiendommensAdresseType()
                {
                    adresselinje1 = adresselinje1,
                    gatenavn = gatenavn,
                    husnr = husnr,
                    postnr = postnr,
                    poststed = poststed
                };
            }

            List<EiendomType> eiendomTypes;
            if (_form.eiendomByggested == null)
            {
                eiendomTypes = new List<EiendomType>() {eiendomByggested};
            }
            else
            {
                eiendomTypes = _form.eiendomByggested.ToList();
                eiendomTypes.Add(eiendomByggested);
            }

            _form.eiendomByggested = eiendomTypes.ToArray();

            return this;
        }

        public ArbeidstilsynetsSamtykkeBuilder Tiltakshaver(string organisasjonsnummer = null, string foedselsnummer = null, string partsTypeKodeverdi = null, string partsTypeKodebeskrivelse = null, string navn = null, string adressLine = null, string postnr = null, string landkode = null
            , string poststed = null, string telefonnummer = null, string mobilnummer = null, string epost = null, string kontaktpersonNavn = null, string KontaktPersonEpost = null, string kontaktPersonMobilnummer = null)
        {
            _form.tiltakshaver = new PartType();

            if (!string.IsNullOrEmpty(partsTypeKodeverdi))
            {
                _form.tiltakshaver.partstype = new KodeType
                {
                    kodeverdi = partsTypeKodeverdi,
                    kodebeskrivelse = ""
                };
            }

            _form.tiltakshaver.navn = navn;

            if (!string.IsNullOrEmpty(adressLine) || !string.IsNullOrEmpty(postnr) || !string.IsNullOrEmpty(landkode) || !string.IsNullOrEmpty(poststed))
            {
                _form.tiltakshaver.adresse = new EnkelAdresseType()
                {
                    adresselinje1 = adressLine,
                    postnr = postnr,
                    landkode = landkode,
                    poststed = poststed
                };
            }

            _form.tiltakshaver.organisasjonsnummer = organisasjonsnummer; //"974760673" - 974760673 BRREG, Altinn brukerservice
            if (!string.IsNullOrEmpty(kontaktpersonNavn) || !string.IsNullOrEmpty(KontaktPersonEpost) || !string.IsNullOrEmpty(kontaktPersonMobilnummer))
            {
                _form.tiltakshaver.kontaktperson = new KontaktpersonType()
                {
                    navn = kontaktpersonNavn,
                    epost = KontaktPersonEpost,
                    mobilnummer = kontaktPersonMobilnummer
                };
            }
            //_form.tiltakshaver.kontaktperson = kontaktperson;
            if (!string.IsNullOrEmpty(foedselsnummer))
            {
                _form.tiltakshaver.foedselsnummer = foedselsnummer;
            }
            _form.tiltakshaver.telefonnummer = telefonnummer;
            _form.tiltakshaver.epost = epost;

            return this;
        }

        public ArbeidstilsynetsSamtykkeBuilder BeskrivelseAvTilta(string bygningstypeKodeverdi = null, string bygningstypeKodebeskrivelse = null)
        {
            var tiltakType = new TiltakType();

            if (!string.IsNullOrEmpty(bygningstypeKodeverdi) || !string.IsNullOrEmpty(bygningstypeKodebeskrivelse))
            {
                tiltakType.bruk = new FormaalType()
                {
                    bygningstype = new KodeType()
                    {
                        kodeverdi = bygningstypeKodeverdi,
                        kodebeskrivelse = bygningstypeKodebeskrivelse
                    }
                };

            }
            AddTiltak(tiltakType);
            return this;
        }

        public ArbeidstilsynetsSamtykkeBuilder Arbeidsplasser(bool? eksisterende = null, bool? framtidige = null, bool? faste = null, bool? midlertidige = null,
            bool? utleieBygg = null, string antallVirksomheter = null, string antallAnsatte = null, string beskrivelse = null)
        {
            if (eksisterende.HasValue || framtidige.HasValue)
            {
                _form.arbeidsplasser = new ArbeidsplasserType();
                if (eksisterende.HasValue)
                {
                    _form.arbeidsplasser.eksisterendeSpecified = true;
                    _form.arbeidsplasser.eksisterende = eksisterende;
                }

                if (framtidige.HasValue)
                {
                    _form.arbeidsplasser.framtidigeSpecified = true;
                    _form.arbeidsplasser.framtidige = framtidige;
                }
                if (faste.HasValue)
                {
                    _form.arbeidsplasser.fasteSpecified = true;
                    _form.arbeidsplasser.faste = faste;
                }
                if (midlertidige.HasValue)
                {
                    _form.arbeidsplasser.midlertidigeSpecified = true;
                    _form.arbeidsplasser.midlertidige = midlertidige;
                }
                if (utleieBygg.HasValue)
                {
                    _form.arbeidsplasser.utleieByggSpecified = true;
                    _form.arbeidsplasser.utleieBygg = utleieBygg;
                }

                _form.arbeidsplasser.antallVirksomheter = antallVirksomheter;
                _form.arbeidsplasser.antallAnsatte = antallAnsatte;
                _form.arbeidsplasser.beskrivelse = beskrivelse;
            }

            return this;
        }

        public ArbeidstilsynetsSamtykkeBuilder Fakturamottaker(bool? fakturaPapir = null, bool? ehfFaktura = null, string organisasjonsnummer = null, string bestillerReferanse = null, string fakturareferanser = null, string navn = null, string prosjektnummer = null, string adresselinje1 = null, string postnr = null, string poststed = null, string landKode = null)
        {
            _form.fakturamottaker = new FakturamottakerType()
            {
                organisasjonsnummer = organisasjonsnummer,
                bestillerReferanse = bestillerReferanse,
                fakturareferanser = fakturareferanser,
                navn = navn,
                prosjektnummer = prosjektnummer,
            };
            if (!string.IsNullOrEmpty(adresselinje1) || !string.IsNullOrEmpty(postnr) || !string.IsNullOrEmpty(poststed) || !string.IsNullOrEmpty(landKode))
            {
                _form.fakturamottaker.adresse = new EnkelAdresseType()
                {
                    adresselinje1 = adresselinje1,
                    postnr = postnr,
                    poststed = poststed,
                    landkode = landKode,
                };
            }

            if (fakturaPapir.HasValue)
            {
                _form.fakturamottaker.fakturaPapirSpecified = true;
                _form.fakturamottaker.fakturaPapir = fakturaPapir.Value;
            }

            if (ehfFaktura.HasValue)
            {
                _form.fakturamottaker.ehfFakturaSpecified = true;
                _form.fakturamottaker.ehfFaktura = ehfFaktura.Value;
            }

            return this;
        }

        //Sumethods
        private void AddTiltak(TiltakType tiltakType)
        {
            if (_form.beskrivelseAvTiltak == null)
            {
                _form.beskrivelseAvTiltak = new[] { tiltakType };
            }
            else
            {
                var tiltakTypes = _form.beskrivelseAvTiltak.ToList();
                tiltakTypes.Add(tiltakType);
                _form.beskrivelseAvTiltak = tiltakTypes.ToArray();
            }
        }
        //


        internal ArbeidstilsynetsSamtykkeBuilder Tiltakshaver(SampleAktoerData.Aktoer foretak)
        {
            Tiltakshaver(foretak.organisasjonsnummer, foretak.foedselsnummer, foretak.partsTypeKodeverdi, foretak.partsTypeKodebeskrivelse, foretak.navn, foretak.Adresse.adresselinje1,
                foretak.Adresse.postnr, foretak.Adresse.landkode, foretak.Adresse.poststed, foretak.telefonnummer, foretak.mobilnummer, foretak.Epost, foretak.kontaktperson?.navn, foretak.kontaktperson?.Epost, foretak.kontaktperson?.mobilnummer);
            return this;
        }
        internal ArbeidstilsynetsSamtykkeBuilder EiendomByggested(SampleEiendomData.Eiendom eiendom)
        {
            EiendomByggested(eiendom.EiendomsIdentifikasjon.Kommunenummer, eiendom.EiendomsIdentifikasjon.Gaardsnummer, eiendom.EiendomsIdentifikasjon.Bruksnummer, eiendom.EiendomsIdentifikasjon.Festenummer, eiendom.EiendomsIdentifikasjon.Seksjonsnummer,
                eiendom.Kommunenavn, eiendom.Bolignummer, eiendom.Bygningsnummer, eiendom.Adresse.adresselinje1, eiendom.Adresse.gatenavn, eiendom.Adresse.husnr, eiendom.Adresse.postnr, eiendom.Adresse.poststed);
            return this;
        }



    }
}