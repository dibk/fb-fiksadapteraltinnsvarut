﻿using System;
using System.Collections.Generic;
using System.Linq;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.SampleData;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;
using no.kxml.skjema.dibk.suppleringAvSoknad;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.Builders
{
    public class SuppleringAvSoeknadBuilder
    {
        private readonly SuppleringAvSoeknadType _form;

        public SuppleringAvSoeknadBuilder()
        {
            _form = new SuppleringAvSoeknadType();
        }

        public SuppleringAvSoeknadType Build()
        {
            return _form;
        }

        public SuppleringAvSoeknadBuilder Metadata(string fraSluttbrukersystem = null, string ftbId = null, string prosjektnavn = null, string sluttbrukersystemUrl = null, bool? klartForSigneringFraSluttbrukersystem = null)
        {
            _form.metadata = new MetadataType();
            _form.metadata.fraSluttbrukersystem = fraSluttbrukersystem;
            _form.metadata.ftbId = ftbId;
            _form.metadata.prosjektnavn = prosjektnavn;
            _form.metadata.sluttbrukersystemUrl = sluttbrukersystemUrl;

            if (klartForSigneringFraSluttbrukersystem.HasValue)
            {
                _form.metadata.klartForSigneringFraSluttbrukersystem = klartForSigneringFraSluttbrukersystem;
                _form.metadata.klartForSigneringFraSluttbrukersystemSpecified = true;
            }

            return this;
        }

        public SuppleringAvSoeknadBuilder EiendomByggested(string knr = null, string gnr = null, string bnr = null, string fnr = null, string snr = null, string kommunenavn = null, string bolignummer = null, string bygningsnummer = null
            , string adresselinje1 = null, string gatenavn = null, string husnr = null, string postnr = null, string poststed = null)
        {
            var eiendomByggested = new EiendomType();
            eiendomByggested.bolignummer = bolignummer;
            eiendomByggested.kommunenavn = kommunenavn;
            eiendomByggested.eiendomsidentifikasjon = new MatrikkelnummerType()
            {
                kommunenummer = knr,
                gaardsnummer = gnr,
                bruksnummer = bnr,
                festenummer = fnr,
                seksjonsnummer = snr
            };

            eiendomByggested.bygningsnummer = bygningsnummer;
            if (!string.IsNullOrEmpty(adresselinje1) || !string.IsNullOrEmpty(gatenavn) || !string.IsNullOrEmpty(husnr))
            {
                eiendomByggested.adresse = new EiendommensAdresseType()
                {
                    adresselinje1 = adresselinje1,
                    gatenavn = gatenavn,
                    husnr = husnr,
                    postnr = postnr,
                    poststed = poststed
                };
            }

            var eiendomTypes = _form.eiendomByggested?.ToList() ?? new List<EiendomType>();
            eiendomTypes.Add(eiendomByggested);

            _form.eiendomByggested = eiendomTypes.ToArray();

            return this;
        }

        // AnsvarligSoeker
        public SuppleringAvSoeknadBuilder AnsvarligSoeker(string organisasjonsnummer = null, string foedselsnummer = null, string partsTypeKodeverdi = null, string partsTypeKodebeskrivelse = null, string navn = null, string adressLine = null, string postnr = null, string landkode = null
            , string poststed = null, string telefonnummer = null, string mobilnummer = null, string epost = null, string kontaktpersonNav = null, string kontaktpersonTel = null, string kontaktpersonEpost = null)
        {
            _form.ansvarligSoeker = new PartType();

            if (!string.IsNullOrEmpty(partsTypeKodeverdi))
            {
                _form.ansvarligSoeker.partstype = new KodeType
                {
                    kodeverdi = partsTypeKodeverdi,
                    kodebeskrivelse = partsTypeKodebeskrivelse
                };
            }
            _form.ansvarligSoeker.foedselsnummer = foedselsnummer;
            _form.ansvarligSoeker.organisasjonsnummer = organisasjonsnummer;
            _form.ansvarligSoeker.navn = navn;

            if (!string.IsNullOrEmpty(adressLine) || !string.IsNullOrEmpty(postnr) || !string.IsNullOrEmpty(landkode) || !string.IsNullOrEmpty(poststed))
            {
                _form.ansvarligSoeker.adresse = new EnkelAdresseType
                {
                    adresselinje1 = adressLine,
                    postnr = postnr,
                    landkode = landkode,
                    poststed = poststed
                };
            }
            _form.ansvarligSoeker.telefonnummer = telefonnummer;
            _form.ansvarligSoeker.mobilnummer = mobilnummer;
            _form.ansvarligSoeker.epost = epost;

            if (!string.IsNullOrEmpty(kontaktpersonNav) || !string.IsNullOrEmpty(kontaktpersonTel) || !string.IsNullOrEmpty(kontaktpersonEpost))
            {
                _form.ansvarligSoeker.kontaktperson = new KontaktpersonType()
                {
                    navn = kontaktpersonNav,
                    telefonnummer = kontaktpersonTel,
                    epost = kontaktpersonEpost
                };

            }
            return this;
        }
        // Tiltakshaver
        public SuppleringAvSoeknadBuilder Tiltakshaver(string organisasjonsnummer = null, string foedselsnummer = null, string partsTypeKodeverdi = null, string partsTypekodebeskrivelse = null, string navn = null, string adressLine = null, string postnr = null, string landkode = null
            , string poststed = null, string telefonnummer = null, string mobilnummer = null, string epost = null, string kontaktpersonNav = null, string kontaktpersonTel = null, string kontaktpersonEpost = null, string kontaktpersonMob = null)
        {
            _form.tiltakshaver = new PartType();

            if (!string.IsNullOrEmpty(partsTypeKodeverdi) || !string.IsNullOrEmpty(partsTypekodebeskrivelse))
            {
                _form.tiltakshaver.partstype = new KodeType
                {
                    kodeverdi = partsTypeKodeverdi,
                    kodebeskrivelse = partsTypekodebeskrivelse
                };
            }
            _form.tiltakshaver.foedselsnummer = foedselsnummer;
            _form.tiltakshaver.organisasjonsnummer = organisasjonsnummer;
            _form.tiltakshaver.navn = navn;
            if (!string.IsNullOrEmpty(adressLine) || !string.IsNullOrEmpty(postnr) || !string.IsNullOrEmpty(landkode) || !string.IsNullOrEmpty(poststed))
            {
                _form.tiltakshaver.adresse = new EnkelAdresseType
                {
                    adresselinje1 = adressLine,
                    postnr = postnr,
                    poststed = poststed,
                    landkode = landkode
                };
            }
            _form.tiltakshaver.telefonnummer = telefonnummer;
            _form.tiltakshaver.mobilnummer = mobilnummer;
            _form.tiltakshaver.epost = epost;

            if (!string.IsNullOrEmpty(kontaktpersonNav) || !string.IsNullOrEmpty(kontaktpersonTel) || !string.IsNullOrEmpty(kontaktpersonMob) || !string.IsNullOrEmpty(kontaktpersonEpost))
            {
                _form.tiltakshaver.kontaktperson = new KontaktpersonType()
                {
                    navn = kontaktpersonNav,
                    telefonnummer = kontaktpersonTel,
                    mobilnummer = kontaktpersonMob,
                    epost = kontaktpersonEpost
                };

            }

            return this;
        }
        public SuppleringAvSoeknadBuilder kommunensSaksnummer(string sakssekvensnummer = null, string saksaar = null)
        {
            _form.kommunensSaksnummer = new SaksnummerType()
            {
                sakssekvensnummer = sakssekvensnummer,
                saksaar = saksaar
            };
            return this;
        }

        public SuppleringAvSoeknadBuilder Ettersending(string tittel = null, string temaKodeverdi = null, string temaKodebeskrivelse = null,
            string vedleggFilnavn = null, string vedleggKodeverdi = null, string vedleggKodebeskrivelse = null, string vedleggVersjonsnummer = null, DateTime? vedleggVersjonsdato = null, string underskjemaKodeverdi = null, string underskjemaKodebeskrivelse = null)
        {
            EttersendingType ettersending = new EttersendingType()
            {
                tittel = tittel
            };

            if (!string.IsNullOrEmpty(temaKodeverdi) || !string.IsNullOrEmpty(temaKodebeskrivelse))
            {
                ettersending.tema = new KodeType()
                {
                    kodeverdi = temaKodeverdi,
                    kodebeskrivelse = temaKodebeskrivelse
                };
            }

            VedleggType vedlegg = null;
            if (!string.IsNullOrEmpty(vedleggFilnavn) || !string.IsNullOrEmpty(vedleggKodeverdi) || !string.IsNullOrEmpty(vedleggKodebeskrivelse) || !string.IsNullOrEmpty(vedleggKodebeskrivelse) || vedleggVersjonsdato.HasValue)
            {
                vedlegg = new VedleggType()
                {
                    filnavn = vedleggFilnavn,
                    versjonsnummer = vedleggVersjonsnummer,
                };

                if (vedleggVersjonsdato.HasValue)
                {
                    vedlegg.versjonsdato = vedleggVersjonsdato.Value;
                    vedlegg.versjonsdatoSpecified = true;
                }

                if (!string.IsNullOrEmpty(vedleggKodeverdi) || !string.IsNullOrEmpty(vedleggKodebeskrivelse))
                {
                    vedlegg.vedleggstype = new KodeType()
                    {
                        kodeverdi = vedleggKodeverdi,
                        kodebeskrivelse = vedleggKodebeskrivelse
                    };
                }
            }


            KodeType underskjema = null;
            if (!string.IsNullOrEmpty(underskjemaKodeverdi) || !string.IsNullOrEmpty(underskjemaKodebeskrivelse))
            {
                underskjema = new KodeType()
                {
                    kodeverdi = underskjemaKodeverdi,
                    kodebeskrivelse = underskjemaKodebeskrivelse
                };
            }

            List<EttersendingType> ettersendingtypes = _form.ettersending?.ToList() ?? new List<EttersendingType>();

            if (ettersendingtypes.Any() && Helpers.ObjectIsNullOrEmpty(ettersending))
            {
                if (vedlegg != null)
                {
                    var vedleggTypes = ettersendingtypes?.LastOrDefault()?.vedlegg?.ToList() ?? new List<VedleggType>();
                    vedleggTypes.Add(vedlegg);
                    ettersendingtypes.LastOrDefault().vedlegg = vedleggTypes.ToArray();

                }
                if (underskjema != null)
                {
                    var underskjemaTypes = ettersendingtypes?.LastOrDefault()?.underskjema?.ToList() ?? new List<KodeType>();
                    underskjemaTypes.Add(underskjema);
                    ettersendingtypes.LastOrDefault().underskjema = underskjemaTypes.ToArray();
                }
            }
            else
            {
                if (vedlegg != null)
                    ettersending.vedlegg = new[] { vedlegg };

                if (underskjema != null)
                    ettersending.underskjema = new[] { underskjema };

                ettersendingtypes.Add(ettersending);
            }

            _form.ettersending = ettersendingtypes.ToArray();
            return this;
        }
        //Mangel

        public SuppleringAvSoeknadBuilder Mangelbesvarelse(bool? erMangelBesvaresSenere = null, string tittel = null, string mangelId = null, string mangeltekstKodeverdi = null, string mangeltekstKodebeskrivelse = null, string kommentar = null,
            string vedleggFilnavn = null, string vedleggKodeverdi = null, string vedleggKodebeskrivelse = null, string vedleggVersjonsnummer = null, DateTime? vedleggVersjonsdato = null, string underskjemaKodeverdi = null, string underskjemaKodebeskrivelse = null, string temaKodeverdi = null, string temaKodebeskrivelse = null)
        {
            MangelbesvarelseType mangelbesvarelse = new MangelbesvarelseType()
            {
                tittel = tittel,
                kommentar = kommentar,
            };

            if (!string.IsNullOrEmpty(temaKodeverdi) || !string.IsNullOrEmpty(temaKodebeskrivelse))
            {
                mangelbesvarelse.tema = new KodeType()
                {
                    kodeverdi = temaKodeverdi,
                    kodebeskrivelse = temaKodebeskrivelse
                };
            }

            if (erMangelBesvaresSenere.HasValue)
            {
                mangelbesvarelse.erMangelBesvaresSenere = erMangelBesvaresSenere.Value;
                mangelbesvarelse.erMangelBesvaresSenereSpecified = true;
            }

            VedleggType vedlegg = null;
            if (!string.IsNullOrEmpty(vedleggFilnavn) || !string.IsNullOrEmpty(vedleggKodeverdi) || !string.IsNullOrEmpty(vedleggKodebeskrivelse) || !string.IsNullOrEmpty(vedleggKodebeskrivelse) || vedleggVersjonsdato.HasValue)
            {
                vedlegg = new VedleggType()
                {
                    filnavn = vedleggFilnavn,
                    versjonsnummer = vedleggVersjonsnummer,
                };

                if (vedleggVersjonsdato.HasValue)
                {
                    vedlegg.versjonsdato = vedleggVersjonsdato.Value;
                    vedlegg.versjonsdatoSpecified = true;
                }

                if (!string.IsNullOrEmpty(vedleggKodeverdi) || !string.IsNullOrEmpty(vedleggKodebeskrivelse))
                {
                    vedlegg.vedleggstype = new KodeType()
                    {
                        kodeverdi = vedleggKodeverdi,
                        kodebeskrivelse = vedleggKodebeskrivelse
                    };
                }
            }


            if (!string.IsNullOrEmpty(mangelId) || !string.IsNullOrEmpty(mangeltekstKodeverdi) || !string.IsNullOrEmpty(mangeltekstKodebeskrivelse))
            {
                mangelbesvarelse.mangel = new MangeltekstType()
                {
                    mangelId = mangelId,
                };
                if (!string.IsNullOrEmpty(mangeltekstKodeverdi) || !string.IsNullOrEmpty(mangeltekstKodebeskrivelse))
                {
                    mangelbesvarelse.mangel.mangeltekst = new KodeType()
                    {
                        kodeverdi = mangeltekstKodeverdi,
                        kodebeskrivelse = mangeltekstKodebeskrivelse,
                    };
                }
            }

            KodeType underskjema = null;
            if (!string.IsNullOrEmpty(underskjemaKodeverdi) || !string.IsNullOrEmpty(underskjemaKodebeskrivelse))
            {
                underskjema = new KodeType()
                {
                    kodeverdi = underskjemaKodeverdi,
                    kodebeskrivelse = underskjemaKodebeskrivelse
                };
            }

            List<MangelbesvarelseType> mangelbesvarelseTypes = _form.mangelbesvarelse?.ToList() ?? new List<MangelbesvarelseType>();

            if (mangelbesvarelseTypes.Any() && Helpers.ObjectIsNullOrEmpty(mangelbesvarelse))
            {
                if (vedlegg != null)
                {
                    var vedleggTypes = mangelbesvarelseTypes?.LastOrDefault()?.vedlegg?.ToList() ?? new List<VedleggType>();
                    vedleggTypes.Add(vedlegg);
                    mangelbesvarelseTypes.LastOrDefault().vedlegg = vedleggTypes.ToArray();

                }
                if (underskjema != null)
                {
                    var underskjemaTypes = mangelbesvarelseTypes?.LastOrDefault()?.underskjema?.ToList() ?? new List<KodeType>();
                    underskjemaTypes.Add(underskjema);
                    mangelbesvarelseTypes.LastOrDefault().underskjema = underskjemaTypes.ToArray();
                }
            }
            else
            {
                if (vedlegg != null)
                    mangelbesvarelse.vedlegg = new[] { vedlegg };

                if (underskjema != null)
                    mangelbesvarelse.underskjema = new[] { underskjema };

                mangelbesvarelseTypes.Add(mangelbesvarelse);
            }

            _form.mangelbesvarelse = mangelbesvarelseTypes.ToArray();
            return this;
        }

        internal SuppleringAvSoeknadBuilder EiendomByggested(SampleEiendomData.Eiendom eiendom)
        {
            EiendomByggested(eiendom.EiendomsIdentifikasjon.Kommunenummer, eiendom.EiendomsIdentifikasjon.Gaardsnummer, eiendom.EiendomsIdentifikasjon.Bruksnummer, eiendom.EiendomsIdentifikasjon.Festenummer, eiendom.EiendomsIdentifikasjon.Seksjonsnummer,
                eiendom.Kommunenavn, eiendom.Bolignummer, eiendom.Bygningsnummer, eiendom.Adresse.adresselinje1, eiendom.Adresse.gatenavn, eiendom.Adresse.husnr, eiendom.Adresse.postnr, eiendom.Adresse.poststed);
            return this;
        }
        internal SuppleringAvSoeknadBuilder AnsvarligSoeker(SampleAktoerData.Aktoer ansvarligAktoer)
        {
            AnsvarligSoeker(ansvarligAktoer.organisasjonsnummer, ansvarligAktoer.foedselsnummer, ansvarligAktoer.partsTypeKodeverdi, ansvarligAktoer.partsTypeKodebeskrivelse, ansvarligAktoer.navn, ansvarligAktoer.Adresse.adresselinje1
                , ansvarligAktoer.Adresse.postnr, ansvarligAktoer.Adresse.landkode, ansvarligAktoer.Adresse.poststed, ansvarligAktoer.telefonnummer, ansvarligAktoer.mobilnummer, ansvarligAktoer.Epost, ansvarligAktoer.kontaktperson?.navn, ansvarligAktoer.kontaktperson?.telefonnummer, ansvarligAktoer.kontaktperson?.Epost);
            return this;
        }
        internal SuppleringAvSoeknadBuilder Tiltakshaver(SampleAktoerData.Aktoer tiltakshaver)
        {
            Tiltakshaver(tiltakshaver.organisasjonsnummer, tiltakshaver.foedselsnummer, tiltakshaver.partsTypeKodeverdi, tiltakshaver.partsTypeKodebeskrivelse, tiltakshaver.navn, tiltakshaver.Adresse.adresselinje1
                , tiltakshaver.Adresse.postnr, tiltakshaver.Adresse.landkode, tiltakshaver.Adresse.poststed, tiltakshaver.telefonnummer, tiltakshaver.mobilnummer, tiltakshaver.Epost, tiltakshaver.kontaktperson?.navn, tiltakshaver.kontaktperson?.telefonnummer, tiltakshaver.kontaktperson?.Epost, tiltakshaver.kontaktperson?.mobilnummer);
            return this;
        }

    }
}