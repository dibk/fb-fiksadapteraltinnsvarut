﻿using no.kxml.skjema.dibk.kontrollerklaeringDirekteOpprettet;
using System;
using System.Linq;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.SampleData;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.Builders
{
    class KontrollerklaeringDirekteBuilder
    {
        private readonly no.kxml.skjema.dibk.kontrollerklaeringDirekteOpprettet.KontrollerklaeringDirekteType _form;

        public KontrollerklaeringDirekteBuilder()
        {
            _form = new KontrollerklaeringDirekteType();
        }
        
        public KontrollerklaeringDirekteType Build()
        {
            return _form;
        }

        public KontrollerklaeringDirekteBuilder ProsjektData(string fraStluttbrukersystem, string prosjektnr, string hovedinnsendingsnummer, string prosjektnavn)
        {
            _form.fraSluttbrukersystem = fraStluttbrukersystem;
            _form.prosjektnr = prosjektnr;
            _form.hovedinnsendingsnummer = hovedinnsendingsnummer;
            _form.prosjektnavn = prosjektnavn;

            //Statics
            _form.erTEK10 = false;

            return this;
        }

        public KontrollerklaeringDirekteBuilder KommunensSaksnummer(string saksaar, string sakssekvensnummer)
        {
            _form.kommunensSaksnummer = new SaksnummerType(){saksaar = saksaar, sakssekvensnummer = sakssekvensnummer };

            return this;
        }

        public KontrollerklaeringDirekteBuilder AnsvarligSoeker(string organisasjonsnummer, string partsTypeKodeverdi = null, string partsTypeKodebeskrivelse = null, string kontaktperson = null, string navn = null, string adressLine = null, string postnr = null, string landkode = null
            , string poststed = null, string telefonnummer = null, string mobilnummer = null, string epost = null)
        {
            _form.ansvarligSoeker = new PartType();

            if (!string.IsNullOrEmpty(partsTypeKodeverdi) || !string.IsNullOrEmpty(partsTypeKodebeskrivelse))
            {
                _form.ansvarligSoeker.partstype = new KodeType
                {
                    kodeverdi = partsTypeKodeverdi,
                    kodebeskrivelse = partsTypeKodebeskrivelse
                };
            }
            _form.ansvarligSoeker.navn = navn;

            if (!string.IsNullOrEmpty(adressLine) || !string.IsNullOrEmpty(postnr) || !string.IsNullOrEmpty(landkode))
            {
                _form.ansvarligSoeker.adresse = new EnkelAdresseType
                {
                    adresselinje1 = adressLine,
                    postnr = postnr,
                    landkode = landkode,
                    poststed = poststed
                };
            }

            if (!string.IsNullOrEmpty(kontaktperson))
            {
                _form.ansvarligSoeker.kontaktperson = new KontaktpersonType()
                {
                    navn = kontaktperson
                };
            }

            _form.ansvarligSoeker.organisasjonsnummer = organisasjonsnummer; //"974760673" - 974760673 BRREG, Altinn brukerservice
            _form.ansvarligSoeker.telefonnummer = telefonnummer;
            _form.ansvarligSoeker.mobilnummer = mobilnummer;
            _form.ansvarligSoeker.epost = epost;

            return this;
        }

        public KontrollerklaeringDirekteBuilder Foretak(string organisasjonsnummer, string partsTypeKodeverdi = null, string partsTypeKodebeskrivelse = null, string kontaktperson = null, string navn = null, string adressLine = null, string postnr = null, string landkode = null
            , string poststed = null, string telefonnummer = null, string mobilnummer = null, string epost = null)
        {
            _form.foretak = new PartType();

            if (!string.IsNullOrEmpty(partsTypeKodeverdi) || !string.IsNullOrEmpty(partsTypeKodebeskrivelse))
            {
                _form.foretak.partstype = new KodeType
                {
                    kodeverdi = partsTypeKodeverdi,
                    kodebeskrivelse = partsTypeKodebeskrivelse
                };
            }

            _form.foretak.navn = navn;

            if (!string.IsNullOrEmpty(adressLine) || !string.IsNullOrEmpty(postnr) || !string.IsNullOrEmpty(landkode))
            {
                _form.foretak.adresse = new EnkelAdresseType
                {
                    adresselinje1 = adressLine,
                    postnr = postnr,
                    landkode = landkode,
                    poststed = poststed
                };
            }

            if (!string.IsNullOrEmpty(kontaktperson))
            {
                _form.foretak.kontaktperson = new KontaktpersonType()
                {
                    navn = kontaktperson
                };
            }

            _form.foretak.organisasjonsnummer = organisasjonsnummer; //"974760673" - 974760673 BRREG, Altinn brukerservice
            _form.foretak.telefonnummer = telefonnummer;
            _form.foretak.mobilnummer = mobilnummer;
            _form.foretak.epost = epost;

            return this;
        }

        public KontrollerklaeringDirekteBuilder Ansvarsrett(string funksjonKodeverdi = null, string funksjonKodebeskrivelse = null, bool? observerteAvvik = null, bool? aapneAvvik = null, bool? ingenAvvik = null, bool? ansvarsomraadetAvsluttet = null,
            DateTime? ansvarsrettErklaert = null, string soeknadssystemetsReferanse = null, string beskrivelseAvAnsvarsomraadet = null)
        {

            _form.ansvarsrett = new AnsvarsomraadeType();
            if (funksjonKodeverdi != null)
            {
                _form.ansvarsrett.funksjon = new KodeType()
                {
                    kodeverdi = funksjonKodeverdi,
                    kodebeskrivelse = funksjonKodebeskrivelse
                };
            }

            if (observerteAvvik.HasValue || aapneAvvik.HasValue || ingenAvvik.HasValue)
            {
                _form.ansvarsrett.kontrollerende = new KontrollerendeType();
                if (observerteAvvik.HasValue)
                {
                    _form.ansvarsrett.kontrollerende.observerteAvvikSpecified = true;
                    _form.ansvarsrett.kontrollerende.observerteAvvik = observerteAvvik.Value;
                }
                if (aapneAvvik.HasValue)
                {
                    _form.ansvarsrett.kontrollerende.aapneAvvikSpecified = true;
                    _form.ansvarsrett.kontrollerende.aapneAvvik = aapneAvvik.Value;
                }
                if (ingenAvvik.HasValue)
                {
                    _form.ansvarsrett.kontrollerende.ingenAvvikSpecified = true;
                    _form.ansvarsrett.kontrollerende.ingenAvvik = ingenAvvik.Value;
                }
            }

            if (ansvarsomraadetAvsluttet.HasValue)
            {
                _form.ansvarsrett.ansvarsomraadetAvsluttetSpecified = true;
                _form.ansvarsrett.ansvarsomraadetAvsluttet = ansvarsomraadetAvsluttet.Value;
            }

            if (ansvarsrettErklaert.HasValue)
            {
                _form.ansvarsrett.ansvarsrettErklaertSpecified = true;
                _form.ansvarsrett.ansvarsrettErklaert = ansvarsrettErklaert.Value;
            }

            _form.ansvarsrett.soeknadssystemetsReferanse = soeknadssystemetsReferanse;
            _form.ansvarsrett.beskrivelseAvAnsvarsomraadet = beskrivelseAvAnsvarsomraadet;

            return this;
        }
        public KontrollerklaeringDirekteBuilder ErklaeringKontroll(bool? erklaeringKontroll)
        {
            _form.erklaeringKontroll = erklaeringKontroll;
            _form.erklaeringKontrollSpecified = true;
            return this;
        }

        public KontrollerklaeringDirekteBuilder EiendomByggested(string knr = null, string gnr = null, string bnr = null, string fnr = null, string snr = null, string kommunenavn = null, string bolignummer = null, string bygningsnummer = null
            , string adresselinje1 = null, string gatenavn = null, string husnr = null, string postnr = null, string poststed = null)
        {
            var eiendomByggested = new EiendomType();
            eiendomByggested.bolignummer = bolignummer;
            eiendomByggested.kommunenavn = kommunenavn;
            eiendomByggested.eiendomsidentifikasjon = new MatrikkelnummerType()
            {
                kommunenummer = knr,
                gaardsnummer = gnr,
                bruksnummer = bnr,
                festenummer = fnr,
                seksjonsnummer = snr
            };

            eiendomByggested.bygningsnummer = bygningsnummer;
            if (!string.IsNullOrEmpty(adresselinje1) || !string.IsNullOrEmpty(gatenavn) || !string.IsNullOrEmpty(husnr))
            {
                eiendomByggested.adresse = new EiendommensAdresseType()
                {
                    adresselinje1 = adresselinje1,
                    gatenavn = gatenavn,
                    husnr = husnr,
                    postnr = postnr,
                    poststed = poststed
                };
            }

            AddEiendomToFrom(eiendomByggested);
            return this;
        }
        private void AddEiendomToFrom(EiendomType eiendomByggested)
        {
            if (_form.eiendomByggested == null)
            {
                _form.eiendomByggested = new[] { eiendomByggested };
            }
            else
            {
                var eiendomTypes = _form.eiendomByggested.ToList();
                eiendomTypes.Add(eiendomByggested);
                _form.eiendomByggested = eiendomTypes.ToArray();
            }
        }
        public KontrollerklaeringDirekteBuilder Foretak(SampleAktoerData.Aktoer foretak)
        {
            Foretak(foretak.organisasjonsnummer, foretak.partsTypeKodeverdi, foretak.partsTypeKodebeskrivelse, foretak.kontaktperson?.navn, foretak.navn, foretak.Adresse.adresselinje1,
                foretak.Adresse.postnr, foretak.Adresse.landkode, foretak.Adresse.poststed, foretak.telefonnummer, foretak.mobilnummer, foretak.Epost);
            return this;
        }
        public KontrollerklaeringDirekteBuilder AnsvarligSoeker(SampleAktoerData.Aktoer ansvarligAktoer)
        {
            AnsvarligSoeker(ansvarligAktoer.organisasjonsnummer, ansvarligAktoer.partsTypeKodeverdi, ansvarligAktoer.partsTypeKodebeskrivelse, ansvarligAktoer.kontaktperson?.navn, ansvarligAktoer.navn, ansvarligAktoer.Adresse.adresselinje1,
                ansvarligAktoer.Adresse.postnr, ansvarligAktoer.Adresse.landkode, ansvarligAktoer.Adresse.poststed, ansvarligAktoer.telefonnummer, ansvarligAktoer.mobilnummer, ansvarligAktoer.Epost);
            return this;
        }

        public KontrollerklaeringDirekteBuilder EiendomByggested(SampleEiendomData.Eiendom eiendom)
        {
            EiendomByggested(eiendom.EiendomsIdentifikasjon.Kommunenummer, eiendom.EiendomsIdentifikasjon.Gaardsnummer, eiendom.EiendomsIdentifikasjon.Bruksnummer, eiendom.EiendomsIdentifikasjon.Festenummer, eiendom.EiendomsIdentifikasjon.Seksjonsnummer,
                eiendom.Kommunenavn, eiendom.Bolignummer, eiendom.Bygningsnummer, eiendom.Adresse.adresselinje1, eiendom.Adresse.gatenavn, eiendom.Adresse.husnr, eiendom.Adresse.postnr, eiendom.Adresse.poststed);
            return this;
        }
    }
}
