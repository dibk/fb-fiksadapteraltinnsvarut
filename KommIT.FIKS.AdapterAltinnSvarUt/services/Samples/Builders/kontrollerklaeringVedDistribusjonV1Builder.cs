﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using no.kxml.skjema.dibk.kontrollerklaeringVedDistribusjon;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.Builders
{
    public class KontrollerklaeringVedDistribusjonV1Builder
    {
        private readonly KontrollerklaeringType _form;

        public KontrollerklaeringVedDistribusjonV1Builder()
        {
            _form = new KontrollerklaeringType();
        }

        public KontrollerklaeringType Build()
        {
            return _form;
        }

        internal KontrollerklaeringVedDistribusjonV1Builder EiendomByggested(string knr = null, string gnr = null, string bnr = null, string fnr = null, string snr = null, string kommunenavn = null, string bolignummer = null, string bygningsnummer = null
            , string adresselinje1 = null, string gatenavn = null, string husnr = null, string postnr = null, string poststed = null)
        {
            var eiendomByggested = new EiendomType();
            eiendomByggested.bolignummer = bolignummer;
            eiendomByggested.kommunenavn = kommunenavn;
            eiendomByggested.eiendomsidentifikasjon = new MatrikkelnummerType()
            {
                kommunenummer = knr,
                gaardsnummer = gnr,
                bruksnummer = bnr,
                festenummer = fnr,
                seksjonsnummer = snr
            };

            eiendomByggested.bygningsnummer = bygningsnummer;
            if (!string.IsNullOrEmpty(adresselinje1) || !string.IsNullOrEmpty(gatenavn) || !string.IsNullOrEmpty(husnr))
            {
                eiendomByggested.adresse = new EiendommensAdresseType()
                {
                    adresselinje1 = adresselinje1,
                    gatenavn = gatenavn,
                    husnr = husnr,
                    postnr = postnr,
                    poststed = poststed
                };
            }

            AddEiendomToFrom(eiendomByggested);
            return this;
        }


        internal KontrollerklaeringVedDistribusjonV1Builder Ansvarsrett(string funksjonKodeverdi = null,string funksjonKodebeskrivelse = null, bool? observerteAvvik = null, bool? aapneAvvik=null, bool? ingenAvvik = null,DateTime? ansvarsrettErklaert = null)
        {

            _form.ansvarsrett = new AnsvarsomraadeType();
            if (funksjonKodeverdi != null)
            {
                _form.ansvarsrett.funksjon = new KodeType()
                {
                    kodeverdi = funksjonKodeverdi,
                    kodebeskrivelse = funksjonKodebeskrivelse
                };
            }

            if (observerteAvvik.HasValue || aapneAvvik.HasValue || ingenAvvik.HasValue)
            {
                _form.ansvarsrett.kontrollerende= new KontrollerendeType();
                if (observerteAvvik.HasValue)
                {
                    _form.ansvarsrett.kontrollerende.observerteAvvikSpecified = true;
                    _form.ansvarsrett.kontrollerende.observerteAvvik = observerteAvvik;
                }
                if (aapneAvvik.HasValue)
                {
                    _form.ansvarsrett.kontrollerende.aapneAvvikSpecified = true;
                    _form.ansvarsrett.kontrollerende.aapneAvvik = aapneAvvik;
                } 
                if (ingenAvvik.HasValue)
                {
                    _form.ansvarsrett.kontrollerende.ingenAvvikSpecified = true;
                    _form.ansvarsrett.kontrollerende.ingenAvvik = ingenAvvik;
                }

            }

            if (ansvarsrettErklaert.HasValue)
            {
                _form.ansvarsrett.ansvarsrettErklaert = ansvarsrettErklaert.Value;
            }
            return this;
        }

        public KontrollerklaeringVedDistribusjonV1Builder Foretak(string organisasjonsnummer, string foedselsnummer, string partsTypeKodeverdi = null, string partsTypeKodebeskrivelse = null, string kontaktperson = null, string navn = null, string adressLine = null, string postnr = null, string landkode = null
            , string poststed = null, string telefonnummer = null, string mobilnummer = null, string epost = null)
        {
            _form.foretak = new PartType();

            if (!string.IsNullOrEmpty(partsTypeKodeverdi) || !string.IsNullOrEmpty(partsTypeKodebeskrivelse))
            {
                _form.foretak.partstype = new KodeType
                {
                    kodeverdi = partsTypeKodeverdi,
                    kodebeskrivelse = partsTypeKodebeskrivelse
                };
            }

            _form.foretak.navn = navn;

            if (!string.IsNullOrEmpty(adressLine) || !string.IsNullOrEmpty(postnr) || !string.IsNullOrEmpty(landkode))
            {
                _form.foretak.adresse = new EnkelAdresseType
                {
                    adresselinje1 = adressLine,
                    postnr = postnr,
                    landkode = landkode,
                    poststed = poststed
                };
            }

            if (!string.IsNullOrEmpty(kontaktperson))
            {
                _form.foretak.kontaktperson = new KontaktpersonType()
                {
                    navn = kontaktperson
                };
            }

            _form.foretak.organisasjonsnummer = organisasjonsnummer; //"974760673" - 974760673 BRREG, Altinn brukerservice
            _form.foretak.foedselsnummer = foedselsnummer;
            _form.foretak.telefonnummer = telefonnummer;
            _form.foretak.mobilnummer = mobilnummer;
            _form.foretak.epost = epost;

            return this;
        }
        public KontrollerklaeringVedDistribusjonV1Builder AnsvarligSoeker(string organisasjonsnummer, string foedselsnummer, string partsTypeKodeverdi = null, string partsTypeKodebeskrivelse = null, string kontaktperson = null, string navn = null, string adressLine = null, string postnr = null, string landkode = null
            , string poststed = null, string telefonnummer = null, string mobilnummer = null, string epost = null)
        {
            _form.ansvarligSoeker = new PartType();

            if (!string.IsNullOrEmpty(partsTypeKodeverdi) || !string.IsNullOrEmpty(partsTypeKodebeskrivelse))
            {
                _form.ansvarligSoeker.partstype = new KodeType
                {
                    kodeverdi = partsTypeKodeverdi,
                    kodebeskrivelse = partsTypeKodebeskrivelse
                };
            }

            _form.ansvarligSoeker.navn = navn;

            if (!string.IsNullOrEmpty(adressLine) || !string.IsNullOrEmpty(postnr) || !string.IsNullOrEmpty(landkode))
            {
                _form.ansvarligSoeker.adresse = new EnkelAdresseType
                {
                    adresselinje1 = adressLine,
                    postnr = postnr,
                    landkode = landkode,
                    poststed = poststed
                };
            }

            if (!string.IsNullOrEmpty(kontaktperson))
            {
                _form.ansvarligSoeker.kontaktperson = new KontaktpersonType()
                {
                    navn = kontaktperson
                };
            }

            _form.ansvarligSoeker.organisasjonsnummer = organisasjonsnummer; //"974760673" - 974760673 BRREG, Altinn brukerservice
            _form.ansvarligSoeker.foedselsnummer = foedselsnummer;
            _form.ansvarligSoeker.telefonnummer = telefonnummer;
            _form.ansvarligSoeker.mobilnummer = mobilnummer;
            _form.ansvarligSoeker.epost = epost;

            return this;
        }

        public KontrollerklaeringVedDistribusjonV1Builder Bekreftelser(bool? erklaeringKontroll, DateTime? now = null)
        {
            if (erklaeringKontroll.HasValue)
            {
                _form.erklaeringKontrollSpecified = true;
                _form.erklaeringKontroll = erklaeringKontroll;
            }

            return this;
        }


        private void AddEiendomToFrom(EiendomType eiendomByggested)
        {
            if (_form.eiendomByggested == null)
            {
                _form.eiendomByggested = new[] { eiendomByggested };
            }
            else
            {
                var eiendomTypes = _form.eiendomByggested.ToList();
                eiendomTypes.Add(eiendomByggested);
                _form.eiendomByggested = eiendomTypes.ToArray();
            }
        }

    }
}
