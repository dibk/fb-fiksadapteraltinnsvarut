﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using no.kxml.skjema.dibk.vedlegg;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.Builders
{
    class VedleggsopplysningerV1Builder
    {
        private readonly VedleggsopplysningerType _form;

        public VedleggsopplysningerV1Builder()
        {
            _form = new VedleggsopplysningerType();
        }

        public VedleggsopplysningerType Build()
        {
            return _form;
        }

        public VedleggsopplysningerV1Builder Vedlegg(string ifcValidationId = null, string vedleggstype = null, DateTime? tegningsdato = null, string tegningsnr = null)
        {
            _form.vedlegg = new[] { new VedleggType() };

            if (ifcValidationId != null)
                _form.vedlegg[0].ifcValidationId = ifcValidationId;

            if (vedleggstype != null)
                _form.vedlegg[0].vedleggstype = vedleggstype;

            if (tegningsdato != null) {
                _form.vedlegg[0].tegningsdato = tegningsdato;
                _form.vedlegg[0].tegningsdatoSpecified = true;
            }

            if (tegningsnr != null)
                _form.vedlegg[0].tegningsnr = tegningsnr;

            _form.vedlegg[0].vedleggsid = "1234567";
            _form.vedlegg[0].beskrivelse = "Erklæring ansvarsrett fra Brann Systemer AS";
            _form.vedlegg[0].vedleggsUrl = "signert_ansvarsrett_1234567.pdf";
            _form.vedlegg[0].sluttbrukersystemReferanse = "c3adb4a3-317e-4ff6-9a42-c1c1655cf162";
            _form.vedlegg[0].vedleggskategori = "G";
            return this;
        }

    }
}
