﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using no.kxml.skjema.dibk.gjennomforingsplanV4;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.Builders
{
    class GjennomfoeringsplanV4Builder
    {
        private readonly GjennomfoeringsplanType _form = new GjennomfoeringsplanType();


        public GjennomfoeringsplanV4Builder()
        {
            _form.gjennomfoeringsplan = new AnsvarsomraadeType[1];
        }

        public GjennomfoeringsplanType Build()
        {
            return _form;
        }

        public GjennomfoeringsplanV4Builder AnsvarsOmraader(bool ansvarsomraadeAvsluttet, string arbeidsomraade, string fagomraade,
            string funksjonkodeverdi, bool skMidlertidigbrukstillatelsePlanlagt, bool skRammeTillatelsePlanlagt, 
            bool skIgangsettingTillatelsePlanlagt, bool skFerdigattestPlanlagt,
            DateTime ? skMidlertidigbrukstillatelseForeligger, DateTime ? skFerdigattestForeligger, 
            DateTime ? skIgangsettingTillatelseForeligger, DateTime ? skRammeTillatelseForeligger)
        {
            _form.gjennomfoeringsplan = new AnsvarsomraadeType[]
            {
                new AnsvarsomraadeType()
                {
                    ansvarsomraadetAvsluttet = ansvarsomraadeAvsluttet,
                    arbeidsomraade = arbeidsomraade,
                    fagomraade = fagomraade,

                    funksjon = new KodeType()
                    {
                        kodeverdi = funksjonkodeverdi,
                    },

                    tiltaksklasse = new KodeType()
                    {
                        kodeverdi = "1"
                    },

                    foretak = new ForetakType()
                    {
                        partstype = new KodeType()
                        {
                            kodeverdi = "Foretak",
                            kodebeskrivelse = ""
                        },
                        organisasjonsnummer = "974760223",
                        navn = "Arkitektum",
                        adresse = new EnkelAdresseType()
                        {
                            postnr = "3800",
                            landkode = "NO",
                            poststed = "Bø"
                        }

                    },
                    samsvarKontrollPlanlagtVedMidlertidigBrukstillatelse = skMidlertidigbrukstillatelsePlanlagt,
                    samsvarKontrollPlanlagtVedRammetillatelse = skRammeTillatelsePlanlagt,
                    samsvarKontrollPlanlagtVedIgangsettingstillatelse = skIgangsettingTillatelsePlanlagt,
                    samsvarKontrollPlanlagtVedFerdigattest = skFerdigattestPlanlagt,

                    samsvarKontrollForeliggerVedMidlertidigBrukstillatelse = skMidlertidigbrukstillatelseForeligger,
                    samsvarKontrollForeliggerVedRammetillatelse = skRammeTillatelseForeligger,
                    samsvarKontrollForeliggerVedIgangsettingstillatelse = skIgangsettingTillatelseForeligger,
                    samsvarKontrollForeliggerVedFerdigattest = skFerdigattestForeligger,

                }
            };

            return this;
        }

    }
}
