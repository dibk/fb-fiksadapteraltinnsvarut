﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.SampleData;
using no.kxml.skjema.dibk.nabovarselV4;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.Builders
{
    class NabovarselV4Builder
    {
        private readonly NabovarselType _form = new NabovarselType();

        public NabovarselV4Builder()
        {
            _form.naboeier = new NaboGjenboerType[] { };
        }

        public NabovarselType Build()
        {
            return _form;
        }

        public NabovarselV4Builder NaboEiere(string navn = null, string partstypeKodeverdi = null, string foedselsNummer = null, string orgNummer = null,
            string adresselinje1 = null, string postnr = null, string poststed = null, string landkode = null)
        {
            var naboeier = new NaboGjenboerType()
            {

                navn = navn,
                foedselsnummer = foedselsNummer,
                organisasjonsnummer = orgNummer,

            };

            if (!string.IsNullOrEmpty(partstypeKodeverdi))
            {
                naboeier.partstype = new KodeType()
                {
                    kodeverdi = partstypeKodeverdi,
                };
            }

            if (!string.IsNullOrEmpty(adresselinje1) || !string.IsNullOrEmpty(postnr) || !string.IsNullOrEmpty(landkode) || !string.IsNullOrEmpty(landkode))
            {
                naboeier.adresse = new EnkelAdresseType()
                {
                    adresselinje1 = adresselinje1,
                    postnr = postnr,
                    poststed = poststed,
                    landkode = landkode,

                };
            }

            AddNaboeier(naboeier);
            return this;
        }



        public NabovarselV4Builder AddGjelderNaboeiendom(string bygningsnummer = null, string bolignummer = null, string eier = null, string kommunenavn = null)
        {

            var gjelderNaboeiendom = new EiendomType()
            {
                bygningsnummer = bygningsnummer,
                bolignummer = bolignummer,
                eier = eier,
                kommunenavn = kommunenavn
            };
            AddNaboeier(null, gjelderNaboeiendom, null, null);

            return this;
        }
        public NabovarselV4Builder AddEiendomsIdentifikasjonToGelderNaboeiendom(string kommunenummer = null, string bruksnummer = null, string festenummer = null, string gaardsnummer = null, string seksjonsnummer = null)
        {

            var eiendomsIdentifikasjon = new MatrikkelnummerType()
            {
                kommunenummer = kommunenummer,
                bruksnummer = bruksnummer,
                festenummer = festenummer,
                gaardsnummer = gaardsnummer,
                seksjonsnummer = seksjonsnummer
            };

            AddNaboeier(null, null, null, eiendomsIdentifikasjon);
            return this;
        }
        public NabovarselV4Builder AddAddressToGjelderNaboEiendom(string adresselinje1 = null, string gatenavn = null, string husnr = null, string bokstav = null, string postnr = null, string poststed = null, string landkode = null)
        {
            var address = new EiendommensAdresseType()
            {
                adresselinje1 = adresselinje1,
                gatenavn = gatenavn,
                husnr = husnr,
                bokstav = bokstav,
                postnr = postnr,
                poststed = poststed,
                landkode = landkode
            };

            AddNaboeier(null, null, address);
            return this;
        }

        public NabovarselV4Builder AnsvarligSoeker(string organisasjonsnummer, string foedselsnummer, string partsTypeKodeverdi = null, string partsTypeKodebeskrivelse = null, string kontaktperson = null, string navn = null, string adressLine = null, string postnr = null, string landkode = null
          , string poststed = null, string telefonnummer = null, string mobilnummer = null, string epost = null)
        {
            _form.ansvarligSoeker = new PartType();

            if (!string.IsNullOrEmpty(partsTypeKodeverdi))
            {
                _form.ansvarligSoeker.partstype = new KodeType
                {
                    kodeverdi = partsTypeKodeverdi,
                    kodebeskrivelse = ""
                };
            }

            _form.ansvarligSoeker.navn = navn;

            if (!string.IsNullOrEmpty(adressLine) || !string.IsNullOrEmpty(postnr) || !string.IsNullOrEmpty(landkode) || !string.IsNullOrEmpty(poststed))
            {
                _form.ansvarligSoeker.adresse = new EnkelAdresseType
                {
                    adresselinje1 = adressLine,
                    postnr = postnr,
                    landkode = landkode,
                    poststed = poststed
                };
            }

            if (!string.IsNullOrEmpty(kontaktperson))
            {
                _form.ansvarligSoeker.kontaktperson = new KontaktpersonType()
                {
                    navn = kontaktperson
                };
            }

            _form.ansvarligSoeker.organisasjonsnummer = organisasjonsnummer; //"974760673" - 974760673 BRREG, Altinn brukerservice
            _form.ansvarligSoeker.foedselsnummer = foedselsnummer;
            _form.ansvarligSoeker.telefonnummer = telefonnummer;
            _form.ansvarligSoeker.mobilnummer = mobilnummer;
            _form.ansvarligSoeker.epost = epost;

            return this;
        }
        public NabovarselV4Builder Tiltakshaver(string organisasjonsnummer, string foedselsnummer, string partsTypeKodeverdi = null, string partsTypeKodebeskrivelse = null, string kontaktperson = null, string navn = null, string adressLine = null, string postnr = null, string landkode = null
           , string poststed = null, string telefonnummer = null, string mobilnummer = null, string epost = null)
        {
            _form.tiltakshaver = new PartType();

            if (!string.IsNullOrEmpty(partsTypeKodeverdi) || !string.IsNullOrEmpty(partsTypeKodebeskrivelse))
            {
                _form.tiltakshaver.partstype = new KodeType
                {
                    kodeverdi = partsTypeKodeverdi,
                    kodebeskrivelse = partsTypeKodebeskrivelse
                };
            }

            _form.tiltakshaver.navn = navn;

            if (!string.IsNullOrEmpty(adressLine) || !string.IsNullOrEmpty(postnr) || !string.IsNullOrEmpty(landkode))
            {
                _form.tiltakshaver.adresse = new EnkelAdresseType
                {
                    adresselinje1 = adressLine,
                    postnr = postnr,
                    landkode = landkode,
                    poststed = poststed
                };
            }

            if (!string.IsNullOrEmpty(kontaktperson))
            {
                _form.tiltakshaver.kontaktperson = new KontaktpersonType()
                {
                    navn = kontaktperson
                };
            }

            _form.tiltakshaver.organisasjonsnummer = organisasjonsnummer; //"974760673" - 974760673 BRREG, Altinn brukerservice
            _form.tiltakshaver.foedselsnummer = foedselsnummer;
            _form.tiltakshaver.telefonnummer = telefonnummer;
            _form.tiltakshaver.mobilnummer = mobilnummer;
            _form.tiltakshaver.epost = epost;

            return this;
        }

        public NabovarselV4Builder BeskrivelseAvTiltak(Dictionary<string, string> tiltalkTypes = null, string beskrivPlanlagtFormaal = null, Dictionary<string, string> tiltalkformaals = null)
        {
            var tiltakType = new TiltakType();


            if (!string.IsNullOrEmpty(beskrivPlanlagtFormaal))
            {
                if (tiltakType.bruk == null)
                    tiltakType.bruk = new FormaalType();
                tiltakType.bruk.beskrivPlanlagtFormaal = beskrivPlanlagtFormaal;
            }

            if (tiltalkformaals != null)
            {
                if (tiltakType.bruk == null)
                    tiltakType.bruk = new FormaalType();

                var kodeTypes = new List<KodeType>();
                foreach (var kodeType in tiltalkformaals)
                {
                    kodeTypes.Add(new KodeType()
                    {
                        kodeverdi = kodeType.Key,
                        kodebeskrivelse = kodeType.Value
                    });
                }
                tiltakType.bruk.tiltaksformaal = kodeTypes.ToArray();
            }

            if (tiltalkTypes != null)
            {
                var kodeTypes = new List<KodeType>();
                foreach (var kodeType in tiltalkTypes)
                {
                    kodeTypes.Add(new KodeType()
                    {
                        kodeverdi = kodeType.Key,
                        kodebeskrivelse = kodeType.Value
                    });
                }
                tiltakType.type = kodeTypes.ToArray();
            }

            AddbeskrivelseAvTiltak(tiltakType);
            return this;
        }
        public NabovarselV4Builder Dispensasjon(string kodeverdi, string kodebeskrivelse, string beskrivelse, string begrunelse)
        {
            _form.dispensasjon = new[]
            {
                new DispensasjonType(){
                    dispensasjonstype = new KodeType()
                    {
                        kodeverdi = kodeverdi,
                        kodebeskrivelse = kodebeskrivelse
                    },
                    begrunnelse = begrunelse,
                    beskrivelse = beskrivelse
                },
            };
            return this;
        }

        internal NabovarselV4Builder EiendomByggested(string knr = null, string gnr = null, string bnr = null, string fnr = null, string snr = null, string kommunenavn = null, string bolignummer = null, string bygningsnummer = null
            , string adresselinje1 = null, string gatenavn = null, string husnr = null, string postnr = null, string poststed = null)
        {
            var eiendomByggested = new EiendomType();
            eiendomByggested.bolignummer = bolignummer;
            eiendomByggested.kommunenavn = kommunenavn;
            eiendomByggested.eiendomsidentifikasjon = new MatrikkelnummerType()
            {
                kommunenummer = knr,
                gaardsnummer = gnr,
                bruksnummer = bnr,
                festenummer = fnr,
                seksjonsnummer = snr
            };

            eiendomByggested.bygningsnummer = bygningsnummer;
            if (!string.IsNullOrEmpty(adresselinje1) || !string.IsNullOrEmpty(gatenavn) || !string.IsNullOrEmpty(husnr))
            {
                eiendomByggested.adresse = new EiendommensAdresseType()
                {
                    adresselinje1 = adresselinje1,
                    gatenavn = gatenavn,
                    husnr = husnr,
                    postnr = postnr,
                    poststed = poststed
                };
            }

            AddEiendomToFrom(eiendomByggested);
            return this;
        }
        public NabovarselV4Builder FraSluttbrukersystem(string fraSluttbrukersystem)
        {
            _form.fraSluttbrukersystem = fraSluttbrukersystem;
            return this;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="plantypeKodeverdi">"RP"</param>
        /// <param name="plantypeKodebeskrivelse">"Reguleringsplan"</param>
        /// <returns></returns>
        public NabovarselV4Builder GjeldendePlan(string plantypeKodeverdi, string plantypeKodebeskrivelse)
        {
            _form.gjeldendePlan = new PlanType
            {

                plantype = new KodeType
                {
                    kodeverdi = plantypeKodeverdi,//RP
                    kodebeskrivelse = plantypeKodebeskrivelse //"Reguleringsplan"
                },
                navn = "rammebetingelser navn"
            };

            return this;
        }

        private void AddNaboeier(NaboGjenboerType naboeier, EiendomType gjelderNaboeiendom = null, EiendommensAdresseType gjelderNaboeiendomAdresse = null, MatrikkelnummerType GjelderNaboeiendomEiendomSpesificajon = null, EnkelAdresseType adresseType = null)
        {

            if (_form.naboeier == null)
            {
                naboeier = naboeier ?? new NaboGjenboerType();
                _form.naboeier = new[] { naboeier };
            }
            else
            {
                if (naboeier != null)
                {
                    var naboeiers = _form.naboeier.ToList();
                    naboeiers.Add(naboeier);
                    _form.naboeier = naboeiers.ToArray();
                }

                var index = _form.naboeier.Length - 1;
                if (gjelderNaboeiendom != null)
                {
                    _form.naboeier[index].gjelderNaboeiendom = gjelderNaboeiendom;
                }

                if (gjelderNaboeiendomAdresse != null)
                {
                    if (_form.naboeier[index].gjelderNaboeiendom == null)
                        _form.naboeier[index].gjelderNaboeiendom = new EiendomType();

                    _form.naboeier[index].gjelderNaboeiendom.adresse = gjelderNaboeiendomAdresse;
                }

                if (GjelderNaboeiendomEiendomSpesificajon != null)
                {
                    if (_form.naboeier[index].gjelderNaboeiendom == null)
                        _form.naboeier[index].gjelderNaboeiendom = new EiendomType();

                    _form.naboeier[index].gjelderNaboeiendom.eiendomsidentifikasjon = GjelderNaboeiendomEiendomSpesificajon;
                }
                if (adresseType != null)
                {
                    _form.naboeier[index].adresse = adresseType;
                }
            }
        }
        private void AddbeskrivelseAvTiltak(TiltakType tiltak)
        {

            if (_form.beskrivelseAvTiltak == null)
            {
                tiltak = tiltak ?? new TiltakType();
                _form.beskrivelseAvTiltak = new[] { tiltak };
            }
            else
            {
                var tiltaks = _form.beskrivelseAvTiltak.ToList();
                tiltaks.Add(tiltak);
                _form.beskrivelseAvTiltak = tiltaks.ToArray();

            }
        }

        private void AddEiendomToFrom(EiendomType eiendomByggested)
        {
            if (_form.eiendomByggested == null)
            {
                _form.eiendomByggested = new[] { eiendomByggested };
            }
            else
            {
                var eiendomTypes = _form.eiendomByggested.ToList();
                eiendomTypes.Add(eiendomByggested);
                _form.eiendomByggested = eiendomTypes.ToArray();
            }
        }

        //TODO get a beter solution to this methods
        internal NabovarselV4Builder EiendomByggested(SampleEiendomData.Eiendom eiendom)
        {
            EiendomByggested(eiendom.EiendomsIdentifikasjon.Kommunenummer, eiendom.EiendomsIdentifikasjon.Gaardsnummer, eiendom.EiendomsIdentifikasjon.Bruksnummer, eiendom.EiendomsIdentifikasjon.Festenummer, eiendom.EiendomsIdentifikasjon.Seksjonsnummer,
                eiendom.Kommunenavn, eiendom.Bolignummer, eiendom.Bygningsnummer, eiendom.Adresse.adresselinje1, eiendom.Adresse.gatenavn, eiendom.Adresse.husnr, eiendom.Adresse.postnr);

            return this;
        }

        internal NabovarselV4Builder NaboEiere(SampleNaboData.Naboen naboen)
        {
            NaboEiere(naboen.Nabo.navn, naboen.Nabo.partsTypeKodeverdi, naboen.Nabo.foedselsnummer, naboen.Nabo.organisasjonsnummer, naboen.Nabo.Adresse.adresselinje1, naboen.Nabo.Adresse.postnr, naboen.Nabo.Adresse.poststed, naboen.Nabo.Adresse.landkode);
            AddGjelderNaboeiendom(naboen.GjelderNaboenEiendom.Bygningsnummer, naboen.GjelderNaboenEiendom.Bolignummer, naboen.GjelderNaboenEiendom.Eier, naboen.GjelderNaboenEiendom.Kommunenavn);
            AddEiendomsIdentifikasjonToGelderNaboeiendom(naboen.GjelderNaboenEiendom.EiendomsIdentifikasjon.Kommunenummer, naboen.GjelderNaboenEiendom.EiendomsIdentifikasjon.Bruksnummer, naboen.GjelderNaboenEiendom.EiendomsIdentifikasjon.Festenummer,
                naboen.GjelderNaboenEiendom.EiendomsIdentifikasjon.Gaardsnummer, naboen.GjelderNaboenEiendom.EiendomsIdentifikasjon.Seksjonsnummer);
            AddAddressToGjelderNaboEiendom(naboen.GjelderNaboenEiendom.Adresse.adresselinje1, naboen.GjelderNaboenEiendom.Adresse.gatenavn, naboen.GjelderNaboenEiendom.Adresse.husnr, naboen.GjelderNaboenEiendom.Adresse.bokstav, naboen.GjelderNaboenEiendom.Adresse.postnr,
                naboen.GjelderNaboenEiendom.Adresse.poststed, naboen.GjelderNaboenEiendom.Adresse.landkode);
            return this;
        }

        internal NabovarselV4Builder Tiltakshaver(SampleAktoerData.Aktoer tiltakshaver)
        {
            Tiltakshaver(tiltakshaver.organisasjonsnummer, tiltakshaver.foedselsnummer, tiltakshaver.partsTypeKodeverdi, tiltakshaver.partsTypeKodebeskrivelse, tiltakshaver.kontaktperson?.navn, tiltakshaver.navn,
            tiltakshaver.Adresse.adresselinje1, tiltakshaver.Adresse.postnr, tiltakshaver.Adresse.landkode, tiltakshaver.Adresse.poststed, tiltakshaver.telefonnummer, tiltakshaver.mobilnummer, tiltakshaver.Epost);

            return this;
        }
        internal NabovarselV4Builder AnsvarligSoeker(SampleAktoerData.Aktoer ansvarligAktoer)
        {
            AnsvarligSoeker(ansvarligAktoer.organisasjonsnummer, ansvarligAktoer.foedselsnummer, ansvarligAktoer.partsTypeKodeverdi, ansvarligAktoer.partsTypeKodebeskrivelse, ansvarligAktoer.kontaktperson?.navn, ansvarligAktoer.navn,
                ansvarligAktoer.Adresse.adresselinje1, ansvarligAktoer.Adresse.postnr, ansvarligAktoer.Adresse.landkode, ansvarligAktoer.Adresse.poststed, ansvarligAktoer.telefonnummer, ansvarligAktoer.mobilnummer, ansvarligAktoer.Epost);
            return this;
        }
    }
}
