﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.SampleData;
using no.kxml.skjema.dibk.midlertidigbrukstillatelseV2;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.Builders
{
    class MidlertidigBrukstillatelseV2Builder
    {
        private readonly MidlertidigBrukstillatelseType _form;

        public MidlertidigBrukstillatelseV2Builder()
        {
            _form = new MidlertidigBrukstillatelseType();
        }
        public MidlertidigBrukstillatelseType Build()
        {
            return _form;
        }

        public MidlertidigBrukstillatelseV2Builder Datoferdigattest(DateTime? datoFerdigattest)
        {
            if (datoFerdigattest.HasValue)
            {
                _form.datoFerdigattest = datoFerdigattest.Value;
            }
            return this;
        }

        public MidlertidigBrukstillatelseV2Builder SoknadsDelerAvTiltaken(bool gjelderHeleTiltaket, string delAvTiltaket = null, string gjenstaaendeUtenfor = null, string typeArbeider = null)
        {
            _form.gjelderHeleTiltaket = gjelderHeleTiltaket;
            _form.gjenstaaendeUtenfor = gjenstaaendeUtenfor;
            _form.delAvTiltaket = delAvTiltaket;
            _form.typeArbeider = typeArbeider;
            return this;
        }

        public MidlertidigBrukstillatelseV2Builder HarTilstrekkeligSikkerhet(bool harTilstrekkeligSikkerhet, string typeArbeider, DateTime? utfoertInnen = null, DateTime? bekreftelseInnen = null)
        {
            _form.harTilstrekkeligSikkerhetSpecified = true;
            _form.harTilstrekkeligSikkerhet = harTilstrekkeligSikkerhet;

            // Set Dato utfoertInnen
            if (utfoertInnen.HasValue)
            {
                _form.utfoertInnenSpecified = true;
                _form.utfoertInnen = utfoertInnen.Value;
            }
            else
            {
                _form.utfoertInnenSpecified = false;
            }

            // Set Dato tobekreftelseInnen

            if (bekreftelseInnen.HasValue)
            {
                _form.bekreftelseInnenSpecified = true;
                _form.bekreftelseInnen = bekreftelseInnen.Value;
            }
            else
            {
                _form.bekreftelseInnenSpecified = false;
            }

            _form.typeArbeider = typeArbeider;
            return this;
        }

        // eiendomByggested
        public MidlertidigBrukstillatelseV2Builder EiendomType(string knr, string gnr, string bnr, string fnr, string snr, string kommunenavn, string bolignummer)
        {
            _form.eiendomByggested = new[]
            {
                new EiendomType
                {
                    kommunenavn = kommunenavn,

                    eiendomsidentifikasjon =new MatrikkelnummerType
                    {
                        kommunenummer = knr,
                        gaardsnummer = gnr,
                        bruksnummer = bnr,
                        festenummer = fnr,
                        seksjonsnummer = snr
                    },
                    adresse = new EiendommensAdresseType()
                    {
                        adresselinje1 = "Nygate 33",
                        postnr = "3825",
                        poststed = "Lunde"
                    },
                    bygningsnummer = "123456789",
                    bolignummer = bolignummer
                }
            };
            return this;
        }

        public MidlertidigBrukstillatelseV2Builder KommunensSaksnummer(string saksaar, string sakssekvensnummer)
        {
            if (!string.IsNullOrEmpty(saksaar) || !string.IsNullOrEmpty(sakssekvensnummer))
            {
                _form.kommunensSaksnummer = new SaksnummerType()
                {
                    saksaar = saksaar,
                    sakssekvensnummer = sakssekvensnummer
                };
            }
            return this;
        }

        public MidlertidigBrukstillatelseV2Builder Tiltakshaver(string organisasjonsnummer, string foedselsnummer, string partsTypeKodeverdi = null, string kontaktperson = null, string navn = null, string adressLine = null, string postnr = null, string landkode = null
            , string poststed = null, string telefonnummer = null, string mobilnummer = null)
        {
            _form.tiltakshaver = new PartType();

            if (!string.IsNullOrEmpty(partsTypeKodeverdi))
            {
                _form.tiltakshaver.partstype = new KodeType
                {
                    kodeverdi = partsTypeKodeverdi,
                    kodebeskrivelse = ""
                };
            }
            _form.tiltakshaver.foedselsnummer = foedselsnummer;
            _form.tiltakshaver.organisasjonsnummer = organisasjonsnummer;
            _form.tiltakshaver.navn = navn;
            if (!string.IsNullOrEmpty(adressLine) || !string.IsNullOrEmpty(postnr) || !string.IsNullOrEmpty(landkode) || !string.IsNullOrEmpty(poststed))
            {
                _form.tiltakshaver.adresse = new EnkelAdresseType
                {
                    adresselinje1 = adressLine,
                    postnr = postnr,
                    poststed = poststed,
                    landkode = landkode
                };
            }
            _form.tiltakshaver.telefonnummer = telefonnummer;
            _form.tiltakshaver.mobilnummer = mobilnummer;
            _form.tiltakshaver.kontaktperson = kontaktperson;

            return this;
        }
        public MidlertidigBrukstillatelseV2Builder AnsvarligSoeker(string organisasjonsnummer, string foedselsnummer, string partsTypeKodeverdi = null, string kontaktperson = null, string navn = null, string adressLine = null, string postnr = null, string landkode = null
            , string poststed = null, string telefonnummer = null, string mobilnummer = null)
        {
            _form.ansvarligSoeker = new PartType();

            if (!string.IsNullOrEmpty(partsTypeKodeverdi))
            {
                _form.ansvarligSoeker.partstype = new KodeType
                {
                    kodeverdi = partsTypeKodeverdi,
                    kodebeskrivelse = ""
                };
            }

            _form.ansvarligSoeker.navn = navn;

            if (!string.IsNullOrEmpty(adressLine) || !string.IsNullOrEmpty(postnr) || !string.IsNullOrEmpty(landkode) || !string.IsNullOrEmpty(poststed))
            {
                _form.ansvarligSoeker.adresse = new EnkelAdresseType
                {
                    adresselinje1 = adressLine,
                    postnr = postnr,
                    landkode = landkode,
                    poststed = poststed
                };
            }

            _form.ansvarligSoeker.organisasjonsnummer = organisasjonsnummer; //"974760673" - 974760673 BRREG, Altinn brukerservice
            _form.ansvarligSoeker.kontaktperson = kontaktperson;
            _form.ansvarligSoeker.foedselsnummer = foedselsnummer;
            _form.ansvarligSoeker.telefonnummer = telefonnummer;
            _form.ansvarligSoeker.mobilnummer = mobilnummer;

            return this;
        }
        public MidlertidigBrukstillatelseV2Builder EiendomByggested(string knr = null, string gnr = null, string bnr = null, string fnr = null, string snr = null, string kommunenavn = null, string bolignummer = null, string bygningsnummer = null
            , string adresselinje1 = null, string gatenavn = null, string husnr = null, string postnr = null, string poststed = null)
        {
            var eiendomByggested = new EiendomType();
            eiendomByggested.bolignummer = bolignummer;
            eiendomByggested.kommunenavn = kommunenavn;
            eiendomByggested.eiendomsidentifikasjon = new MatrikkelnummerType()
            {
                kommunenummer = knr,
                gaardsnummer = gnr,
                bruksnummer = bnr,
                festenummer = fnr,
                seksjonsnummer = snr
            };

            eiendomByggested.bygningsnummer = bygningsnummer;
            if (!string.IsNullOrEmpty(adresselinje1) || !string.IsNullOrEmpty(gatenavn) || !string.IsNullOrEmpty(husnr))
            {
                eiendomByggested.adresse = new EiendommensAdresseType()
                {
                    adresselinje1 = adresselinje1,
                    gatenavn = gatenavn,
                    husnr = husnr,
                    postnr = postnr,
                    poststed = poststed
                };
            }

            var eiendomTypes = _form.eiendomByggested?.ToList() ?? new List<EiendomType>();
            eiendomTypes.Add(eiendomByggested);

            _form.eiendomByggested = eiendomTypes.ToArray();

            return this;
        }

        internal MidlertidigBrukstillatelseV2Builder Tiltakshaver(SampleAktoerData.Aktoer tiltakshaver)
        {
            Tiltakshaver(tiltakshaver.organisasjonsnummer, tiltakshaver.foedselsnummer, tiltakshaver.partsTypeKodeverdi,
                tiltakshaver.kontaktperson?.navn, tiltakshaver.navn, tiltakshaver.Adresse.adresselinje1
                , tiltakshaver.Adresse.postnr, tiltakshaver.Adresse.landkode, tiltakshaver.Adresse.poststed, tiltakshaver.telefonnummer, tiltakshaver.mobilnummer);
            return this;
        }
        internal MidlertidigBrukstillatelseV2Builder AnsvarligSoeker(SampleAktoerData.Aktoer ansvarligAktoer)
        {
            AnsvarligSoeker(ansvarligAktoer.organisasjonsnummer, ansvarligAktoer.foedselsnummer, ansvarligAktoer.partsTypeKodeverdi, ansvarligAktoer.kontaktperson?.navn, ansvarligAktoer.navn, ansvarligAktoer.Adresse.adresselinje1
                , ansvarligAktoer.Adresse.postnr, ansvarligAktoer.Adresse.landkode, ansvarligAktoer.Adresse.poststed, ansvarligAktoer.telefonnummer, ansvarligAktoer.mobilnummer);
            return this;
        }

        internal MidlertidigBrukstillatelseV2Builder EiendomByggested(SampleEiendomData.Eiendom eiendom)
        {
            EiendomByggested(eiendom.EiendomsIdentifikasjon.Kommunenummer, eiendom.EiendomsIdentifikasjon.Gaardsnummer, eiendom.EiendomsIdentifikasjon.Bruksnummer, eiendom.EiendomsIdentifikasjon.Festenummer, eiendom.EiendomsIdentifikasjon.Seksjonsnummer,
                eiendom.Kommunenavn, eiendom.Bolignummer, eiendom.Bygningsnummer, eiendom.Adresse.adresselinje1, eiendom.Adresse.gatenavn, eiendom.Adresse.husnr, eiendom.Adresse.postnr, eiendom.Adresse.poststed);
            return this;
        }
    }
}
