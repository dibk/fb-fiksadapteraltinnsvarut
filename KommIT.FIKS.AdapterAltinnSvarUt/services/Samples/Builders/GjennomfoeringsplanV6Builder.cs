﻿using KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.SampleData;
using no.kxml.skjema.dibk.gjennomforingsplanV6;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.Builders
{
    public class GjennomfoeringsplanV6Builder
    {
        private readonly GjennomfoeringsplanType _form;


        public GjennomfoeringsplanV6Builder()
        {
            _form = new GjennomfoeringsplanType();
        }

        public GjennomfoeringsplanType Build()
        {
            return _form;
        }

        internal GjennomfoeringsplanV6Builder GeneralInfo(string versjon = null)
        {
            _form.versjon = versjon;
            return this;
        }

        internal GjennomfoeringsplanV6Builder EiendomByggested(string knr = null, string gnr = null, string bnr = null, string fnr = null, string snr = null, string kommunenavn = null, string bolignummer = null, string bygningsnummer = null
            , string adresselinje1 = null, string gatenavn = null, string husnr = null, string postnr = null, string poststed = null)
        {
            var eiendomByggested = new EiendomType();
            eiendomByggested.bolignummer = bolignummer;
            eiendomByggested.kommunenavn = kommunenavn;
            eiendomByggested.eiendomsidentifikasjon = new MatrikkelnummerType()
            {
                kommunenummer = knr,
                gaardsnummer = gnr,
                bruksnummer = bnr,
                festenummer = fnr,
                seksjonsnummer = snr
            };

            eiendomByggested.bygningsnummer = bygningsnummer;
            if (!string.IsNullOrEmpty(adresselinje1) || !string.IsNullOrEmpty(gatenavn) || !string.IsNullOrEmpty(husnr))
            {
                eiendomByggested.adresse = new EiendommensAdresseType()
                {
                    adresselinje1 = adresselinje1,
                    gatenavn = gatenavn,
                    husnr = husnr,
                    postnr = postnr,
                    poststed = poststed
                };
            }

            List<EiendomType> eiendomTypes;
            if (_form.eiendomByggested == null)
            {
                eiendomTypes = new List<EiendomType>() { eiendomByggested };
            }
            else
            {
                eiendomTypes = _form.eiendomByggested.ToList();
                eiendomTypes.Add(eiendomByggested);
            }
            _form.eiendomByggested = eiendomTypes.ToArray();

            return this;
        }
        //KommunensSaksnummer
        public GjennomfoeringsplanV6Builder KommunensSaksnummer(string saksaar = null, string sakssekvensnummer = null)
        {
            _form.kommunensSaksnummer = new SaksnummerType()
            {
                saksaar = saksaar,
                sakssekvensnummer = sakssekvensnummer
            };
            return this;
        }
        //Signatur
        public GjennomfoeringsplanV6Builder Signatur(DateTime? signaturdato = null, string signertAv = null, string signertPaaVegneAv = null)
        {
            _form.signatur = new SignaturType();
            _form.signatur.signaturdato = signaturdato;
            _form.signatur.signertAv = signertAv;
            _form.signatur.signertPaaVegneAv = signertPaaVegneAv;
            return this;
        }
        //ansvarligSoekerTiltaksklasse
        public GjennomfoeringsplanV6Builder AnsvarligSoekerTiltaksklasse(string kodeverdi = null, string kodebeskrivelse = null)
        {
            _form.ansvarligSoekerTiltaksklasse = new KodeType()
            {
                kodeverdi = kodeverdi,
                kodebeskrivelse = kodebeskrivelse
            };
            return this;
        }

        public GjennomfoeringsplanV6Builder Ansvarsomraade(string funksjonKodeverdi = null, string funksjonKodebeskrivelse = null, string ansvarsomraade = null, string tiltaksklasseKodeverdi = null, string tiltaksklasseKodebeskrivelse = null,
            bool? samsvarKontrollPlanlagtVedRammetillatelse = null, bool? samsvarKontrollPlanlagtVedIgangsettingstillatelse = null, bool? samsvarKontrollPlanlagtVedMidlertidigBrukstillatelse = null,
            bool? samsvarKontrollPlanlagtVedFerdigattest = null, DateTime? samsvarKontrollForeliggerVedRammetillatelse = null, DateTime? samsvarKontrollForeliggerVedIgangsettingstillatelse = null,
            DateTime? samsvarKontrollForeliggerVedMidlertidigBrukstillatelse = null, DateTime? samsvarKontrollForeliggerVedFerdigattest = null, bool? ansvarsomraadetAvsluttet = null, string erklaeringArkivreferanse = null
            , bool? erklaeringSignert = null, string sluttbrukersystemReferanse = null, string sluttbrukersystemStatus = null, DateTime? ansvarsomraadeSistEndret = null, string filnavnVedlegg = null)
        {

            var ansvarsomraadeType = new AnsvarsomraadeType()
            {
                ansvarsomraade = ansvarsomraade,
                erklaeringArkivreferanse = erklaeringArkivreferanse,
                sluttbrukersystemReferanse = sluttbrukersystemReferanse,
                sluttbrukersystemStatus = sluttbrukersystemStatus,
                filnavnVedlegg = filnavnVedlegg
            };

            if (!string.IsNullOrEmpty(funksjonKodeverdi) || !string.IsNullOrEmpty(funksjonKodebeskrivelse))
            {
                ansvarsomraadeType.funksjon = new KodeType()
                {
                    kodeverdi = funksjonKodeverdi,
                    kodebeskrivelse = funksjonKodebeskrivelse
                };
            }
            if (!string.IsNullOrEmpty(tiltaksklasseKodeverdi) || !string.IsNullOrEmpty(tiltaksklasseKodebeskrivelse))
            {
                ansvarsomraadeType.tiltaksklasse = new KodeType()
                {
                    kodeverdi = tiltaksklasseKodeverdi,
                    kodebeskrivelse = tiltaksklasseKodebeskrivelse
                };
            }

            if (samsvarKontrollPlanlagtVedRammetillatelse.HasValue)
            {
                ansvarsomraadeType.samsvarKontrollPlanlagtVedRammetillatelseSpecified = true;
                ansvarsomraadeType.samsvarKontrollPlanlagtVedRammetillatelse = samsvarKontrollPlanlagtVedRammetillatelse.Value;
            }
            if (samsvarKontrollPlanlagtVedIgangsettingstillatelse.HasValue)
            {
                ansvarsomraadeType.samsvarKontrollPlanlagtVedIgangsettingstillatelseSpecified = true;
                ansvarsomraadeType.samsvarKontrollPlanlagtVedIgangsettingstillatelse = samsvarKontrollPlanlagtVedIgangsettingstillatelse.Value;
            }
            if (samsvarKontrollPlanlagtVedMidlertidigBrukstillatelse.HasValue)
            {
                ansvarsomraadeType.samsvarKontrollPlanlagtVedMidlertidigBrukstillatelseSpecified = true;
                ansvarsomraadeType.samsvarKontrollPlanlagtVedMidlertidigBrukstillatelse = samsvarKontrollPlanlagtVedMidlertidigBrukstillatelse.Value;
            }
            if (samsvarKontrollPlanlagtVedFerdigattest.HasValue)
            {
                ansvarsomraadeType.samsvarKontrollPlanlagtVedMidlertidigBrukstillatelseSpecified = true;
                ansvarsomraadeType.samsvarKontrollPlanlagtVedFerdigattest = samsvarKontrollPlanlagtVedFerdigattest.Value;
            }
            if (samsvarKontrollForeliggerVedRammetillatelse.HasValue)
            {
                ansvarsomraadeType.samsvarKontrollForeliggerVedRammetillatelseSpecified = true;
                ansvarsomraadeType.samsvarKontrollForeliggerVedRammetillatelse = samsvarKontrollForeliggerVedRammetillatelse.Value;
            }
            if (samsvarKontrollForeliggerVedIgangsettingstillatelse.HasValue)
            {
                ansvarsomraadeType.samsvarKontrollForeliggerVedIgangsettingstillatelseSpecified = true;
                ansvarsomraadeType.samsvarKontrollForeliggerVedIgangsettingstillatelse = samsvarKontrollForeliggerVedIgangsettingstillatelse.Value;
            }
            if (samsvarKontrollForeliggerVedMidlertidigBrukstillatelse.HasValue)
            {
                ansvarsomraadeType.samsvarKontrollForeliggerVedMidlertidigBrukstillatelseSpecified = true;
                ansvarsomraadeType.samsvarKontrollForeliggerVedMidlertidigBrukstillatelse = samsvarKontrollForeliggerVedMidlertidigBrukstillatelse.Value;
            }
            if (samsvarKontrollForeliggerVedFerdigattest.HasValue)
            {
                ansvarsomraadeType.samsvarKontrollForeliggerVedFerdigattestSpecified = true;
                ansvarsomraadeType.samsvarKontrollForeliggerVedFerdigattest = samsvarKontrollForeliggerVedFerdigattest.Value;
            }
            if (ansvarsomraadetAvsluttet.HasValue)
            {
                ansvarsomraadeType.ansvarsomraadetAvsluttetSpecified = true;
                ansvarsomraadeType.ansvarsomraadetAvsluttet = ansvarsomraadetAvsluttet.Value;
            }
            if (erklaeringSignert.HasValue)
            {
                ansvarsomraadeType.erklaeringSignertSpecified = true;
                ansvarsomraadeType.erklaeringSignert = erklaeringSignert.Value;
            }
            if (ansvarsomraadeSistEndret.HasValue)
            {
                ansvarsomraadeType.ansvarsomraadeSistEndretSpecified = true;
                ansvarsomraadeType.ansvarsomraadeSistEndret = ansvarsomraadeSistEndret.Value;
            }


            var gjennomfoeringsplans = _form.gjennomfoeringsplan?.ToList() ?? new List<AnsvarsomraadeType>();
            gjennomfoeringsplans.Add(ansvarsomraadeType);
            _form.gjennomfoeringsplan = gjennomfoeringsplans.ToArray();

            return this;
        }
        //Foretak
        public GjennomfoeringsplanV6Builder Foretak(string organisasjonsnummer, string foedselsnummer, string partsTypeKodeverdi = null, string partsTypekodebeskrivelse = null, string navn = null, string adressLine = null, string postnr = null, string landkode = null
    , string poststed = null, string telefonnummer = null, string mobilnummer = null, string epost = null, string kontaktpersonNav = null, string kontaktpersonTel = null, string kontaktpersonEpost = null, bool? harSentralGodkjenning = null)
        {



            var foretak = new ForetakType();

            if (!string.IsNullOrEmpty(partsTypeKodeverdi) || !string.IsNullOrEmpty(partsTypekodebeskrivelse))
            {
                foretak.partstype = new KodeType
                {
                    kodeverdi = partsTypeKodeverdi,
                    kodebeskrivelse = partsTypekodebeskrivelse
                };
            }
            foretak.foedselsnummer = foedselsnummer;
            foretak.organisasjonsnummer = organisasjonsnummer;
            foretak.navn = navn;
            if (!string.IsNullOrEmpty(adressLine) || !string.IsNullOrEmpty(postnr) || !string.IsNullOrEmpty(landkode) || !string.IsNullOrEmpty(poststed))
            {
                foretak.adresse = new EnkelAdresseType
                {
                    adresselinje1 = adressLine,
                    postnr = postnr,
                    poststed = poststed,
                    landkode = landkode
                };
            }
            foretak.telefonnummer = telefonnummer;
            foretak.mobilnummer = mobilnummer;
            foretak.epost = epost;

            if (!string.IsNullOrEmpty(kontaktpersonNav) || !string.IsNullOrEmpty(kontaktpersonTel) || !string.IsNullOrEmpty(kontaktpersonEpost))
            {
                foretak.kontaktperson = new KontaktpersonType()
                {
                    navn = kontaktpersonNav,
                    telefonnummer = kontaktpersonTel,
                    epost = kontaktpersonEpost
                };

            }

            if (harSentralGodkjenning.HasValue)
            {
                foretak.harSentralGodkjenningSpecified = true;
                foretak.harSentralGodkjenning = harSentralGodkjenning;
            }

            List<AnsvarsomraadeType> gjennomfoeringsplans = new List<AnsvarsomraadeType>();
            if (_form.gjennomfoeringsplan?.LastOrDefault()!= null)
            {
                gjennomfoeringsplans = _form.gjennomfoeringsplan.ToList();
                gjennomfoeringsplans.LastOrDefault().foretak = foretak;
            }
            else
            {
                gjennomfoeringsplans.Add(new AnsvarsomraadeType()
                {
                    foretak = foretak
                });
            }

            _form.gjennomfoeringsplan = gjennomfoeringsplans.ToArray();
            return this;
        }
        //Metadata
        public GjennomfoeringsplanV6Builder Metadata(string fraSluttbrukersystem = null, string ftbId = null, string prosjektnavn = null, string sluttbrukersystemUrl = null, string hovedinnsendingsnummer = null)
        {
            _form.metadata = new MetadataType()
            {
                fraSluttbrukersystem = fraSluttbrukersystem,
                ftbId = ftbId,
                prosjektnavn = prosjektnavn,
                sluttbrukersystemUrl = sluttbrukersystemUrl,
                hovedinnsendingsnummer = hovedinnsendingsnummer
            };
            return this;
        }
        // AnsvarligSoeker
        public GjennomfoeringsplanV6Builder AnsvarligSoeker(string organisasjonsnummer=null, string foedselsnummer=null, string partsTypeKodeverdi = null, string partsTypeKodebeskrivelse = null, string navn = null, string adressLine = null, string postnr = null, string landkode = null
            , string poststed = null, string telefonnummer = null, string mobilnummer = null, string epost = null, string kontaktpersonNav = null, string kontaktpersonTel = null, string kontaktpersonEpost = null)
        {
            _form.ansvarligSoeker = new PartType();

            if (!string.IsNullOrEmpty(partsTypeKodeverdi))
            {
                _form.ansvarligSoeker.partstype = new KodeType
                {
                    kodeverdi = partsTypeKodeverdi,
                    kodebeskrivelse = partsTypeKodebeskrivelse
                };
            }
            _form.ansvarligSoeker.foedselsnummer = foedselsnummer;
            _form.ansvarligSoeker.organisasjonsnummer = organisasjonsnummer;
            _form.ansvarligSoeker.navn = navn;

            if (!string.IsNullOrEmpty(adressLine) || !string.IsNullOrEmpty(postnr) || !string.IsNullOrEmpty(landkode) || !string.IsNullOrEmpty(poststed))
            {
                _form.ansvarligSoeker.adresse = new EnkelAdresseType
                {
                    adresselinje1 = adressLine,
                    postnr = postnr,
                    landkode = landkode,
                    poststed = poststed
                };
            }
            _form.ansvarligSoeker.telefonnummer = telefonnummer;
            _form.ansvarligSoeker.mobilnummer = mobilnummer;
            _form.ansvarligSoeker.epost = epost;

            if (!string.IsNullOrEmpty(kontaktpersonNav) || !string.IsNullOrEmpty(kontaktpersonTel) || !string.IsNullOrEmpty(kontaktpersonEpost))
            {
                _form.ansvarligSoeker.kontaktperson = new KontaktpersonType()
                {
                    navn = kontaktpersonNav,
                    telefonnummer = kontaktpersonTel,
                    epost = kontaktpersonEpost
                };

            }

            return this;
        }
        //Parse
        internal GjennomfoeringsplanV6Builder EiendomByggested(SampleEiendomData.Eiendom eiendom)
        {
            EiendomByggested(eiendom.EiendomsIdentifikasjon.Kommunenummer, eiendom.EiendomsIdentifikasjon.Gaardsnummer, eiendom.EiendomsIdentifikasjon.Bruksnummer, eiendom.EiendomsIdentifikasjon.Festenummer, eiendom.EiendomsIdentifikasjon.Seksjonsnummer,
                eiendom.Kommunenavn, eiendom.Bolignummer, eiendom.Bygningsnummer, eiendom.Adresse.adresselinje1, eiendom.Adresse.gatenavn, eiendom.Adresse.husnr, eiendom.Adresse.postnr, eiendom.Adresse.poststed);
            return this;
        }
        internal GjennomfoeringsplanV6Builder Foretak(SampleAktoerData.Aktoer foretak, bool? harSentralGodkjenning = null)
        {
            Foretak(foretak.organisasjonsnummer, foretak.foedselsnummer, foretak.partsTypeKodeverdi, foretak.partsTypeKodebeskrivelse, foretak.navn, foretak.Adresse.adresselinje1
                , foretak.Adresse.postnr, foretak.Adresse.landkode, foretak.Adresse.poststed, foretak.telefonnummer, foretak.mobilnummer, foretak.Epost, foretak.kontaktperson?.navn, foretak.kontaktperson?.telefonnummer, foretak.kontaktperson?.Epost, harSentralGodkjenning);
            return this;
        }
        internal GjennomfoeringsplanV6Builder AnsvarligSoeker(SampleAktoerData.Aktoer ansvarligSoeker)
        {
            AnsvarligSoeker(ansvarligSoeker.organisasjonsnummer, ansvarligSoeker.foedselsnummer, ansvarligSoeker.partsTypeKodeverdi, ansvarligSoeker.partsTypeKodebeskrivelse, ansvarligSoeker.navn, ansvarligSoeker.Adresse.adresselinje1
                , ansvarligSoeker.Adresse.postnr, ansvarligSoeker.Adresse.landkode, ansvarligSoeker.Adresse.poststed, ansvarligSoeker.telefonnummer, ansvarligSoeker.mobilnummer, ansvarligSoeker.Epost, ansvarligSoeker.kontaktperson?.navn, ansvarligSoeker.kontaktperson?.telefonnummer, ansvarligSoeker.kontaktperson?.Epost);
            return this;
        }
    }
}