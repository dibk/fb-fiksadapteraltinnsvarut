using KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.Builders;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.FormsSamples;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples
{
    public class SampleService
    {
        public List<SampleVariant> GetSampleVariants(string DataFormatID, string DataFormatVersion)
        {
            List<SampleVariant> varianter = new List<SampleVariant>();
            List<Skjema> forms = GetForms(false);
            List<Skjema> variants = forms.FindAll(f => f.DataFormatID == DataFormatID && f.DataFormatVersion == DataFormatVersion);
            foreach (var item in variants)
            {
                varianter.Add(new SampleVariant() { DataFormatId = item.DataFormatID, DataFormatVersion = item.DataFormatVersion, Variant = item.VariantId, Description = item.VariantBeskrivelse });
            }
            return varianter;
        }

        public Sample GetSample(string DataFormatID, string DataFormatVersion, string variantid, string reporteeid, string reporteeName)
        {
            List<Skjema> forms = GetForms(true);
            Skjema form = forms.Find(f => f.DataFormatID == DataFormatID && f.DataFormatVersion == DataFormatVersion && f.VariantId == variantid);
            return new Sample(form);
        }

        private List<Skjema> GetForms(bool data)
        {
            List<Skjema> forms = new List<Skjema>();

            // Tiltak uten ansvarsrett V3
            var tiltakUtenAnsvarsrettV3Dfv45753 = GetClassSkjemaMethods(new TiltakUtenAnsvarsrettV3Dfv45753(), data);
            forms.AddRange(tiltakUtenAnsvarsrettV3Dfv45753);

            // Rammetillatelse V3
            var rammeSoknadV3Dfv45752Schemas = GetClassSkjemaMethods(new RammeSoknadV3Dfv45752(), data);
            forms.AddRange(rammeSoknadV3Dfv45752Schemas);

            // Ett-trinns-søknad V3
            var ettTrinV3Skjemas = GetClassSkjemaMethods(new EttTrinnV3Dfv45751(), data);
            forms.AddRange(ettTrinV3Skjemas);
            // Endring av tillatelse
            var endringAvTillatelseDfV42350Skjemas = GetClassSkjemaMethods(new EndringAvTillatelseDfV42350(), data);
            forms.AddRange(endringAvTillatelseDfV42350Skjemas);

            // Kontrollerklaering - direkte opprettet
            var kontrollerklaeringDirekteOpprettetDfv45020Skjemas = GetClassSkjemaMethods(new KontrollerklaeringDirekteOpprettetDfv45020(), data);
            forms.AddRange(kontrollerklaeringDirekteOpprettetDfv45020Skjemas);

            //**Samsvarserklaering Direkte Opprettet
            var samsvarserklaeringDirekteOpprettetSkjemas = GetClassSkjemaMethods(new SamsvarserklaeringDirekteOpprettetDfv44169(), data);
            forms.AddRange(samsvarserklaeringDirekteOpprettetSkjemas);

            // Arbeidstilsynets samtykke
            var arbeidstilsynetsSamtykke = GetClassSkjemaMethods(new ArbeidstilsynetsSamtykkeDfv41999(), data);
            forms.AddRange(arbeidstilsynetsSamtykke);

            // Midlertidig brukstillatelse V2
            var midlertidigBrukstillatelseV2 = GetClassSkjemaMethods(new MidlertidigBrukstillatelseV2Dfv43006(), data);
            forms.AddRange(midlertidigBrukstillatelseV2);

            // Ferdigattest versjon 2
            var ferdigattestV2Skjemas = GetClassSkjemaMethods(new FerdigattestV2Dfv43192(), data);
            forms.AddRange(ferdigattestV2Skjemas);

            // Gjennomføringsplan V6
            var gjennomfoeringsplanV6Schema = GetClassSkjemaMethods(new GjennomfoeringsplanV6Dfv44096(), data);
            forms.AddRange(gjennomfoeringsplanV6Schema);

            // Supplering av søknad
            var suppleringAvSoeknad = GetClassSkjemaMethods(new SuppleringAvSoeknadDfv46471(), data);
            forms.AddRange(suppleringAvSoeknad);

            // Igangsettingstillatelse V2
            var igangsettingstillatelseV2Form = new Skjema
            {
                FormId = "5151",
                ServiceCode = 4401,
                ServiceEditionCode = 2,
                DataFormatID = "5845",
                DataFormatVersion = "43007",
                Name = "Søknad om igangsettingstillatelse V2",
                AvailableService = true,
                OpenWikiUrl = "https://dibk-utvikling.atlassian.net/wiki/pages/viewpage.action?pageId=20021280",
                SendTo = "Kommunen",
                FormType = "Hovedsøknad",
                Status = "Testmiljø",
                VariantId = "Mini",
                VariantBeskrivelse = "Tom søknad"
            };
            if (data)
            {
                igangsettingstillatelseV2Form.Data = SerializeUtil.Serialize(new IgangsettingstillatelseV2Builder().Build());
            }

            forms.Add(igangsettingstillatelseV2Form);

            // Gjennomføringsplan V4

            var gjennomforingsplanFormV4 = new Skjema
            {
                FormId = "5185",
                ServiceCode = 4398,
                ServiceEditionCode = 5,
                DataFormatID = "5786",
                DataFormatVersion = "42725",
                Name = "Gjennomføringsplan V4",
                AvailableService = true,
                OpenWikiUrl = "https://dibk-utvikling.atlassian.net/wiki/pages/viewpage.action?pageId=20021255",
                SendTo = "Kommunen",
                FormType = "Underskjema/søknad",
                Status = "Testmiljø og i produksjon",
                VariantId = "Mini",
                VariantBeskrivelse = "Tom søknad"
            };
            if (data)
            {
                gjennomforingsplanFormV4.Data = SerializeUtil.Serialize(new GjennomfoeringsplanV4Builder().Build());
            }

            forms.Add(gjennomforingsplanFormV4);

            // Erklæring om ansvarsrett direkte oppretting
            var ansvarsrettDirekteOpprettetForm = new Skjema
            {
                FormId = "5181",
                ServiceCode = 4965,
                ServiceEditionCode = 2,
                DataFormatID = "5700",
                DataFormatVersion = "42467",
                Name = "Erklæring om ansvarsrett (initiert fra ansvarlig foretak)",
                AvailableService = true,
                OpenWikiUrl = "https://dibk-utvikling.atlassian.net/wiki/pages/viewpage.action?pageId=20021262",
                SendTo = "Ansvarlig søker",
                FormType = "Erklæring/vedlegg",
                Status = "Testmiljø. I produksjon som underskjema",
                VariantId = "Mini",
                VariantBeskrivelse = "Tom søknad"
            };
            if (data)
            {
                ansvarsrettDirekteOpprettetForm.Data = SerializeUtil.Serialize(new ErklaeringAnsvarsrettV2Builder().Build());
            }

            forms.Add(ansvarsrettDirekteOpprettetForm);

            // Gjenpart nabovarsel V2
            var gjenpartnabovarselFormV2 = new Skjema
            {
                FormId = "5155*",
                ServiceCode = 4816,
                ServiceEditionCode = 2,
                DataFormatID = "5826",
                DataFormatVersion = "42895",
                Name = "Gjenpart nabovarsel V2",
                AvailableService = false,
                OpenWikiUrl = "",
                SendTo = "Kommunen",
                FormType = "Underskjema/vedlegg",
                Status = "Testmiljø",
                VariantId = "Mini",
                VariantBeskrivelse = "Tom søknad"
            };

            forms.Add(gjenpartnabovarselFormV2);

            // Svar på nabovarselV2
            var svarPaaNabovarselV2Form = new Skjema
            {
                FormId = "5156*",
                ServiceCode = 4699,
                ServiceEditionCode = 2,
                DataFormatID = "5703",
                DataFormatVersion = "42543",
                Name = "Svar på nabovarsel V2",
                AvailableService = false,
                OpenWikiUrl = "https://dibk-utvikling.atlassian.net/wiki/spaces/FB/pages/133169178/Svar+p+nabovarsel+kvittering",
                SendTo = "Ansvarlig søker/tiltakshaver",
                FormType = "Kvittering/vedlegg",
                Status = "Testmiljø",
                VariantId = "Mini",
                VariantBeskrivelse = "Tom søknad"
            };
            forms.Add(svarPaaNabovarselV2Form);

            var avfallsplan = new Skjema
            {
                FormId = "5178/5179",
                ServiceCode = 4864,
                ServiceEditionCode = 1,
                DataFormatID = "5584",
                DataFormatVersion = "42035",
                Name = "Sluttrapport med avfallsplan",
                AvailableService = false,
                TemplateUrl = "",
                WikiUrl = "",
                OpenWikiUrl = "",
                ModelUrl = "",
                SendTo = "Kommunen",
                FormType = "Underskjema/vedlegg",
                Status = "Testmiljø",
                VariantId = "Mini",
                VariantBeskrivelse = "Tom søknad"
            };

            forms.Add(avfallsplan);

            // Opphør av ansvarsrett
            var opphorAnsvarsrett = new Skjema
            {
                FormId = "5183",
                ServiceCode = 4848,
                ServiceEditionCode = 1,
                DataFormatID = "5564",
                DataFormatVersion = "42021",
                Name = "Opphør av ansvarsrett",
                AvailableService = false,
                TemplateUrl = "",
                WikiUrl = "",
                OpenWikiUrl = "",
                ModelUrl = "",
                SendTo = "Ansvarlig søker",
                FormType = "",
                Status = "Testmiljø",
                VariantId = "Mini",
                VariantBeskrivelse = "Tom søknad"
            };
            forms.Add(opphorAnsvarsrett);

            // Matrikkel
            var matrikkel = new Skjema
            {
                FormId = "5176",
                ServiceCode = 4920,
                ServiceEditionCode = 1,
                DataFormatID = "5625",
                DataFormatVersion = "42144",
                Name = "Matrikkelopplysninger",
                AvailableService = false,
                OpenWikiUrl =
                    "https://dibk-utvikling.atlassian.net/wiki/spaces/FB/pages/53415647/Matrikkelopplysninger",
                SendTo = "Kommunen",
                FormType = "Underskjema",
                Status = "Testmiljø",
                VariantId = "Adresser",
                VariantBeskrivelse = "Eksempel på eiendom med flere adresser"
            };
            if (data)
            {
                var sample = new MatrikkelopplysningerV1Builder();
                sample.EiendomType("0825", "1", "2", "0", "0", true);
                sample.AdresseType("1234", "Nygate", "33", "");

                matrikkel.Data = SerializeUtil.Serialize(sample.Build());
            }
            forms.Add(matrikkel);

            // Vedleggsopplysninger
            var vedlegg = new Skjema
            {
                FormId = "*",
                ServiceCode = 0,
                ServiceEditionCode = 0,
                DataFormatID = "5797",
                DataFormatVersion = "42813",
                Name = "Vedleggsopplysninger",
                AvailableService = false,
                OpenWikiUrl =
                    "https://dibk-utvikling.atlassian.net/wiki/spaces/FB/pages/334954497/Vedleggsopplysninger",
                SendTo = "Kommunen",
                FormType = "Underskjema",
                Status = "Testmiljø",
                VariantId = "Mini",
                VariantBeskrivelse = "Tom søknad"
            };
            if (data) vedlegg.Data = SerializeUtil.Serialize(new VedleggsopplysningerV1Builder().Build());
            forms.Add(vedlegg);

            // Vedleggsopplysninger
            var vedlegg2 = new Skjema
            {
                FormId = "*",
                ServiceCode = 0,
                ServiceEditionCode = 0,
                DataFormatID = "5797",
                DataFormatVersion = "42813",
                Name = "Vedleggsopplysninger",
                AvailableService = false,
                OpenWikiUrl =
                    "https://dibk-utvikling.atlassian.net/wiki/spaces/FB/pages/334954497/Vedleggsopplysninger",
                SendTo = "Kommunen",
                FormType = "Underskjema",
                Status = "Testmiljø",
                VariantId = "Ansvarsrett",
                VariantBeskrivelse = "Referanse til signert ansvarsrett med referanse til ansvarsområde i gjennomføringsplan"
            };
            if (data)
            {
                var sample = new VedleggsopplysningerV1Builder();
                sample.Vedlegg(null, "ErklaeringAnsvarsrett", DateTime.Now.Date, null);
                vedlegg2.Data = SerializeUtil.Serialize(sample.Build());
            }
            forms.Add(vedlegg2);

            return forms;
        }

        public static List<Skjema> GetClassSkjemaMethods(object classObject, bool data)
        {
            List<Skjema> forms = new List<Skjema>();

            if (classObject == null) return forms;

            var objectType = classObject.GetType();

            var methodInfos = objectType.GetMethods(BindingFlags.Public | BindingFlags.Instance);
            if (methodInfos.Any())
            {
                List<MethodInfo> classSkejemaMethodInfos = new List<MethodInfo>();

                // Get all methods of type "Skjema" in the class
                var skjemaMethodInfos = methodInfos.Where(m => m.ReturnType.Name == "Skjema").ToArray();
                // Get all methods of type "List<Skjema>" in the class
                var skjemaMethodInfosList = methodInfos.Where(m => m.ReturnType.Name.Contains("List")).ToArray();

                classSkejemaMethodInfos.AddRange(skjemaMethodInfos);
                classSkejemaMethodInfos.AddRange(skjemaMethodInfosList);

                if (classSkejemaMethodInfos.Any())
                {
                    foreach (var methodInfo in classSkejemaMethodInfos)
                    {
                        var skjemaList = GetListOfSkjemaFormMethodInfo(objectType, methodInfo, data);
                        if (skjemaList.Any())
                        {
                            forms.AddRange(skjemaList);
                        }
                    }
                }
            }

            var sortForms = forms.OrderBy(sk => sk.VariantId).ToList();
            return sortForms;
        }

        public static List<Skjema> GetListOfSkjemaFormMethodInfo(Type objectType, MethodInfo methodInfo, bool data)
        {
            List<Skjema> listSkjemas = new List<Skjema>();
            var parameters = new object[1] { data };
            try
            {
                var inst = Activator.CreateInstance(objectType);
                listSkjemas = methodInfo.Invoke(inst, parameters) as List<Skjema>;
                var skjema = methodInfo.Invoke(inst, parameters) as Skjema;

                if (listSkjemas == null)
                {
                    if (skjema != null)
                        listSkjemas = new List<Skjema>() { skjema };
                }
            }
            catch
            {
                //
            }
            return listSkjemas;
        }

        public static Dictionary<string, string> GetLisOfXmlTestDataFormAssamble<T>(string dfvId)
        {
            Dictionary<string, string> xmlTestDataDictionary = new Dictionary<string, string>();

            //var resourcePaths = AppDomain.CurrentDomain.GetAssemblies()
            //    .Where(a => a.FullName.ToLower().StartsWith("kommit.fiks") && !a.FullName.ToLower().Contains(".test"));
            //var resourcesNames = resourcePaths?.FirstOrDefault()?.GetManifestResourceNames()?.Where(r => r.Contains(dfvId))?.ToList();

            var assembly = Assembly.GetExecutingAssembly();

            //select all embedded resources that contain "DataFormatVersion"
            var resourcePathList = assembly.GetManifestResourceNames()
                .Where(str => str.Contains(dfvId))
                .ToArray();

            if (resourcePathList.Any())
            {
                for (int i = 0; i < resourcePathList.Length; i++)
                {
                    var extension = Path.GetExtension(resourcePathList[i]);

                    if (extension == ".xml")
                    {
                        try
                        {
                            using (Stream stream = assembly.GetManifestResourceStream(resourcePathList[i]))
                            using (StreamReader reader = new StreamReader(stream))
                            {
                                var filePath = Path.GetFileNameWithoutExtension(resourcePathList[i]);
                                var fileName = filePath.Split('.')?.Last();

                                var textFile = reader.ReadToEnd();

                                //deserialize xml
                                var deserializeObject = SerializeUtil.DeserializeFromString<T>(textFile);

                                //check if xml is can be deserialize to Type
                                if (deserializeObject != null && deserializeObject.GetType() == typeof(T))
                                {
                                    if (xmlTestDataDictionary.ContainsKey(fileName))
                                    {
                                        fileName = $"{fileName}_{i}";
                                    }

                                    var xmlData = SerializeUtil.Serialize(deserializeObject);
                                    xmlTestDataDictionary.Add(fileName, xmlData);
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            //
                        }
                    }
                }
            }
            return xmlTestDataDictionary;
        }
    }
}