﻿using System;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.Builders;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.SampleData;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.FormsSamples
{
    public class GjennomfoeringsplanV6Dfv44096
    {
        private readonly Skjema _skjema = new Skjema()
        {
            FormId = "5185",
            ServiceCode = 4398,
            ServiceEditionCode = 4,
            DataFormatID = "6146",
            DataFormatVersion = "44096",
            Name = "Gjennomføringsplan V6",
            AvailableService = true,
            OpenWikiUrl = "https://dibk-utvikling.atlassian.net/wiki/pages/viewpage.action?pageId=20021255",
            SendTo = "Kommunen",
            FormType = "Underskjema/søknad",
            Status = "Testmiljø",
        };

        public Skjema _01Gjennomfoeringsplan(bool data)
        {

            var skjema = _skjema;

            skjema.VariantBeskrivelse = "Gjennomfoeringsplan v6 for testing";
            skjema.VariantId = "01_GjennomfoeringsplanV6_Test";

            if (data)
            {
                var gjennomfoeringsplanType = new GjennomfoeringsplanV6Builder()
                    .EiendomByggested(new SampleEiendomData().FrongTestKommune)
                    .KommunensSaksnummer("2020", "12345")
                    .Signatur(new DateTime(2020, 07, 02), "signert av", "signart på vegen av")
                    .AnsvarligSoekerTiltaksklasse("1","1")
                    .Ansvarsomraade("PRO", "Ansvarlig prosjektering", "Arkitektur prosjektering av bygg", "1", "1", true, false,
                        false, false, new DateTime(2020, 07, 06), new DateTime(2020, 07, 06), new DateTime(2020, 07, 06), new DateTime(2020, 07, 06)
                        , true, "AR132456", true, "NN123548", "status", new DateTime(2020, 07, 06), "vedlegg navn")
                    .Foretak(new SampleAktoerData().PrivatAktorLudvigMoholt, true)
                    .Foretak(new SampleAktoerData().PrivatAktorLudvigMoholt, true)
                    .Metadata("Test motor", "123456", "Gjennomfoeringsplan v6 for testing", "www.arkitektum.no", "12345")
                    .AnsvarligSoeker(new SampleAktoerData().EnheterArkitektFlink)
                    .GeneralInfo("6.0")
                    .Build();
                skjema.Data = SerializeUtil.Serialize(gjennomfoeringsplanType);

            }
            
            return skjema;
        }
    }
}