﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.Builders;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.SampleData;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;
using no.kxml.skjema.dibk.ferdigattestV2;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.FormsSamples
{
    public class FerdigattestV2Dfv43192
    {
        private Skjema _skjema = new Skjema
        {
            FormId = "5167",
            ServiceCode = 4400,
            ServiceEditionCode = 2,
            DataFormatID = "5855",
            DataFormatVersion = "43192",
            Name = "Søknad om ferdigattest V2",
            AvailableService = true,
            OpenWikiUrl = "https://dibk-utvikling.atlassian.net/wiki/pages/viewpage.action?pageId=23691266",
            SendTo = "Kommunen",
            FormType = "Hovedsøknad",
            Status = "Under utvikling",
        };
        public Skjema FerdigattestV2Mini(bool data)
        {
            Skjema skjema = _skjema;

            skjema.VariantId = "01_Mini";
            skjema.VariantBeskrivelse = "Tom søknad";

            if (data)
            {
                skjema.Data = SerializeUtil.Serialize(new FerdigattestV2Builder().Build());
            }
            return skjema;
        }

        public Skjema FerdigattestV2_02(bool data)
        {
            Skjema skjema = _skjema;

            skjema.VariantId = "01_FerdigattestV2";
            skjema.VariantBeskrivelse = "01 - Søknad om ferdigattest V2";

            if (data)
            {
                var form = new FerdigattestV2Builder()
                    .Tiltakshaver(new SampleAktoerData().PrivatAktorLudvigMoholt)
                    .AnsvarligSoeker(new SampleAktoerData().EnheterArkitektFlink)
                    .EiendomByggested(new SampleEiendomData().FrongTestKommune)
                    .FraSluttbrukersystem("Fellestjenester bygg testmotor")
                    .HarTilstrekkeligSikkerhet(false, "Montert rekkverk på trapp", DateTime.Now.AddDays(10), DateTime.Now.AddDays(10))
                    .KommunensSaksnummer("2015", "124340")
                    .ForetattIkkeSoeknadspliktigeJusteringer(true)
                    .Build();
                skjema.Data = SerializeUtil.Serialize(form);
            }
            return skjema;
        }
        public List<Skjema> FerdigattestXmlFiles(bool data)
        {
            List<Skjema> listSkjemas = new List<Skjema>();
            var xmlTestExamples = SampleService.GetLisOfXmlTestDataFormAssamble<FerdigattestType>("43192");

            if (xmlTestExamples.Any())
            {
                for (int index = 0; index < xmlTestExamples.Count; index++)
                {
                    Skjema skjema = new Skjema()
                    {
                        FormId = _skjema.FormId,
                        ServiceCode = _skjema.ServiceCode,
                        ServiceEditionCode = _skjema.ServiceEditionCode,
                        DataFormatID = _skjema.DataFormatID,
                        DataFormatVersion = _skjema.DataFormatVersion,
                        Name = _skjema.Name,
                        AvailableService = _skjema.AvailableService,
                        OpenWikiUrl = _skjema.OpenWikiUrl,
                        SendTo = _skjema.SendTo,
                        FormType = _skjema.FormType,
                        Status = _skjema.Status
                    };

                    skjema.VariantBeskrivelse = FormSamplesServices.GetDescriptionFormFileName(xmlTestExamples.ElementAt(index).Key);
                    skjema.VariantId = $"{xmlTestExamples.ElementAt(index).Key}";
                    if (data)
                    {
                        skjema.Data = xmlTestExamples.ElementAt(index).Value;
                    }
                    listSkjemas.Add(skjema);
                }
            }

            return listSkjemas;
        }
    }
}