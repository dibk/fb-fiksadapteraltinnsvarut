﻿using System;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.Builders;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.SampleData;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.FormsSamples
{
    public class TiltakUtenAnsvarsrettV3Dfv45753
    {
        private readonly Skjema _skjema = new Skjema()
        {
            FormId = "5153",
            ServiceCode = 4373,
            ServiceEditionCode = 3,
            DataFormatID = "6742",
            DataFormatVersion = "45753",
            Name = "Søknad om tillatelse til tiltak uten ansvarsrett v3",
            AvailableService = true,
            OpenWikiUrl = "https://dibk-utvikling.atlassian.net/wiki/pages/viewpage.action?pageId=31129618",
            SendTo = "Kommunen",
            FormType = "Hovedsøknad",
            Status = "Under utvikling",

        };

        public Skjema _01TiltakUtenAnsvarsrett(bool data)
        {

            Skjema skjema = _skjema;

            skjema.VariantBeskrivelse = "TiltakUtenAnsvarsrett søknad uten ansvar - bruksendring1";
            skjema.VariantId = "01_TiltakUtenAnsvarsrettV3_Test";

            if (data)
            {
                var tiltakUtenAnsvarsrett = new TiltakUtenAnsvarsrettV3Builder()
                    .EiendomByggested(new SampleEiendomData().FrongTestKommune)
                    .BeskrivelseAvTiltak("foelgeBrev", "nyttbyggunder70m2", "Nytt bygg - Under 70 m2 - ikke boligformål", "Eksempelbeskrivelse")
                    .BeskrivelseAvTiltakBruk("X", "Bolig", "111", "Enebolig", "Bolig", "Bolig", "Eksempelbeskrivelse av planlagt formål")
                    .Tiltakshaver(new SampleAktoerData().EnheterSaebovaagenOgLongyearbyen)
                    .Dispensasjon("TEK", "Byggteknisk forskrift med veiledning", "Klarer ikke å oppfylle energikravene til nytt kjellerrom", "Det søkes om disp fra energikravene i TEK for nytt treningsrom i kjeller")
                    .KommunensSaksnummer("2020", "12345")
                    .Varsling(true, "0", false, "Ingen merknader", "www.Dibk.no", "Hilde")
                    .RammebetingelserAdkomst(false, "KommunalVeg", "KommunalVeg", null)
                    .RammebetingelserArealdisponering(1000.5,30.2,50.3,1020,300.2,200.4,
                        10.6,30.5,18,240.5,23.36)
                    .RammebetingerlserGenerelleVilkaar(false, false, false, true)
                    .RammebetingelserPlanGeldendePlan(23.37, "RP", "Reguleringsplan", "Eksemplelplannavn", "Byggeområde for boligbebyggelse", "%BYA", "Bebygd areal", "Områdereguleringsplan")
                    .RammebetingelserPlanAndrePlaner("34", "Områderegulering")
                    .RammebetingelserPlanAndrePlaner("35", "Detaljregulering")
                    .RammebetingelserVannforsyningType("AnnenPrivatIkkeInnlagt", "AnnenPrivatIkkeInnlagt", "Felles privat vannforsyning i hele reguleringsområdet", false, true)
                    .RammebetingelserAvloep("PrivatKloakk", "Privat Kloakk", true, true, false, false, true, false)
                    .RammebetingelserKravTilByggegrunn(false, false, false)
                    .RammebetingelserPlassering(false, 5.2, false, 10.3, 25.7, true)
                    .Signatur(new DateTime(2020, 07, 02), "signert av", "signart på vegen av")
                    .Metadata("Test motor", "123456", "Gjennomfoeringsplan v6 for testing", "www.arkitektum.no")
                    .Build();

                skjema.Data = SerializeUtil.Serialize(tiltakUtenAnsvarsrett);

            }

            return skjema;
        }
    }
}