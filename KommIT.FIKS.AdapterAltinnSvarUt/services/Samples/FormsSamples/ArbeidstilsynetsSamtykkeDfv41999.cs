﻿using KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.Builders;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.SampleData;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.FormsSamples
{
    public class ArbeidstilsynetsSamtykkeDfv41999
    {
        private readonly Skjema _skjema = new Skjema()
        {
            FormId = "5177",
            ServiceCode = 4845,
            ServiceEditionCode = 1,
            DataFormatID = "5547",
            DataFormatVersion = "41999",
            Name = "Søknad om Arbeidstilsynets samtykke",
            AvailableService = false,
            TemplateUrl = "",
            WikiUrl = "",
            OpenWikiUrl = "https://dibk-utvikling.atlassian.net/wiki/pages/viewpage.action?pageId=52269827",
            ModelUrl = "",
            SendTo = "Arbeidstilsynet",
            FormType = "Underskjema/Søknad",
            Status = "Testmiljø",
        };

        public Skjema _01ArbeidstilsynetsSamtykke(bool data)
        {
            Skjema skjema = _skjema;
            skjema.VariantBeskrivelse = "Søknad om Arbeidstilsynets samtykke Example data 1";
            skjema.VariantId = "01-eksempelData";

            if (data)
            {
                var beskrivelse = "Søknaden gjelder oppføring av nytt kombinert næringsbygg, bygget i to plan, hvorav plan 2 kun består av et teknisk rom. Bygningen er planlagt brukt av Xylem til kontor/ administrasjon og lager,samt et verksted for reparasjon av punper og et sveise - monteringsverksted." +
                                  "Arbeidsplasser: Eksisterende: 8 stk, Faste nå:     8 stk, Fremtidige:   3 stk, Totalt:      11 stk, ";
 
                 var arbeidstilsynetsType = new ArbeidstilsynetsSamtykkeBuilder()
                        .ProsjektInfo("Elveseltta Eiendom AS", "202012345", "202054321", "2020", "123456789", "2020")
                        .EiendomByggested(new SampleEiendomData().FrongTestKommune)
                        .Tiltakshaver(new SampleAktoerData().EnheterArbeidstilsyneBlomsterdalenOgOvreSnertingdal)
                        .Arbeidsplasser(true,true,true,false,true,"1","11", beskrivelse)
                        .BeskrivelseAvTilta("211", "Fabrikkbygning")//Mangler i eksampler data
                        .FraSluttbrukersystemErUtfylt("Testmotor") //Mangler
                        .Fakturamottaker(true, false,"123456789","BARB_126","FaRef_1258","NoenSaamTrengeFakture AS", "202012345","StorGate 126","3800","Bø i Telemark","NO") // Mangler
                        .Build();
                 skjema.Data = SerializeUtil.Serialize(arbeidstilsynetsType);
            }

            return skjema;
        }
        public Skjema _02ArbeidstilsynetsSamtykke(bool data)
        {
            Skjema skjema = _skjema;
            skjema.VariantBeskrivelse = "Søknad om Arbeidstilsynets samtykke Example data 2";
            skjema.VariantId = "02-eksempelData";

            if (data)
            {
                 var arbeidstilsynetsType = new ArbeidstilsynetsSamtykkeBuilder()
                        .ProsjektInfo("KS Torggata Bad AS", "202012345", "202054321", "2020", "123456789", "2020")
                        .EiendomByggested(new SampleEiendomData().OperaHouseOslo)
                        .Tiltakshaver(new SampleAktoerData().EnheterArbeidstilsyneBlomsterdalenOgOvreSnertingdal)
                        .Arbeidsplasser(false,true,false,true,true,"2","88", "Ordinær kontordrift.")
                        .BeskrivelseAvTilta("311", "Kontor- og administrasjonsbygning eller rådhus")//Mangler i eksampler data
                        .FraSluttbrukersystemErUtfylt("Testmotor") //Mangler
                        .Fakturamottaker(false, true, "123456789", "BARB_126", "FaRef_1258", "NoenSaamTrengeFakture AS", "202012345", "StorGate 126", "3800", "Bø i Telemark", "NO") // Mangler

                        .Build();
                 skjema.Data = SerializeUtil.Serialize(arbeidstilsynetsType);
            }

            return skjema;
        }
        public Skjema _03ArbeidstilsynetsSamtykke(bool data)
        {
            Skjema skjema = _skjema;
            skjema.VariantBeskrivelse = "Søknad om Arbeidstilsynets samtykke Example data 3";
            skjema.VariantId = "03-eksempelData";

            if (data)
            {
                 var arbeidstilsynetsType = new ArbeidstilsynetsSamtykkeBuilder()
                     .ProsjektInfo("Nammo Raufoss AS", "202012345", "202054321","2020", "123456789","2020")
                     .EiendomByggested(new SampleEiendomData().Hellandtunet)
                     .Tiltakshaver(new SampleAktoerData().EnheterArbeidstilsyneBlomsterdalenOgOvreSnertingdal)
                     .Arbeidsplasser(true, false, true, false, false, "1", "35", "Det skal etableres et tilbygg til eksisterende bygg. Tilbygget består åv lager, uten faste arbeidsplasser.")
                     .BeskrivelseAvTilta("231", "Lagerhall")//Mangler i eksampler data
                     .FraSluttbrukersystemErUtfylt("Testmotor") //Mangler
                     .Fakturamottaker(true, false, "123456789", "BARB_126", "FaRef_1258", "NoenSaamTrengeFakture AS", "202012345", "StorGate 126", "3800", "Bø i Telemark", "NO") // Mangler

                     .Build();
                skjema.Data = SerializeUtil.Serialize(arbeidstilsynetsType);
            }

            return skjema;
        }
        public Skjema _04ArbeidstilsynetsSamtykke(bool data)
        {
            Skjema skjema = _skjema;
            skjema.VariantBeskrivelse = "Søknad om Arbeidstilsynets samtykke Example data 4 (feil 4845.1.5.1)";
            skjema.VariantId = "04-eksempelData";

            if (data)
            {
                var beskrivelse = "Midlertidig undervisningsbygg i form av brakkerigg, telt og adskilt uteområde som brukes til opplæring for kjøring av truck. Området brukes i dag til undervisning for videregående opplæring i anleggsteknikk, det skal nå søkes om en utvidelse av tilbudet. Tiltakshaver er Rogaland fylkeskommune.";
                var arbeidstilsynetsType = new ArbeidstilsynetsSamtykkeBuilder()
                     .ProsjektInfo("Rogaland fylkeskommune", "202012345", "202054321", "2020", "123456789", "2020")
                     .EiendomByggested(new SampleEiendomData().Hellandtunet)
                     .Tiltakshaver(new SampleAktoerData().EnheterArbeidstilsyneBlomsterdalenOgOvreSnertingdal)
                     .Arbeidsplasser(true,false,true,false,false,"1","10",beskrivelse)
                     .BeskrivelseAvTilta("311", "Kontor- og administrasjonsbygning eller rådhus")//Mangler i eksampler data
                     .FraSluttbrukersystemErUtfylt("Testmotor") //Mangler
                     .Fakturamottaker(false, true, "123456789", "BARB_126", "FaRef_1258", "NoenSaamTrengeFakture AS", "202012345", "StorGate 126", "3800", "Bø i Telemark", "NO") // Mangler
                     .Build();
                skjema.Data = SerializeUtil.Serialize(arbeidstilsynetsType);
            }

            return skjema;
        }
    }
}