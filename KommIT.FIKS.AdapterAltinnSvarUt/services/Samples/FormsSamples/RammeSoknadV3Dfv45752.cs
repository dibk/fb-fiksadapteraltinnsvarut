using System;
using System.Collections.Generic;
using System.Linq;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.Builders;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.SampleData;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;
using no.kxml.skjema.dibk.rammesoknadV3;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.FormsSamples
{
    public class RammeSoknadV3Dfv45752
    {
        private Skjema _skjema = new Skjema
        {
            FormId = "5174",
            ServiceCode = 4397,
            ServiceEditionCode = 3,
            DataFormatID = "6741",
            DataFormatVersion = "45752",
            Name = "Søknad om rammetillatelse V3",
            AvailableService = true,
            OpenWikiUrl = "https://dibk-utvikling.atlassian.net/wiki/pages/viewpage.action?pageId=31129620",
            SendTo = "Kommunen",
            FormType = "Hovedsøknad",
            Status = "Testmiljø"
        };
        public Skjema RammeSoknadV2Mini(bool data)
        {
            Skjema skjema = _skjema;

            skjema.VariantBeskrivelse = "Mini";
            skjema.VariantId = "Tom søknad";

            if (data)
            {
                var rammeSoknadV3 = new RammetillatelseV3Builder().Build();
                skjema.Data = SerializeUtil.Serialize(rammeSoknadV3);
            }
            return skjema;
        }
        public Skjema _01RammeSoknadV3Test(bool data)
        {
            Skjema skjema = _skjema;

            skjema.VariantBeskrivelse = "Rammetillatelse v3 for testing";
            skjema.VariantId = "01_Rammetillatelsev3_Test";

            if (data)
            {
                var sample = new RammetillatelseV3Builder()
                    .Tiltakshaver(new SampleAktoerData().EnheterArkitektFlink)
                   .EiendomByggested(new SampleEiendomData().FrongTestKommune)
                    .RammetillatelseAdkomst(true, "RiksFylkesveg", "Riksveg/fylkesveg", true, false, false)
                    .RammebetingelserArealdisponering(20.4, 4.2, 12.6, 24.1, 28.2, 89.2, 8.4, 16.3, 45.2, 152.7, 142.3)
                    .RammebetingerlserGenerelleVilkaar(true, false, false, false, false, true, true)
                    .RammebetingelserVannforsyningType("AnnenPrivatInnlagt", "Annen privat vannforsyning, innlagt vann", "brønn i hage", false, false)
                    .RammebetingelserAvloep("PrivatKloakk", "Privat avløpsanlegg", true, true, false, false, true, false)
                    .RammebetingelserKravTilByggegrunn(false, false, false, false, false, false, false, false, false)
                    .RammetillatelseLoefteinnretninger(false, false, false, false, false, false)
                    .RammetillatelsePlassering(false, false)
                    .RammebetingelserPlanGeldendePlan(24.5, "RP", "Reguleringsplan", "Eksemplelplannavn", "Byggeområde for boligbebyggelse", "BYA", "Bebygd areal", "Områdereguleringsplan")
                    .RammebetingelserPlanAndrePlaner("34", "Områderegulering", "Plan Navn")
                    .RammebetingelserPlanAndrePlaner("35", "Detaljregulering", "Plan Navn")
                    .BeskrivelseAvTiltakBruk("andre", "Andre", "Y", "Næringsgruppe for annet som ikke er næring", "161", "Hytter, sommerhus og fritidsbygg",
                        "Fritidsbolig", "Fritidsbolig", "Ny fritidsbolig med laftet anneks")
                    .BeskrivelseAvTiltakBruk(null, null, null, null, null, null,
                        "Annet", "Annet", " beskriv planlagt formål")
                    .BeskrivelseAvTiltakType("nyttbyggunder70m2", "Nytt bygg - Under 70 m2 - ikke boligformål")
                    .BeskrivelseAvTiltakFoelgebrev("Det søkes om bygging av en ny fritidsbolig og et laftet lite anneks. Annekset skal bygges først og benyttes som 'arbeidsbrakke' mens hovedhytta bygges.")
                    .Dispensasjon("TEK", "Byggteknisk forskrift med veiledning", "Klarer ikke å oppfylle energikravene til nytt kjellerrom", "Det søkes om disp fra energikravene i TEK for nytt treningsrom i kjeller")
                    .Signatur(new DateTime(2020, 07, 02), "signert av", "signart på vegen av")
                    .Varsling(true, true, "0", "Ingen merknader", "www.Dibk.no", "Hilde")
                    .AnsvarsrettSoeker(true, "2", "2", true)
                    .Fakturamottaker("911455315", "2840hgl", "13253", "Sæbøvågen og Nes på Hedmark", "01", true, false, "Storgata 15", "7003", "Trondheim")
                    .Metadata("Fellestjenester bygg testmotor", "123456", "Rammetillatelse v3 for testing", "www.arkitektum.no", "samtykke",true)
                    .AnsvarligSoeker(new SampleAktoerData().EnheterSaebovaagenOgLongyearbyen)
                    .Build();
                skjema.Data = SerializeUtil.Serialize(sample);
            }
            return skjema;
        }
        public Skjema _02RammeSoknadV3Signatur(bool data)
        {
            Skjema skjema = _skjema;

            skjema.VariantBeskrivelse = "Rammetillatelse v3 -Samtykke til byggeplaner - Tiltakshaver:'Ludvig Moholt' , AnsvarligSøker:'Arkitekt Flink', AnsvarligSøker OrgNr:'910065203', eiendom byggested:'9997-1/19/0/5 ";
            skjema.VariantId = "02_Rammetillatelsev3_SaBy";

            if (data)
            {
                var sample = new RammetillatelseV3Builder()
                    .AnsvarligSoeker(new SampleAktoerData().EnheterArkitektFlink)
                    .Tiltakshaver(new SampleAktoerData().PrivatAktorLudvigMoholt)
                    .EiendomByggested(new SampleEiendomData().FrongTestKommune)
                    .RammetillatelseAdkomst(true, "RiksFylkesveg", "Riksveg/fylkesveg", true, false, false)
                    .RammebetingelserArealdisponering(20.4, 4.2, 12.6, 24.1, 28.2, 89.2, 8.4, 16.3, 45.2, 152.7, 142.3)
                    .RammebetingerlserGenerelleVilkaar(true, false, false, false, false, true, true)
                    .RammebetingelserVannforsyningType("AnnenPrivatInnlagt", "Annen privat vannforsyning, innlagt vann", "brønn i hage", false, false)
                    .RammebetingelserAvloep("PrivatKloakk", "Privat avløpsanlegg", true, true, false, false, true, false)
                    .RammebetingelserKravTilByggegrunn(false, false, false, false, false, false, false, false, false)
                    .RammetillatelseLoefteinnretninger(false, false, false, false, false, false)
                    .RammetillatelsePlassering(false, false)
                    .RammebetingelserPlanGeldendePlan(24.5, "RP", "Reguleringsplan", "Eksemplelplannavn", "Byggeområde for boligbebyggelse", "BYA", "Bebygd areal", "Områdereguleringsplan")
                    .RammebetingelserPlanAndrePlaner("34", "Områderegulering", "Plan Navn")
                    .RammebetingelserPlanAndrePlaner("35", "Detaljregulering", "Plan Navn")
                    .BeskrivelseAvTiltakBruk("andre", "Andre", "Y", "Næringsgruppe for annet som ikke er næring", "161", "Hytter, sommerhus og fritidsbygg",
                        "Fritidsbolig", "Fritidsbolig", "Ny fritidsbolig med laftet anneks")
                    .BeskrivelseAvTiltakBruk(null, null, null, null, null, null,
                        "Annet", "Annet", " beskriv planlagt formål")
                    .BeskrivelseAvTiltakType("nyttbyggunder70m2", "Nytt bygg - Under 70 m2 - ikke boligformål")
                    .BeskrivelseAvTiltakFoelgebrev("Det søkes om bygging av en ny fritidsbolig og et laftet lite anneks. Annekset skal bygges først og benyttes som 'arbeidsbrakke' mens hovedhytta bygges.")
                    .Dispensasjon("TEK", "Byggteknisk forskrift med veiledning", "Klarer ikke å oppfylle energikravene til nytt kjellerrom", "Det søkes om disp fra energikravene i TEK for nytt treningsrom i kjeller")
                    .Signatur(new DateTime(2020, 07, 02), "signert av", "signart på vegen av")
                    .Varsling(true, true, "0", "Ingen merknader", "www.Dibk.no", "Hilde")
                    .AnsvarsrettSoeker(true, "2", "2", true)
                    .Fakturamottaker("911455315", "2840hgl", "13253", "Sæbøvågen og Nes på Hedmark", "01", true, false, "Storgata 15", "7003", "Trondheim")
                    .Metadata("Fellestjenester bygg testmotor", "123456", "Rammetillatelse v3 for testing", "www.arkitektum.no", "samtykke",true)
                    .Build();
                skjema.Data = SerializeUtil.Serialize(sample);
            }
            return skjema;
        }

       public List<Skjema> RammeSoknadXmlFiles(bool data)
        {
            List<Skjema> listSkjemas = new List<Skjema>();
            var xmlTestExamples = SampleService.GetLisOfXmlTestDataFormAssamble<RammetillatelseType>("42425");

            if (xmlTestExamples.Any())
            {
                for (int index = 0; index < xmlTestExamples.Count; index++)
                {
                    
                    Skjema skjema = new Skjema()
                    {
                        FormId = _skjema.FormId,
                        ServiceCode = _skjema.ServiceCode,
                        ServiceEditionCode = _skjema.ServiceEditionCode,
                        DataFormatID = _skjema.DataFormatID,
                        DataFormatVersion = _skjema.DataFormatVersion,
                        Name = _skjema.Name,
                        AvailableService = _skjema.AvailableService,
                        OpenWikiUrl = _skjema.OpenWikiUrl,
                        SendTo = _skjema.SendTo,
                        FormType = _skjema.FormType,
                        Status = _skjema.Status
                    };

                    skjema.VariantBeskrivelse = FormSamplesServices.GetDescriptionFormFileName(xmlTestExamples.ElementAt(index).Key);
                    skjema.VariantId = $"{xmlTestExamples.ElementAt(index).Key}";
                    if (data)
                    {
                        skjema.Data = xmlTestExamples.ElementAt(index).Value;
                    }
                    listSkjemas.Add(skjema);
                }
            }

            return listSkjemas;
        }

    }
}