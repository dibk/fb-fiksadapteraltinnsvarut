﻿using KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.SampleData;
using System;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.Builders;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.FormsSamples
{
    public class EttTrinnV3Dfv45751
    {
        private readonly Skjema _skjema = new Skjema()
        {
            FormId = "5174",
            ServiceCode = 4528,
            ServiceEditionCode = 3,
            DataFormatID = "6740",
            DataFormatVersion = "45751",
            Name = "Søknad om tillatelse i ett trinn V3",
            AvailableService = true,
            OpenWikiUrl = "https://dibk-utvikling.atlassian.net/wiki/pages/viewpage.action?pageId=31129622",
            SendTo = "Kommunen",
            FormType = "Hovedsøknad",
            Status = "Testmiljø"
        };
        public Skjema _01EttTrinnV3(bool data)
        {

            Skjema skjema = _skjema;

            skjema.VariantBeskrivelse = "Ett trin søknad V3 for testing";
            skjema.VariantId = "01_EttTrinnV3_Test";
            //Attachemnts =  "Matrikkelopplysninger", "Gjennomføringsplan", "TegningNyFasade", "TegningNyttSnitt", "TegningNyPlan", "Situasjonsplan"

            if (data)
            {
                var etttrinnsoknad = new EttTrinnSoknadV3Builder()
                     .Tiltakshaver(new SampleAktoerData().EnheterArkitektFlink)
                    .EiendomByggested(new SampleEiendomData().FrongTestKommune)
                    .RammetillatelseAdkomst(true, "RiksFylkesveg", "Riksveg/fylkesveg", true, false, false)
                    .RammebetingelserArealdisponering(20.4, 4.2, 12.6, 24.1, 28.2, 89.2, 8.4, 16.3, 45.2, 152.7, 142.3)
                    .RammebetingerlserGenerelleVilkaar(true, false, false, false, false, true, true)
                    .RammebetingelserVannforsyningType("AnnenPrivatInnlagt", "Annen privat vannforsyning, innlagt vann", "brønn i hage", false, false)
                    .RammebetingelserAvloep("PrivatKloakk", "Privat avløpsanlegg", true, true, false, false, true, false)
                    .RammebetingelserKravTilByggegrunn(false, false, false, false, false, false, false, false, false)
                    .RammetillatelseLoefteinnretninger(false, false, false, false, false, false)
                    .RammetillatelsePlassering(false, false)
                    .RammebetingelserPlanGeldendePlan(24.5, "RP", "Reguleringsplan", "Eksemplelplannavn", "Byggeområde for boligbebyggelse", "BYA", "Bebygd areal", "Områdereguleringsplan")
                    .RammebetingelserPlanAndrePlaner("34", "Områderegulering", "Plan Navn")
                    .RammebetingelserPlanAndrePlaner("35", "Detaljregulering", "Plan Navn")
                    .BeskrivelseAvTiltakBruk("andre", "Andre", "Y", "Næringsgruppe for annet som ikke er næring", "161", "Hytter, sommerhus og fritidsbygg",
                        "Fritidsbolig", "Fritidsbolig", "Ny fritidsbolig med laftet anneks")
                    .BeskrivelseAvTiltakBruk(null, null, null, null, null, null,
                        "Annet", "Annet", " beskriv planlagt formål")
                    .BeskrivelseAvTiltakType("nyttbyggunder70m2", "Nytt bygg - Under 70 m2 - ikke boligformål")
                    .BeskrivelseAvTiltakFoelgebrev("Det søkes om bygging av en ny fritidsbolig og et laftet lite anneks. Annekset skal bygges først og benyttes som 'arbeidsbrakke' mens hovedhytta bygges.")
                    .Dispensasjon("TEK", "Byggteknisk forskrift med veiledning", "Klarer ikke å oppfylle energikravene til nytt kjellerrom", "Det søkes om disp fra energikravene i TEK for nytt treningsrom i kjeller")
                    .Signatur(new DateTime(2020, 07, 02), "signert av", "signart på vegen av")
                    .Varsling(true, true, "0", "Ingen merknader", "www.Dibk.no", "Hilde")
                    .AnsvarsrettSoeker(true, "2", "2", true)
                    .Fakturamottaker("911455315", "2840hgl", "13253", "Sæbøvågen og Nes på Hedmark", "01", true, false, "Storgata 15", "7003", "Trondheim")
                    .Metadata("test motor", "123456", "Rammetillatelse v3 for testing", "www.arkitektum.no", "samtykke",true)
                    .AnsvarligSoeker(new SampleAktoerData().EnheterSaebovaagenOgLongyearbyen)
                    .Build();
                skjema.Data = SerializeUtil.Serialize(etttrinnsoknad);
            }

            return skjema;
        }
        public Skjema _02EttTrinnV3(bool data)
        {

            Skjema skjema = _skjema;

            skjema.VariantBeskrivelse = "Ett trin søknad V3 -Samtykke til byggeplaner - Tiltakshaver:'Ludvig Moholt' , AnsvarligSøker:'Arkitekt Flink', AnsvarligSøker OrgNr:'910065203', eiendom byggested:'9997-1/19/0/5 ";
            skjema.VariantId = "02_EttTrinnV3_SaBy";

            if (data)
            {
                var etttrinnsoknad = new EttTrinnSoknadV3Builder()
                    .AnsvarligSoeker(new SampleAktoerData().EnheterArkitektFlink)
                    .Tiltakshaver(new SampleAktoerData().PrivatAktorLudvigMoholt)
                    .EiendomByggested(new SampleEiendomData().FrongTestKommune)
                    .RammetillatelseAdkomst(true, "RiksFylkesveg", "Riksveg/fylkesveg", true, false, false)
                    .RammebetingelserArealdisponering(20.4, 4.2, 12.6, 24.1, 28.2, 89.2, 8.4, 16.3, 45.2, 152.7, 142.3)
                    .RammebetingerlserGenerelleVilkaar(true, false, false, false, false, true, true)
                    .RammebetingelserVannforsyningType("AnnenPrivatInnlagt", "Annen privat vannforsyning, innlagt vann", "brønn i hage", false, false)
                    .RammebetingelserAvloep("PrivatKloakk", "Privat avløpsanlegg", true, true, false, false, true, false)
                    .RammebetingelserKravTilByggegrunn(false, false, false, false, false, false, false, false, false)
                    .RammetillatelseLoefteinnretninger(false, false, false, false, false, false)
                    .RammetillatelsePlassering(false, false)
                    .RammebetingelserPlanGeldendePlan(24.5, "RP", "Reguleringsplan", "Eksemplelplannavn", "Byggeområde for boligbebyggelse", "BYA", "Bebygd areal", "Områdereguleringsplan")
                    .RammebetingelserPlanAndrePlaner("34", "Områderegulering", "Plan Navn")
                    .RammebetingelserPlanAndrePlaner("35", "Detaljregulering", "Plan Navn")
                    .BeskrivelseAvTiltakBruk("andre", "Andre", "Y", "Næringsgruppe for annet som ikke er næring", "161", "Hytter, sommerhus og fritidsbygg",
                        "Fritidsbolig", "Fritidsbolig", "Ny fritidsbolig med laftet anneks")
                    .BeskrivelseAvTiltakBruk(null, null, null, null, null, null,
                        "Annet", "Annet", " beskriv planlagt formål")
                    .BeskrivelseAvTiltakType("nyttbyggunder70m2", "Nytt bygg - Under 70 m2 - ikke boligformål")
                    .BeskrivelseAvTiltakFoelgebrev("Det søkes om bygging av en ny fritidsbolig og et laftet lite anneks. Annekset skal bygges først og benyttes som 'arbeidsbrakke' mens hovedhytta bygges.")
                    .Dispensasjon("TEK", "Byggteknisk forskrift med veiledning", "Klarer ikke å oppfylle energikravene til nytt kjellerrom", "Det søkes om disp fra energikravene i TEK for nytt treningsrom i kjeller")
                    .Signatur(new DateTime(2020, 07, 02), "signert av", "signart på vegen av")
                    .Varsling(true, true, "0", "Ingen merknader", "www.Dibk.no", "Hilde")
                    .AnsvarsrettSoeker(true, "2", "2", true)
                    .Fakturamottaker("911455315", "2840hgl", "13253", "Sæbøvågen og Nes på Hedmark", "01", true, false, "Storgata 15", "7003", "Trondheim")
                    .Metadata("test motor", "123456", "Rammetillatelse v3 for testing", "www.arkitektum.no", "samtykke",true)
                    .Build();
                skjema.Data = SerializeUtil.Serialize(etttrinnsoknad);
            }

            return skjema;
        }
        public Skjema _03EttTrinnV3(bool data)
        {

            Skjema skjema = _skjema;

            skjema.VariantBeskrivelse = "Ett trin søknad V3, tiltakshaver er organisasjon";
            skjema.VariantId = "03_EttTrinnV3_Tiltakshaver_Organisasjon";

            if (data)
            {
                var etttrinnsoknad = new EttTrinnSoknadV3Builder()
                    .AnsvarligSoeker(new SampleAktoerData().EnheterByggmesterBob)
                    .Tiltakshaver(new SampleAktoerData().EnheterArkitektFlink)
                    .EiendomByggested(new SampleEiendomData().FrongTestKommune)
                    .RammetillatelseAdkomst(true, "PrivatVeg", "PrivatVeg", false, true, false)
                    .RammebetingelserArealdisponering(1000.5, 30.2, 50.3, 1020, 300.2, 200.4, 10.6, 30.5, 18, 240.5, 23.36)
                    .RammebetingerlserGenerelleVilkaar(false, true, true, false, false, true, true,false)
                    .RammebetingelserVannforsyningType("AnnenPrivatInnlagt", "Annen privat vannforsyning, innlagt vann", "Felles privat vannforsyning i hele reguleringsområdet", true, true)
                    .RammebetingelserAvloep("OffentligKloakk", "Offentlig avløpsanlegg", true, true, true, true, false, true)
                    .RammebetingelserKravTilByggegrunn(false, false, false, false, false, false, false, false, false)
                    .RammetillatelseLoefteinnretninger(true, true, true, false, false, false)
                    .RammetillatelsePlassering(false, false)
                    .RammebetingelserPlanGeldendePlan(23, "RP", "Reguleringsplan", "Naustgrendanabbevannet", "Fritidsbebyggelse", "%BYA", "Prosent bebygd areal", "Andre relevante krav")
                    .RammebetingelserPlanAndrePlaner("34", "Områderegulering", "Områderegulering Midt-Telemark")
                    .RammebetingelserPlanAndrePlaner("34", "Områderegulering", "Områderegulering Midt i midten-Telemark")
                    .BeskrivelseAvTiltakBruk("andre", "Andre", "Y", "Næringsgruppe for annet som ikke er næring", "161", "Hytter, sommerhus og fritidsbygg",
                        "Fritidsbolig", "Fritidsbolig", "Ny fritidsbolig med laftet anneks")
                    .BeskrivelseAvTiltakBruk(null, null, null, null, null, null,
                        "Annet", "Annet", " beskriv planlagt formål")
                    .BeskrivelseAvTiltakType("nyttbyggboligformal", "Nytt bygg - Boligformål")
                    .BeskrivelseAvTiltakFoelgebrev("Det søkes om bygging av en ny fritidsbolig og et laftet lite anneks. Annekset skal bygges først og benyttes som 'arbeidsbrakke' mens hovedhytta bygges.")
                    .Dispensasjon("TEK", "Byggteknisk forskrift med veiledning", "Begrunnelse for dispensasjon", "Søknad om dispensasjon fra bestemmelsen NN i plan")
                    //.Signatur(new DateTime(2020, 07, 02), "signert av", "signart på vegen av")
                    .Varsling(true, false, "1", "Merknaden er tatt til følge", "www.Dibk.no", "Hilde")
                    .AnsvarsrettSoeker(true, "1", "1", true)
                    .Fakturamottaker("911455315", "2840hgl", "13253", "Sæbøvågen og Nes på Hedmark", "01", true, false, "Storgata 15", "7003", "Trondheim")
                    .Metadata("Fellestjenester bygg testmotor", "1000000000001", "Revitalisering av sentrum", "https://fellesbygg.dibk.no", "samtykke",true)
                    .Build();
                skjema.Data = SerializeUtil.Serialize(etttrinnsoknad);
            }

            return skjema;
        }
    }
}