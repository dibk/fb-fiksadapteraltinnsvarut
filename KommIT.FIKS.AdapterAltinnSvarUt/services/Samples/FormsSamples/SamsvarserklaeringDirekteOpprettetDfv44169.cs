﻿using System;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.Builders;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.SampleData;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.FormsSamples
{
    public class SamsvarserklaeringDirekteOpprettetDfv44169
    {
        private readonly Skjema _skjema = new Skjema()
        {
            FormId = "5148",
            ServiceCode = 5315,
            ServiceEditionCode = 1,
            DataFormatID = "6167",
            DataFormatVersion = "44169",
            Name = "Samsvarserklæring",
            AvailableService = false,
            OpenWikiUrl = "https://dibk-utvikling.atlassian.net/wiki/spaces/FB/pages/133365793/S+knad+om+endring+av+gitt+tillatelse",
            SendTo = "Ansvarlig søker",
            FormType = "Erklæring",
            Status = "Under utvikling",
        };

        public Skjema _01SamsvarUtforendeOkForFerdigattest(bool data)
        {

            Skjema skjema = _skjema;

            skjema.VariantBeskrivelse = "Direkte innsendt samsvarserklæring for ansvarlig utførelse, ok for ferdigattest";
            skjema.VariantId = "01-SamsvarUtfOkForFerdigattest";

            if (data)
            {
                var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                    .ProsjektData("Fellestjenester Bygg", "22", "f40d439b-c6b1-4a8d-8122-5fa705130990",
                        "Høddingsruebergskvatten utvikling")
                    .Foretak(new SampleAktoerData().EnheterByggmesterBob)
                    .AnsvarligSoeker(new SampleAktoerData().EnheterArkitektFlink)
                    .EiendomByggested(new SampleEiendomData().FrongTestKommune)
                    .EiendomByggested(new SampleEiendomData().Hellandtunet2)
                    .KommunensSaksnummer("2019", "17")
                    .Ansvarsrett("UTF", "Ansvarlig utførelse",
                        false, false, false, true,
                        false, true, true,
                        "Forskaling av mur.", "Trapp konstruksjon øst",
                        "Mangler rekkverk til trapp", DateTime.Now.AddDays(13),
                        "Installasjon branninstallasjoner")
                    .ErklaeringUtforelse(true)
                    .Build();

                skjema.Data = SerializeUtil.Serialize(samsvarserklaeringDirekteType);
            }
            return skjema;
        }

        public Skjema _02SamsvarUtforendeOkMidlertidigBrukHarTilstrekkeligSikkerhet(bool data)
        {
            Skjema skjema = _skjema;
            skjema.VariantBeskrivelse = "Direkte innsendt samsvarserklæring for ansvarlig utførelse, ok for midlertidig brukstillatelse med tilstrekkelig sikkerhet ";
            skjema.VariantId = "02-SamsvarUtfOkForMidlertidigBrukHarTilstrekkeligSikkerhet";

            if (data)
            {
                var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                    .ProsjektData("Fellestjenester Bygg", "22", "f40d439b-c6b1-4a8d-8122-5fa705130990",
                        "Høddingsruebergskvatten utvikling")
                    .Foretak(new SampleAktoerData().EnheterByggmesterBob)
                    .AnsvarligSoeker(new SampleAktoerData().EnheterArkitektFlink)
                    .EiendomByggested(new SampleEiendomData().FrongTestKommune)
                    .EiendomByggested(new SampleEiendomData().Hellandtunet2)
                    .KommunensSaksnummer("2019", "17")
                    .Ansvarsrett("UTF", "Ansvarlig utførelse",
                        false, null, null, true,
                        false, true, true,
                        "Forskaling av mur.", "Trapp konstruksjon øst",
                        "Mangler rekkverk til trapp", DateTime.Now.AddDays(13),
                        "Installasjon branninstallasjoner")
                    .ErklaeringUtforelse(true)
                    .Build();
                skjema.Data = SerializeUtil.Serialize(samsvarserklaeringDirekteType);
            }

            return skjema;
        }

        public Skjema _03SamsvarUtforendeOkMidlertidigBrukHarIkkeTilstrekkeligSikkerhet(bool data)
        {
            Skjema skjema = _skjema;
            skjema.VariantBeskrivelse = "Direkte innsendt samsvarserklæring for ansvarlig utførelse, ok for midlertidig brukstillatelse uten tilstrekkelig sikkerhet ";
            skjema.VariantId = "03-SamsvarUtfOkForMidlertidigBrukHarIkkeTilstrekkeligSikkerhet";

            if (data)
            {
                var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                    .ProsjektData("Fellestjenester Bygg", "22", Guid.NewGuid().ToString(),
                        "Høddingsruebergskvatten utvikling")
                    .Foretak(new SampleAktoerData().EnheterSaebovaagenOgLongyearbyen)
                    .AnsvarligSoeker(new SampleAktoerData().EnheterAktoerSnekkergutta)
                    .EiendomByggested(new SampleEiendomData().FrongTestKommune)
                    .EiendomByggested(new SampleEiendomData().Hellandtunet2)
                    .KommunensSaksnummer("2019", "17")
                    .Ansvarsrett("UTF", "Ansvarlig utførelse",
                        false, false, false, true,
                        false, true, false,
                        "Forskaling av mur.", "Trapp konstruksjon øst",
                        "Mangler rekkverk til trapp", DateTime.Now.AddDays(13),
                        "Installasjon branninstallasjoner")
                    .ErklaeringUtforelse(true)
                    .Build();
                skjema.Data = SerializeUtil.Serialize(samsvarserklaeringDirekteType);
            }

            return skjema;
        }


        public Skjema _04SamsvarProsjekterendeOkForFerdigattest(bool data)
        {

            Skjema skjema = _skjema;
            skjema.VariantBeskrivelse = "Direkte innsendt samsvarserklæring for ansvarlig prosjektering, ok for ferdigattest ";
            skjema.VariantId = "04-SamsvarProOkForFerdigattest";

            if (data)
            {
                var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                    .ProsjektData("Fellestjenester Bygg", "22", Guid.NewGuid().ToString(),
                        "Høddingsruebergskvatten utvikling")
                    .Foretak(new SampleAktoerData().EnheterArkitektFlink)
                    .AnsvarligSoeker(new SampleAktoerData().EnheterArkitektFlink)
                    .EiendomByggested(new SampleEiendomData().FrongTestKommune)
                    .KommunensSaksnummer("2019", "17")
                    .Ansvarsrett("PRO", "Ansvarlig prosjektering",
                        true, null, null, null,
                        null, null, true,
                        "Forskaling av mur.", "Trapp konstruksjon øst",
                        "Mangler rekkverk til trapp", DateTime.Now.AddDays(13),
                        "Installasjon branninstallasjoner")
                    .ErklaeringProsjektering(true)
                    .Build();
                skjema.Data = SerializeUtil.Serialize(samsvarserklaeringDirekteType);
            }

            return skjema;
        }

        public Skjema _05SamsvarProsjekterendeOkForRammetillatelse(bool data)
        {

            Skjema skjema = _skjema;
            skjema.VariantBeskrivelse = "Direkte innsendt samsvarserklæring for ansvarlig prosjektering, ok for rammetillatelse ";
            skjema.VariantId = "05-SamsvarProOkForRamme";

            if (data)
            {
                var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                    .ProsjektData("Fellestjenester Bygg", "22", Guid.NewGuid().ToString(),
                        "Høddingsruebergskvatten utvikling")
                    .Foretak(new SampleAktoerData().EnheterArkitektFlink)
                    .AnsvarligSoeker(new SampleAktoerData().EnheterArkitektFlink)
                    .EiendomByggested(new SampleEiendomData().FrongTestKommune)
                    .KommunensSaksnummer("2019", "17")
                    .Ansvarsrett("PRO", "Ansvarlig prosjektering",
                        null, true, null, null,
                        null, null, null,
                        "Forskaling av mur.", "Trapp konstruksjon øst",
                        "Mangler rekkverk til trapp", DateTime.Now.AddDays(13),
                        "Installasjon branninstallasjoner")
                    .ErklaeringProsjektering(true)
                    .Build();
                skjema.Data = SerializeUtil.Serialize(samsvarserklaeringDirekteType);
            }

            return skjema;
        }

        public Skjema _06SamsvarProsjekterendeOkForIgangsetting(bool data)
        {
            Skjema skjema = _skjema;
            skjema.VariantBeskrivelse = "Direkte innsendt samsvarserklæring for ansvarlig prosjektering, ok for igangsetting ";
            skjema.VariantId = "06-SamsvarProOkForIgangsetting";

            if (data)
            {
                var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                    .ProsjektData("Fellestjenester Bygg", "22", Guid.NewGuid().ToString(),
                        "Høddingsruebergskvatten utvikling")
                    .Foretak(new SampleAktoerData().EnheterArkitektFlink)
                    .AnsvarligSoeker(new SampleAktoerData().EnheterArkitektFlink)
                    .EiendomByggested(new SampleEiendomData().FrongTestKommune)
                    .KommunensSaksnummer("2019", "17")
                    .Ansvarsrett("PRO", "Ansvarlig prosjektering",
                        null, true, null, null,
                        null, null, null,
                        "Forskaling av mur.", "Trapp konstruksjon øst",
                        "Mangler rekkverk til trapp", DateTime.Now.AddDays(13),
                        "Installasjon branninstallasjoner")
                    .ErklaeringProsjektering(true)
                    .Build();
                skjema.Data = SerializeUtil.Serialize(samsvarserklaeringDirekteType);
            }

            return skjema;
        }

        public Skjema _07SamsvarProsjekterendeOkForMidlertidigBrukstillatelse(bool data)
        {
            Skjema skjema = _skjema;
            skjema.VariantBeskrivelse = "Direkte innsendt samsvarserklæring for ansvarlig prosjektering, ok for midlertidig brukstillatelse";
            skjema.VariantId = "07-SamsvarProOkForMB";

            if (data)
            {
                var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                    .ProsjektData("Fellestjenester Bygg", "22", Guid.NewGuid().ToString(),
                        "Høddingsruebergskvatten utvikling")
                    .Foretak(new SampleAktoerData().EnheterArkitektFlink)
                    .AnsvarligSoeker(new SampleAktoerData().EnheterArkitektFlink)
                    .EiendomByggested(new SampleEiendomData().FrongTestKommune)
                    .KommunensSaksnummer("2019", "17")
                    .Ansvarsrett("PRO", "Ansvarlig prosjektering",
                        null, null, null, true,
                        null, null, null,
                        "Forskaling av mur.", "Trapp konstruksjon øst",
                        "Mangler rekkverk til trapp", DateTime.Now.AddDays(13),
                        "Installasjon branninstallasjoner")
                    .ErklaeringProsjektering(true)
                    .Build();
                skjema.Data = SerializeUtil.Serialize(samsvarserklaeringDirekteType);
            }
            return skjema;
        }
        public Skjema _08SamsvarUtfOkFerdigatest(bool data)
        {

            Skjema skjema = _skjema;
            skjema.VariantBeskrivelse = "Direkte innsendt samsvarserklæring for Ansvarlig utførelse, ok forferdigatest";
            skjema.VariantId = "08-SamsvarUtfOkFerdigatest";

            if (data)
            {
                var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                    .ProsjektData("Fellestjenester Bygg", "22", Guid.NewGuid().ToString(),
                        "Høddingsruebergskvatten utvikling")
                    .Foretak(new SampleAktoerData().EnheterByggmesterBob)
                    .AnsvarligSoeker(new SampleAktoerData().EnheterArkitektFlink)
                    .EiendomByggested(new SampleEiendomData().FrongTestKommune)
                    .KommunensSaksnummer("2019", "17")
                    .Ansvarsrett("UTF", "Ansvarlig utførelse",
                        true, null, null, null,
                        false, true, false,
                        "Forskaling av mur.", "Trapp konstruksjon øst",
                        "Mangler rekkverk til trapp", DateTime.Now.AddDays(13),
                        "Installasjon branninstallasjoner")
                    .ErklaeringUtforelse(true)
                    .Build();
                skjema.Data = SerializeUtil.Serialize(samsvarserklaeringDirekteType);
            }
            return skjema;
        }
        public Skjema _09SamsvarserklaeringDirekteProOgFerdigattest(bool data)
        {
            Skjema skjema = _skjema;
            skjema.VariantBeskrivelse = "Direkte innsendt samsvarserklæring for ansvarlig prosjektering, PRO og Ferdigattest";
            skjema.VariantId = "09_200121_ProOgFerdigattest";

            if (data)
            {

                var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                 .ProsjektData("Fellestjenester bygg testmotor", "99", "44556677",
                     "Hilde samsvar 3")
                 .EiendomByggested(new SampleEiendomData().Hellandtunet)
                 .KommunensSaksnummer("2016", "231")
                 .Foretak(new SampleAktoerData().EnheterNordmannByggOgAnlegg)
                 .AnsvarligSoeker(new SampleAktoerData().EnheterArkitektFlink)
                 .Ansvarsrett("PRO", "Ansvarlig prosjektering",
                     null, null, null, true,
                     null, null, null,
                     "Forskaling av mur.", "Trapp konstruksjon øst",
                     "Mangler rekkverk til trapp", DateTime.Now.AddDays(13),
                     "Installasjon branninstallasjoner")
                 .ErklaeringProsjektering(true)
                 .Build();
                skjema.Data = SerializeUtil.Serialize(samsvarserklaeringDirekteType);
            }
            return skjema;
        }
        public Skjema _10SamsvarserklaeringDirekteProOgIg(bool data)
        {
            Skjema skjema = _skjema;
            skjema.VariantBeskrivelse = "Direkte innsendt samsvarserklæring for ansvarlig prosjektering, PRO og IG";
            skjema.VariantId = "10_200121_ProOg_Ig";

            if (data)
            {

                var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                 .ProsjektData("Fellestjenester bygg testmotor", "99", "44556677",
                     "Hilde samsvar 2")
                 .EiendomByggested(new SampleEiendomData().Hellandtunet)
                 .KommunensSaksnummer("2016", "231")
                 .Foretak(new SampleAktoerData().EnheterNordmannByggOgAnlegg)
                 .AnsvarligSoeker(new SampleAktoerData().EnheterArkitektFlink)
                 .Ansvarsrett("PRO", "Ansvarlig prosjektering",
                     false, false, true, false,
                     false, false, false,
                     "", "",
                     "Mangler rekkverk til trapp", DateTime.Now.AddDays(13),
                     "Arkitekturprosjektering",
                     false)
                 .ErklaeringProsjektering(true)
                 .ErklaeringUtforelse(false)
                 .Build();
                skjema.Data = SerializeUtil.Serialize(samsvarserklaeringDirekteType);
            }
            return skjema;
        }
        public Skjema _11SamsvarserklaeringDirekteProOgMidlertidig(bool data)
        {
            Skjema skjema = _skjema;
            skjema.VariantBeskrivelse = "Direkte innsendt samsvarserklæring for ansvarlig prosjektering, UTF og Midlertidig";
            skjema.VariantId = "11_200121_UtfMidlertidig";

            if (data)
            {

                var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                 .ProsjektData("Fellestjenester bygg testmotor", "99", "44556677",
                     "FTB Panorama", false)
                 .EiendomByggested(new SampleEiendomData().Hellandtunet)
                 .KommunensSaksnummer("2016", "231")
                 .Foretak(new SampleAktoerData().EnheterNordmannByggOgAnlegg)
                 .AnsvarligSoeker(new SampleAktoerData().EnheterArkitektFlink)
                 .Ansvarsrett("UTF", "Ansvarlig utførelse",
                     false, false, false, true,
                     false, true, true,
                     "", "Oppsetting av støttemur",
                     "Mangler rekkverk til trapp", DateTime.Now.AddDays(13),
                     "Utføring av cement arbeid",
                     true)
                 .ErklaeringProsjektering(false)
                 .ErklaeringUtforelse(true)
                 .Build();
                skjema.Data = SerializeUtil.Serialize(samsvarserklaeringDirekteType);
            }
            return skjema;
        }
        public Skjema _12_200121UTFOgBrukstillatelseSikkerhetIvaretatt(bool data)
        {
            Skjema skjema = _skjema;
            skjema.VariantBeskrivelse = "Direkte innsendt samsvarserklæring for ansvarlig prosjektering, UTF og brukstillatelse sikkerhet ivaretatt";
            skjema.VariantId = "12_200121UTFOgBrukstillatelseSikkerhetIvaretatt";

            if (data)
            {

                var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                 .ProsjektData("Fellestjenester bygg testmotor", "99", "44556677",
                     "FTB Panorama", false)
                 .EiendomByggested(new SampleEiendomData().Hellandtunet)
                 .KommunensSaksnummer("2016", "231")
                 .Foretak(new SampleAktoerData().EnheterNordmannByggOgAnlegg)
                 .AnsvarligSoeker(new SampleAktoerData().EnheterArkitektFlink)
                 .Ansvarsrett("UTF", "Ansvarlig utførelse",
                     false, false, false, false,
                     false, true, true,
                     "Det søkes brukstillatelse for hele tiltaket.", "Arrondering av terreng",
                     "Mangler rekkverk til trapp", DateTime.Now.AddDays(13),
                     "Grunnarbeider",
                     false)
                 .ErklaeringProsjektering(false)
                 .ErklaeringUtforelse(true)
                 .Build();
                skjema.Data = SerializeUtil.Serialize(samsvarserklaeringDirekteType);
            }
            return skjema;
        }
        public Skjema _13_200122_UTFFerdig(bool data)
        {
            Skjema skjema = _skjema;
            skjema.VariantBeskrivelse = "Direkte innsendt samsvarserklæring for ansvarlig prosjektering, UTF ferdig";
            skjema.VariantId = "13_200122_UTFFerdig";

            if (data)
            {
                var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                 .ProsjektData("Fellestjenester bygg testmotor", "99", "44556677",
                     "Samsvar Hilde 8", false)
                 .EiendomByggested(new SampleEiendomData().Hellandtunet)
                 .KommunensSaksnummer("2016", "231")
                 .Foretak(new SampleAktoerData().EnheterNordmannByggOgAnlegg)
                 .AnsvarligSoeker(new SampleAktoerData().EnheterArkitektFlink)
                 .Ansvarsrett("UTF", "Ansvarlig utførelse",
                     false, false, false, false,
                     true, false, false,
                     "", "",
                     "Mangler rekkverk til trapp", DateTime.Now.AddDays(13),
                     "Grunnarbeider",
                     false)
                 .ErklaeringProsjektering(false)
                 .ErklaeringUtforelse(true)
                 .Build();
                skjema.Data = SerializeUtil.Serialize(samsvarserklaeringDirekteType);
            }
            return skjema;
        }
        public Skjema _14_200122_UTFMidlertidigSikkerhetIkkeIvaretatt(bool data)
        {
            Skjema skjema = _skjema;
            skjema.VariantBeskrivelse = "Direkte innsendt samsvarserklæring for ansvarlig prosjektering, UTF midlertidig sikkerhet ikke ivaretatt";
            skjema.VariantId = "14_200122_UTFMidlertidigSikkerhetIkkeIvaretatt";

            if (data)
            {
                var samsvarserklaeringDirekteType = new SamsvarserklaeringDirekteBuilder()
                 .ProsjektData("Fellestjenester bygg testmotor", "99", "44556677",
                     "Samsvar Hilde 7", false)
                 .EiendomByggested(new SampleEiendomData().Hellandtunet)
                 .KommunensSaksnummer("2016", "231")
                 .Foretak(new SampleAktoerData().EnheterNordmannByggOgAnlegg)
                 .AnsvarligSoeker(new SampleAktoerData().EnheterArkitektFlink)
                 .Ansvarsrett("UTF", "Ansvarlig utførelse",
                     false, false, false, false,
                     false, true, false,
                     "Det søkes brukstillatelse for hele tiltaket", "Arondering av terreng",
                     "Rekkverk ved inngangsparti", DateTime.Now.AddDays(13),
                     "Grunnarbeider",
                     false)
                 .ErklaeringProsjektering(false)
                 .ErklaeringUtforelse(true)
                 .Build();
                skjema.Data = SerializeUtil.Serialize(samsvarserklaeringDirekteType);
            }
            return skjema;
        }
    }
}