﻿using System;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.Builders;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.SampleData;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.FormsSamples
{
    public class KontrollerklaeringDirekteOpprettetDfv45020
    {
        private readonly Skjema _skjema = new Skjema()
        {
            FormId = "5149",
            ServiceCode = 5444,
            ServiceEditionCode = 1,
            DataFormatID = "6341",
            DataFormatVersion = "45020",
            Name = "Kontrollerklæring med sluttrapport",
            AvailableService = false,
            OpenWikiUrl = "https://dibk-utvikling.atlassian.net/wiki/spaces/FB/pages/133365793/S+knad+om+endring+av+gitt+tillatelse",
            SendTo = "Ansvarlig søker",
            FormType = "Erklæring",
            Status = "Under utvikling",
        };

        public Skjema _01KontrollerklaeringIngenAvvik(bool data)
        {

            Skjema skjema = _skjema;
            skjema.VariantBeskrivelse = "Direkte innsendt kontroll erklæring, ingen avvik";
            skjema.VariantId = "01-KontrollDirekteIngenAvvik";

            if (data)
            {
                var kontrollerklaeringDirekteType = new KontrollerklaeringDirekteBuilder()
                    .ProsjektData("Fellestjenester Bygg", "22", "88c7ff03-90f8-4b2c-acef-46778275bd78", "Høddingsruebergskvatten utvikling")
                    .AnsvarligSoeker(new SampleAktoerData().EnheterArkitektFlink)
                    .Foretak(new SampleAktoerData().EnheterAktoerSnekkergutta)
                    .EiendomByggested(new SampleEiendomData().FrongTestKommune)
                    .KommunensSaksnummer("2019", "17")
                    .Ansvarsrett("KONTROLL", "Ansvarlig kontroll",
                        false, false, true, true, 
                        DateTime.Now.AddDays(13),
                        "35a07aa9-7be7-4b19-ae74-a7217a7280d1", "Kontroll av branninstallasjoner")
                    .ErklaeringKontroll(true);

                skjema.Data = SerializeUtil.Serialize(kontrollerklaeringDirekteType.Build());
            }
                      
            return skjema;
        }

        public Skjema _02KontrollerklaeringObserverteAvvik(bool data)
        {

            Skjema skjema = _skjema;
            skjema.VariantBeskrivelse = "Direkte innsendt kontroll erklæring, observerte avvik";
            skjema.VariantId = "02-KontrollDirekteObserverteAvvik";

            if (data)
            {
                var kontrollerklaeringDirekteType = new KontrollerklaeringDirekteBuilder()
                    .ProsjektData("Fellestjenester Bygg", "22", "3dcc74f1-9352-41cd-a988-f80b46dddaa5", "Høddingsruebergskvatten utvikling")
                    .EiendomByggested(new SampleEiendomData().FrongTestKommune)
                    .KommunensSaksnummer("2019", "17")
                    .Foretak(new SampleAktoerData().EnheterAktoerSnekkergutta)
                    .Ansvarsrett("KONTROLL", "Ansvarlig kontroll",
                        true, false, false, true,
                        new DateTime(2017,01,18),
                        "f9b76c6e-8fc2-47ee-929a-31c28da40533", "Kontroll av branninstallasjoner")
                    .AnsvarligSoeker(new SampleAktoerData().EnheterArkitektFlink)
                    .ErklaeringKontroll(true);

                skjema.Data = SerializeUtil.Serialize(kontrollerklaeringDirekteType.Build());
            }

            return skjema;
        }

        public Skjema _03KontrollerklaeringObserverteOgAapneAvvik(bool data)
        {

            Skjema skjema = _skjema;
            skjema.VariantBeskrivelse = "Direkte innsendt kontroll erklæring, observerte og åpne avvik";
            skjema.VariantId = "03-KontrollDirekteObserverteOgAapneAvvik";

            if (data)
            {
                var kontrollerklaeringDirekteType = new KontrollerklaeringDirekteBuilder()
                    .ProsjektData("Fellestjenester Bygg", "22", "c486a9e4-b37f-44d7-bf9c-eb6b0474c64c", "Høddingsruebergskvatten utvikling")
                    .EiendomByggested(new SampleEiendomData().FrongTestKommune)
                    .KommunensSaksnummer("2019", "17")
                    .Foretak(new SampleAktoerData().EnheterAktoerSnekkergutta)
                    .Ansvarsrett("KONTROLL", "Ansvarlig kontroll",
                        true, true, false, true,
                        new DateTime(2017, 01, 18),
                        "345d39a0-54ee-4617-8a6e-347620a94627", "Kontroll av branninstallasjoner")
                    .AnsvarligSoeker(new SampleAktoerData().EnheterArkitektFlink)
                    .ErklaeringKontroll(true);

                skjema.Data = SerializeUtil.Serialize(kontrollerklaeringDirekteType.Build());
            }

            return skjema;
        }
    }
}