﻿using KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.SampleData;
using KommIT.FIKS.AdapterAltinnSvarUt.Tests.Services.Builders;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.FormsSamples
{
    public class EndringAvTillatelseDfV42350
    {
        private readonly Skjema _skjema = new Skjema()
        {
            FormId = "5168",
            ServiceCode = 4402,
            ServiceEditionCode = 1,
            DataFormatID = "5689",
            DataFormatVersion = "42350",
            Name = "Søknad om endring av gitt tillatelse eller godkjenning",
            AvailableService = false,
            OpenWikiUrl = "https://dibk-utvikling.atlassian.net/wiki/spaces/FB/pages/133365793/S+knad+om+endring+av+gitt+tillatelse",
            SendTo = "Kommune",
            FormType = "Søknad",
            Status = "Under utvikling",
        };
        public Skjema _01EndringAvTillatelseAvAnnetFasade(bool data)
        {
            Skjema skjema = _skjema;
            skjema.VariantBeskrivelse = "01_EndringAnnetFasade.";
            skjema.VariantId = "Søknad om endring av annet - fasada";
            //var attachments = new List<string>() { "GjenpartNabovarsel", "KvitteringNabovarsel", "TegningNyFasade", "TegningNyPlan", "TegningNyttSnitt" });

            if (data)
            {
                var endringAvTillatelse = new EndringAvTillatelseBuilder()
                    .EndringAvTillatelse(false, false, false, false, false, true)
                    .Tiltakshaver(new SampleAktoerData().PrivateAktoerFilipMohamed)
                    .EiendomByggested(new SampleEiendomData().Hammerfest200413)
                    .kommunensSaksnummer("12345", "2018")
                    .RammebetingerlserGenerelleVilkaar(false, false, null, null)
                    .RammebetingelserGjeldendePlan("RP", "Reguleringsplan", "%BYA", "Prosent bebygd areal", "Naustgrendanabbevannet",
                       "andre relevante krav her", "Fritidsbegyggelse", 23.37)
                    .FraSluttbrukersystem("Fellestjenester bygg testmotor")
                    .Varsling(false, false, null, null, "Fellestjenester bygg", "Fellestjenester bygg")
                    .ProsjektInfo("FTB Endringssøknad Soeknad om endring av annet - fasada")
                    .BeskrivelseAvTiltak("nyttbyggboligformal", "Nytt bygg - Boligformål",
                        "Ny fritidsbolig med laftet anneks", "Annet", "Annet", "161", "Hytter, sommerhus og fritidsbygg", "y", "Næringsgruppe for annet som ikke er næring", "andre", "Andre",
                        "Det søkes om å endre fasade mot syd. Ny fasadetegning er vedlagt. Naboer er varslet og det er ikke kommet inn noen merknader.")
                    .AnsvarligSoeker(new SampleAktoerData().PrivateAktoerFilipMohamed)
                    .Build();
                skjema.Data = SerializeUtil.Serialize(endringAvTillatelse);
            }
            return skjema;
        }
        public Skjema _02EndringAvTillatelseAvAreal(bool data)
        {
            Skjema skjema = _skjema;
            skjema.VariantBeskrivelse = "_02EndringAreal.";
            skjema.VariantId = "Søknad om endring av Areal";
            //var attachments = new List<string>() { "Gjennomføringsplan", "Situasjonsplan", "TegningNyFasade", "TegningNyPlan", "TegningNyttSnitt" });

            if (data)
            {
                var endringAvTillatelse = new EndringAvTillatelseBuilder()
                    .EndringAvTillatelse(true, false, false, false, false, false)
                    .Tiltakshaver(new SampleAktoerData().PrivateAktoerFilipMohamed)
                    .EiendomByggested(new SampleEiendomData().Hammerfest200413)
                    .kommunensSaksnummer("12345", "2018")
                    .RammebetingelserArealdisponering(200.4, 30.5, 10.6, 18.0, 23.36, 1020, 240.5, 1000.5,
                        30.2, 50.3, 300.2)
                    .RammebetingerlserGenerelleVilkaar(false, false, null, null)
                    .RammebetingelserGjeldendePlan("RP", "Reguleringsplan", "%BYA", "Prosent bebygd areal", "Naustgrendanabbevannet",
                       "andre relevante krav her", "Fritidsbegyggelse", 23.37)
                    .RammebetingelserAdkomst("PrivatVeg", "PrivatVeg", true, true, false, false)
                    .RammebetingelserKravTilByggegrunn(false, false, false, false, false, false, true, false, null)
                    .rammebetingelserPlassering(false, false)
                    .FraSluttbrukersystem("Fellestjenester bygg testmotor")
                    .Varsling(true, false, null, "0", "Fellestjenester bygg", "Fellestjenester bygg")
                    .ProsjektInfo("FTB Søknad om endring av Areal")
                    .BeskrivelseAvTiltak("nyttbyggboligformal", "Nytt bygg - Boligformål",
                        "Ny fritidsbolig med laftet anneks", "Fritidsbolig", "Fritidsbolig", "161", "Hytter, sommerhus og fritidsbygg", "y", "Næringsgruppe for annet som ikke er næring", "andre", "Andre",
                        "Det søkes om å endre godkjent plassering med 0,5 meter mot nord.")
                    .BeskrivelseAvTiltakBrukTiltaksformaal("Annet", "Annet")
                    .Dispensasjon("Beskrivelse...", "Begrunnelse...", "PLAN", "Arealplanner")
                    .AnsvarligSoeker(new SampleAktoerData().EnheterArkitektFlink)
                    .Build();
                skjema.Data = SerializeUtil.Serialize(endringAvTillatelse);
            }
            return skjema;
        }
        public Skjema _03EndringAvArealUfullstendig(bool data)
        {
            Skjema skjema = _skjema;
            skjema.VariantBeskrivelse = "03_EndringArealUfullstendig.";
            skjema.VariantId = "Søknad om endring av areal ufullstendig";
            //var attachments = new List<string>() { "Gjennomføringsplan", "Situasjonsplan", "TegningNyFasade", "TegningNyPlan", "TegningNyttSnitt" });

            if (data)
            {
                var endringAvTillatelse = new EndringAvTillatelseBuilder()
                    .EndringAvTillatelse(true, false, false, false, false, false)
                    .EiendomByggested(new SampleEiendomData().Hammerfest200413)
                    .RammebetingelserArealdisponering(200.4, 30.5, 10.6, 18.0, 23.36, 1020, 240.5, 1000.5,
                        30.2, 50.3, 300.2)
                    
                    .RammebetingelserGjeldendePlan("RP", "Reguleringsplan", "%BYA", "Prosent bebygd areal", "Plan Navn",
                       null, null, 23.37)
                    .RammebetingelserAdkomst(null, null, false, null, null, null)
                    .RammebetingelserKravTilByggegrunn(false, false, null, null, null, null, null, null, null)
                    .rammebetingelserPlassering(false, false)
                    //.FraSluttbrukersystem("Fellestjenester bygg testmotor")
                    .Varsling(true, false, null, null, null, null)
                    //.ProsjektInfo("FTB Søknad om endring av Areal")
                    .BeskrivelseAvTiltak("underbygg", "Endring av bygg - utvendig - Underbygg",
                        "Byggeområde for boliger", "Bolig", "Bolig", null, null, null, null, null, null,
                        "Det søkes om bygging av en ny fritidsbolig og et laftet lite anneks. Annekset skal bygges først og benyttes som 'arbeidsbrakke' mens hovedhytta bygges.")
                    .AnsvarligSoeker(new SampleAktoerData().PrivatAktorLudvigMoholt)
                    .Build();
                skjema.Data = SerializeUtil.Serialize(endringAvTillatelse);
            }
            return skjema;
        }
        public Skjema _04EndringAvBrukMedDisp(bool data)
        {
            Skjema skjema = _skjema;
            skjema.VariantBeskrivelse = "04_EndringAvBrukMedDisp";
            skjema.VariantId = "Søknad om endring av bruk med søknad om disp";
            //var attachments = new List<string>() { "Matrikkelopplysninger", "Avkjoerselsplan" });


            if (data)
            {
                var endringAvTillatelse = new EndringAvTillatelseBuilder()
                    .EndringAvTillatelse(false, false, false, true, true, false)
                    .Tiltakshaver(new SampleAktoerData().PrivateAktoerFilipMohamed)
                    .EiendomByggested(new SampleEiendomData().Hammerfest200413)
                    .kommunensSaksnummer("12345", "2018")
                    .RammebetingelserArealdisponering(200.4, 30.5, 10.6, 18.0, 23.36, 1020, 240.5, 1000.5,
                        30.2, 50.3, 300.2)
                    .RammebetingerlserGenerelleVilkaar(false, false, null, null)
                    .RammebetingelserGjeldendePlan("RP", "Reguleringsplan", "%BYA", "Prosent bebygd areal", "Naustgrendanabbevannet",
                       "andre relevante krav her", "Fritidsbegyggelse", 23.37)
                    .RammebetingelserAdkomst("PrivatVeg", "PrivatVeg",true,true,false,false)
                    .RammebetingelserKravTilByggegrunn(false, false, false, false, false, false,true, false,null)
                    .rammebetingelserPlassering(false,false)
                    .FraSluttbrukersystem("Fellestjenester bygg testmotor")
                    .Varsling(true, false, null, null, "Fellestjenester bygg", "Fellestjenester bygg")
                    .ProsjektInfo("FTB Endringssøknad om endring av bruk med søknad om disp")
                    .BeskrivelseAvTiltak("nyttbyggboligformal", "Nytt bygg - Boligformål",
                        "Ny fritidsbolig med laftet anneks", "Fritidsbolig", "Fritidsbolig", "161", "Hytter, sommerhus og fritidsbygg", "y", "Næringsgruppe for annet som ikke er næring", "andre", "Andre",
                        "Det søkes om å endre nytt uthus til anneks.")
                    .BeskrivelseAvTiltakBrukTiltaksformaal("Annet","Annet")
                    .Dispensasjon("Det søkes om dispensasjon fra gjeldende bestemmelse i plan for bruk av uthus som anneks.", "Særlige grunner for å kunne invilge dispensasjon er  at uthus ligger svært nær hovedhytte og oppfattes som en del av hovedhytta. Dersom de var sammenbygde, ville det være i tråd med planen.","PLAN", "Arealplaner")
                    .AnsvarligSoeker(new SampleAktoerData().EnheterArkitektFlink)
                    .Build();
                skjema.Data = SerializeUtil.Serialize(endringAvTillatelse);
            }
            return skjema;
        }
        public Skjema _05EndringPlassering(bool data)
        {
            Skjema skjema = _skjema;
            skjema.VariantBeskrivelse = "05_EndringPlassering";
            skjema.VariantId = "Søknad om endring av plassering";
            //var attachments = new List<string>() { "Gjennomføringsplan", "Situasjonsplan", "TegningNyFasade" });


            if (data)
            {
                var endringAvTillatelse = new EndringAvTillatelseBuilder()
                    .EndringAvTillatelse(false, true, false, false, true, false)
                    .Tiltakshaver(new SampleAktoerData().PrivateAktoerFilipMohamed)
                    .EiendomByggested(new SampleEiendomData().Hammerfest200413)
                    .kommunensSaksnummer("12345", "2018")
                    .RammebetingelserArealdisponering(200.4, 30.5, 10.6, 18.0, 23.36, 1020, 240.5, 1000.5,
                        30.2, 50.3, 300.2)
                    .RammebetingerlserGenerelleVilkaar(false, false, null, null)
                    .RammebetingelserGjeldendePlan("RP", "Reguleringsplan", "%BYA", "Prosent bebygd areal", "Naustgrendanabbevannet",
                       "andre relevante krav her", "Fritidsbegyggelse", 23.37)
                    .RammebetingelserAdkomst("PrivatVeg", "PrivatVeg", true, true, false, false)
                    .RammebetingelserKravTilByggegrunn(false, false, false, false, false, false, true, false, null)
                    .rammebetingelserPlassering(false, false)
                    .FraSluttbrukersystem("Fellestjenester bygg testmotor")
                    .Varsling(true, false, null, "0", "Fellestjenester bygg", "Fellestjenester bygg")
                    .ProsjektInfo("Søknad om endring av plassering")
                    .BeskrivelseAvTiltak("nyttbyggboligformal", "Nytt bygg - Boligformål",
                        "Ny fritidsbolig med laftet anneks", "Fritidsbolig", "Fritidsbolig", "161", "Hytter, sommerhus og fritidsbygg", "y", "Næringsgruppe for annet som ikke er næring", "andre", "Andre",
                        "Det søkes om å endre godkjent plassering med 0,5 meter mot nord.")
                    .BeskrivelseAvTiltakBrukTiltaksformaal("Annet", "Annet")
                    .Dispensasjon("...Beskrivelse...", "...begrunelse...", "PLAN", "Arealplaner")
                    .AnsvarligSoeker(new SampleAktoerData().EnheterArkitektFlink)
                    .Build();
                skjema.Data = SerializeUtil.Serialize(endringAvTillatelse);
            }
            return skjema;
        }
        public Skjema _06EndringPlasseringMedNabomerknader(bool data)
        {
            Skjema skjema = _skjema;
            skjema.VariantBeskrivelse = "06_EndringPlasseringMedNabomerknader";
            skjema.VariantId = "Søknad om endring av plassering med nabomerknader";
            //var attachments = new List<string>() { "Gjennomføringsplan", "Situasjonsplan", "TegningNyFasade", "GjenpartNabovarsel", "KvitteringNabovarsel", "Nabomerknader"});


            if (data)
            {
                var endringAvTillatelse = new EndringAvTillatelseBuilder()
                    .EndringAvTillatelse(false, true, false, false, false, false)
                    .Tiltakshaver(new SampleAktoerData().PrivateAktoerFilipMohamed)
                    .EiendomByggested(new SampleEiendomData().Hammerfest200413)
                    .kommunensSaksnummer("12345", "2018")
                    .RammebetingelserArealdisponering(200.4, 30.5, 10.6, 18.0, 23.36, 1020, 240.5, 1000.5,
                        30.2, 50.3, 300.2)
                    .RammebetingerlserGenerelleVilkaar(false, false, null, null)
                    .RammebetingelserGjeldendePlan("RP", "Reguleringsplan", "%BYA", "Prosent bebygd areal", "Naustgrendanabbevannet",
                       "Andre relevante krav her", "Fritidsbegyggelse", 23)
                    .RammebetingelserAdkomst("PrivatVeg", "PrivatVeg", true, true, false, false)
                    .RammebetingelserKravTilByggegrunn(false, false, false, false, false, false, false, false, false)
                    .rammebetingelserPlassering(false, false)
                    .FraSluttbrukersystem("Fellestjenester bygg testmotor")
                    .Varsling(false, true, "Merknadene er tatt til følge", "1", "Fellestjenester bygg", "Fellestjenester bygg")
                    .ProsjektInfo("Søknad om endring av plassering med nabomerknader")
                    .BeskrivelseAvTiltak("nyttbyggboligformal", "Nytt bygg - Boligformål",
                        "Ny fritidsbolig", "Fritidsbolig", "Fritidsbolig", "161", "Hytter, sommerhus og fritidsbygg", "y", "Næringsgruppe for annet som ikke er næring", "andre", "Andre",
                        "Det søkes om å endre godkjent plassering med 1,5 meter mot nord.")
                    .Dispensasjon("...Beskrivelse...", "...begrunelse...", "PLAN", "Arealplaner")
                    .AnsvarligSoeker(new SampleAktoerData().EnheterArkitektFlink)
                    .Build();
                skjema.Data = SerializeUtil.Serialize(endringAvTillatelse);
            }
            return skjema;
        }

        public Skjema _07EndringPlasseringDispensasjonTrengerSkred(bool data)
        {
            Skjema skjema = _skjema;
            skjema.VariantBeskrivelse = "07_EndringPlasseringDispensasjonTrengerSkred";
            skjema.VariantId = "Søknad om endring av plassering, søknad om dispensasjon, trenger vedlegg skred.";
            //var attachments = new List<string>() { "Gjennomføringsplan", "Situasjonsplan", "TegningNyFasade", "GjenpartNabovarsel", "KvitteringNabovarsel", "Nabomerknader"});


            if (data)
            {

                var begrunelse = "Da regulert friareal ligger 6 meter høyere enn boligtomta og boligen som skal bygges har en mønehøyde på 5 meter, vil ikke plasseringen han noen negative konsekvenser for noen. Ved å flytte boligen 0,5 meter mot nord,vil nabo beholde sol på sin uteplass en time lenger på ettermiddagen.";



                var endringAvTillatelse = new EndringAvTillatelseBuilder()
                    .EndringAvTillatelse(false, true, false, false, true, false)
                    .Tiltakshaver(new SampleAktoerData().PrivateAktoerFilipMohamed)
                    .EiendomByggested(new SampleEiendomData().Hammerfest200413)
                    .kommunensSaksnummer("12345", "2018")
                    .RammebetingelserArealdisponering(200.4, 30.5, 10.6, 18.0, 23.36, 1020, 240.5, 1000.5,
                        30.2, 50.3, 300.2)
                    .RammebetingerlserGenerelleVilkaar(false, false, null, null)
                    .RammebetingelserGjeldendePlan("RP", "Reguleringsplan", "%BYA", "Prosent bebygd areal", "Naustgrendanabbevannet",
                       "Andre relevante krav her", "Fritidsbegyggelse", 23)
                    .RammebetingelserAdkomst("PrivatVeg", "PrivatVeg", true, true, false, false)
                    .RammebetingelserKravTilByggegrunn(false, true, false, false, false, false, true, false, false)
                    .rammebetingelserPlassering(false, false)
                    .FraSluttbrukersystem("Fellestjenester bygg testmotor")
                    .Varsling(true, false, "Merknadene er tatt til følge", "0", "Fellestjenester bygg", "Fellestjenester bygg")
                    .ProsjektInfo("Søknad om endring av plassering, søknad om dispensasjon, trenger vedlegg skred.")
                    .BeskrivelseAvTiltak("nyttbyggboligformal", "Nytt bygg - Boligformål",
                        "Ny fritidsbolig", "Fritidsbolig", "Fritidsbolig", "161", "Hytter, sommerhus og fritidsbygg", "y", "Næringsgruppe for annet som ikke er næring", "andre", "Andre",
                        "Det søkes om å endre godkjent plassering med 0,5 meter mot nord. Det søkes om dispensasjon fra regulert friareal mot nord.")
                    .Dispensasjon("Det søkes om dispensasjon fra regulert byggegrense mot friareal", begrunelse, "PLAN", "Arealplaner")
                    .AnsvarligSoeker(new SampleAktoerData().EnheterArkitektFlink)
                    .Build();
                skjema.Data = SerializeUtil.Serialize(endringAvTillatelse);
            }
            return skjema;
        }
        public Skjema _08EndringAnnet(bool data)
        {
            Skjema skjema = _skjema;
            skjema.VariantBeskrivelse = "08_EndringssøknadAnnetMinimumssoeknad.";
            skjema.VariantId = "Søknad om endring av annet - minimun søknad";
            //var attachments = new List<string>() { "GjenpartNabovarsel", "KvitteringNabovarsel", "TegningNyFasade", "TegningNyPlan", "TegningNyttSnitt" });

            if (data)
            {
                var endringAvTillatelse = new EndringAvTillatelseBuilder()
                    .EndringAvTillatelse(false, false, false, false, false, true)
                    .EiendomByggested(new SampleEiendomData().Hammerfest200413)
                    .kommunensSaksnummer("1378", "2020")
                    .RammebetingelserGjeldendePlan("RP", "Reguleringsplan", "%BYA", "Prosent bebygd areal", "Naustgrendanabbevannet",
                       "andre relevante krav her", "Fritidsbegyggelse", 23.37)
                    .FraSluttbrukersystem("Fellestjenester bygg testmotor")
                    .Varsling(true, false, null, "0", "Fellestjenester bygg", "Fellestjenester bygg")
                    .ProsjektInfo("FTB Minimal endringssøknad")
                    .BeskrivelseAvTiltak("nyttbyggboligformal", "Nytt bygg - Boligformål",
                        null, null, null, null, null, null, null, null, null,
                        "I dette følgebrevet beskriver jeg hvilke type endringer jeg har lyst til å gjøre.")
                    .AnsvarligSoeker(new SampleAktoerData().EnheterArkitektFlink)
                    .Build();
                skjema.Data = SerializeUtil.Serialize(endringAvTillatelse);
            }
            return skjema;
        } 
        public Skjema _09EndringAnnetLangeTekststrengerOgLinjeskift(bool data)
        {
            Skjema skjema = _skjema;
            skjema.VariantBeskrivelse = "09_EndringAnnetLangeTekststrengerOgLinjeskift.";
            skjema.VariantId = "Søknad om endring av annet - lange tekster og linje skift";
            //var attachments = new List<string>() { "GjenpartNabovarsel", "KvitteringNabovarsel", "TegningNyFasade", "TegningNyPlan", "TegningNyttSnitt" });

            if (data)
            {
                var navn = "Naustgrendanabbevannet er navnet på denne planen, som kan være ganske så langt";
                var formaal = "Fritidsbegyggelse Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean est tellus, ultricies et metus non, pellentesque varius risus. Aliquam nec diam in mi elementum vestibulum eget eu magna. Sed fringilla tempor tincidunt. Quisque auctor mi et nisi egestas tincidunt. Curabitur mauris velit, molestie at urna ut, dignissim congue urna. Aliquam viverra risus nec varius ultrices. Proin luctus dolor ac egestas venenatis. Nullam volutpat velit ex, cursus mollis justo commodo sit amet. Ut sit amet semper elit, at egestas magna. Quisque ornare ultricies rutrum. Curabitur sit amet turpis justo. Nulla quis suscipit eros. Cras sagittis malesuada vehicula." +
                              "Proin consectetur fringilla purus. Nulla fermentum libero in molestie mollis. Aenean fermentum fermentum magna, nec viverra sem lacinia id. Morbi fringilla massa vel mi sodales, a pellentesque nibh mollis.Phasellus et neque eros. Suspendisse suscipit, orci quis interdum molestie, mauris magna ultrices augue, varius interdum sapien dolor et odio.Integer ullamcorper imperdiet gravida. Ut aliquam felis vitae elementum convallis. In viverra magna eget venenatis sagittis." +
                              "Curabitur rutrum rhoncus enim, nec posuere sapien fringilla at. Suspendisse ut nulla vulputate, congue magna a, maximus dui.Donec gravida felis eget libero convallis ullamcorper.Pellentesque dui turpis, imperdiet et cursus volutpat, convallis vitae nulla. Aliquam felis ipsum, rutrum in fermentum vel, facilisis at risus. Nam tincidunt, ligula id blandit mollis, lorem dolor auctor nibh, ac venenatis leo elit id lectus.Fusce vestibulum nulla in augue tristique interdum.Proin sit amet arcu felis.Donec molestie elit id tempor imperdiet. Nam ut ligula nec orci imperdiet lacinia.Sed id nunc a ex ultrices rutrum sit amet vel ex.Sed bibendum sagittis aliquam.";

                var andreRelevanteKrav = "andre relevante krav herLorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean est tellus, ultricies et metus non, pellentesque varius risus. Aliquam nec diam in mi elementum vestibulum eget eu magna. Sed fringilla tempor tincidunt. Quisque auctor mi et nisi egestas tincidunt. Curabitur mauris velit, molestie at urna ut, dignissim congue urna. Aliquam viverra risus nec varius ultrices. Proin luctus dolor ac egestas venenatis. Nullam volutpat velit ex, cursus mollis justo commodo sit amet. Ut sit amet semper elit, at egestas magna. Quisque ornare ultricies rutrum. Curabitur sit amet turpis justo. Nulla quis suscipit eros. Cras sagittis malesuada vehicula." +
                                         "Proin consectetur fringilla purus. Nulla fermentum libero in molestie mollis. Aenean fermentum fermentum magna, nec viverra sem lacinia id. Morbi fringilla massa vel mi sodales, a pellentesque nibh mollis.Phasellus et neque eros. Suspendisse suscipit, orci quis interdum molestie, mauris magna ultrices augue, varius interdum sapien dolor et odio.Integer ullamcorper imperdiet gravida. Ut aliquam felis vitae elementum convallis. In viverra magna eget venenatis sagittis." +
                                         "Curabitur rutrum rhoncus enim, nec posuere sapien fringilla at. Suspendisse ut nulla vulputate, congue magna a, maximus dui.Donec gravida felis eget libero convallis ullamcorper.Pellentesque dui turpis, imperdiet et cursus volutpat, convallis vitae nulla. Aliquam felis ipsum, rutrum in fermentum vel, facilisis at risus. Nam tincidunt, ligula id blandit mollis, lorem dolor auctor nibh, ac venenatis leo elit id lectus.Fusce vestibulum nulla in augue tristique interdum.Proin sit amet arcu felis.Donec molestie elit id tempor imperdiet. Nam ut ligula nec orci imperdiet lacinia.Sed id nunc a ex ultrices rutrum sit amet vel ex.Sed bibendum sagittis aliquam.";
                var beskrivPlanlagtFormaal = "Ny fritidsbolig med laftet anneks " +
                                             "Lorem ipsum dolor sit amet, consectetur adipiscing elit.Aenean est tellus, ultricies et metus non, pellentesque varius risus. Aliquam nec diam in mi elementum vestibulum eget eu magna. Sed fringilla tempor tincidunt. Quisque auctor mi et nisi egestas tincidunt.Curabitur mauris velit, molestie at urna ut, dignissim congue urna. Aliquam viverra risus nec varius ultrices. Proin luctus dolor ac egestas venenatis. Nullam volutpat velit ex, cursus mollis justo commodo sit amet.Ut sit amet semper elit, at egestas magna.Quisque ornare ultricies rutrum. Curabitur sit amet turpis justo.Nulla quis suscipit eros. Cras sagittis malesuada vehicula." +
                                             "Proin consectetur fringilla purus. Nulla fermentum libero in molestie mollis. Aenean fermentum fermentum magna, nec viverra sem lacinia id. Morbi fringilla massa vel mi sodales, a pellentesque nibh mollis.Phasellus et neque eros. Suspendisse suscipit, orci quis interdum molestie, mauris magna ultrices augue, varius interdum sapien dolor et odio.Integer ullamcorper imperdiet gravida. Ut aliquam felis vitae elementum convallis. In viverra magna eget venenatis sagittis." +
                                             "Curabitur rutrum rhoncus enim, nec posuere sapien fringilla at. Suspendisse ut nulla vulputate, congue magna a, maximus dui.Donec gravida felis eget libero convallis ullamcorper.Pellentesque dui turpis, imperdiet et cursus volutpat, convallis vitae nulla. Aliquam felis ipsum, rutrum in fermentum vel, facilisis at risus. Nam tincidunt, ligula id blandit mollis, lorem dolor auctor nibh, ac venenatis leo elit id lectus.Fusce vestibulum nulla in augue tristique interdum.Proin sit amet arcu felis.Donec molestie elit id tempor imperdiet. Nam ut ligula nec orci imperdiet lacinia.Sed id nunc a ex ultrices rutrum sit amet vel ex.Sed bibendum sagittis aliquam.";
                var foelgebrev = "Følgebrev " +
                                 "Lorem ipsum dolor sit amet, consectetur adipiscing elit.Aenean est tellus, ultricies et metus non, pellentesque varius risus. Aliquam nec diam in mi elementum vestibulum eget eu magna. Sed fringilla tempor tincidunt. Quisque auctor mi et nisi egestas tincidunt.Curabitur mauris velit, molestie at urna ut, dignissim congue urna. Aliquam viverra risus nec varius ultrices. Proin luctus dolor ac egestas venenatis. Nullam volutpat velit ex, cursus mollis justo commodo sit amet.Ut sit amet semper elit, at egestas magna.Quisque ornare ultricies rutrum. Curabitur sit amet turpis justo.Nulla quis suscipit eros. Cras sagittis malesuada vehicula." +
                                 "Proin consectetur fringilla purus. Nulla fermentum libero in molestie mollis. Aenean fermentum fermentum magna, nec viverra sem lacinia id. Morbi fringilla massa vel mi sodales, a pellentesque nibh mollis.Phasellus et neque eros. Suspendisse suscipit, orci quis interdum molestie, mauris magna ultrices augue, varius interdum sapien dolor et odio.Integer ullamcorper imperdiet gravida. Ut aliquam felis vitae elementum convallis. In viverra magna eget venenatis sagittis." +
                                 "Curabitur rutrum rhoncus enim, nec posuere sapien fringilla at. Suspendisse ut nulla vulputate, congue magna a, maximus dui.Donec gravida felis eget libero convallis ullamcorper.Pellentesque dui turpis, imperdiet et cursus volutpat, convallis vitae nulla. Aliquam felis ipsum, rutrum in fermentum vel, facilisis at risus. Nam tincidunt, ligula id blandit mollis, lorem dolor auctor nibh, ac venenatis leo elit id lectus.Fusce vestibulum nulla in augue tristique interdum.Proin sit amet arcu felis.Donec molestie elit id tempor imperdiet. Nam ut ligula nec orci imperdiet lacinia.Sed id nunc a ex ultrices rutrum sit amet vel ex.Sed bibendum sagittis aliquam.";

                var begrunelse = "Begrunnelse Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean est tellus, ultricies et metus non, pellentesque varius risus. Aliquam nec diam in mi elementum vestibulum eget eu magna. Sed fringilla tempor tincidunt. Quisque auctor mi et nisi egestas tincidunt. Curabitur mauris velit, molestie at urna ut, dignissim congue urna. Aliquam viverra risus nec varius ultrices. Proin luctus dolor ac egestas venenatis. Nullam volutpat velit ex, cursus mollis justo commodo sit amet. Ut sit amet semper elit, at egestas magna. Quisque ornare ultricies rutrum. Curabitur sit amet turpis justo. Nulla quis suscipit eros. Cras sagittis malesuada vehicula." +
                                 "Proin consectetur fringilla purus. Nulla fermentum libero in molestie mollis. Aenean fermentum fermentum magna, nec viverra sem lacinia id. Morbi fringilla massa vel mi sodales, a pellentesque nibh mollis.Phasellus et neque eros. Suspendisse suscipit, orci quis interdum molestie, mauris magna ultrices augue, varius interdum sapien dolor et odio.Integer ullamcorper imperdiet gravida. Ut aliquam felis vitae elementum convallis. In viverra magna eget venenatis sagittis." +
                                 "Curabitur rutrum rhoncus enim, nec posuere sapien fringilla at. Suspendisse ut nulla vulputate, congue magna a, maximus dui.Donec gravida felis eget libero convallis ullamcorper.Pellentesque dui turpis, imperdiet et cursus volutpat, convallis vitae nulla. Aliquam felis ipsum, rutrum in fermentum vel, facilisis at risus. Nam tincidunt, ligula id blandit mollis, lorem dolor auctor nibh, ac venenatis leo elit id lectus.Fusce vestibulum nulla in augue tristique interdum.Proin sit amet arcu felis.Donec molestie elit id tempor imperdiet. Nam ut ligula nec orci imperdiet lacinia.Sed id nunc a ex ultrices rutrum sit amet vel ex.Sed bibendum sagittis aliquam.";
                var beskrivelse = "Beskrivelse Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean est tellus, ultricies et metus non, pellentesque varius risus. Aliquam nec diam in mi elementum vestibulum eget eu magna. Sed fringilla tempor tincidunt. Quisque auctor mi et nisi egestas tincidunt. Curabitur mauris velit, molestie at urna ut, dignissim congue urna. Aliquam viverra risus nec varius ultrices. Proin luctus dolor ac egestas venenatis. Nullam volutpat velit ex, cursus mollis justo commodo sit amet. Ut sit amet semper elit, at egestas magna. Quisque ornare ultricies rutrum. Curabitur sit amet turpis justo. Nulla quis suscipit eros. Cras sagittis malesuada vehicula." +
                                  "Proin consectetur fringilla purus. Nulla fermentum libero in molestie mollis. Aenean fermentum fermentum magna, nec viverra sem lacinia id. Morbi fringilla massa vel mi sodales, a pellentesque nibh mollis.Phasellus et neque eros. Suspendisse suscipit, orci quis interdum molestie, mauris magna ultrices augue, varius interdum sapien dolor et odio.Integer ullamcorper imperdiet gravida. Ut aliquam felis vitae elementum convallis. In viverra magna eget venenatis sagittis." +
                                  "Curabitur rutrum rhoncus enim, nec posuere sapien fringilla at. Suspendisse ut nulla vulputate, congue magna a, maximus dui.Donec gravida felis eget libero convallis ullamcorper.Pellentesque dui turpis, imperdiet et cursus volutpat, convallis vitae nulla. Aliquam felis ipsum, rutrum in fermentum vel, facilisis at risus. Nam tincidunt, ligula id blandit mollis, lorem dolor auctor nibh, ac venenatis leo elit id lectus.Fusce vestibulum nulla in augue tristique interdum.Proin sit amet arcu felis.Donec molestie elit id tempor imperdiet. Nam ut ligula nec orci imperdiet lacinia.Sed id nunc a ex ultrices rutrum sit amet vel ex.Sed bibendum sagittis aliquam.";
                
                
                var endringAvTillatelse = new EndringAvTillatelseBuilder()
                    .Tiltakshaver(new SampleAktoerData().PrivateAktoerFilipMohamedLongText)
                    .EndringAvTillatelse(false, false, false, false, false, true)
                    .EiendomByggested(new SampleEiendomData().LangKomunenavn)
                    .kommunensSaksnummer("12345", "2020")
                    .RammebetingelserArealdisponering(200.4, 30.5, 10.6, 18.0, 23.36, 1020, 240.5, 1000.5,
                        30.2, 50.3, 300.2)
                    .RammebetingerlserGenerelleVilkaar(false, false, null, null)

                    .RammebetingelserGjeldendePlan("RP", "Reguleringsplan", "%BYA", "Prosent bebygd areal", navn,
                        andreRelevanteKrav, formaal, 23)
                    .RammebetingelserAdkomst("PrivatVeg", "PrivatVeg", true, true, false, false)
                    .RammebetingelserKravTilByggegrunn(false, true, false, false, false, false, true, false, false)
                    .rammebetingelserPlassering(false, false)
                    .Varsling(true, false, null, "0", "Fellestjenester bygg", "Fellestjenester bygg")
                    .ProsjektInfo("FTB Endringssøknad om mange endringer for de fleste av tingene som det kan være bra å endre, men som er innenfor kravene til endringssøknad")
                    .BeskrivelseAvTiltak("nyttbyggboligformal", "Nytt bygg - Boligformål",
                        beskrivPlanlagtFormaal, "Fritidsbolig", "Fritidsbolig", "161", "Hytter, sommerhus og fritidsbygg", "Y", "Næringsgruppe for annet som ikke er næring", "andre", "Andre",
                        foelgebrev)
                    .BeskrivelseAvTiltakBrukTiltaksformaal("Annet", "Annet")
                    .Dispensasjon(beskrivelse,begrunelse,"PLAN", "Arealplaner")
                    .AnsvarligSoeker(new SampleAktoerData().EnheterFanaOgHafsloRevisjonLongText)
                    .Build();
                skjema.Data = SerializeUtil.Serialize(endringAvTillatelse);
            }
            return skjema;
        }

        public Skjema EndringAvTillatelseEiendom(bool data)
        {
            Skjema skjema = _skjema;
            skjema.VariantBeskrivelse = "Eiendom";
            skjema.VariantId = "EiendomByggested - Hammerfest kommune";

            if (data)
            {
                var adresseData = new SampleAdresseData().Sandoeybotn;
                var matrikkelData = new SampleMatrikkelData().Hammerfest;
                var endringAvTillatelse = new EndringAvTillatelseBuilder()
                    .EiendomByggested(matrikkelData.kommunenummer, matrikkelData.gaardsnummer, matrikkelData.seksjonsnummer, matrikkelData.festenummer, matrikkelData.kommunenavn, matrikkelData.bolignummer, matrikkelData.bygningsnummer, adresseData.gatenavn, adresseData.husnr, adresseData.postnr, adresseData.poststed)
                    .Build();
                skjema.Data = SerializeUtil.Serialize(endringAvTillatelse);
            }
            return skjema;
        }
        public Skjema EndringAvTillatelseForm(bool data)
        {
            Skjema skjema = _skjema;
            skjema.VariantBeskrivelse = "Mini";
            skjema.VariantId = "Tom søknad";

            if (data)
            {
                var endringAvTillatelse = new EndringAvTillatelseBuilder().Build();
                skjema.Data = SerializeUtil.Serialize(endringAvTillatelse);
            }
            return skjema;
        }
        public Skjema EndringAvTillatelseMatrikkel(bool data)
        {

            Skjema skjema = _skjema;
            skjema.VariantBeskrivelse = "Matrikkel";
            skjema.VariantId = "Eiendom med matrikkel data - Hammerfest kommune";

            if (data)
            {
                var matrikkelData = new SampleMatrikkelData().Hammerfest;
                var endringAvTillatelse = new EndringAvTillatelseBuilder()
                    .EiendomByggested(matrikkelData.kommunenummer, matrikkelData.gaardsnummer, matrikkelData.bruksnummer, matrikkelData.festenummer, matrikkelData.seksjonsnummer, matrikkelData.kommunenavn, matrikkelData.bolignummer, matrikkelData.bygningsnummer)
                    .Build();
                skjema.Data = SerializeUtil.Serialize(endringAvTillatelse);
            }
            return skjema;
        }
        public Skjema EndringAvTillatelseAdresse(bool data)
        {
            Skjema skjema = _skjema;
            skjema.VariantBeskrivelse = "Adresse";
            skjema.VariantId = "Eiendom med Adresse data -SANDØYBOTN";

            if (data)
            {
                var adresseData = new SampleAdresseData().Sandoeybotn;
                var endringAvTillatelse = new EndringAvTillatelseBuilder()
                    .EiendomByggested(null, null, null, null, null, null, null, null, adresseData.gatenavn, adresseData.husnr, adresseData.postnr, adresseData.poststed)
                    .Build();
                skjema.Data = SerializeUtil.Serialize(endringAvTillatelse);
            }
            return skjema;
        }
        public Skjema EndringAvTillatelseAvPlassering(bool data)
        {
            Skjema skjema = _skjema;
            skjema.VariantBeskrivelse = "EndringAvPlassering";
            skjema.VariantId = "Soeknad om endring av tillatelse - endring av plassering";

            if (data)
            {
                var adresseData = new SampleAdresseData().Sandoeybotn;
                var matrikkelData = new SampleMatrikkelData().Hammerfest;
                var soeker = new SampleAktoerData().PrivateAktoerFilipMohamed;
                var endringAvTillatelse = new EndringAvTillatelseBuilder()
                    .EndringAvTillatelse(null, true, null, null, null)
                    .EiendomByggested(matrikkelData.kommunenummer, matrikkelData.gaardsnummer, matrikkelData.bruksnummer, matrikkelData.festenummer, matrikkelData.seksjonsnummer, matrikkelData.kommunenavn, matrikkelData.bolignummer, matrikkelData.bygningsnummer,
                        adresseData.gatenavn, adresseData.husnr, adresseData.postnr, adresseData.poststed)
                    .BeskrivelseAvTiltak("underbygg", "Endring av bygg - utvendig - Underbygg", "Byggeområde for boliger", "Bolig", "Bolig")
                    .RammebetingelserGjeldendePlan("RP", "Reguleringsplan", "%BYA", "Prosent bebygd areal","Plan Navn")
                    .RammebetingelserKravTilByggegrunn(false, false, null, null, null, null, null, null, null)
                    .RammebetingelserAdkomst(null, null, false, null, null, null)
                    .rammebetingelserPlassering(false, false)
                    .Varsling(false, false, null, null)
                    .AnsvarligSoeker(soeker.organisasjonsnummer, soeker.foedselsnummer, soeker.partsTypeKodeverdi, soeker.partsTypeKodebeskrivelse, soeker.kontaktperson?.navn, soeker.navn, soeker.Adresse.adresselinje1, soeker.Adresse.postnr, soeker.Adresse.landkode, soeker.Adresse.poststed, soeker.telefonnummer, soeker.mobilnummer)
                    .Build();
                skjema.Data = SerializeUtil.Serialize(endringAvTillatelse);
            }
            return skjema;
        }
        public Skjema EndringAvTillatelseAvAreal(bool data)
        {
            Skjema skjema = _skjema;
            skjema.VariantBeskrivelse = "EndringAvAreal";
            skjema.VariantId = "Soeknad om endring av tillatelse - endring av Areal";

            if (data)
            {
                var adresseData = new SampleAdresseData().Sandoeybotn;
                var matrikkelData = new SampleMatrikkelData().Hammerfest;
                var soeker = new SampleAktoerData().PrivateAktoerFilipMohamed;
                var endringAvTillatelse = new EndringAvTillatelseBuilder()
                    .EndringAvTillatelse(true, false, false, false, false)
                    .EiendomByggested(matrikkelData.kommunenummer, matrikkelData.gaardsnummer, matrikkelData.bruksnummer, matrikkelData.festenummer, matrikkelData.seksjonsnummer, matrikkelData.kommunenavn, matrikkelData.bolignummer, matrikkelData.bygningsnummer,
                        adresseData.gatenavn, adresseData.husnr, adresseData.postnr, adresseData.poststed)
                    .BeskrivelseAvTiltak("underbygg", "Endring av bygg - utvendig - Underbygg", "Byggeområde for boliger", "Bolig", "Bolig")
                    .RammebetingelserGjeldendePlan("RP", "Reguleringsplan", "%BYA", "Prosent bebygd areal","Plan Nav")
                    .RammebetingelserArealdisponering(200.4, 30.5, 10.6, 18.0, 23.36, 1020, 240.5, 1000.5,
                        30.2, 50.3, 300.2)
                    .RammebetingelserKravTilByggegrunn(false, false, null, null, null, null, null, null, null)
                    .RammebetingelserAdkomst(null, null, false, null, null, null)
                    .rammebetingelserPlassering(false, false)
                    .Varsling(false, false, null, null)
                    .AnsvarligSoeker(soeker.organisasjonsnummer, soeker.foedselsnummer, soeker.partsTypeKodeverdi, soeker.partsTypeKodebeskrivelse, soeker.kontaktperson?.navn, soeker.navn, soeker.Adresse.adresselinje1, soeker.Adresse.postnr, soeker.Adresse.landkode, soeker.Adresse.poststed, soeker.telefonnummer, soeker.mobilnummer)
                    .Build();
                skjema.Data = SerializeUtil.Serialize(endringAvTillatelse);
            }
            return skjema;
        }
        public Skjema EndringAvTillatelseAvBruk(bool data)
        {
            Skjema skjema = _skjema;
            skjema.VariantBeskrivelse = "EndringAvBruk";
            skjema.VariantId = "Soeknad om endring av tillatelse - endring av Bruk";

            if (data)
            {
                var adresseData = new SampleAdresseData().Sandoeybotn;
                var matrikkelData = new SampleMatrikkelData().Hammerfest;
                var soeker = new SampleAktoerData().PrivateAktoerFilipMohamed;
                var endringAvTillatelse = new EndringAvTillatelseBuilder()
                    .EndringAvTillatelse(null, null, null, true, null)
                    .EiendomByggested(matrikkelData.kommunenummer, matrikkelData.gaardsnummer, matrikkelData.bruksnummer, matrikkelData.festenummer, matrikkelData.seksjonsnummer, matrikkelData.kommunenavn, matrikkelData.bolignummer, matrikkelData.bygningsnummer,
                        adresseData.gatenavn, adresseData.husnr, adresseData.postnr, adresseData.poststed)
                    .BeskrivelseAvTiltak("tilbyggover50m2", "Endring av bygg - utvendig - Tilbygg med samlet areal større enn 50 m2", "Etablering av frisørsalong i sokkeletasje", "Bolig", "Bolig")
                    .RammebetingelserGjeldendePlan("RP", "Reguleringsplan", "%BYA", "Prosent bebygd areal","Plan Navn")
                    .AnsvarligSoeker(soeker.organisasjonsnummer, soeker.foedselsnummer, soeker.partsTypeKodeverdi, soeker.partsTypeKodebeskrivelse, soeker.kontaktperson?.navn, soeker.navn, soeker.Adresse.adresselinje1, soeker.Adresse.postnr, soeker.Adresse.landkode, soeker.Adresse.poststed, soeker.telefonnummer, soeker.mobilnummer)
                    .Varsling(false, false, null, null)
                    .Build();
                skjema.Data = SerializeUtil.Serialize(endringAvTillatelse);
            }
            return skjema;
        }
        public Skjema EndringAvTillatelseSomKreverDispensasjon(bool data)
        {
            Skjema skjema = _skjema;
            skjema.VariantBeskrivelse = "EndringSomKreverDispensasjon";
            skjema.VariantId = "Soeknad om endring av tillatelse - endring som krever Dispensasjon";

            if (data)
            {
                var adresseData = new SampleAdresseData().Sandoeybotn;
                var matrikkelData = new SampleMatrikkelData().Hammerfest;
                var soeker = new SampleAktoerData().PrivateAktoerFilipMohamed;
                var endringAvTillatelse = new EndringAvTillatelseBuilder()
                    .EndringAvTillatelse(null, null, null, null, true)
                    .EiendomByggested(matrikkelData.kommunenummer, matrikkelData.gaardsnummer, matrikkelData.bruksnummer, matrikkelData.festenummer, matrikkelData.seksjonsnummer, matrikkelData.kommunenavn, matrikkelData.bolignummer, matrikkelData.bygningsnummer,
                        adresseData.gatenavn, adresseData.husnr, adresseData.postnr, adresseData.poststed)
                    .RammebetingelserGjeldendePlan("RP", "Reguleringsplan", "%BYA", "Prosent bebygd areal", "Plan Navn")
                    .AnsvarligSoeker(soeker.organisasjonsnummer, soeker.foedselsnummer, soeker.partsTypeKodeverdi, soeker.partsTypeKodebeskrivelse, soeker.kontaktperson?.navn, soeker.navn, soeker.Adresse.adresselinje1, soeker.Adresse.postnr, soeker.Adresse.landkode, soeker.Adresse.poststed, soeker.telefonnummer, soeker.mobilnummer)
                    .Dispensasjon("beskrivelse...", "Det søkes om dispensasjon fra krav til minste uteoppholdsareal fordi eiendommen ligger inntil marka med store friluftsmuligheter", "PLAN", "Arealplaner")
                    .Varsling(true, null, null, null)
                    .Build();
                skjema.Data = SerializeUtil.Serialize(endringAvTillatelse);
            }
            return skjema;
        }

    }
}