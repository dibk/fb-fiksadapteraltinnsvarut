﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.FormsSamples
{
    public class FormSamplesServices
    {
        public static string GetDescriptionFormFileName(string fileNAme)
        {
            if (string.IsNullOrEmpty(fileNAme))
                return string.Empty;

            string description;
            var groupMatch = Regex.Match(fileNAme, "(?:.*?_){2}(.*)");
            if (groupMatch.Success)
            {
                var nameGroup = groupMatch.Groups[1].Value;
                description = Regex.Replace(nameGroup, "([a-z](?=[A-Z])|[A-Z](?=[A-Z][a-z]))", "$1 ");
            }
            else
            {
                string declaringClassName = "Sample data";
                var declaringType = new StackTrace().GetFrame(1).GetMethod().DeclaringType;

                if (!ReferenceEquals(declaringType, null))
                {
                    declaringClassName = declaringType.Name;
                }
                description = $"{declaringClassName},  {fileNAme}";
            }
            return description;
        }
    }
}