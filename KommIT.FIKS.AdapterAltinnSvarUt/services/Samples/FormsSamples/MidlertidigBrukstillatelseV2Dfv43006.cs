﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.Builders;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.SampleData;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;
using no.kxml.skjema.dibk.midlertidigbrukstillatelseV2;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.FormsSamples
{
    public class MidlertidigBrukstillatelseV2Dfv43006
    {
        private Skjema _skjema = new Skjema
        {
            FormId = "5169",
            ServiceCode = 4399,
            ServiceEditionCode = 5,
            DataFormatID = "5844",
            DataFormatVersion = "43006",
            Name = "Søknad om midlertidig brukstillatelse V2",
            AvailableService = true,
            OpenWikiUrl = "https://dibk-utvikling.atlassian.net/wiki/pages/viewpage.action?pageId=20021282",
            SendTo = "Kommunen",
            FormType = "Hovedsøknad",
            Status = "Testmiljø",
        };
        public Skjema MidlertidigBrukstillatelseV2Mini(bool data)
        {
            Skjema skjema = _skjema;

            skjema.VariantId = "01_Mini";
            skjema.VariantBeskrivelse = "Tom søknad";

            if (data)
            {
                skjema.Data = SerializeUtil.Serialize(new MidlertidigBrukstillatelseV2Builder().Build());
            }
            return skjema;
        }
        public Skjema MidlertidigBrukstillatelseV2_02(bool data)
        {
            Skjema skjema = _skjema;

            skjema.VariantId = "02_Mini";
            skjema.VariantBeskrivelse = "Tom søknad";

            if (data)
            {
                var form = new MidlertidigBrukstillatelseV2Builder()
                    .Tiltakshaver(new SampleAktoerData().PrivatAktorLudvigMoholt)
                    .AnsvarligSoeker(new SampleAktoerData().EnheterArkitektFlink)
                    .EiendomByggested(new SampleEiendomData().FrongTestKommune)
                    .Datoferdigattest(DateTime.Now.AddDays(20))
                    .SoknadsDelerAvTiltaken(false, "Første byggetrinn", "Overflatebehandling gulv", "Montert rekkverk på trapp")
                    .HarTilstrekkeligSikkerhet(false, "Montert rekkverk på trapp", DateTime.Now.AddDays(10), DateTime.Now.AddDays(10))
                    .KommunensSaksnummer("2015", "124340")
                    .Build();
                skjema.Data = SerializeUtil.Serialize(form);
            }
            return skjema;
        }

        public List<Skjema> MidlertidigBrukstillatelseXmlFiles(bool data)
        {
            List<Skjema> listSkjemas = new List<Skjema>();
            var xmlTestExamples = SampleService.GetLisOfXmlTestDataFormAssamble<MidlertidigBrukstillatelseType>("43006");

            if (xmlTestExamples.Any())
            {
                for (int index = 0; index < xmlTestExamples.Count; index++)
                {
                    Skjema skjema = new Skjema()
                    {
                        FormId = _skjema.FormId,
                        ServiceCode = _skjema.ServiceCode,
                        ServiceEditionCode = _skjema.ServiceEditionCode,
                        DataFormatID = _skjema.DataFormatID,
                        DataFormatVersion = _skjema.DataFormatVersion,
                        Name = _skjema.Name,
                        AvailableService = _skjema.AvailableService,
                        OpenWikiUrl = _skjema.OpenWikiUrl,
                        SendTo = _skjema.SendTo,
                        FormType = _skjema.FormType,
                        Status = _skjema.Status
                    };

                    skjema.VariantBeskrivelse = FormSamplesServices.GetDescriptionFormFileName(xmlTestExamples.ElementAt(index).Key);
                    skjema.VariantId = $"{xmlTestExamples.ElementAt(index).Key}";
                    if (data)
                    {
                        skjema.Data = xmlTestExamples.ElementAt(index).Value;
                    }
                    listSkjemas.Add(skjema);
                }
            }
            return listSkjemas;
        }
    }
}