﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.Builders;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.SampleData;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;
using no.kxml.skjema.dibk.suppleringAvSoknad;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.FormsSamples
{
    public class SuppleringAvSoeknadDfv46471
    {
        private readonly Skjema _skjema = new Skjema()
        {
            FormId = "",
            ServiceCode = 5721,
            ServiceEditionCode = 1,
            DataFormatID = "6962",
            DataFormatVersion = "46471",
            Name = "Supplering av søknad",
            AvailableService = true,
            OpenWikiUrl = "https://dibk.atlassian.net/wiki/spaces/FB/pages/2134671380/Supplering%2Bav%2Bs%2Bknad",
            SendTo = "Kommunen",
            FormType = "Hovedsøknad",
            Status = "Testmiljø"
        };

        public Skjema _01SuppleringAvSoknad(bool data)
        {
            Skjema skjema = _skjema;

            skjema.VariantBeskrivelse = "Supplering av søknad 01";
            skjema.VariantId = "01_SuppleringAvSoknad";

            if (data)
            {
                var sample = new SuppleringAvSoeknadBuilder()
                    .EiendomByggested(new SampleEiendomData().FrongTestKommune)
                    .AnsvarligSoeker(new SampleAktoerData().EnheterArkitektFlink)
                    .Tiltakshaver(new SampleAktoerData().PrivatAktorLudvigMoholt)
                    .kommunensSaksnummer("1234","2021")
                    .Ettersending("nokoTitel", "andreForhold", "Andre forhold","enFilNavn", "AndreRedegjoerelser", "Andre redegjørelser", "versjon v12231465",new DateTime(2021,06,23), "SluttrapportBygningsavfall", "Sluttrapport for bygningsavfall")
                    .Ettersending(null, null, null,"enFilNavn2", "AvkjoeringstillatelseVegmyndighet", "Avkjøringstillatelse fra vegmyndigheten", "versjon v12231465", new DateTime(2021, 06, 23))
                    .Ettersending(null, null, null,null, null, null, null,null,"ErklaeringAnsvarsrett", "Erklæring om ansvarsrett")
                    .Metadata("fra testmotor","ftb12345","Supplering v1","www.dibk.no",false)
                    .Mangelbesvarelse(false,"mangel Titel","mangelId_123564","1.8", "Noko kodebeskrivelse har","kommentar","detteErEnVedleggFil", 
                        "ByggesaksBIM", "ByggesaksBIM", "versjon v12231465", new DateTime(2021, 06, 23), "Gjennomfoeringsplan", "Gjennomføringsplan","andreForhold", "Andre forhold")
                    .Build();
                skjema.Data = SerializeUtil.Serialize(sample);
            }

            return skjema;
        }

        public List<Skjema> SuppleringSoknadXmlFiles(bool data)
        {
            List<Skjema> listSkjemas = new List<Skjema>();
            var xmlTestExamples = SampleService.GetLisOfXmlTestDataFormAssamble<SuppleringAvSoeknadType>("46471");

            if (xmlTestExamples.Any())
            {
                for (int index = 0; index < xmlTestExamples.Count; index++)
                {

                    Skjema skjema = new Skjema()
                    {
                        FormId = _skjema.FormId,
                        ServiceCode = _skjema.ServiceCode,
                        ServiceEditionCode = _skjema.ServiceEditionCode,
                        DataFormatID = _skjema.DataFormatID,
                        DataFormatVersion = _skjema.DataFormatVersion,
                        Name = _skjema.Name,
                        AvailableService = _skjema.AvailableService,
                        OpenWikiUrl = _skjema.OpenWikiUrl,
                        SendTo = _skjema.SendTo,
                        FormType = _skjema.FormType,
                        Status = _skjema.Status
                    };

                    skjema.VariantBeskrivelse = FormSamplesServices.GetDescriptionFormFileName(xmlTestExamples.ElementAt(index).Key);
                    skjema.VariantId = $"{xmlTestExamples.ElementAt(index).Key}";
                    if (data)
                    {
                        skjema.Data = xmlTestExamples.ElementAt(index).Value;
                    }
                    listSkjemas.Add(skjema);
                }
            }

            return listSkjemas;
        }
    }


}