using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples
{
    public class SampleVariant
    {
        /// <summary>
        /// DataFormatId of the Form. https://www.altinn.no/api/help
        /// </summary>
        [Required]
        public string DataFormatId { get; set; }
        /// <summary>
        /// DataFormatVersion of the Form. https://www.altinn.no/api/help
        /// </summary>
        [Required]
        public string DataFormatVersion { get; set; }
        //[Required]
        //public string FormName { get; set; }

        [Required]
        public string Variant { get; set; }

        public string Description { get; set; }
    }
}