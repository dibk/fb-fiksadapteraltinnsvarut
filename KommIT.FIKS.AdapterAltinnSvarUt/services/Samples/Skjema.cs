﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples
{
    public class Skjema
    {
        public string FormId { get; set; }
        public int ServiceCode { get; set; }
        public int ServiceEditionCode { get; set; }

        public string DataFormatID { get; set; }
        public string DataFormatVersion { get; set; }

        public string Name { get; set; }
        public bool AvailableService { get; set; }
        public string TemplateUrl { get; set; }
        public string WikiUrl { get; set; }

        public string OpenWikiUrl { get; set; }
        public string ModelUrl { get; set; }
        public string Data { get; set; }

        public string SendTo { get; set; }

        public string FormType { get; set; }

        public string Status { get; set; }

        public string VariantId { get; set; }
        public string VariantBeskrivelse { get; set; }

        public Skjema(string formId, int serviceCode, int serviceEditionCode, string dataFormatID, string dataFormatVersion, string name, bool availableService, string templateUrl, string wikiUrl, string openWikiUrl, string modelUrl, string data, string sendTo, string formtype)
        {
            FormId = formId;
            ServiceCode = serviceCode;
            ServiceEditionCode = serviceEditionCode;
            DataFormatID = dataFormatID;
            DataFormatVersion = dataFormatVersion;
            Name = name;
            AvailableService = availableService;
            TemplateUrl = templateUrl;
            WikiUrl = wikiUrl;
            OpenWikiUrl = openWikiUrl;
            ModelUrl = modelUrl;
            Data = data;
            SendTo = sendTo;
            FormType = formtype;
            

        }

        public bool HasDocumentation
        {
            get
            {
                return !String.IsNullOrEmpty(OpenWikiUrl);
            }
        }

        public Skjema()
        {

        }
    }
}