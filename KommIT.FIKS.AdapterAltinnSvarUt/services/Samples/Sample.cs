﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples
{
    public class Sample
    {
        public Sample(Skjema form) {
            this.DataFormatId = form.DataFormatID;
            this.DataFormatVersion = form.DataFormatVersion;
            this.Description = form.VariantBeskrivelse;
            this.Variant = form.VariantId;
            this.FormData = form.Data;

        }

        /// <summary>
        /// DataFormatId of the Form. https://www.altinn.no/api/help
        /// </summary>
        [Required]
        public string DataFormatId { get; set; }
        /// <summary>
        /// DataFormatVersion of the Form. https://www.altinn.no/api/help
        /// </summary>
        [Required]
        public string DataFormatVersion { get; set; }
        //[Required]
        //public string FormName { get; set; }

        [Required]
        public string Variant { get; set; }

        public string Description { get; set; }
        /// <summary>
        /// The Form data. https://www.altinn.no/api/help
        /// </summary>
        [Required]
        public string FormData { get; set; }

        /// <summary>
        /// List of all attachment types and forms/subforms name used to validate required documentation
        /// </summary>
        [Required]
        public string[] AttachmentTypesAndForms { get; set; }
    }
}