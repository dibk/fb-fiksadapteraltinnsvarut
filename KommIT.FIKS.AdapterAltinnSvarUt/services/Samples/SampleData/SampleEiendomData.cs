﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.SampleData
{
    public class SampleEiendomData
    {
      
        public Eiendom Hammerfest200413 { get; set; }
        public Eiendom Hellandtunet { get; set; }
        public Eiendom Hellandtunet2 { get; set; }
        public Eiendom Hellandtunet3 { get; set; }
        public Eiendom OperaHouseOslo { get; set; }
        public Eiendom FrongTestKommune { get; set; }
        public Eiendom LangKomunenavn { get; set; }

        public SampleEiendomData()
        {
            LangKomunenavn = new Eiendom()
            {

                EiendomsIdentifikasjon = new EiendomsIdentifikasjon()
                {
                    Kommunenummer = "9999",
                    Gaardsnummer = "582",
                    Bruksnummer = "854",
                    Festenummer = "0",
                    Seksjonsnummer = "5",

                },
                Adresse = new SampleAdresseData().LangAdresse,
                Bolignummer = "H0101",
                Kommunenavn = "Sande i More og Romsdal",
                Bygningsnummer = "80466985",
                Eier = "Tor Oskar Tine Matias Veronika Øystein Tor Kjetill Ole Anastasiya Benjamin Strand Olsen"
            };
            FrongTestKommune = new Eiendom()
            {

                EiendomsIdentifikasjon = new EiendomsIdentifikasjon()
                {
                    Kommunenummer = "9997",
                    Gaardsnummer = "1",
                    Bruksnummer = "19",
                    Festenummer = "0",
                    Seksjonsnummer = "5",

                },
                Adresse = new SampleAdresseData().Vardeveien_1,
                Bolignummer = "H0101",
                Kommunenavn = "Frong",
                Bygningsnummer = "80466985",
                Eier = "Eier Navn"
            };
            Hammerfest200413 = new Eiendom()
            {

                EiendomsIdentifikasjon = new EiendomsIdentifikasjon()
                {
                    Kommunenummer = "2004",
                    Gaardsnummer = "1",
                    Bruksnummer = "3",
                    Festenummer = "0",
                    Seksjonsnummer = "0",

                },
                Adresse = new SampleAdresseData().Sandoeybotn,
                Bolignummer = "H0101",
                Kommunenavn = "Hammerfest",
                Bygningsnummer = "192423880",
                Eier = "Eier Navn"
            };
            Hellandtunet = new Eiendom()
            {

                EiendomsIdentifikasjon = new EiendomsIdentifikasjon()
                {
                    Kommunenummer = "3817",
                    Gaardsnummer = "55",
                    Bruksnummer = "40",
                    Festenummer = "0",
                    Seksjonsnummer = "0",

                },
                Adresse = new SampleAdresseData().Hellandtunet,
                Bolignummer = "H0214",
                Kommunenavn = "MIDT-TELEMARK",
                Bygningsnummer = "165679733",
                Eier = "Eier Navn"
            };
            Hellandtunet2 = new Eiendom()
            {

                EiendomsIdentifikasjon = new EiendomsIdentifikasjon()
                {
                    Kommunenummer = "3817",
                    Gaardsnummer = "55",
                    Bruksnummer = "40",
                    Festenummer = "0",
                    Seksjonsnummer = "0",

                },
                Adresse = new SampleAdresseData().Hellandtunet,
                Bolignummer = "H0101",
                Kommunenavn = "MIDT-TELEMARK",
                Bygningsnummer = "8625743",
                Eier = "Eier Navn"
            };
            Hellandtunet3 = new Eiendom()
            {

                EiendomsIdentifikasjon = new EiendomsIdentifikasjon()
                {
                    Kommunenummer = "3817",
                    Gaardsnummer = "55",
                    Bruksnummer = "40",
                    Festenummer = "0",
                    Seksjonsnummer = "0",

                },
                Adresse = new SampleAdresseData().Hellandtunet,
                Bolignummer = "H0102",
                Kommunenavn = "MIDT-TELEMARK",
                Bygningsnummer = "300135918",
                Eier = "Eier Navn"
            };
            OperaHouseOslo = new Eiendom()
            {

                EiendomsIdentifikasjon = new EiendomsIdentifikasjon()
                {
                    Kommunenummer = "0301",
                    Gaardsnummer = "55",
                    Bruksnummer = "40",
                    Festenummer = "0",
                    Seksjonsnummer = "0",
                },
                Adresse = new SampleAdresseData().OperaHouse,
                Kommunenavn = "Oslo",
                Bygningsnummer = "81770859",
                Eier = "Eier Navn"
            };
        }

        public class Eiendom
        {
            public SampleAdresseData.Adresse Adresse { get; set; }
            public EiendomsIdentifikasjon EiendomsIdentifikasjon { get; set; }
            public string Bygningsnummer { get; set; }
            public string Bolignummer { get; set; }
            public string Kommunenavn { get; set; }
            public string Eier { get; set; }
        }

        public class EiendomsIdentifikasjon
        {
            public string Kommunenummer { get; set; }
            public string Gaardsnummer { get; set; }
            public string Bruksnummer { get; set; }
            public string Festenummer { get; set; }
            public string Seksjonsnummer { get; set; }
        }

    }
}