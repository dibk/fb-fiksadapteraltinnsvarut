﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.SampleData
{
    public class SampleArealdisponeringData
    {
        private readonly double _tomtearealByggeomraade=0;
        private readonly double _tomtearealSomTrekkesFra=0;
        private readonly double _tomtearealSomLeggesTil=0;
        private readonly double _tomtearealBeregnet=1020;
        private readonly double _beregnetMaksByggeareal=0;
        private readonly double _arealBebyggelseEksisterende=200.4;
        private readonly double _arealBebyggelseSomSkalRives=10.6;
        private readonly double _arealBebyggelseNytt=30.5;
        private readonly double _parkeringsarealTerreng=18.0;
        private readonly double _arealSumByggesak=0;
        private readonly double _beregnetGradAvUtnytting=26.36;
        public Arealdisponering Areal { get; set; }

        public SampleArealdisponeringData()
        {
            Areal= new Arealdisponering()
            {
                arealBebyggelseEksisterende = _arealBebyggelseEksisterende,
                arealBebyggelseNytt = _arealBebyggelseNytt,
                arealBebyggelseSomSkalRives = _arealBebyggelseSomSkalRives,
                parkeringsarealTerreng = _parkeringsarealTerreng,
                beregnetGradAvUtnytting = _beregnetGradAvUtnytting,
                tomtearealBeregnet = _tomtearealBeregnet,
                arealSumByggesak = _arealSumByggesak,
            };  
        }
        public class Arealdisponering
        {
            public double tomtearealByggeomraade { get; set; }
            public double tomtearealSomTrekkesFra { get; set; }
            public double tomtearealSomLeggesTil { get; set; }
            public double tomtearealBeregnet { get; set; }
            public double beregnetMaksByggeareal { get; set; }
            public double arealBebyggelseEksisterende { get; set; }
            public double arealBebyggelseSomSkalRives { get; set; }
            public double arealBebyggelseNytt { get; set; }
            public double parkeringsarealTerreng { get; set; }
            public double arealSumByggesak { get; set; }
            public double beregnetGradAvUtnytting { get; set; }

        }
    }
}