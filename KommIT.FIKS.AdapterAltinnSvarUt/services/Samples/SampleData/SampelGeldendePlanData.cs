﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.SampleData
{
    public class SampelGeldendePlanData
    {
        private readonly double? _utnyttingsgrad;
        private readonly string _plantypeKodeverdi;
        private readonly string _plantypeKodebeskrivelse;
        private readonly string _navn;
        private readonly string _formaal;
        private readonly string _andreRelevanteKrav;
        private readonly string _beregningsregelGradAvUtnyttingKodeverdi;
        private readonly string _beregningsregelGradAvUtnyttingKodebeskrivelse;

        public GeldendePlan ProsentBebygdAreal { get; set; }
        public GeldendePlan BebygdAreal { get; set; }

        public SampelGeldendePlanData()
        {
            ProsentBebygdAreal = new GeldendePlan()
            {
                plantypeKodeverdi = "%BYA",
                plantypeKodebeskrivelse = "Prosent bebygd areal",
            };
            BebygdAreal = new GeldendePlan()
            {
                plantypeKodeverdi = "BYA",
                plantypeKodebeskrivelse = "Bebygd area",
            };
        }

        public class GeldendePlan
        {
            public Double utnyttingsgrad { get; set; }
            public string plantypeKodeverdi { get; set; }
            public string plantypeKodebeskrivelse { get; set; }
            public string navn { get; set; }
            public string formaal { get; set; }
            public string andreRelevanteKrav { get; set; }
            public string beregningsregelGradAvUtnyttingKodeverdi { get; set; }
            public string beregningsregelGradAvUtnyttingKodebeskrivelse { get; set; }
        }
    }
}