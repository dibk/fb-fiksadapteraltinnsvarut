﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.SampleData
{
    public class SampleAktoerData
    {
        private readonly string _mobilnummer = "555999382";
        private readonly string _telefonnummer = "5552368";
        private readonly string _epost = "epost@test.no";

        //Privatperson
        public Aktoer PrivateAktoerFilipMohamedLongText { get; set; }
        public Aktoer PrivateAktoerFilipMohamed { get; set; }
        public Aktoer PrivatAktorFilipMohamedEncrypted { get; set; }
        public Aktoer PrivatAktorTomBola { get; set; }
        public Aktoer PrivatAktorTomBolaEncrypted { get; set; }
        public Aktoer PrivatAktorTomBolaEncrypted2 { get; set; }
        public Aktoer PrivatAktorArbeidstilsyneEilifOmar { get; set; }
        public Aktoer PrivatAktorArbeidstilsyneSigmundAasberg { get; set; }
        public Aktoer PrivatAktorLudvigMoholt { get; set; }
        public Aktoer PrivatAktorHansHansen { get; set; }



        //Kontaktprson
        public Aktoer kontaktpersonBenjamin { get; set; }
        public Aktoer kontaktpersonSprengerens { get; set; }

        //Enheter
        public Aktoer EnheterArbeidstilsyneBlomsterdalenOgOvreSnertingdal { get; set; }
        public Aktoer EnheterGravOgSprengAS { get; set; }
        public Aktoer EnheterByggefirmaPerJalla { get; set; }
        public Aktoer EnheterNordmannByggOgAnlegg { get; set; }
        public Aktoer EnheterSaebovaagenOgLongyearbyen { get; set; }
        public Aktoer EnheterAktoerSnekkergutta { get; set; }
        public Aktoer EnheterSaebovaagenOgLongyearbyenUtlandet { get; set; }
        public Aktoer EnheterByggmesterBob { get; set; }
        public Aktoer EnheterArkitektFlink { get; set; }
        public Aktoer EnheterFanaOgHafsloRevisjonLongText { get; set; }

        public SampleAktoerData()
        {
            //kontaktperson
            kontaktpersonBenjamin = new Aktoer()
            {
                navn = "Benjamin Fjell",
                mobilnummer = "90900999",
                telefonnummer = "36584123",
                Epost = _epost,
            };
            kontaktpersonSprengerens = new Aktoer()
            {
                navn = "Sprengerens kontaktperson",
                mobilnummer = "90900999",
                telefonnummer = "36584123",
                Epost = "kontakt@gravogspreng.no",
            };
            //Enheter
            EnheterFanaOgHafsloRevisjonLongText = new Aktoer()
            {

                organisasjonsnummer = "910297937",
                partsTypeKodeverdi = "Foretak",
                partsTypeKodebeskrivelse = "Foretak",
                kontaktperson = kontaktpersonBenjamin,
                navn = "FANA OG HAFSLO REVISJON dette navnet kan nok også være ganske så langt",
                mobilnummer = "87654321",
                telefonnummer = "12345678",
                Epost = "FANA.OG.HAFSLO.REVISJON.dette.navnet.kan.nok.også.være.ganske.så.langt@LangEpost.no",
                Adresse = new SampleAdresseData().LangAdresse
            };
            EnheterArbeidstilsyneBlomsterdalenOgOvreSnertingdal = new Aktoer()
            {

                organisasjonsnummer = "910748548",
                partsTypeKodeverdi = "Foretak",
                partsTypeKodebeskrivelse = "Foretak",
                kontaktperson = kontaktpersonBenjamin,
                navn = "BLOMSTERDALEN OG ØVRE SNERTINGDAL",
                mobilnummer = "87654321",
                telefonnummer = "12345678",
                Epost = "hej@dibk.no",
                Adresse = new SampleAdresseData().Hellandtunet
            };
            EnheterNordmannByggOgAnlegg = new Aktoer()
            {

                organisasjonsnummer = "910065211",
                partsTypeKodeverdi = "Foretak",
                partsTypeKodebeskrivelse = "Foretak",
                kontaktperson = kontaktpersonBenjamin,
                navn = "Byggefirma Per Jalla",
                mobilnummer = "87654321",
                telefonnummer = "12345678",
                Epost = "hej@dibk.no",
                Adresse = new SampleAdresseData().Hellandtunet
            };
            EnheterByggefirmaPerJalla = new Aktoer()
            {

                organisasjonsnummer = "910065211",
                partsTypeKodeverdi = "Foretak",
                partsTypeKodebeskrivelse = "Foretak",
                kontaktperson = kontaktpersonBenjamin,
                navn = "Byggefirma Per Jalla",
                mobilnummer = "87654321",
                telefonnummer = "12345678",
                Epost = "hej@dibk.no",
                Adresse = new SampleAdresseData().Hellandtunet
            };
            EnheterGravOgSprengAS = new Aktoer()
            {

                organisasjonsnummer = "910065157",
                partsTypeKodeverdi = "Foretak",
                partsTypeKodebeskrivelse = "Foretak",
                kontaktperson = kontaktpersonBenjamin,
                navn = "Grav og Spreng AS",
                mobilnummer = _mobilnummer,
                telefonnummer = _telefonnummer,
                Epost = "hej@dibk.no",
                Adresse = new SampleAdresseData().Hellandtunet
            };
            EnheterArkitektFlink = new Aktoer()
            {
                organisasjonsnummer = "910065203",
                partsTypeKodeverdi = "Foretak",
                partsTypeKodebeskrivelse = "Foretak",
                kontaktperson = kontaktpersonBenjamin,
                navn = "ARKITEKT FLINK",
                mobilnummer = _mobilnummer,
                telefonnummer = _telefonnummer,
                Epost = _epost,
                Adresse = new SampleAdresseData().OperaHouse
            };
            EnheterByggmesterBob = new Aktoer()
            {
                organisasjonsnummer = "910065149",
                partsTypeKodeverdi = "Foretak",
                partsTypeKodebeskrivelse = "Foretak",
                kontaktperson = kontaktpersonBenjamin,
                navn = "BYGGMESTER BOB",
                mobilnummer = _mobilnummer,
                telefonnummer = _telefonnummer,
                Epost = _epost,
                Adresse = new SampleAdresseData().OperaHouse
            };
            EnheterAktoerSnekkergutta = new Aktoer()
            {
                organisasjonsnummer = "910065165",
                partsTypeKodeverdi = "Foretak",
                partsTypeKodebeskrivelse = "Foretak",
                kontaktperson = kontaktpersonBenjamin,
                navn = "SNEKKERGUTTA",
                mobilnummer = _mobilnummer,
                telefonnummer = _telefonnummer,
                Epost = _epost,
                Adresse = new SampleAdresseData().OperaHouse
            };
            EnheterAktoerSnekkergutta = new Aktoer()
            {
                organisasjonsnummer = "910065165",
                partsTypeKodeverdi = "Foretak",
                partsTypeKodebeskrivelse = "Foretak",
                kontaktperson = kontaktpersonBenjamin,
                navn = "SNEKKERGUTTA",
                mobilnummer = _mobilnummer,
                telefonnummer = _telefonnummer,
                Epost = _epost,
                Adresse = new SampleAdresseData().OperaHouse
            };
            EnheterSaebovaagenOgLongyearbyen = new Aktoer()
            {
                organisasjonsnummer = "911455307",
                partsTypeKodeverdi = "Foretak",
                partsTypeKodebeskrivelse = "Foretak",
                kontaktperson = kontaktpersonBenjamin,
                navn = "SÆBØVÅGEN OG LONGYEARBYEN",
                mobilnummer = _mobilnummer,
                telefonnummer = _telefonnummer,
                Epost = _epost,
                Adresse = new SampleAdresseData().OperaHouse

            };
            EnheterSaebovaagenOgLongyearbyenUtlandet = new Aktoer()
            {
                organisasjonsnummer = "911455307",
                partsTypeKodeverdi = "Foretak",
                partsTypeKodebeskrivelse = "Foretak",
                kontaktperson = kontaktpersonBenjamin,
                navn = "SÆBØVÅGEN OG LONGYEARBYEN",
                mobilnummer = _mobilnummer,
                telefonnummer = _telefonnummer,
                Epost = _epost,
                Adresse = new SampleAdresseData().UtlandetAdresseDk

            };

            //Privatperson

            PrivatAktorHansHansen = new Aktoer()
            {
                foedselsnummer = "08021612345",
                partsTypeKodeverdi = "Privatperson",
                partsTypeKodebeskrivelse = "Privatperson",
                navn = "HANS HANSEN",
                mobilnummer = "555999382",
                telefonnummer = "5552368",
                Epost = _epost,
                Adresse = new SampleAdresseData().StorGate,
                kontaktperson = kontaktpersonBenjamin

            }; 
            PrivatAktorLudvigMoholt = new Aktoer()
            {
                foedselsnummer = "28044102120",
                partsTypeKodeverdi = "Privatperson",
                partsTypeKodebeskrivelse = "Privatperson",
                navn = "LUDVIG MOHOLT",
                mobilnummer = "555999382",
                telefonnummer = "5552368",
                Epost = _epost,
                Adresse = new SampleAdresseData().StorGate,
                kontaktperson = kontaktpersonBenjamin

            };
            PrivatAktorArbeidstilsyneSigmundAasberg = new Aktoer()
            {
                foedselsnummer = "24040100594",
                partsTypeKodeverdi = "Privatperson",
                partsTypeKodebeskrivelse = "Privatperson",
                navn = "SIGMUND AASBERG",
                mobilnummer = _mobilnummer,
                telefonnummer = _telefonnummer,
                Epost = _epost,
                Adresse = new SampleAdresseData().StorGate
            };
            PrivatAktorArbeidstilsyneEilifOmar = new Aktoer()
            {
                foedselsnummer = "15054601333",
                partsTypeKodeverdi = "Privatperson",
                partsTypeKodebeskrivelse = "Privatperson",
                navn = "Eilif Omar",
                mobilnummer = _mobilnummer,
                telefonnummer = _telefonnummer,
                Epost = _epost,
                Adresse = new SampleAdresseData().StorGate
            };
            PrivateAktoerFilipMohamed = new Aktoer()
            {
                foedselsnummer = "25125401530",
                partsTypeKodeverdi = "Privatperson",
                partsTypeKodebeskrivelse = "Privatperson",
                navn = "Filip Mohamed",
                mobilnummer = _mobilnummer,
                telefonnummer = _telefonnummer,
                Epost = _epost,
                Adresse = new SampleAdresseData().OperaHouse
            };
            PrivateAktoerFilipMohamedLongText = new Aktoer()
            {
                foedselsnummer = "25125401530",
                partsTypeKodeverdi = "Privatperson",
                partsTypeKodebeskrivelse = "Privatperson",
                navn = "Filip Mohamed har et ganske så langt navn",
                mobilnummer = _mobilnummer,
                telefonnummer = _telefonnummer,
                Epost = "epost.Til.FilipMohamed.har.et.ganske.så.langt.navn.er.ogsaa.velde.langt@kjempelangtEPost.no",
                Adresse = new SampleAdresseData().LangAdresse
            };
            PrivatAktorFilipMohamedEncrypted = new Aktoer()
            {
                foedselsnummer = "yFNSdYXRoUXyobi8ousqk/tC6pOQ9beKApBzDPonOy1xkMNBLPqxpBXvd3a7XUsJTdJjluVyjAe1viEosL36QdsSzBJm7kMa7frpHfOVyyF6lfzUQSijBcTbY7DCm/Frc3hGM81BcmH6WX8e410oOuZh6lk1t7H93bmu8BknxXs=",
                partsTypeKodeverdi = "Privatperson",
                partsTypeKodebeskrivelse = "Privatperson",
                navn = "Filip Mohamed",
                mobilnummer = _mobilnummer,
                telefonnummer = _telefonnummer,
                Epost = _epost,
                Adresse = new SampleAdresseData().Hellandtunet,
            };
            PrivatAktorTomBola = new SampleAktoerData.Aktoer()
            {
                foedselsnummer = "11037000320",
                partsTypeKodeverdi = "Privatperson",
                partsTypeKodebeskrivelse = "Privatperson",
                navn = "Tom Bola",
                mobilnummer = _mobilnummer,
                telefonnummer = _telefonnummer,
                Epost = _epost,
                Adresse = new SampleAdresseData().StorGate,
            };
            PrivatAktorTomBolaEncrypted = new SampleAktoerData.Aktoer()
            {
                foedselsnummer = "FNo1fp2RhCurInjoapYy12NHSEVThJXdQr2tBUSrhXxqPkahl/R01oS8bWqxDwsiT9jxXCaYQe3f0VlWqGw3hMRmxjKWk+p5ZmIlXPsHoGvvg7hpZ4ysC63K/EgZNSHiKT8Bd9KY2Ve+h9h6tZB+66y7usOi4lFTXBOvMpmpj3M=",
                partsTypeKodeverdi = "Privatperson",
                partsTypeKodebeskrivelse = "Privatperson",
                navn = "Tom Bola",
                mobilnummer = _mobilnummer,
                telefonnummer = _telefonnummer,
                Epost = _epost,
                Adresse = new SampleAdresseData().StorGate,
            };
            PrivatAktorTomBolaEncrypted2 = new SampleAktoerData.Aktoer()
            {
                foedselsnummer = "FNo1fp2RhCurInjoapYy12NHSEVThJXdQr2tBUSrhXxqPkahl/R01oS8bWqxDwsiT9jxXCaYQe3f0VlWqGw3hMRmxjKWk+p5ZmIlXPsHoGvvg7hpZ4ysC63K/EgZNSHiKT8Bd9KY2Ve+h9h6tZB+66y7usOi4lFTXBOvMpmpj3M=",
                partsTypeKodeverdi = "Privatperson",
                partsTypeKodebeskrivelse = "Privatperson",
                navn = "Tom Bola",
                mobilnummer = _mobilnummer,
                telefonnummer = _telefonnummer,
                Epost = _epost,
                Adresse = new SampleAdresseData().Hellandtunet,
            };

        }
        public class Aktoer
        {
            public string organisasjonsnummer { get; set; }
            public string foedselsnummer { get; set; }
            public string partsTypeKodeverdi { get; set; }
            public string partsTypeKodebeskrivelse { get; set; }
            public Aktoer kontaktperson { get; set; }
            public string navn { get; set; }
            public string telefonnummer { get; set; }
            public string mobilnummer { get; set; }
            public string Epost { get; set; }
            public SampleAdresseData.Adresse Adresse { get; set; }

        }
    }
}