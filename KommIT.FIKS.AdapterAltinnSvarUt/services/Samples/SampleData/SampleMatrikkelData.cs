﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Owin.Security.Provider;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.SampleData
{
    public class SampleMatrikkelData
    {

        private readonly string _bolignummer = "H0101";
        private readonly string _kommunenavn = "Hammerfest";
        private readonly string _kommunenummer = "2004";
        private readonly string _gaardsnummer = "1";
        private readonly string _bruksnummer = "3";
        private readonly string _festenummer = "0";
        private readonly string _seksjonsnummer = "0";
        private readonly string _bygningsnummer = "192423880";

        public Matrikkel Hammerfest { get; set; }

        public SampleMatrikkelData()
        {
            Hammerfest = new Matrikkel()
            {
                bolignummer = _bolignummer,
                kommunenavn = _kommunenavn,
                kommunenummer = _kommunenummer,
                gaardsnummer = _gaardsnummer,
                bruksnummer = _bruksnummer,
                festenummer = _festenummer,
                seksjonsnummer = _seksjonsnummer,
                bygningsnummer = _bygningsnummer,
            };
        }

        public class Matrikkel
        {
            public string bolignummer { get; set; }
            public string kommunenavn { get; set; }
            public string kommunenummer { get; set; }
            public string gaardsnummer { get; set; }
            public string bruksnummer { get; set; }
            public string festenummer { get; set; }
            public string seksjonsnummer { get; set; }
            public string bygningsnummer { get; set; }
        }
    }
}