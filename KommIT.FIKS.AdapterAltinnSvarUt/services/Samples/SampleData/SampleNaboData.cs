﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.SampleData
{
    public class SampleNaboData
    {
        public Naboen NaboErOrganisasjon { get; set; }
        public Naboen NaboErOrganisasjonUtlandet { get; set; }
        public Naboen NaboErPrivatPresjonTomBola { get; set; }
        public Naboen NaboErPrivatPresjonTomBola2 { get; set; }
        public Naboen NaboErPrivatPresjonFilipMohamed { get; set; }

        public SampleNaboData()
        {
            NaboErPrivatPresjonTomBola = new Naboen()
            {
                Nabo = new SampleAktoerData().PrivatAktorTomBolaEncrypted,
                GjelderNaboenEiendom = new SampleEiendomData().FrongTestKommune,
            };
            NaboErPrivatPresjonTomBola2 = new Naboen()
            {
                Nabo = new SampleAktoerData().PrivatAktorTomBolaEncrypted2,
                GjelderNaboenEiendom = new SampleEiendomData().Hellandtunet2,
            };
            NaboErPrivatPresjonFilipMohamed = new Naboen()
            {
                Nabo = new SampleAktoerData().PrivatAktorFilipMohamedEncrypted,
                GjelderNaboenEiendom = new SampleEiendomData().Hellandtunet3,
            };
            NaboErOrganisasjon = new Naboen()
            {
                Nabo = new SampleAktoerData().EnheterSaebovaagenOgLongyearbyen,
                GjelderNaboenEiendom = new SampleEiendomData().Hellandtunet3,
            };
            NaboErOrganisasjonUtlandet = new Naboen()
            {
                Nabo = new SampleAktoerData().EnheterSaebovaagenOgLongyearbyenUtlandet,
                GjelderNaboenEiendom = new SampleEiendomData().Hellandtunet3,
            };
        }

        public class Naboen
        {
            public SampleEiendomData.Eiendom GjelderNaboenEiendom { get; set; }
            public SampleAktoerData.Aktoer Nabo { get; set; }

        }
    }
}