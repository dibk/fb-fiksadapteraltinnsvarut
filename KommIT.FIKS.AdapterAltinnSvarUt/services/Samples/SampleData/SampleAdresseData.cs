﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples.SampleData
{
    public class SampleAdresseData
    {
        private static readonly string _gatenavn = "Båtsfjorden";
        private static readonly string _husnr = "6";

        private readonly string _adresselinje1 = $"{_gatenavn} {_husnr}";
        private readonly string _adresselinje2 = string.Empty;
        private readonly string _adresselinje3 = string.Empty;
        private readonly string _postnr = "9664";
        private readonly string _poststed = "SANDØYBOTN";
        private readonly string _landkode = "NO";
        //private readonly string _landnavn;// kommer
        private readonly string _bokstav = String.Empty;
        public Adresse Sandoeybotn { get; set; }
        public Adresse Raelingen { get; set; }
        public Adresse StorGate { get; set; }
        public Adresse UtlandetAdresseDk { get; set; }
        public Adresse Hellandtunet { get; set; }
        public Adresse OperaHouse { get; set; }
        public Adresse Vardeveien_1 { get; set; }
        public Adresse LangAdresse { get; set; }

        public SampleAdresseData()
        {
            LangAdresse = new Adresse()
            {
                gatenavn = "Helga Helgaens gate som er extra lang",
                husnr = "1290",
                adresselinje1 = "Lillegata 5 er en liten gate som heter lillegata og det er dermed ikke storgata",
                postnr = "1514",
                poststed = "liten by i Sande i More og Romsdal",
                landkode = "NO",
                //landnavn = _landnavn, //Kommer
                //bokstav = _bokstav,
            };
            Vardeveien_1 = new Adresse()
            {
                gatenavn = "Vardeveien",
                husnr = "1",
                adresselinje1 = "Vardeveien 1",
                postnr = "1440",
                poststed = "Frong",
                landkode = "NO",
                //landnavn = _landnavn, //Kommer
                //bokstav = _bokstav,
            };
            Sandoeybotn = new Adresse()
            {
                gatenavn = _gatenavn,
                husnr = _husnr,
                adresselinje1 = _adresselinje1,
                adresselinje2 = _adresselinje2,
                adresselinje3 = _adresselinje3,
                postnr = _postnr,
                poststed = _poststed,
                landkode = _landkode,
                //landnavn = _landnavn, //Kommer
                bokstav = _bokstav,
            };
            Raelingen = new Adresse()
            {
                gatenavn = "Rælingen",
                husnr = "92/242",
                adresselinje1 = "Rælingen, 92/242",
                adresselinje2 = null,
                adresselinje3 = null,
                postnr = "2008",
                poststed = "FJERDINGBY",
                landkode = null,
                //landnavn = _landnavn, //Kommer
                bokstav = null,
            };
            Hellandtunet = new Adresse()
            {
                gatenavn = "Kyrkjevegen",
                husnr = "6",
                adresselinje1 = "Kyrkjevegen 6",
                adresselinje2 = null,
                adresselinje3 = null,
                postnr = "3800",
                poststed = "Bø i Telemark",
                landkode = "NO",
                //landnavn = _landnavn, //Kommer
                bokstav = null,
            };
            StorGate = new Adresse()
            {
                gatenavn = "Storgate",
                husnr = "3",
                adresselinje1 = "Storgate 3",
                adresselinje2 = null,
                adresselinje3 = null,
                postnr = "3800",
                poststed = "Bø i Telemark",
                landkode = "NO",
                //landnavn = _landnavn, //Kommer
                bokstav = null,
            };
            OperaHouse = new Adresse()
            {
                gatenavn = "Kirsten Flagstads plass",
                husnr = "1",
                adresselinje1 = "Kirsten Flagstads plass 1",
                adresselinje2 = null,
                adresselinje3 = null,
                postnr = "0150",
                poststed = "Oslo",
                landkode = "NO",
                //landnavn = _landnavn, //Kommer
                bokstav = null,
            };
            UtlandetAdresseDk = new Adresse()
            {
                gatenavn = "Refshalevej",
                husnr = "96",
                adresselinje1 = "Refshalevej 96",
                adresselinje2 = null,
                adresselinje3 = null,
                postnr = "1432",
                poststed = "København K",
                landkode = "DK",
                //landnavn = _landnavn, //Kommer
                bokstav = null,
            };
        }

        public class Adresse
        {
            public string adresselinje1 { get; set; }
            public string adresselinje2 { get; set; }
            public string adresselinje3 { get; set; }
            public string postnr { get; set; }
            public string poststed { get; set; }

            public string landkode { get; set; }

            //public string landnavn { get; set; } kommer
            public string gatenavn { get; set; }
            public string husnr { get; set; }
            public string bokstav { get; set; }
        }
    }
}