﻿using Dibk.Ftpb.Common.Datamodels;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Altinn;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services
{
    public class FormValidationService
    {
        public ValidationResult ValidateFormStructure(string dataFormatId, string dataFormatVersion, string formDataAsXml)
        {
            ValidationResult valResult = new ValidationResult(0, 0);
            string xsdSchema = "";

            var instances = AltinnFormTypeResolver.GetForms();

            foreach (var item in instances)
            {
                if (item.GetDataFormatId() == dataFormatId && item.GetDataFormatVersion() == dataFormatVersion)
                {
                    xsdSchema = item.GetSchemaFile();
                }
            }

            //var xml = new XmlUtil();
            //var xsdStream = XsdResolver.GetXsd(xsdSchema);
            //valResult = xml.Validate(new MemoryStream(Encoding.UTF8.GetBytes(formDataAsXml)), xsdStream);

            var xmlValidator = new XmlValidator();
            using (var xsdStream = XsdResolver.GetXsd(xsdSchema))
            {
                using (var xmlStream = new MemoryStream(Encoding.UTF8.GetBytes(formDataAsXml)))
                {
                    valResult = xmlValidator.Validate(xmlStream, xsdStream);
                }
            }

            return valResult;
        }

        public ValidationResult ValidateFormData(string dataFormatId, string dataFormatVersion, string formDataAsXml, List<string> attachmenttypesAndForms)
        {
            ValidationResult valResult = new ValidationResult(0, 0);

            var instances = AltinnFormTypeResolver.GetForms();

            foreach (var item in instances)
            {
                if (item.GetDataFormatId() == dataFormatId && item.GetDataFormatVersion() == dataFormatVersion)
                {
                    item.InitiateForm(formDataAsXml);
                    item.SetFormSetElements(attachmenttypesAndForms);
                    valResult = item.ValidateData();
                }
            }
            return valResult;
        }
    }
}