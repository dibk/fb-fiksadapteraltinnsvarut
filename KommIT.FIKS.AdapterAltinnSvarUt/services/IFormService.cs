﻿using System.Collections.Generic;
using KommIT.FIKS.AdapterAltinnSvarUt.Models;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services
{
    public interface IFormService
    {
        List<Form> GetDefaultForms();
        Form Get(int formId);
    }
}
