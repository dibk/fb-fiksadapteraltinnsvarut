﻿using System.Collections.Generic;
using System.Configuration;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services
{
    public class PolicyConfigurationProvider : IPolicyConfigurationProvider
    {
        public virtual List<int> RetryAttemptWaitSecondsStructure => GetRetryStructure();

        public virtual string RetryStructureSettingKey { get { return "Polly:RetryStructure"; } }

        private List<int> GetRetryStructure()
        {
            var retVal = new List<int>();
            var retryStructureString = ConfigurationManager.AppSettings[RetryStructureSettingKey];
            string[] separator = { ";" };

            if (string.IsNullOrEmpty(retryStructureString))
            {
                //Defaults to standard settings
                retryStructureString = "10;30;60;120;180";
            }

            string[] retryArray = retryStructureString.Split(separator, System.StringSplitOptions.RemoveEmptyEntries);

            for (int i = 0; i < retryArray.Length; i++)
            {
                int result;
                if (int.TryParse(retryArray[i], out result))
                {
                    retVal.Add(result);
                }
            }
            return retVal;
        }
    }
}