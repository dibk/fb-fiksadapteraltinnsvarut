﻿using System.Collections.Generic;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services
{
    public interface IPolicyConfigurationProvider
    {
        List<int> RetryAttemptWaitSecondsStructure { get; }
    }
}