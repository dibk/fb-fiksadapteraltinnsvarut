﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.NotificationForm;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.NotificationForm.Nabovarsel;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services
{
    public class SyncRecordsOfCombinedDistributions : ISyncRecordsOfCombinedDistributions
    {
        private readonly IFormMetadataService _formMetadataService;
        private readonly ILogEntryService _logEntryService;

        public SyncRecordsOfCombinedDistributions(IFormMetadataService formMetadataService, ILogEntryService logEntryService)
        {
            _formMetadataService = formMetadataService;
            _logEntryService = logEntryService;
        }


        public void Sync(IAltinnForm altinnForm, DistributionForm dForm)
        {
            if (altinnForm is INabovarselSvar)
            {
                AdjustTheRecords(dForm);
            }
        }


        public void Sync(INotificationForm notificationForm, DistributionForm dForm)
        {
            if (notificationForm is INabovarselSvar)
            {
                AdjustTheRecords(dForm);
            }
        }

        public void Sync(IAltinnForm altinnForm, Guid internalReference)
        {
            if (altinnForm is INabovarselSvar)
            {
                DistributionForm dForm = _formMetadataService.GetDistributionFormByGuid(internalReference);
                if (dForm != null)
                {
                    AdjustTheRecords(dForm);
                }
            }
        }

        private void AdjustTheRecords(DistributionForm dForm)
        {
            // Get list of distributions with the dForm's DistributionReference
            List<DistributionForm> distForms = _formMetadataService.GetDistributionFormsForDistributionReference(dForm.Id);

            if (distForms.Count > 1)
            {
                // Extract the "dummy" records that have to be duplicated with data from the main record
                var recordsToBeUpdated = distForms.FindAll(p => p.Id != p.DistributionReference).ToList();
                foreach (var record in recordsToBeUpdated)
                {
                    record.SubmitAndInstantiatePrefilledFormTaskReceiptId = dForm.SubmitAndInstantiatePrefilledFormTaskReceiptId;
                    record.SignedArchiveReference = dForm.SignedArchiveReference;
                    record.SubmitAndInstantiatePrefilled = dForm.SubmitAndInstantiatePrefilled;
                    record.Signed = dForm.Signed;
                    record.DistributionStatus = dForm.DistributionStatus;
                    record.RecieptSentArchiveReference = dForm.RecieptSentArchiveReference;
                    record.RecieptSent = dForm.RecieptSent;
                    record.ErrorMessage = dForm.ErrorMessage;
                    record.InitialExternalSystemReference = dForm.InitialExternalSystemReference;
                    record.Printed = dForm.Printed;
                    _formMetadataService.SaveDistributionForm(record);
                }
            }
        }

    }



    /// <summary>
    /// *** INTERFACE ***
    /// </summary>
    public interface ISyncRecordsOfCombinedDistributions
    {
       void Sync(IAltinnForm altinnForm, DistributionForm dForm);
       void Sync(IAltinnForm altinnForm, Guid internalReference);
       void Sync(INotificationForm notificationForm, DistributionForm dForm);
    }

}