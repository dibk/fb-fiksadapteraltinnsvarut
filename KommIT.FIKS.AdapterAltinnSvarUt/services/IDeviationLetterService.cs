using System;
using System.Collections.Generic;
using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Models.ApiModels;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.WS.Altinn;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services
{
    public interface IDeviationLetterService
    {
        void SaveDeviationLetter(DeviationLetter dvl);
        List<DeviationLetter> GetListOfDeviationLettersByArchiveReference(string archiveReference);
        List<DeviationLetter> GetListOfDeviationLettersByCaseYearAndSequence(string kommunenr, int saksaar, long sakssekvensnummer);
        List<DeviationLetter> GetListOfDeviationLettersByArchiveYearAndSequence(string kommunenr, long journalsekvensnummer, int journalaar);
    }
}
