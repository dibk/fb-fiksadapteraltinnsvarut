﻿using System;
using System.Text;
using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Models.Signatur;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.DecryptFnrForTh
{
    public class DecryptFnr : IDecryptFnr
    {
        private readonly ITiltakshaverSamtykkeService _tiltakshaverSamtykkeService;

        public DecryptFnr(ITiltakshaverSamtykkeService tiltakshaverSamtykkeService)
        {
            _tiltakshaverSamtykkeService = tiltakshaverSamtykkeService;
        }

        public Attachment DecryptFnrForTh(Guid signatureIdGuid, string archiveReference)
        {
            var thFnr = _tiltakshaverSamtykkeService.GetReporteePersonalNumberFromSamtykke(signatureIdGuid);
            if (string.IsNullOrEmpty(thFnr) || string.IsNullOrWhiteSpace(archiveReference))
            { 
                return null;
            }
            return GenerateAttachment(thFnr, archiveReference);
        }

        public bool HasPersonalNumberInformation(Guid signatureIdGuid)
        {
            return _tiltakshaverSamtykkeService.PersonalNumberExistForTiltakshaverId(signatureIdGuid);
        }

        public bool SignatureDataExists(Guid signatureIdGuid)
        {
            bool output = false;

            var approval = _tiltakshaverSamtykkeService.GetTiltakshaversSamtykke(signatureIdGuid);

            if (approval != null && approval.Status == TiltakshaverSamtykkeConstants.Arkivert)
            {
                output = true;
            }


            return output;
        }

        public Attachment GenerateAttachment(string fnr, string archiveReference)
        {
            var decryptedFnr = new DecryptedFnr();
            decryptedFnr.Tiltakshaver = new Tiltakshaver { Foedselsnummer = fnr };

            var xmlfile = SerializeUtil.Serialize(decryptedFnr);
            byte[] xmlBytes = Encoding.ASCII.GetBytes(xmlfile);

            return new Attachment(archiveReference,
                                xmlBytes,
                                "application/xml",
                                Dibk.Ftpb.Common.Constants.DataTypes.DekryptertFoedselsnummerTiltakshaver,
                                "Tiltakshavers fødselsnummer-UNTATT-OFFENTLIGHET.xml",
                                false);
        }
    }
}