﻿using KommIT.FIKS.AdapterAltinnSvarUt.Models;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.DecryptFnrForTh
{
    public interface ITiltakshaversSignedApprovalDocument
    {
        Attachment GetSignedAttachment(string signatureIdGuid, string formArchiveReference);
    }
}
