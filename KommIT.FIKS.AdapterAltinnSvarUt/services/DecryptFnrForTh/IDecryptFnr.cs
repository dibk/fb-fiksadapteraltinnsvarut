﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KommIT.FIKS.AdapterAltinnSvarUt.Models;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.DecryptFnrForTh
{
    public interface IDecryptFnr
    {
        Attachment DecryptFnrForTh(Guid signatureIdGuid, string archiveReference);
        bool HasPersonalNumberInformation(Guid signatureIdGuid);

        bool SignatureDataExists(Guid signatureIdGuid);
    }
}
