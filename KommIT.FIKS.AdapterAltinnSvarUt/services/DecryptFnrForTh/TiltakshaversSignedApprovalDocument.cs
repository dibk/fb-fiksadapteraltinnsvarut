﻿using Dibk.Ftpb.Common.Constants;
using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Models.ApiModels;
using KommIT.FIKS.AdapterAltinnSvarUt.Models.Signatur;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Azure;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.DecryptFnrForTh
{
    public class TiltakshaversSignedApprovalDocument : ITiltakshaversSignedApprovalDocument
    {
        private readonly IFileStorage _fileStorage;
        private readonly IFileDownloadStatusService _fileDownloadStatusService;
        private readonly ITiltakshaverSamtykkeService _tiltakshaverSamtykkeService;

        public TiltakshaversSignedApprovalDocument(IFileStorage fileStorage,
                                                   IFileDownloadStatusService fileDownloadStatusService,
                                                   ITiltakshaverSamtykkeService tiltakshaverSamtykkeService)
        {
            _fileStorage = fileStorage;
            _fileDownloadStatusService = fileDownloadStatusService;
            _tiltakshaverSamtykkeService = tiltakshaverSamtykkeService;
        }

        public Attachment GetSignedAttachment(string signatureIdGuid, string formArchiveReference)
        {
            Attachment attachment = null;

            //Find signed AR from the TH sig database
            var thSigData = _tiltakshaverSamtykkeService.GetTiltakshaversSamtykke(signatureIdGuid);

            // Get the BLOB storage information for the stored attachment
            if (thSigData != null && thSigData.Status == TiltakshaverSamtykkeConstants.Arkivert)
            {
                var fileDownloadStatus = FindDocumentRecord(thSigData.AltinnArkivreferanse);

                if (fileDownloadStatus != null)
                {
                    byte[] dataBytes = ReadBlobDocument(fileDownloadStatus);

                    attachment = new Attachment(formArchiveReference,
                                                dataBytes,
                                                "application/pdf",
                                                DataTypes.TiltakshaverSignatur,
                                                "TiltakshaverSignatur.pdf",
                                                false);
                }
            }

            return attachment;
        }

        private FileDownloadStatus FindDocumentRecord(string archiveReferences)
        {
            return (_fileDownloadStatusService.GetFileDownloadStatusForArchiveReferencesAndFileType(archiveReferences, FilTyperForNedlasting.SkjemaPdf));
        }

        private byte[] ReadBlobDocument(FileDownloadStatus fileDownloadStatus)
        {
            return _fileStorage.GetFile(fileDownloadStatus.Guid.ToString(), fileDownloadStatus.Filename);
        }
    }
}