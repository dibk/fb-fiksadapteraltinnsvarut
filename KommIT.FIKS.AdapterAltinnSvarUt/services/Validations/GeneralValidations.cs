using DIBK.FBygg.Altinn.MapperCodelist;
using KommIT.FIKS.AdapterAltinnSvarUt.Extensions;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Encryption;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Matrikkel;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.MunicipalityApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Http;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Validations
{
    public interface IMunicipalityValidator
    {
        GeneralValidationResult ValidateKommunenummer(string kommunenummer);
        GeneralValidationResult ValidateKommunenummerExists(string kommunenummer);
        MunicipalityValidationResult Validate_kommunenummerStatus(string kommunenummer);
    }

    public enum MunicipalityValidation
    {
        Ok,
        Empty,
        Invalid,
        Expired,
        TooSoon
    }

    public class MunicipalityValidationResult
    {
        public MunicipalityValidation Status { get; set; }
        public string Message { get; set; }
    }
    public class MunicipalityValidator : IMunicipalityValidator, IDisposable
    {
        private System.Web.Http.Dependencies.IDependencyScope _scope;
        private MunicipalityCacheService _municipalityCacheService;
        public MunicipalityValidator()
        {
            _scope = GlobalConfiguration.Configuration.DependencyResolver.BeginScope();
            _municipalityCacheService = _scope.GetService(typeof(MunicipalityCacheService)) as MunicipalityCacheService;

        }

        // ### KOMMUNENUMMER validation ###
        public GeneralValidationResult ValidateKommunenummer(string kommunenummer)
        {
            if (string.IsNullOrEmpty(kommunenummer))
            {
                return new GeneralValidationResult(ValidationStatus.Fail, "Kommunenummer er tomt");
            }

            GeneralValidationResult result = null;
            Models.Municipality municipality = GetMunicipality(kommunenummer);

            if (municipality == null)
                result = new GeneralValidationResult(ValidationStatus.Fail, $"Ugyldig kommunenummer ({kommunenummer})");
            else
            {
                if (municipality.ValidTo.HasValue && DateTime.Now > municipality.ValidTo.Value)
                    result = new GeneralValidationResult(ValidationStatus.Fail, $"Ugyldig kommunenummer ({ kommunenummer }). Nytt kommunenummer er: {municipality.NewMunicipalityCode}");
                else if (municipality.ValidFrom.HasValue && DateTime.Now < municipality.ValidFrom.Value)
                    result = new GeneralValidationResult(ValidationStatus.Fail, $"Ugyldig kommunenummer ({ kommunenummer }). Kommunenummeret er ikke gyldig før {municipality.ValidFrom.Value.ToShortDateString()}");
                else
                    result = new GeneralValidationResult(ValidationStatus.Ok, "Ok");
            }

            return result;
        }

        // ### KOMMUNENUMMER validation ###
        public GeneralValidationResult ValidateKommunenummerExists(string kommunenummer)
        {
            if (string.IsNullOrEmpty(kommunenummer))
            {
                return new GeneralValidationResult(ValidationStatus.Fail, "Kommunenummer er tomt");
            }

            GeneralValidationResult result = null;
            Models.Municipality municipality = GetMunicipality(kommunenummer);

            if (municipality == null)
                result = new GeneralValidationResult(ValidationStatus.Fail, $"Ugyldig kommunenummer ({kommunenummer})");

            return result ?? (new GeneralValidationResult(ValidationStatus.Ok, "Ok"));
        }

        private Models.Municipality GetMunicipality(string kommunenummer)
        {
            return _municipalityCacheService.GetMunicipality(kommunenummer);
        }

        public MunicipalityValidationResult Validate_kommunenummerStatus(string kommunenummer)
        {

            if (string.IsNullOrEmpty(kommunenummer))
            {
                return new MunicipalityValidationResult() { Status = MunicipalityValidation.Empty, Message = string.Empty };
            }

            MunicipalityValidationResult result = new MunicipalityValidationResult();

            //var municipality = new MunicipalityApiService().GetMunicipality(kommunenummer);
            Models.Municipality municipality = GetMunicipality(kommunenummer);

            if (municipality == null)
            {
                result.Status = MunicipalityValidation.Invalid;
                result.Message = string.Empty;
            }
            else if (municipality.ValidTo.HasValue && DateTime.Now > municipality.ValidTo.Value)
            {
                result.Status = MunicipalityValidation.Expired;
                result.Message = municipality.NewMunicipalityCode;
            }
            else if (municipality.ValidFrom.HasValue && DateTime.Now < municipality.ValidFrom.Value)
            {
                result.Status = MunicipalityValidation.TooSoon;
                result.Message = municipality.ValidFrom.Value.ToShortDateString();
            }
            else
            {
                result.Status = MunicipalityValidation.Ok;
                result.Message = "ok";
            }

            return result;

        }

        public void Dispose()
        {
            _municipalityCacheService = null;

            if (_scope != null)
                _scope.Dispose();
        }
    }



    public class GeneralValidations
    {

        public enum FoedselnumerValidation
        {
            Ok,
            Empty,
            Invalid,
            InvalidDigitsControl,
            InvalidEncryption
        }
        public enum OrganisasjonsnummerValidation
        {
            Ok,
            Empty,
            Invalid,
            InvalidDigitsControl,
        }

        // ### ORGNUMMER validation ###
        public static Match OrgnummerDigitsValidation(string orgnum)
        {
            if (string.IsNullOrEmpty(orgnum))
                return Match.Empty;

            Match isOrgNumValid = Regex.Match(orgnum, "^([0-9])([0-9])([0-9])([0-9])([0-9])([0-9])([0-9])([0-9])([0-9])$");
            return isOrgNumValid;
        }
        public static bool OrgnummerDigitsControlValidation(Match isOrgNumValid)
        {
            if (isOrgNumValid == null)
                return false;
            int products = Convert.ToInt32(isOrgNumValid.Groups[1].Value) * 3 +
                           Convert.ToInt32(isOrgNumValid.Groups[2].Value) * 2 +
                           Convert.ToInt32(isOrgNumValid.Groups[3].Value) * 7 +
                           Convert.ToInt32(isOrgNumValid.Groups[4].Value) * 6 +
                           Convert.ToInt32(isOrgNumValid.Groups[5].Value) * 5 +
                           Convert.ToInt32(isOrgNumValid.Groups[6].Value) * 4 +
                           Convert.ToInt32(isOrgNumValid.Groups[7].Value) * 3 +
                           Convert.ToInt32(isOrgNumValid.Groups[8].Value) * 2;

            int controlDigit = 11 - (products % 11);
            if (controlDigit == 11)
            {
                controlDigit = 0;
            }
            return controlDigit == Convert.ToInt32(isOrgNumValid.Groups[9].Value);
        }

        public static OrganisasjonsnummerValidation Validate_OrgnummerEnum(string organisasjonsnummer)
        {
            if (String.IsNullOrEmpty(organisasjonsnummer))
            {
                return OrganisasjonsnummerValidation.Empty;
            }
            else
            {
                // Org number valid format: 
                //    https://www.brreg.no/om-oss-nn/oppgavene-vare/registera-vare/om-einingsregisteret/organisasjonsnummeret/
                var orgNumerRegexMatch = OrgnummerDigitsValidation(organisasjonsnummer);

                if (orgNumerRegexMatch.Success)
                {
                    if (!OrgnummerDigitsControlValidation(orgNumerRegexMatch))
                    {
                        return OrganisasjonsnummerValidation.Invalid;
                    }
                }
                else
                {
                    return OrganisasjonsnummerValidation.InvalidDigitsControl;
                }
            }
            return OrganisasjonsnummerValidation.Ok;
        }



        public static GeneralValidationResult Validate_orgnummer(string orgnum)
        {
            // Org number valid format: 
            //    https://www.brreg.no/om-oss/oppgavene-vare/alle-registrene-vare/om-enhetsregisteret/organisasjonsnummeret/
            GeneralValidationResult result;

            if (!String.IsNullOrEmpty(orgnum))
            {
                Match isOrgNumValid =
                Regex.Match(orgnum, "^([0-9])([0-9])([0-9])([0-9])([0-9])([0-9])([0-9])([0-9])([0-9])$");

                if (isOrgNumValid.Success)
                {
                    int products = Convert.ToInt32(isOrgNumValid.Groups[1].Value) * 3 +
                                   Convert.ToInt32(isOrgNumValid.Groups[2].Value) * 2 +
                                   Convert.ToInt32(isOrgNumValid.Groups[3].Value) * 7 +
                                   Convert.ToInt32(isOrgNumValid.Groups[4].Value) * 6 +
                                   Convert.ToInt32(isOrgNumValid.Groups[5].Value) * 5 +
                                   Convert.ToInt32(isOrgNumValid.Groups[6].Value) * 4 +
                                   Convert.ToInt32(isOrgNumValid.Groups[7].Value) * 3 +
                                   Convert.ToInt32(isOrgNumValid.Groups[8].Value) * 2;

                    int controlDigit = 11 - (products % 11);
                    if (controlDigit == 11)
                    {
                        controlDigit = 0;
                    }

                    if (controlDigit == Convert.ToInt32(isOrgNumValid.Groups[9].Value))
                    {
                        result = new GeneralValidationResult(ValidationStatus.Ok, "Ok");
                    }
                    else
                    {
                        result = new GeneralValidationResult(ValidationStatus.Fail, "Organisasjonsnummeret (" + orgnum + ") har ikke gyldig kontrollsiffer");
                    }
                }
                else
                {
                    result = new GeneralValidationResult(ValidationStatus.Fail, "Organisasjonsnummeret (" + orgnum + ") er ikke gyldig");
                }
            }
            else
            {
                result = new GeneralValidationResult(ValidationStatus.Fail, "Organisasjonsnummeret mangler");
            }

            return result;
        }


        //// ### KOMMUNENUMMER validation ###
        //public static GeneralValidationResult Validate_kommunenummer(string kommunenummer)
        //{
        //    if (string.IsNullOrEmpty(kommunenummer))
        //    {
        //        return new GeneralValidationResult(ValidationStatus.Fail, "Kommunenummer er tomt");
        //    }

        //    GeneralValidationResult result = null;

        //    var municipality = new MunicipalityApiService().GetMunicipality(kommunenummer);

        //    if (municipality == null)
        //        result = new GeneralValidationResult(ValidationStatus.Fail, $"Ugyldig kommunenummer ({kommunenummer})");
        //    else
        //    {
        //        if (municipality.ValidTo.HasValue && DateTime.Now > municipality.ValidTo.Value)
        //            result = new GeneralValidationResult(ValidationStatus.Fail, $"Ugyldig kommunenummer({ kommunenummer }). Nytt kommunenummer er: {municipality.NewMunicipalityCode}");
        //        else if (municipality.ValidFrom.HasValue && DateTime.Now < municipality.ValidFrom.Value)
        //            result = new GeneralValidationResult(ValidationStatus.Fail, $"Ugyldig kommunenummer({ kommunenummer }). Kommunenummeret er ikke gyldig før {municipality.ValidFrom.Value.ToShortDateString()}");
        //        else
        //            result = new GeneralValidationResult(ValidationStatus.Ok, "Ok");
        //    }

        //    return result;
        //}

        ////### Get validation for KOMMUNENUMMER Status
        //public static GeneralValidationResult Validate_kommunenummerStatus(string kommunenummer)
        //{
        //    GeneralValidationResult result = null;

        //    Dictionary<string, CodelistFormat> codeData = new CodeListService().GetCodelist("kommunenummer", RegistryType.Kommunenummer);
        //    if (codeData != null && codeData.Any())
        //    {
        //        CodelistFormat kommune;
        //        if (codeData.TryGetValue(kommunenummer, out kommune))
        //        {
        //            switch (kommune.Status)
        //            {
        //                case "Sendt inn":
        //                    result = new GeneralValidationResult(ValidationStatus.Ok, "Sendt inn");
        //                    break;
        //                case "Gyldig":
        //                    result = new GeneralValidationResult(ValidationStatus.Ok, "Gyldig");
        //                    break;
        //                case "Utgått":
        //                    result = new GeneralValidationResult(ValidationStatus.Warning, codeData[kommunenummer].Description);
        //                    break;
        //            }
        //        }
        //        else
        //        {
        //            if (TestData.IsValidTestKommune(kommunenummer))
        //            {
        //                result = new GeneralValidationResult(ValidationStatus.Ok, "Testkommune");
        //            }
        //        }
        //    }
        //    return result;
        //}

        // ### FODSELSNUMMER validation ###
        public static GeneralValidationResult Validate_fodselsnummer(string fodselsnummer)
        {

            ////TODO: Kan validere sjekksum - https://no.wikipedia.org/wiki/F%C3%B8dselsnummer 


            GeneralValidationResult result;
            if (!String.IsNullOrEmpty(fodselsnummer))
            {
                //try
                //{

                if (fodselsnummer.Length > 11)
                {
                    if (!Decryption.Instance.TryDecryptText(fodselsnummer, out fodselsnummer))
                        return new GeneralValidationResult(ValidationStatus.Fail, $"Fødselsnummer kan ikke dekrypteres");
                }
                //}
                //catch (Exception ex)
                //{
                //    //TODO loger - Fødselsnummer kan ikke dekrypteres
                //    //result = new GeneralValidationResult(ValidationStatus.Fail, $"Fødselsnummeret er ikke gyldig, feilmelding: {ex.Message}");
                //    return new GeneralValidationResult(ValidationStatus.Fail, $"Fødselsnummer kan ikke dekrypteres");
                //}

                Match isFNrValid = Regex.Match(fodselsnummer, "^([0-9])([0-9])([0-9])([0-9])([0-9])([0-9])([0-9])([0-9])([0-9])([0-9])([0-9])$");

                if (isFNrValid.Success)
                {

                    int productsFirstControlDigit = ControlDigitCheck(new[] { 3, 7, 6, 1, 8, 9, 4, 5, 2 }, isFNrValid);

                    int firstControlDigit = 11 - (productsFirstControlDigit % 11);

                    if (firstControlDigit == 11)
                    {
                        firstControlDigit = 0;
                    }

                    int productsSecondControlDigit = ControlDigitCheck(new[] { 5, 4, 3, 2, 7, 6, 5, 4, 3 }, isFNrValid, firstControlDigit, 2);

                    int secondControlDigit = 11 - (productsSecondControlDigit % 11);

                    if (secondControlDigit == 11)
                    {
                        secondControlDigit = 0;
                    }

                    if (firstControlDigit == Convert.ToInt32(isFNrValid.Groups[10].Value) && secondControlDigit == Convert.ToInt32(isFNrValid.Groups[11].Value))
                    {
                        result = new GeneralValidationResult(ValidationStatus.Ok, "Ok");
                    }
                    else
                    {
                        result = new GeneralValidationResult(ValidationStatus.Fail, "Fødselsnummeret har ikke gyldig kontrollsiffer");
                    }
                }
                else
                {
                    //TODO "Fødselsnummeret er ikke en numerisk verdi."?
                    result = new GeneralValidationResult(ValidationStatus.Fail, "Fødselsnummeret er ikke gyldig");
                }
            }
            else
            {
                result = new GeneralValidationResult(ValidationStatus.Fail, "Fødselsnummeret mangler");
            }
            return result;
        }


        public static FoedselnumerValidation Validate_foedselsnummer(string foedselsnummer)
        {
            if (string.IsNullOrEmpty(foedselsnummer))
            {
                return FoedselnumerValidation.Empty;
            }
            foedselsnummer = GeneralValidations.GetDecryptedFoedselnummer(foedselsnummer);

            if (string.IsNullOrEmpty(foedselsnummer))
                return FoedselnumerValidation.InvalidEncryption;

            var isfoedselsnummerMatch = GeneralValidations.FoedselnumerDigitsValidation(foedselsnummer);
            if (!isfoedselsnummerMatch.Success)
                return FoedselnumerValidation.InvalidDigitsControl;

            if (!FoedselsnummerDigitsControl(isfoedselsnummerMatch))
                return FoedselnumerValidation.Invalid;

            return FoedselnumerValidation.Ok;
        }



        public static string GetDecryptedFoedselnummer(string fodselsnummer)
        {
            //try
            //{
            if (fodselsnummer.Length > 11)
            {
                //fodselsnummer = Decryption.Instance.DecryptText(fodselsnummer);
                if (!Decryption.Instance.TryDecryptText(fodselsnummer, out fodselsnummer))
                    return null;
            }

            return fodselsnummer;
            //}
            //catch
            //{
            //    //TODO loger - Fødselsnummer kan ikke dekrypteres
            //    return null;  //"Fødselsnummer kan ikke dekrypteres"
            //}
        }

        public static Match FoedselnumerDigitsValidation(string foedselsnummer)
        {
            if (string.IsNullOrWhiteSpace(foedselsnummer))

                return Match.Empty;
            Match isFNrValid = Regex.Match(foedselsnummer, "^([0-9])([0-9])([0-9])([0-9])([0-9])([0-9])([0-9])([0-9])([0-9])([0-9])([0-9])$");
            return isFNrValid;
        }

        public static bool FoedselsnummerDigitsControl(Match isFNrValid)
        {
            int productsFirstControlDigit = ControlDigitCheck(new[] { 3, 7, 6, 1, 8, 9, 4, 5, 2 }, isFNrValid);

            int firstControlDigit = 11 - (productsFirstControlDigit % 11);

            if (firstControlDigit == 11)
            {
                firstControlDigit = 0;
            }

            int productsSecondControlDigit = ControlDigitCheck(new[] { 5, 4, 3, 2, 7, 6, 5, 4, 3 }, isFNrValid, firstControlDigit, 2);

            int secondControlDigit = 11 - (productsSecondControlDigit % 11);

            if (secondControlDigit == 11)
            {
                secondControlDigit = 0;
            }

            if (firstControlDigit == Convert.ToInt32(isFNrValid.Groups[10].Value) && secondControlDigit == Convert.ToInt32(isFNrValid.Groups[11].Value))
            {
                return true;
            }
            else
            {
                return false; //"Fødselsnummeret har ikke gyldig kontrollsiffer"
            }
        }

        private static int ControlDigitCheck(int[] weightCoefficients, Match isNumberValid, int productsFirstControlDigit = 0, int coefficientForFirstControlNumber = 0)
        {
            int products = 0;

            for (int i = 1; i <= weightCoefficients.Length; i++)
            {
                products += Convert.ToInt32(isNumberValid.Groups[i].Value) * weightCoefficients[i - 1];

            }

            if (coefficientForFirstControlNumber != 0)
            {
                products += productsFirstControlDigit * coefficientForFirstControlNumber;
            }
            return products;
        }

        /// <summary>
        /// Validerer at bruksenhetsnummer er gyldig (ex H0101)
        /// </summary>
        /// <param name="bruksenhetsnr"></param>
        /// <returns></returns>
        public static GeneralValidationResult Validate_bruksenhetsnummer(string bruksenhetsnr)
        {
            GeneralValidationResult result;

            if (!Regex.Match(bruksenhetsnr, @"^[HKLU]\d{4}$").Success)
            {
                result = new GeneralValidationResult(ValidationStatus.Fail, "Bruksenhetsnummer er ikke gyldig");
            }
            else
            {
                result = new GeneralValidationResult(ValidationStatus.Ok, "Success");
            }
            return result;
        }
        //**PostNumer

        public static Match postNumberMatch(string postNr)
        {
            if (string.IsNullOrEmpty(postNr))
                return Match.Empty;
            Match isPostNrValid = Regex.Match(postNr, "^([0-9])([0-9])([0-9])([0-9])$");
            return isPostNrValid;
        }

        public static GeneralValidationResult Validate_postnummer(string postNr, string landkode, bool postNrIsWarning = true)
        {
            GeneralValidationResult result = null;
            if (!landkode.IsNullOrEmpty() && !landkode.ToUpper().Equals("NO") && !landkode.Equals("0"))
                return null;

            if (!postNr.IsNullOrEmpty()) // && !landkode.IsNullOrEmpty()
            {
                Match isPostNrValid = Regex.Match(postNr, "^([0-9])([0-9])([0-9])([0-9])$");

                if (isPostNrValid.Success)
                {
                    result = new GeneralValidationResult(ValidationStatus.Ok, "Ok");
                }
                else
                {
                    result = new GeneralValidationResult(ValidationStatus.Fail, "Ugyldig postnummer");
                }
            }
            else
            {
                result = postNrIsWarning ? new GeneralValidationResult(ValidationStatus.Warning, "Postnummer bør være utfylt") : new GeneralValidationResult(ValidationStatus.Fail, "Postnummer må fylles ut");
            }

            return result;
        }
        public static GeneralValidationResult Validate_postnummerAndPoststed(string postNr, string poststed, string landkode, bool postNrIsWarning = true)
        {
            GeneralValidationResult result;
            if (!landkode.IsNullOrEmpty() && !landkode.ToUpper().Equals("NO") && !landkode.Equals("0"))
                return new GeneralValidationResult(ValidationStatus.Fail, "Kan ikke validere utenlandsk landkode");

            if (!postNr.IsNullOrEmpty()) // && !landkode.IsNullOrEmpty()
            {
                Match isPostNrValid = Regex.Match(postNr, "^([0-9])([0-9])([0-9])([0-9])$");

                if (isPostNrValid.Success)
                {
                    var postalCodeProvider = new PostalCode.PostalCodeProviderFactory().GetPostalCodeProvider();
                    var postnrValidation = postalCodeProvider.ValidatePostnr(postNr, landkode);
                    if (postnrValidation != null)
                    {
                        if (postnrValidation.Valid)
                        {
                            if (postnrValidation.Result.Equals(poststed, StringComparison.CurrentCultureIgnoreCase))
                            {
                                result = new GeneralValidationResult(ValidationStatus.Ok, "Ok");
                            }
                            else
                            {
                                result = new GeneralValidationResult(ValidationStatus.Invalid, $"postnummeret ({postNr}) stemmer ikke overens med poststedet '{poststed}', det burde være '{postnrValidation.Result}'.");
                            }
                        }
                        else
                        {
                            //TODO Ugyldig norsk postnummer {postNr}
                            result = new GeneralValidationResult(ValidationStatus.Invalid, $"Ugyldig postnummer {postNr}");
                        }
                    }
                    else
                    {
                        result = new GeneralValidationResult(ValidationStatus.Warning, $"Postnummeret ({postNr}) kan være riktig, men en teknisk feil gjør at vi ikke kan validere postnummeret.");
                    }
                }
                else
                {
                    //TODO Ugyldig postnummer format {postNr}
                    result = new GeneralValidationResult(ValidationStatus.Invalid, $"Ugyldig postnummer {postNr}");
                }
            }
            else
            {
                result = postNrIsWarning ? new GeneralValidationResult(ValidationStatus.Warning, "Postnummer bør være utfylt") : new GeneralValidationResult(ValidationStatus.Fail, "Postnummer må fylles ut");
            }

            return result;
        }

        /// <summary>
        /// Validates zip-code and country code for Norway. For other countries, always returns true
        /// </summary>
        /// <param name="postalCode"></param>
        /// <param name="landkode"></param>
        /// <returns></returns>
        public static bool IsValidNorwegianPostalCode(string postalCode, string landkode)
        {
            if (!postalCode.IsNullOrEmpty())
            {
                Match isPostNrValid = Regex.Match(postalCode, "^([0-9])([0-9])([0-9])([0-9])$");

                if (isPostNrValid.Success)
                {
                    var isoLandkode = Utils.CountryCodeHandler.ReturnCountryIsoCode(landkode);
                    var postalCodeProvider = new PostalCode.PostalCodeProviderFactory().GetPostalCodeProvider();
                    var postnrValidationResult = postalCodeProvider.ValidatePostnr(postalCode, isoLandkode);

                    if (postnrValidationResult == null) //Unable to validate using Bring
                        return true;

                    return postnrValidationResult.Valid;

                }
                return false;
            }
            else
                return false;
        }

        public static bool IsAddressValidForPrint(Models.Address printReceiverAddress)
        {
            if (Utils.CountryCodeHandler.IsCountryNorway(printReceiverAddress.CountryCode))
            {
                //Må være utfyllt:
                //  Poststed
                //  Postnr
                var postalCodeValid = GeneralValidations.IsValidNorwegianPostalCode(printReceiverAddress.PostalCode, printReceiverAddress.CountryCode);
                var cityValid = !string.IsNullOrEmpty(printReceiverAddress.City);

                return postalCodeValid && cityValid;
            }
            else
            {
                //Må være utfyllt:
                //  Poststed
                //  Landkode
                return !string.IsNullOrEmpty(printReceiverAddress.City) && !string.IsNullOrEmpty(printReceiverAddress.CountryCode);
            }
        }

        public static GeneralValidationResult Validate_EiendomsidentifikasjonGeneralResult(string kommunenummer, string gaardsnummer, string bruksnummer, string festenummer, string seksjonsnummer)
        {

            GeneralValidationResult result = null;

            if (!string.IsNullOrEmpty(gaardsnummer) && !string.IsNullOrEmpty(bruksnummer) && !string.IsNullOrEmpty(kommunenummer))
            {
                try
                {
                    int festenummerInt = 0;
                    int seksjonsnummerInt = 0;
                    int gaardsnumerInt = 0;
                    int bruksnummerInt = 0;

                    if (int.TryParse(gaardsnummer, out gaardsnumerInt) && int.TryParse(bruksnummer, out bruksnummerInt))
                    {
                        int.TryParse(festenummer, out festenummerInt);
                        int.TryParse(seksjonsnummer, out seksjonsnummerInt);

                        if (gaardsnumerInt >= 0 && bruksnummerInt >= 0)
                        {
                            var matrikkelnrExist = new MatrikkelService().MatrikkelnrExist(kommunenummer, gaardsnumerInt, bruksnummerInt, festenummerInt, seksjonsnummerInt);

                            if (matrikkelnrExist.HasValue)
                            {
                                if (matrikkelnrExist.Value)
                                {
                                    result = new GeneralValidationResult(ValidationStatus.Ok, "Ok");
                                }
                                else
                                {
                                    result = new GeneralValidationResult(ValidationStatus.Invalid, $"Eiendommen finnes ikke i Matrikkelen ({kommunenummer}-{gaardsnummer}/{bruksnummer}/{festenummer}/{seksjonsnummer}).");
                                }
                            }
                            else
                            {
                                result = new GeneralValidationResult(ValidationStatus.Fail, "Eiendomsidentifikasjon ble ikke kontrollert mot Matrikkel.");
                            }
                        }
                        else
                        {
                            if (gaardsnumerInt < 0)
                                result = new GeneralValidationResult(ValidationStatus.Invalid, $"Gårdsnummer '{gaardsnummer}' for eiendom/byggested må være '0' eller større.");

                            if (bruksnummerInt < 0)
                                result = new GeneralValidationResult(ValidationStatus.Invalid, $"Bruksnummer '{bruksnummer}' for eiendom/byggested må være '0' eller større.");
                        }
                    }
                    else
                    {
                        result = new GeneralValidationResult(ValidationStatus.Invalid, $"Gårdsnummer og bruksnummer må være tall ({gaardsnummer}/{bruksnummer}).");

                    }
                }
                catch
                {
                    result = new GeneralValidationResult(ValidationStatus.Fail, "Feil i validering av Eiendomsidentifikasjon.");
                }
            }
            else
            {
                result = new GeneralValidationResult(ValidationStatus.Invalid, "Kommunenummer, gårdsnummer og bruksnummer må være utfylt for å validere mot Matrikkel.");
            }
            return result;
        }
        public static bool? Validate_Eiendomsidentifikasjon(string kommunenummer, string gaardsnummer, string bruksnummer, string festenummer, string seksjonsnummer)
        {
            bool? returnValue = null;

            if (!string.IsNullOrEmpty(gaardsnummer) && !string.IsNullOrEmpty(bruksnummer))
            {
                try
                {
                    int festenummerInt = 0;
                    int seksjonsnummerInt = 0;
                    int gaardsnumerInt = 0;
                    int bruksnummerInt = 0;

                    if (int.TryParse(gaardsnummer, out gaardsnumerInt) && int.TryParse(bruksnummer, out bruksnummerInt))
                    {
                        int.TryParse(festenummer, out festenummerInt);
                        int.TryParse(seksjonsnummer, out seksjonsnummerInt);

                        if (gaardsnumerInt >= 0 && bruksnummerInt >= 0)
                        {
                            returnValue = (new MatrikkelService().MatrikkelnrExist(kommunenummer, gaardsnumerInt, bruksnummerInt, festenummerInt, seksjonsnummerInt));
                        }
                    }
                    else
                    {
                        returnValue = false;
                    }
                }
                catch
                {
                    returnValue = null;
                }
            }
            return returnValue;
        }

        public static bool? Validate_Bygningsnr(string bygningsnr, string kommunenummer, string gaardsnummer, string bruksnummer, string festenummer, string seksjonsnummer)
        {
            bool? returnValue = null;

            if (!string.IsNullOrEmpty(gaardsnummer) && !string.IsNullOrEmpty(bruksnummer))
            {
                try
                {
                    int festenummerInt = 0;
                    int seksjonsnummerInt = 0;
                    int gaardsnumerInt = 0;
                    int bruksnummerInt = 0;
                    long bygningsnrLong = 0;
                    if (long.TryParse(bygningsnr, out bygningsnrLong))
                    {
                        if (int.TryParse(gaardsnummer, out gaardsnumerInt) && int.TryParse(bruksnummer, out bruksnummerInt))
                        {
                            int.TryParse(festenummer, out festenummerInt);
                            int.TryParse(seksjonsnummer, out seksjonsnummerInt);

                            if (gaardsnumerInt > 0 && bruksnummerInt > 0)
                            {
                                returnValue = new MatrikkelService().IsBygningOnMatrikkelnr(bygningsnrLong, kommunenummer, gaardsnumerInt, bruksnummerInt, festenummerInt, seksjonsnummerInt);
                            }
                        }
                    }
                    else
                    {
                        returnValue = false;
                    }
                }
                catch
                {
                    returnValue = null;
                }
            }
            return returnValue;
        }

        public static bool? Validate_Adresse(string adressenavn, string husnr, string bokstav, string kommunenummer, string gaardsnummer, string bruksnummer, string festenummer, string seksjonsnummer)
        {
            bool? returnValue = null;

            if (!string.IsNullOrEmpty(gaardsnummer) && !string.IsNullOrEmpty(bruksnummer))
            {
                try
                {
                    int festenummerInt = 0;
                    int seksjonsnummerInt = 0;
                    int gaardsnumerInt = 0;
                    int bruksnummerInt = 0;

                    if (int.TryParse(gaardsnummer, out gaardsnumerInt) && int.TryParse(bruksnummer, out bruksnummerInt))
                    {
                        int.TryParse(festenummer, out festenummerInt);
                        int.TryParse(seksjonsnummer, out seksjonsnummerInt);

                        returnValue = new MatrikkelService().IsVegadresseOnMatrikkelnr(adressenavn, husnr, bokstav, kommunenummer, gaardsnumerInt, bruksnummerInt, festenummerInt, seksjonsnummerInt);
                    }
                }
                catch
                {
                    returnValue = null;
                }
            }
            return returnValue;
        }
    }
}