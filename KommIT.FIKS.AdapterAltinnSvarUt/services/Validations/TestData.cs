﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using DIBK.FBygg.Altinn.MapperCodelist;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Validations
{
    public class TestData
    {
        public static bool IsValidTestKommune(string kommunenummer)
        {
            if (ConfigurationManager.AppSettings["RunInTestMode"] == "true" &&
                (kommunenummer == "9999" || kommunenummer == "9998" || kommunenummer == "9997" ||
                 kommunenummer == "9996" || kommunenummer == "9995" || kommunenummer == "9994"))
            {
                return true;
            }
            return false;
        }
    }
}