﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Validations
{
    public class GeneralValidationResult
    {
        public ValidationStatus Status { get; set; }
        public string Message { get; set; }

        public GeneralValidationResult()
        {
        }

        public GeneralValidationResult(ValidationStatus status, string message)
        {
            Status = status;
            Message = message;
        }
    }

    public enum ValidationStatus
    {
        Ok,
        Warning, 
        Fail,
        Invalid
    }
}