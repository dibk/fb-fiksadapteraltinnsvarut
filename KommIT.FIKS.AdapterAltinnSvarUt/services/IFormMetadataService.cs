using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using System;
using System.Collections.Generic;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services
{
    public interface IFormMetadataService
    {
        /// <summary>
        /// Update with kvitteringsrapport
        /// </summary>
        /// <param name="archiveReference"></param>
        /// <param name="distributionRecieptLink"></param>
        void SaveDistributionRecieptLink(string archiveReference, string distributionRecieptLink);

        void SaveSvarUtShippingInformation(string archiveReference, string forseldelsesId);

        string GetForsendelsesIdByArchiveReference(string archiveReference);

        FormMetadata GetFormMetadata(string archivereference);

        List<FormMetadata> GetFormMetadataListByCaseId(int saksaar, long saksseksvensnummer);

        List<FormMetadata> GetFormMetadataList(int skip, int topLogEntries);

        List<FormMetadata> GetFormMetadataList(int skip, int topLogEntries, string filter);

        int CountFormMetadataList();

        void SaveArchiveReference(string archiveReference, string senderSystem);

        bool SaveFormMetadata(FormMetadata formMetadata);

        /// <summary>
        /// Save result of validation
        /// </summary>
        /// <param name="archiveReference"></param>
        /// <param name="status"></param>
        /// <param name="errors"></param>
        /// <param name="warnings"></param>
        void UpdateValidationResultToFormMetadata(string archiveReference, string status, int errors, int warnings);

        /// <summary>
        /// Update status on message
        /// </summary>
        /// <param name="archiveReference"></param>
        /// <param name="status"></param>
        void UpdateStatusToFormMetadata(string archiveReference, string status);

        /// <summary>
        /// Save servicecode to log
        /// </summary>
        /// <param name="archiveReference"></param>
        /// <param name="servicecode"></param>
        /// <param name="serviceeditioncode"></param>
        void UpdateServiceCodeToFormMetadata(string archiveReference, string servicecode, string serviceeditioncode);

        void SaveFormDataToFormMetadataLog(FormData formData);

        void SaveReceiver(IReceivingAuthority receiver, string archiveReference);

        void SaveMunicipalityArchiveCase(string archiveReference, int municipalityArchiveCaseYear, long municipalityArchiveCaseSequence);

        void SaveDecision(Decision dc);

        List<Decision> GetDecisionListByCaseId(int municipalityArchiveCaseYear, long municipalityArchiveCaseSequence);

        Decision GetDecision(string archivereference);

        DistributionForm InsertDistributionForm(string distributionServiceArchiveReference, string initialExternalSystemReference, string externalSystemReference, string distributionType);

        void InsertDistributionForms(IEnumerable<DistributionForm> distributionForms);

        void SaveDistributionForm(DistributionForm df);

        List<DistributionForm> GetDistributionForm(string archivereference);

        List<DistributionForm> GetDistributionFormsForDistributionReference(Guid distributionReference);

        DistributionForm GetDistributionFormByGuid(Guid id);

        List<DistributionForm> GetDistributionFormByOurReference(string archiveReference, List<string> sluttbrukersystemVaarReferanse);

        string GetMostRecentDistributionForsendelsesIdNotSigned();

        int CountDistributionFormsByStatus(string archivereference, IEnumerable<DistributionStatus> distributionStatuses);

        int CountUniqueDistributions(string archivereference);

        void SaveDistributionFormStatusSubmittedPrefilled(Guid id, DistributionStatus distributionStatus, string submitAndInstantiatePrefilledFormTaskReceiptId, DateTime submitAndInstantiatePrefilled);

        void SaveDistributionFormStatusSubmittedPrefilledError(Guid id, DateTime submitAndInstantiatePrefilled, string errorMessage);

        bool SaveDistributionFormStatusSigned(Guid id, DistributionStatus distributionStatus, DateTime signedDateTime, string signedArchiveReference);

        void SaveDistributionFormStatusNotified(Guid id, DistributionStatus distributionStatus, DateTime notifiedDateTime, string recieptSentArchiveReference);

        void UpdateReferencedDistributions(Guid id, DistributionForm updatedDistributionForm);

        void SaveDistributionFormStatusNotifiedError(Guid id, DateTime notifiedDateTime, string errorMessage);

        bool IsDistributionSent(string ourReference, string archiveReference);

        Boolean IsFormInQueue(string archivereference);

        void SaveDistributionFormStatusSubmittedPrefilledPrinted(Guid id, DistributionStatus distributionStatus, string submitAndInstantiatePrefilledFormTaskReceiptId, DateTime submitAndInstantiatePrefilled);

        PostDistributionMetaData SavePostDistributionReferenceId(Guid distributionFormId, string workflowReferenceId);

        PostDistributionMetaData GetPostDistributionMetaData(Guid distributionFormId);

        List<PostDistributionMetaData> GetPostDistributionMetaDatas(List<Guid> distributionFormIds);

        void SaveDeviationLetter(DeviationLetter dvl);

        bool MetadataExists(string kvitteringAltinnArkivreferanse);

        int RemoveMetadata(string archiveReference);

        void UpdateFormMetadata(FormMetadata formMetadata);

        void AddFtId(string archiveReference, string ftbid);

        void SaveFormAttachmentMetadata(List<FormAttachmentMetadata> formAttachmentMetadata);
    }
}