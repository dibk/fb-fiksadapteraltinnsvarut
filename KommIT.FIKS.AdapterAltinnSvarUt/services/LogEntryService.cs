﻿using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Models.UserAccess;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.SearchEngine;
using System;
using System.Collections.Generic;
using System.Linq;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services
{
    public class LogEntryService : ILogEntryService
    {
        private ApplicationDbContext _dbContext;
        private IApplicationUserService _applicationUserService;
        private readonly ISearchEngineIndexer _searchEngineIndexer;

        public LogEntryService(ApplicationDbContext dbContext, IApplicationUserService applicationUserService, ISearchEngineIndexer searchEngineIndexer)
        {
            _dbContext = dbContext;
            _applicationUserService = applicationUserService;
            _searchEngineIndexer = searchEngineIndexer;
        }

        public void Save(LogEntry logEntry)
        {
            logEntry.Timestamp = DateTime.Now; 

            _dbContext.LogEntries.Add(logEntry);
            _dbContext.SaveChanges();

            _searchEngineIndexer.Add(logEntry);
        }

        public void SaveBulk(IEnumerable<LogEntry> logEntries)
        {
            foreach (var logEntry in logEntries)
            {
                logEntry.Timestamp = DateTime.Now;
                _dbContext.LogEntries.Add(logEntry);
            }
            _dbContext.SaveChanges();

            AsyncHelper.RunSync(async () => await _searchEngineIndexer.AddManyAsync(logEntries));
        }

        public List<LogEntry> GetAllLogEntries()
        {
            var queryResult = from l in _dbContext.LogEntries 
                                orderby l.Timestamp descending
                              select l;
            var temp = queryResult.Take(100); //Timeout utfordring
            return temp.ToList();
        }

        public List<LogEntry> GetLogEntriesByUser(ApplicationUser user)
        {
            List<LogEntry> logEntriesByUser = new List<LogEntry>();
            if (_applicationUserService.IsAdmin())
            {
                return GetAllLogEntries();
            }
            else {
                foreach (var municipality in user.MunicipalitiesToAdministrate)
                {
                    var queryResult = from m in _dbContext.LogEntries
                                      where m.FormMetadata.MunicipalityCode == municipality.Code
                                      select m;

                    foreach (var item in queryResult.ToList())
                    {
                        logEntriesByUser.Add(item);
                    }

                }
                return logEntriesByUser;
            }
        }

        public List<LogEntry> GetLogEntriesByArchiveReference(string archivereference)
        {
            var queryResult = from m in _dbContext.LogEntries
                              where m.ArchiveReference == archivereference
                              orderby m.Timestamp descending 
                              select m;

            return queryResult.ToList();
        }


        public List<LogEntry> GetTopLogEntriesByArchiveReference(string archivereference, int topLogEntries)
        {
            var queryResult = (from m in _dbContext.LogEntries
                              where m.ArchiveReference == archivereference && m.OnlyInternal != true // allows display of only non-restricted messages
                              orderby m.Timestamp descending
                              select m).Take(topLogEntries);

            return queryResult.ToList();
        }
    }
}