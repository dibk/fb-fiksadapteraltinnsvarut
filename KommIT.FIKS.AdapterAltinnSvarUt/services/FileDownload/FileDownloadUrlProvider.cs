﻿using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Models.ApiModels;
using System;
using System.Configuration;
using System.Web;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.FileDownload
{
    public class FileDownloadApiUrlProvider
    {
        public static FilTyperForNedlasting[] ReceiptTypes => new[]
        {
            FilTyperForNedlasting.KvitteringNabovarsel,
            FilTyperForNedlasting.VarselTilBeroerteParterKvittering,
            FilTyperForNedlasting.VarselTilHoeringsmyndigheterKvittering,
            FilTyperForNedlasting.DistribusjonHoeringOgOffentligEttersynKvittering,      
            FilTyperForNedlasting.InnsendingAvReguleringsplanforslagKvittering,
            FilTyperForNedlasting.PlanvarselKvittering,
            FilTyperForNedlasting.SoeknadOmIgangsettingstillatelseKvittering,
        };

        private readonly string _host;

        public FileDownloadApiUrlProvider(string host)
        {
            _host = host.TrimEnd(new[] { '/' });
        }

        public string GenerateDownloadUri(FileDownloadStatus fileDownload)
        {
            return GenerateUri(fileDownload, _host);
        }

        public static string GenerateDownloadApiUri(FileDownloadStatus fileDownload)
        {
            var host = ConfigurationManager.AppSettings["ServerURL"];
            host = host.TrimEnd(new[] { '/' });
            return GenerateUri(fileDownload, host);
        }

        private static string GenerateUri(FileDownloadStatus fileDownloadStatus, string host)
        {
            var urlEncodedFilename = HttpUtility.UrlPathEncode(Uri.EscapeDataString(fileDownloadStatus.Filename));
            return $"{host}/api/download/{fileDownloadStatus.Guid.ToString()}/{urlEncodedFilename}";
        }
    }
}