﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Models.UserAccess;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services
{
    public class ApplicationUserService : IApplicationUserService
    {

        public ApplicationUserService()
        {

        }

        public bool AccessDetailsPage(ApplicationUser applicationUser, Municipality municipality)
        {
            if (IsAdmin())
            {
                return true;
            }
            else {
                foreach (var item in applicationUser.MunicipalitiesToAdministrate)
                {
                    if (item == municipality)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public string GetUserClaims(string type)
        {
            string result = null;
            foreach (var claim in System.Security.Claims.ClaimsPrincipal.Current.Claims)
            {
                if (claim.Type.Contains(type) && !string.IsNullOrWhiteSpace(claim.Value))
                {
                    result = claim.Value;
                    break;
                }
            }
            return result;
        }

        public bool IsAdmin()
        {
            string role = GetUserClaims("claims/role");
            return role == "administrator";
        }
    }
}