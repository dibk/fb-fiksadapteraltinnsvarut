﻿using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Models.ApiModels;
using System;
using System.Collections.Generic;
using System.Linq;


namespace KommIT.FIKS.AdapterAltinnSvarUt.Services
{
    public class FileDownloadStatusService : IFileDownloadStatusService
    {

        private ApplicationDbContext _dbContext;

        public FileDownloadStatusService(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void Add(FileDownloadStatus fileDownloadStatus)
        {
            if (!RecordExists(fileDownloadStatus.ArchiveReference, fileDownloadStatus.Guid, fileDownloadStatus.Filename))
            {
                _dbContext.FileDownloadStatus.Add(fileDownloadStatus);
                _dbContext.SaveChanges();
            }
        }

        public bool Update(FileDownloadStatus fileDownloadStatus)
        {
            var entity = _dbContext.FileDownloadStatus.Find(fileDownloadStatus.Id);
            if (entity == null)
                return false;

            if (fileDownloadStatus.TimeReceived == default)
                fileDownloadStatus.TimeReceived = entity.TimeReceived;

            _dbContext.Entry(entity).CurrentValues.SetValues(fileDownloadStatus);
            _dbContext.SaveChanges();
            return true;
        }

        public List<FileDownloadStatus> GetFileDownloadStatusBy(string guid, string filename)
        {
            Guid decodeGuid;

            if (!Guid.TryParse(guid, out decodeGuid))
            {
                throw new ArgumentException("Kunne ikke dekode GUID");
            }

            var queryResult = from s in _dbContext.FileDownloadStatus
                              where s.Guid == decodeGuid && s.Filename == filename
                              select s;
            return queryResult.ToList();
        }

        public List<FileDownloadStatus> GetFileDownloadStatusesForArchiveReference(string archiveReference)
        {            
            var queryResult = from s in _dbContext.FileDownloadStatus
                              where s.ArchiveReference == archiveReference
                              select s;
            return queryResult.ToList();
        }

        public List<FileDownloadStatus> GetFileDownloadStatusesForArchiveReferences(List<string> archiveReferences)
        {
            var queryResult = from s in _dbContext.FileDownloadStatus
                              where archiveReferences.Contains(s.ArchiveReference)
                              select s;
            return queryResult.ToList();
        }

        public FileDownloadStatus GetFileDownloadStatusForArchiveReferencesAndFileType(string archiveReference, FilTyperForNedlasting fileType)
        {
            var queryResult = _dbContext.FileDownloadStatus.Where(s => s.ArchiveReference == archiveReference && s.FileType == fileType);
            return queryResult.FirstOrDefault();
        }

        private bool RecordExists(string archiveReference, Guid guid, Models.ApiModels.FilTyperForNedlasting fileType)
        {
            return _dbContext.FileDownloadStatus.Any(f => f.ArchiveReference == archiveReference && f.Guid == guid && f.FileType == fileType);
        }
        private bool RecordExists(string archiveReference, Guid guid, string fileName)
        {
            return _dbContext.FileDownloadStatus.Any(f => f.ArchiveReference == archiveReference && f.Guid == guid && f.Filename == fileName);
        }
    }
}