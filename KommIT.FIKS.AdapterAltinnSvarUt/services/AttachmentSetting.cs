using Dibk.Ftpb.Common.Constants;
using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services
{
    public class AttachmentSetting
    {
        public static string GetAttachmentTypeFriendlyName(string attachmentType)
        {
            string friendlyName = attachmentType;
            try
            {
                var displayName = Dibk.Ftpb.Common.DataTypeMapper.Get(attachmentType)?.DisplayName;
                if (!string.IsNullOrEmpty(displayName))
                {
                    friendlyName = displayName;
                }
            }
            catch (Exception)
            {
                return friendlyName;
            }

            return friendlyName;
        }

        public static readonly Dictionary<string, string> vedleggBlankettgruppe = new Dictionary<string, string>()
        {
            {"Dispensasjonssoeknad", "B"},
            {"RedegjoerelseUnntakTEK", "B"},
            {"Nabovarsel", "C"},
            {"KvitteringNabovarsel", "C"},
            {"Nabomerknader", "C"},
            {"KommentarNabomerknader", "C"},
            {"Situasjonsplan", "D"},
            {"Avkjoerselsplan", "D"},
            {"UnderlagUtnytting", "D"},
            {"TegningEksisterendeFasade", "E"},
            {"TegningEksisterendePlan", "E"},
            {"TegningEksisterendeSnitt", "E"},
            {"TegningNyFasade", "E"},
            {"TegningNyPlan", "E"},
            {"TegningNyttSnitt", "E"},
            {"ByggesaksBIM", "E"},
            {"Folgebrev", "F"},
            {"Situasjonskart", "F"},
            {"Kart", "F"},
            {"RedegjoerelseSkredOgFlom", "F"},
            {"RedegjoerelseAndreNaturMiljoeforhold", "F"},
            {"RedegjoerelseGrunnforhold", "F"},
            {"RedegjoerelseEstetikk", "F"},
            {"RedegjoerelseForurensetGrunn", "F"},
            {"ErklaeringAnsvarsrett", "G"},
            {"Gjennomfoeringsplan", "G"},
            {"BoligspesifikasjonMatrikkel", "H"},
            {"UttalelseVedtakAnnenOffentligMyndighet", "I"},
            {"UttalelseKulturminnemyndighet", "I"},
            {"SamtykkeArbeidstilsynet", "I"},
            {"AvkjoeringstillatelseVegmyndighet", "I"},
            {"RekvisisjonAvOppmaalingsforretning", "J"},
            {"AvklaringVA", "Q"},
            {"AvklaringHoyspent", "Q"},
            {"Annet", "Q"},
            {"SluttrapportForBygningavfall", "Q"},
            {"Foto", "Q"},
            {"KvitteringForLevertAvfall", "Q"},
            {"Organisasjonsplan", "Q"},
            {"Revisjonserklaering", "Q"},
            {"Miljoesaneringsbeskrivelse", "K"},
            {"RapportFraMiljoekartlegging","Q" },
            {"SoknadUnntasOffentlighet","Q" },
        };

        public static string GetSubformDataType(string formName)
        {
            var configs = Dibk.Ftpb.Common.DataTypeMapper.GetByDisplayName(formName, Dibk.Ftpb.Common.Models.ConfigType.SubForm);
            return configs?.FirstOrDefault()?.DataType;
        }

        public static string GetMainformDataType(string attachmentType)
        {
            var mainformMap = new Dictionary<string, string>
            {
                { "SØK-ES", DataTypes.SøknadOmEndringAvTillatelse },
                { "SØK-FA", DataTypes.SøknadOmFerdigAttest },
                { "SØK-IG", DataTypes.SøknadOmIgangsettingstillatelse },
                { "SØK-MB", DataTypes.SøknadOmMidlertidigBrukstillatelse },
                { "SØK-RS", DataTypes.SøknadOmRammetillatelse },
                { "SØK-ET", DataTypes.SøknadOmTillatelseIEttrinn },
                { "SØK-TA", DataTypes.SøknadOmTillatelseTilTiltakUtenAnsvarsrett },
                { "Gjennomføringsplan", DataTypes.GjennomføringsplanHovedskjema },

                { "Nabovarsel", DataTypes.Nabovarsel },
                { "Supplering av søknad", DataTypes.SuppleringAvSøknad },

                {"ErklaeringAnsvarsrett", DataTypes.ErklaeringAnsvarsrett },
                {"Samsvarserklæring", DataTypes.Samsvarserklæring },
                {"Kontrollerklæring med sluttrapport", DataTypes.Kontrollerklæring },

                {"Varselbrev", DataTypes.Varselbrev },
                {"SvarNabovarselPlan",DataTypes.SvarNabovarselPlan },
                {"Tiltakshavers signatur" , DataTypes.TiltakshaverSignatur },

                { "ArbeidstilsynetSamtykkeSoknad", DataTypes.ArbeidstilsynetSamtykkeSoknad },
            };

            return mainformMap[attachmentType];
        }

        public static string GetAttachmentType(Attachment attachment)
        {
            if (attachment.FileName.Equals("valideringsrapportFraFellestjenesterBygg.xml", StringComparison.OrdinalIgnoreCase))
            {
                return "Valideringsrapport";
            }
            else return attachment.AttachmentTypeName;
        }

        private static readonly Dictionary<string, AttachmentNameInfo> eByggesakServicecodeDoctypes
            = new Dictionary<string, AttachmentNameInfo>()
        {
            {"4528", new AttachmentNameInfo(){ AttachmentTypeName = "SØK-ET", FileName = GetDefaultMainFormPDFFileName() }},
            {"4397", new AttachmentNameInfo(){ AttachmentTypeName = "SØK-RS", FileName = GetDefaultMainFormPDFFileName() }},
            {"4373", new AttachmentNameInfo(){ AttachmentTypeName = "SØK-TA", FileName = GetDefaultMainFormPDFFileName() }},
            {"4401", new AttachmentNameInfo(){ AttachmentTypeName = "SØK-IG", FileName = GetDefaultMainFormPDFFileName() }},
            {"4399", new AttachmentNameInfo(){ AttachmentTypeName = "SØK-MB", FileName = GetDefaultMainFormPDFFileName() }},
            {"4400", new AttachmentNameInfo(){ AttachmentTypeName = "SØK-FA", FileName = GetDefaultMainFormPDFFileName() }},
            {"4402", new AttachmentNameInfo(){ AttachmentTypeName = "SØK-ES", FileName = GetDefaultMainFormPDFFileName() }},
            {"4398", new AttachmentNameInfo(){ AttachmentTypeName = "Gjennomføringsplan", FileName = GetDefaultMainFormPDFFileName() }},

            {"4655", new AttachmentNameInfo(){ AttachmentTypeName = "Nabovarsel", FileName = GetDefaultMainFormPDFFileName() }},
            {"4699", new AttachmentNameInfo(){ AttachmentTypeName = "Nabovarsel", FileName = GetDefaultMainFormPDFFileName() }},

            {"4762", new AttachmentNameInfo(){ AttachmentTypeName = "ErklaeringAnsvarsrett", FileName = GetDefaultMainFormPDFFileName() }},
            {"4419", new AttachmentNameInfo(){ AttachmentTypeName = "ErklaeringAnsvarsrett", FileName = GetDefaultMainFormPDFFileName() }},
            {"4965", new AttachmentNameInfo(){ AttachmentTypeName = "ErklaeringAnsvarsrett", FileName = GetDefaultMainFormPDFFileName() }},

            {"5207", new AttachmentNameInfo(){ AttachmentTypeName = "Samsvarserklæring", FileName = GetDefaultMainFormPDFFileName() }},
            {"5315", new AttachmentNameInfo(){ AttachmentTypeName = "Samsvarserklæring", FileName = GetDefaultMainFormPDFFileName() }},
            {"5444", new AttachmentNameInfo(){ AttachmentTypeName = "Kontrollerklæring med sluttrapport", FileName = GetDefaultMainFormPDFFileName() }},
            {"5445", new AttachmentNameInfo(){ AttachmentTypeName = "Kontrollerklæring med sluttrapport", FileName = GetDefaultMainFormPDFFileName() }},

            {"5418", new AttachmentNameInfo(){ AttachmentTypeName = "Varselbrev", FileName = "Varselbrev.pdf" }},
            {"5419", new AttachmentNameInfo(){ AttachmentTypeName = "SvarNabovarselPlan", FileName = GetDefaultMainFormPDFFileName() }},
            {"5303", new AttachmentNameInfo(){ AttachmentTypeName = "Tiltakshavers signatur", FileName = GetDefaultMainFormPDFFileName() }},
            {"5721", new AttachmentNameInfo(){ AttachmentTypeName = "Supplering av søknad", FileName = GetDefaultMainFormPDFFileName() }},
                        
            {"5689", new AttachmentNameInfo(){ AttachmentTypeName = "ArbeidstilsynetSamtykkeSoknad", FileName = GetDefaultMainFormPDFFileName() }},

            // Aldri tatt i bruk
            {"5697", new AttachmentNameInfo(){ AttachmentTypeName = "ArbeidstilsynetSamtykkeSignatur", FileName = "ArbeidstilsynetSamtykkeSignatur.pdf" }},
            {"5851", new AttachmentNameInfo(){ AttachmentTypeName = "Supplering av søknad til Arbeidstilsynets samtykke", FileName = "Supplering av søknad til Arbeidstilsynets samtykke.pdf" }},
        };

        public static string GetAttachmentTypeName(string serviceCode)
        {
            var attachmentTypeName = eByggesakServicecodeDoctypes.First(f => f.Key == serviceCode).Value.AttachmentTypeName;
            if (attachmentTypeName == null || attachmentTypeName.Trim().Length == 0)
            {
                attachmentTypeName = "";
            }

            return attachmentTypeName;
        }

        public static string GetMainFormPDFAttachmentFileName(string serviceCode)
        {
            var fileName = eByggesakServicecodeDoctypes.First(f => f.Key == serviceCode).Value.FileName;
            if (fileName == null || fileName.Trim().Length == 0)
            {
                fileName = GetDefaultMainFormPDFFileName();
            }

            return fileName;
        }

        private static string GetDefaultMainFormPDFFileName()
        {
            return Attachment.FilenameMainFormPdf;
        }
    }

    internal class AttachmentNameInfo
    {
        public string AttachmentTypeName { get; set; }
        public string FileName { get; set; }
    }
}