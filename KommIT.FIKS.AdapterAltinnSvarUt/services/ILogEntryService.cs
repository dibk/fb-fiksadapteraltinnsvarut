﻿using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Models.UserAccess;
using System.Collections.Generic;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services
{
    public interface ILogEntryService
    {
        void Save(LogEntry logEntry);
        void SaveBulk(IEnumerable<LogEntry> logEntries);
        List<LogEntry> GetAllLogEntries();
        List<LogEntry> GetLogEntriesByUser(ApplicationUser user);
        List<LogEntry> GetLogEntriesByArchiveReference(string archivereference);

        List<LogEntry> GetTopLogEntriesByArchiveReference(string archivereference, int topLogEntries);
    }
}
