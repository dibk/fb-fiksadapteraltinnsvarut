﻿using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Serilog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Metadata
{
    public class AltinnMetadataServices
    {
        private readonly ICacheMetadata _cacheService;
        

        public AltinnMetadataServices()
        {
            _cacheService = new CacheMetadata();
            
        }

        public AltinnMetadataServices(ICacheMetadata cacheService)
        {
            _cacheService = cacheService;
            
        }

        public AltinnMetadata GetAltinMetadata(string dataFormatId, string dataFormatVersion)
        {
            // try to get Metadata from cache
            var cachedValue = _cacheService.Get($"{dataFormatId}/{dataFormatVersion}");

            // if not null return value
            if (cachedValue != null)
                return cachedValue;
            // GET all DibkServices 
            var allDibkServicesMetadata = GetAllDibkServicesMetadata();
            // create Dictionary have of dibk altin services - key string format "dataFormatId/dataFormatVersion"

            Dictionary<string, AltinnMetadata> altinFormServices = SerializeDibkServicesMetadata(allDibkServicesMetadata);
            // check if services altin were get
            if (altinFormServices.Any())
            {
                foreach (var altinFormService in altinFormServices)
                {
                    _cacheService.Add(altinFormService.Key, altinFormService.Value, DateTime.Now.AddMinutes(60));
                }
                cachedValue = _cacheService.Get($"{dataFormatId}/{dataFormatVersion}");
            }


            return cachedValue;
        }

        public Dictionary<string, AltinnMetadata> SerializeDibkServicesMetadata(Object allDibkServicesMetadata)
        {
            Dictionary<string, AltinnMetadata> altinMetadataDictionary = new Dictionary<string, AltinnMetadata>();

            if (allDibkServicesMetadata.GetType() == typeof(JArray))
            {
                var jsonArray = (JArray)allDibkServicesMetadata;
                // "i" and "j" are used for internal control for duplicate keys and services not found
                var i = 0;
                var j = 0;
                // loop throw all services
                foreach (var jsonObject in jsonArray)
                {

                    // get Services info from Json object
                    string serviceCode = (string)jsonObject["ServiceCode"];
                    string serviceCodeEdition = (string)jsonObject["ServiceEditionCode"];

                    // control if service exist
                    if (!string.IsNullOrEmpty(serviceCode) && !string.IsNullOrEmpty(serviceCodeEdition))
                    {
                        // add parameters to altinmetadata model
                        var altinMetadata = new AltinnMetadata();
                        altinMetadata.ServiceName = (string)jsonObject["ServiceName"];
                        altinMetadata.ServiceCode = serviceCode;
                        altinMetadata.ServiceEditionCode = (string)jsonObject["ServiceEditionCode"];
                        altinMetadata.ServiceType = (string)jsonObject["ServiceType"];
                        if (altinMetadata.ServiceType.ToLower() == "formtask")
                        {
                            var json = GetMatadataForAltinnServices(serviceCode, serviceCodeEdition);

                            // this is to avoid the problem if the service is not found
                            if (!string.IsNullOrEmpty(json.ToString()))
                            {
                                var metadataForm = (JObject)json;
                                var attachmentRules = (JArray)metadataForm["AttachmentRules"];

                                var mainFormsMetaData = metadataForm["FormsMetaData"]
                                    .FirstOrDefault(f => f["FormType"].Value<string>() == "MainForm");
                                var subFormsMetaData = metadataForm["FormsMetaData"]
                                    .Where(f => f["FormType"].Value<string>() != "MainForm");
                                altinMetadata.SubFormsMetaData = SerializeSubFormsMetadata(subFormsMetaData);
                                altinMetadata.AttachmentRules = SerializeAttachmentRules(attachmentRules);

                                string dataFormatId = (string)mainFormsMetaData["DataFormatID"];
                                string dataFormatVersion = (string)mainFormsMetaData["DataFormatVersion"];

                                if (altinMetadataDictionary.ContainsKey($"{dataFormatId}/{dataFormatVersion}"))
                                {
                                    altinMetadataDictionary.Add($"{dataFormatId}/{dataFormatVersion}-{i}", altinMetadata);
                                }
                                else
                                {
                                    altinMetadataDictionary.Add($"{dataFormatId}/{dataFormatVersion}", altinMetadata);
                                }
                            }
                            else
                            {
                                // for services not found
                                j++;
                                altinMetadataDictionary.Add($"{serviceCode}/{serviceCodeEdition} -{i}/{j}", altinMetadata);
                            }
                        }
                        // for duplicate keys
                        i++;
                    }
                }
            }
            return altinMetadataDictionary;
        }

        private List<AttachmentRule> SerializeAttachmentRules(JArray attachmentRules)
        {
            var subFormsList = new List<AttachmentRule>();
            if (attachmentRules == null || !attachmentRules.Any()) return subFormsList;
            foreach (var attachRule in attachmentRules)
            {
                var attachmentRule = new AttachmentRule();
                attachmentRule.AttachmentTypeName = (string)attachRule["AttachmentTypeName"];
                attachmentRule.AllowedFileTypes = (string)attachRule["AllowedFileTypes"];
                attachmentRule.MaxAttachmentCount = (int)attachRule["MaxAttachmentCount"];
                attachmentRule.MaxFileSize = (int)attachRule["MaxFileSize"];
                attachmentRule.MinAttachmentCount = (int)attachRule["MinAttachmentCount"];
                subFormsList.Add(attachmentRule);
            }
            return subFormsList;
        }

        private List<FormMetaData> SerializeSubFormsMetadata(IEnumerable<JToken> subFormsMetaData)
        {
            var subFormsList = new List<FormMetaData>();
            foreach (var subFormMetadata in subFormsMetaData)
            {
                var formMetaData = new FormMetaData();
                formMetaData.FormName = (string)subFormMetadata["FormName"];
                formMetaData.DataFormatID = (string)subFormMetadata["DataFormatID"];
                formMetaData.DataFormatVersion = (string)subFormMetadata["DataFormatVersion"];
                subFormsList.Add(formMetaData);
            }
            return subFormsList;
        }


        public Object GetMatadataForAltinnServices(string servicecode, string servicecodeEdition)
        {
            var altinMetadata = String.Empty;
            try
            {
                string urlAltinMetadata = $"{ConfigurationManager.AppSettings["AltinnServer"]}/api/metadata/formtask/{servicecode}/{servicecodeEdition}";
                altinMetadata = DownloadJson(urlAltinMetadata);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Feil ved tilgangen til Altinn metadata for skjema");
            }

            return ConvertToJson(altinMetadata);
        }

        private static Object ConvertToJson(string objectString)
        {
            return string.IsNullOrEmpty(objectString) ? string.Empty : JsonConvert.DeserializeObject(objectString);
        }

        public Object GetAllDibkServicesMetadata()
        {
            var altinMetadata = String.Empty;
            try
            {
                //ToDo: =============== ATIL SOS v2: How do we deal with ATIL DLQ???

                // Url to get all Dibk services "https://altinn.no/api/metadata?$filter=ServiceOwnerCode eq 'DIBK'"
                string urlAltinMetadata =
                    $"{ConfigurationManager.AppSettings["AltinnServer"]}/api/metadata?$filter=ServiceOwnerCode%20eq%20%27DIBK%27";
                // Download Json from URL
                altinMetadata = DownloadJson(urlAltinMetadata);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Feil ved tilgangen til Altinn metadata for skjema");
            }
            return ConvertToJson(altinMetadata);
        }


        private string DownloadJson(string url)
        {
            var result = string.Empty;
            try
            {
                using (WebClient client = new WebClient())
                {
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                    client.Encoding = Encoding.UTF8;
                    client.Headers.Add(HttpRequestHeader.Accept, "application/json");
                    result = client.DownloadString(url);
                }
            }
            catch (Exception ex)
            {

                Log.Error(ex, "Unable to retrieve altinn metadata from URL: {url}", url);
                throw;
            }

            return result;
        }
    }
}