﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Web;
using KommIT.FIKS.AdapterAltinnSvarUt.Models;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Metadata
{
    public class CacheMetadata : ICacheMetadata
    {
        public void Add(string key, AltinnMetadata metadata, DateTimeOffset expirationDateTime)
        {
            MemoryCache.Default.Add(key, metadata, expirationDateTime);
        }

        public AltinnMetadata Get(string key)
        {
            return (AltinnMetadata)MemoryCache.Default.Get(key);
        }
    }
}