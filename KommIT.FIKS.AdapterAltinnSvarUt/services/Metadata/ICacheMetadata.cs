﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KommIT.FIKS.AdapterAltinnSvarUt.Models;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Metadata
{
    public interface ICacheMetadata
    {
        /// <summary>
        /// Key er "DataFormatID,DataFormatVersion"
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        AltinnMetadata Get(string key);

        /// <summary>
        /// Add item to cache.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="metadata"></param>
        /// <param name="expirationDateTime"></param>
        void Add(string key, AltinnMetadata metadata, DateTimeOffset expirationDateTime);
    }
}