﻿using KommIT.FIKS.AdapterAltinnSvarUt.Services.Svarut.REST;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Svarut.StatusService
{
    public class SvarUtStatusService : ISvarUtStatusService
    {
        private readonly ILogger<SvarUtStatusService> _log;
        private readonly SvarUtClientFactory _svarUtClientFactory;

        public SvarUtStatusService(ILogger<SvarUtStatusService> logger, SvarUtClientFactory svarUtClientFactory)
        {
            _log = logger;
            _svarUtClientFactory = svarUtClientFactory;
        }

        public string GetShipmentStatus(string forseldelsesId)
        {
            try
            {
                var result = AsyncHelper.RunSync(async () => await _svarUtClientFactory.CreateSvarUtClient().GetForsendelseStatusAsync(forseldelsesId));

                return result.Status.HasValue ? result.Status.Value.ToString() : string.Empty;
            }
            catch (Exception e)
            {
                _log.LogError(e, "Error ({exceptionType}) occurred when communicating with KS SvarUt - {externalResource} - ExceptionMessage: {exceptionMessage}", e.GetType().ToString(), "ISvarUtClient.RetrieveForsendelseStatus", e.Message);
                // todo log exception to system log
                return "Feil ved kommunikasjon med SvarUt.";
            }
        }

        public List<Tuple<string, string>> GetShipmentHistoricalLog(string forsendelsesId)
        {
            var historicalLog = new List<Tuple<string, string>>();

            var result = AsyncHelper.RunSync(async () => await _svarUtClientFactory.CreateSvarUtClient().GetForsendelseHistorikkAsync(forsendelsesId));

            foreach (var item in result.HendelsesLogger)
            {
                var valueSet = Tuple.Create(item.Hendelse, item.Tidspunkt);
                historicalLog.Add(valueSet);
            }

            return historicalLog;
        }

        public List<ForsendelseStatus> GetShipmentStatuses(string[] forsendelsesider)
        {
            var statuses = new List<ForsendelseStatus>();

            var results = AsyncHelper.RunSync(async () => await _svarUtClientFactory.CreateSvarUtClient().GetForsendelseStatuserAsync(forsendelsesider.ToList()));

            return results.Statuser.Select(item => new ForsendelseStatus() { Id = item.ForsendelsesId.Id, Status = item.Status.ToString(), StatusEndret = item.SisteStatusEndring }).ToList();
        }
    }
}