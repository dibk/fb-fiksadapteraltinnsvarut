﻿using System;
using System.Collections.Generic;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Svarut.StatusService
{
    public interface ISvarUtStatusService
    {
        string GetShipmentStatus(string forseldelsesId);
        List<ForsendelseStatus> GetShipmentStatuses(string[] forsendelsesider);
        List<Tuple<string, string>> GetShipmentHistoricalLog(string forsendelsesId);
    }
}