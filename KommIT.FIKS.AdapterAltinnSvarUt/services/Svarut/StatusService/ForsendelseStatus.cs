﻿using System;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Svarut.StatusService
{
    public class ForsendelseStatus
    {
        public string Id { get; set; }
        public string Status { get; set; }
        public DateTime? StatusEndret { get; set; }
    }
}