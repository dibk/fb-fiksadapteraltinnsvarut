﻿namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Svarut
{
    public interface ISvarUtPolicyConfigurationProvider : IPolicyConfigurationProvider { }
    public class SvarUtPolicyConfigurationProvider : PolicyConfigurationProvider, ISvarUtPolicyConfigurationProvider
    {
        public override string RetryStructureSettingKey => "Polly:SvarUtClient:RetryStructure";
    }
}