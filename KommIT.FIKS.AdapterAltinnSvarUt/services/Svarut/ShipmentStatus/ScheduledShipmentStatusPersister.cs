﻿using Hangfire;
using KommIT.FIKS.AdapterAltinnSvarUt.HangfireConfiguration;
using KommIT.FIKS.AdapterAltinnSvarUt.Logger;
using Serilog;
using System.Configuration;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Svarut.ShipmentStatus
{
    public class ScheduledShipmentStatusPersister
    {
        private readonly ISvarUtShipmentStatusPersister _statusPersister;

        public ScheduledShipmentStatusPersister(ISvarUtShipmentStatusPersister statusPersister)
        {
            _statusPersister = statusPersister;
        }

        [HangfireBasicJobLogger]
        public void UpdateShipmentStatutses()
        {
            _statusPersister.RetrieveAndUpdateStatus();
        }

        public void ScheduleUpdateShipmentStatutses()
        {
            var scheduleHoursConfiguration = ConfigurationManager.AppSettings["SvarUtShipmentStatusUpdate:ScheduleHours"];
            int defaultScheduleHours = 12;
            int scheduleHours = 0;

            if (!int.TryParse(scheduleHoursConfiguration, out scheduleHours))
            {
                Log.Logger.Error($"Unable to find or parse SvarUtShipmentStatusUpdate:ScheduleHours to integer value, uses default instead {defaultScheduleHours}");
                scheduleHours = defaultScheduleHours;
            }

            var scheduleJobString = ConfigurationManager.AppSettings["SvarUtShipmentStatusUpdate:ScheduleRecurringJob"];
            bool scheduleJob;
            if (!bool.TryParse(scheduleJobString, out scheduleJob))
                Log.Logger.Error("Unable to find or parse SvarUtShipmentStatusUpdate:ScheduleRecurringJob to boolean value, will not schedule job");

            //Scheduling update of shipment statuses
            if (scheduleJob)
                RecurringJob.AddOrUpdate("ScheduleUpdateShipmentStatutses", () => UpdateShipmentStatutses(), CronExpressions.HourInterval(scheduleHours));
            else
                RecurringJob.RemoveIfExists("ScheduleUpdateShipmentStatutses");
        }
    }
}