﻿namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Svarut.ShipmentStatus
{
    public interface ISvarUtShipmentStatusPersister
    {
        void RetrieveAndUpdateStatus();
    }
}