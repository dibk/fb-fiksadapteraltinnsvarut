﻿using System.Collections.Generic;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Svarut.ShipmentStatus
{
    public interface IFinalStatusFilter
    {
        IEnumerable<string> GetStatuses();
    }

    public class FinalStatusFilter : IFinalStatusFilter
    {
        private readonly List<string> _filteredStatuses;

        public FinalStatusFilter()
        {
            _filteredStatuses = new List<string>()
            {
                "LEST",
                "SENDT_PRINT",
                "SENDT_SDP",
                "LEVERT_SDP",
                "PRINTET",
                "AVVIST",
                "MANUELT_HANDTERT"
            };
        }

        public IEnumerable<string> GetStatuses()
        {
            return _filteredStatuses;
        }
    }
}