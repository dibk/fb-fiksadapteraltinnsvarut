﻿using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Repositories;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Svarut.StatusService;
using Serilog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Svarut.ShipmentStatus
{
    public class SvarUtShipmentStatusPersister : ISvarUtShipmentStatusPersister
    {
        private readonly IFormMetadataRepository _formMetadataRepository;
        private readonly ISvarUtStatusService _svarUtStatusService;
        private readonly IFinalStatusFilter _statusFilter;
        private readonly ILogger _logger = Log.ForContext<SvarUtShipmentStatusPersister>();

        public SvarUtShipmentStatusPersister(IFormMetadataRepository formMetadataRepository, ISvarUtStatusService svarUtStatusService, IFinalStatusFilter statusFilter)
        {
            _formMetadataRepository = formMetadataRepository;
            _svarUtStatusService = svarUtStatusService;
            _statusFilter = statusFilter;
        }
        public void RetrieveAndUpdateStatus()
        {
            int numberOfDaysPast = GetNumberOfDaysPastConfiguration();

            //Get candidates from FormMetadata
            var oldestDateForCandidates = DateTime.Now.AddDays(numberOfDaysPast * -1);
            var filterStatuses = _statusFilter.GetStatuses();
            //Filter candidates on status
            var candidates = _formMetadataRepository.GetFormMetadataBy(oldestDateForCandidates, filterStatuses);

            var candidateIds = candidates.Select(c => c.SvarUtForsendelsesId).ToList();
            var statuses = GetStatusesFromSvarUt(candidateIds);

            if (statuses.Count > 0)
                foreach (var item in candidates)
                {
                    var status = statuses.Where(s => s.Id == item.SvarUtForsendelsesId).FirstOrDefault();
                    if (status != null)
                        if (item.SvarUtForsendelsesStatus == null)
                            item.SvarUtForsendelsesStatus = new SvarUtForsendelsesStatus() { SvarUtForsendelsesId = item.SvarUtForsendelsesId, ForsendelseStatus = status.Status.ToUpper(), EndretStatusDato = status.StatusEndret, ArchiveReference = item.ArchiveReference, MunicipalityName = item.GetMunicipalityName() };
                        else
                        {
                            if (!AreEqual(item.SvarUtForsendelsesStatus, status))
                            {
                                item.SvarUtForsendelsesStatus.ForsendelseStatus = status.Status;
                                item.SvarUtForsendelsesStatus.EndretStatusDato = status.StatusEndret;
                                item.SvarUtForsendelsesStatus.ArchiveReference = item.ArchiveReference;
                                item.SvarUtForsendelsesStatus.MunicipalityName = item.GetMunicipalityName();
                            }
                        }
                }

            _formMetadataRepository.Complete();
        }

        private List<ForsendelseStatus> GetStatusesFromSvarUt(List<string> candidateIds)
        {
            var statuses = new List<ForsendelseStatus>();
            try
            {
                if (candidateIds.Count() > 0)
                {
                    //Split list of candidates into bulks
                    var bulkSize = 300;
                    decimal iterationsForCandidates = (decimal)candidateIds.Count() / bulkSize;

                    if (iterationsForCandidates <= 1)
                        statuses = _svarUtStatusService.GetShipmentStatuses(candidateIds.ToArray());
                    else
                    {
                        var iterations = (int)Math.Floor(iterationsForCandidates);
                        var startIndex = 0;
                        for (int i = 0; i < iterations; i++)
                        {
                            startIndex = i == 0 ? i : (i * bulkSize) - 1;
                            var candidateBulk = candidateIds.GetRange(startIndex, bulkSize);
                            statuses.AddRange(_svarUtStatusService.GetShipmentStatuses(candidateBulk.ToArray()));
                        }

                        var candidateRemainingBulk = candidateIds.GetRange((iterations * bulkSize) - 1, candidateIds.Count - (iterations * bulkSize));
                        statuses.AddRange(_svarUtStatusService.GetShipmentStatuses(candidateRemainingBulk.ToArray()));
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "An error occurred while retrieving status updates for {candidateStatusCount}", candidateIds.Count());
            }

            return statuses;
        }

        private bool AreEqual(SvarUtForsendelsesStatus svarUtForsendelsesStatus, ForsendelseStatus forsendelseStatus)
        {
            if (!svarUtForsendelsesStatus.EndretStatusDato.ToString().Equals(forsendelseStatus.StatusEndret.ToString()) ||
                !svarUtForsendelsesStatus.ForsendelseStatus.Equals(forsendelseStatus.Status) ||
                string.IsNullOrEmpty(svarUtForsendelsesStatus.MunicipalityName))
                return false;

            return true;
        }

        private int GetNumberOfDaysPastConfiguration()
        {
            var numberOfDaysPastConfig = ConfigurationManager.AppSettings["SvarUtShipmentStatusUpdate:NumberOfDaysPast"];
            int numberOfDaysPastDefault = 60;
            int numberOfDaysPast;

            if (!int.TryParse(numberOfDaysPastConfig, out numberOfDaysPast))
            {
                _logger.Error($"Unable to parse configured value for SvarUtShipmentStatusUpdate:NumberOfDaysPast, defaults to {numberOfDaysPastDefault}");
                numberOfDaysPast = numberOfDaysPastDefault;
            }

            return numberOfDaysPast;
        }
    }
}