﻿using Dibk.Ftpb.Common;
using Dibk.Ftpb.Common.Constants;
using Dibk.Ftpb.Common.Models;
using Dibk.Ftpb.Integration.SvarUt.Builders.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using Microsoft.Extensions.Logging;
using System;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Svarut.REST
{
    public class SvarUtRestBuilderBase
    {
        protected readonly ILogger Logger;

        public SvarUtRestBuilderBase(ILogger logger)
        {
            Logger = logger;
        }

        public (string, string, string, string, string) GetDataTypeConfig(string dataType)
        {
            DataTypeConfig retVal;
            try
            {
                retVal = DataTypeMapper.Get(dataType);
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, "Unable to find dataTypeConfig for {DataType}", dataType);
                retVal = DataTypeMapper.Get(DataTypes.Annet);
            }

            return (retVal.DataType, retVal.DisplayName, retVal.Arkivlett1DokumentType, retVal.Arkivlett2DokumentType, retVal.Category);
        }

        protected static ForsendelseDataBygg GetFormData(FormData formData)
        {
            return new ForsendelseDataBygg(formData.ArchiveReference)
            {
                Adresselinje1 = formData.Mainform.GetPropertyIdentifiers().Adresselinje1,
                Adresselinje2 = formData.Mainform.GetPropertyIdentifiers().Adresselinje2,
                Adresselinje3 = formData.Mainform.GetPropertyIdentifiers().Adresselinje3,
                Kommunenummer = formData.Mainform.GetPropertyIdentifiers().Kommunenummer,
                Gårdsnummer = formData.Mainform.GetPropertyIdentifiers().Gaardsnummer,
                Bruksnummer = formData.Mainform.GetPropertyIdentifiers().Bruksnummer,
                Seksjonsnummer = formData.Mainform.GetPropertyIdentifiers().Seksjonsnummer,
                Festenummer = formData.Mainform.GetPropertyIdentifiers().Festenummer,
                Postnr = formData.Mainform.GetPropertyIdentifiers().Postnr,
                Poststed = formData.Mainform.GetPropertyIdentifiers().Poststed,
                AvgivendeSystem = formData.Mainform.GetPropertyIdentifiers().FraSluttbrukersystem,
                Bolignummer = formData.Mainform.GetPropertyIdentifiers().Bolignummer,
                Bygningsnummer = formData.Mainform.GetPropertyIdentifiers().Bygningsnummer,
                KommunensSaksnummerSekvensnummer = formData.Mainform.GetPropertyIdentifiers().KommunensSaksnummerSekvensnummer,
                KommunensSaksnummerÅr = formData.Mainform.GetPropertyIdentifiers().KommunensSaksnummerAar,
                ForsendelseType = "Byggesøknad",
                SøknadSkjemaNavn = formData.Mainform.GetPropertyIdentifiers().SoeknadSkjemaNavn,
            };
        }
    }
}