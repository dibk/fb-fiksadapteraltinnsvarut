﻿using Dibk.Ftpb.Integration.SvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using Microsoft.Extensions.Logging;
using System;
using System.Diagnostics;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Svarut.REST
{
    public class SvarUtRestService
    {
        private readonly ILogger<SvarUtRestService> _logger;
        private readonly ILogEntryService _logEntryService;
        private readonly IFormMetadataService _formMetadataService;
        private readonly SvarUtClientFactory _svarUtClientFactory;

        public SvarUtRestService(ILogger<SvarUtRestService> logger,
                                 ILogEntryService logEntryService,
                                 IFormMetadataService formMetadataService,
                                 SvarUtClientFactory svarUtClientFactory)
        {
            _logger = logger;
            _logEntryService = logEntryService;
            _formMetadataService = formMetadataService;
            _svarUtClientFactory = svarUtClientFactory;
        }

        public string SendShipment(Forsendelse forsendelsePayload, bool storeForsendelseIdOnForm = true)
        {
            var stopWatch = new Stopwatch();
            try
            {
                stopWatch.Start();

                var forsendelseId = string.Empty;

                var responseForsendelseId = AsyncHelper.RunSync(async () => await _svarUtClientFactory.CreateSvarUtClient().SendForsendelseAsync(forsendelsePayload));
                forsendelseId = responseForsendelseId.Id;

                stopWatch.Stop();

                var logEntry = LogEntry.NewInfo(forsendelsePayload.EksternReferanse, "SvarUt har akseptert forsendelsen. ForsendelsesId: " + forsendelseId, ProcessLabel.SvarUtForsendelse, stopWatch);
                _logEntryService.Save(logEntry);

                if (storeForsendelseIdOnForm)
                    SaveSvarUtShippingInformation(forsendelsePayload.EksternReferanse, forsendelseId);

                _logger.LogInformation(logEntry.ToString());

                return forsendelseId;
            }
            catch (Exception e)
            {
                stopWatch.Stop();

                _logger.LogError(e, "Error ({exceptionType}) occurred when communicating with KS SvarUt - {externalResource} - ExceptionMessage: {exceptionMessage}", e.GetType().ToString(), "ISvarUtClient.SendForsendelse", e.Message);
                _logEntryService.Save(LogEntry.NewError(forsendelsePayload.EksternReferanse, "SvarUt har feilet. " + e, ProcessLabel.SvarUtForsendelse, stopWatch));
                _logger.LogError(e, "SvarUt code exception for shipment '{ForsendelseTittel}' archive ref {ArchiveReference}.", forsendelsePayload.Tittel, forsendelsePayload.EksternReferanse);

                throw;
            }
        }

        private void SaveSvarUtShippingInformation(string archiveReference, string svarUtForsendelseId)
        {
            _formMetadataService.SaveSvarUtShippingInformation(archiveReference, svarUtForsendelseId);
        }
    }
}