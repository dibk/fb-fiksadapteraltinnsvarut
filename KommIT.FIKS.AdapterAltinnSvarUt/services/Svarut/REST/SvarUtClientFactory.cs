﻿using Dibk.Ftpb.Integration.SvarUt;
using Microsoft.Extensions.Logging;
using System;
using System.Configuration;
using System.Net.Http;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Svarut.REST
{
    public class SvarUtClientFactory
    {
        private readonly ILogger<SvarUtClient> _svarUtClientLogger;
        private readonly IHttpClientFactory _httpClientFactory;

        public SvarUtClientFactory(ILogger<SvarUtClient> svarUtClientLogger, IHttpClientFactory httpClientFactory)
        {
            _svarUtClientLogger = svarUtClientLogger;
            _httpClientFactory = httpClientFactory;
        }

        public SvarUtClient CreateSvarUtClient()
        {
            var svarUtBaseUri = ConfigurationManager.AppSettings["SvarUt:BaseUri"];
            var svarUtBaseAddress = new Uri($"{svarUtBaseUri}");

            var svarUtUsername = ConfigurationManager.AppSettings["SvarUt:Username"];
            var svarUtPassword = ConfigurationManager.AppSettings["SvarUt:Password"];

            var httpClient = _httpClientFactory.CreateClient();
            httpClient.BaseAddress = svarUtBaseAddress;
            httpClient.Timeout =  TimeSpan.FromMinutes(16);
            var svarUtClient = new SvarUtClient(httpClient, _svarUtClientLogger);
            svarUtClient.SetAuthentication(svarUtUsername, svarUtPassword);

            return svarUtClient;
        }
    }
}