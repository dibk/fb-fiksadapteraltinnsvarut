﻿using Dibk.Ftpb.Integration.SvarUt;
using Microsoft.Extensions.Logging;
using Polly;
using System;
using System.Collections.Generic;
using System.Net.Http;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Svarut.REST
{
    public class SvarUtRestRetryPolicyProvider
    {
        private readonly ILogger<SvarUtRestRetryPolicyProvider> _log;
        private readonly ISvarUtPolicyConfigurationProvider _svarUtConfigurationProvider;

        public SvarUtRestRetryPolicyProvider(ILogger<SvarUtRestRetryPolicyProvider> logger, ISvarUtPolicyConfigurationProvider svarUtConfigurationProvider)
        {
            _log = logger;
            _svarUtConfigurationProvider = svarUtConfigurationProvider;
        }

        public Policy GetRetryPolicy(string api)
        {
            List<int> retryAttemptWaitStructure = _svarUtConfigurationProvider.RetryAttemptWaitSecondsStructure;
            var retryCount = 0;
            var policy = Policy.Handle<SvarUtRequestException>(r => r.HttpStatusCode >= System.Net.HttpStatusCode.InternalServerError || r.HttpStatusCode >= System.Net.HttpStatusCode.RequestTimeout)
                .Or<HttpRequestException>()
                .WaitAndRetry(
                retryCount: retryAttemptWaitStructure.Count,
                sleepDurationProvider: attempt =>
                {
                    var jitterer = new Random();
                    var timeSpan = TimeSpan.FromSeconds(retryAttemptWaitStructure[retryCount])
                           + TimeSpan.FromMilliseconds(jitterer.Next(0, 1000));
                    retryCount++;

                    return timeSpan;
                },
                onRetry: (exception, calculatedWaitDuration) =>
                {
                    _log.LogWarning(exception, "Failed to contact {RetryApi} {RetryCount} time(s). Waits for {CalculatedWaitDuration} and retries", api, retryCount, calculatedWaitDuration);
                });

            return policy;
        }
    }
}