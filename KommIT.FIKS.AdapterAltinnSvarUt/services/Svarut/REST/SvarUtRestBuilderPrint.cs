﻿using Dibk.Ftpb.Integration.SvarUt.Builders;
using Dibk.Ftpb.Integration.SvarUt.Builders.Models;
using Dibk.Ftpb.Integration.SvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Extensions;
using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Svarut.REST
{
    public class SvarUtRestBuilderPrint : SvarUtRestBuilderBase
    {
        public SvarUtRestBuilderPrint(ILogger<SvarUtRestBuilderPrint> logger) : base(logger)
        {
        }

        public Forsendelse BuildForsendelseForPrint(string forsendelseType, Address recipientAddress, Address sender, string title, List<Attachment> attachments, FormData formData)
        {
            ForsendelseDataBygg forsendelseData = GetFormData(formData);
            forsendelseData.AvgivendeSystem = !string.IsNullOrEmpty(forsendelseData.AvgivendeSystem) ? forsendelseData.AvgivendeSystem : "FellestjenesterBygg-Altinn";
            forsendelseData.ForsendelseType = forsendelseType;
            forsendelseData.ForsendelseTittel = title;

            var forsendelseBuilder = new ForsendelseBuilder(forsendelseData, GetDataTypeConfig);
            forsendelseBuilder.AddSender(PopulateSender(sender));
            forsendelseBuilder.AddReceiver(PopulateReceiver(recipientAddress));

            var forsendelsePayload = forsendelseBuilder.Build();

            forsendelsePayload.KunDigitalLevering = false;
            forsendelsePayload.Kryptert = false;
            forsendelsePayload.KrevNiva4Innlogging = false;
            forsendelsePayload.UtskriftsKonfigurasjon = new UtskriftsKonfigurasjon()
            {
                Tosidig = true,
                UtskriftMedFarger = true
            };

            forsendelsePayload.Dokumenter = ConvertAttachmentsToSvarUtFormat(attachments);

            return forsendelsePayload;
        }

        private List<IDokument> ConvertAttachmentsToSvarUtFormat(List<Attachment> attachments)
        {
            var dokuments = new List<IDokument>();
            foreach (var attachment in attachments)
            {
                var tempDok = new SvarUtDokument();
                tempDok.Filnavn = attachment.FileName;
                tempDok.DocumentContent = attachment.AttachmentData.ToStream();
                tempDok.DokumentType = attachment.AttachmentTypeName;
                tempDok.SkalSigneres = false;
                dokuments.Add(tempDok);
            }

            return dokuments;
        }

        private Adresse PopulateReceiver(Address recipientAddress)
        {
            return new Adresse()
            {
                DigitalAdresse = new Digitaladresse(),
                PostAdresse = new PostAdresse()
                {
                    Adresse1 = recipientAddress.AddressLine1,
                    Adresse2 = recipientAddress.AddressLine2,
                    Adresse3 = recipientAddress.AddressLine3,
                    Land = CountryCodeHandler.ReturnCountryName(recipientAddress.CountryCode),
                    Navn = recipientAddress.Name,
                    PostNummer = CountryCodeHandler.ReturnAdjustedPostalcode(recipientAddress.PostalCode, CountryCodeHandler.ReturnCountryIsoCode(recipientAddress.CountryCode)),
                    PostSted = recipientAddress.City
                }
            };
        }

        private Adresse PopulateSender(Address senderAddress)
        {
            var addr = new Adresse()
            {
                PostAdresse = new PostAdresse()
                {
                    Adresse1 = senderAddress.AddressLine1,
                    Adresse2 = senderAddress.AddressLine2,
                    Adresse3 = senderAddress.AddressLine3,
                    Land = senderAddress.CountryCode,
                    Navn = senderAddress.Name,
                    PostNummer = senderAddress.PostalCode,
                    PostSted = senderAddress.City
                }
            };

            if (!string.IsNullOrWhiteSpace(senderAddress.OrganizationNumber)
                || (!string.IsNullOrWhiteSpace(senderAddress.SosialServiceNumber)))
            {
                addr.DigitalAdresse = new Digitaladresse()
                {
                    OrganisasjonsNummer = senderAddress.OrganizationNumber
                ,
                    FodselsNummer = senderAddress.SosialServiceNumber
                };
            }

            return addr;
        }
    }
}