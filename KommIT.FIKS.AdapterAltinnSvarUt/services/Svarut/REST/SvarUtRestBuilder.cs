﻿using Dibk.Ftpb.Common.Registry;
using Dibk.Ftpb.Integration.SvarUt.Builders;
using Dibk.Ftpb.Integration.SvarUt.Builders.Models;
using Dibk.Ftpb.Integration.SvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Extensions;
using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Svarut.REST
{
    public class SvarUtRestBuilder : SvarUtRestBuilderBase
    {
        public SvarUtRestBuilder(ILogger<SvarUtRestBuilder> logger) : base(logger)
        { }

        public Forsendelse BuildForsendelse(FormData formData, string receiverOrgnr, string receiverName)
        {
            ForsendelseDataBygg forsendelseData = GetFormData(formData);

            var forsendelseBuilder = new ForsendelseBuilder(forsendelseData, GetDataTypeConfig);

            forsendelseBuilder.AddBasicClassificationValues();
            forsendelseBuilder.AddSender(CreateSvarSendesTil(formData.Mainform.GetPropertyIdentifiers()));
            forsendelseBuilder.AddDigitalReceiver(receiverName, receiverOrgnr);

            forsendelseBuilder.AddMainFormXml(AttachmentSetting.GetMainformDataType(formData.MainFormPdf.AttachmentTypeName), Attachment.FilenameMainFormXml, formData.FormDataAsXml.ToStream(), $"{formData.MainformDataFormatID}.{formData.MainformDataFormatVersionID}");
            forsendelseBuilder.AddMainFormPdf(AttachmentSetting.GetMainformDataType(formData.MainFormPdf.AttachmentTypeName), Attachment.FilenameMainFormPdf, formData.MainFormPdf.AttachmentData.ToStream());

            foreach (var attachment in formData.Attachments)
            {
                bool inneholderPersonsensitivinformasjon = DataTypeRegistry.PersonalSensitiveInformationDataTypes.Contains(attachment.AttachmentTypeName);

                var metadata = attachment.GetVedleggMetadata(formData.GetVedleggOpplysningerSkjema());
                List<KeyValuePair<string, string>> urlDecodedMetadata = null;
                if (metadata != null)
                    urlDecodedMetadata = metadata.ToMetadataItems().Select(s => new KeyValuePair<string, string>(s.Key, System.Web.HttpUtility.UrlDecode(s.Value))).ToList();

                forsendelseBuilder.AddAttachment(AttachmentSetting.GetAttachmentType(attachment), attachment.FileName, null, attachment.AttachmentData.ToStream(), urlDecodedMetadata, inneholderPersonsensitivinformasjon);
            }

            foreach (var subform in formData.Subforms)
            {
                forsendelseBuilder.AddSubformXml(AttachmentSetting.GetSubformDataType(subform.FormName), $"{subform.FormName}.xml", subform.FormDataXml.ToStream(), $"{subform.DataFormatId}.{subform.DataFormatVersion}");
                forsendelseBuilder.AddSubformPdf(AttachmentSetting.GetSubformDataType(subform.FormName), $"{subform.FormName}.pdf", subform.PdfFileBytes.ToStream());
            }

            var forsendelsePayload = forsendelseBuilder.Build();
            return forsendelsePayload;
        }

        private Adresse CreateSvarSendesTil(PropertyIdentifiers identifiers)
        {
            var svarSendesTil = new Adresse();
            if (!string.IsNullOrEmpty(identifiers.AnsvarligSokerFnr))
            {
                svarSendesTil.DigitalAdresse = new Digitaladresse()
                {
                    FodselsNummer = Helpers.GetDecryptedFnr(identifiers.AnsvarligSokerFnr)
                };
            }
            else
            {
                svarSendesTil.DigitalAdresse = new Digitaladresse()
                {
                    OrganisasjonsNummer = identifiers.AnsvarligSokerOrgnr
                };
            }

            svarSendesTil.PostAdresse = new PostAdresse()
            {
                Navn = identifiers.AnsvarligSokerNavn,
                Adresse1 = identifiers.AnsvarligSokerAdresselinje1,
                PostNummer = identifiers.AnsvarligSokerPostnr,
                PostSted = identifiers.AnsvarligSokerPoststed
            };

            if (string.IsNullOrEmpty(svarSendesTil.PostAdresse.PostNummer)) svarSendesTil.PostAdresse.PostNummer = "9999";
            if (string.IsNullOrEmpty(svarSendesTil.PostAdresse.PostSted)) svarSendesTil.PostAdresse.PostSted = "Digital levering";

            return svarSendesTil;
        }
    }
}