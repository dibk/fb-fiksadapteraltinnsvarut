﻿using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Models.ApiModels;
using System.Collections.Generic;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services
{
    public interface IFileDownloadStatusService
    {
        void Add(FileDownloadStatus fileDownloadStatus);
        bool Update(FileDownloadStatus fileDownloadStatus);
        List<FileDownloadStatus> GetFileDownloadStatusesForArchiveReference(string archiveReference);
        List<FileDownloadStatus> GetFileDownloadStatusesForArchiveReferences(List<string> archiveReferences);
        List<FileDownloadStatus> GetFileDownloadStatusBy(string guid, string filename);        
        FileDownloadStatus GetFileDownloadStatusForArchiveReferencesAndFileType(string archiveReferences, FilTyperForNedlasting fileType);
    }
}
