﻿using DIBK.FBygg.Altinn.MapperCodelist;
using Hangfire;
using KommIT.FIKS.AdapterAltinnSvarUt.HangfireConfiguration;
using KommIT.FIKS.AdapterAltinnSvarUt.Logger;
using Serilog;
using System;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Codelists
{
    public class CodeListRefreshScheduler
    {
        private ICodeListService _codeListService;

        public CodeListRefreshScheduler()
        {
            _codeListService = new CodeListService();
        }

        [HangfireBasicJobLogger]
        public void RefreshCodeLists()
        {
            _codeListService.LoadCodeListInCache();
        }

        public void ScheduleRefreshingOfCodeLists()
        {
            Log.Logger.Debug("Schedules caching of code lists");
            //Initial loading of code lists into cache
            BackgroundJob.Schedule(() => RefreshCodeLists(), TimeSpan.FromSeconds(10));

            //Scheduling refresh of cache each # hours
            RecurringJob.AddOrUpdate(() => RefreshCodeLists(), CronExpressions.HourInterval(3));
        }
    }
}