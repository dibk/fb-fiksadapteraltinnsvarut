﻿using System.Collections.Generic;

namespace DIBK.FBygg.Altinn.MapperCodelist
{
    public interface ICodeListService
    {
        void LoadCodeListInCache();
        Dictionary<string, CodelistFormat> GetCodelist(string codeListName, RegistryType registryType);
    }
}
