﻿using Serilog;
using System;
using System.Configuration;
using System.Net;
using System.Text;

namespace DIBK.FBygg.Altinn.MapperCodelist
{
    public class CodeListDownloader : ICodeListDownloader
    {
        private readonly ILogger _logger = Log.ForContext<CodeListDownloader>();
        public string DownloadCodeList(string url)
        {
            var result = string.Empty;
            try
            {
                using (WebClient client = new WebClient())
                {
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                    client.Encoding = Encoding.UTF8;
                    client.Headers.Add(HttpRequestHeader.Accept, "application/json");
                    result = client.DownloadString(url);
                }
            }
            catch (Exception ex)
            {

                _logger.Error(ex, "Unable to retrieve code list from URL: {url}", url);
                throw;
            }

            return result;
        }

        public string DownloadCodeList(string codeListName, RegistryType registryType)
        {
            var url = GetUrlForCodeList(codeListName, registryType);
            return DownloadCodeList(url);
        }

        private string GetUrlForCodeList(string codeListName, RegistryType registryType)
        {
            switch (registryType)
            {
                case RegistryType.Kommunenummer:
                    return $"{ConfigurationManager.AppSettings["GeonorgeRegistryMunicipalityApiUrl"]}{codeListName}";
                case RegistryType.NabovarselPlan:
                    return $"{ConfigurationManager.AppSettings["GeonorgeRegistryNabovarselPlanCodeListUrl"]}{codeListName}";
                case RegistryType.Arbeidstilsynet:
                    return $"{ConfigurationManager.AppSettings["GeonorgeRegistryArbeidstilsynetCodeListUrl"]}{codeListName}";
                case RegistryType.Ebyggesak:
                    return $"{ConfigurationManager.AppSettings["GeonorgeRegistryEbyggesakCodeListUrl"]}{codeListName}";

                default:
                    return $"{ConfigurationManager.AppSettings["GeonorgeRegistryApiUrl"]}{codeListName}";
            }
        }
    }
}