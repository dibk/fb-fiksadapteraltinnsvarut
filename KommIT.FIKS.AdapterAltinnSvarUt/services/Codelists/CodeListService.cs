﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Caching;
using Newtonsoft.Json.Linq;
using Serilog;

namespace DIBK.FBygg.Altinn.MapperCodelist
{
    public class CodeListService : ICodeListService, IDisposable
    {
        private readonly ILogger _logger = Log.ForContext<CodeListService>();
        private ICacheService _cacheService;
        private readonly ICodeListDownloader _codeListDownloader;

        public CodeListService()
        {
            _cacheService = MemoryCacheService.Instance;
            _codeListDownloader = new CodeListDownloader();
        }

        public CodeListService(ICacheService cacheService, ICodeListDownloader codeListDownloader)
        {
            _cacheService = cacheService;
            _codeListDownloader = codeListDownloader;
        }

        public bool IsCodelistValid(string codeListName, string codelistEntry, RegistryType registryType = RegistryType.KodelisteByggesoknad)
        {
            if (String.IsNullOrEmpty(codelistEntry)) return false;
            Dictionary<string, CodelistFormat> codelist = GetCodelist(codeListName, registryType);
            return codelist.ContainsKey(codelistEntry);
        }

        public bool IsCodelistLabelValid(string codeListName, string codelistEntry, string label, RegistryType registryType = RegistryType.KodelisteByggesoknad)
        {
            if (String.IsNullOrEmpty(codelistEntry) || string.IsNullOrEmpty(label)) return false;
            Dictionary<string, CodelistFormat> codelist = GetCodelist(codeListName, registryType);
            CodelistFormat result;
            if (codelist.TryGetValue(codelistEntry, out result))
            {
                if (result.Name.Equals(label)) return true;
                else return false;
            }
            else return false;
        }


        public string GetValidKommuneName(string kommunenummer)
        {
            string kommuneName = null;
            Dictionary<string, CodelistFormat> kommuneNumbers = new CodeListService().GetCodelist(CodelistNames.Kommunenummer.ToString(), RegistryType.Kommunenummer);
            if (kommuneNumbers.ContainsKey(kommunenummer))
            {
                kommuneName = kommuneNumbers[kommunenummer].Name;
            }

            return kommuneName;
        }

        public Dictionary<string, CodelistFormat> GetCodelist(string codeListName, RegistryType registryType = RegistryType.KodelisteByggesoknad)
        {
            return GetCodeListFromCache(codeListName, registryType);
        }

        private Dictionary<string, CodelistFormat> ParseCodeList(string data)
        {
            var codeList = new Dictionary<string, CodelistFormat>(StringComparer.CurrentCultureIgnoreCase);

            var response = JObject.Parse(data);

            foreach (var code in response["containeditems"])
            {
                string codevalue = code["codevalue"].ToString();
                string codename = code["label"].ToString();
                string codestatus = code["status"].ToString();
                string codeDescription;


                if (code["description"] == null)
                {
                    codeDescription = "";
                }
                else
                {
                    codeDescription = code["description"].ToString();
                }


                if (!string.IsNullOrWhiteSpace(codevalue) && !codeList.ContainsKey(codevalue))
                {
                    codeList.Add(codevalue, new CodelistFormat(codename, codeDescription, codestatus));
                }
            }

            return codeList;
        }
        private Dictionary<string, CodelistFormat> GetCodeListFromCache(string codeListName, RegistryType registryType = RegistryType.KodelisteByggesoknad)
        {
            var cachedValue = _cacheService.GetObject(codeListName) as Dictionary<string, CodelistFormat>;

            if (cachedValue != null)
                return cachedValue;

            RefreshCacheFor(codeListName, registryType);
            cachedValue = _cacheService.GetObject(codeListName) as Dictionary<string, CodelistFormat>;

            if (cachedValue == null)
            {
                //Refresh of cache didn't work, which means that there is no list available.
                _logger.Error("Code list {CodeListName} wasn't available in cache or from provider", codeListName);
            }

            return cachedValue;
        }

        public void LoadCodeListInCache()
        {
            GetMunicipalityCodesForCache();
            GetAllByggsoknadCodeListsForCache();
            GetAllNabovarselPlanCodeListsForCache();
        }

        private void GetAllByggsoknadCodeListsForCache()
        {
            _logger.Debug("Retrieves list of all code lists from GeoNorge");
            var result = _codeListDownloader.DownloadCodeList(ConfigurationManager.AppSettings["GeonorgeRegistryByggesoknadCodeListsUrl"]);

            if (!string.IsNullOrEmpty(result))
            {
                var codeListNames = GetCodeListNames(result);

                foreach (var name in codeListNames)
                    RefreshCacheFor(name, RegistryType.KodelisteByggesoknad);
            }
            else
            {
                _logger.Error("Unable to retrieve list of all code lists from GeoNorge");
            }
        }

        private void GetMunicipalityCodesForCache()
        {
            _logger.Debug("Refreshes cache for municipality code lists");
            RefreshCacheFor(CodelistNames.Kommunenummer.ToString(), RegistryType.Kommunenummer);
        }
        private void GetAllNabovarselPlanCodeListsForCache()
        {
            _logger.Debug("Retrieves list of all code lists from GeoNorge");
            var result = _codeListDownloader.DownloadCodeList(ConfigurationManager.AppSettings["GeonorgeRegistryNabovarselPlanCodeListUrl"]);

            if (!string.IsNullOrEmpty(result))
            {
                var codeListNames = GetCodeListNames(result);

                foreach (var name in codeListNames)
                    RefreshCacheFor(name, RegistryType.NabovarselPlan);
            }
            else
            {
                _logger.Error("Unable to retrieve list of all code lists from GeoNorge");
            }
        }



        /// <summary>
        /// Refreshes the cached code list by downloading and adding it to the cache
        /// </summary>
        /// <param name="codeListName"></param>
        /// <param name="registryType"></param>
        private void RefreshCacheFor(string codeListName, RegistryType registryType = RegistryType.KodelisteByggesoknad)
        {
            var codeList = _codeListDownloader.DownloadCodeList(codeListName, registryType);

            if (!string.IsNullOrEmpty(codeList))
            {
                _cacheService.AddOrReplaceCachedItem(codeListName, ParseCodeList(codeList), DateTime.Now.AddHours(24));
                _logger.Debug("Cache refreshed for the {CodeListName} code list", codeListName);
            }
            else
                _logger.Error("Failed to download and refresh the {CodeListName} code list", codeListName);
        }

        /// <summary>
        /// Parses and retrieves the code list name from the ID field. Cannot use the "label" element since it contains Norwegian characters.
        /// </summary>
        /// <param name="data">JSON string</param>
        /// <returns></returns>
        private List<string> GetCodeListNames(string data)
        {
            var codeListNames = new List<string>();

            if (!string.IsNullOrEmpty(data))
            {
                var response = JObject.Parse(data);

                foreach (var registry in response["containedSubRegisters"])
                {
                    var registryId = registry["id"].ToString();

                    var codeListName = registryId.Split('/').Last();
                    codeListNames.Add(codeListName);
                }
            }
            else
                _logger.Error("Unable to find code list names. The JSON-string is empty.");

            return codeListNames;
        }

        // new method with havin in consideration Status

        public bool IsCodeValidInCodeList(string codeListName, string codelistEntry)
        {
            if (String.IsNullOrEmpty(codelistEntry)) return false;
            Dictionary<string, CodelistFormat> codelist = GetCodelist(codeListName);
            var validList = codelist.Where(c => c.Value.Status.ToLower() != "erstattet" && c.Value.Status.ToLower() != "utgått").ToDictionary(d => d.Key, d => d.Value);
            return validList.ContainsKey(codelistEntry);
        }
        public string CodeHaveStatus(string codeListName, string codelistEntry)
        {
            if (!String.IsNullOrEmpty(codelistEntry) || !String.IsNullOrEmpty(codeListName))
            {
                Dictionary<string, CodelistFormat> codelist = GetCodelist(codeListName);
                CodelistFormat codelistFormat;
                if (codelist.TryGetValue(codelistEntry, out codelistFormat))
                {
                    return codelistFormat.Status;
                }
            }
            return null;
        }

        //public Dictionary<string, CodelistFormat> GetValidCodelist(string codeListName, RegistryType registryType = RegistryType.KodelisteByggesoknad)
        //{
        //    string rawCodeList = DownloadCodeList(codeListName, registryType);
        //    var validList = ParseCodeList(rawCodeList).Where(c => c.Value.Status.ToLower() != "erstattet" && c.Value.Status.ToLower() != "utgått").ToDictionary(co => co.Key, co => co.Value);
        //    return validList;
        //}
        //public Dictionary<string, CodelistFormat> GetInvalidCodelist(string codeListName, RegistryType registryType = RegistryType.KodelisteByggesoknad)
        //{
        //    string rawCodeList = DownloadCodeList(codeListName, registryType);
        //    var validList = ParseCodeList(rawCodeList).Where(c => c.Value.Status.ToLower() != "erstattet" && c.Value.Status.ToLower() != "utgått").ToDictionary(co => co.Key, co => co.Value);
        //    return validList;
        //}

        public bool IsCodeInCodelist(string codeListName, string codelistEntry)
        {
            if (String.IsNullOrEmpty(codelistEntry)) return false;
            Dictionary<string, CodelistFormat> codelist = GetCodelist(codeListName);
            return codelist.ContainsKey(codelistEntry);
        }

        public void Dispose()
        {
            _cacheService = null;
        }
    }

    //public enum StatusCode
    //{
    //    //Forslag:
    //    Utkast,
    //    IkkeGodkjent,
    //    SendtInn,
    //    //Gyldig:
    //    Gyldig,
    //    //Historiske:
    //    Utgått,
    //    Erstattet
    //}

    public enum CodelistNames
    {
        Anleggstype,
        Avlopstilknytning,
        BeregningsregelGradAvUtnytting,
        Bruksenhetstype,
        Bygningstype,
        Dispensasjonstype,
        EnergiforsyningType,
        Etasjeplan,
        Fagomrade,
        FarligAvfall,
        Funksjon,
        Kjokkentilgang,
        Naeringsgruppe,
        OrdinaertAvfall,
        Partstype,
        Plantype,
        Soknadstype,
        Tiltaksformal,
        Tiltaksklasse,
        Tiltaktype,
        Vanntilknytning,
        Varmefordeling,
        Vegtype,
        Kommunenummer
    }

    public enum RegistryType
    {
        Kommunenummer,
        KodelisteByggesoknad,
        NabovarselPlan,
        Arbeidstilsynet,
        Ebyggesak,
    }

    public class CodelistFormat
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }

        public CodelistFormat()
        {
        }
        public CodelistFormat(string name, string description, string status)
        {
            Name = name;
            Description = description;
            Status = status;
        }
    }
}