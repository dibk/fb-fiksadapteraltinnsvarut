﻿namespace DIBK.FBygg.Altinn.MapperCodelist
{
    public interface ICodeListDownloader
    {
        string DownloadCodeList(string url);
        string DownloadCodeList(string codeListName, RegistryType registryType);
    }
}
