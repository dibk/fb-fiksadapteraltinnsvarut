﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Models.ApiModels;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Receipt
{
    public class DecisionService : IDecisionService
    {
        private readonly ApplicationDbContext _dbContext;

        public DecisionService(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void AddDecision(Vedtak vedtak)
        {
            Decision dc = new Decision();
            dc.ArchiveReference = vedtak.AltinnArkivreferanse;
            dc.DecisionDate = vedtak.Vedtaksdato;
            dc.DecisionStatus = vedtak.Status;
            dc.MunicipalityArchiveCaseYear = vedtak.Saksaar;
            dc.MunicipalityArchiveCaseSequence = vedtak.Sakssekvensnummer;
            dc.DocumentUrl = vedtak.UtgaaendeDokumentUrl;

            if (vedtak.Vilkaar != null)
            {
                foreach (var vilkaar in vedtak.Vilkaar)
                {
                    dc.Terms.Add(new Terms()
                    {
                        TermsId = vilkaar.VilkaarID,
                        TermForProcess = vilkaar.VilkaarTilProsess,
                        TermDescription = vilkaar.Beskrivelse
                    });
                }
            }

            //Lagre vedtak og vilkår
            SaveDecision(dc);
        }

        private void SaveDecision(Models.Decision decision)
        {
            _dbContext.Decisions.Add(decision);
            _dbContext.SaveChanges();
        }
    }
}