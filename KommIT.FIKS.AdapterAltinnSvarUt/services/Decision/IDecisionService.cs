﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Receipt
{
    public interface IDecisionService 
    {
        void AddDecision(Models.ApiModels.Vedtak vedtak);

    }
}
