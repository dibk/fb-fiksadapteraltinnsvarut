﻿using KommIT.FIKS.AdapterAltinnSvarUt.Models.ApiModels;
using KommIT.FIKS.AdapterAltinnSvarUt.Services;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Authentication;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;

namespace KommIT.FIKS.AdapterAltinnSvarUt.ControllersApi.FormLogData
{
    [ApiExplorerSettings(IgnoreApi = true)]
    [BasicAuthentication]
    public class FormMetadataApiController : ApiController
    {
        private readonly IFormMetadataService _formMetadataService;

        public FormMetadataApiController(IFormMetadataService formMetadataService)
        {
            _formMetadataService = formMetadataService;
        }

        [Route("api/formlogdata/{archiveReference}/formmetadata")]
        [HttpGet]
        public IHttpActionResult GetFormMetadata(string archiveReference)
        {
            var result = _formMetadataService.GetFormMetadata(archiveReference);
            if (result == null)
                return NotFound();
            return Ok<FormMetadataApiModel>(FormMetadataApiMapper.Map(result));
        }

        [Route("api/formlogdata/formmetadata")]
        [HttpPost]
        public IHttpActionResult PostFormMetadata(FormMetadataApiModel formMetadata)
        {
            if (_formMetadataService.SaveFormMetadata(FormMetadataApiMapper.Map(formMetadata)))
            {
                return Ok<FormMetadataApiModel>(formMetadata);
            }
            else
            {
                return Content(System.Net.HttpStatusCode.Conflict, $"FormMetadata for archivereference {formMetadata.ArchiveReference}, allready exists");
            }
        }

        [Route("api/formlogdata/{archiveReference}/formmetadata")]
        [HttpPut]
        public IHttpActionResult PutFormMetadata(string archiveReference, [FromBody] FormMetadataApiModel formMetadata)
        {
            if (!_formMetadataService.MetadataExists(archiveReference))
                return NotFound();

            _formMetadataService.UpdateFormMetadata(FormMetadataApiMapper.Map(formMetadata));
            return Ok();
        }

        [Route("api/formlogdata/{archiveReference}/formmetadata/attachments")]
        [HttpPost]
        public IHttpActionResult PostFormAttachments(string archiveReference, [FromBody] List<FormAttachmentMetadataApiModel> attachmentMetadata)
        {
            if (!_formMetadataService.MetadataExists(archiveReference))
                return NotFound();

            if (!attachmentMetadata.Any())
                return BadRequest("Payload is empty");

            _formMetadataService.SaveFormAttachmentMetadata(attachmentMetadata.Select(am => FormAttachmentMetadataMapper.Map(archiveReference, am)).ToList());
            return Ok();
        }
    }
}