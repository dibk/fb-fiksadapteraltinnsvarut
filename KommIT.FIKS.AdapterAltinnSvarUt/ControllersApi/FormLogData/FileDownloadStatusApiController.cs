﻿using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Services;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Authentication;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;

namespace KommIT.FIKS.AdapterAltinnSvarUt.ControllersApi.FormLogData
{
    [ApiExplorerSettings(IgnoreApi = true)]
    [BasicAuthentication]
    public class FileDownloadStatusApiController : ApiController
    {
        private readonly IFormMetadataService _formMetadataService;
        private readonly IFileDownloadStatusService _fileDownloadStatusService;
        private readonly string _serverUrl;

        public FileDownloadStatusApiController(IFormMetadataService formMetadataService, IFileDownloadStatusService fileDownloadStatusService)
        {
            _formMetadataService = formMetadataService;
            _fileDownloadStatusService = fileDownloadStatusService;

            _serverUrl = ConfigurationManager.AppSettings["ServerURL"];
        }

        [Route("api/formlogdata/{archiveReference}/filedownloads")]
        [HttpGet]
        public IHttpActionResult GetFileDownloadStatuses(string archiveReference)
        {
            if (!_formMetadataService.MetadataExists(archiveReference))
                return NotFound();

            var result = _fileDownloadStatusService.GetFileDownloadStatusesForArchiveReference(archiveReference);

            return Ok<IEnumerable<FileDownloadStatus>>(result);
        }

        [Route("api/formlogdata/{archiveReference}/filedownloads")]
        [HttpPost]
        public IHttpActionResult AddFiledownloadStatus(string archiveReference, [FromBody] FileDownloadStatus filedownloadStatus)
        {
            if (!_formMetadataService.MetadataExists(archiveReference))
                return NotFound();

            if (filedownloadStatus == null)
                return BadRequest("Payload is empty");

            filedownloadStatus.TimeReceived = DateTime.Now;
            filedownloadStatus.IsDeleted = false;

            _fileDownloadStatusService.Add(filedownloadStatus);

            var createdEntity = _fileDownloadStatusService.GetFileDownloadStatusBy(filedownloadStatus.Guid.ToString(), filedownloadStatus.Filename).FirstOrDefault();

            return Created(new Uri($"{_serverUrl}api/formlogdata/{archiveReference}/filedownloads"), createdEntity);
        }

        [Route("api/formlogdata/{archiveReference}/filedownloads")]
        [HttpPut]
        public IHttpActionResult UpdateFiledownloadStatus(string archiveReference, [FromBody] FileDownloadStatus filedownloadStatus)
        {
            if (!_formMetadataService.MetadataExists(archiveReference))
                return NotFound();

            if (filedownloadStatus == null)
                return BadRequest("Payload is empty");

            var updated = _fileDownloadStatusService.Update(filedownloadStatus);

            if (updated)
                return new System.Web.Http.Results.StatusCodeResult(System.Net.HttpStatusCode.NoContent, Request);

            return BadRequest($"Unable to update filedownloadstatus. referenceid: {archiveReference} id: {filedownloadStatus.Id} filename: {filedownloadStatus.Filename}");
        }
    }
}