﻿using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Services;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Authentication;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;

namespace KommIT.FIKS.AdapterAltinnSvarUt.ControllersApi.FormLogData
{
    [ApiExplorerSettings(IgnoreApi = true)]
    [BasicAuthentication]
    public class LogEntriesApiController : ApiController
    {
        private readonly IFormMetadataService _formMetadataService;
        private readonly ILogEntryService _logEntryService;

        public LogEntriesApiController(IFormMetadataService formMetadataService, ILogEntryService logEntryService)
        {
            _formMetadataService = formMetadataService;
            _logEntryService = logEntryService;
        }

        [Route("api/formlogdata/{archiveReference}/logentries")]
        [HttpPost]
        public IHttpActionResult PostLogEntries(string archiveReference, [FromBody] List<LogEntry> logEntries)
        {
            if (!_formMetadataService.MetadataExists(archiveReference))
                return NotFound();

            _logEntryService.SaveBulk(logEntries);
            return Ok();
        }
    }
}