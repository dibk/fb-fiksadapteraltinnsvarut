﻿using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Services;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Authentication;
using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;

namespace KommIT.FIKS.AdapterAltinnSvarUt.ControllersApi.FormLogData
{
    [ApiExplorerSettings(IgnoreApi = true)]
    [BasicAuthentication]
    public class DistributionFormsApiController : ApiController
    {
        private readonly IFormMetadataService _formMetadataService;

        public DistributionFormsApiController(IFormMetadataService formMetadataService)
        {
            _formMetadataService = formMetadataService;
        }

        [Route("api/formlogdata/{archiveReference}/distributions")]
        [HttpPost]
        public IHttpActionResult PostDistributions(string archiveReference, [FromBody] List<DistributionForm> distributionForms)
        {
            if (!_formMetadataService.MetadataExists(archiveReference))
                return NotFound();

            //Check if distribution form already exists
            foreach (var distForm in distributionForms)
            {
                var result = _formMetadataService.GetDistributionFormByGuid(distForm.Id);
                if (result != null)
                    return Conflict();
            }

            _formMetadataService.InsertDistributionForms(distributionForms);

            return Ok();
        }

        [Route("api/formlogdata/distributions/{id}")]
        [HttpGet]
        public IHttpActionResult GetDistribution(string id)
        {
            Guid distributionIdGuid;
            var GuidOK = Guid.TryParse(id, out distributionIdGuid);

            if (GuidOK)
            {
                var distributionForm = _formMetadataService.GetDistributionFormByGuid(distributionIdGuid);
                return Ok<DistributionForm>(distributionForm);
            }
            else
            {
                return NotFound();
            }
        }

        [Route("api/formlogdata/{archiveReference}/distributions")]
        [HttpGet]
        public IHttpActionResult GetDistributions(string archiveReference)
        {
            var ar = archiveReference.ToUpper();

            if (!_formMetadataService.MetadataExists(ar))
                return NotFound();

            var distributionForms = _formMetadataService.GetDistributionForm(ar);
            return Ok(distributionForms);
        }

        [Route("api/formlogdata/{archiveReference}/distributions/{id}")]
        [HttpPut]
        public IHttpActionResult UpdateDistribution(string archiveReference, string id, [FromBody] DistributionForm updatedDistributionForm)
        {
            var ar = archiveReference.ToUpper();

            if (!_formMetadataService.MetadataExists(ar))
                return NotFound();

            Guid distributionIdGuid;
            var GuidOK = Guid.TryParse(id, out distributionIdGuid);

            if (!GuidOK)
                return NotFound();

            var form = _formMetadataService.GetDistributionFormByGuid(distributionIdGuid);

            if (form == null)
                return NotFound();

            if (!ar.Equals(form.InitialArchiveReference, StringComparison.OrdinalIgnoreCase))
                return NotFound();

            form.DistributionStatus = updatedDistributionForm.DistributionStatus;
            form.Signed = updatedDistributionForm.Signed;
            form.SignedArchiveReference = updatedDistributionForm.SignedArchiveReference;
            form.SubmitAndInstantiatePrefilledFormTaskReceiptId = updatedDistributionForm.SubmitAndInstantiatePrefilledFormTaskReceiptId;
            form.RecieptSent = updatedDistributionForm.RecieptSent;
            form.RecieptSentArchiveReference = updatedDistributionForm.RecieptSentArchiveReference;
            form.ErrorMessage = updatedDistributionForm.ErrorMessage;
            form.DistributionReference = updatedDistributionForm.DistributionReference;
            form.Printed = updatedDistributionForm.Printed;

            if (updatedDistributionForm.AnSaKoStatus != null)
            {
                if (form.AnSaKoStatus == null)
                    form.AnSaKoStatus = new AnSaKoStatus();

                form.AnSaKoStatus.AnSaKoProcessStatus = updatedDistributionForm.AnSaKoStatus.AnSaKoProcessStatus;
                form.AnSaKoStatus.SigningDeadline = updatedDistributionForm.AnSaKoStatus.SigningDeadline;
                form.AnSaKoStatus.StatusDetails = updatedDistributionForm.AnSaKoStatus.StatusDetails;
            }

            _formMetadataService.SaveDistributionForm(form);
            _formMetadataService.UpdateReferencedDistributions(form.Id, updatedDistributionForm);

            return Ok();
        }
    }
}