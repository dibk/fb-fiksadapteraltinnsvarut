using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Models.ApiModels;
using KommIT.FIKS.AdapterAltinnSvarUt.Services;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Authentication;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.FileDownload;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;
using Serilog;
using Serilog.Context;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;

namespace KommIT.FIKS.AdapterAltinnSvarUt.ControllersApi
{
    /// <summary>
    /// API for informasjon om status på altinn forsendelser
    /// </summary>
    [EnableCors(origins: "*", headers: "*", methods: "*", exposedHeaders: "x-correlation-id")]
    public class StatusController : ApiController
    {
        private readonly ILogEntryService _logEntryService;
        private readonly IFormMetadataService _formMetadataService;
        private readonly IFileDownloadStatusService _fileDownloadStatusService;
        private readonly string _serverURL;
        private readonly ILogger _logger;
        private readonly IAuthorizationService _authorizationService;

        public StatusController(ILogEntryService logEntryService, IFormMetadataService formMetadataService,
            IFileDownloadStatusService fileDownloadStatusService, ILogger logger, IAuthorizationService authorizationService)
        {
            _logEntryService = logEntryService;
            _formMetadataService = formMetadataService;
            _fileDownloadStatusService = fileDownloadStatusService;
            _serverURL = ConfigurationManager.AppSettings["ServerURL"];
            _logger = logger;
            _authorizationService = authorizationService;
        }

        /// <summary>
        /// Hente ut status på en forsendelse
        /// </summary>
        /// <param name="arkivreferanse">arkivreferanse i Altinn (eks AR12345)</param>
        /// <returns></returns>
        /// <response code="200">ok</response>
        /// <response code="400">BadRequest - ugyldig forespørsel</response>
        /// <response code="404">NotFound - finner ikke innsending med gitt arkivreferanse i altinn. NB det kan ta flere minutter fra innsending til arkivreferanse er tilgjengelig i fellestjenester bygg</response>
        /// <response code="403">Forbidden - ingen tilgang</response>
        /// <response code="500">Error - intern feil</response>
        [BasicAuthentication]
        [Route("api/status/{arkivreferanse}")]
        [HttpGet]
        [ResponseType(typeof(Models.ApiModels.Status))]
        public HttpResponseMessage GetStatus(string arkivreferanse)
        {
            var archiveReference = arkivreferanse?.ToUpper();
            StatusApiResponse status;

            using (LogContext.PushProperty("ArchiveReference", archiveReference))
            {
                _logger.Verbose("Status API request");

                try
                {
                    status = GetStatusData(archiveReference);
                }
                catch (Exception ex)
                {
                    _logger.Error(ex, "Unexpected error occurred when getting status");
                    status = new StatusApiResponse();
                    status.HttpStatusCode = HttpStatusCode.InternalServerError;
                    status.Exception = ex;
                }

                if (status.HttpStatusCode == HttpStatusCode.OK)
                {
                    HttpResponseMessage response = Request.CreateResponse(status.HttpStatusCode, status.ApiModelsStatus);
                    return response;
                }

                if (status.HttpStatusCode == HttpStatusCode.InternalServerError)
                {
                    return Request.CreateErrorResponse(status.HttpStatusCode, status.Exception);
                }

                if (status.HttpStatusCode == HttpStatusCode.BadRequest)
                {
                    return Request.CreateResponse(status.HttpStatusCode);
                }

                if (status.HttpStatusCode == HttpStatusCode.NotFound)
                {
                    return Request.CreateResponse(status.HttpStatusCode, status.Description);
                }

                if (status.HttpStatusCode == HttpStatusCode.Forbidden)
                {
                    return Request.CreateResponse(status.HttpStatusCode);
                }

                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "API ukjent feil");
            }
        }

        /// <summary>
        /// Hente ut status på en forsendelse
        /// </summary>
        /// <param name="arkivreferanse">arkivreferanse i Altinn (eks AR12345)</param>
        /// <returns></returns>
        /// <response code="200">ok</response>
        /// <response code="400">BadRequest - ugyldig forespørsel</response>
        /// <response code="404">NotFound - finner ikke innsending med gitt arkivreferanse i altinn. NB det kan ta flere minutter fra innsending til arkivreferanse er tilgjengelig i fellestjenester bygg</response>
        /// <response code="403">Forbidden - ingen tilgang</response>
        /// <response code="500">Error - intern feil</response>
        [Route("api/statusNBA/{arkivreferanse}")]
        [HttpGet]
        [ResponseType(typeof(Models.ApiModels.Status))]
        [ApiExplorerSettings(IgnoreApi = true)]
        public HttpResponseMessage GetStatusNBA(string arkivreferanse)
        {
            var archiveReference = arkivreferanse?.ToUpper();
            StatusApiResponse status;

            using (LogContext.PushProperty("ArchiveReference", archiveReference))
            {

                if (ConfigurationManager.AppSettings["RunInTestMode"] != "true")
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound, "Ikke tilgjenglig i prod miljø");
                }

                _logger.Verbose("Status API request");
                _logger.Verbose("API requested in No Basic Authentication mode. Server in test mode.");

                try
                {
                    status = GetStatusData(archiveReference);
                }
                catch (Exception ex)
                {
                    _logger.Error(ex, "Unexpected error occurred when getting status");
                    status = new StatusApiResponse();
                    status.HttpStatusCode = HttpStatusCode.InternalServerError;
                    status.Exception = ex;
                }

                if (status.HttpStatusCode == HttpStatusCode.OK)
                {
                    HttpResponseMessage response = Request.CreateResponse(status.HttpStatusCode, status.ApiModelsStatus);
                    return response;
                }

                if (status.HttpStatusCode == HttpStatusCode.InternalServerError)
                {
                    return Request.CreateErrorResponse(status.HttpStatusCode, status.Exception);
                }

                if (status.HttpStatusCode == HttpStatusCode.BadRequest)
                {
                    return Request.CreateResponse(status.HttpStatusCode);
                }

                if (status.HttpStatusCode == HttpStatusCode.NotFound)
                {
                    return Request.CreateResponse(status.HttpStatusCode, status.Description);
                }

                if (status.HttpStatusCode == HttpStatusCode.Forbidden)
                {
                    return Request.CreateResponse(status.HttpStatusCode);
                }

                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "API ukjent feil");
            }
        }

        internal StatusApiResponse GetStatusData(string arkivreferanse)
        {
            FormMetadata formMetadata;
            StatusApiResponse returnValue = new StatusApiResponse();
            Models.ApiModels.Status statusapi = new Models.ApiModels.Status();

            if (string.IsNullOrEmpty(arkivreferanse))
            {
                _logger.Error("Ingen AR angitt");
                returnValue.HttpStatusCode = HttpStatusCode.BadRequest;
                return returnValue;
            }

            formMetadata = _formMetadataService.GetFormMetadata(arkivreferanse);
            if (formMetadata == null)
            {
                _logger.Information("Finner ingen forsendelse med denne Altinn arkivreferanse");
                returnValue.HttpStatusCode = HttpStatusCode.NotFound;
                returnValue.Description = "Finner ingen forsendelse med denne Altinn arkivreferanse";
                return returnValue;
            }

            //Has access
            if (!_authorizationService.UserHasAccess(formMetadata))
            {
                returnValue.HttpStatusCode = HttpStatusCode.Forbidden;
                return returnValue;
            }

            try
            {
                _logger.Verbose("Genererer API melding ... generelt");
                statusapi.Arkivreferanse = arkivreferanse;
                statusapi.AltinnInnsending = formMetadata.ArchiveTimestamp;
                statusapi.ForsendelsesIdSvarUt = formMetadata.SvarUtForsendelsesId;
                statusapi.SvarUtForsendelse = formMetadata.SvarUtShippingTimestamp;
                statusapi.Saksaar = formMetadata.MunicipalityArchiveCaseYear;
                statusapi.Sakssekvensnummer = formMetadata.MunicipalityArchiveCaseSequence;
                statusapi.OffentligJournalUrl = formMetadata.MunicipalityPublicArchiveCaseUrl;
                statusapi.StatusInnsending = formMetadata.Status;
                statusapi.Kvitteringsrapport = formMetadata.DistributionRecieptLink;
                statusapi.Nabovarsel = null;
                statusapi.Slettet = formMetadata.Slettet;

                //Hendelser
                _logger.Verbose("Genererer API melding ... hendelser");
                List<Hendelse> list = new List<Hendelse>();

                var loglist = _logEntryService.GetTopLogEntriesByArchiveReference(formMetadata.ArchiveReference, 30);

                foreach (var logitem in loglist)
                {
                    list.Add(new Hendelse()
                    {
                        Tidspunkt = logitem.Timestamp,
                        Beskrivelse = TextObfuscation.ObfuscateSSN(logitem.Message),
                        VisMerUrl = logitem.Url
                    });
                }

                statusapi.Hendelser = list;

                _logger.Verbose("Genererer API melding ... vilkar");
                var decision = _formMetadataService.GetDecision(arkivreferanse);
                if (decision != null)
                {
                    statusapi.Vedtak = decision.DecisionStatus;
                    statusapi.Vedtaksdato = decision.DecisionDate;
                    statusapi.VedtaksDokumentUrl = decision.DocumentUrl;

                    List<Vilkaar> listvilkaar = new List<Vilkaar>();
                    foreach (var term in decision.Terms)
                    {
                        listvilkaar.Add(new Vilkaar(term.TermsId, term.TermForProcess, term.TermDescription));
                    }

                    statusapi.Vilkaar = listvilkaar;
                }

                // Distribusjoner
                _logger.Verbose("Genererer API melding ... distribusjoner");
                var listDistribusjon = _formMetadataService.GetDistributionForm(arkivreferanse);

                statusapi.Distribusjoner = new List<Distribusjon>();

                var archiveReferences = new List<string>();
                archiveReferences.Add(arkivreferanse);
                archiveReferences.AddRange(listDistribusjon.Where(d => !string.IsNullOrEmpty(d.SignedArchiveReference)).Select(d => d.SignedArchiveReference).ToList());

                var fileDownloadStatuses = _fileDownloadStatusService.GetFileDownloadStatusesForArchiveReferences(archiveReferences);

                var distributionIds = new List<Guid>();
                distributionIds.AddRange(listDistribusjon.Select(s => s.Id));

                var postDistributionMetaDatas = _formMetadataService.GetPostDistributionMetaDatas(distributionIds);

                foreach (var distributionItem in listDistribusjon)
                {
                    List<FilInformasjonForNedlasting> filInformasionForNedlastings =
                        new List<FilInformasjonForNedlasting>();

                    if (distributionItem.DistributionStatus == DistributionStatus.receiptSent || distributionItem.DistributionStatus == DistributionStatus.signed)
                    {
                        foreach (var fileDownload in fileDownloadStatuses.Where(f => f.ArchiveReference == distributionItem.SignedArchiveReference))
                        {
                            filInformasionForNedlastings.Add(new FilInformasjonForNedlasting(GenerateDownloadApiUri(fileDownload),
                                                                                             fileDownload.FormName,
                                                                                             fileDownload.MimeType,
                                                                                             fileDownload.Filename));
                        }
                    }

                    var postDistributionMetaData = postDistributionMetaDatas.Where(p => p.Id == distributionItem.Id).FirstOrDefault();

                    statusapi.Distribusjoner.Add(new Distribusjon()
                    {
                        Distribusjonstype = distributionItem.DistributionType,
                        ReferanseSluttbrukersystem = distributionItem.ExternalSystemReference,
                        Status = distributionItem.DistributionStatus.ToString(),
                        ReferanseIdSendt = distributionItem.SubmitAndInstantiatePrefilledFormTaskReceiptId,
                        Feilmelding = TextObfuscation.ObfuscateSSN(distributionItem.ErrorMessage),
                        Sendt = distributionItem.SubmitAndInstantiatePrefilled,
                        ReferanseIdSignert = distributionItem.SignedArchiveReference,
                        Signert = distributionItem.Signed,
                        ReferanseIdKvittering = distributionItem.RecieptSentArchiveReference,
                        SendtKvittering = distributionItem.RecieptSent,
                        Filnedlastinger = filInformasionForNedlastings,
                        ReferanseOpprinneligDistribusjonSluttbrukersystem = distributionItem.InitialExternalSystemReference,
                        AltinnInnboksMeldingsreferanse = postDistributionMetaData?.ReferenceId,
                        AnSaKoStatus = distributionItem.AnSaKoStatus != null ? new AnSaKoStatusDetaljer()
                        {
                            Signeringsfrist = distributionItem.AnSaKoStatus.SigningDeadline,
                            Status = distributionItem.AnSaKoStatus.AnSaKoProcessStatus,
                            StatusDetaljer = distributionItem.AnSaKoStatus.StatusDetails
                        } : null
                    });
                }

                // Legg ut kvittering og nabovarsel-lenker i status api når disse forekommer
                if (!string.IsNullOrEmpty(statusapi.Kvitteringsrapport))
                {
                    //GetNabovarselKvittering(arkivreferanse, statusapi);
                    GetNabovarselKvittering(arkivreferanse, statusapi, fileDownloadStatuses);
                }

                // Legger til andre filer som kan lastes ned
                if (fileDownloadStatuses?.Count > 0)
                    statusapi.Filnedlastinger = fileDownloadStatuses
                                                    .Where(p => p.ArchiveReference.Equals(arkivreferanse, StringComparison.InvariantCultureIgnoreCase)
                                                               && !(p.Filename.Equals(Resources.TextStrings.KvitteringNabovarselFilename, StringComparison.InvariantCultureIgnoreCase)
                                                               || p.Filename.Equals(Resources.TextStrings.NabovarselMainformFilename, StringComparison.InvariantCultureIgnoreCase)))
                                                    .Select(p =>
                                                        new FilInformasjonForNedlasting(GenerateDownloadApiUri(p),
                                                                                        p.FileType.ToString(),
                                                                                        p.MimeType,
                                                                                        p.Filename))
                                                    .ToList();

                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, statusapi);
                _logger.Verbose("Status fetched");

                returnValue.HttpStatusCode = HttpStatusCode.OK;
                returnValue.ApiModelsStatus = statusapi;
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Unexpexted error occurred when getting status");
                returnValue.HttpStatusCode = HttpStatusCode.InternalServerError;
                returnValue.Exception = ex;
            }

            return returnValue;
        }

        internal void GetNabovarselKvittering(string archiveReference, Models.ApiModels.Status statusapi, List<FileDownloadStatus> fileDownloadStatuses)
        {
            statusapi.Kvitteringsrapport = null;

            // slå opp arkivreferansen i FileDownloadStatus
            var fs = fileDownloadStatuses.Where(f => f.ArchiveReference == archiveReference).ToList();
            if (!fs.Any())
            {
                return;
            }

            var receiptFile = fs.FirstOrDefault(f => FileDownloadApiUrlProvider.ReceiptTypes.Contains(f.FileType));

            if (receiptFile != null)
                statusapi.Kvitteringsrapport = GenerateDownloadApiUri(receiptFile);

            var nabovarselFile = fs.FirstOrDefault(f => f.Filename.Equals(Resources.TextStrings.NabovarselMainformFilename, StringComparison.InvariantCultureIgnoreCase));

            if (nabovarselFile != null)
                statusapi.Nabovarsel = GenerateDownloadApiUri(nabovarselFile);
        }

        private string GenerateDownloadApiUri(FileDownloadStatus fileDownload)
        {
            return new FileDownloadApiUrlProvider(_serverURL).GenerateDownloadUri(fileDownload);
        }
    }

    public class StatusApiResponse
    {
        public Exception Exception { get; set; }
        public String Description { get; set; }
        public HttpStatusCode HttpStatusCode { get; set; }
        public Models.ApiModels.Status ApiModelsStatus { get; set; }
    }
}