using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Models.ApiModels;
using KommIT.FIKS.AdapterAltinnSvarUt.Services;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Authentication;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Receipt;
using Serilog;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace KommIT.FIKS.AdapterAltinnSvarUt.ControllersApi
{
    /// <summary>
    /// API for kvitteringer fra kommuner
    /// </summary>
    [BasicAuthentication]
    [EnableCors(origins: "*", headers: "*", methods: "*", exposedHeaders: "x-correlation-id")]
    public class VedtakController : ApiController
    {
        private readonly ILogEntryService _logEntryService;
        private readonly IFormMetadataService _formMetadataService;
        private readonly ILogger _logger;
        private readonly IDecisionService _decisionService;

        public VedtakController(ILogEntryService logEntryService, IFormMetadataService formMetadataService, ILogger logger, IDecisionService decisionService)
        {
            _logEntryService = logEntryService;
            _formMetadataService = formMetadataService;
            _logger = logger;
            _decisionService = decisionService;
        }

        /// <summary>
        /// Oppretter vedtak og setter vilkår for videre prosess
        /// </summary>
        /// <response code="200">ok</response>
        /// <response code="400">BadRequest - ugyldig forespørsel</response>
        /// <response code="404">NotFound - finner ikke innsending med gitt arkivreferanse i altinn</response>
        /// <response code="403">Forbidden - ingen tilgang</response>
        /// <response code="500">Error - intern feil</response>
        [Route("api/vedtak/ny")]
        [HttpPost]
        public HttpResponseMessage PostVedtak(Vedtak vedtak)
        {
            if (vedtak != null)
            {
                try
                {
                    var formm = _formMetadataService.GetFormMetadata(vedtak.AltinnArkivreferanse);

                    if (formm != null)
                    {
                        LogEntry log = new LogEntry(vedtak.AltinnArkivreferanse, vedtak.Melding);
                        if (!String.IsNullOrEmpty(vedtak.OffentligJournalUrl))
                            log.Url = vedtak.OffentligJournalUrl;

                        _logEntryService.Save(log);

                        HttpResponseMessage response;

                        try
                        {
                            _decisionService.AddDecision(vedtak);
                            _logger.Information("Vedtak med evt vilkår fra kommune mottatt {Saksaar}/{Sakssekvensnummer} Altinn arkivreferanse {AltinnArkivreferanse}", vedtak.Saksaar, vedtak.Sakssekvensnummer, vedtak.AltinnArkivreferanse);
                            response = Request.CreateResponse(HttpStatusCode.OK);
                        }
                        catch (Exception ex)
                        {
                            _logger.Error(ex, "Vedtak feilet ved lagring i databasen");
                            response = Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
                        }

                        return response;
                    }
                    else
                    {
                        _logger.Information("Finner ingen forsendelse med denne Altinn arkivreferanse {AltinnArkivreferanse}", vedtak.AltinnArkivreferanse);
                        return Request.CreateResponse(HttpStatusCode.NotFound, "Finner ingen forsendelse med denne Altinn arkivreferanse");
                    }
                }
                catch (Exception ex)
                {
                    _logger.Error(ex, "Det har skjedd en feil");
                    return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
                }
            }
            else
            {
                _logger.Information("Vedtak er null ");
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }
    }
}