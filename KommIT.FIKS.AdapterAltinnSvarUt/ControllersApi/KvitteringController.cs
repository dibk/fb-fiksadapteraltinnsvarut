using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Models.ApiModels;
using KommIT.FIKS.AdapterAltinnSvarUt.Services;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Authentication;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Receipt;
using Serilog;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace KommIT.FIKS.AdapterAltinnSvarUt.ControllersApi
{
    /// <summary>
    /// API for kvitteringer fra kommuner
    /// </summary>
    [BasicAuthentication]
    [EnableCors(origins: "*", headers: "*", methods: "*", exposedHeaders: "x-correlation-id")]
    public class KvitteringController : ApiController
    {
        private readonly ILogEntryService _logEntryService;
        private readonly IFormMetadataService _formMetadataService;
        private readonly IReceiptService _receiptService;
        private readonly ILogger _logger;

        public KvitteringController(ILogEntryService logEntryService, IFormMetadataService formMetadataService, ILogger logger,
            IReceiptService receiptService)
        {
            _logEntryService = logEntryService;
            _formMetadataService = formMetadataService;
            _receiptService = receiptService;
            _logger = logger;
        }

        /// <summary>
        /// Oppretter kvittering for tildeling av saksnummer i kommunen
        /// </summary>
        /// <response code="200">ok</response>
        /// <response code="400">BadRequest - ugyldig forespørsel</response>
        /// <response code="401">Unauthorized - ingen tilgang</response>
        /// <response code="500">Error - intern feil</response>
        [Route("api/kvittering/ny")]
        [HttpPost]
        public HttpResponseMessage PostKvittering(KommuneKvittering kvittering)
        {
            if (kvittering != null)
            {
                try
                {
                    // NB! Flere like kvitteringer?
                    Receipt receipt = new Receipt(kvittering);
                    _receiptService.SaveReceipt(receipt);

                    // NB! Fungerer ikke for søknader eller svar på mangler som ikke har en arkivreferanse
                    if (_formMetadataService.MetadataExists(kvittering.AltinnArkivreferanse))
                    {
                        LogEntry log = new LogEntry(kvittering.AltinnArkivreferanse, kvittering.Melding);
                        if (!String.IsNullOrEmpty(kvittering.OffentligJournalUrl))
                            log.Url = kvittering.OffentligJournalUrl;

                        _logEntryService.Save(log);

                        if (kvittering.Saksaar > 0 && kvittering.Sakssekvensnummer > 0)
                        {
                            _formMetadataService.SaveMunicipalityArchiveCase(kvittering.AltinnArkivreferanse, kvittering.Saksaar, kvittering.Sakssekvensnummer);
                        }

                        _logger.Information("Kvittering for søknad fra kommune mottatt {Saksaar}/{Sakssekvensnummer} Altinn arkivreferanse {AltinnArkivreferanse}", kvittering.Saksaar, kvittering.Sakssekvensnummer, kvittering.AltinnArkivreferanse);
                    }
                    else
                    {
                        _logger.Information("Finner ikke metadata på arkivreferansen {arkivreferanse}", kvittering.AltinnArkivreferanse);
                    }

                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                    return response;
                }
                catch (Exception ex)
                {
                    _logger.Error(ex, "Det har skjedd en feil");
                    return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
                }
            }
            else
            {
                _logger.Information("Kvittering er null ");
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }
    }
}