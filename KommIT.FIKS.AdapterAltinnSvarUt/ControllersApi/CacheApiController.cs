﻿using DIBK.FBygg.Altinn.MapperCodelist;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Authentication;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.MunicipalityApi;
using System.Web.Http;
using System.Web.Http.Description;

namespace KommIT.FIKS.AdapterAltinnSvarUt.ControllersApi
{
    [BasicAuthentication]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class CacheApiController : ApiController
    {
        private readonly ICodeListService _codeListService;
        private readonly MunicipalityCacheService _municipalityCacheService;

        public CacheApiController(ICodeListService codeListService, MunicipalityCacheService municipalityCacheService)
        {
            _codeListService = codeListService;
            _municipalityCacheService = municipalityCacheService;
        }

        [Route("api/refreshCache")]
        [HttpGet]
        public IHttpActionResult RefreshCache()
        {
            _codeListService.LoadCodeListInCache();
            _municipalityCacheService.RefreshMunicipalityCache();

            return Ok();
        }
    }
}