﻿using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Models.ApiModels;
using KommIT.FIKS.AdapterAltinnSvarUt.Services;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Authentication;
using Serilog;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;

namespace KommIT.FIKS.AdapterAltinnSvarUt.ControllersApi
{
    [BasicAuthentication]
    [EnableCors(origins: "*", headers: "*", methods: "*", exposedHeaders: "x-correlation-id")]
    public class MangelbrevStatusAPIController : ApiController
    {
        private readonly ILogger _logger;
        private readonly IDeviationLetterService _deviationLetterService;

        public MangelbrevStatusAPIController(ILogger logger, IDeviationLetterService deviationLetterService)
        {
            _logger = logger;
            _deviationLetterService = deviationLetterService;
        }

        /// <summary>
        /// Hent ut mangelbrev på en gitt arkivreferanse
        /// </summary>
        /// <param name="archiveReference">Arkivreferanse i Altinn (eks AR12345)</param>
        /// <returns></returns>
        /// <response code="200">ok</response>
        /// <response code="400">BadRequest - ugyldig forespørsel</response>
        /// <response code="500">Error - intern feil</response>
        [Route("api/mangelbrev/arkivreferanse/{archiveReference}")]
        [HttpGet]
        [ResponseType(typeof(Mangelbrev[]))]
        public HttpResponseMessage GetMangelbrevByArchiveReference(string archiveReference)
        {
            if (archiveReference == null) return Request.CreateResponse(HttpStatusCode.BadRequest);

            try
            {
                List<DeviationLetter> deviationLetters = _deviationLetterService.GetListOfDeviationLettersByArchiveReference(archiveReference);
                List<Mangelbrev> mangelbrev = Mangelbrev.MappingToList(deviationLetters);

                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, mangelbrev);
                return response;
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception occurred when requesting mangelbrev for {ArchiveReference}", archiveReference);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        /// <summary>
        /// Hent ut mangelbrev på et gitt saksnummer - saksaar/sakssseksvensnummer
        /// </summary>
        /// <param name="kommunenr">Kommunenummer</param>
        /// <param name="saksaar">Kommunens saksår (første del i saksnummer eks 2018/xxxxxx)</param>
        /// <param name="sakssekvensnummer">Kommunens sakssekvensnummer (andre del i saksnummer eks xxxx/123456)</param>
        /// <returns></returns>
        [Route("api/mangelbrev/saksnummer/{kommunenr}/{saksaar}/{sakssekvensnummer}")]
        [HttpGet]
        [ResponseType(typeof(Mangelbrev[]))]
        public HttpResponseMessage GetMangelbrevBySaksnummer(string kommunenr, int saksaar, long sakssekvensnummer)
        {
            try
            {
                List<DeviationLetter> deviationLetters = _deviationLetterService.GetListOfDeviationLettersByCaseYearAndSequence(kommunenr, saksaar, sakssekvensnummer);
                List<Mangelbrev> mangelbrev = Mangelbrev.MappingToList(deviationLetters);

                return Request.CreateResponse(HttpStatusCode.OK, mangelbrev);
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception occurred when requesting mangelbrev for {Kommunenr} {Saksaar} {Sakssekvensnummer}", kommunenr, saksaar, sakssekvensnummer);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        /// <summary>
        /// Hent ut et mangelbrev på et gitt journalnummer - jordalsekvensnummer/journalaar
        /// </summary>
        /// <param name="kommunenr">Kommunenr</param>
        /// <param name="journalsekvensnummer">Kommunens journal sekvensnummer (første del i saksnummer eks 123456/xxxx)</param>
        /// <param name="journalaar">Kommunens journal saksår (siste del i saksnummer eks xxxxxx/2018</param>
        /// <returns></returns>
        [Route("api/mangelbrev/journalnummer/{kommunenr}/{journalsekvensnummer}/{journalaar}")]
        [HttpGet]
        [ResponseType(typeof(Mangelbrev[]))]
        public HttpResponseMessage GetMangelbrevByJournalnummer(string kommunenr, long journalsekvensnummer, int journalaar)
        {
            try
            {
                List<DeviationLetter> deviationLetters = _deviationLetterService.GetListOfDeviationLettersByArchiveYearAndSequence(kommunenr, journalsekvensnummer, journalaar);
                List<Mangelbrev> mangelbrev = Mangelbrev.MappingToList(deviationLetters);

                return Request.CreateResponse(HttpStatusCode.OK, mangelbrev);
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception occurred when requesting mangelbrev for {Kommunenr} {Journalsekvensnummer} {Journalaar}", kommunenr, journalsekvensnummer, journalaar);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }
    }
}