﻿using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace KommIT.FIKS.AdapterAltinnSvarUt.ControllersApi.StatusV2
{
    public static class ServiceCodeHelper
    {
        public const string Shipping = "ForsendelseTilKommune";
        public const string Distribution = "Distribusjon";
        public const string Notification = "Notifikasjon";

        private static string GetInnsendingsType(string serviceCode)
        {
            if (string.IsNullOrWhiteSpace(serviceCode))
                return string.Empty;

            var serviceCodeShippingConfigValue = ConfigurationManager.AppSettings["StatusAPI:ForsendelseTilKommune:ServiceCodes"];
            var serviceCodeDistributionConfigValue = ConfigurationManager.AppSettings["StatusAPI:Distribusjon:ServiceCodes"];
            var serviceCodeNotificationConfigValue = ConfigurationManager.AppSettings["StatusAPI:Notifikasjon:ServiceCodes"];

            var tl = new List<Tuple<string, string>>();
            tl.AddRange(serviceCodeShippingConfigValue.Split(',').Select(s => new Tuple<string, string>(s, Shipping)));
            tl.AddRange(serviceCodeDistributionConfigValue.Split(',').Select(s => new Tuple<string, string>(s, Distribution)));
            tl.AddRange(serviceCodeNotificationConfigValue.Split(',').Select(s => new Tuple<string, string>(s, Notification)));

            var innsendingsType = tl.FirstOrDefault(t => t.Item1.Trim().Equals(serviceCode.Trim()));

            return innsendingsType == null ? string.Empty : innsendingsType.Item2;
        }

        public static string GetInnsendingsType(FormMetadata formMetadata)
        {
            var innsendingstype = GetInnsendingsType(formMetadata.ServiceCode);

            if (string.IsNullOrEmpty(innsendingstype))
            {
                if (formMetadata.SenderSystem.Equals("ANSAKO", StringComparison.InvariantCultureIgnoreCase))
                    innsendingstype = Distribution;
            }

            return innsendingstype;
        }
    }
}