﻿using KommIT.FIKS.AdapterAltinnSvarUt.ControllersApi.StatusV2.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Models;

namespace KommIT.FIKS.AdapterAltinnSvarUt.ControllersApi.StatusV2
{
    public class DistributionFormsRepository : StatusRepositoryBase<DistribusjonsdetaljerRepresentation>
    {
        public DistributionFormsRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }
    }
}