﻿using KommIT.FIKS.AdapterAltinnSvarUt.ControllersApi.StatusV2.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Models;

namespace KommIT.FIKS.AdapterAltinnSvarUt.ControllersApi.StatusV2
{
    public abstract class StatusRepositoryBase<TEntity> where TEntity : class
    {
        protected readonly ApplicationDbContext dbContext;

        public StatusRepositoryBase(ApplicationDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public PagedResult<TEntity> Find(IPagedQuery<TEntity> query, int pageNumber, int itemsPerPage)
        {
            return query.Execute(dbContext, (pageNumber - 1) * itemsPerPage, itemsPerPage);
        }
    }
}