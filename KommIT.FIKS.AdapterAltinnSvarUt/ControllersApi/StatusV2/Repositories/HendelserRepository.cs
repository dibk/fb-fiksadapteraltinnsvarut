﻿using KommIT.FIKS.AdapterAltinnSvarUt.ControllersApi.StatusV2.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Models;

namespace KommIT.FIKS.AdapterAltinnSvarUt.ControllersApi.StatusV2
{
    public class HendelserRepository : StatusRepositoryBase<InnsendingHendelse>
    {
        public HendelserRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }
    }
}