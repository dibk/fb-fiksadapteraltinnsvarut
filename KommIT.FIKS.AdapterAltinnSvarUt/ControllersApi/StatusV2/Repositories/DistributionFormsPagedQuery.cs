﻿using KommIT.FIKS.AdapterAltinnSvarUt.ControllersApi.StatusV2.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.FileDownload;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace KommIT.FIKS.AdapterAltinnSvarUt.ControllersApi.StatusV2
{
    public class DistributionFormsPagedQuery : IPagedQuery<DistribusjonsdetaljerRepresentation>
    {
        private readonly Expression<Func<DistributionForm, bool>> where;

        public DistributionFormsPagedQuery(Expression<Func<DistributionForm, bool>> where = null)
        {
            this.where = where ?? (b => true);
        }

        public PagedResult<DistribusjonsdetaljerRepresentation> Execute(ApplicationDbContext dbContext, int skip, int take)
        {
            var distrs = dbContext.DistributionForms
                            .Where(where)
                            .OrderBy(d => d.SubmitAndInstantiatePrefilled)
                            .Skip(skip)
                            .Take(take).ToList();

            var distrIds = distrs.Select(d => d.Id);
            var postDistrMetadata = dbContext.PostDistributionMetaData.Where(p => distrIds.Contains(p.Id)).ToList();

            var signedArchiveReferences = new List<string>();
            signedArchiveReferences.AddRange(distrs.Where(d => !string.IsNullOrEmpty(d.SignedArchiveReference)).Select(d => d.SignedArchiveReference).ToList());
            var fileDownloadStatuses = dbContext.FileDownloadStatus.Where(f => signedArchiveReferences.Contains(f.ArchiveReference)).ToList();

            var distrReps = distrs
                .Select(d => new DistribusjonsdetaljerRepresentation
                {
                    Id = d.Id.ToString(),
                    Referanse = d.InitialArchiveReference,
                    Distribusjonstype = d.DistributionType,
                    ReferanseSluttbrukersystem = d.ExternalSystemReference,
                    Status = d.DistributionStatus.ToString(),
                    ReferanseIdSendt = d.SubmitAndInstantiatePrefilledFormTaskReceiptId,
                    Feilmelding = Utils.TextObfuscation.ObfuscateSSN(d.ErrorMessage),
                    Sendt = d.SubmitAndInstantiatePrefilled,
                    ReferanseIdSignert = d.SignedArchiveReference,
                    Signert = d.Signed,
                    ReferanseIdKvittering = d.RecieptSentArchiveReference,
                    SendtKvittering = d.RecieptSent,
                    Filnedlastinger = GetFileDownloads(fileDownloadStatuses, d),
                    ReferanseOpprinneligDistribusjonSluttbrukersystem = d.InitialExternalSystemReference,
                    AltinnInnboksMeldingsreferanse = postDistrMetadata.FirstOrDefault(p => p.Id.Equals(d.Id))?.ReferenceId,
                    AnSaKoStatus = d.AnSaKoStatus != null ? new AnSaKoStatusRepresentation()
                    {
                        Signeringsfrist = d.AnSaKoStatus.SigningDeadline,
                        Status = d.AnSaKoStatus.AnSaKoProcessStatus,
                        StatusDetaljer = d.AnSaKoStatus.StatusDetails
                    } : null
                }
            ).ToList();

            var count = dbContext.DistributionForms.Where(where).Count();

            return new PagedResult<DistribusjonsdetaljerRepresentation>(distrReps, count, skip, take);
        }

        private List<FileDownloads> GetFileDownloads(List<FileDownloadStatus> fileDownloadStatuses, DistributionForm distributionForm)
        {
            List<FileDownloads> filnedlastinger = null;
            if (distributionForm.DistributionStatus == DistributionStatus.receiptSent || distributionForm.DistributionStatus == DistributionStatus.signed)
            {
                var fileDownloadsForSignedArchiveReference = fileDownloadStatuses.Where(f => f.ArchiveReference.Equals(distributionForm.SignedArchiveReference)).ToList();

                if (fileDownloadsForSignedArchiveReference != null)
                {
                    filnedlastinger = new List<FileDownloads>();

                    foreach (var fileDownload in fileDownloadsForSignedArchiveReference)
                    {
                        filnedlastinger.Add(new FileDownloads()
                        {
                            MimeType = fileDownload.MimeType,
                            Vedleggstype = fileDownload.FileType.ToString(),
                            Beskrivelse = fileDownload.FormName,
                            FilUrl = FileDownloadApiUrlProvider.GenerateDownloadApiUri(fileDownload)
                        });
                    }
                }
            }
            return filnedlastinger;
        }
    }
}