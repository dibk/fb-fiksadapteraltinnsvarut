﻿using KommIT.FIKS.AdapterAltinnSvarUt.ControllersApi.StatusV2.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Models;

namespace KommIT.FIKS.AdapterAltinnSvarUt.ControllersApi.StatusV2
{
    public interface IPagedQuery<T>
    {
        PagedResult<T> Execute(ApplicationDbContext dbContext, int skip, int take);
    }
}