﻿using KommIT.FIKS.AdapterAltinnSvarUt.Services;

namespace KommIT.FIKS.AdapterAltinnSvarUt.ControllersApi.StatusV2
{
    public class DistributionVorpaSummaryRepository : DistributionSummaryRepositoryBase
    {
        public DistributionVorpaSummaryRepository(IFormMetadataService formMetadataService) : base(formMetadataService)
        {
        }

        public override int GetTotalDistributions(string reference)
        {
            var tableStorage = new Services.Azure.TableStorage<Services.Azure.DistributionSubmittalEntity>();
            var distributionTableEntity = tableStorage.Get(reference, reference);

            return distributionTableEntity != null ? distributionTableEntity.ReceiverCount : 0;
        }
    }
}