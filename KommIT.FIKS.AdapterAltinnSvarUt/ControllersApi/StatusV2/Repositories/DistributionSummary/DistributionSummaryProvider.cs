﻿using KommIT.FIKS.AdapterAltinnSvarUt.Services;

namespace KommIT.FIKS.AdapterAltinnSvarUt.ControllersApi.StatusV2
{
    public class DistributionSummaryProvider
    {
        private readonly IFormMetadataService _formMetadataService;

        public DistributionSummaryProvider(IFormMetadataService formMetadataService)
        {
            _formMetadataService = formMetadataService;
        }

        public DistributionSummaryRepositoryBase GetDistributionSummaryFor(string serviceCode)
        {
            if (!string.IsNullOrEmpty(serviceCode) && serviceCode.Equals("5418"))
                return new DistributionVorpaSummaryRepository(_formMetadataService);
            else
                return new DistributionSummaryRepositoryBase(_formMetadataService);
        }
    }
}