﻿using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Services;
using System.Collections.Generic;

namespace KommIT.FIKS.AdapterAltinnSvarUt.ControllersApi.StatusV2
{
    public class DistributionSummaryRepositoryBase
    {
        private readonly IFormMetadataService _formMetadataService;

        public DistributionSummaryRepositoryBase(IFormMetadataService formMetadataService)
        {
            _formMetadataService = formMetadataService;
        }

        public virtual int GetOkDistributions(string reference)
        {
            return _formMetadataService.CountDistributionFormsByStatus(reference, new List<DistributionStatus>() { DistributionStatus.submittedPrefilled, DistributionStatus.signed, DistributionStatus.receiptSent });
        }

        public virtual int GetFailedDistributions(string reference)
        {
            return _formMetadataService.CountDistributionFormsByStatus(reference, new List<DistributionStatus>() { DistributionStatus.error });
        }

        public virtual int GetTotalDistributions(string reference)
        {
            return _formMetadataService.CountUniqueDistributions(reference);
        }
    }
}