﻿using KommIT.FIKS.AdapterAltinnSvarUt.ControllersApi.StatusV2.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace KommIT.FIKS.AdapterAltinnSvarUt.ControllersApi.StatusV2
{
    public class LogEntriesQuery : IPagedQuery<InnsendingHendelse>
    {
        private readonly Expression<Func<LogEntry, bool>> where;

        public LogEntriesQuery(Expression<Func<LogEntry, bool>> where = null)
        {
            this.where = where ?? (b => true);
        }

        public PagedResult<InnsendingHendelse> Execute(ApplicationDbContext dbContext, int skip, int take)
        {
            var logEntries = dbContext.LogEntries
                            .Where(where)
                            .OrderBy(d => d.Id)
                            .Skip(skip)
                            .Take(take).ToList();

            var hendelser = logEntries.Select(d => new InnsendingHendelse
            {
                Melding = TextObfuscation.ObfuscateSSN(d.Message),
                Tidspunkt = d.Timestamp,
                VisMerURL = d.Url
            }
                          ).ToList();
            var count = dbContext.LogEntries.Where(where).Count();

            return new PagedResult<InnsendingHendelse>(hendelser, count, skip, take);
        }
    }
}