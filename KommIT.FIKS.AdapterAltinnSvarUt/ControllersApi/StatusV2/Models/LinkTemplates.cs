﻿using WebApi.Hal;

namespace KommIT.FIKS.AdapterAltinnSvarUt.ControllersApi.StatusV2.Models
{
    public static class LinkTemplates
    {
        public static class Innsendinger
        {
            public static Link Innsending
            { get { return new Link("innsending", "~/api/v1/innsending/{referanse}"); } }
            public static Link InnsendingVedlegg
            { get { return new Link("vedlegg", "~/api/v1/innsending/{referanse}/vedlegg"); } }
            public static Link Distribusjoner
            { get { return new Link("distribusjon", "~/api/v1/innsending/distribusjoner/{referanse}"); } }
            public static Link DistribusjonsDetaljer
            { get { return new Link("distribusjoner", "~/api/v1/innsending/distribusjoner/{referanse}/distribusjonsdetaljer{?page}"); } }
            public static Link Distribusjon
            { get { return new Link("distribusjoner", "~/api/v1/innsending/distribusjoner/{referanse}/distribusjonsdetaljer/{id}"); } }
            public static Link DistribusjonerFeilet
            { get { return new Link("distribusjonerfeilet", "~/api/v1/innsending/distribusjoner/{referanse}/distribusjonsdetaljer?status=error"); } }
            public static Link DistribusjonSignertSvar
            { get { return new Link("signertsvar", "~/api/v1/innsending/{referanse}"); } }
            public static Link Forsendelse
            { get { return new Link("forsendelse", "~/api/v1/innsending/forsendelser/{referanse}"); } }
            public static Link InnsendingHendelser
            { get { return new Link("hendelser", "~/api/v1/innsending/{referanse}/hendelser{?page}"); } }
        }
    }
}