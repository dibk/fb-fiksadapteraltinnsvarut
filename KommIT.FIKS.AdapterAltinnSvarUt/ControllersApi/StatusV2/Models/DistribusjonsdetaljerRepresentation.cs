﻿using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using WebApi.Hal;

namespace KommIT.FIKS.AdapterAltinnSvarUt.ControllersApi.StatusV2.Models
{
    public class DistribusjonsdetaljerRepresentation : Representation
    {
        public override string Href
        {
            get { return LinkTemplates.Innsendinger.Distribusjon.CreateLink(new { id = Id, referanse = Referanse }).Href; }
            set { }
        }

        public override string Rel
        {
            get { return LinkTemplates.Innsendinger.Distribusjon.Rel; }
            set { }
        }

        protected override void CreateHypermedia()
        {
            if (!string.IsNullOrEmpty(ReferanseIdSignert))
                Links.Add(LinkTemplates.Innsendinger.DistribusjonSignertSvar.CreateLink(new { referanse = ReferanseIdSignert }));
        }

        [JsonIgnore]
        public string Referanse { get; set; }

        [JsonIgnore]
        public string Id { get; set; }

        /// <summary>
        /// Type distribusjon
        /// </summary>
        public string Distribusjonstype { get; set; }

        /// <summary>
        /// Referansen til søknadssystemet/sluttbukersystem på forsendelse. Angitt som VaarReferanse i datamodeller for ansvarsrett og nabovarsel
        /// </summary>
        public string ReferanseSluttbrukersystem { get; set; }

        /// <summary>
        /// Feilet, Sendt/varslet, Signert, Kvittering sendt
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// Feks feil i orgnr/fødselsnr, reservert mot digital forsendelse
        /// </summary>
        public string Feilmelding { get; set; }

        /// <summary>
        /// kvitteringsid på sendt skjema - (Altinn SubmitAndInstantiatePrefilledForm ReceiptExternal.ReceiptId)
        /// </summary>
        public string ReferanseIdSendt { get; set; }

        /// <summary>
        /// Tidspunkt for når skjema er lagt i mottakers meldingsboks (ansvarlig foretak/nabo)
        /// </summary>
        public DateTime? Sendt { get; set; }

        /// <summary>
        /// Id på signert/mottatt preutfylt svarskjema
        /// </summary>
        public string ReferanseIdSignert { get; set; }

        /// <summary>
        /// Tidspunkt for når svar skjema er signert (ansvarlig foretak/nabo)
        /// </summary>
        public DateTime? Signert { get; set; }

        /// <summary>
        /// Id på kvittering som er sendt til opprinnelig avsender (ansvarlig søker)
        /// Altinn arkivreferanse/meldingsid
        /// Denne ID skal kunne brukes for å hente ut melding fra meldingsboks med MessageID=ReferanseIdKvittering
        /// </summary>
        public string ReferanseIdKvittering { get; set; }

        /// <summary>
        /// Tidspunkt for når kvittering er sendt til opprinnelig avsender (ansvarlig søker)
        /// </summary>
        public DateTime? SendtKvittering { get; set; }

        /// <summary>
        /// Liste av filer tilgjenglig for nedlasting
        /// </summary>
        public List<FileDownloads> Filnedlastinger { get; set; }

        /// <summary>
        /// Referansen til opprinnelig hovedinnsendingsnr fra sluttbrukersystem på distribusjonen.
        /// </summary>
        public string ReferanseOpprinneligDistribusjonSluttbrukersystem { get; set; }

        /// <summary>
        /// Referanse id til det distribuerte dokumentet. Kan typisk brukes til å generere en dyplenke.
        /// </summary>
        public string AltinnInnboksMeldingsreferanse { get; set; }

        public AnSaKoStatusRepresentation AnSaKoStatus { get; set; }
    }

    public class AnSaKoStatusRepresentation
    {
        [JsonConverter(typeof(StringEnumConverter))]
        public AnSaKoProcessStatusType Status { get; set; }

        public string StatusDetaljer { get; set; }
        public DateTime? Signeringsfrist { get; set; }
    }
}