﻿using Newtonsoft.Json;
using System.Collections.Generic;
using WebApi.Hal;

namespace KommIT.FIKS.AdapterAltinnSvarUt.ControllersApi.StatusV2.Models
{
    public class DistribusjonerRepresentation : Representation
    {
        [JsonIgnore]
        public string Referanse { get; set; }

        public int? AntallDistribusjoner { get; set; }
        public int? AntallDistribusjonerOk { get; set; }
        public int? AntallDistribusjonerFeilet { get; set; }
        public List<FileDownloads> Filnedlastinger { get; set; }

        public DistribusjonerRepresentation(string reference)
        {
            Referanse = reference;
            Filnedlastinger = new List<FileDownloads>();
        }

        public override string Href
        {
            get { return LinkTemplates.Innsendinger.Distribusjoner.CreateLink(new { referanse = Referanse }).Href; }
            set { }
        }

        public override string Rel
        {
            get { return LinkTemplates.Innsendinger.Distribusjoner.Rel; }
            set { }
        }

        protected override void CreateHypermedia()
        {
            //base.CreateHypermedia();
            //if(AntallDistribusjoner.HasValue && AntallDistribusjoner > 0) //As long as only one service counts total distributions, we cannot have this check..
            Links.Add(LinkTemplates.Innsendinger.DistribusjonsDetaljer.CreateLink(new { referanse = Referanse }));

            if (AntallDistribusjonerFeilet.HasValue && AntallDistribusjonerFeilet > 0)
                Links.Add(LinkTemplates.Innsendinger.DistribusjonerFeilet.CreateLink(new { referanse = Referanse }));
        }
    }
}