﻿namespace KommIT.FIKS.AdapterAltinnSvarUt.ControllersApi.StatusV2.Models
{
    public class FileDownloads
    {
        public string Vedleggstype { get; set; }
        public string Beskrivelse { get; set; }
        public string VariantFormat { get; set; }
        public string FilUrl { get; set; }
        public string MimeType { get; set; }
    }
}