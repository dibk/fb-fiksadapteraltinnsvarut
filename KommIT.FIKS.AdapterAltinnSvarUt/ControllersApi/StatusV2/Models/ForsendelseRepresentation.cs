﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using WebApi.Hal;

namespace KommIT.FIKS.AdapterAltinnSvarUt.ControllersApi.StatusV2.Models
{
    public class ForsendelseRepresentation : Representation
    {
        [JsonIgnore]
        public string Referanse { get; set; }

        public string Saksaar { get; set; }
        public string Sakssekvensnummer { get; set; }
        public string OffentligJournalUrl { get; set; }
        public string SvarUtReferanseId { get; set; }
        public DateTime? SvarUtForsendelseMottatt { get; set; }
        public string SvarUtForsendelseStatus { get; set; }
        public List<FileDownloads> Filnedlastinger { get; set; }

        public ForsendelseRepresentation(string referanse)
        {
            Referanse = referanse;
            Filnedlastinger = null;
        }

        public override string Href
        {
            get { return LinkTemplates.Innsendinger.Forsendelse.CreateLink(new { referanse = Referanse }).Href; }
            set { }
        }

        public override string Rel
        {
            get { return LinkTemplates.Innsendinger.Forsendelse.Rel; }
            set { }
        }
    }
}