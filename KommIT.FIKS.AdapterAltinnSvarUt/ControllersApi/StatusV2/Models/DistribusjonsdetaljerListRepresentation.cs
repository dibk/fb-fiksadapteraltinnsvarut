﻿using System.Collections.Generic;
using WebApi.Hal;

namespace KommIT.FIKS.AdapterAltinnSvarUt.ControllersApi.StatusV2.Models
{
    public class DistribusjonsdetaljerListRepresentation : PagedRepresentationList<DistribusjonsdetaljerRepresentation>
    {
        public DistribusjonsdetaljerListRepresentation(IList<DistribusjonsdetaljerRepresentation> res, int totalResults, int totalPages, int page, Link uriTemplate, object uriTemplateSubstitutionParams)
            : base(res, totalResults, totalPages, page, uriTemplate, uriTemplateSubstitutionParams) { }
    }
}