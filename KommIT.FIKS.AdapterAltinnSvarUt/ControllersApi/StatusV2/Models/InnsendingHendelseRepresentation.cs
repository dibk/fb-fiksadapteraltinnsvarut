﻿using System;
using System.Collections.Generic;
using WebApi.Hal;

namespace KommIT.FIKS.AdapterAltinnSvarUt.ControllersApi.StatusV2.Models
{
    public class InnsendingHendelseListRepresentation : SimplePagedRepresentationList<InnsendingHendelse>
    {
        public IList<InnsendingHendelse> Hendelser { get; set; }

        public InnsendingHendelseListRepresentation(IList<InnsendingHendelse> res, int totalResults, int totalPages, int page, Link uriTemplate, object uriTemplateSubstitutionParams)
            : base(totalResults, totalPages, page, uriTemplate, uriTemplateSubstitutionParams)
        {
            Hendelser = res;
        }
    }

    public class InnsendingHendelse
    {
        /// <summary>
        /// Beskrivelse av hendelsen
        /// </summary>
        public string Melding { get; set; }

        /// <summary>
        /// Tidspunkt for hendelsen
        /// </summary>
        public DateTime Tidspunkt { get; set; }

        public string VisMerURL { get; set; }
    }
}