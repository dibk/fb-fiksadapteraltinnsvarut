﻿using Newtonsoft.Json;
using System.Collections.Generic;
using WebApi.Hal;

namespace KommIT.FIKS.AdapterAltinnSvarUt.ControllersApi.StatusV2.Models
{
    public class Vedlegg
    {
        public string Referanse { get; set; }
        public string Filnavn { get; set; }
        public int Stoerrelse { get; set; }
        public string Vedleggstype { get; set; }
    }

    public class InnsendingsVedleggRepresentation : Representation
    {
        [JsonIgnore]
        public string Referanse { get; set; }

        public List<Vedlegg> InnsendingsVedlegg { get; set; }

        public override string Href
        {
            get { return LinkTemplates.Innsendinger.InnsendingVedlegg.CreateLink(new { referanse = Referanse }).Href; }
            set { }
        }

        public override string Rel
        {
            get { return LinkTemplates.Innsendinger.InnsendingVedlegg.Rel; }
            set { }
        }
    }
}