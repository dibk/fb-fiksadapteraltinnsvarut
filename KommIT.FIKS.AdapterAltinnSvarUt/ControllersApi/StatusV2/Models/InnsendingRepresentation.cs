﻿using System;
using WebApi.Hal;

namespace KommIT.FIKS.AdapterAltinnSvarUt.ControllersApi.StatusV2.Models
{
    public class InnsendingRepresentation : Representation
    {
        public string Referanse { get; set; }
        public string InnsendingsType { get; set; }
        public DateTime? AltinnInnsending { get; set; }
        public string Status { get; set; }
        public int? AntallVedlegg { get; set; }
        public DateTime? Slettet { get; set; }

        public override string Href
        {
            get { return LinkTemplates.Innsendinger.Innsending.CreateLink(new { referanse = Referanse }).Href; }
            set { }
        }

        public override string Rel
        {
            get { return LinkTemplates.Innsendinger.Innsending.Rel; }
            set { }
        }

        protected override void CreateHypermedia()
        {
            Links.Add(LinkTemplates.Innsendinger.InnsendingHendelser.CreateLink(new { referanse = Referanse }));

            if (AntallVedlegg.HasValue && AntallVedlegg > 0)
                Links.Add(LinkTemplates.Innsendinger.InnsendingVedlegg.CreateLink(new { referanse = Referanse }));

            if (InnsendingsType.Equals(ServiceCodeHelper.Distribution))
                Links.Add(LinkTemplates.Innsendinger.Distribusjoner.CreateLink(new { referanse = Referanse }));
            else if (InnsendingsType.Equals(ServiceCodeHelper.Shipping))
                Links.Add(LinkTemplates.Innsendinger.Forsendelse.CreateLink(new { referanse = Referanse }));
        }
    }
}