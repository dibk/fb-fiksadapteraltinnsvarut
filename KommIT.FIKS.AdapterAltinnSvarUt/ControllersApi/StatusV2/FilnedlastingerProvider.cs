﻿using KommIT.FIKS.AdapterAltinnSvarUt.ControllersApi.StatusV2.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Services;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.FileDownload;
using System.Collections.Generic;

namespace KommIT.FIKS.AdapterAltinnSvarUt.ControllersApi.StatusV2
{
    public class FilnedlastingerProvider
    {
        private readonly IFileDownloadStatusService _filedownloadStatusService;

        public FilnedlastingerProvider(IFileDownloadStatusService filedownloadStatusService)
        {
            _filedownloadStatusService = filedownloadStatusService;
        }

        public List<FileDownloads> GetFiledownloads(string archiveReference)
        {
            var filedownloads = _filedownloadStatusService.GetFileDownloadStatusesForArchiveReference(archiveReference);
            List<FileDownloads> filnedlastinger = null;
            if (filedownloads?.Count > 0)
            {
                filnedlastinger = new List<FileDownloads>();
                foreach (var fileDownload in filedownloads)
                {
                    var filnedlasting = new FileDownloads()
                    {
                        MimeType = fileDownload.MimeType,
                        Vedleggstype = fileDownload.FileType.ToString(),
                        FilUrl = FileDownloadApiUrlProvider.GenerateDownloadApiUri(fileDownload)
                    };

                    filnedlastinger.Add(filnedlasting);
                }
            }
            return filnedlastinger;
        }
    }
}