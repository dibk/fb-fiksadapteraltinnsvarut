using KommIT.FIKS.AdapterAltinnSvarUt.ControllersApi.StatusV2.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Services;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Authentication;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;

namespace KommIT.FIKS.AdapterAltinnSvarUt.ControllersApi.StatusV2
{
    /// <summary>
    /// Startpunktet i APIet for status på innsendinger til Fellestjenester Plan og Bygg
    /// </summary>
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [BasicAuthentication]
    [Swagger.SwaggerResponseContentType(responseType: "application/hal+json", Exclusive = true)]
    public class InnsendingController : ApiController
    {
        private readonly IFormMetadataService _formMetadataService;
        private readonly IAuthorizationService _authorizationService;

        public InnsendingController(IFormMetadataService formMetadataService,
                                    IAuthorizationService authorizationService)
        {
            _formMetadataService = formMetadataService;
            _authorizationService = authorizationService;
        }

        public class Context
        {
            public string Arkivreferanse { get; set; }
            public FormMetadata FormMetadata { get; set; }
            public List<FormAttachmentMetadata> Attachments { get; set; }
            public IHttpActionResult Reason { get; set; }
        }

        private bool ValidateInput(string arkivreferanse, out Context context)
        {
            context = new Context();
            if (string.IsNullOrWhiteSpace(arkivreferanse))
            {
                context.Reason = BadRequest("arkivreferanse må ha verdi");
                return false;
            }
            context.Arkivreferanse = arkivreferanse.Trim().ToUpper();
            context.FormMetadata = _formMetadataService.GetFormMetadata(context.Arkivreferanse);

            if (context.FormMetadata == null)
            {
                context.Reason = NotFound();
                return false;
            }

            if (!_authorizationService.UserHasAccess(context.FormMetadata))
            {
                context.Reason = Unauthorized();
                return false;
            }

            context.Attachments = context.FormMetadata.FormAttachmentMetadatas;

            return true;
        }

        /// <summary>
        /// Henter ut status for en innsending
        /// </summary>
        /// <param name="arkivreferanse">arkivreferanse i Altinn (eks AR12345)</param>
        /// <returns></returns>
        /// <response code="200">Ok</response>
        /// <response code="401">Unauthorized - Ingen tilgang</response>
        /// <response code="404">NotFound - finner ikke innsending med gitt arkivreferanse. NB det kan ta flere minutter fra innsending til arkivreferanse er tilgjengelig i fellestjenester bygg</response>
        /// <response code="500">Error - intern feil</response>
        [Swashbuckle.Swagger.Annotations.SwaggerOperation(Tags = new[] { "Status innsending" })]
        [ResponseType(typeof(InnsendingRepresentation))]
        [Route("api/v1/innsending/{arkivreferanse}")]
        [HttpGet]
        public IHttpActionResult Get(string arkivreferanse = null)
        {
            Context context;
            if (!ValidateInput(arkivreferanse, out context))
            {
                return context.Reason;
            }

            var innsending = new InnsendingRepresentation()
            {
                AltinnInnsending = context.FormMetadata.ArchiveTimestamp,
                AntallVedlegg = context.FormMetadata.FormAttachmentMetadatas?.Count,
                InnsendingsType = ServiceCodeHelper.GetInnsendingsType(context.FormMetadata),
                Referanse = context.Arkivreferanse,
                Status = context.FormMetadata.Status,
                Slettet = context.FormMetadata.Slettet,
            };
            return Ok(innsending);
        }

        /// <summary>
        /// Henter liste med info om vedlegg som var med i innsendingen
        /// </summary>
        /// <param name="arkivreferanse">arkivreferanse i Altinn (eks AR12345)</param>
        /// <returns></returns>
        /// <response code="200">Ok</response>
        /// <response code="401">Unauthorized - Ingen tilgang</response>
        /// <response code="404">NotFound - finner ikke innsending med gitt arkivreferanse. NB det kan ta flere minutter fra innsending til arkivreferanse er tilgjengelig i fellestjenester bygg</response>
        /// <response code="500">Error - intern feil</response>
        [Swashbuckle.Swagger.Annotations.SwaggerOperation(Tags = new[] { "Status innsending" })]
        [ResponseType(typeof(InnsendingsVedleggRepresentation))]
        [Route("api/v1/innsending/{arkivreferanse}/vedlegg")]
        [HttpGet]
        public IHttpActionResult GetVedlegg(string arkivreferanse = null)
        {
            Context context;
            if (!ValidateInput(arkivreferanse, out context))
            {
                return context.Reason;
            }
            if (!context.Attachments.Any())
            {
                return NotFound();
            }

            var vedlegg = new List<Vedlegg>();

            foreach (var entry in context.Attachments)
            {
                vedlegg.Add(new Vedlegg()
                {
                    Filnavn = entry.FileName,
                    Stoerrelse = entry.Size,
                    Vedleggstype = entry.AttachmentType
                });
            }

            var vedleggRepresentation = new InnsendingsVedleggRepresentation()
            {
                Referanse = arkivreferanse,
                InnsendingsVedlegg = vedlegg
            };

            return Ok(vedleggRepresentation);
        }
    }
}