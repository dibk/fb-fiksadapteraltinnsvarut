using KommIT.FIKS.AdapterAltinnSvarUt.ControllersApi.StatusV2.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Services;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Authentication;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.FileDownload;
using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;

namespace KommIT.FIKS.AdapterAltinnSvarUt.ControllersApi.StatusV2
{
    /// <summary>
    /// API for detaljert info på enkeltdistribusjoner
    /// </summary>
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [BasicAuthentication]
    [Swagger.SwaggerResponseContentType(responseType: "application/hal+json", Exclusive = true)]
    public class InnsendingDistribusjonDetaljerController : ApiController
    {
        private readonly IFormMetadataService _formMetadataService;
        private readonly IFileDownloadStatusService _fileDownloadStatusService;
        private readonly IAuthorizationService _authorizationService;

        public InnsendingDistribusjonDetaljerController(IFormMetadataService formMetadataService,
                                                        IFileDownloadStatusService fileDownloadStatusService,
                                                        IAuthorizationService authorizationService)
        {
            _formMetadataService = formMetadataService;
            _fileDownloadStatusService = fileDownloadStatusService;
            _authorizationService = authorizationService;
        }

        private bool ValidateInput(string arkivreferanse, string distributionFormId, out DistribusjonsDetaljerContext context)
        {
            context = new DistribusjonsDetaljerContext();
            if (string.IsNullOrWhiteSpace(arkivreferanse))
            {
                context.Reason = BadRequest("arkivreferanse må ha verdi");
                return false;
            }
            context.Arkivreferanse = arkivreferanse.Trim().ToUpper();
            context.FormMetadata = _formMetadataService.GetFormMetadata(context.Arkivreferanse);

            if (context.FormMetadata == null)
            {
                context.Reason = NotFound();
                return false;
            }

            if (!_authorizationService.UserHasAccess(context.FormMetadata))
            {
                context.Reason = Unauthorized();
                return false;
            }

            Guid parsedDistributionFormId;
            if (!Guid.TryParse(distributionFormId, out parsedDistributionFormId))
            {
                context.Reason = BadRequest("id må være en GUID");
                return false;
            }

            context.DistributionForm = _formMetadataService.GetDistributionFormByGuid(parsedDistributionFormId);

            if (context.DistributionForm == null)
            {
                context.Reason = NotFound();
                return false;
            }

            return true;
        }

        /// <summary>
        /// Henter ut detaljert info for en enkelt distribusjon
        /// </summary>
        /// <param name="arkivreferanse">arkivreferanse i Altinn (eks AR12345)</param>
        /// <param name="id">distribusjonensid</param>
        /// <returns></returns>
        /// <response code="200">Ok</response>
        /// <response code="400">BadRequest ved ugyldig distribusjonsid (GUID)</response>
        /// <response code="401">Unauthorized - Ingen tilgang</response>
        /// <response code="404">NotFound - finner ikke innsending med gitt arkivreferanse. NB det kan ta flere minutter fra innsending til arkivreferanse er tilgjengelig i fellestjenester bygg</response>
        /// <response code="500">Error - intern feil</response>
        [Swashbuckle.Swagger.Annotations.SwaggerOperation(Tags = new[] { "Status innsending" })]
        [ResponseType(typeof(DistribusjonsdetaljerRepresentation))]
        [Route("api/v1/innsending/distribusjoner/{arkivreferanse}/distribusjonsdetaljer/{id}")]
        [HttpGet]
        public IHttpActionResult GetDistribusjonsdetaljerById(string arkivreferanse, string id)
        {
            DistribusjonsDetaljerContext context;
            if (!ValidateInput(arkivreferanse, id, out context))
            {
                return context.Reason;
            }

            if (!ServiceCodeHelper.GetInnsendingsType(context.FormMetadata).Equals(ServiceCodeHelper.Distribution))
                return NotFound();

            var postDistrMetadata = _formMetadataService.GetPostDistributionMetaData(context.DistributionForm.Id);

            List<FileDownloads> filnedlastinger = null;
            if (context.DistributionForm.DistributionStatus == DistributionStatus.receiptSent || context.DistributionForm.DistributionStatus == DistributionStatus.signed)
            {
                filnedlastinger = new List<FileDownloads>();
                var fileDownloads = _fileDownloadStatusService.GetFileDownloadStatusesForArchiveReference(context.DistributionForm.SignedArchiveReference);
                foreach (var fileDownload in fileDownloads)
                {
                    filnedlastinger.Add(new FileDownloads()
                    {
                        MimeType = fileDownload.MimeType,
                        Vedleggstype = fileDownload.FileType.ToString(),
                        Beskrivelse = fileDownload.FormName,
                        FilUrl = FileDownloadApiUrlProvider.GenerateDownloadApiUri(fileDownload)
                    });
                }
            }

            var distrFormDetaljer = new DistribusjonsdetaljerRepresentation()
            {
                Id = context.DistributionForm.Id.ToString(),
                Referanse = context.DistributionForm.InitialArchiveReference,
                Distribusjonstype = context.DistributionForm.DistributionType,
                ReferanseSluttbrukersystem = context.DistributionForm.ExternalSystemReference,
                Status = context.DistributionForm.DistributionStatus.ToString(),
                ReferanseIdSendt = context.DistributionForm.SubmitAndInstantiatePrefilledFormTaskReceiptId,
                Feilmelding = Utils.TextObfuscation.ObfuscateSSN(context.DistributionForm.ErrorMessage),
                Sendt = context.DistributionForm.SubmitAndInstantiatePrefilled,
                ReferanseIdSignert = context.DistributionForm.SignedArchiveReference,
                Signert = context.DistributionForm.Signed,
                ReferanseIdKvittering = context.DistributionForm.RecieptSentArchiveReference,
                SendtKvittering = context.DistributionForm.RecieptSent,
                Filnedlastinger = filnedlastinger,
                ReferanseOpprinneligDistribusjonSluttbrukersystem = context.DistributionForm.InitialExternalSystemReference,
                AltinnInnboksMeldingsreferanse = postDistrMetadata?.ReferenceId
            };

            if (context.DistributionForm.AnSaKoStatus != null)
                distrFormDetaljer.AnSaKoStatus = new AnSaKoStatusRepresentation()
                {
                    Signeringsfrist = context.DistributionForm.AnSaKoStatus.SigningDeadline,
                    Status = context.DistributionForm.AnSaKoStatus.AnSaKoProcessStatus,
                    StatusDetaljer = context.DistributionForm.AnSaKoStatus.StatusDetails
                };

            return Ok(distrFormDetaljer);
        }

        private class DistribusjonsDetaljerContext
        {
            public string Arkivreferanse { get; set; }
            public DistributionForm DistributionForm { get; set; }
            public FormMetadata FormMetadata { get; set; }
            public IHttpActionResult Reason { get; set; }
        }
    }
}