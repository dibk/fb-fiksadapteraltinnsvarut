using KommIT.FIKS.AdapterAltinnSvarUt.ControllersApi.StatusV2.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Services;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Authentication;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;

namespace KommIT.FIKS.AdapterAltinnSvarUt.ControllersApi.StatusV2
{
    /// <summary>
    /// API for informasjon om status på forsendelser
    /// </summary>
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [BasicAuthentication]
    [Swagger.SwaggerResponseContentType(responseType: "application/hal+json", Exclusive = true)]
    public class InnsendingForsendelseController : ApiController
    {
        private readonly IFormMetadataService _formMetadataService;
        private readonly IFileDownloadStatusService _fileDownloadStatusService;
        private readonly IAuthorizationService _authorizationService;

        public InnsendingForsendelseController(IFormMetadataService formMetadataService,
                                               IFileDownloadStatusService fileDownloadStatusService,
                                               IAuthorizationService authorizationService)
        {
            _formMetadataService = formMetadataService;
            _fileDownloadStatusService = fileDownloadStatusService;
            _authorizationService = authorizationService;
        }

        private bool ValidateInput(string arkivreferanse, out ForsendelseContext context)
        {
            context = new ForsendelseContext();
            if (string.IsNullOrWhiteSpace(arkivreferanse))
            {
                context.Reason = BadRequest("arkivreferanse må ha verdi");
                return false;
            }
            context.Arkivreferanse = arkivreferanse.Trim().ToUpper();
            context.FormMetadata = _formMetadataService.GetFormMetadata(context.Arkivreferanse);

            if (context.FormMetadata == null)
            {
                context.Reason = NotFound();
                return false;
            }

            if (!_authorizationService.UserHasAccess(context.FormMetadata))
            {
                context.Reason = Unauthorized();
                return false;
            }

            return true;
        }

        /// <summary>
        /// Henter ut status på en forsendelse
        /// </summary>
        /// <param name="arkivreferanse">arkivreferanse i Altinn (eks AR12345)</param>
        /// <returns></returns>
        /// <response code="200">Ok</response>
        /// <response code="401">Unauthorized - Ingen tilgang</response>
        /// <response code="404">NotFound - finner ikke innsending med gitt arkivreferanse. NB det kan ta flere minutter fra innsending til arkivreferanse er tilgjengelig i fellestjenester bygg</response>
        /// <response code="500">Error - intern feil</response>
        [Swashbuckle.Swagger.Annotations.SwaggerOperation(Tags = new[] { "Status innsending" })]
        [ResponseType(typeof(ForsendelseRepresentation))]
        [Route("api/v1/innsending/forsendelser/{arkivreferanse}")]
        [HttpGet]
        public IHttpActionResult Get(string arkivreferanse = null)
        {
            ForsendelseContext context;
            if (!ValidateInput(arkivreferanse, out context))
            {
                return context.Reason;
            }

            if (!ServiceCodeHelper.GetInnsendingsType(context.FormMetadata).Equals(ServiceCodeHelper.Shipping))
                return NotFound();

            var forsendelse = new ForsendelseRepresentation(context.Arkivreferanse)
            {
                // Can be put into separate receipt class - START
                Saksaar = context.FormMetadata.MunicipalityArchiveCaseYear > 0 ? context.FormMetadata.MunicipalityArchiveCaseYear.ToString() : null,
                Sakssekvensnummer = context.FormMetadata.MunicipalityArchiveCaseSequence > 0 ? context.FormMetadata.MunicipalityArchiveCaseSequence.ToString() : null,
                // Can be put into separate receipt class - END

                OffentligJournalUrl = context.FormMetadata.MunicipalityPublicArchiveCaseUrl, //Isn't used for anything since it's present in Receipt as well
                SvarUtForsendelseStatus = context.FormMetadata.SvarUtForsendelsesStatus?.ForsendelseStatus,
                SvarUtForsendelseMottatt = context.FormMetadata.SvarUtShippingTimestamp,
                SvarUtReferanseId = context.FormMetadata.SvarUtForsendelsesId
            };

            forsendelse.Filnedlastinger = new FilnedlastingerProvider(_fileDownloadStatusService).GetFiledownloads(context.Arkivreferanse);

            return Ok(forsendelse);
        }

        private class ForsendelseContext
        {
            public string Arkivreferanse { get; set; }
            public FormMetadata FormMetadata { get; set; }
            public IHttpActionResult Reason { get; set; }
        }
    }
}