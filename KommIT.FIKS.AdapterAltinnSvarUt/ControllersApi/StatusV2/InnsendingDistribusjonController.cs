using KommIT.FIKS.AdapterAltinnSvarUt.ControllersApi.StatusV2.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Services;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Authentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;

namespace KommIT.FIKS.AdapterAltinnSvarUt.ControllersApi.StatusV2
{
    /// <summary>
    /// API for informasjon om status på distribusjoner
    /// </summary>
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [BasicAuthentication]
    [Swagger.SwaggerResponseContentType(responseType: "application/hal+json", Exclusive = true)]
    public class InnsendingDistribusjonController : ApiController
    {
        private readonly IFormMetadataService _formMetadataService;
        private readonly IFileDownloadStatusService _fileDownloadStatusService;
        private readonly DistributionFormsRepository _distributionFormsRepository;
        private readonly IAuthorizationService _authorizationService;

        public InnsendingDistribusjonController(IFormMetadataService formMetadataService,
                                                IFileDownloadStatusService fileDownloadStatusService,
                                                DistributionFormsRepository distributionFormsRepository,
                                                IAuthorizationService authorizationService)
        {
            _formMetadataService = formMetadataService;
            _fileDownloadStatusService = fileDownloadStatusService;
            _distributionFormsRepository = distributionFormsRepository;
            _authorizationService = authorizationService;
        }

        private bool ValidateInput(string arkivreferanse, string filterStatus, out DistribusjonContext context)
        {
            context = new DistribusjonContext();
            if (string.IsNullOrWhiteSpace(arkivreferanse))
            {
                context.Reason = BadRequest("arkivreferanse må ha verdi");
                return false;
            }
            context.Arkivreferanse = arkivreferanse.Trim().ToUpper();
            context.FormMetadata = _formMetadataService.GetFormMetadata(context.Arkivreferanse);

            if (context.FormMetadata == null)
            {
                context.Reason = NotFound();
                return false;
            }
            if (!_authorizationService.UserHasAccess(context.FormMetadata))
            {
                context.Reason = Unauthorized();
                return false;
            }

            if (!string.IsNullOrEmpty(filterStatus) && !filterStatus.Equals("all", System.StringComparison.OrdinalIgnoreCase))
            {
                var filterStatusEnumValue = DistributionStatus.error;
                if (Enum.TryParse(filterStatus, true, out filterStatusEnumValue))
                {
                    context.FilterByStatus = true;
                    context.FilterStatuses = new List<DistributionStatus>() { filterStatusEnumValue };
                }
            }
            return true;
        }

        /// <summary>
        /// Henter ut status på en distribusjon
        /// </summary>
        /// <param name="arkivreferanse">arkivreferanse i Altinn (eks AR12345)</param>
        /// <returns></returns>
        /// <response code="200">Ok</response>
        /// <response code="401">Ingen tilgang</response>
        /// <response code="404">NotFound - finner ikke innsending med gitt arkivreferanse. NB det kan ta flere minutter fra innsending til arkivreferanse er tilgjengelig i fellestjenester bygg</response>
        /// <response code="500">Error - intern feil</response>
        [Swashbuckle.Swagger.Annotations.SwaggerOperation(Tags = new[] { "Status innsending" })]
        [ResponseType(typeof(DistribusjonerRepresentation))]
        [Route("api/v1/innsending/distribusjoner/{arkivreferanse}")]
        [HttpGet]
        public IHttpActionResult Get(string arkivreferanse = null)
        {
            DistribusjonContext context;
            if (!ValidateInput(arkivreferanse, null, out context))
            {
                return context.Reason;
            }

            if (!ServiceCodeHelper.GetInnsendingsType(context.FormMetadata).Equals(ServiceCodeHelper.Distribution))
                return NotFound();

            var distributionSummary = new DistributionSummaryProvider(_formMetadataService).GetDistributionSummaryFor(context.FormMetadata.ServiceCode);

            var distribusjon = new DistribusjonerRepresentation(context.Arkivreferanse)
            {
                AntallDistribusjoner = distributionSummary.GetTotalDistributions(context.Arkivreferanse),
                AntallDistribusjonerFeilet = distributionSummary.GetFailedDistributions(context.Arkivreferanse),
                AntallDistribusjonerOk = distributionSummary.GetOkDistributions(context.Arkivreferanse)
            };

            distribusjon.Filnedlastinger = new FilnedlastingerProvider(_fileDownloadStatusService).GetFiledownloads(context.Arkivreferanse);

            return Ok(distribusjon);
        }

        /// <summary>
        /// Henter ut detaljert status på distribusjoner for en arkivreferanse
        /// </summary>
        /// <param name="arkivreferanse">arkivreferanse i Altinn (eks AR12345)</param>
        /// <param name="status">
        /// <list type="table">
        /// <item>
        /// <term>error</term>
        /// <description>Feilede distribusjoner</description>
        /// </item>
        /// <item>
        /// <term>submittedPrefilled</term>
        /// <description>Distribusjoner sent til mottaker</description>
        /// </item>
        /// <item>
        /// <term>signed</term>
        /// <description>Distribusjoner besvart og signert av mottaker</description>
        /// </item>
        /// <item>
        /// <term>receiptSent</term>
        /// <description>Distribusjoner hvor innsender har blitt notifisert om besvarelsen</description>
        /// </item>
        /// </list>
        /// </param>
        /// <param name="page">Sidenummer</param>
        /// <returns></returns>
        /// <response code="200">Ok</response>
        /// <response code="401">Unauthorized - Ingen tilgang</response>
        /// <response code="404">NotFound - finner ikke innsending med gitt arkivreferanse. NB det kan ta flere minutter fra innsending til arkivreferanse er tilgjengelig i fellestjenester bygg</response>
        /// <response code="500">Error - intern feil</response>
        [Swashbuckle.Swagger.Annotations.SwaggerOperation(Tags = new[] { "Status innsending" })]
        [ResponseType(typeof(DistribusjonsdetaljerListRepresentation))]
        [Route("api/v1/innsending/distribusjoner/{arkivreferanse}/distribusjonsdetaljer")]
        [HttpGet]
        public IHttpActionResult GetDistribusjonsdetaljer(string arkivreferanse, string status = "all", int page = 1)
        {
            var pageSize = 20;

            DistribusjonContext context;
            if (!ValidateInput(arkivreferanse, status, out context))
            {
                return context.Reason;
            }

            if (!ServiceCodeHelper.GetInnsendingsType(context.FormMetadata).Equals(ServiceCodeHelper.Distribution))
                return NotFound();

            PagedResult<DistribusjonsdetaljerRepresentation> distrs = null;
            if (context.FilterByStatus)
                distrs = _distributionFormsRepository.Find(new DistributionFormsPagedQuery(d => d.InitialArchiveReference.Equals(context.Arkivreferanse) && context.FilterStatuses.Contains(d.DistributionStatus)), page, pageSize);
            else
                distrs = _distributionFormsRepository.Find(new DistributionFormsPagedQuery(d => d.InitialArchiveReference.Equals(context.Arkivreferanse)), page, pageSize);

            var resourceList = new DistribusjonsdetaljerListRepresentation(distrs.ToList(), distrs.TotalResults, distrs.TotalPages, page, LinkTemplates.Innsendinger.DistribusjonsDetaljer, new { referanse = context.Arkivreferanse });

            return Ok(resourceList);
        }

        private class DistribusjonContext
        {
            public string Arkivreferanse { get; set; }
            public FormMetadata FormMetadata { get; set; }
            public List<DistributionStatus> FilterStatuses { get; set; }
            public bool FilterByStatus { get; set; }
            public IHttpActionResult Reason { get; set; }
        }
    }
}