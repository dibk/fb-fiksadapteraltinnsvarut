﻿using KommIT.FIKS.AdapterAltinnSvarUt.ControllersApi.StatusV2.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Services;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Authentication;
using Serilog;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;

namespace KommIT.FIKS.AdapterAltinnSvarUt.ControllersApi.StatusV2
{
    public class Context
    {
        public string Arkivreferanse { get; set; }
        public FormMetadata FormMetadata { get; set; }
        public List<FormAttachmentMetadata> Attachments { get; set; }
        public IHttpActionResult Reason { get; set; }
    }

    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [BasicAuthentication]
    [Swagger.SwaggerResponseContentType(responseType: "application/hal+json", Exclusive = true)]
    public class InnsendingHendelseController : ApiController
    {
        private readonly IFormMetadataService _formMetadataService;
        private readonly HendelserRepository _hendelseRepository;
        private readonly IAuthorizationService _authorizationService;
        private readonly ILogger _logger;

        public InnsendingHendelseController(ILogger logger,
                                            IFormMetadataService formMetadataService,
                                            HendelserRepository hendelseRepository,
                                            IAuthorizationService authorizationService)
        {
            _formMetadataService = formMetadataService;
            _hendelseRepository = hendelseRepository;
            _authorizationService = authorizationService;
            _logger = logger;
        }

        /// <summary>
        /// Henter ut hendelser på en innsending
        /// </summary>
        /// <param name="arkivreferanse">arkivreferanse i Altinn (eks AR12345)</param>
        /// <returns></returns>
        /// <response code="200">Ok</response>
        /// <response code="401">Unauthorized - Ingen tilgang</response>
        /// <response code="404">NotFound - finner ikke innsending med gitt arkivreferanse. NB det kan ta flere minutter fra innsending til arkivreferanse er tilgjengelig i fellestjenester bygg</response>
        /// <response code="500">Error - intern feil</response>
        [ResponseType(typeof(InnsendingHendelseListRepresentation))]
        [Swashbuckle.Swagger.Annotations.SwaggerOperation(Tags = new[] { "Status innsending" })]
        [Route("api/v1/innsending/{arkivreferanse}/hendelser")]
        [HttpGet]
        public IHttpActionResult Get(string arkivreferanse = null, int page = 1)
        {
            int pageSize = 30;
            Context context;
            if (!ValidateInput(arkivreferanse, out context))
            {
                return context.Reason;
            }

            var logEntry = _hendelseRepository.Find(new LogEntriesQuery(d => d.ArchiveReference.Equals(context.Arkivreferanse)), page, pageSize);
            var resourceList = new InnsendingHendelseListRepresentation(logEntry.ToList(), logEntry.TotalResults, logEntry.TotalPages, page, LinkTemplates.Innsendinger.InnsendingHendelser, new { referanse = context.Arkivreferanse });

            return Ok(resourceList);
        }

        private bool ValidateInput(string arkivreferanse, out Context context)
        {
            context = new Context();
            if (string.IsNullOrWhiteSpace(arkivreferanse))
            {
                context.Reason = BadRequest("arkivreferanse må ha verdi");
                return false;
            }
            context.Arkivreferanse = arkivreferanse.Trim().ToUpper();
            context.FormMetadata = _formMetadataService.GetFormMetadata(context.Arkivreferanse);

            if (context.FormMetadata == null)
            {
                context.Reason = NotFound();
                return false;
            }

            if (!_authorizationService.UserHasAccess(context.FormMetadata))
            {
                context.Reason = Unauthorized();
                return false;
            }

            context.Attachments = context.FormMetadata.FormAttachmentMetadatas;

            return true;
        }
    }
}