﻿using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Models.ApiModels;
using KommIT.FIKS.AdapterAltinnSvarUt.Services;
using Serilog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;

namespace KommIT.FIKS.AdapterAltinnSvarUt.ControllersApi
{
    [EnableCors(origins: "*", headers: "*", methods: "*", exposedHeaders: "x-correlation-id")]
    public class FormValidationController : ApiController
    {
        private readonly ILogger _logger;
        private readonly IApplicationLogService _applicationLogService;

        public FormValidationController(ILogger logger, IApplicationLogService applicationLogService)
        {
            _logger = logger;
            _applicationLogService = applicationLogService;
        }

        /// <summary>
        /// Valider skjema struktur, innhold og altinn vedleggsregler
        /// </summary>
        /// <param name="form">skjema som skal valideres</param>
        /// <returns>Valideringsresultat</returns>
        /// <response code="200">ok</response>
        /// <response code="400">BadRequest - ugyldig forespørsel</response>
        /// <response code="403">Forbidden - ingen tilgang</response>
        /// <response code="500">Error - intern feil</response>
        [Route("api/validatev2/form")]
        [HttpPost]
        [ResponseType(typeof(ValidationResult))]
        public HttpResponseMessage ValidateV2Form(ValidateFormv2 form)
        {
            var stopWatch = new Stopwatch();

            ValidationContext context;
            if (!InputIsValid(out context))
                return context.Reason;

            if (form != null)
            {
                var appLog = new ApplicationLogBuilder(ProcessLabel.ValidationApi, "ValidateV2Form");
                _logger.Debug("API validation request for form {DataFormatId} - {DataFormatVersion}", form.DataFormatId, form.DataFormatVersion);

                appLog.SetDatamodelIds(form.DataFormatId, form.DataFormatVersion);
                _applicationLogService.Save(appLog.SetMessage("API Validation Request", ApplicationLog.Info));

                try
                {
                    stopWatch.Start();
                    _applicationLogService.Save(appLog.SetMessage("Starting form validation", ApplicationLog.Info));

                    FormValidationServiceV2 val = new FormValidationServiceV2();
                    _logger.Debug("API validation request for form {DataFormatId} - and {DataFormatVersion} : Validating structure", form.DataFormatId, form.DataFormatVersion);
                    ValidationResult result = val.ValidateFormStructure(form.DataFormatId, form.DataFormatVersion, form.FormData);
                    if (result.IsOk())
                    {
                        _logger.Debug("API validation request for form {DataFormatId} - {DataFormatVersion} : Validating form data", form.DataFormatId, form.DataFormatVersion);

                        //Validate FormData
                        List<string> attachmentNames = new List<string>();
                        if (form.AttachmentTypesAndForms != null && form.AttachmentTypesAndForms.Any())
                            attachmentNames = form.AttachmentTypesAndForms.Select(attach => attach.Name).ToList();
                        val.ValidateFormData(form.DataFormatId, form.DataFormatVersion, form.FormData, attachmentNames);

                        //Validate Attachments
                        result = val.ValidateAttachmentsAndSubForms(form.DataFormatId, form.DataFormatVersion, form.AttachmentTypesAndForms.ToList());
                    }
                    _logger.Debug("API Validation request for {DataFormatId} - {DataFormatVersion} : OK -- Errors: {ValidationErrors} Warnings: {ValidationWarnings} ElapsedTime: {ElapsedTime}", form.DataFormatId, form.DataFormatVersion, result.Errors, result.Warnings, stopWatch.ElapsedMilliseconds);
                    _applicationLogService.Save(appLog.SetMessage($"Completed form validation (errors: {result.Errors}/ warnings: {result.Warnings})", ApplicationLog.Info, stopWatch));

                    return Request.CreateResponse(HttpStatusCode.OK, result);
                }
                catch (Utils.DeserializationException dex)
                {
                    _logger.Error(dex, "Unable to deserialize xml {DataFormatId} - {DataFormatVersion}", form.DataFormatId, form.DataFormatVersion);
                    var result = new ValidationResult();
                    result.AddError($"Unable to deserialize xml {form.DataFormatId} - {form.DataFormatVersion} : {dex.Message}", string.Empty);

                    return Request.CreateResponse(HttpStatusCode.BadRequest, result);
                }
                catch (Exception ex)
                {
                    _logger.Error(ex, "API Validation request for form {DataFormatId} - {DataFormatVersion} failed", form.DataFormatId, form.DataFormatVersion);
                    _applicationLogService.Save(appLog.SetMessage($"Exception in form validation: {ex.Message}", ApplicationLog.Exception, stopWatch, ex.StackTrace));

                    return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
                }
            }
            return Request.CreateResponse(HttpStatusCode.BadRequest);
        }

        /// <summary>
        /// Valider skjema struktur og innhold
        /// </summary>
        /// <param name="form">skjema som skal valideres</param>
        /// <returns>Valideringsresultat</returns>
        /// <response code="200">ok</response>
        /// <response code="400">BadRequest - ugyldig forespørsel</response>
        /// <response code="403">Forbidden - ingen tilgang</response>
        /// <response code="500">Error - intern feil</response>
        [Route("api/validate/form")]
        [HttpPost]
        [ResponseType(typeof(ValidationResult))]
        public HttpResponseMessage ValidateForm(ValidateForm form)
        {
            ValidationContext context;
            if (!InputIsValid(out context))
                return context.Reason;

            if (form != null)
            {
                _logger.Debug("API validation request for form {DataFormatId} - {DataFormatVersion}", form.DataFormatId, form.DataFormatVersion);

                try
                {
                    FormValidationService val = new FormValidationService();
                    _logger.Debug("API validation request for form {DataFormatId} - {DataFormatVersion} : Validating structure", form.DataFormatId, form.DataFormatVersion);
                    ValidationResult result = val.ValidateFormStructure(form.DataFormatId, form.DataFormatVersion, form.FormData);
                    if (result.IsOk())
                    {
                        _logger.Debug("API validation request for form {DataFormatId} - {DataFormatVersion} : Validating form data", form.DataFormatId, form.DataFormatVersion);
                        var AttachmentAndFormsList = new List<string>();
                        if (form.AttachmentTypesAndForms != null) AttachmentAndFormsList.AddRange(form.AttachmentTypesAndForms);
                        result = val.ValidateFormData(form.DataFormatId, form.DataFormatVersion, form.FormData, AttachmentAndFormsList);
                    }

                    _logger.Debug("API Validation request for {DataFormatId} - {DataFormatVersion} : OK -- Errors: {ValidationErrors} Warnings: {ValidationWarnings} ", form.DataFormatId, form.DataFormatVersion, result.Errors, result.Warnings);

                    return Request.CreateResponse(HttpStatusCode.OK, result);
                }
                catch (Utils.DeserializationException dex)
                {
                    _logger.Error(dex, "Unable to deserialize xml {DataFormatId} - {DataFormatVersion}", form.DataFormatId, form.DataFormatVersion);
                    var result = new ValidationResult();
                    result.AddError($"Unable to deserialize xml {form.DataFormatId} - {form.DataFormatVersion} : {dex.Message}", string.Empty);

                    return Request.CreateResponse(HttpStatusCode.BadRequest, result);
                }
                catch (Exception ex)
                {
                    _logger.Error(ex, "API Validation request for form {DataFormatId} - {DataFormatVersion} failed", form.DataFormatId, form.DataFormatVersion);

                    return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
                }
            }
            return Request.CreateResponse(HttpStatusCode.BadRequest);
        }

        private bool InputIsValid(out ValidationContext context)
        {
            context = new ValidationContext();
            if (!ModelState.IsValid)
            {
                ValidationResult modelStateResult = new ValidationResult();
                foreach (var state in ModelState.Values)
                {
                    foreach (var error in state.Errors)
                    {
                        if (string.IsNullOrEmpty(error.ErrorMessage))
                            modelStateResult.AddError(error.Exception.Message, string.Empty);
                        else
                            modelStateResult.AddError(error.ErrorMessage, string.Empty);
                    }
                }
                context.Reason = Request.CreateResponse(HttpStatusCode.BadRequest, modelStateResult);
                return false;
            }
            return true;
        }

        public class ValidationContext
        {
            public HttpResponseMessage Reason { get; set; }
        }
    }
}