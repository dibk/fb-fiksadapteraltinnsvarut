﻿using KommIT.FIKS.AdapterAltinnSvarUt.Models.ViewModels;
using KommIT.FIKS.AdapterAltinnSvarUt.Services;
using Serilog;
using System;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;

namespace KommIT.FIKS.AdapterAltinnSvarUt.ControllersApi
{
    [ApiExplorerSettings(IgnoreApi = true)]
    [EnableCors(origins: "*", headers: "*", methods: "*", exposedHeaders: "x-correlation-id")]
    public class MunicipalityAPIController : ApiController
    {
        private readonly IMunicipalityService municipalityService;
        private readonly ILogger logger;

        public MunicipalityAPIController(IMunicipalityService municipalityService, ILogger logger)
        {
            this.municipalityService = municipalityService;
            this.logger = logger.ForContext<MunicipalityAPIController>();
        }

        [Route("api/municipalities/{municipalityCode}")]
        [HttpGet]
        public HttpResponseMessage GetMunicipality(string municipalityCode)
        {
            if (string.IsNullOrEmpty(municipalityCode))
                return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);

            try
            {
                logger.Debug("Request received for municipalityCode {municipalityCode}", municipalityCode);
                var municipality = municipalityService.GetMunicipality(municipalityCode.Trim());
                logger.Debug("Data fetched for municipalityCode {municipalityCode}", municipalityCode);
                var municipalityViewModel = new MunicipalityViewModelMapper().MapToViewModel(municipality);
                logger.Debug("Data mapped to view model for municipalityCode {municipalityCode}", municipalityCode);

                if (municipalityViewModel == null)
                    return Request.CreateResponse(System.Net.HttpStatusCode.NotFound);

                return Request.CreateResponse(System.Net.HttpStatusCode.OK, municipalityViewModel);
            }
            catch (ArgumentException)
            {
                return Request.CreateResponse(System.Net.HttpStatusCode.NotFound);
            }
            catch (Exception e)
            {
                logger.Error(e, "An error occurred while retrieving municipality with code: {municipalityCode}", municipalityCode);

                return Request.CreateErrorResponse(System.Net.HttpStatusCode.InternalServerError, $"An error occurred while retrieving municipality with code: {municipalityCode}");
            }
        }

        //TODO extend with the option to get the whole list
    }
}