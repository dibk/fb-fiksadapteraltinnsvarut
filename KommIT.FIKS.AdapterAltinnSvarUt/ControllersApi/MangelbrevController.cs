using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Models.ApiModels;
using KommIT.FIKS.AdapterAltinnSvarUt.Services;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Authentication;
using Serilog;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace KommIT.FIKS.AdapterAltinnSvarUt.ControllersApi
{
    /// <summary>
    /// API for kvitteringer fra kommuner
    /// </summary>
    [BasicAuthentication]
    [EnableCors(origins: "*", headers: "*", methods: "*", exposedHeaders: "x-correlation-id")]
    public class MangelbrevController : ApiController
    {
        private readonly IDeviationLetterService _deviationLetterService;
        private readonly IDeviationService _deviationService;
        private readonly ILogger _logger;

        public MangelbrevController(ILogger logger,
            IDeviationLetterService deviationLetterService, IDeviationService deviationService)
        {
            _deviationLetterService = deviationLetterService;
            _deviationService = deviationService;
            _logger = logger;
        }

        /// <summary>
        /// Oppretter mangelbrev for søknad
        /// </summary>
        /// <response code="200">ok</response>
        /// <response code="400">BadRequest - ugyldig forespørsel</response>
        /// <response code="404">NotFound - finner ikke innsending med gitt arkivreferanse i altinn</response>
        /// <response code="401">Unauthorized - ingen tilgang</response>
        /// <response code="500">Error - intern feil</response>
        [Route("api/mangelbrev")]
        [HttpPost]
        public HttpResponseMessage PostMangelbrev(Mangelbrev mangelbrev)
        {
            if (mangelbrev != null)
            {
                try
                {
                    DeviationLetter dvl = new DeviationLetter(mangelbrev);
                    List<Deviation> deviations = Deviation.MapToList(dvl.DeviationLetterId, mangelbrev.Mangler);

                    //Lagre mangelbrev og mangler
                    _deviationLetterService.SaveDeviationLetter(dvl);

                    //Lagre mangler
                    _deviationService.SaveDeviations(deviations);

                    _logger.Information("Mangelbrev med evt mangler fra kommune mottatt {Saksaar}/{Sakssekvensnummer} Altinn arkivreferanse {AltinnArkivreferanse}", mangelbrev.Saksaar, mangelbrev.Sakssekvensnummer, mangelbrev.AltinnArkivreferanse);

                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                    return response;
                }
                catch (Exception ex)
                {
                    _logger.Error(ex, "Det har skjedd en feil");
                    return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
                }
            }
            else
            {
                _logger.Information("Mangelbrev er null ");
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }
    }
}