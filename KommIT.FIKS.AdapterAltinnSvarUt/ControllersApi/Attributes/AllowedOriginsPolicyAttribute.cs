﻿using System;
using System.Configuration;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Cors;
using System.Web.Http.Cors;

namespace KommIT.FIKS.AdapterAltinnSvarUt.ControllersApi
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class, AllowMultiple = false)]
    public class AllowedOriginsPolicyAttribute : Attribute, ICorsPolicyProvider
    {
        private CorsPolicy _policy;

        public AllowedOriginsPolicyAttribute(string configKey)
        {
            _policy = new CorsPolicy
            {
                AllowAnyMethod = true,
                AllowAnyHeader = true
            };

            _policy.ExposedHeaders.Add("Content-Disposition");
            _policy.ExposedHeaders.Add("x-correlation-id");

            var configuredOrigins = ConfigurationManager.AppSettings[configKey];
            string[] origins = null;
            if (!string.IsNullOrEmpty(configuredOrigins))
                origins = configuredOrigins.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            else
                origins = new string[] { "http://localhost:57262" };

            foreach (var origin in origins)
            {
                _policy.Origins.Add(origin);
            }
        }

        public Task<CorsPolicy> GetCorsPolicyAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            return Task.FromResult(_policy);
        }
    }
}