﻿using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Services;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Azure;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;

namespace KommIT.FIKS.AdapterAltinnSvarUt.ControllersApi
{
    [ApiExplorerSettings(IgnoreApi = true)]
    [EnableCors(origins: "*", headers: "*", methods: "*", exposedHeaders: "x-correlation-id")]
    public class DownloadController : ApiController
    {
        private ILogger _logger = Log.ForContext<DownloadController>();
        private readonly IFileDownloadStatusService _fileDownloadStatusService;
        private readonly IFileStorage _fileStorage;

        public DownloadController(IFileDownloadStatusService fileDownloadStatusService, IFileStorage fileStorage)
        {
            _fileDownloadStatusService = fileDownloadStatusService;
            _fileStorage = fileStorage;
        }

        [System.Web.Http.Route("api/download/{guid}/{filename}")]
        [System.Web.Http.HttpGet]
        public HttpResponseMessage Download(string guid, string filename)
        {
            List<FileDownloadStatus> downloadStatuses;

            try
            {
                _logger.Debug("{guid} - {filename} - retrieves file information.", guid, filename);
                downloadStatuses = _fileDownloadStatusService.GetFileDownloadStatusBy(guid, filename);
            }
            catch (ArgumentException ae)
            {
                _logger.Error(ae, "{guid} - {filename} - parameters for retrieving download status are invalid.", guid, filename);
                return Request.CreateResponse(HttpStatusCode.NotFound, "Kunne ikke dekode fil sti");
            }
            catch (Exception e)
            {
                _logger.Error(e, "{guid} - {filename} - an error occurred while retrieving file information", guid, filename);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, "Kunne ikke behandle forespørsel");
            }

            if (downloadStatuses.Count == 0)
            {
                _logger.Debug("{guid} - {filename} - no occurrences found", guid, filename);
                return Request.CreateResponse(HttpStatusCode.NotFound, "Fant ikke fil");
            }

            FileDownloadStatus downloadStatus = downloadStatuses.FirstOrDefault();

            if (downloadStatus.IsDeleted == true)
            {
                _logger.Debug("{guid} - {filename} - file is marked as deleted", guid, filename);
                return Request.CreateResponse(HttpStatusCode.Gone, "Serverfil er slettet");
            }

            downloadStatus.FileAccessCount++;
            _fileDownloadStatusService.Update(downloadStatus);

            try
            {
                _logger.Debug("{guid} - {filename} - ready for streaming file to response - adds headers.", guid, filename);

                var stream = new MemoryStream(); //Do not use "using" on this one. It would dispose the stream when returning the response

                try
                {
                    _fileStorage.StreamFile(downloadStatus.Guid.ToString(), downloadStatus.Filename, stream);
                }
                catch (Azure.RequestFailedException ex)
                {
                    if (ex.Status == 404)
                        _fileStorage.StreamFile(downloadStatus.BlobLink, stream);
                    else
                        throw;
                }

                stream.Seek(0, SeekOrigin.Begin);
                var response = Request.CreateResponse(HttpStatusCode.OK);

                _logger.Debug("{guid} - {filename} - streams file to response.", guid, filename);
                response.Content = new StreamContent(stream);
                response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/octet-stream");
                response.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment") { FileName = filename };

                return response;
            }
            catch (Exception e)
            {
                _logger.Error(e, "{guid} - {filename} - an error occurred while streaming file to output.", guid, filename);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e.Message);
            }
        }
    }
}