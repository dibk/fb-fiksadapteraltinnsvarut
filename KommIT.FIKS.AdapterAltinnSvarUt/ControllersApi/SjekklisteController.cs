﻿using Serilog;
using System.Collections.Generic;
using System.Configuration;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;

namespace KommIT.FIKS.AdapterAltinnSvarUt.ControllersApi
{
    /// <summary>
    ///     Åpne API for sjekklister fra fellestjenester bygg
    /// </summary>
    //[RoutePrefix("api/sjekkliste")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class SjekklisteController : ApiController
    {
        // ***
        //    API controller only used for redirects to new checklist solution
        // ***

        //private readonly ActivityService _activityService;
        private readonly ILogger _logger;

        private static string ftbChecklistBaseUri = ConfigurationManager.AppSettings["FTBSjekklisteServer"];

        public SjekklisteController(ILogger iLogger)
        {
            //_activityService = activityService;
            _logger = iLogger;
        }

        /// <summary>
        ///     Hente ut SAK10 sjekkliste for en søknadstype/prosesskategori, milepel og tiltakstyper
        /// </summary>
        /// <returns>Sjekkliste</returns>
        /// <response code="200">ok</response>
        /// <response code="400">BadRequest - ugyldig forespørsel</response>
        /// <response code="404">NotFound - finner ikke sjekkliste for prosesskategori, milepel og tiltakstyper</response>
        /// <response code="500">Error - intern feil</response>
        [Route("api/sjekkliste")]
        [Route("api/sjekkliste/{prosesskategori}")]
        [Route("api/sjekkliste/{prosesskategori}/milepel/{milepel}")]
        [HttpGet]
        public IHttpActionResult GetSjekkliste(string prosesskategori = null, [FromUri] List<string> tiltakstyper = null,
            string milepel = null)
        {
            var resource = string.Empty;

            if (!string.IsNullOrEmpty(prosesskategori))
                resource = $"/{prosesskategori}";

            if (!string.IsNullOrEmpty(milepel))
                resource = $"{resource}/milepel/{milepel}";

            if (tiltakstyper?.Count > 0)
                resource = $"{resource}?tiltakstyper[]={string.Join(",", tiltakstyper)}";

            var redirectUri = $"{ftbChecklistBaseUri}/api/sjekkliste{resource}";

            return Redirect(redirectUri);

            //List<Activity> activitiesList = new List<Activity>();
            //try
            //{
            //    _logger.Debug("Sjekkliste API kall mottatt");

            //activitiesList = _activityService.GetActivities(prosesskategori, tiltakstyper, milepel);

            //    var checklist = _activityService.ToCheckList(activitiesList);

            //    return Request.CreateResponse(HttpStatusCode.OK, checklist.ToArray());
            //}

            //catch (Exception ex)
            //{
            //    _logger.Error($"Exception i sjekkliste API kall: {ex.ToString()}, {ex.Message}, {ex.StackTrace}");
            //    return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            //}
        }

        /// <summary>
        ///     Hente ut tiltakstyper for en søknadstype/prosesskategori og sjekkpunkt
        /// </summary>
        /// <returns>Liste med tiltakstyper</returns>
        /// <response code="200">ok</response>
        /// <response code="400">BadRequest - ugyldig forespørsel</response>
        /// <response code="500">Error - intern feil</response>
        [Route("api/tiltakstyper/{prosesskategori}/sjekkpunkt/{pkt}")]
        [HttpGet]
        [ResponseType(typeof(string[]))]
        public IHttpActionResult GetTiltakstyper(string prosesskategori, string pkt)
        {
            return Redirect($"{ftbChecklistBaseUri}/tiltakstyper/{prosesskategori}/sjekkpunkt/{pkt}");
            //try
            //{
            //    _logger.Debug("GetTiltakstyper API kall mottatt");

            //var EnterpriseTermsList = _activityService.GetActivityEnterpriseTerms(pkt, prosesskategori);

            //    return Request.CreateResponse(HttpStatusCode.OK, EnterpriseTermsList);
            //}

            //catch (Exception ex)
            //{
            //    _logger.Error($"Exception i GetTiltakstyper API kall: {ex.ToString()}, {ex.Message}, {ex.StackTrace}");
            //    return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            //}
        }

        /// <summary>
        ///     Hente ut informasjon om et sjekkpunkt
        /// </summary>
        /// <returns>Sjekkpunkt</returns>
        /// <response code="200">ok</response>
        /// <response code="400">BadRequest - ugyldig forespørsel</response>
        /// <response code="500">Error - intern feil</response>
        [Route("api/sjekkpunkt/{prosesskategori}/sjekkpunkt/{pkt}")]
        [HttpGet]
        public IHttpActionResult GetSjekkpunkt(string prosesskategori, string pkt)
        {
            return Redirect($"{ftbChecklistBaseUri}/api/sjekkpunkt/{prosesskategori}/sjekkpunkt/{pkt}");
            //try
            //{
            //    _logger.Debug("GetSjekkpunkt API kall mottatt");

            //    var sjekkpkt = _activityService.GetSjekkpunkt(pkt, prosesskategori);

            //    return Request.CreateResponse(HttpStatusCode.OK, sjekkpkt);
            //}

            //catch (Exception ex)
            //{
            //    _logger.Error($"Exception i GetSjekkpunkt API kall: {ex.ToString()}, {ex.Message}, {ex.StackTrace}");
            //    return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            //}
        }
    }
}