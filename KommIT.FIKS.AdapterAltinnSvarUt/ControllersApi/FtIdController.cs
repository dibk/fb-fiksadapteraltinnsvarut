using KommIT.FIKS.AdapterAltinnSvarUt.App_Start;
using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Models.ApiModels;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Authentication;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.FtId;
using Microsoft.Extensions.Logging;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace KommIT.FIKS.AdapterAltinnSvarUt.ControllersApi
{
    [BasicAuthentication]
    [EnableCors(origins: "*", headers: "*", methods: "*", exposedHeaders: "x-correlation-id")]
    public class FtIdController : ApiController
    {
        private readonly ILogger<FtIdController> _logger;
        public readonly IFtIdService _ftIdService;

        public FtIdController(
            ILogger<FtIdController> logger,
            IFtIdService ftIdService)
        {
            _logger = logger;
            _ftIdService = ftIdService;
        }

        [Route("api/ftId")]
        [HttpPost]
        [RateLimit(TimeUnit = TimeUnit.Minute, Count = 200)]
        public HttpResponseMessage CreateNew([FromBody] NewFtIdPayload payload)
        {
            try
            {
                if (payload == null)
                    return Request.CreateResponse(HttpStatusCode.BadRequest);

                payload.Validate();

                var result = _ftIdService.CreateNew(payload);

                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (FtIdException exception)
            {
                _logger.LogWarning(exception, "Error creating new ftid");
                return Request.CreateResponse(HttpStatusCode.BadRequest, exception.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Unhandeled error occurred while creating new ftid");
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        [Route("api/ftId/attach")]
        [HttpPost]
        public HttpResponseMessage Attach([FromBody] AttachFtIdPayload payload)
        {
            try
            {
                if (payload == null)
                    return Request.CreateResponse(HttpStatusCode.BadRequest);

                payload.Validate();

                _ftIdService.Attach(payload);

                return Request.CreateResponse(HttpStatusCode.NoContent);
            }
            catch (FtIdException exception)
            {
                _logger.LogWarning(exception, "Error attaching ftid and archivereference");
                return Request.CreateResponse(HttpStatusCode.BadRequest, exception.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Unhandeld error occured while attaching ftid and archivereference");
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        [Route("api/ftId/{ftId}")]
        [HttpGet]
        public HttpResponseMessage GetAttachedMetadata(string ftId)
        {
            try
            {
                var result = _ftIdService.GetAttachedMetadata(ftId);

                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Unhandeld error occurred while retreiving ftid metadata.");
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }
    }
}