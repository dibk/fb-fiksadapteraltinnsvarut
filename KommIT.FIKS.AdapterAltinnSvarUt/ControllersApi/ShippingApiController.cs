﻿using KommIT.FIKS.AdapterAltinnSvarUt.Services.Altinn;
using Microsoft.Extensions.Logging;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace KommIT.FIKS.AdapterAltinnSvarUt.ControllersApi
{
    [ApiExplorerSettings(IgnoreApi = true)]
    public class ShippingApiController : ApiController
    {
        private readonly ILogger<ShippingApiController> _logger;
        private readonly AltinnFormDownloadAndRoutingService_DIBK _dibk_altinnFormDownloadAndRoutingService;
        private readonly AltinnFormDownloadAndRoutingService_ATIL _atil_altinnFormDownloadAndRoutingService;

        public ShippingApiController(ILogger<ShippingApiController> logger, AltinnFormDownloadAndRoutingService_DIBK altinnFormDownloadAndRoutingService, AltinnFormDownloadAndRoutingService_ATIL atil_altinnFormDownloadAndRoutingService)
        {
            _logger = logger;
            _dibk_altinnFormDownloadAndRoutingService = altinnFormDownloadAndRoutingService;
            _atil_altinnFormDownloadAndRoutingService = atil_altinnFormDownloadAndRoutingService;
        }

        [Route("api/shipping/{serviceOwner}/{archiveReference}/{forceProcessing}")]
        [HttpPost]
        public HttpResponseMessage ProcessShipment(string serviceOwner, string archiveReference, bool forceProcessing = false)
        {
            try
            {
                if (serviceOwner.Equals("DIBK", StringComparison.OrdinalIgnoreCase))
                    _dibk_altinnFormDownloadAndRoutingService.InitiateProcessingOf(archiveReference, forceProcessing);
                else if (serviceOwner.Equals("ATIL", StringComparison.OrdinalIgnoreCase))
                    _atil_altinnFormDownloadAndRoutingService.InitiateProcessingOf(archiveReference, forceProcessing);
                else
                    throw new ArgumentException("No service owner found..");

                return Request.CreateResponse(HttpStatusCode.OK, $"Shipping of {archiveReference} initiated");
            }
            catch (ArgumentException aex)
            {
                _logger.LogError(aex, "Error in ProcessShipment");
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, aex.Message);
            }
        }
    }
}