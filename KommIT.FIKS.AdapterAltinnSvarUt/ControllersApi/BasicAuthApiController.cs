﻿using KommIT.FIKS.AdapterAltinnSvarUt.Services.Authentication;
using System;
using System.Web.Http;
using System.Web.Http.Description;

namespace KommIT.FIKS.AdapterAltinnSvarUt.ControllersApi
{
    [ApiExplorerSettings(IgnoreApi = true)]
    public class BasicAuthApiController : ApiController
    {
        private readonly ITokenService _tokenService;
        private readonly double _tokenExpirationMinutes = 30;

        public BasicAuthApiController(ITokenService tokenService)
        {
            _tokenService = tokenService;
            _tokenExpirationMinutes = double.Parse(System.Configuration.ConfigurationManager.AppSettings["Authentication:TokenGeneration:TokenExpirationMinutes"]);
        }

        [BasicAuthentication]
        [Route("api/authenticate")]
        [HttpGet]
        public IHttpActionResult Authenticate()
        {
            var token = _tokenService.GenerateToken(DateTime.UtcNow.AddMinutes(_tokenExpirationMinutes));

            return Ok(new { Token = token });
        }
    }
}