﻿using KommIT.FIKS.AdapterAltinnSvarUt.Services.Samples;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;

namespace KommIT.FIKS.AdapterAltinnSvarUt.ControllersApi
{
    [ApiExplorerSettings(IgnoreApi = true)]
    [EnableCors(origins: "*", headers: "*", methods: "*", exposedHeaders: "x-correlation-id")]
    public class SamplesController : ApiController
    {
        /// <summary>
        /// Hente ut eksempel skjema varianter
        /// </summary>
        /// <returns>Skjemavarianter</returns>
        /// <response code="200">ok</response>
        /// <response code="400">BadRequest - ugyldig forespørsel</response>
        /// <response code="404">NotFound - finner ikke</response>
        /// <response code="500">Error - intern feil</response>
        [Route("api/skjemaeksempel/{dataformatid}/{dataformatversion}")]
        [HttpGet]
        [ResponseType(typeof(SampleVariant[]))]
        public HttpResponseMessage GetSampleVariants(string dataformatid, string dataformatversion)
        {
            List<SampleVariant> skjemavariantList = new List<SampleVariant>();
            try
            {
                //_logger.Debug("Sjekkliste API kall mottatt");

                skjemavariantList = new SampleService().GetSampleVariants(dataformatid, dataformatversion);

                return Request.CreateResponse(HttpStatusCode.OK, skjemavariantList.ToArray());
            }
            catch (Exception ex)
            {
                //_logger.Error($"Exception i SampleVariants API kall: {ex.ToString()}, {ex.Message}, {ex.StackTrace}");
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        /// <summary>
        /// Hente ut eksempel skjema
        /// </summary>
        /// <returns>Skjemaeksempel</returns>
        /// <response code="200">ok</response>
        /// <response code="400">BadRequest - ugyldig forespørsel</response>
        /// <response code="404">NotFound - finner ikke</response>
        /// <response code="500">Error - intern feil</response>
        [Route("api/skjemaeksempelvariant/{dataformatid}/{dataformatversion}/{variantid}")]
        [HttpGet]
        [ResponseType(typeof(Sample))]
        public HttpResponseMessage GetSample(string dataformatid, string dataformatversion, string variantid)
        {
            // List<Skjemavariant> skjemavariantList = new List<Skjemavariant>();
            try
            {
                //_logger.Debug("Sjekkliste API kall mottatt");

                Sample skjema = new SampleService().GetSample(dataformatid, dataformatversion, variantid, "", "");

                return Request.CreateResponse(HttpStatusCode.OK, skjema);
            }
            catch (Exception ex)
            {
                //_logger.Error($"Exception i SampleVariants API kall: {ex.ToString()}, {ex.Message}, {ex.StackTrace}");
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }
    }
}