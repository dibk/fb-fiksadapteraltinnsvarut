﻿using KommIT.FIKS.AdapterAltinnSvarUt.Services.HealthChecks;
using Serilog;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;

namespace KommIT.FIKS.AdapterAltinnSvarUt.ControllersApi
{
    [ApiExplorerSettings(IgnoreApi = true)]
    public class HealthApiController : ApiController
    {
        private readonly IHealthService _healthService;
        private readonly ILogger _logger;

        public HealthApiController(IHealthService healthService, ILogger logger)
        {
            _healthService = healthService;
            _logger = logger.ForContext<HealthApiController>();
        }

        [Route("api/healthcheck")]
        [HttpGet]
        public IHttpActionResult CheckHealth()
        {
            var healthResult = _healthService.PerformHealthChecks();

            return Ok(healthResult);
        }

        [Route("api/healthcheck/{name}")]
        [HttpGet]
        public IHttpActionResult CheckHealth(string name)
        {
            var healthResult = _healthService.PerformHealthChecks();

            var data = healthResult.FirstOrDefault(d => d.Name == name);

            if (data != null)
                return Ok(data);
            return BadRequest("Name is not in list");
        }
    }
}