using KommIT.FIKS.AdapterAltinnSvarUt.Models.ApiModels.Internal;
using KommIT.FIKS.AdapterAltinnSvarUt.Models.Signatur;
using KommIT.FIKS.AdapterAltinnSvarUt.Services;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Azure;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Email;
using no.kxml.skjema.dibk.tiltakshaversSignatur;
using Serilog;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Http.Description;

namespace KommIT.FIKS.AdapterAltinnSvarUt.ControllersApi
{
    [IdPortenAuthorize]
    [AllowedOriginsPolicy("AllowedOrigins:TiltakshaverSignatur")]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class TiltakshaverSignaturInternalController : TiltakshaverSignaturBaseController
    {
        private readonly ILogger _logger;
        private readonly IFileStorage _fileStorage;

        public TiltakshaverSignaturInternalController(ITiltakshaverSamtykkeService samtykkeService, ILogger logger, IFileStorage fileStorage) : base(samtykkeService)
        {
            _logger = logger;
            _fileStorage = fileStorage;
        }

        [Route("api/internal/samtykketiltakshaver/{id}/form")]
        [HttpGet]
        [ResponseType(typeof(TiltakshaversSignaturType))]
        public HttpResponseMessage GetFormModel(string id)
        {
            Context context;
            if (!IsInputValid(id, out context))
                return context.Reason;
            //Hvis status er signert så er alt veiviser ferdig -> feilmelding

            var samtykke = _samtykkeService.GetTiltakshaversSamtykke(context.SamtykkeId);

            if (samtykke == null)
                return Request.CreateResponse(HttpStatusCode.NotFound);

            var samtykkeTiltakshaver = SamtykkeTiltakshaver.Map(samtykke);

            var tiltakshaversSignatur = TiltakshaversSignaturTypeMapper.Map(samtykkeTiltakshaver);
            return Request.CreateResponse(HttpStatusCode.OK, tiltakshaversSignatur);
        }

        /// <summary>
        /// Oppdater status og signatur på samtykke tiltakshaver
        /// </summary>
        /// <response code="200">ok</response>
        /// <response code="400">BadRequest - ugyldig forespørsel</response>
        /// <response code="403">Forbidden - ingen tilgang</response>
        /// <response code="500">Error - intern feil</response>
        [Route("api/internal/samtykketiltakshaver/{id}/oppdater")] //Obsolete route, slett når frontend er oppdatert til å bruke den andre routen.
        [Route("api/internal/samtykketiltakshaver/{id}/signert")]
        [HttpPost]
        public HttpResponseMessage SamtykkeSignert(string id, SamtykkeTiltakshaverOppdater samtykkePayload)
        {
            if (samtykkePayload != null)
            {
                try
                {
                    Context context;
                    if (!IsInputValid(id, out context))
                        return context.Reason;

                    if (id?.ToLower() != samtykkePayload.Id?.ToLower())
                        return Request.CreateResponse(HttpStatusCode.BadRequest, $"Ressursens id {id} korresponderer ikke med id i payload {samtykkePayload.Id}");

                    if (context.TiltakshaversSamtykke.Status != TiltakshaverSamtykkeConstants.Opprettet)
                        return Request.CreateResponse(HttpStatusCode.BadRequest, "Samtykke er allerede signert");

                    _samtykkeService.SamtykkeGodkjent(context.SamtykkeId, samtykkePayload.AltinnArkivreferanse, samtykkePayload.SignertTiltakshaverOrgnr);

                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);

                    return response;
                }
                catch (Exception ex)
                {
                    _logger.Error(ex, "Error occurred when registering samtykke as godkjent");
                    return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
                }
            }
            return Request.CreateResponse(HttpStatusCode.BadRequest, "finner ingen informasjon for å oppdatere samtykke");
        }

        [Route("api/internal/samtykketiltakshaver/{id}/receipt")]
        [HttpPost]
        public HttpResponseMessage SendReceiptEmail(string id, [FromBody] ReceiptEmail receiptEmail)
        {
            Context context;
            if (!IsInputValid(id, out context))
                return context.Reason;

            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);

            _logger.Information("Send samtykketiltakshaver {sabyId} receipt to {ToAddress}", id, receiptEmail.ToEmailAddress);
            var client = new EmailClient();
            client.SendEmail(receiptEmail.ToEmailAddress, receiptEmail.Subject, receiptEmail.HtmlBody);

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [Route("api/internal/samtykketiltakshaver/{id}/vedlegg/{vedleggId}")]
        [HttpGet]
        public HttpResponseMessage GetFile(string id, int vedleggId)
        {
            try
            {
                Context context;
                if (!IsInputValid(id, out context))
                    return context.Reason;

                if (!_fileStorage.IsFolderPresent(id))
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, $"Can't find any blob container with name:'{id}'");

                var vedlegg = _samtykkeService.GetVedlegg(id, vedleggId);
                if (vedlegg == null)
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, $"Can't find vedlegg for id:'{id}' and vedleggId: '{vedleggId}'");

                //  create a local file
                using (MemoryStream stream = new MemoryStream())
                {
                    string fileName = vedlegg.Navn;
                    _fileStorage.StreamFileFromLocation(vedlegg.Referanse, stream);

                    // processing the stream.
                    var result = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new ByteArrayContent(stream.ToArray())
                    };
                    result.Content.Headers.ContentDisposition =
                        new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
                        {
                            FileName = fileName
                        };
                    result.Content.Headers.ContentType =
                        new MediaTypeHeaderValue("application/octet-stream");
                    return result;
                }
            }
            catch (Exception e)
            {
                _logger.Error(e, "An error occurred when retrieving vedlegg");
                return Request.CreateResponse(HttpStatusCode.BadRequest, e);
            }
        }
    }
}