using KommIT.FIKS.AdapterAltinnSvarUt.Models.ApiModels.Internal;
using KommIT.FIKS.AdapterAltinnSvarUt.Models.Signatur;
using KommIT.FIKS.AdapterAltinnSvarUt.Services;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Azure;
using Serilog;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;

namespace KommIT.FIKS.AdapterAltinnSvarUt.ControllersApi
{
    [AllowedOriginsPolicy("AllowedOrigins:TiltakshaverSignatur")]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class TiltakshaverSignaturInternalNoAuthController : TiltakshaverSignaturBaseController
    {
        private readonly ILogger _logger;
        private readonly IFileStorage _fileStorage;

        public TiltakshaverSignaturInternalNoAuthController(ITiltakshaverSamtykkeService samtykkeService, ILogger logger, IFileStorage fileStorage) : base(samtykkeService)
        {
            _logger = logger;
            _fileStorage = fileStorage;
        }

        /// <summary>
        /// Hente hoveddata for samtykke tiltakshaver
        /// </summary>
        /// <param name="id">id for et samtykke av tiltakshaver</param>
        /// <returns></returns>
        /// <response code="200">ok</response>
        /// <response code="400">BadRequest - ugyldig forespørsel</response>
        /// <response code="404">NotFound - finner ikke samtykke</response>
        /// <response code="403">Forbidden - ingen tilgang</response>
        /// <response code="500">Error - intern feil</response>
        [AllowAnonymous]
        [Route("api/internal/samtykketiltakshaver/{id}")]
        [HttpGet]
        [ResponseType(typeof(SamtykkeTiltakshaver))]
        public HttpResponseMessage GetSamtykkeTiltakshaver(string id)
        {
            Context context;
            if (!IsInputValid(id, out context))
                return context.Reason;

            //Hvis status er signert så er alt veiviser ferdig -> feilmelding
            var status = _samtykkeService.GetTiltakshaversSamtykke(context.SamtykkeId);

            if (status == null)
                return Request.CreateResponse(HttpStatusCode.NotFound);

            var retur = SamtykkeTiltakshaver.Map(status);

            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, retur);

            return response;
        }

        /// <summary>
        /// Convert main form to Tiltakshaver samtykke  model
        /// </summary>
        /// <param name="samtykkeTiltakshaverXmlData"></param>
        /// <returns></returns>
        /// <response code="200">ok</response>
        /// <response code="400">BadRequest - ugyldig forespørsel</response>
        /// <response code="403">Forbidden - ingen tilgang</response>
        /// <response code="500">Error - intern feil</response>
        [Route("api/internal/convertXmlMainFormToTiltakshaverSamtykke")]
        [HttpPost]
        [ResponseType(typeof(TiltakshaverSamtykkeAbstractClass))]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public HttpResponseMessage ConvertMainFormToTiltakshaverSamtykke(SamtykkeTiltakshaverXmlData samtykkeTiltakshaverXmlData)
        {
            if (samtykkeTiltakshaverXmlData != null)
            {
                _logger.Debug($"API convertXmlMainFormToTiltakshaverSantikke request for form DataFormatId {samtykkeTiltakshaverXmlData.DataFormatId} and DataFormatVersion {samtykkeTiltakshaverXmlData.DataFormatVersion}");

                var dataFormatIdDataFormatVersion = $"{samtykkeTiltakshaverXmlData.DataFormatId}:{samtykkeTiltakshaverXmlData.DataFormatVersion}";
                try
                {
                    TiltakshaverSamtykkeBase tiltakshaverSamtykkeBase = new TiltakshaversamtykkeFactory().GetTiltakshaverSamtykkeBase(dataFormatIdDataFormatVersion, samtykkeTiltakshaverXmlData.XmlData);

                    if (tiltakshaverSamtykkeBase != null)
                    {
                        var tiltakshaverSamtykkeModel = tiltakshaverSamtykkeBase.GeTiltakshaverSamtykkeModel();

                        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, tiltakshaverSamtykkeModel);
                        _logger.Debug($"API convertXmlMainFormToTiltakshaverSantikke request for form DataFormatId {samtykkeTiltakshaverXmlData.DataFormatId} and DataFormatVersion {samtykkeTiltakshaverXmlData.DataFormatVersion}: OK");
                        return response;
                    }

                    return Request.CreateResponse(HttpStatusCode.BadRequest, "Can't find DataFormatId , DataFormatVersion to convert");
                }
                catch (Exception ex)
                {
                    //TODO SEND exception
                    _logger.Error($"API convertXmlMainFormToTiltakshaverSantikke request for form DataFormatId {samtykkeTiltakshaverXmlData.DataFormatId} and DataFormatVersion {samtykkeTiltakshaverXmlData.DataFormatVersion} Exception:");
                    _logger.Error($"{ex.Message}");
                    _logger.Error($"{ex.StackTrace}");

                    return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Wrong xml or DataFormatId/DataFormatVersion, cannot parse");
                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }
    }
}