using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Models.ApiModels;
using KommIT.FIKS.AdapterAltinnSvarUt.Models.Signatur;
using KommIT.FIKS.AdapterAltinnSvarUt.Services;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Authentication;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Azure;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;
using Serilog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;

namespace KommIT.FIKS.AdapterAltinnSvarUt.ControllersApi
{
    [BasicAuthentication]
    [EnableCors(origins: "*", headers: "*", methods: "*", exposedHeaders: "x-correlation-id")]
    public class TiltakshaverSignaturController : TiltakshaverSignaturBaseController
    {
        private readonly ILogger _logger;
        private readonly IFileDownloadStatusService _fileDownloadStatusService;
        private readonly IFileStorage _fileStorage;
        private readonly string _sabyClientHost;

        public TiltakshaverSignaturController(ITiltakshaverSamtykkeService samtykkeService,
                                              ILogger logger,
                                              IFileDownloadStatusService fileDownloadStatusService,
                                              IFileStorage fileStorage) : base(samtykkeService)
        {
            _fileDownloadStatusService = fileDownloadStatusService;
            _fileStorage = fileStorage;
            _logger = logger;
            _sabyClientHost = ConfigurationManager.AppSettings["Tiltakshaverssignatur:ClientUrl"];
        }

        /// <summary>
        /// Oppretter hoveddata for veiviser til tiltakshavers samtykke til byggesøknad
        /// </summary>
        /// <response code="200">Ok - Tiltakshavers samtykke opprettet</response>
        /// <response code="400">BadRequest - ugyldig forespørsel</response>
        /// <response code="403">Forbidden - ingen tilgang</response>
        /// <response code="500">Error - intern feil</response>
        /// <returns>Id for det opprettede tiltakshavers samtykke</returns>
        [Route("api/signatur/samtykketiltakshaver/ny")]
        [HttpPost]
        public HttpResponseMessage PostTiltakshaver(SamtykkeTiltakshaverNy samtykkeTiltakshaverNy)
        {
            try
            {
                if (samtykkeTiltakshaverNy != null)
                {
                    //POST Tiltakshavers signatur - input hoveddata fra søknaden - retur guid
                    if (ModelState.IsValid)
                    {
                        Guid nyId = _samtykkeService.AddNew(samtykkeTiltakshaverNy);
                        string id = nyId.ToString();
                        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, id);
                        if (string.IsNullOrEmpty(_sabyClientHost))
                            response.Headers.Location = new Uri(Request.RequestUri.Scheme + "://" + Request.RequestUri.Authority + "/Signatur/Tiltakshaver/" + id);
                        else
                            response.Headers.Location = new Uri($"{_sabyClientHost}/{id}");

                        _logger.Information("SaBy API Tiltakshaverssamtykke oprettet med id {sabyId}", id);

                        return response;
                    }
                    else
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "Ikke nok data for å starte tiltaksvaver samtykke");
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Error occurred when registering a new samtykke tiltaksaver");
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        /// <summary>
        /// Opplasting av vedlegg
        /// </summary>
        /// <param name="id">referanse til samtykke id</param>
        /// <param name="vedleggstype">altinn vedleggstype kode</param>
        /// <param name="filnavn">filnavn</param>
        /// <response code="200">Ok - fil lagt til samtykket</response>
        /// <response code="400">BadRequest - ugyldig forespørsel</response>
        /// <response code="403">Forbidden - ingen tilgang</response>
        /// <response code="404">NotFound - finner ikke tiltakshaverssamtykke for id</response>
        /// <response code="500">Error - intern feil</response>
        [Route("api/signatur/samtykketiltakshaver/streamvedlegg/{id}/{vedleggstype}/{filnavn}")]
        [HttpPost]
        public async Task<HttpResponseMessage> PostFile(string id, string vedleggstype, string filnavn)
        {
            Context context;
            if (!IsInputValid(id, out context))
                return context.Reason;

            if (string.IsNullOrEmpty(vedleggstype) || string.IsNullOrEmpty(filnavn))
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            // Check if the request contains multipart/form-data.
            if (!Request.Content.IsMimeMultipartContent())
            {
                _logger.Error($"API streamvedlegg can't find multipart/form-data");
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            try
            {
                _logger.Information("SaBy API streamvedlegg vedlegg id {id} - vedlegg type {vedleggsType}", context.SamtykkeId, vedleggstype);

                var provider = new MultipartMemoryStreamProvider();

                //TODO vedleggstype må være en av de lovlige i altinn for denne tjenesten GetMetadata sjekk?
                // Read the form data and return an async task.

                _logger.Debug("Reading content as multipart async - Starting");
                await Request.Content.ReadAsMultipartAsync(provider);
                _logger.Debug("Reading content as multipart async - Done");

                foreach (var fileData in provider.Contents)
                {
                    try
                    {
                        //For å unngå overskriving ved opplasting av fil med samme navn
                        // legges filene i mapper i blobstorage
                        //get vedlegg-id for å anngi mappenavn

                        _logger.Debug("Creates row for attachment in database");
                        int vedleggRowId = _samtykkeService.PrepareVedleggRow(context.SamtykkeId);

                        var filename = fileData.Headers.ContentDisposition.FileName.Trim('"');
                        var mimetype = fileData.Headers.ContentType.ToString();

                        var filenameWithFolder = $"{vedleggRowId}/{filename}";

                        long fileSize;
                        string uri = string.Empty;

                        using (var stream = await fileData.ReadAsStreamAsync())
                        {
                            _logger.Debug("Uploading attachment to Azure Blob Storage");
                            uri = _fileStorage.AddFileAsStreamToFolder(context.SamtykkeId.ToString(), filenameWithFolder, stream, mimetype);

                            fileSize = stream.Length;
                        }

                        //legge inn referanse til vedlegg
                        _logger.Debug("Adding attachment information to database");
                        _samtykkeService.AddVedlegg(context.SamtykkeId, vedleggRowId, filnavn, vedleggstype, uri, fileSize);
                    }
                    catch (Exception e)
                    {
                        _logger.Error(e, "API streamvedlegg Unable to upload attachment to Azure Blob Storage {fileName}", filnavn);
                        return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                _logger.Error(e, "API streamvedlegg Unable to complete request");
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }

        /// <summary>
        /// Sletting av opplastet vedlegg
        /// </summary>
        /// <param name="id">referanse til samtykke id</param>
        /// <param name="vedleggId">referanse til opplastet vedlegg</param>
        /// <response code="200">Ok - fil slettet</response>
        /// <response code="400">BadRequest - ugyldig forespørsel</response>
        /// <response code="403">Forbidden - ingen tilgang</response>
        /// <response code="404">NotFound - finner ikke tiltakshaverssamtykke for id</response>///
        /// <response code="500">Error - intern feil</response>
        [Route("api/signatur/samtykketiltakshaver/streamvedlegg/{id}/{vedleggId}")]
        [HttpDelete]
        public HttpResponseMessage DeleteFile(string id, int vedleggId)
        {
            Context context;
            if (!IsInputValid(id, out context))
                return context.Reason;

            if (context.TiltakshaversSamtykke.Status != TiltakshaverSamtykkeConstants.Opprettet)
                return Request.CreateResponse(HttpStatusCode.BadRequest, $"Cannot delete file when status is '{TiltakshaverSamtykkeConstants.GetStringValue(context.TiltakshaversSamtykke.Status)}' for TiltakshaversSamtykke");

            if (vedleggId == default(int))
                return Request.CreateResponse(HttpStatusCode.BadRequest, $"Invalid vedlegg id '{vedleggId}' for TiltakshaversSamtykke");

            if (!context.TiltakshaversSamtykke.Vedlegg.Any(p => p.VedleggId == vedleggId))
                return Request.CreateResponse(HttpStatusCode.BadRequest, $"TiltakshaversSamtykke has no attachment with vedleggId {vedleggId}");

            try
            {
                var vedlegg = context.TiltakshaversSamtykke.Vedlegg.First(p => p.VedleggId == vedleggId);

                var success = _fileStorage.DeleteFileFromLocation(vedlegg.Referanse);
                if (success)
                    _samtykkeService.DeleteVedlegg(context.SamtykkeId, vedleggId);

                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                _logger.Error(e, "API streamvedlegg Unable to complete request");
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }

        /// <summary>
        /// Hente ut status på samtykke tiltakshaver
        /// </summary>
        /// <param name="id">id for et samtykke av tiltakshaver</param>
        /// <returns></returns>
        /// <response code="200">Ok</response>
        /// <response code="400">BadRequest - ugyldig forespørsel</response>
        /// <response code="403">Forbidden - ingen tilgang</response>
        /// <response code="404">NotFound - finner ikke tiltakshaverssamtykke for id</response>
        /// <response code="500">Error - intern feil</response>
        [Route("api/signatur/samtykketiltakshaver/status/{id}")]
        [HttpGet]
        [ResponseType(typeof(SamtykkeTiltakshaverStatus))]
        public HttpResponseMessage GetSamtykkeStatus(string id)
        {
            _logger.Verbose($"API request: GetSamtykkeStatus on samtykketiltakshaver Id {id}");

            Context context;
            try
            {
                if (!IsInputValid(id, out context))
                    return context.Reason;
            }
            catch (Exception ex)
            {
                _logger.Error(ex,
                    $"API request: GetSamtykkeStatus on TiltakshaversSamtykkeId {id} {HttpStatusCode.InternalServerError} EXCEPTION {ex.ToString()}");
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }

            var samtykkeTiltakshaverStatus = new SamtykkeTiltakshaverStatus().Map(context.TiltakshaversSamtykke);

            if (context.TiltakshaversSamtykke.Status == TiltakshaverSamtykkeConstants.Signert
                || context.TiltakshaversSamtykke.Status == TiltakshaverSamtykkeConstants.Arkivert)
            {
                var arkivreferanse = context.TiltakshaversSamtykke.AltinnArkivreferanse;

                if (string.IsNullOrEmpty(arkivreferanse))
                    return Request.CreateResponse(HttpStatusCode.BadRequest, $"Kan ikke finner noen altinn arkivreferanse med disse Tiltakshavers Samtykke Id:'{id}'");

                List<FileDownloadStatus> filesStatus = null;
                try
                {
                    filesStatus = _fileDownloadStatusService.GetFileDownloadStatusesForArchiveReference(arkivreferanse)?.Where(p => p.IsDeleted == false).ToList();
                }
                catch (Exception ex)
                {
                    _logger.Error($"API request: GetSamtykkeStatus Can't get information of files from AltinArchiveReference {arkivreferanse} {HttpStatusCode.InternalServerError} EXCEPTION {ex.ToString()}");
                    return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
                }

                if (filesStatus != null && filesStatus.Any())
                {
                    var uriHelpoer = new UtilHelpers();
                    if (filesStatus.Any(f => f.FileType == FilTyperForNedlasting.SkjemaPdf))
                    {
                        samtykkeTiltakshaverStatus.SamtykketiltakshaverPdf = uriHelpoer.GenerateDownloadApiUri(filesStatus.First(f => f.FileType == FilTyperForNedlasting.SkjemaPdf));
                    }
                    if (filesStatus.Any(f => f.FileType == FilTyperForNedlasting.MaskinlesbarXml))
                    {
                        samtykkeTiltakshaverStatus.SamtykketiltakshaverXml = uriHelpoer.GenerateDownloadApiUri(filesStatus.First(f => f.FileType == FilTyperForNedlasting.MaskinlesbarXml));
                    }
                }
            }

            return Request.CreateResponse(HttpStatusCode.OK, samtykkeTiltakshaverStatus);
        }
    }
}