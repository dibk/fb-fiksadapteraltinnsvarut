﻿using KommIT.FIKS.AdapterAltinnSvarUt.Models.Signatur;
using KommIT.FIKS.AdapterAltinnSvarUt.Services;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace KommIT.FIKS.AdapterAltinnSvarUt.ControllersApi
{
    public class TiltakshaverSignaturBaseController : ApiController
    {
        internal readonly ITiltakshaverSamtykkeService _samtykkeService;

        public TiltakshaverSignaturBaseController(ITiltakshaverSamtykkeService samtykkeService)
        {
            _samtykkeService = samtykkeService;
        }

        internal bool IsInputValid(string samtykkeId, out Context context)
        {
            context = new Context();
            if (string.IsNullOrWhiteSpace(samtykkeId))
            {
                context.Reason = Request.CreateResponse(HttpStatusCode.BadRequest, $"Invalid Id '{samtykkeId}' for TiltakshaversSamtykke");
                return false;
            }

            Guid tiltakshaversSamtykkeId;
            if (!Guid.TryParse(samtykkeId, out tiltakshaversSamtykkeId))
                context.Reason = Request.CreateResponse(HttpStatusCode.BadRequest, $"Invalid Id '{samtykkeId}' for TiltakshaversSamtykke");

            context.SamtykkeId = tiltakshaversSamtykkeId;

            context.TiltakshaversSamtykke = _samtykkeService.GetTiltakshaversSamtykke(samtykkeId);
            if (context.TiltakshaversSamtykke == null)
            {
                context.Reason = Request.CreateResponse(HttpStatusCode.NotFound, $"Invalid TiltakshaversSamtykkeId '{tiltakshaversSamtykkeId}'");
                return false;
            }

            return true;
        }

        internal class Context
        {
            public Guid SamtykkeId { get; set; }
            public TiltakshaversSamtykke TiltakshaversSamtykke { get; set; }
            public HttpResponseMessage Reason { get; set; }
        }
    }
}