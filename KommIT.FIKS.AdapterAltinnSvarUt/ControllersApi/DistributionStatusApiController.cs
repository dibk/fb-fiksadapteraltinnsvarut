﻿using KommIT.FIKS.AdapterAltinnSvarUt.Services;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace KommIT.FIKS.AdapterAltinnSvarUt.ControllersApi
{
    [ApiExplorerSettings(IgnoreApi = true)]
    public class DistributionStatusApiController : ApiController
    {
        public readonly IDistributionStatusService _shipmentStatusService;

        public DistributionStatusApiController(IDistributionStatusService shipmentStatusService)
        {
            _shipmentStatusService = shipmentStatusService;
        }

        [Route("api/distributionstatus/{ar}")]
        [HttpGet]
        public HttpResponseMessage GetDistributionStatus(string ar)
        {
            var result = _shipmentStatusService.GetStatusFor(ar);

            return Request.CreateResponse(System.Net.HttpStatusCode.OK, result);
        }
    }
}