﻿using KommIT.FIKS.AdapterAltinnSvarUt.Services.Encryption;
using Serilog;
using System;
using System.Configuration;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace KommIT.FIKS.AdapterAltinnSvarUt.ControllersApi
{
    public class DevToolsApiController : ApiController
    {
        private readonly ILogger _logger;
        private readonly string _runInTesMode;

        public DevToolsApiController(ILogger logger)
        {
            _logger = logger;
            _runInTesMode = ConfigurationManager.AppSettings["RunInTestMode"];
        }

        /// <summary>
        /// Encrypt a string on test server only
        /// </summary>
        /// <param name="cleartext">text for encryption</param>
        /// <returns></returns>
        /// <response code="200">ok</response>
        /// <response code="400">BadRequest - ugyldig forespørsel</response>
        /// <response code="500">Error - intern feil</response>
        [Route("api/GetEncryptedString/{cleartext}")]
        [HttpGet]
        [ResponseType(typeof(string))]
        [ApiExplorerSettings(IgnoreApi = true)]
        public HttpResponseMessage TestGetEncryptedString(string cleartext)
        {
            if (_runInTesMode != "true")
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "Ikke tilgjenglig i prod miljø");
            }

            _logger.Verbose("api/GetEncryptedString: Getting encrypted text");

            if (!String.IsNullOrEmpty(cleartext))
            {
                try
                {
                    //Decryption decrypt = new Decryption();
                    string chiperText = Decryption.Instance.EncryptText(cleartext);

                    if (String.IsNullOrEmpty(chiperText))
                    {
                        _logger.Error("api/GetEncryptedString: Could not encrypt");
                        return Request.CreateResponse(HttpStatusCode.InternalServerError,
                            "Did not get an encryption result");
                    }
                    else
                    {
                        _logger.Verbose("api/GetEncryptedString: Encrypted text");
                        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, chiperText);
                        return response;
                    }
                }
                catch (Exception ex)
                {
                    _logger.Error(
                        $"api/GetEncryptedString: {HttpStatusCode.InternalServerError} EXCEPTION {ex.ToString()}");
                    return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
                }
            }
            else
            {
                _logger.Error($"api/GetEncryptedString: No cleartext given {HttpStatusCode.BadRequest}");
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Decrypt a string on test server only
        /// </summary>
        /// <param name="ciphertext">encrypted text</param>
        /// <returns></returns>
        /// <response code="200">ok</response>
        /// <response code="400">BadRequest - ugyldig forespørsel</response>
        /// <response code="500">Error - intern feil</response>
        [Route("api/GetDecryptedString")]
        [HttpPost]
        [ResponseType(typeof(string))]
        [ApiExplorerSettings(IgnoreApi = true)]
        public async System.Threading.Tasks.Task<HttpResponseMessage> TestGetDecryptedStringAsync()
        {
            if (_runInTesMode != "true")
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "Ikke tilgjenglig i prod miljø");
            }

            _logger.Verbose("api/GetDecryptedString: Getting encrypted text");

            try
            {
                string ciphertext = await Request.Content.ReadAsStringAsync();

                //Decryption decrypt = new Decryption();
                string clearText = Decryption.Instance.DecryptText(ciphertext);

                if (String.IsNullOrEmpty(clearText))
                {
                    _logger.Error("api/GetDecryptedString: Could not decrypt");
                    return Request.CreateResponse(HttpStatusCode.InternalServerError,
                        "Did not get an encryption result");
                }
                else
                {
                    _logger.Verbose("api/GetDecryptedString: Decrypted text");
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, clearText);
                    return response;
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"api/GetDecryptedString: {HttpStatusCode.InternalServerError} EXCEPTION {ex.ToString()}");
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }
    }
}