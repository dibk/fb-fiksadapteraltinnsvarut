using KommIT.FIKS.AdapterAltinnSvarUt.Services.SearchEngine;
using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;

namespace KommIT.FIKS.AdapterAltinnSvarUt.ControllersApi
{
    [ApiExplorerSettings(IgnoreApi = true)]
    [Authorize(Roles = ApplicationRole.Administrator)]
    public class StatisticsController : ApiController
    {
        private readonly IStatisticsService _statisticsService;
        private readonly IBatchIndexer _batchIndexer;

        public StatisticsController(IStatisticsService statisticsService, IBatchIndexer batchIndexer)
        {
            _statisticsService = statisticsService;
            _batchIndexer = batchIndexer;
        }

        [HttpGet]
        [Route("api/statistikk")]
        public IHttpActionResult Index()
        {
            Dictionary<string, string> response = _statisticsService.GetNumberOfFormsGroupedByType();
            return Json(response);
        }

        [HttpGet]
        [Route("api/statistikk/logg")]
        public IHttpActionResult LogEntries()
        {
            Dictionary<string, string> response = _statisticsService.GetNumberOfLogEntryTypes();
            return Json(response);
        }

        /// <summary>
        /// Start background index job of form metadata.
        /// </summary>
        /// <returns>Http status code 204 when job has started.</returns>
        [HttpGet]
        [Route("api/statistikk/indekser-metadata")]
        public IHttpActionResult IndexMetadata()
        {
            _batchIndexer.StartBatchIndexingFormMetadataJob();
            return StatusCode(HttpStatusCode.NoContent);
        }

        /// <summary>
        /// Start background index job of log entries.
        /// </summary>
        /// <returns>Http status code 204 when job has started.</returns>
        [HttpGet]
        [Route("api/statistikk/indekser-logg")]
        public IHttpActionResult IndexLogEntries()
        {
            _batchIndexer.StartBatchIndexingLogEntriesJob();
            return StatusCode(HttpStatusCode.NoContent);
        }

        /// <summary>
        /// Start background index job of applicationlog entries.
        /// </summary>
        /// <returns>Http status code 204 when job has started.</returns>
        [HttpGet]
        [Route("api/statistikk/indekser-Applicationlog")]
        public IHttpActionResult IndexApplicationLog()
        {
            _batchIndexer.StartBatchIndexingApplicationLogJob();
            return StatusCode(HttpStatusCode.NoContent);
        }

        /// <summary>
        /// Start background index job of distribution forms.
        /// </summary>
        /// <returns>Http status code 204 when job has started.</returns>
        [HttpGet]
        [Route("api/statistikk/indekser-distribusjoner")]
        public IHttpActionResult IndexDistributionForms()
        {
            _batchIndexer.StartBatchIndexingDistributionFormsJob(new DateTime(DateTime.Now.Year, 1, 1));
            return StatusCode(HttpStatusCode.NoContent);
        }
    }
}