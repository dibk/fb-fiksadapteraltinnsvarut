﻿using System.Net;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Authentication
{
    public class AuthenticationResult
    {
        public HttpStatusCode HttpStatusCode { get; set; }
        public string ResultText { get; set; }
        public bool IsResultOk { get; set; }

        public AuthenticationResult(HttpStatusCode httpStatusCode, string resultText, bool isResultOk)
        {
            HttpStatusCode = httpStatusCode;
            ResultText = resultText;
            IsResultOk = isResultOk;
        }
    }
}