﻿using System;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Authentication
{
    public interface ITokenService
    {
        string GenerateToken(DateTime expirationUtcTime);
    }
}