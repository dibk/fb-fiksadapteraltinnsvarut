﻿using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Authentication
{
    public class AuthorizationService : IAuthorizationService
    {
        private readonly ApplicationUserManager _userManager;
        private readonly IFormMetadataService _formMetadataService;
        private readonly ClientSystemService _clientSystemService;
        private readonly string _claimType = "clientsystem";

        public AuthorizationService(ApplicationUserManager userManager, IFormMetadataService formMetadataService, ClientSystemService clientSystemService)
        {
            _userManager = userManager;
            _formMetadataService = formMetadataService;
            _clientSystemService = clientSystemService;
        }

        public bool UserHasAccess(string archiveReference)
        {
            return UserHasAccess(_formMetadataService.GetFormMetadata(archiveReference));
        }

        public bool UserHasAccess(FormMetadata formMetaData)
        {
            if (!UserAccessCheckEnabled()) //Temporary solution to disable user access check. Should be removed when all users have been assigned access.
                return true;

            var currentPrincipal = HttpContext.Current.User as ClaimsPrincipal;

            if (UserHasAdministratorRole(currentPrincipal.Identity.GetUserId()))
                return true;

            //Get claim type values
            var claims = currentPrincipal.Claims.Where(c => c.Type == _claimType).ToList();
            if (claims.Count > 0)
            {
                var claimValues = claims.Select(p => p.Value).ToList();
                if (claimValues.Count == 0)
                    return false;

                //List of client system ids
                var configuredClientSystemIds = _clientSystemService.GetClientSystems().Where(p => claimValues.Contains(p.SystemKey));

                char[] separator = { ';' };
                var clientSystemIds = new List<string>();
                foreach (var item in configuredClientSystemIds) //Split and trim client system ids
                {
                    clientSystemIds.AddRange(item.Identifiers.Split(separator, StringSplitOptions.RemoveEmptyEntries).Select(q => q.Trim()).ToList());
                }

                return clientSystemIds.Exists(p => p.Equals(formMetaData.Application, StringComparison.OrdinalIgnoreCase)); //Check if user has access to the form
            }

            return false;
        }

        private bool UserAccessCheckEnabled()
        {
            var userAccessCheckEnabled = System.Configuration.ConfigurationManager.AppSettings["Autorization:UserAccessCheckEnabled"];

            if (bool.TryParse(userAccessCheckEnabled, out bool result))
                return result;

            return false;
        }

        public bool UserHasAdministratorRole(string userId)
        {
            return AsyncHelper.RunSync(async () => await _userManager.IsInRoleAsync(userId, ApplicationRole.Administrator));
        }

        public bool UserHasApiAdministratorRole(string userId)
        {
            return AsyncHelper.RunSync(async () => await _userManager.IsInRoleAsync(userId, ApplicationRole.SystemIntegrator));
        }
    }
}