﻿using Microsoft.AspNet.Identity;
using System;
using System.Configuration;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Authentication
{
    [AttributeUsage(validOn: AttributeTargets.Class | AttributeTargets.Method)]
    public class BasicAuthenticationAttribute : AuthorizeAttribute
    {
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            bool runInTestMode;
            bool.TryParse(ConfigurationManager.AppSettings["LocalDevelopmentTestMode"], out runInTestMode);

            if (!runInTestMode)
                using (var scope = GlobalConfiguration.Configuration.DependencyResolver.BeginScope())
                {
                    var apiAuthenticationService = scope.GetService(typeof(IBasicAuthUserAuthenticationService)) as IBasicAuthUserAuthenticationService;
                    var result = apiAuthenticationService.AuthenticateUser(HttpContext.Current);

                    if (result.IsResultOk) //Authentication successful - set the principal to current context
                    {
                        //Get the authentication token from the request header
                        string authenticationToken = actionContext.Request.Headers.Authorization.Parameter;
                        //Decode the string
                        string decodedAuthenticationToken = Encoding.UTF8.GetString(Convert.FromBase64String(authenticationToken));
                        //Convert the string into an string array
                        string[] usernamePasswordArray = decodedAuthenticationToken.Split(':');
                        //First element of the array is the username
                        string apiUsername = usernamePasswordArray[0];
                        //call the login method to check the username and password

                        //Get the user id from the api username
                        var apiService = scope.GetService(typeof(IAPIAuthenticationService)) as IAPIAuthenticationService;
                        var userId = apiService.GetUserIdByApiUsername(apiUsername);

                        //Get user by user id
                        var _userManager = scope.GetService(typeof(ApplicationUserManager)) as ApplicationUserManager;
                        var appUser = _userManager.FindById(userId);

                        var claimsIdentity = AsyncHelper.RunSync(async () => await _userManager.ClaimsIdentityFactory.CreateAsync(_userManager, appUser, "Basic")); ;
                        IPrincipal principal = new ClaimsPrincipal(claimsIdentity);

                        Thread.CurrentPrincipal = principal;
                        if (HttpContext.Current != null)
                        {
                            HttpContext.Current.User = principal;
                        }
                    }
                    else
                    {
                        actionContext.Response = actionContext.Request.CreateResponse(result.HttpStatusCode, result.ResultText);
                    }
                }
        }
    }
}