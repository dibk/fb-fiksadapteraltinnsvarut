﻿using System.Web;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Authentication
{
    public interface IBasicAuthUserAuthenticationService
    {
        AuthenticationResult AuthenticateUser(HttpContext httpContext);
    }
}