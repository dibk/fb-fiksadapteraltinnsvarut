﻿using System;
using System.Net;
using System.Text;
using System.Web;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Authentication
{
    public partial class BasicAuthUserAuthenticationService : IBasicAuthUserAuthenticationService
    {
        private readonly IAPIAuthenticationService _apiAuthenticationService;

        public BasicAuthUserAuthenticationService(IAPIAuthenticationService apiAuthenticationService)
        {
            _apiAuthenticationService = apiAuthenticationService;
        }

        public AuthenticationResult AuthenticateUser(HttpContext httpContext)
        {
            string username;
            string password;

            if (string.IsNullOrWhiteSpace(httpContext.Request.Headers["Authorization"]))
            {
                return new AuthenticationResult(HttpStatusCode.Unauthorized, "Finner ikke Basic Authentication header", false);
            }
            else if (!ExtractUserNamePasswordFromAuthorizationHeader(httpContext.Request.Headers["Authorization"], out username, out password))
            {
                return new AuthenticationResult(HttpStatusCode.Unauthorized, "Klarte ikke å dekode Basic Authentication header", false);
            }
            else if (!ValidateBasicAuthHeaderCredentials(username, password))
            {
                // TODO: return unauthorized
                return new AuthenticationResult(HttpStatusCode.Unauthorized, "Unauthorized", false);
            }
            else
            {
                return new AuthenticationResult(HttpStatusCode.Accepted, "", true);
            }
        }

        private bool ValidateBasicAuthHeaderCredentials(string username, string password)
        {
            return _apiAuthenticationService.CredentialsAreValid(username, password);
        }

        private bool ExtractUserNamePasswordFromAuthorizationHeader(string authorizationHeader, out string username, out string password)
        {
            username = string.Empty;
            password = string.Empty;
            bool isSuccess = false;

            if (authorizationHeader != null && authorizationHeader.StartsWith("Basic"))
            {
                string encodedUsernamePassword = authorizationHeader.Substring("Basic ".Length).Trim();
                string decodedCredentials = Encoding.UTF8.GetString(Convert.FromBase64String(encodedUsernamePassword));

                string[] credentials = decodedCredentials.Split(':');

                if (credentials.Length == 2)
                {
                    username = credentials[0];
                    password = credentials[1];
                    isSuccess = true;
                }
            }
            return isSuccess;
        }
    }
}