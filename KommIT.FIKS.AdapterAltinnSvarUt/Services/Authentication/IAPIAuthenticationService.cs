﻿using KommIT.FIKS.AdapterAltinnSvarUt.Models.UserAccess;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Authentication
{
    public interface IAPIAuthenticationService
    {
        bool AddOrUpdateApiCredentials(APIAuthentication apiAuthentication);

        bool CredentialsAreValid(string username, string password);

        string GetUserIdByApiUsername(string username);

        string GetApiUsernameForUserId(string userId);
    }
}