﻿using KommIT.FIKS.AdapterAltinnSvarUt.Models;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Authentication
{
    public interface IAuthorizationService
    {
        bool UserHasAdministratorRole(string userId);

        bool UserHasApiAdministratorRole(string userId);

        bool UserHasAccess(string archiveReference);

        bool UserHasAccess(FormMetadata formMetaData);
    }
}