﻿using Microsoft.AspNet.Identity;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Web;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Authentication
{
    public class TokenService : ITokenService
    {
        private readonly string _serverUrl;
        private readonly string _jwtSecret;

        public TokenService()
        {
            _serverUrl = ConfigurationManager.AppSettings["ServerURL"];
            _jwtSecret = ConfigurationManager.AppSettings["Authentication:TokenGeneration:JwtSecret"];
        }

        public string GenerateToken(DateTime expirationUtcTime)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_jwtSecret));
            var signingCredentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var tokenDescriptor = new SecurityTokenDescriptor()
            {
                Issuer = _serverUrl,
                Audience = _serverUrl,
                Claims = GetClaims(),
                Expires = expirationUtcTime,
                SigningCredentials = signingCredentials
            };

            string jwt = new Microsoft.IdentityModel.JsonWebTokens.JsonWebTokenHandler().CreateToken(tokenDescriptor);

            return jwt;
        }

        private List<string> _claimKeys = new List<string> { "clientsystem" };

        private Dictionary<string, object> GetClaims()
        {
            var currentPrincipal = HttpContext.Current.User as ClaimsPrincipal;

            var claims = new Dictionary<string, object>
            {
                { JwtRegisteredClaimNames.Sub, currentPrincipal.Identity.GetUserId() },
                { JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString() },
                { JwtRegisteredClaimNames.Iat, DateTime.UtcNow.Ticks }
            };

            if (currentPrincipal.Claims != null && currentPrincipal.Claims.Count() > 0)
            {
                char[] separator = { ';' };
                var f = currentPrincipal.Claims.Where(c => _claimKeys.Exists(ck => c.Type.Equals(ck, StringComparison.OrdinalIgnoreCase))).ToList();
                foreach (var item in f)
                {
                    var claimValues = item.Value.Split(separator, StringSplitOptions.RemoveEmptyEntries).Select(p => p.Trim());
                    foreach (var value in claimValues)
                    {
                        claims.Add(item.Type, value);
                    }
                }
            }

            return claims;
        }
    }
}