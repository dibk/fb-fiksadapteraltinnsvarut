﻿using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Models.UserAccess;
using System.Configuration;
using System.Linq;
using System.Text;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Authentication
{
    public class APIAuthenticationService : IAPIAuthenticationService
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly IAuthorizationService _authorizationService;
        private readonly string _salt;

        public APIAuthenticationService(ApplicationDbContext dbContext, IAuthorizationService authorizationService)
        {
            _dbContext = dbContext;
            _authorizationService = authorizationService;
            _salt = ConfigurationManager.AppSettings["BasicAuthSalt"];
        }

        public bool AddOrUpdateApiCredentials(APIAuthentication apiAuthentication)
        {
            bool status = false;

            apiAuthentication.Password = HashPassword(apiAuthentication.Password, _salt);

            if (_authorizationService.UserHasApiAdministratorRole(apiAuthentication.UserId))
            {
                status = true;
                if (ApiUserIdExists(apiAuthentication.UserId))
                {
                    Update(apiAuthentication);
                }
                else
                    _dbContext.APIAuthentications.Add(apiAuthentication);
                _dbContext.SaveChanges();
            }
            else
            {
                status = false;
            }
            return status;
        }

        private string HashPassword(string password, string salt)
        {
            string saltedInput = password + salt;
            var sha256 = System.Security.Cryptography.SHA256.Create();
            var inputBytes = Encoding.ASCII.GetBytes(saltedInput);
            var hash = sha256.ComputeHash(inputBytes);

            var sb = new StringBuilder();
            for (var i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();
        }

        public bool CredentialsAreValid(string username, string password)
        {
            password = HashPassword(password, _salt);
            return _dbContext.APIAuthentications.Any(f => f.Username == username && f.Password == password);
        }

        private bool ApiUserIdExists(string userId)
        {
            return _dbContext.APIAuthentications.Any(f => f.UserId == userId);
        }

        private void Update(APIAuthentication apiAuthentication)
        {
            _dbContext.Entry(apiAuthentication).State = System.Data.Entity.EntityState.Modified;
            _dbContext.SaveChanges();
        }

        public string GetUserIdByApiUsername(string username)
        {
            var queryResult = from s in _dbContext.APIAuthentications
                              where s.Username == username
                              select s;
            if (queryResult.Count() == 0)
            {
                return null;
            }
            else
            {
                return queryResult.FirstOrDefault().UserId;
            }
        }

        public string GetApiUsernameForUserId(string userId)
        {
            var queryResult = from s in _dbContext.APIAuthentications
                              where s.UserId == userId
                              select s;
            if (queryResult.Count() == 0)
            {
                return null;
            }
            else
            {
                return queryResult.FirstOrDefault().Username;
            }
        }
    }
}