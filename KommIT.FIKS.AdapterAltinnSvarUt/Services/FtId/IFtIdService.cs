﻿using KommIT.FIKS.AdapterAltinnSvarUt.Models.ApiModels;
using System.Collections.Generic;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.FtId
{
    public interface IFtIdService
    {
        string CreateNew(NewFtIdPayload payload);

        void Attach(AttachFtIdPayload payload);

        List<FtIdMetadata> GetAttachedMetadata(string ftId);

        bool FtIdExists(string ftId);
    }
}