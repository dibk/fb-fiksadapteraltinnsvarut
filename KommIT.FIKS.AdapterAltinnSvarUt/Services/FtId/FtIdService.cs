using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Models.ApiModels;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Validations;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Linq;
using FTId = KommIT.FIKS.AdapterAltinnSvarUt.Models.FtId;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.FtId
{
    public class FtIdService : IFtIdService
    {
        private static readonly string _gs1Prefix = ConfigurationManager.AppSettings["GS1Prefix"];
        private static readonly string _freeDigits = "000";
        private readonly ILogger<FtIdService> _logger;
        private readonly ApplicationDbContext _context;

        public FtIdService(ILogger<FtIdService> logger, ApplicationDbContext context)
        {
            _logger = logger;
            _context = context;
        }

        public string CreateNew(NewFtIdPayload payload)
        {
            var existing = GetFtId(payload);

            if (existing != null)
            {
                _logger.LogDebug("Prosjekt {NyFtIdPayload} allerede registrert med FtID {FtId}.", payload, existing.EAN);
                AddProsjektnummerIfNotExisting(existing, payload);
                return existing.EAN;
            }

            var validationResult = new MunicipalityValidator().ValidateKommunenummer(payload.Kommunenummer);

            if (validationResult.Status == ValidationStatus.Fail)
                throw new FtIdException(validationResult.Message);

            var ftId = new FTId
            {
                EAN = GenerateEan(),
                Kommunenummer = payload.Kommunenummer.Trim(),
                Gaardsnummer = payload.Gaardsnummer.Value,
                Bruksnummer = payload.Bruksnummer.Value,
                Prosjektnavn = payload.Prosjektnavn.Trim(),
                Prosjektnummer = payload.Prosjektnr?.Trim(),
                System = payload.System.Trim(),
                Distributed = DateTime.Now
            };

            _context.FtIds.Add(ftId);

            _context.SaveChanges();

            _logger.LogInformation("Ny FtID {FtId} opprettet for prosjekt {NyFtIdPayload}.", ftId.EAN, payload);

            return ftId.EAN;
        }

        public void Attach(AttachFtIdPayload payload)
        {
            if (!ArchiveReferenceExists(payload.ArchiveReference))
                throw new FtIdException($"Fant ingen datapunkter med arkivreferansen {payload.ArchiveReference}");

            if (!FtIdExists(payload.FtId))
                throw new FtIdException($"FtId {payload.FtId} eksisterer ikke");

            var logEntry = _context.FormWorkflowLog
                .SingleOrDefault(entry => entry.ArchiveReference == payload.ArchiveReference);

            if (logEntry == null)
                _context.FormWorkflowLog.Add(new FormWorkflowLog { ArchiveReference = payload.ArchiveReference, FtId = payload.FtId });
            else
                logEntry.FtId = payload.FtId;

            _context.SaveChanges();
        }

        public List<FtIdMetadata> GetAttachedMetadata(string ftId)
        {
            var archiveReferences = _context.FormWorkflowLog
                .Where(logEntry => logEntry.FtId == ftId)
                .Select(logEntry => logEntry.ArchiveReference)
                .AsNoTracking()
                .ToList();

            if (!archiveReferences.Any())
                return new List<FtIdMetadata>();

            var formMetadata = _context.FormMetadata
                .Where(metadata => archiveReferences.Contains(metadata.ArchiveReference))
                .AsNoTracking()
                .ToList();

            return formMetadata
                .ConvertAll(metadata => new FtIdMetadata(metadata.ArchiveReference, metadata.FormType));
        }

        private string GenerateEan()
        {
            var sequenceNumber = GetNextSequenceNumber();
            var ean = $"{_gs1Prefix}{_freeDigits}{sequenceNumber:D7}";
            var checkDigit = CalculateCheckDigit(ean);

            return ean + checkDigit;
        }

        private int GetNextSequenceNumber()
        {
            var param = new SqlParameter("@result", SqlDbType.Int)
            {
                Direction = ParameterDirection.Output
            };

            _context.Database.ExecuteSqlCommand("SET @result = NEXT VALUE FOR FtId_Sequence", param);

            return (int)param.Value;
        }

        private FTId GetFtId(NewFtIdPayload payload)
        {
            var prosjektnavn = payload.Prosjektnavn.Trim().ToLower();

            return _context.FtIds
                .SingleOrDefault(ftId => ftId.Kommunenummer.Equals(payload.Kommunenummer) &&
                    ftId.Gaardsnummer == payload.Gaardsnummer &&
                    ftId.Bruksnummer == payload.Bruksnummer &&
                    ftId.Prosjektnavn.ToLower().Equals(prosjektnavn));
        }

        private void AddProsjektnummerIfNotExisting(FTId ftId, NewFtIdPayload payload)
        {
            var prosjektnr = payload.Prosjektnr?.Trim();

            if (ftId.Prosjektnummer == null && !string.IsNullOrWhiteSpace(prosjektnr))
            {
                ftId.Prosjektnummer = prosjektnr;
                _context.SaveChanges();
            }
        }

        private bool ArchiveReferenceExists(string archiveReference)
        {
            return _context.FormMetadata
                .AsNoTracking()
                .Any(metadata => metadata.ArchiveReference == archiveReference);
        }

        private static int CalculateCheckDigit(string ean)
        {
            var digits = ean.ToCharArray()
                .Select(ch => int.Parse(ch.ToString()))
                .ToList();

            var sum = 0;

            for (var i = 0; i < digits.Count; i++)
            {
                if (i % 2 == 0)
                    sum += digits[i] * 3;
                else
                    sum += digits[i];
            }

            var nearestTen = (int)(Math.Ceiling(sum / 10.0d) * 10);

            return nearestTen - sum;
        }

        public bool FtIdExists(string ftId)
        {
            return _context.FtIds
                        .AsNoTracking()
                        .Any(id => id.EAN == ftId);
        }
    }
}