﻿namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.HealthChecks.HealthCheckImpls
{
    public static class MatrikkelHealthCheckConfig
    {
        public static int TimeoutSeconds => 20;
    }
}