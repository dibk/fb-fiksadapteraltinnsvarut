﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Encryption
{
    public interface IDecryptionFactory 
    {
        IDecryption GetDecryptor();
    }
    public class DecryptionFactory : IDecryptionFactory
    {
        public IDecryption GetDecryptor()
        {
            return Decryption.Instance;
        }
    }
}