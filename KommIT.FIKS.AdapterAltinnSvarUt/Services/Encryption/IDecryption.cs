﻿using System;
using System.Security.Cryptography;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Encryption
{
    public interface IDecryption
    {
        /// <summary>
        /// Decrypts base64 encoded strings
        /// </summary>
        /// <param name="cipherText">Base64 encoded encrypted string</param>
        /// <param name="decryptedText">Plain textvalue of input. Null if decryption failed</param>
        /// <returns>true if decryption successfull.</returns>
        bool TryDecryptText(string cipherText, out string decryptedText);
        /// <summary>
        /// Decrypts base64 encoded strings
        /// </summary>
        /// <param name="cipherText">Base64 encoded encrypted string</param>
        /// <returns>Plain text value of input</returns>
        /// <exception cref="FormatException">Thrown if the chipered text is not base64 encoded</exception>
        /// <exception cref="CryptographicException">Thrown if the chipered text is not decryptable</exception>
        string DecryptText(string cipherText);
        string EncryptText(string clearText);

    }
}