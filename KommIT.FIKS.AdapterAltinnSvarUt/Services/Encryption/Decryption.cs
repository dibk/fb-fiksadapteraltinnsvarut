﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Encryption
{
    public class Decryption : IDecryption
    {
        internal static RSACryptoServiceProvider PrivateKeyProvider;
        internal static RSACryptoServiceProvider PublicKeyProvider;

        private static IDecryption _instance = null;
        public static IDecryption Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new Decryption();
                return _instance;

            }
        }

        private Decryption()
        {
            var certificateThumbprintSetting = ConfigurationManager.AppSettings["Encrypt.CertificateThumbprint"];
            var cryptoProviders = GetRsaCryptoProviderFromKeyStore(certificateThumbprintSetting);

            PrivateKeyProvider = cryptoProviders.Item1;
            PublicKeyProvider = cryptoProviders.Item2;
        }

        /// <summary>
        /// Decrypts base64 encoded strings
        /// </summary>
        /// <param name="cipherText">Base64 encoded encrypted string</param>
        /// <param name="decryptedText">Plain textvalue of input. Null if decryption failed</param>
        /// <returns>true if decryption successfull.</returns>
        public bool TryDecryptText(string cipherText, out string decryptedText)
        {
            return TryDecryptString(PrivateKeyProvider, cipherText, out decryptedText);
        }

        /// <summary>
        /// Decrypts base64 encoded strings
        /// </summary>
        /// <param name="cipherText">Base64 encoded encrypted string</param>
        /// <returns>Plain text value of input</returns>
        /// <exception cref="FormatException">Thrown if the chipered text is not base64 encoded</exception>
        /// <exception cref="CryptographicException">Thrown if the chipered text is not decryptable</exception>
        public string DecryptText(string cipherText)
        {
            return DecryptString(PrivateKeyProvider, cipherText);
        }

        private static bool TryDecryptString(RSACryptoServiceProvider rsaCryptoPrivateKey, string cipherB64Text, out string decryptedValue)
        {
            byte[] cipherBytes;

            if (!TryGetFromBase64String(cipherB64Text, out cipherBytes))
            {
                decryptedValue = null;
                return false;
            }

            byte[] decryptedBytes;

            if (!rsaCryptoPrivateKey.TryDecrypt(cipherBytes, out decryptedBytes))
            {
                decryptedValue = null;
                return false;
            }

            var byteConverter = new UnicodeEncoding();
            decryptedValue = byteConverter.GetString(decryptedBytes);
            return true;
        }

        private static string DecryptString(RSACryptoServiceProvider rsaCryptoPrivateKey, string cipherB64Text)
        {
            byte[] cipherBytes = Convert.FromBase64String(cipherB64Text);
            var decryptedBytes = rsaCryptoPrivateKey.Decrypt(cipherBytes, false);
            var byteConverter = new UnicodeEncoding();
            return byteConverter.GetString(decryptedBytes);
        }

        private static bool TryGetFromBase64String(string input, out byte[] output)
        {
            output = null;
            try
            {
                output = Convert.FromBase64String(input);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        public string EncryptText(string clearText)
        {
            return EncryptString(PublicKeyProvider, clearText);
        }

        private static string EncryptString(RSACryptoServiceProvider rsaCryptoPublicKey, string clearText)
        {
            var byteConverter = new UnicodeEncoding();
            var clearTextBytes = byteConverter.GetBytes(clearText);
            var cipherBytes = rsaCryptoPublicKey.Encrypt(clearTextBytes, false);
            var clearTextBase64 = Convert.ToBase64String(cipherBytes);

            return clearTextBase64;
        }

        private static Tuple<RSACryptoServiceProvider, RSACryptoServiceProvider> GetRsaCryptoProviderFromKeyStore(string thumbprint)
        {
            X509Certificate2 cert = null;

            List<X509Store> certStores = new List<X509Store>();
            certStores.Add(new X509Store(StoreName.My, StoreLocation.CurrentUser));
            certStores.Add(new X509Store(StoreName.My, StoreLocation.LocalMachine));

            foreach (X509Store certStore in certStores)
            {
                certStore.Open(OpenFlags.ReadOnly);
                var certCollection = certStore.Certificates.Find(
                    X509FindType.FindByThumbprint,
                    thumbprint,
                    false);
                // Get the first cert with the thumbprint
                if (certCollection.Count > 0)
                {
                    cert = certCollection[0];
                }
                certStore.Close();

                // Exit foreach if already found
                if (cert != null)
                {
                    break;
                }
            }

            if (cert != null)
                return Tuple.Create((RSACryptoServiceProvider)cert.PrivateKey, (RSACryptoServiceProvider)cert.PublicKey.Key);
            throw new Exception($"No certificate found for given thumbprint {thumbprint}");
        }
    }

    public static class RSACryptoServiceProviderExtensions
    {
        public static bool TryDecrypt(this RSACryptoServiceProvider provider, byte[] cipherBytes, out byte[] decryptedBytes)
        {
            try
            {
                decryptedBytes = provider.Decrypt(cipherBytes, false);
                return true;
            }
            catch (CryptographicException)
            {
                decryptedBytes = null;
                return false;
            }
        }
    }
}