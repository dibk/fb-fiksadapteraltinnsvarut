﻿using Newtonsoft.Json;
using System.Configuration;
using System.Net;
using System.Text;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Encryption.TextEncyption
{
    public class TextEncryptionService : ITextEncryptionService
    {
        private readonly string _baseUrl;
        public TextEncryptionService()
        {
            _baseUrl = ConfigurationManager.AppSettings["TextEncryptorApiUrl"];
        }
        public string Encrypt(string textToEncrypt)
        {
            var url = $"{_baseUrl}/Encryption/encrypt?cryptText={textToEncrypt}";
            var encryptedText = string.Empty;
            using (WebClient client = new WebClient())
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                client.Encoding = Encoding.UTF8;
                client.Headers.Add(HttpRequestHeader.Accept, "application/text");
                encryptedText = client.DownloadString(url);
            }

            return encryptedText;
        }

        public string Decrypt(string encryptedText)
        {
            var url = $"{_baseUrl}/Encryption/decrypt";
            var decryptedText = string.Empty;

            var payloadObj = new TextEncryptionPayload() { CryptText = encryptedText };
            var payloadJson = JsonConvert.SerializeObject(payloadObj);

            using (WebClient client = new WebClient())
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                client.Encoding = Encoding.UTF8;
                client.Headers.Add(HttpRequestHeader.Accept, "application/text");
                client.Headers.Add(HttpRequestHeader.ContentType, "application/json");
                decryptedText = client.UploadString(url, payloadJson);
            }

            return decryptedText;
        }
    }

    internal class TextEncryptionPayload
    {
        public string CryptText { get; set; }
    }
}