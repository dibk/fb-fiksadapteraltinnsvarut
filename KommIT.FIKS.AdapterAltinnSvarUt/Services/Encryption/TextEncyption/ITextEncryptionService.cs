﻿namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Encryption.TextEncyption
{
    public interface ITextEncryptionService
    {
        string Decrypt(string encryptedText);
        string Encrypt(string textToEncrypt);
    }
}