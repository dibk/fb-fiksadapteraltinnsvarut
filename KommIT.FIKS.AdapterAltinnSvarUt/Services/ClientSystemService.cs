﻿using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Models.UserAccess;
using System.Collections.Generic;
using System.Linq;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services
{
    public class ClientSystemService
    {
        private readonly ApplicationDbContext _dbContext;

        public ClientSystemService(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void AddClientSystem(ClientSystem clientSystem)
        {
            _dbContext.ClientSystems.Add(clientSystem);
            _dbContext.SaveChanges();
        }

        public void UpdateClientSystem(ClientSystem clientSystem)
        {
            _dbContext.Entry(clientSystem).State = System.Data.Entity.EntityState.Modified;
            _dbContext.SaveChanges();
        }

        public List<ClientSystem> GetClientSystems()
        {
            //Consider caching
            return _dbContext.ClientSystems.OrderBy(a => a.DisplayName).ToList();
        }
    }
}