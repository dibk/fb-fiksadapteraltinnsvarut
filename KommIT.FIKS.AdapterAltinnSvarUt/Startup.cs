﻿using KommIT.FIKS.AdapterAltinnSvarUt.App_Start;
using Microsoft.Owin;
using Owin;
using System.Configuration;
using System.IO;
using System.Web;

[assembly: OwinStartupAttribute(typeof(KommIT.FIKS.AdapterAltinnSvarUt.Startup))]
namespace KommIT.FIKS.AdapterAltinnSvarUt
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.UseForwardedHeaders();

            DependencyConfig.Configure(app);
            ConfigureAuth(app);
            CreateAppDataDirecotryIfMissing();

            SetUpHangfire(app);
        }

        private static void SetUpHangfire(IAppBuilder app)
        {
            var hangfireEnabledConfigValue = ConfigurationManager.AppSettings["HangfireEnabled"];
            
            bool hangfireEnabled;
            if (!bool.TryParse(hangfireEnabledConfigValue, out hangfireEnabled))
                hangfireEnabled = true;

            if (hangfireEnabled)
                app.UseHangfire();
        }

        private void CreateAppDataDirecotryIfMissing()
        {
            string appDataFolder = HttpContext.Current.Server.MapPath("~/App_Data/");
            if (!Directory.Exists(appDataFolder))
                Directory.CreateDirectory(appDataFolder);
        }
    }
}
