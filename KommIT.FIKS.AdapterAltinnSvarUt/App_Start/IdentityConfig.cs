﻿using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Models.UserAccess;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Email;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.DataProtection;
using Serilog;
using System;
using System.Security.Claims;
using System.Threading.Tasks;

namespace KommIT.FIKS.AdapterAltinnSvarUt
{
    public static class ApplicationRole
    {
        public const string Administrator = "administrator";
        public const string User = "user";
        public const string SystemIntegrator = "systemintegrator";
    }

    public class EmailService : IIdentityMessageService
    {
        private readonly ILogger _logger = Log.ForContext<EmailService>();

        public Task SendAsync(IdentityMessage message)
        {
            _logger.Information($"Sending email [to={message.Destination}] [subject={message.Subject}]");

            try
            {
                var e = new EmailClient();
                e.SendEmail(message.Destination, message.Subject, message.Body);
            }
            catch (Exception ex)
            {
                _logger.Error(ex, $"Error occurred when sending email [to={message.Destination}] [subject={message.Subject}]");
                throw;
            }

            //Services.EmailService email = new Services.EmailService();
            //email.SendAsync(message);
            return Task.FromResult(0);
        }
    }

    public class SmsService : IIdentityMessageService
    {
        public Task SendAsync(IdentityMessage message)
        {
            // Plug in your SMS service here to send a text message.
            return Task.FromResult(0);
        }
    }

    public class ApplicationUserStore : UserStore<ApplicationUser>
    {
        public ApplicationUserStore(ApplicationDbContext context)
            : base(context)
        {
        }
    }

    // Configure the application user manager used in this application. UserManager is defined in ASP.NET Identity and is used by the application.
    public class ApplicationUserManager : UserManager<ApplicationUser>
    {
        public ApplicationUserManager(IUserStore<ApplicationUser> store, IDataProtectionProvider dataProtectionProvider)
            : base(store)
        {
            UserValidator = new UserValidator<ApplicationUser>(this)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true
            };

            PasswordValidator = new PasswordValidator
            {
                RequiredLength = 6,
                RequireDigit = true,
                RequireLowercase = true,
                RequireUppercase = true,
            };

            UserLockoutEnabledByDefault = false;
            EmailService = new EmailService();
            SmsService = new SmsService();

            UserTokenProvider = new DataProtectorTokenProvider<ApplicationUser>(dataProtectionProvider.Create("ASP.NET Identity"));
        }

        /// <summary>
        /// Find user by email
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public Task<ApplicationUser> FindUserById(string userId)
        {
            return Store.FindByIdAsync(userId);
        }
    }

    // Configure the application sign-in manager which is used in this application.
    public class ApplicationSignInManager : SignInManager<ApplicationUser, string>
    {
        public ApplicationSignInManager(ApplicationUserManager userManager, IAuthenticationManager authenticationManager)
            : base(userManager, authenticationManager)
        {
        }

        public override Task<ClaimsIdentity> CreateUserIdentityAsync(ApplicationUser user)
        {
            return user.GenerateUserIdentityAsync((ApplicationUserManager)UserManager);
        }
    }
}