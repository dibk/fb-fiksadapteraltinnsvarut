﻿using Hangfire;
using Hangfire.Dashboard;
using KommIT.FIKS.AdapterAltinnSvarUt.Services;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Codelists;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.MunicipalityApi;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Svarut.ShipmentStatus;
using Owin;
using Serilog;
using System;
using System.Configuration;

namespace KommIT.FIKS.AdapterAltinnSvarUt
{
    public static class HangfireConfig
    {
        public static IAppBuilder UseHangfire(this IAppBuilder app)
        {
            try
            {
                //Sets connectionFactory to Microsoft.Data.SqlClient to be able to use AAD identification
                //https://discuss.hangfire.io/t/using-microsoft-data-sqlclient-in-hangfire-sqlserver/9360

                var connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

                GlobalConfiguration.Configuration.UseSqlServerStorage(
                        connectionFactory: () => new Microsoft.Data.SqlClient.SqlConnection(connectionString),
                        options: new Hangfire.SqlServer.SqlServerStorageOptions()
                        {
                            SlidingInvisibilityTimeout = TimeSpan.FromDays(4), //https://discuss.hangfire.io/t/how-does-the-following-sqlserverstorageoptions-work/6127/4
                            QueuePollInterval = TimeSpan.Zero,
                            UseRecommendedIsolationLevel = true,
                        });

                GlobalJobFilters.Filters.Add(new AutomaticRetryAttribute { Attempts = 0 });

                var options = new DashboardOptions
                {
                    Authorization = new[]
                    {
                    new AuthorizationFilter { Roles = ApplicationRole.Administrator },
                }
                };
                app.UseHangfireDashboard("/hangfire", options);

                //Uses two hangfire servers to make sure purge items are processed ASAP
                var hangfireOptionsMainServer = new BackgroundJobServerOptions()
                {
                    Queues = new string[] { HangfireConfiguration.HangfireQueueNames.Default },
                    ServerName = string.Format("{0}.{1}", Environment.MachineName, Guid.NewGuid().ToString())
                };

                var hangfireOptionsPurgeServer = new BackgroundJobServerOptions()
                {
                    Queues = new string[] { HangfireConfiguration.HangfireQueueNames.Purge },
                    ServerName = string.Format("{0}.{1}", Environment.MachineName, Guid.NewGuid().ToString())
                };

                var workerCountInConfig = ConfigurationManager.AppSettings["Hangfire:WorkerCount"];
                int workerCount;
                if (!string.IsNullOrEmpty(workerCountInConfig) && int.TryParse(workerCountInConfig, out workerCount))
                {
                    hangfireOptionsMainServer.WorkerCount = workerCount;
                    hangfireOptionsPurgeServer.WorkerCount = workerCount;
                }
                app.UseHangfireServer(hangfireOptionsMainServer);
                app.UseHangfireServer(hangfireOptionsPurgeServer);

                ScheduleRecurringJobs();

                return app;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Error occurred when setting up Hangfire");
                throw;
            }
        }

        private static void ScheduleRecurringJobs()
        {
            new CodeListRefreshScheduler().ScheduleRefreshingOfCodeLists();

            var cacheService = (MunicipalityCacheService)System.Web.Mvc.DependencyResolver.Current.GetService(typeof(MunicipalityCacheService));
            new MunicipalityMemoryCacheRefreshScheduler(cacheService).ScheduleMunicipalityMemoryCacheRefreshScheduler();

            var svarUtShipmentStatusPersister = (ISvarUtShipmentStatusPersister)System.Web.Mvc.DependencyResolver.Current.GetService(typeof(ISvarUtShipmentStatusPersister));
            new ScheduledShipmentStatusPersister(svarUtShipmentStatusPersister).ScheduleUpdateShipmentStatutses();

            var sabyCleanUpService = (TiltakshaverSamtykkeCleanUpService)System.Web.Mvc.DependencyResolver.Current.GetService(typeof(TiltakshaverSamtykkeCleanUpService));
            sabyCleanUpService.ScheduleCleanUpJobs();
        }
    }
}