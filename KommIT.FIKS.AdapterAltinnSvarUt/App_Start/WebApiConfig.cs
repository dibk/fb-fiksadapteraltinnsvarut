using KommIT.FIKS.AdapterAltinnSvarUt.Logger.SerilogEnrichers.WebApi;
using KommIT.FIKS.AdapterAltinnSvarUt.WebApi;
using Microsoft.Owin.Security.OAuth;
using System.Web.Http;
using WebApi.Hal;

namespace KommIT.FIKS.AdapterAltinnSvarUt
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.SuppressDefaultHostAuthentication();
            config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));
            config.Filters.Add(new StoreWebApInfoInHttpContextAuthenticationFilter());
            config.Filters.Add(new ErrorHandler.LogExceptionFilterWebApiAttribute());
            config.MessageHandlers.Add(new Logger.RequestResponseLoggingMessageHandler());

            config.EnableCors();

            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            //config.Formatters.XmlFormatter.SupportedMediaTypes.Clear();
            config.Formatters.XmlFormatter.UseXmlSerializer = true;

            //Replaces built-in JSON formatter
            //and adds JSON formatters to support both application/json and application/hal+json            
            var standardJsonFormatter = config.Formatters.JsonFormatter;
            config.Formatters.Remove(standardJsonFormatter);
            config.Formatters.Insert(0, new JsonDefaultMediaTypeFormatter());
            config.Formatters.Add(new JsonHalMediaTypeOutputFormatter());
        }
    }
}
