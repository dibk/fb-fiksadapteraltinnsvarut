﻿using System.Web.Http;
using System.Web.Http.Controllers;

namespace KommIT.FIKS.AdapterAltinnSvarUt
{
    public class IdPortenAuthorizeAttribute : AuthorizeAttribute
    {
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            if (!IdPortenAuthConfig.AuthenticationEnabled)
                return;

            base.OnAuthorization(actionContext);
        }
    }
}