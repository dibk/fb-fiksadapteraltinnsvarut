﻿using System.Configuration;

namespace KommIT.FIKS.AdapterAltinnSvarUt
{
    public static class IdPortenAuthConfig
    {
        public static string Issuer => ConfigurationManager.AppSettings["Authentication:IdPorten:Issuer"];
        public static bool AuthenticationEnabled { get {

                bool enabled;
                var enabledConfigValue = ConfigurationManager.AppSettings["Authentication:IdPorten:Enabled"];
                if (!bool.TryParse(enabledConfigValue, out enabled))
                    enabled = true;

                return enabled;
            } }
        public static string Audience => ConfigurationManager.AppSettings["Authentication:IdPorten:Audience"];
    }
}