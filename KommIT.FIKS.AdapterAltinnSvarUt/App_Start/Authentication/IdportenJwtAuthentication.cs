﻿using Microsoft.IdentityModel.Protocols;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Jwt;
using Owin;
using Serilog;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;

namespace KommIT.FIKS.AdapterAltinnSvarUt
{
    public static class IdportenJwtAuthentication
    {
        public static IAppBuilder AddIdportenJwtAuthentication(this IAppBuilder app)
        {
            var issuer = IdPortenAuthConfig.Issuer;
            var apiIdentifier = IdPortenAuthConfig.Audience;
            var keyResolver = new OpenIdConnectSigningKeyResolver(issuer);

            return app.UseJwtBearerAuthentication(
                new JwtBearerAuthenticationOptions
                {
                    AuthenticationMode = AuthenticationMode.Active,
                    TokenValidationParameters = new TokenValidationParameters()
                    {
                        ValidateLifetime = true,
                        ValidAudience = apiIdentifier,
                        ValidateAudience = false,
                        ValidIssuer = issuer,                        
                        ValidateIssuer = true,
                        IssuerSigningKeyResolver = (token, securityToken, kid, parameters) => keyResolver.GetSigningKey(kid),
                    },
                    TokenHandler = new LoggingTokenHandler(),
                }) ;
        }
    }

    public class LoggingTokenHandler : JwtSecurityTokenHandler
    {
        public override ClaimsPrincipal ValidateToken(string securityToken, TokenValidationParameters validationParameters, out SecurityToken validatedToken)
        {
            try
            {
                return base.ValidateToken(securityToken, validationParameters, out validatedToken);
            }
            catch (Exception ex)
            {
                Log.Logger.Error(ex, "Token validation failed");
                throw;
            }
        }
    }

    public class OpenIdConnectSigningKeyResolver
    {
        private readonly OpenIdConnectConfiguration openIdConfig;

        public OpenIdConnectSigningKeyResolver(string authority)
        {
            var cm = new ConfigurationManager<OpenIdConnectConfiguration>($"{authority.TrimEnd('/')}/.well-known/openid-configuration", new OpenIdConnectConfigurationRetriever());
            openIdConfig = AsyncHelper.RunSync(async () => await cm.GetConfigurationAsync());
        }

        public SecurityKey[] GetSigningKey(string kid)
        {
            return new[] { openIdConfig.JsonWebKeySet.GetSigningKeys().FirstOrDefault(t => t.KeyId == kid) };
        }
    }
}