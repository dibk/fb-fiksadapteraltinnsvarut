﻿using System.EnterpriseServices.CompensatingResourceManager;
using System.Web.Optimization;

namespace KommIT.FIKS.AdapterAltinnSvarUt
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/Content/bower_components/dibk-fb-webcommon/assets/js/scripts").Include(
                        "~/Content/bower_components/dibk-fb-webcommon/assets/js/main.min.js",
                        "~/Content/bower_components/chosen/chosen.jquery.js",
                        "~/Content/bower_components/select2/dist/js/select2.full.min.js",
                        "~/Content/bower_components/vue/dist/vue.js",
                        "~/Content/bower_components/Sortable/Sortable.min.js",
                        "~/Content/bower_components/respond/dest/respond.min.js",
                        "~/Content/bower_components/axios/dist/axios.min.js",
                        "~/Scripts/main.js"));

            bundles.Add(new StyleBundle("~/Content/bower_components/dibk-fb-webcommon/assets/css/styles").Include(
                        "~/Content/bower_components/dibk-fb-webcommon/assets/css/main.css",
                        "~/Content/bower_components/chosen/chosen.css",
                        "~/Content/bower_components/select2/dist/css/select2.min.css",
                        "~/Content/JSONViewer/json-viewer.css",
                        "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/bower_components/dibk-fb-webcommon-wiz/assets/css/styles").Include(
                       "~/Content/bower_components/dibk-fb-webcommon/assets/css/main.css",
                       "~/Content/site.css",
                       "~/Content/dibk-fb-webcommon-override.css",
                       "~/Content/wizard.css"));

            BundleTable.EnableOptimizations = false;
        }
    }
}
