﻿using Owin;
using System;
using System.Web;

namespace KommIT.FIKS.AdapterAltinnSvarUt.App_Start
{
    public static class UseForwardedHeadersExtension
    {
        private const string ForwardedHeadersAdded = "ForwardedHeadersAdded";

        /// <summary>
        /// Checks for the presence of <c>X-Forwarded-Host</c> header, and if present updates the <c>HTTP_HOST</c> header.
        /// </summary>
        /// <remarks>
        /// This extension method is required for running behind FrontDoor. FrontDoor adds the <c>X-Forwarded-Host</c> headers to indicate the host from the original request.
        /// </remarks>
        public static IAppBuilder UseForwardedHeaders(this IAppBuilder app)
        {
            if (app == null)
            {
                throw new ArgumentNullException(nameof(app));
            }

            // no need to add more than one instance of this middleware to the pipeline.
            if (app.Properties.ContainsKey(ForwardedHeadersAdded)) return app;

            app.Properties[ForwardedHeadersAdded] = true;

            app.Use(async (context, next) =>
            {
                var request = context.Request;

                if (request.Headers.ContainsKey("X-Forwarded-Host"))
                {
                    var httpContext = context.Get<HttpContextBase>(typeof(HttpContextBase).FullName);
                    var serverVars = httpContext.Request.ServerVariables;
                    serverVars["HTTP_HOST"] = request.Headers["X-Forwarded-Host"];
                }

                await next.Invoke().ConfigureAwait(false);
            });

            return app;
        }
    }
}