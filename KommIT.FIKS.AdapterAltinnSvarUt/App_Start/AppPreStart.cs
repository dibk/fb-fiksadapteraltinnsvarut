﻿using KommIT.FIKS.AdapterAltinnSvarUt.App_Start;

//using Serilog;
//using System.IO;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(AppPreStart), nameof(AppPreStart.PreApplicationStart))]

namespace KommIT.FIKS.AdapterAltinnSvarUt.App_Start
{
    public class AppPreStart
    {
        public static void PreApplicationStart()
        {
            //Uncomment to enable selflog
            //Serilog.Debugging.SelfLog.Enable(TextWriter.Synchronized(new StreamWriter(@"c:\Temp\serilog-selflog.txt")));

            var logger = Logger.LogConfig.GetLoggerConfiguration().CreateLogger().ForContext<AppPreStart>();
            logger.Verbose("Logger configured");
            logger.Information("Application starts");
        }
    }
}