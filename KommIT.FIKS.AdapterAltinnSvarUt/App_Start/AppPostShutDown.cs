﻿using KommIT.FIKS.AdapterAltinnSvarUt.App_Start;
using Serilog;

[assembly: WebActivatorEx.ApplicationShutdownMethod(typeof(AppPostShutDown), nameof(AppPostShutDown.PostApplicationShutDown))]
namespace KommIT.FIKS.AdapterAltinnSvarUt.App_Start
{
    public class AppPostShutDown
    {
        private static ILogger Logger = Log.ForContext<AppPostShutDown>();

        public static void PostApplicationShutDown()
        {
            Logger.Debug("PostApplicationShutDown");

            // force flushing the last "not logged" events
            Logger.Debug("Closing and flushing the logger! ");
            Log.CloseAndFlush();
        }
    }
}