﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using AutofacSerilogIntegration;
using DIBK.FBygg.Altinn.MapperCodelist;
using Hangfire;
using KommIT.FIKS.AdapterAltinnSvarUt.ControllersApi.StatusV2;
using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Models.UserAccess;
using KommIT.FIKS.AdapterAltinnSvarUt.QueuePreparators;
using KommIT.FIKS.AdapterAltinnSvarUt.Services;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Altinn;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Altinn.Correspondence;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Altinn.Prefill;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Authentication;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Azure;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Codelists;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.DecryptFnrForTh;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Encryption;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Encryption.TextEncyption;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.FtId;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.HealthChecks;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.HealthChecks.HealthCheckImpls;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.MunicipalityApi;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Notification;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Prefill;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Receipt;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Repositories;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.SearchEngine;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Svarut;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Svarut.REST;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Svarut.ShipmentStatus;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Svarut.StatusService;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.SvarUt;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.UserNotification;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.WS;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.WS.Altinn;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;
using Microsoft.AspNet.Identity;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.DataProtection;
using Owin;
using Serilog;
using Serilog.Extensions.Autofac.DependencyInjection;
using System.Net.Http;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace KommIT.FIKS.AdapterAltinnSvarUt
{
    public class DependencyConfig
    {
        public static void Configure(IAppBuilder app)
        {
            ConfigureDependenciesForMvc(app);
            ConfigureDependenciesForHangfire(app);
        }

        private static void ConfigureDependenciesForMvc(IAppBuilder app)
        {
            var builder = new ContainerBuilder();

            ConfigureBasicSetup(builder);
            ConfigureAppDependencies(builder);
            AddIntegrationTestServices(builder);

            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            builder.RegisterType<ApplicationDbContext>().AsSelf().InstancePerRequest();
            builder.RegisterType<BlobStorage>().AsSelf().InstancePerRequest();
            builder.RegisterType<PublicBlobStorage>().AsSelf().InstancePerRequest();
            builder.RegisterType<ApplicationUserStore>().As<IUserStore<ApplicationUser>>().InstancePerRequest();
            builder.RegisterType<ApplicationUserManager>().AsSelf().InstancePerRequest();
            builder.RegisterType<ApplicationSignInManager>().AsSelf().InstancePerRequest();
            builder.Register<IAuthenticationManager>(c => HttpContext.Current.GetOwinContext().Authentication).InstancePerRequest();
            builder.Register<IDataProtectionProvider>(c => app.GetDataProtectionProvider()).InstancePerRequest();

            IContainer container = builder.Build();

            var loggerFactory = container.Resolve<ILoggerFactory>();
            loggerFactory.AddSerilog();

            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));

            // MVC WebAPI configuration
            System.Web.Http.GlobalConfiguration.Configuration.DependencyResolver = new AutofacWebApiDependencyResolver(container);

            app.UseAutofacMiddleware(container);
            app.UseAutofacMvc();
        }

        private static void ConfigureDependenciesForHangfire(IAppBuilder app)
        {
            var builder = new ContainerBuilder();

            ConfigureBasicSetup(builder);
            ConfigureAppDependencies(builder);

            builder.RegisterType<ApplicationDbContext>().AsSelf().InstancePerBackgroundJob();
            builder.RegisterType<BlobStorage>().AsSelf().InstancePerBackgroundJob();
            builder.RegisterType<PublicBlobStorage>().AsSelf().InstancePerBackgroundJob();
            builder.RegisterType<ApplicationUserStore>().As<IUserStore<ApplicationUser>>().InstancePerBackgroundJob();
            builder.RegisterType<ApplicationUserManager>().AsSelf().InstancePerBackgroundJob();
            builder.Register<IDataProtectionProvider>(c => app.GetDataProtectionProvider()).InstancePerBackgroundJob();
            builder.RegisterType<CodeListRefreshScheduler>().AsSelf().InstancePerBackgroundJob();
            builder.RegisterType<MunicipalityMemoryCacheRefreshScheduler>().AsSelf().InstancePerBackgroundJob();

            IContainer container = builder.Build();

            GlobalConfiguration.Configuration.UseAutofacActivator(container);
        }

        private static void ConfigureBasicSetup(ContainerBuilder builder)
        {
            IServiceCollection services = new ServiceCollection();
            services.AddLogging();
            services.AddHttpClient();
            builder.Populate(services);

            builder.RegisterLogger(autowireProperties: true);
            builder.RegisterSerilog(Logger.LogConfig.GetLoggerConfiguration());

            builder.RegisterControllers(typeof(MvcApplication).Assembly).PropertiesAutowired();
            builder.RegisterModule(new AutofacWebTypesModule());
        }

        private static void ConfigureAppDependencies(ContainerBuilder builder)
        {
            builder.RegisterType<AltinnFormDeserializer>().As<IAltinnFormDeserializer>();

            //Default service setup
            builder.RegisterType<AltinnFormDownloadAndRoutingService>().AsSelf();
            builder.RegisterType<AltinnService_DIBK>().As<IAltinnService>();

            //Specific service setup -- start
            builder.RegisterType<AltinnFormDownloadAndRoutingService_DIBK>().AsSelf();
            builder.RegisterType<AltinnService_DIBK>().AsSelf();

            builder.RegisterType<AltinnFormDownloadAndRoutingService_ATIL>().AsSelf();
            builder.RegisterType<AltinnService_ATIL>().AsSelf();
            //Specific service setup -- end

            builder.RegisterType<AltinnDownloadStreamAttachment>().As<IDownloadStreamAttachment>();
            builder.RegisterType<DownloadQueueExternalBasicClient>().As<IDownloadQueueExternalBasic>();
            builder.RegisterType<FormShippingService>().As<IFormShippingService>();
            builder.RegisterType<MunicipalityService>().As<IMunicipalityService>();

            //Auth services
            builder.RegisterType<APIAuthenticationService>().As<IAPIAuthenticationService>();
            builder.RegisterType<BasicAuthUserAuthenticationService>().As<IBasicAuthUserAuthenticationService>();
            builder.RegisterType<AuthorizationService>().As<IAuthorizationService>();
            builder.RegisterType<TokenService>().As<ITokenService>();

            builder.RegisterType<LogEntryService>().As<ILogEntryService>();
            builder.RegisterType<ApplicationLogService>().As<IApplicationLogService>();
            builder.RegisterType<FormMetadataService>().As<IFormMetadataService>();
            builder.RegisterType<ApplicationUserService>().As<IApplicationUserService>();
            builder.RegisterType<FormService>().As<IFormService>();
            builder.RegisterType<AltinnPrefillService>().As<IAltinnPrefillService>();
            builder.RegisterType<SendPrefillFormServiceV2>().AsSelf();
            builder.RegisterType<FormDistributionServiceV2>().AsSelf();
            builder.RegisterType<ExportToSubmittalQueueService>().AsSelf();
            builder.RegisterType<SendCorrespondenceHelper>().AsSelf();
            builder.RegisterType<AltinnCorrespondenceService>().As<IAltinnCorrespondenceService>();
            builder.RegisterType<CorrespondencePolicyConfigurationProvider>().As<ICorrespondencePolicyConfigurationProvider>();
            builder.RegisterType<CorrespondencePolicyProvider>().As<ICorrespondencePolicyProvider>();
            builder.RegisterType<FileStorage>().As<IFileStorage>();
            builder.RegisterType<PublicFileStorage>().As<IPublicFileStorage>();
            builder.RegisterType<FileDownloadStatusService>().As<IFileDownloadStatusService>();
            builder.RegisterType<DecryptionFactory>().As<IDecryptionFactory>();
            builder.RegisterType<UserNotificationService>().As<IUserNotificationService>();
            builder.RegisterType<UserNotificationHandler>().AsSelf();
            builder.RegisterType<AltinnNotificationService>().As<IAltinnNotificationService>();
            builder.RegisterType<SendSecondNotifications>().As<ISendSecondNotifications>();
            builder.RegisterType<ElasticIndexer>().As<ISearchEngineIndexer>();
            builder.RegisterType<StatisticsService>().As<IStatisticsService>();
            builder.RegisterType<BatchIndexer>().As<IBatchIndexer>();
            builder.RegisterType<SyncRecordsOfCombinedDistributions>().As<ISyncRecordsOfCombinedDistributions>();
            builder.RegisterType<DistributionStatusApiService>().As<IDistributionStatusService>();
            builder.RegisterType<ServiceCodeProvider>().As<IServiceCodeProvider>();
            builder.RegisterType<PrefillPolicyProvider>().As<IPrefillPolicyProvider>();
            builder.RegisterType<PrefillPolicyConfigurationProvider>().As<IPrefillPolicyConfigurationProvider>();
            builder.RegisterType<HealthService>().As<IHealthService>();
            builder.RegisterType<CodeListDownloader>().As<ICodeListDownloader>();
            builder.RegisterType<CodeListService>().As<ICodeListService>();
            builder.RegisterType<DeviationLetterService>().As<IDeviationLetterService>();
            builder.RegisterType<DeviationService>().As<IDeviationService>();
            builder.RegisterType<ReceiptService>().As<IReceiptService>();
            builder.RegisterType<DecisionService>().As<IDecisionService>();
            builder.RegisterType<MunicipalityCacheService>().AsSelf();
            builder.RegisterType<SvarUtShipmentStatusPersister>().As<ISvarUtShipmentStatusPersister>();
            builder.RegisterType<FinalStatusFilter>().As<IFinalStatusFilter>();
            builder.RegisterType<FormMetadataRepository>().As<IFormMetadataRepository>();
            builder.RegisterType<ScheduledShipmentStatusPersister>().AsSelf();
            builder.RegisterType<TextEncryptionService>().As<ITextEncryptionService>();
            builder.RegisterType<TiltakshaverSamtykkeService>().As<ITiltakshaverSamtykkeService>();
            builder.RegisterType<FtIdService>().As<IFtIdService>();

            builder.RegisterType<NotificationServiceResolver>().As<INotificationServiceResolver>();
            builder.RegisterType<NotificationServiceV2>().As<INotificationServiceV2>();
            builder.RegisterType<SamtykkeTilByggeplanNotificationService>().As<INotificationServiceV2>();
            builder.RegisterType<ExportToSubmittalQueueService>().As<INotificationServiceV2>();

            builder.RegisterType<DistributionFormsRepository>().AsSelf();
            builder.RegisterType<HendelserRepository>().AsSelf();

            builder.RegisterType<DecryptFnr>().As<IDecryptFnr>();
            builder.RegisterType<TiltakshaversSignedApprovalDocument>().As<ITiltakshaversSignedApprovalDocument>();

            builder.RegisterType<TiltakshaverSamtykkeCleanUpService>().AsSelf();

            builder.RegisterType<SvarUtShipmentStatusPersister>().As<ISvarUtShipmentStatusPersister>();
            builder.RegisterType<SvarUtStatusService>().As<ISvarUtStatusService>();
            builder.RegisterType<SvarUtPolicyConfigurationProvider>().As<ISvarUtPolicyConfigurationProvider>();
            builder.RegisterType<SvarUtClientFactory>().AsSelf();
            builder.RegisterType<SvarUtRestRetryPolicyProvider>().AsSelf();
            builder.RegisterType<SvarUtRestBuilder>().AsSelf();
            builder.RegisterType<SvarUtRestBuilderPrint>().AsSelf();
            builder.RegisterType<SvarUtRestService>().AsSelf();

            builder.RegisterType<ClientSystemService>().AsSelf();

            builder.RegisterAssemblyTypes(Assembly.GetAssembly(typeof(IHealthCheck)))
                        .Where(t => typeof(IHealthCheck).IsAssignableFrom(t))
                        .As<IHealthCheck>();
        }

        private static void AddIntegrationTestServices(ContainerBuilder builder)
        {
            builder.RegisterType<AltinnServiceMockVarselOppstartAvPlanarbeid>().Named<IAltinnService>("AltinnServiceMockVarselOppstartAvPlanarbeid");
            builder.RegisterType<AltinnServiceMockSvarPaaVarselOppstartAvPlanarbeid>().Named<IAltinnService>("AltinnServiceMockSvarPaaVarselOppstartAvPlanarbeid");
            builder.RegisterType<AltinnServiceMockATILSoknadOmSamtykkeV2>().Named<IAltinnService>("AltinnServiceMockATILSoknadOmSamtykkeV2");

            builder.Register(c => new IntegrationTestAltinnFormDownloadAndRoutingServiceVarselOppstartAvPlanarbeid(
                c.ResolveNamed<IAltinnService>("AltinnServiceMockVarselOppstartAvPlanarbeid"),
                c.Resolve<IAltinnFormDeserializer>(),
                c.Resolve<IFormShippingService>(),
                c.Resolve<ILogEntryService>(),
                c.Resolve<IFormMetadataService>(),
                c.Resolve<FormDistributionServiceV2>(),
                c.Resolve<INotificationServiceResolver>(),
                c.Resolve<SendCorrespondenceHelper>(),
                c.Resolve<IApplicationLogService>(),
                c.Resolve<ExportToSubmittalQueueService>(),
                c.Resolve<ISearchEngineIndexer>(),
                c.Resolve<IFtIdService>())).AsSelf();

            builder.Register(c => new IntegrationTestAltinnFormDownloadAndRoutingServiceSvarPaaVarselOppstartAvPlanarbeid(
                c.ResolveNamed<IAltinnService>("AltinnServiceMockSvarPaaVarselOppstartAvPlanarbeid"),
                c.Resolve<IAltinnFormDeserializer>(),
                c.Resolve<IFormShippingService>(),
                c.Resolve<ILogEntryService>(),
                c.Resolve<IFormMetadataService>(),
                c.Resolve<FormDistributionServiceV2>(),
                c.Resolve<INotificationServiceResolver>(),
                c.Resolve<SendCorrespondenceHelper>(),
                c.Resolve<IApplicationLogService>(),
                c.Resolve<ExportToSubmittalQueueService>(),
                c.Resolve<ISearchEngineIndexer>(),
                c.Resolve<IFtIdService>())).AsSelf();

            builder.Register(c => new IntegrationTestAltinnFormDownloadAndRoutingServiceATILSoknadOmSamtykkeV2(
                c.ResolveNamed<IAltinnService>("AltinnServiceMockATILSoknadOmSamtykkeV2"),
                c.Resolve<IAltinnFormDeserializer>(),
                c.Resolve<IFormShippingService>(),
                c.Resolve<ILogEntryService>(),
                c.Resolve<IFormMetadataService>(),
                c.Resolve<FormDistributionServiceV2>(),
                c.Resolve<INotificationServiceResolver>(),
                c.Resolve<SendCorrespondenceHelper>(),
                c.Resolve<IApplicationLogService>(),
                c.Resolve<ExportToSubmittalQueueService>(),
                c.Resolve<ISearchEngineIndexer>(),
                c.Resolve<IFtIdService>())).AsSelf();
        }
    }
}