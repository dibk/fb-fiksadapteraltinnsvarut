﻿using Microsoft.Owin;
using System;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Caching;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace KommIT.FIKS.AdapterAltinnSvarUt.App_Start
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
    public class RateLimitAttribute : ActionFilterAttribute
    {
        public int TimeValue { get; set; } = 1;
        public TimeUnit TimeUnit { get; set; }
        public int Count { get; set; }

        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            var seconds = Convert.ToInt32(TimeUnit) * TimeValue;

            var key = string.Join(
                "-",
                seconds,
                actionContext.Request.Method,
                actionContext.ControllerContext.ControllerDescriptor.ControllerName,
                actionContext.ActionDescriptor.ActionName,
                GetClientIpAddress(actionContext.Request)
            );

            var count = 1;

            if (HttpRuntime.Cache[key] != null)
                count = (int)HttpRuntime.Cache[key] + 1;

            HttpRuntime.Cache.Insert(
                key,
                count,
                null,
                DateTime.UtcNow.AddSeconds(seconds),
                Cache.NoSlidingExpiration,
                CacheItemPriority.Low,
                null
            );

            if (count > Count)
            {
                var response = new HttpResponseMessage
                {
                    StatusCode = (HttpStatusCode)429,
                    ReasonPhrase = "Too Many Requests",
                    Content = new StringContent($"For mange forepørsler. Prøv igjen om {seconds} sekunder.")
                };

                response.Headers.Add("Retry-After", seconds.ToString());

                actionContext.Response = response;
            }
        }

        private static string GetClientIpAddress(HttpRequestMessage request)
        {
            if (request.Properties.ContainsKey("MS_HttpContext"))
                return IPAddress.Parse(((HttpContextBase)request.Properties["MS_HttpContext"]).Request.UserHostAddress).ToString();

            if (request.Properties.ContainsKey("MS_OwinContext"))
                return IPAddress.Parse(((OwinContext)request.Properties["MS_OwinContext"]).Request.RemoteIpAddress).ToString();

            return string.Empty;
        }
    }

    public enum TimeUnit
    {
        Minute = 60,
        Hour = 3600,
        Day = 86400
    }
}
