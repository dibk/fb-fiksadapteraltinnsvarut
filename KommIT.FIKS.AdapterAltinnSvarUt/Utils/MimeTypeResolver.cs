﻿using MimeTypes;
using System.IO;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Utils
{
    public static class MimeTypeResolver
    {
        public static string GetMimeType(string filePath)
        {
            string mimeType;
            try
            {
                var fileExtension = Path.GetExtension(filePath);

                if (fileExtension.ToLower().Equals(".xml")) // MimeTypeMap maps xml to text/xml which is deprecated. https://www.w3.org//2006/02/son-of-3023/draft-murata-kohn-lilley-xml-04.html#textxml
                    mimeType = "application/xml";
                else if (fileExtension.ToLower().Equals(".sos"))
                    mimeType = "application/sosi";

                // BuildingSmart file types
                else if (fileExtension.ToLower().Equals(".ifc"))
                    mimeType = "application/x-step";
                else if (fileExtension.ToLower().Equals(".ifcXML"))
                    mimeType = "application/xml";
                else if (fileExtension.ToLower().Equals(".ifcZIP"))
                    mimeType = "application/zip";

                // General mimetype handler
                else
                    mimeType = MimeTypeMap.GetMimeType(fileExtension);
            }
            catch
            {
                //https://serverfault.com/questions/506406/iis7-responds-with-a-404-when-extension-is-not-listed-in-the-mime-types-cant-i
                mimeType = "application/octet-stream";
            }

            return mimeType;
        }
    }
}