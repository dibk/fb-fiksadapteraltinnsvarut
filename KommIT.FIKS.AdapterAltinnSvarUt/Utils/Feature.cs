﻿using System;
using System.Configuration;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Utils
{
    public class Feature
    {
        /// <summary>
        /// Function to check a feature flag.
        /// Returns FALSE if the flag is specifically set to false, otherwise true
        /// </summary>
        /// <param name="featureFlag"></param>
        /// <returns>false if flag is turned off, true otherwise</returns>
        public static bool IsFeatureEnabled(FeatureFlags featureFlag)
        {
            //<add key="FeatureFlag.MatrikkelValidering" value=""/>
            bool isEnabled;
            string settingsTag = $"FeatureFlag.{featureFlag.ToString()}";
            string featureValue = ConfigurationManager.AppSettings[settingsTag];

            if (string.IsNullOrEmpty(featureValue))
            {
                // If flag is not found the serivice is turned on
                isEnabled = true;
            }
            else if (featureValue.Equals("false", StringComparison.InvariantCultureIgnoreCase))
            {
                // If flag is specifically turned off
                isEnabled = false;
            }
            else
            {
                // Defaults to turned on
                isEnabled = true;
            }
            return isEnabled;
        }
    }

    public enum FeatureFlags
    {
        MatrikkelValidering,
        BringPostnummerValidering,
        UnitTestExternalDependence,
        UseNabovarselValidatiorV4
    }

}