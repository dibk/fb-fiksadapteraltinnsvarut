﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Utils
{
    public interface IServiceCodeProvider
    {
        List<string> DistributionServiceCodes { get; }
        List<string> ShippingServiceCodes { get; }
        List<string> NotificationServiceCodes { get; }

        string MessageServiceCode { get; }
        int MessageServiceCodeEdition { get; }
    }
    public class ServiceCodeProvider : IServiceCodeProvider
    {
        private readonly string _serviceCodeShipping;
        private readonly string _serviceCodeDistribution;
        private readonly string _serviceCodeNotification;
        private readonly string _messageServiceCode;
        private readonly int _messageServiceCodeEdition;
        private readonly char[] _separator = { ',' };

        private List<string> _serviceCodeDistributionList = new List<string>();
        private List<string> _serviceCodeNotificationList = new List<string>();
        private List<string> _serviceCodeShippingList = new List<string>();
        public List<string> DistributionServiceCodes
        {
            get
            {
                if (_serviceCodeDistributionList.Count < 1)
                    _serviceCodeDistributionList = GetCodes(_serviceCodeDistribution);
                return _serviceCodeDistributionList;
            }
        }

        public List<string> ShippingServiceCodes
        {
            get
            {
                if (_serviceCodeShippingList.Count < 1)
                    _serviceCodeShippingList = GetCodes(_serviceCodeShipping);
                return _serviceCodeShippingList;
            }
        }

        public List<string> NotificationServiceCodes
        {
            get
            {
                if (_serviceCodeNotificationList.Count < 1)
                    _serviceCodeNotificationList = GetCodes(_serviceCodeNotification);
                return _serviceCodeNotificationList;
            }
        }

        public string MessageServiceCode
        {
            get
            {
                return _messageServiceCode;
            }
        }

        public int MessageServiceCodeEdition
        {
            get
            { return _messageServiceCodeEdition; }
        }


        public ServiceCodeProvider()
        {
            _serviceCodeShipping = ConfigurationManager.AppSettings["Altinn.ServiceCodeShipping"];
            _serviceCodeDistribution = ConfigurationManager.AppSettings["Altinn.ServiceCodeDistributuion"];
            _serviceCodeNotification = ConfigurationManager.AppSettings["Altinn.ServiceCodeNotification"];
            _messageServiceCode = ConfigurationManager.AppSettings["MessageServiceCode"];
            _messageServiceCodeEdition = int.Parse(ConfigurationManager.AppSettings["MessageServiceCodeEdition"]);
        }

        private List<string> GetCodes(string delimitedCodeList)
        {
            return delimitedCodeList.Split(_separator, StringSplitOptions.RemoveEmptyEntries).Select(p => p.Trim()).ToList();
        }
    }
}