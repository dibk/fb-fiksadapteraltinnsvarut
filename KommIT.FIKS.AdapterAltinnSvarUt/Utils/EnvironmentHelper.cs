﻿using System;
using System.Configuration;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Utils
{
    public class EnvironmentHelper
    {
        public static string GetEnvironment()
        {
            return ConfigurationManager.AppSettings["Environment"] ?? "Production";
        }

        public static bool IsDevelopment() => GetEnvironment().Equals("Development", StringComparison.OrdinalIgnoreCase);

        public static bool IsStaging() => GetEnvironment().Equals("Staging", StringComparison.OrdinalIgnoreCase);

        public static bool IsProduction() => GetEnvironment().Equals("Production", StringComparison.OrdinalIgnoreCase);
    }
}