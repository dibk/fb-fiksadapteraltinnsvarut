﻿using System;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Utils
{
    public static class DateTimeHelpers
    {
        /// <summary>
        /// Konverterer til UTC da ELK krever det for søk og korrekt visning i Kibana
        /// Dette krever at tjenesten kjører med WEBSITE_TIME_ZONE satt til 'UTC + 1'
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static DateTime? ToElkDateTime(this DateTime? dateTime)
        {
            if (dateTime == null || !dateTime.HasValue)
                return dateTime;

            return TimeZoneInfo.ConvertTimeToUtc(dateTime.Value, TimeZoneInfo.Local);
        }
    }
}