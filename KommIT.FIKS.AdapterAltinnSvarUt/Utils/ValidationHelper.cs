﻿using System;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Utils
{
    public class ValidationHelper
    {
        /// <summary>
        /// Vi runder av for å unngå små forskjeller som fører til feil
        /// <param name="utfyltareal"></param>
        /// <param name="beregnetareal"></param>
        /// <returns></returns>
        public static bool UtfyltOgBeregnetArealErDetSamme(double utfyltareal, double beregnetareal)
        {
            // https://dibk.atlassian.net/browse/FBHS-1524 , https://stackoverflow.com/a/977807

            var utfyltarealRound = Math.Round(utfyltareal, 2, MidpointRounding.AwayFromZero);
            var beregnetarealRound = Math.Round(beregnetareal, 2, MidpointRounding.AwayFromZero);

            var isTheSameNumber = utfyltarealRound.Equals(beregnetarealRound);
            // TODO waiting response from Hilda
            /// https://arkitektum.atlassian.net/wiki/spaces/FBYGG/pages/1138786315/Testprosedyre+Tiltak+uten+ansvar+versjon+3
            /// rule ID 4373.3.18.11
            //if (!isTheSameNumber)
            //{
            //    var beregnetarealRoundUp = RoundUp(beregnetareal, 2);
            //    isTheSameNumber = utfyltarealRound.Equals(beregnetarealRoundUp);
            //}

            return isTheSameNumber;
        }

        /// <summary>
        /// reound up function to calculate 
        /// </summary>
        /// <param name="input">number to round</param>
        /// <param name="places">decimal place</param>
        /// <returns></returns>
        public static double RoundUp(double input, int places)
        {
            double multiplier = Math.Pow(10, Convert.ToDouble(places));
            return Math.Ceiling(input * multiplier) / multiplier;
        }
        /// <summary>
        /// Beregne grad av utnyttings areal
        /// </summary>
        /// <param name="eksisterende">Areal bebyggelse eksisterende</param>
        /// <param name="skalRives">Areal bebyggelse som skal rives</param>
        /// <param name="nytt">Areal bebyggelse nytt</param>
        /// <param name="parkeringsarealTerreng">Parkeringsareal terreng</param>
        /// <returns></returns>
        public static double BeregnetGradAvUtnyttingAreal(double eksisterende, double skalRives, double nytt, double parkeringsarealTerreng)
        {
            return eksisterende - skalRives + nytt + parkeringsarealTerreng;
        }

        /// <summary>
        /// Beregne grad av utnyttings areal
        /// </summary>
        /// <param name="eksisterende">Areal bebyggelse eksisterende</param>
        /// <param name="skalRives">Areal bebyggelse som skal rives</param>
        /// <param name="nytt">Areal bebyggelse nytt</param>
        /// <param name="parkeringsarealTerreng">Parkeringsareal terreng</param>
        /// <returns></returns>
        public static double BeregnetGradAvUtnyttingArealProsent(double eksisterende, double skalRives, double nytt, double parkeringsarealTerreng, double tomteareal)
        {
            return BeregnetGradAvUtnyttingAreal(eksisterende, skalRives, nytt, parkeringsarealTerreng) / tomteareal * 100;
        }

    }
}