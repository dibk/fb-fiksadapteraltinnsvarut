﻿using KommIT.FIKS.AdapterAltinnSvarUt.Extensions;
using System;
using System.IO;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Utils
{

    public class SerializeUtil
    {
        public static T DeserializeFromString<T>(string objectData)
        {
            return (T)DeserializeFromString(objectData, typeof(T));
        }

        private static object DeserializeFromString(string objectData, Type type)
        {
            var serializer = XmlSerializerCache.Instance.GetSerializer(type);

            object result;

            using (var reader = new StringReader(objectData))
            {
                try
                {
                    result = serializer.Deserialize(reader);
                }
                catch (Exception ex)
                {
                    var errorMessage = ex.ToFormattedString();
                    throw new DeserializationException(errorMessage, ex);
                }
            }

            return result;
        }

        public static string Serialize(object form)
        {
            var serializer = XmlSerializerCache.Instance.GetSerializer(form.GetType());

            using (var stringWriter = new Utf8StringWriter())
            {
                serializer.Serialize(stringWriter, form);
                return stringWriter.ToString();
            }
        }
    }

    public class DeserializationException : Exception
    {
        public DeserializationException(string message, Exception ex) : base(message, ex)
        {
        }
    }
}