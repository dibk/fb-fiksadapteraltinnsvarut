﻿using KommIT.FIKS.AdapterAltinnSvarUt.Services;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Schema;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Utils
{
    public class XmlValidator
    {
        private const int _validationErrorCountLimit = 100;
        private readonly ValidationResult _validationErrorMessages = new ValidationResult(0, 0);

        public ValidationResult Validate(string xmlString, string xmlSchemaString)
        {
            using (var xmlSchemaStream = new MemoryStream(Encoding.UTF8.GetBytes(xmlSchemaString)))
            {
                using (var xmlStream = new MemoryStream(Encoding.UTF8.GetBytes(xmlString)))
                {
                    return Validate(xmlStream, xmlSchemaStream);
                }
            }
        }

        public ValidationResult Validate(Stream xmlStream, Stream xmlSchemaStream)
        {
            XmlSchema xmlSchema = XmlSchema.Read(xmlSchemaStream, ValidationCallBack);
            XmlReaderSettings xmlReaderSettings = SetupXmlValidation(new List<XmlSchema> { xmlSchema });
            Validate(xmlStream, xmlReaderSettings);

            return _validationErrorMessages;
        }

        public ValidationResult Validate(Stream xmlStream, Stream[] xmlSchemaStreams)
        {
            var xmlSchemas = new List<XmlSchema>();

            foreach (Stream xmlSchemaStream in xmlSchemaStreams)
                xmlSchemas.Add(XmlSchema.Read(xmlSchemaStream, ValidationCallBack));

            XmlReaderSettings xmlReaderSettings = SetupXmlValidation(xmlSchemas);
            Validate(xmlStream, xmlReaderSettings);

            return _validationErrorMessages;
        }

        private void Validate(Stream xmlStream, XmlReaderSettings xmlReaderSettings)
        {
            using (XmlReader validationReader = XmlReader.Create(xmlStream, xmlReaderSettings))
            {
                try
                {
                    while (validationReader.Read())
                        if (_validationErrorMessages.messages.Count() >= _validationErrorCountLimit)
                            break;
                }
                catch (XmlException ex)
                {
                    _validationErrorMessages.AddError(ex.Message, "XSD validering");
                }
            }
        }

        private XmlReaderSettings SetupXmlValidation(IEnumerable<XmlSchema> xmlSchemas)
        {
            var settings = new XmlReaderSettings();
            settings.ValidationType = ValidationType.Schema;
            settings.ValidationFlags |= XmlSchemaValidationFlags.ReportValidationWarnings;
            settings.ValidationEventHandler += ValidationCallBack;

            foreach (XmlSchema xmlSchema in xmlSchemas)
            {
                settings.Schemas.Add(xmlSchema);
            }

            return settings;
        }

        private void ValidationCallBack(object sender, ValidationEventArgs args)
        {
            if (args.Severity == XmlSeverityType.Warning)
            {
                _validationErrorMessages.AddWarning("linje " + args.Exception.LineNumber + ", posisjon " + args.Exception.LinePosition + " " + args.Message, "XSD validering");
            }
            else if (args.Severity == XmlSeverityType.Error)
            {
                _validationErrorMessages.AddError("linje " + args.Exception.LineNumber + ", posisjon " + args.Exception.LinePosition + " " + args.Message, "XSD validering");
            }
        }
    }
}