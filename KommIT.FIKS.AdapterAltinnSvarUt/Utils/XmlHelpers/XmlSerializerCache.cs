﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Utils
{
    public class XmlSerializerCache
    {
        private static XmlSerializerCache instance;
        private static readonly object _lock = new object();

        public static XmlSerializerCache Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (_lock)
                    {
                        if (instance == null)
                        {
                            instance = new XmlSerializerCache();
                        }
                    }
                }
                return instance;
            }
        }

        private Dictionary<Type, XmlSerializer> _serializers = new Dictionary<Type, XmlSerializer>();

        public XmlSerializer GetSerializer(Type type)
        {
            if (!_serializers.TryGetValue(type, out var serializer))
            {
                serializer = new XmlSerializer(type);
                _serializers[type] = serializer;
            }
            return serializer;
        }
    }
}