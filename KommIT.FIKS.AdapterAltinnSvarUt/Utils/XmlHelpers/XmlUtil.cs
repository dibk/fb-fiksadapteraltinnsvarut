﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;

internal static class XmlUtil
{
    public static List<string> GetCodeListByElementNameOrXpath(string xmlString, string elementName, string xpath = null)
    {
        var codeList = new List<string>();
        try
        {
            XDocument xmlDoc = XDocument.Parse(RemoveAllNamespaces(xmlString));

            IEnumerable<XElement> xElements;
            xElements = !string.IsNullOrEmpty(xpath) ? xmlDoc.XPathSelectElements(xpath) : xmlDoc.Descendants(XName.Get(elementName));

            foreach (var xElement in xElements)
            {
                var kodeverdi = xElement.Element(XName.Get("kodeverdi"));
                var kodebeskrivelse = xElement.Element(XName.Get("kodebeskrivelse"));
                if (kodeverdi != null && kodebeskrivelse != null)
                {
                    codeList.Add(kodeverdi.Value);
                }
            }
        }
        catch (Exception)
        {
            return null;
        }
        return codeList;
    }

    public static string GetElementValue(string xmlString, string elementName)
    {
        try
        {
            XDocument document = XDocument.Parse(RemoveAllNamespaces(xmlString));
            var node = document.Descendants().Where(e => e.Name.LocalName.ToLower().Equals(elementName.ToLower())).FirstOrDefault();

            return node?.Value?.Trim();
        }
        catch (Exception)
        {
            return null;
        }
    }

    private static string RemoveAllNamespaces(string xmlDocument)
    {
        XElement xmlDocumentWithoutNs = RemoveAllNamespaces(XElement.Parse(xmlDocument));

        return xmlDocumentWithoutNs.ToString();
    }

    //Core recursion function
    private static XElement RemoveAllNamespaces(XElement xmlDocument)
    {
        if (!xmlDocument.HasElements)
        {
            XElement xElement = new XElement(xmlDocument.Name.LocalName);
            xElement.Value = xmlDocument.Value;

            foreach (XAttribute attribute in xmlDocument.Attributes())
                xElement.Add(attribute);

            return xElement;
        }
        return new XElement(xmlDocument.Name.LocalName, xmlDocument.Elements().Select(el => RemoveAllNamespaces(el)));
    }
}