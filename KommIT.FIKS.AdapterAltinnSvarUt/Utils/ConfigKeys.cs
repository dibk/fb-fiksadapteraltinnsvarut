﻿namespace KommIT.FIKS.AdapterAltinnSvarUt.Utils
{
    public class ConfigKeys
    {
        public const string SearchEngineBatchSize = "SearchEngine:BatchSize";
    }
}