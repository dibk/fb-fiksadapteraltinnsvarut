﻿namespace KommIT.FIKS.AdapterAltinnSvarUt.Utils
{
    public static class StringExtensions
    {
        public static string ToMD5Hash(this string input)
        {
            return CryptographyHelpers.GenerateMD5Hash(input);
        }
    }
}