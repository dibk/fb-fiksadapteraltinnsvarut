﻿using System.Text.RegularExpressions;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Utils
{
    public class TextObfuscation
    {
        public static string ObfuscateSSN(string message)
        {
            if (string.IsNullOrEmpty(message))
                return message;

            Regex pattern = new Regex(@"\b[0-9]{11}\b");
            Match match = pattern.Match(message);
            string possibleSsn;
            if (match.Success)
                possibleSsn = match.Value;
            else
                possibleSsn = string.Empty;

            if (!string.IsNullOrEmpty(possibleSsn))
                message = message.Replace(possibleSsn, "ANONYMISERT");

            return message;
        }

        public static bool IsSSN(string ssnCandidate)
        {
            Regex pattern = new Regex(@"\b[0-9]{11}\b");
            Match match = pattern.Match(ssnCandidate);
            return match.Success;
        }
    }
}