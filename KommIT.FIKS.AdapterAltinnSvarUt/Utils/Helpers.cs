using KommIT.FIKS.AdapterAltinnSvarUt.Extensions;
using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Services;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Encryption;
using System;
using System.Collections;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Text.RegularExpressions;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Utils
{
    public class ReporteeInformation
    {
        public string IdNumber { get; set; }
        public string IdType { get; set; }

        public ReporteeInformation() { }
        public ReporteeInformation(string idNumber, string idType)
        {
            IdNumber = idNumber;
            IdType = idType;
        }
    }
    public class Helpers
    {
        /// <summary>
        /// Returns organisasjonsnummer if found. If no ssn will try to return personnummer. If neither is found returns null
        /// </summary>
        /// <param name="orgnum"></param>
        /// <param name="ssn"></param>
        /// <returns>string or null if none found</returns>
        public static ReporteeInformation ReturnFirstOrgNumberOrPersonalNumber(string orgnum, string ssn)
        {
            if (!orgnum.IsNullOrWhiteSpace())
            {
                return new ReporteeInformation(orgnum, "organisasjonsnummer");
            }
            else if (!ssn.IsNullOrWhiteSpace())
            {
                return new ReporteeInformation(ssn, "fødselsnummer");
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Validates email address 
        /// https://docs.microsoft.com/en-us/dotnet/standard/base-types/how-to-verify-that-strings-are-in-valid-email-format
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public static bool IsValidEmailAddress(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
                return false;

            try
            {
                // Normalize the domain
                email = Regex.Replace(email, @"(@)(.+)$", DomainMapper,
                                      RegexOptions.None, TimeSpan.FromMilliseconds(200));

                // Examines the domain part of the email and normalizes it.
                string DomainMapper(Match match)
                {
                    // Use IdnMapping class to convert Unicode domain names.
                    var idn = new IdnMapping();

                    // Pull out and process domain name (throws ArgumentException on invalid)
                    string domainName = idn.GetAscii(match.Groups[2].Value);

                    return match.Groups[1].Value + domainName;
                }
            }
            catch (RegexMatchTimeoutException e)
            {
                return false;
            }
            catch (ArgumentException e)
            {
                return false;
            }

            try
            {
                return Regex.IsMatch(email,
                    @"^[^@\s]+@[^@\s]+\.[^@\s.]+$",
                    RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }
        }

        public static bool ObjectIsNullOrEmpty(object mainObject)
        {
            if (mainObject == null) return true;

            // Simple types
            if (IsSimple(mainObject.GetType()))
            {
                var stringValue = mainObject.ToString();
                return string.IsNullOrEmpty(stringValue);
            }

            //Complex types - Arrays
            if (mainObject.GetType().IsArray)
            {
                foreach (dynamic objectItem in (IEnumerable)mainObject)
                {
                    var isNullOrEmpty = ObjectIsNullOrEmpty(objectItem);
                    if (!isNullOrEmpty)
                        return false;
                }
                return true;

            }

            //Complex type - Child properties
            if (mainObject.GetType().GetProperties().Count() > 0)
            {   
                var props = mainObject.GetType().GetProperties().Where(p => !p.Name.EndsWith("Specified"));

                foreach (var propertyInfo in props)
                {
                    var isNullOrEmpty = ObjectIsNullOrEmpty(propertyInfo.GetValue(mainObject, null));
                    if (!isNullOrEmpty)
                        return false;
                }
            }

            return true;
        }


        public static string GetFullClassPath<T>(Expression<Func<T>> action)
        {

            var path = action.Body.ToString();
            int index = path.LastIndexOf("form.", StringComparison.InvariantCultureIgnoreCase);

            var classPathTemp = index > 0 ? path.Remove(0, index) : path.Remove(0, path.IndexOf(")", StringComparison.Ordinal) + 2);

            string classPath = String.Empty;
            int index2 = classPathTemp.LastIndexOf(".form", StringComparison.InvariantCultureIgnoreCase);


            if (index2 > 0 && classPathTemp.Length == index2 + 5 && !classPathTemp.Contains("form."))
            {
                classPath = classPathTemp.Remove(0, index2 + 1);
            }
            else
            {
                classPath = classPathTemp;
            }

            classPath = classPath.Replace(".", "/");
            return classPath;
        }

        public static bool IsSimple(Type type)
        {
            // https://stackoverflow.com/questions/863881/how-do-i-tell-if-a-type-is-a-simple-type-i-e-holds-a-single-value
            if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
            {
                // nullable type, check if the nested type is simple.
                return IsSimple(type.GetGenericArguments()[0]);
            }
            return type.IsPrimitive
                   || type.IsEnum
                   || type.Equals(typeof(string))
                   || type.Equals(typeof(decimal));
        }


        public static bool ValidateEntityOfAltinnSubmitterDefault(string reportee, PropertyIdentifiers propertyIdentifiers)
        {
            if (reportee == propertyIdentifiers.AnsvarligSokerFnr) return true;
            else if (reportee == propertyIdentifiers.AnsvarligSokerOrgnr) return true;
            else if (reportee == propertyIdentifiers.AnsvarligForetakOrgNr) return true;
            else return false;
        }

        public static bool ValidateEntityOfAltinnSubmitterForAnsvarligSokerOrTiltakshaver(string reportee, PropertyIdentifiers propertyIdentifiers)
        {
            if (reportee == propertyIdentifiers.AnsvarligSokerFnr) return true;
            else if (reportee == propertyIdentifiers.AnsvarligSokerOrgnr) return true;
            else if (reportee == propertyIdentifiers.AnsvarligForetakOrgNr) return true;
            else if (reportee == propertyIdentifiers.TiltakshaversOrgnr) return true;
            else if (reportee == propertyIdentifiers.TiltakshaversFnr) return true;
            else return false;
        }

        public static string GetDecryptedFnr(string fnr)
        {
            string plainFnr = string.Empty;

            if (!string.IsNullOrWhiteSpace(fnr))
            {
                if (fnr.Length > 11)
                {
                    if (!new DecryptionFactory().GetDecryptor().TryDecryptText(fnr, out plainFnr))
                        plainFnr = fnr;
                }
                else
                {
                    plainFnr = fnr;
                }
            }

            return plainFnr;
        }

        public static void RecordErrorAltinnSubmitter(ValidationResult validationResult)
        {
            RecordValidationError(validationResult, "Søker angitt i skjema er ikke samme som avsender(pålogget bruker) i Altinn.", "Submitter");
        }

        public static void RecordValidationError(ValidationResult validationResult, string message, string reference)
        {
            validationResult?.AddError(message, reference);
        }
    }
}