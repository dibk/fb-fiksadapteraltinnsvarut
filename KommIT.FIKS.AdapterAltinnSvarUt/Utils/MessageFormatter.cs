﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Utils
{
    public class MessageFormatter
    {
        public static string ReplaceStaticArchiveReferenceWithActual(string html, string archiveReference)
        {
            string staticAR = "ARXXXXXX";
            return html.Replace(staticAR, archiveReference);
        }
    }
}