﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.FileDownload;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Utils
{
    public class UtilHelpers
    {
        private readonly string _serverURL;
        public UtilHelpers()
        {
            _serverURL = ConfigurationManager.AppSettings["ServerURL"];
        }
        public string GenerateDownloadApiUri(FileDownloadStatus fileDownload)
        {
            //return $"{_serverURL}api/download/{fileDownload.Guid.ToString()}/{fileDownload.Filename}";
            return new FileDownloadApiUrlProvider(_serverURL).GenerateDownloadUri(fileDownload);
        }
    }
}