﻿var vueModel = new Vue({
    el: '#vue-tiltakshaver',
    components: {
        Samtykke: Samtykke,
        Signering: Signering,
        Erklaering: Erklaering,
        Kvittering: Kvittering
    },
    data: {
        appSettings: appSettings,
        steps: {
            1: { name: 'Samtykke til byggeplaner', hasErrors: false },
            2: { name: 'Velg tiltakshaver', hasErrors: false },
            3: { name: 'Gi ditt samtykke', hasErrors: false },
            4: { name: 'Kvittering', hasErrors: false }
        },
        activeStepNumber: 1,
        guid: null,
        availableReportees: null,
        selectedReportee: null,
        attachmentsStatus: null,
        archiveReference: null,
        checkedSamtykke: false,
        serviceCode: null,
        serviceEdition: null,
        samtykketiltakshaverStatus: null,
        hasRequestedStatus: false,
        signOutUrl: `${appSettings.Altinn.Server}/ui/Authentication/Logout?languageID=1044`,
        altinnAuthCookieTimerStart: null,
        altinnAuthCookieTimer: null,
        altinnAuthCookieTimerIsExceeded: false
    },
    created: function () {
        this.guid = this.getGuidFromUrl();
        this.serviceCode = this.getServiceCodeFromUrl();
        this.serviceEdition = this.getServiceEditionCodeFromUrl();
        this.getSamtykketiltakshaverStatus();
        this.activeStepNumber = this.getActiveStepNumberFromUrl();
        this.fetchReportees().then((data) => {
            this.resetAltinnAuthCookieTimer();
            setInterval(() => {
                this.updateAltinnAuthCookieTimer();
            }, 10000)
        });
        
    },
    methods: {
        stepsHasError() {
            return Object.keys(this.steps).filter(function (stepNumber) {
                return this.steps[stepNumber].hasErrors;
            }.bind(this)).length > 0;
        },
        getStepNumberLabel(stepNumber) {
            if (this.steps[stepNumber].hasErrors) {
                return '✘';
            } else if (this.activeStepNumber > stepNumber) {
                return '✔';
            } else {
                return stepNumber;
            }
        },
        getActiveStepNumberFromUrl() {
            var step = new URL(window.location.href).searchParams.get("steg");
            return step && !isNaN(step)
                ? new URL(window.location.href).searchParams.get("steg")
                : '1';
        },
        getServiceCodeFromUrl() {

            var serviceCode = new URL(window.location.href).searchParams.get("serviceCode");
            return serviceCode ? serviceCode : null;
        },
        getServiceEditionCodeFromUrl() {
            var serviceEdition = new URL(window.location.href).searchParams.get("serviceEdition");
            return serviceEdition ? serviceEdition : null;
        },
        getGuidFromUrl() {
            var pathname = new URL(window.location.href).pathname;
            var guid = pathname.substr(pathname.lastIndexOf('/') + 1);
            return guid ? guid : null;
        },
        getActiveStep() {
            return this.steps[this.getActiveStepNumberFromUrl()];
        },
        setActiveStepNumber(stepNumber) {
            this.activeStepNumber = stepNumber;
            var queryString = '?steg=' + stepNumber;
            window.history.pushState(null, null, window.location.pathname + queryString);
        },
        setSelectedReportee(reportee) {
            this.selectedReportee = reportee;
        },
        updateArchiveReference(archiveReference) {
            this.archiveReference = archiveReference;
        },
        setCheckedSamtykke(checkedSamtykke) {
            this.checkedSamtykke = checkedSamtykke;
        },
        getSamtykketiltakshaverStatus() {            
            $.ajax({
                url: '/api/internal/samtykketiltakshaver/' + this.guid,
                success: function (data, status, jqHxr) {
                    this.samtykketiltakshaverStatus = data;
                    this.hasRequestedStatus = true;
                    const hasStatusOpprettet = data.Status && data.Status === 'Opprettet';
                    if (!hasStatusOpprettet && this.activeStepNumber !== '1') {
                        this.activeStepNumber = '1';
                    }
                }.bind(this),
                error: function (jqHxr, textStatus, errorThrown) {
                    console.log(jqHxr);
                    this.hasRequestedStatus = true;
                }.bind(this)
            });
        },
        resetAltinnAuthCookieTimer() {
            this.altinnAuthCookieTimerStart = Date.now();
            this.altinnAuthCookieTimer = 0;
            this.altinnAuthCookieTimerIsExceeded = false;
        },
        updateAltinnAuthCookieTimer() {
            this.altinnAuthCookieTimer = this.altinnAuthCookieTimerStart ? Date.now() - this.altinnAuthCookieTimerStart : null;
            const minutesBeforeNotification = 20;
            const minutesBeforeLogout = 21;

            if (this.altinnAuthCookieTimer >= this.minutesToMilliseconds(minutesBeforeNotification)) {
                this.altinnAuthCookieTimerIsExceeded = true;
            }
            if (this.altinnAuthCookieTimer >= this.minutesToMilliseconds(minutesBeforeLogout)) {
                window.location.href = this.signOutUrl;
            }
        },
        minutesToMilliseconds(minutes) {
            return minutes * 60 * 1000;
        },
        fetchReportees(skip) {
            const serviceCode = 5303;
            const serviceEdition = 1;
            const resultsPerPage = 50; // Maximum for Altinn is 50
            
            const serviceCodeParam = `&serviceCode=${serviceCode}`;
            const serviceEditionParam = `&serviceEdition=${serviceEdition}`;
            const resultsPerPageParam = `&$top=${resultsPerPage}`;
            const skipParam = skip ? `&$skip=${skip}` : '';

            return $.ajax({
                url: this.appSettings.Altinn.Server + `/api/reportees?showConsentReportees=false${resultsPerPageParam}${skipParam}&$orderby=Type desc${serviceCodeParam}${serviceEditionParam}`,
                headers: {
                    'ApiKey': this.appSettings.Altinn.ApiKey,
                    'Accept': 'application/hal+json'
                },
                xhrFields: {
                    withCredentials: true
                },
                success: function (data, status, jqHxr) {
                    const fetchedReportees = data && data._embedded && data._embedded.reportees ? data._embedded.reportees : null;
                    this.availableReportees = Array.isArray(this.availableReportees) ? this.availableReportees.concat(fetchedReportees) : fetchedReportees;
                    if (fetchedReportees.length === resultsPerPage) {
                        this.fetchReportees(this.availableReportees.length, true);
                    }
                    this.resetAltinnAuthCookieTimer();
                }.bind(this),
                error: function (jqHxr, textStatus, errorThrown) {
                    this.activeStepNumber = '1';
                    console.log(jqHxr);
                }.bind(this)
            });
        }
    }
});
