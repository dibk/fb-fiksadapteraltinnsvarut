﻿
function setFixedHeader(containerHeight, fixedHeaderHeight) {
    if ($(".fixed-header").length) {
        $(".fixed-header").css('bottom', containerHeight + 'px');
        $("#fixed-header").css('width', $("#fixed-header").outerWidth() + 'px');
        var fixedHeaderPositionOffset = $(".fixed-header").offset();
        var fixedHeaderPosition = fixedHeaderPositionOffset.top;
        $(window).scroll(function () {
            var scrollPosition = $("body").scrollTop();

            if (scrollPosition > fixedHeaderPosition &&
                scrollPosition < fixedHeaderPosition + containerHeight - fixedHeaderHeight) {
                $(".fixed-header").addClass("fixed-header-fixed");
            } else if (scrollPosition > fixedHeaderPosition + containerHeight - fixedHeaderHeight) {
                $(".fixed-header").removeClass("fixed-header-fixed");
                $(".fixed-header").css('bottom', fixedHeaderHeight + 'px');
            } else {
                $(".fixed-header").removeClass("fixed-header-fixed");
                $(".fixed-header").css('bottom', containerHeight + 'px');
            }
        });
    }
}


$(document).ready(function () {

    $(".modal-show").click(function () {
        var targetId = $(this).data("modal-toggle");
        $("#" + targetId).show();
    });
    $(".modal-close").click(function () {
        var targetId = $(this).data("modal-toggle");
        $("#" + targetId).hide();
    });
    function addDocumentType(documentType, documentTypeSelectListId) {
        // ajax kall som sender inn documentType

        documentType.MunicipalityCode = $("#MunicipalityCode").val();


        $.ajax({
            type: "POST",
            url: "/api/municipalityFormDocumentType",
            data: documentType,
            success: console.log("yiepppaykayay")
        });


        $("#" + documentTypeSelectListId).append("<option value='" + documentType.Code + "' selected='selected'>" + documentType.Name + "</option>");


    }

    $(".add-document-type").click(function () {

        var nameFieldId = $(this).data("name-field-id");
        var codeFieldId = $(this).data("code-field-id");
        var documentTypeSelectListId = $(this).data("document-type-select-list-id");
        var documentType = {
            Name: $("#" + nameFieldId).val(),
            Code: $("#" + codeFieldId).val()
        }
        addDocumentType(documentType, documentTypeSelectListId);
    });

    $('.select2-select').select2({ tags: true });
    $('.select2-selection__arrow').hide();

    if ($("#fixed-header-container").length) {
        $('#fixed-header-container').bind("DOMSubtreeModified",
            function () {
                var containerHeight = $("#fixed-header-container").height();
                var fixedHeaderHeight = $("#fixed-header-items").outerHeight();
                setFixedHeader(containerHeight, fixedHeaderHeight);
            });
    }


});

function showLoadingAnimation() {
    $("#loading-animation").show();
}

function hideLoadingAnimation() {
    $("#loading-animation").hide();
}


