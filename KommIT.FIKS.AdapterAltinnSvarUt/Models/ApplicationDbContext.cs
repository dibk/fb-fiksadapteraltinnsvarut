﻿using KommIT.FIKS.AdapterAltinnSvarUt.Models.EntityConfiguration;
using KommIT.FIKS.AdapterAltinnSvarUt.Models.Signatur;
using KommIT.FIKS.AdapterAltinnSvarUt.Models.UserAccess;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Data.SqlClient;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.SqlServer;
using System.Data.Entity.Validation;
using System.Globalization;
using System.Linq;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Models
{
    //Uses MicrosoftSqlDbConfiguration from package Microsoft.EntityFramework.SqlServer
    // to be able to use Microsoft.Data.SqlClient and EF6
    public class ApplicationDbContextConfiguration : MicrosoftSqlDbConfiguration
    {
        public ApplicationDbContextConfiguration() : base()
        {
            //Add strategy for doing retries if the database is unreachable.
            //Example of exceptions that are retried:
            //    An exception has been raised that is likely due to a transient failure.
            //    If you are connecting to a SQL Azure database consider using SqlAzureExecutionStrategy.,
            //      An error occurred while updating the entries. See the inner exception for details.,
            //      A transport-level error has occurred when receiving results from the server. (provider: TCP Provider, error: 0 - The specified network name is no longer available.),
            //      The specified network name is no longer available
            //
            // https://docs.microsoft.com/en-us/ef/ef6/fundamentals/connection-resiliency/retry-logic
            SetExecutionStrategy(MicrosoftSqlProviderServices.ProviderInvariantName, () => new MicrosoftSqlAzureExecutionStrategy(3, System.TimeSpan.FromSeconds(10)));
        }
    }

    //Initially the ApplicationDbContext inherited from Microsoft.AspNet.Identity.EntityFramework.IdentityDbContext<ApplicationUser> which has a reference to System.Data.SqlClient
    // This stopped the transition to Microsoft.Data.SqlClient. Mapping and logic from the former base class has been moved to get rid of the old System.Data.SqlClient lib.
    [DbConfigurationType(typeof(ApplicationDbContextConfiguration))]
    public class ApplicationDbContext : DbContext
    {
        //Identity
        public virtual IDbSet<ApplicationUser> Users { get; set; }
        public virtual IDbSet<IdentityRole> Roles { get; set; }
        public virtual IDbSet<APIAuthentication> APIAuthentications { get; set; }
        public virtual IDbSet<ClientSystem> ClientSystems { get; set; }
        public bool RequireUniqueEmail { get; set; }

        //Other models...
        public virtual DbSet<Municipality> Municipalities { get; set; }
        public virtual DbSet<LogEntry> LogEntries { get; set; }
        public virtual DbSet<FormMetadata> FormMetadata { get; set; }
        public virtual DbSet<Form> Forms { get; set; }
        public virtual DbSet<Status> Status { get; set; }
        public virtual DbSet<DistributionForm> DistributionForms { get; set; }
        public virtual DbSet<PostDistributionMetaData> PostDistributionMetaData { get; set; }
        public virtual DbSet<Decision> Decisions { get; set; }        
        public virtual DbSet<FileDownloadStatus> FileDownloadStatus { get; set; }
        public virtual DbSet<UserNotification> UserNotifications { get; set; }
        public virtual DbSet<TiltakshaversSamtykke> TiltakshaversSamtykkes { get; set; }
        public virtual DbSet<TiltakshaversSamtykkeByggested> TiltakshaversSamtykkeByggesteds { get; set; }
        public virtual DbSet<TiltakshaversSamtykkeVedlegg> TiltakshaversSamtykkeVedleggs { get; set; }
        public virtual DbSet<TiltakshaversSamtykkeTiltaktype> TiltakshaversSamtykkeTiltaktypes { get; set; }
        public virtual DbSet<DeviationLetter> DeviationLetter { get; set; }
        public virtual DbSet<Deviation> Deviations { get; set; }
        public virtual DbSet<Receipt> Receipts { get; set; }
        public virtual DbSet<ApplicationLog> ApplicationLog { get; set; }
        public virtual DbSet<SvarUtForsendelsesStatus> SvarUtShipmentStatuses { get; set; }
        public virtual DbSet<FormWorkflowLog> FormWorkflowLog { get; set; }
        public virtual DbSet<FormAttachmentMetadata> FormAttachmentMetadatas { get; set; }
        public virtual DbSet<AnSaKoStatus> AnSaKoStatuses { get; set; }        
        public DbSet<FtId> FtIds { get; set; }
        public ApplicationDbContext()
            : base("DefaultConnection")
        { }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Municipality>()
                .HasMany<ApplicationUser>(m => m.AdministratedByUsers)
                .WithMany(u => u.MunicipalitiesToAdministrate)
                .Map(t =>
                {
                    t.MapLeftKey("MunicipalityCode");
                    t.MapRightKey("UserId");
                    t.ToTable("MunicipalityApplicationUser");
                });

            modelBuilder.Entity<TiltakshaversSamtykke>()
                .HasMany(t => t.Tiltaktypes)
                .WithMany(t => t.TiltakshaversSamtykkes)
                .Map(m =>
                {
                    m.ToTable("TiltakshaversSamtykkeTSTiltaktypes");
                    m.MapLeftKey("TiltakshaversSamtykkeId");
                    m.MapRightKey("SamtykkeTiltaktypeId");
                });

            modelBuilder.Entity<FormMetadata>()
                .Map(t => t.ToTable("FormMetadata"))
                .HasOptional<Municipality>(s => s.Municipality).WithMany().HasForeignKey(c => c.MunicipalityCode);

            modelBuilder.Entity<SvarUtForsendelsesStatus>()
                .HasKey(e => new { e.ArchiveReference, e.SvarUtForsendelsesId })
                .HasRequired(f => f.FormMetadata)
                .WithOptional(s => s.SvarUtForsendelsesStatus);

            modelBuilder.Entity<DistributionForm>()
                .HasOptional(a => a.AnSaKoStatus)
                .WithRequired(d => d.DistributionForm)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<LogEntry>()
                .HasOptional(a => a.FormMetadata).WithMany().HasForeignKey(a => a.ArchiveReference);

            modelBuilder.Entity<FormWorkflowLog>()
                .Property(logEntry => logEntry.FtId)
                .IsRequired();

            FtIdConfiguration.Configure(modelBuilder.Entity<FtId>());
            ClientSystemConfiguration.Configure(modelBuilder.Entity<ClientSystem>());

            CreateAspNetIdentityModels(modelBuilder);
        }

        // ************* ASP Net Identity - START *************
        // Moved from Microsoft.AspNet.Identity.EntityFramework.IdentityDbContext<ApplicationUser>
        // These mappings where added to get rid of IdentityDbContext which refered System.Data.SqlClient.
        // The methods added, might be used by some of the internal logic of AspNet Identity..
        private static void CreateAspNetIdentityModels(DbModelBuilder modelBuilder)
        {
            EntityTypeConfiguration<ApplicationUser> entityTypeConfiguration = modelBuilder.Entity<ApplicationUser>().ToTable("AspNetUsers");
            entityTypeConfiguration.HasMany((ApplicationUser u) => u.Roles).WithRequired().HasForeignKey((IdentityUserRole ur) => ur.UserId);
            entityTypeConfiguration.HasMany((ApplicationUser u) => u.Claims).WithRequired().HasForeignKey((IdentityUserClaim uc) => uc.UserId);
            entityTypeConfiguration.HasMany((ApplicationUser u) => u.Logins).WithRequired().HasForeignKey((IdentityUserLogin ul) => ul.UserId);
            entityTypeConfiguration.Property((ApplicationUser u) => u.UserName).IsRequired().HasMaxLength(256)
                .HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute("UserNameIndex")
                {
                    IsUnique = true
                }));
            entityTypeConfiguration.Property((ApplicationUser u) => u.Email).HasMaxLength(256);
            modelBuilder.Entity<IdentityUserRole>().HasKey((IdentityUserRole r) => new { r.UserId, r.RoleId }).ToTable("AspNetUserRoles");
            modelBuilder.Entity<IdentityUserLogin>().HasKey((IdentityUserLogin l) => new { l.LoginProvider, l.ProviderKey, l.UserId }).ToTable("AspNetUserLogins");
            modelBuilder.Entity<IdentityUserClaim>().ToTable("AspNetUserClaims");
            EntityTypeConfiguration<IdentityRole> entityTypeConfiguration2 = modelBuilder.Entity<IdentityRole>().ToTable("AspNetRoles");
            entityTypeConfiguration2.Property((IdentityRole r) => r.Name).IsRequired().HasMaxLength(256)
                .HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute("RoleNameIndex")
                {
                    IsUnique = true
                }));
            entityTypeConfiguration2.HasMany((IdentityRole r) => r.Users).WithRequired().HasForeignKey((IdentityUserRole ur) => ur.RoleId);
        }

        //From former baseclass
        internal static bool IsIdentityV1Schema(DbContext db)
        {
            SqlConnection sqlConnection = db.Database.Connection as SqlConnection;
            if (sqlConnection == null)
            {
                return false;
            }

            if (db.Database.Exists())
            {
                using (SqlConnection sqlConnection2 = new SqlConnection(sqlConnection.ConnectionString))
                {
                    sqlConnection2.Open();
                    return VerifyColumns(sqlConnection2, "AspNetUsers", "Id", "UserName", "PasswordHash", "SecurityStamp", "Discriminator") && VerifyColumns(sqlConnection2, "AspNetRoles", "Id", "Name") && VerifyColumns(sqlConnection2, "AspNetUserRoles", "UserId", "RoleId") && VerifyColumns(sqlConnection2, "AspNetUserClaims", "Id", "ClaimType", "ClaimValue", "User_Id") && VerifyColumns(sqlConnection2, "AspNetUserLogins", "UserId", "ProviderKey", "LoginProvider");
                }
            }

            return false;
        }

        //From former baseclass
        internal static bool VerifyColumns(SqlConnection conn, string table, params string[] columns)
        {
            List<string> list = new List<string>();
            using (SqlCommand sqlCommand = new SqlCommand("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS where TABLE_NAME=@Table", conn))
            {
                sqlCommand.Parameters.Add(new SqlParameter("Table", table));
                using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
                    while (sqlDataReader.Read())
                    {
                        list.Add(sqlDataReader.GetString(0));
                    }
            }

            return columns.All(list.Contains);
        }

        protected override DbEntityValidationResult ValidateEntity(DbEntityEntry entityEntry, IDictionary<object, object> items)
        {
            if (entityEntry != null && entityEntry.State == EntityState.Added)
            {
                List<DbValidationError> list = new List<DbValidationError>();
                ApplicationUser user = entityEntry.Entity as ApplicationUser;
                if (user != null)
                {
                    if (Users.Any((ApplicationUser u) => string.Equals(u.UserName, user.UserName)))
                    {
                        list.Add(new DbValidationError("User", string.Format(CultureInfo.CurrentCulture, "User with username {0} already exists", new object[1] { user.UserName })));
                    }

                    if (RequireUniqueEmail && Users.Any((ApplicationUser u) => string.Equals(u.Email, user.Email)))
                    {
                        list.Add(new DbValidationError("User", string.Format(CultureInfo.CurrentCulture, "User with email {0} already exists", new object[1] { user.Email })));
                    }
                }
                else
                {
                    IdentityRole role = entityEntry.Entity as IdentityRole;
                    if (role != null && Roles.Any((IdentityRole r) => string.Equals(r.Name, role.Name)))
                    {
                        list.Add(new DbValidationError("Role", string.Format(CultureInfo.CurrentCulture, "Role {0} already exists", new object[1] { role.Name })));
                    }
                }

                if (list.Any())
                {
                    return new DbEntityValidationResult(entityEntry, list);
                }
            }

            return base.ValidateEntity(entityEntry, items);
        }

        // ************* ASP Net Identity - END *************
    }
}