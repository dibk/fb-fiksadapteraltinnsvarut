﻿using System;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Models
{
    public class FtId
    {
        public string EAN { get; set; }
        public string Kommunenummer { get; set; }
        public int Gaardsnummer { get; set; }
        public int Bruksnummer { get; set; }
        public string Prosjektnummer { get; set; }
        public string Prosjektnavn { get; set; }
        public string System { get; set; }
        public DateTime Distributed { get; set; }
    }
}