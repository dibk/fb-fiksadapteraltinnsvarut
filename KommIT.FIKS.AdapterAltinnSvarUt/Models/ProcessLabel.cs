﻿using System;
using System.Collections.Generic;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Models
{
    public static class ProcessLabel
    {
        public const string SvarUtForsendelse = "SvarUt-forsendelse";
        public const string AltinnSubmitPrefill = "Altinn-submitPrefill";
        public const string AltinnDistributionPrefill = "Altinn-distribution-submitPrefill";
        public const string AltinnDistributionCorrespondence = "Altinn-distribution-submitCorrespondence";
        public const string AltinnDistributionPostProcess = "Altinn-distribution-postProcess";
        public const string AltinnDistributionLoop = "Altinn-distribution-loop";
        public const string AltinnNotificationCorrespondence = "Altinn-meldingstjeneste";
        public const string ValidationApi = "FTB-Validerings-API";
        public const string FormRouting = "FTB-Form-Routing";
        public const string AltinnDlqQueue = "Altinn-DLQ-Queue";
        public const string AltinnDlqForm = "Altinn-DLQ-DownloadForm";
        public const string AltinnDlqPdf = "Altinn-DLQ-DownloadPdf";
        public const string AltinnDlqStream = "Altinn-DLQ-StreamAttachment";
        public const string AltinnDlqDelete = "Altinn-DLQ-DeleteForm";


        private static Dictionary<string, string> _shortLabels = new Dictionary<string, string>
        {
            {SvarUtForsendelse, "SvarUt"},
            {AltinnSubmitPrefill, "AltinnPrefill"},
            {AltinnDistributionPrefill, "AltinnDistPre"},
            {AltinnDistributionCorrespondence, "AltinnDistCorr"},
            {AltinnDistributionPostProcess, "AltinnDistPost"},
            {AltinnDistributionLoop, "AltinnDistLoop"},
            {AltinnNotificationCorrespondence, "AltinnCorr"},
            {ValidationApi, "FormValAPI"},
            {FormRouting, "FormRouting" },
            {AltinnDlqQueue, "DlqPending" },
            {AltinnDlqForm, "DlqForm" },
            {AltinnDlqPdf, "DlqPdf" },
            {AltinnDlqStream, "DlqStream" },
            {AltinnDlqDelete, "DlqDel" },
        };


        public static string GetSequenceTag(string processLabel)
        {
            string tag = "NOTSET";
            // https://stackoverflow.com/a/41723783
            var uniqueId = (DateTime.Now.Ticks - new DateTime(2019, 1, 1).Ticks).ToString("x");
            _shortLabels.TryGetValue(processLabel, out tag);
            return $"{tag}-{uniqueId}";
        }

    }
}