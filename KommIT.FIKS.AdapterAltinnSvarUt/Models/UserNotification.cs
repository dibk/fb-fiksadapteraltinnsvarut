﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Models
{
    public class UserNotification
    {
        [Key]
        public int Id { get; set; }

        public string ArchiveReference { get; set; }
        public string MessageReference { get; set; }
        public DateTime FirstNotification { get; set; }
        public DateTime SecondNotification { get; set; }
        public string EmailAdress { get; set; }
        public string SMSText { get; set; }
        public string EmailSubject { get; set; }
        public string EmailBody { get; set; }
        public UserNotificationStatus Status { get; set; }
        public string NotificaitonReceipt { get; set; }

        
        public UserNotification(){}

        public UserNotification(string archiveReference, string messageReference, DateTime firstNotification, DateTime secondNotification, string emailAdress, string sMSText, string emailSubject, string emailBody)
        {
            ArchiveReference = archiveReference;
            MessageReference = messageReference;
            FirstNotification = firstNotification;
            SecondNotification = secondNotification;
            EmailAdress = emailAdress;
            SMSText = sMSText;
            EmailSubject = emailSubject;
            EmailBody = emailBody;
        }

        public UserNotification(string archiveReference, string messageReference, DateTime firstNotification, DateTime secondNotification, string emailAdress, string sMSText, string emailSubject, string emailBody, string notificaitonReceipt)
        {
            ArchiveReference = archiveReference;
            MessageReference = messageReference;
            FirstNotification = firstNotification;
            SecondNotification = secondNotification;
            EmailAdress = emailAdress;
            SMSText = sMSText;
            EmailSubject = emailSubject;
            EmailBody = emailBody;
            NotificaitonReceipt = notificaitonReceipt;
        }
    }

    public enum UserNotificationStatus
    {
        Pending,
        Sent,
        SecondNotificaitonNa, 
        ErrorMessageStatus
    }
}