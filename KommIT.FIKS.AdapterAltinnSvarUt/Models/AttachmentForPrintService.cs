﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Models
{
    public class AttachmentForPrintService
    {
        public string ModifiedAttachmentTypeName { get; set; }
        public bool IsFileModifiedForPrintService { get; set; }

        public Attachment Attachment { get; set; }

        public AttachmentForPrintService()
        {
        }

        public AttachmentForPrintService(Attachment attachment)
        {
            Attachment = attachment;
        }

        public AttachmentForPrintService(Attachment attachment, string modifiedAttachmentTypeName, bool isFileModifiedForPrintService)
        {
            Attachment = attachment;
            ModifiedAttachmentTypeName = modifiedAttachmentTypeName;
            IsFileModifiedForPrintService = isFileModifiedForPrintService;
        }
    }
}