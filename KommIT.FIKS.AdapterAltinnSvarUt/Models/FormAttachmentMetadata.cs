﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Models
{
    public class FormAttachmentMetadata
    {
        [Key]
        public int Id { get; set; }

        [Index]
        [StringLength(80)]
        public string ArchiveReference { get; set; }

        public string FileName { get; set; }
        public int Size { get; set; }
        public string AttachmentType { get; set; }
        public string MimeType { get; set; }
        public string Source { get; set; }
        public virtual FormMetadata FormMetadata { get; set; }
    }
}