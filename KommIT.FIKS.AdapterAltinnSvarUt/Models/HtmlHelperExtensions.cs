﻿using System.Web.Configuration;
using System.Web.Mvc;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Models
{
    public static class HtmlHelperExtensions
    {
        public static string ApplicationVersionNumber(this HtmlHelper helper)
        {
            return WebConfigurationManager.AppSettings["AppVersionNumber"];
        }

        public static bool HangfireEnabled(this HtmlHelper helper)
        {
            var configValue = WebConfigurationManager.AppSettings["HangfireEnabled"];
            bool retVal = false;

            if(bool.TryParse(configValue, out var parsedVal))
            {
                retVal = parsedVal;
            }

            return retVal;
        }
    }
}