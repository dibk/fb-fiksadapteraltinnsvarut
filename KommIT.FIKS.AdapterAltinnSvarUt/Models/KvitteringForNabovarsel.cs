﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using no.kxml.skjema.dibk.arbeidstilsynetsSamtykke;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Models
{
    public class KvitteringForNabovarsel
    {
        public string Hovedinnsendingsnummer { get; set; }
        public string NaboNavn { get; set; }
        
        public List<Eiendom> GjelderNaboeiendommer { get; set; }

        public string NaboAdress { get; set; }
        public string NaboPostnr { get; set; }
        public string NaboPoststed { get; set; }

        public List<Eiendom> Eiendommer { get; set; }
    }

    public class Eiendom
    {
        public string Gaardsnummer { get; set; }
        public string Bruksnumme { get; set; }
        public string Festenummer { get; set; }
        public string Seksjonsnummer { get; set; }
        public string Kommunenavn { get; set; }
        public string EiendomsAdress { get; set; }
        public string EiendomsPostnr { get; set; }
        public string EiendomsPoststed { get; set; }
    }
}