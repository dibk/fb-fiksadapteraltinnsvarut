﻿using System.ComponentModel.DataAnnotations;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Models
{
    public class Status
    {
        [Key]
        public string Value { get; set; }

        public Status(string value)
        {
            Value = value;
        }
    }
}