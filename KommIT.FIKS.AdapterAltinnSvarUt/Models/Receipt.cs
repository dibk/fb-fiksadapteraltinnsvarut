﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using KommIT.FIKS.AdapterAltinnSvarUt.Models.ApiModels;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Models
{
    public class Receipt
    {
        [Key]
        public Guid Id { get; set; }

        public string AltinnArchiveReference { get; set; }
        public int ArchiveCaseYear { get; set; }
        public long ArchiveCaseSequence { get; set; }
        public string PublicArchiveCaseUrl { get; set; }
        public int ArchiveYear { get; set; }
        public int ArchiveSequence { get; set; }
        public string TiltaksId { get; set; }
        public string OrganizationNumber { get; set; }
        public string MunicipalityCode { get; set; }
        public string Message { get; set; }
        public DateTime ReceiptDate { get; set; }

        public Receipt()
        {
            Id = Guid.NewGuid(); ;
            ReceiptDate = DateTime.Now;
        }

        public Receipt(KommuneKvittering kvittering)
        {
            Id = Guid.NewGuid();
            AltinnArchiveReference = kvittering.AltinnArkivreferanse;
            ArchiveCaseYear = kvittering.Saksaar;
            ArchiveCaseSequence = kvittering.Sakssekvensnummer;
            PublicArchiveCaseUrl = kvittering.OffentligJournalUrl;
            ArchiveYear = kvittering.Journalaar;
            ArchiveSequence = kvittering.Journalsekvensnummer;
            TiltaksId = kvittering.TiltaksType?.Kode;
            OrganizationNumber = kvittering.Organisasjonsnummer;
            MunicipalityCode = kvittering.Kommunenummer;
            Message = kvittering.Melding;
            ReceiptDate = DateTime.Now;
        }

    }

    public class DeviationReciept : Receipt
    {
        public Guid ReferenceDeviationId { get; set; }
    }


}