using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Models
{
    public class FileDownloadStatus
    {
        [Key]
        public int Id { get; set; }

        [Index]
        [StringLength(maximumLength: 80)]
        public string ArchiveReference { get; set; }

        public Guid Guid { get; set; } //Blir sletta når ContainerName er fylt med data

        [Index]
        [StringLength(maximumLength: 80)]
        public string ContainerName
        { get { return this.Guid.ToString(); } set { } } //Midlertidig 'get' fram til vi får fylt kolonnen med data

        public string FormName { get; set; }
        public ApiModels.FilTyperForNedlasting FileType { get; set; } //Blir sletta når FtpbFileType er fylt med data

        [Index]
        [StringLength(maximumLength: 80)]
        public string FtpbFileType
        { get { return this.FileType.ToString(); } set { } } //Midlertidig 'get' fram til kolonna er fylt med data

        public string Filename { get; set; }
        public string BlobLink { get; set; }
        public string MimeType { get; set; }
        public int FileAccessCount { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime TimeReceived { get; set; }

        public FileDownloadStatus()
        { }

        public FileDownloadStatus(string archiveReference, Guid guid, string formName, ApiModels.FilTyperForNedlasting fileType, string filename, string blobLink, string mimeType)
        {
            ArchiveReference = archiveReference;
            Guid = guid;
            ContainerName = guid.ToString();
            FormName = formName;
            FileType = fileType;
            FtpbFileType = fileType.ToString();
            Filename = filename;
            BlobLink = blobLink;
            MimeType = mimeType;

            TimeReceived = DateTime.Now;
            FileAccessCount = 0;
            IsDeleted = false;
        }
    }
}