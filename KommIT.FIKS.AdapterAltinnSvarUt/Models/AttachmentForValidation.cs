﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Models
{
    public class AttachmentForValidation
    {
        public string Name { get; set; }

        public string Filename { get; set; }

        public int FileSize { get; set; }
    }
   
}