﻿using KommIT.FIKS.AdapterAltinnSvarUt.Models.UserAccess;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Models
{
    public class Municipality
    {
        [Key]
        public string Code { get; set; }

        public string Name { get; set; }
        public string OrganizationNumber { get; set; }
        public string PlanningDepartmentSpecificOrganizationNumber { get; set; }
        public DateTime? ValidTo { get; set; }
        public DateTime? ValidFrom { get; set; }
        public string NewMunicipalityCode { get; set; }

        public virtual List<ApplicationUser> AdministratedByUsers { get; set; }

        public Municipality()
        { }

        public Municipality(string code, string organizationNumber, string name)
        {
            Code = code;
            OrganizationNumber = organizationNumber;
            Name = name;
        }

        public Municipality(string organizationNumber, string name)
        {
            OrganizationNumber = organizationNumber;
            Name = name;
        }
    }
}