﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Models
{
    public class Terms
    {
        public long Id { get; set; }
        public string TermsId { get; set; }
        public string TermForProcess { get; set; }

        public string TermDescription { get; set; }

        public virtual Decision Decision { get; set; }
    }
}