﻿using System;
using System.ComponentModel.DataAnnotations;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Models
{
    public class SvarUtForsendelsesStatus
    {
        [Key]
        public string SvarUtForsendelsesId { get; set; }
        //[Key, ForeignKey("FormMetadata"), Column(Order = 0)]
        public string ArchiveReference { get; set; }
        public string ForsendelseStatus { get; set; }
        public DateTime? EndretStatusDato { get; set; }
        public string MunicipalityName { get; set; }
        public virtual FormMetadata FormMetadata { get; set; }
    }
}