﻿using System;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Models
{
    public class FtIdException : Exception
    {
        public FtIdException()
        {
        }

        public FtIdException(string message) : base(message)
        {
        }

        public FtIdException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}