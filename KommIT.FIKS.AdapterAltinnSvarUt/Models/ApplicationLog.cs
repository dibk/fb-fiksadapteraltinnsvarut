﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Text;
using Swashbuckle.Swagger;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Models
{
    public class ApplicationLog
    {
        /// <summary>
        /// String constants for use with the Type property
        /// </summary>
        public const string Info = "Info";
        public const string Error = "Error";
        public const string Exception = "Exception";


        /// <summary>
        /// Key id for the database row
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Timestamp recorded when the message is recorded
        /// </summary>
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:mm:ss:fff }")]
        public DateTime Timestamp { get; set; }

        /// <summary>
        /// String for tagging entries that belong to the same execution sequence, etc.
        /// </summary>
        public string SequenceTag { get; set; }

        /// <summary>
        /// Descriptive message 
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Severity of the message (info, error)
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Optional for entering if program log is realated to an Archive Reference, SvarUt guid, etc
        /// </summary>
        public string Reference { get; set; }

        /// <summary>
        /// Optional for indicating if work is done on a particular datamodel
        /// </summary>
        public string DataFormatId { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public string DataFormatVersion { get; set; }


        /// <summary>
        /// Optional for identifying the executing code: myClass.MyFunction()
        /// </summary>
        public string CodeIdentifier { get; set; }

        /// <summary>
        /// Optional for dumping a stack trace, etc
        /// </summary>
        public string Trace { get; set; }


        /// <summary>
        /// Identifies application function, like ValidationAPI, AddressAPI, etc.
        /// Ties to class ProcessLabel
        /// </summary>
        public string ProcessLabelId { get; set; }

        /// <summary>
        /// Duration of the current event in milliseconds
        /// </summary>
        public long? EventDuration { get; set; }

        public ApplicationLog()
        {
            
        }


        public ApplicationLog(string sequenceTag, string message, string type, string reference, string dataFormatId, string dataFormatVersion, string codeIdentifier, string trace, string processLabelId, long? eventDuration)
        {
            SequenceTag = sequenceTag;
            Message = message;
            Type = type;
            Reference = reference;
            DataFormatId = dataFormatId;
            DataFormatVersion = dataFormatVersion;
            CodeIdentifier = codeIdentifier;
            Trace = trace;
            ProcessLabelId = processLabelId;
            EventDuration = eventDuration;
        }

    }


    public class ApplicationLogBuilder
    {
        private readonly ApplicationLog _applicationLog;

        public ApplicationLogBuilder(string sequenceTag, string processLabelId, string codeIdentifier = null)
        {
            _applicationLog = new ApplicationLog();
            _applicationLog.SequenceTag = sequenceTag;
            _applicationLog.ProcessLabelId = processLabelId;
            _applicationLog.CodeIdentifier = codeIdentifier;
        }

        public ApplicationLogBuilder(string processLabelId, string codeIdentifier = null)
        {
            _applicationLog = new ApplicationLog();
            _applicationLog.SequenceTag = ProcessLabel.GetSequenceTag(processLabelId); ;
            _applicationLog.ProcessLabelId = processLabelId;
            _applicationLog.CodeIdentifier = codeIdentifier;
        }


        public ApplicationLog SetMessage(string message, string type, string trace = null)
        {
            _applicationLog.Message = message;
            _applicationLog.Type = type;
            _applicationLog.Trace = trace;
            return _applicationLog;
        }
        public ApplicationLog SetMessage(string message, string type, Stopwatch stopWatch, string trace = null)
        {
            _applicationLog.Message = message;
            _applicationLog.Type = type;
            _applicationLog.Trace = trace;
            _applicationLog.EventDuration = stopWatch.ElapsedMilliseconds;
            return _applicationLog;
        }

        public ApplicationLog SetDatamodelIds(string dataFormatId, string dataFormatVersion)
        {
            _applicationLog.DataFormatId = dataFormatId;
            _applicationLog.DataFormatVersion = dataFormatVersion;
            return _applicationLog;
        }

        public ApplicationLog SetReference(string reference)
        {
            _applicationLog.Reference = reference;
            return _applicationLog;
        }

    }





}

