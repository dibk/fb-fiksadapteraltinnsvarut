﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Models
{
    public class FormWorkflowLog
    {
        [Key]
        [StringLength(80)]
        public string ArchiveReference { get; set; }
        [Index]
        [StringLength(80)]
        public string FtId { get; set; }
    }
}