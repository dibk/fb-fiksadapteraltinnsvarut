﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Models
{
    public class Address
    {
        public string Name { get; set; }

        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        public string AddressLine3 { get; set; }

        public string City { get; set; }

        public string PostalCode { get; set; }

        public string CountryCode { get; set; }

        public string OrganizationNumber { get; set; }
        public string SosialServiceNumber { get; set; }

        public Address() { }

        public Address(string name, string addressLine1, string city, string postalCode)
        {
            Name = name;
            AddressLine1 = addressLine1;
            City = city;
            PostalCode = postalCode;

            AddressLine2 = null;
            AddressLine3 = null;
            CountryCode = null;
        }

        public Address(string name, string addressLine1, string addressLine2, string addressLine3, string city, string postalCode)
        {
            Name = name;
            AddressLine1 = addressLine1;
            AddressLine2 = addressLine2;
            AddressLine3 = addressLine3;
            City = city;
            PostalCode = postalCode;

            CountryCode = null;
        }

        public Address(string name, string addressLine1, string addressLine2, string addressLine3, string city, string postalCode, string countryCode)
        {
            Name = name;
            AddressLine1 = addressLine1;
            AddressLine2 = addressLine2;
            AddressLine3 = addressLine3;
            City = city;
            PostalCode = postalCode;
            CountryCode = countryCode;
        }

    }
}


