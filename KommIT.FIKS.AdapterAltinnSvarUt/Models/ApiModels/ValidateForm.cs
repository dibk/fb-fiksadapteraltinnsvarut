﻿using KommIT.FIKS.AdapterAltinnSvarUt.Models.Validation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Models.ApiModels
{
    public class ValidateForm
    {
        /// <summary>
        /// DataFormatId of the Form. https://www.altinn.no/api/help
        /// </summary>
        [Required]
        public string DataFormatId { get; set; }
        /// <summary>
        /// DataFormatVersion of the Form. https://www.altinn.no/api/help
        /// </summary>
        [Required]
        public string DataFormatVersion { get; set; }
        /// <summary>
        /// The Form data. https://www.altinn.no/api/help
        /// </summary>
        [Required]
        public string FormData { get; set; }

        /// <summary>
        /// List of all attachment types and forms/subforms name used to validate required documentation
        /// </summary>
        [Required]
        [StringArrayValidation(AllowEmptyValues = false, ErrorMessage ="Alle vedleggene må ha navn")]
        public string[] AttachmentTypesAndForms { get; set; }
    }
}