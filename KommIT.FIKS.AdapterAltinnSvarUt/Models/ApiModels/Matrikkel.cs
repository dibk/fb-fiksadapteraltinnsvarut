﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Models.ApiModels
{
    public class Matrikkel
    {
        public string Kommunenummer { get; set; }
        public int Gaardsnummer { get; set; }
        public int Bruksnummer { get; set; }
        public int Festenummer { get; set; }
        public int Seksjonsnummer { get; set; }

        public Matrikkel()
        {

        }

        public Matrikkel(string kommunenummer, int gaardsnumer, int bruksnummer, int festenummer, int seksjonsnummer)
        {
            Kommunenummer = kommunenummer;
            Gaardsnummer = gaardsnumer;
            Bruksnummer = bruksnummer;
            Festenummer = festenummer;
            Seksjonsnummer = seksjonsnummer;
        }
    }
}