using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Models.ApiModels
{
    /// <summary>
    /// Målet er å levere komplette søknader. Når det ikke skjer så sendes et mangelbrev fra kommunen
    /// </summary>
    public class Mangelbrev
    {
        /// <summary>
        /// I SvarUt så kommer AltinnArkivreferanse inn som eksternRef
        /// </summary>
        public string AltinnArkivreferanse { get; set; }

        /// <summary>
        /// Referanse til kommunens saksår (henger sammen med sakssekvensnr)
        /// </summary>
        public int Saksaar { get; set; }

        /// <summary>
        /// Referanse til kommunens sakssekvensnummer (henger sammen med saksår)
        /// </summary>
        public long Sakssekvensnummer { get; set; }

        /// <summary>
        /// Url til offentlig journal
        /// </summary>
        public string OffentligJournalUrl { get; set; }

        /// <summary>
        /// Identifikator for et mangelbrev på en søknad. Kan være flere mangelbrev pr søknad.
        /// </summary>
        public int Journalaar { get; set; }

        /// <summary>
        /// Identifikator for et mangelbrev på en søknad. Kan være flere mangelbrev pr søknad.
        /// </summary>
        public int Journalsekvensnummer { get; set; }

        /// <summary>
        /// Dato for når mangelbrevet ble registrert
        /// </summary>
        public DateTime? DokumentetsDato { get; set; }

        /// <summary>
        /// Navnet på brevet som går ut
        /// </summary>
        public string Tittel { get; set; }

        /// <summary>
        /// Referanse til virksomhet/kommune som sender mangelbrev.
        /// Vi vil nå de fleste med dette, men vær obs på at noen kommuner ikke har satt opp denne koblingen... 
        /// </summary>
        public string Organisasjonsnummer { get; set; }

        /// <summary>
        /// Referanse til kommune som sender mangelbrev
        /// </summary>
        public string Kommunenummer { get; set; }

        public Matrikkel Matrikkelnummer { get; set; }


        /// <summary>
        /// Liste med alle mangler
        /// </summary>
        public List<Mangel> Mangler { get; set; }

        public Mangelbrev()
        {
            Mangler = new List<Mangel>();
            Matrikkelnummer = new Matrikkel();
        }

        public Mangelbrev(DeviationLetter deviationLetter)
        {
            AltinnArkivreferanse = deviationLetter.AltinnArchiveReference;
            Saksaar = deviationLetter.MunicipalityArchiveCaseYear;
            Sakssekvensnummer = deviationLetter.MunicipalityArchiveCaseSequence;
            OffentligJournalUrl = deviationLetter.MunicipalityPublicArchiveCaseUrl;
            Journalaar = deviationLetter.ArchiveYear;
            Journalsekvensnummer = deviationLetter.ArchiveSequence;
            DokumentetsDato = deviationLetter.RegistrationDate;
            Tittel = deviationLetter.Title;
            Organisasjonsnummer = deviationLetter.OrganizationNumber;
            Kommunenummer = deviationLetter.MunicipalityCode;
            Matrikkelnummer = new Matrikkel(deviationLetter.MunicipalityCode, deviationLetter.CadastralUnitNumber, deviationLetter.PropertyUnitNumber,
                deviationLetter.LeaseholdNumber, deviationLetter.CondominiumUnitNumber);

            Mangler = Mangel.MappingToList(deviationLetter.Deviations);
        }

        public static List<Mangelbrev> MappingToList(List<DeviationLetter> deviationLetters)
        {
            var mangelbrevListe = new List<Mangelbrev>();
            foreach (var deviationLetter in deviationLetters)
            {
                var mangelbrev = new Mangelbrev(deviationLetter);
                mangelbrevListe.Add(mangelbrev);
            }
            return mangelbrevListe;
        }
    }
}