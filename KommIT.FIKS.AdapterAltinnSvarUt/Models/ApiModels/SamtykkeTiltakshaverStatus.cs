﻿using KommIT.FIKS.AdapterAltinnSvarUt.Models.Signatur;
using System;
using System.Collections.Generic;
using System.Linq;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Models.ApiModels
{
    public class SamtykkeTiltakshaverStatus
    {
        /// <summary>
        /// Id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Navn på tiltakshaver
        /// </summary>
        public string Tiltakshaver { get; set; }

        /// <summary>
        /// Navn på ansvarligSoeker
        /// </summary>
        public string AnsvarligSoeker { get; set; }

        /// <summary>
        /// Referanse til signert skjema i altinn for tiltakshavers samtykke
        /// </summary>
        public string AltinnArkivreferanse { get; set; }

        /// <summary>
        /// Status
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// Prosjekt navn
        /// </summary>
        public string Prosjektnavn { get; set; }

        /// <summary>
        /// Adresse til den første registrerte eiendommen
        /// </summary>
        public string Adresselinje1 { get; set; }

        /// <summary>
        /// Signert som organisasjonsnummer om tiltakshaver er en organisasjon
        /// </summary>
        public string TiltakshaverOrganisasjonsnummer { get; set; }

        public DateTime? SlettetDato { get; set; }

        /// <summary>
        /// Link for å laste ned samtykke fra tiltakshaver i PDF-format.
        /// </summary>
        public string SamtykketiltakshaverPdf { get; set; }

        /// <summary>
        /// Link for å laste ned samtykke fra tiltakshaver i Maskinlesbar XML-format.
        /// </summary>
        public string SamtykketiltakshaverXml { get; set; }

        public List<SamtykkeTiltakshaverStatusVedlegg> Vedlegg { get; set; }

        public SamtykkeTiltakshaverStatus Map(TiltakshaversSamtykke tiltakshaversSamtykke)
        {
            var samtykkeTiltakshaverStatus = new SamtykkeTiltakshaverStatus()
            {
                Id = tiltakshaversSamtykke.Id.ToString(),
                Tiltakshaver = tiltakshaversSamtykke.TiltakshaverNavn,
                AltinnArkivreferanse = tiltakshaversSamtykke.AltinnArkivreferanse,
                TiltakshaverOrganisasjonsnummer = tiltakshaversSamtykke.SignertTiltakshaverOrgnr,
                AnsvarligSoeker = tiltakshaversSamtykke.AnsvarligSoeker,
                Prosjektnavn = tiltakshaversSamtykke.Prosjektnavn,
                Adresselinje1 = tiltakshaversSamtykke.GetApiListByggested()?.FirstOrDefault()?.Adresselinje1,
                Vedlegg = tiltakshaversSamtykke.Vedlegg.ToStatusModel(),
                SlettetDato = tiltakshaversSamtykke.DateDeleted,
                Status = TiltakshaverSamtykkeConstants.GetStringValue(tiltakshaversSamtykke.Status)
            };

            //* TMP fix
            if (tiltakshaversSamtykke.Status == TiltakshaverSamtykkeConstants.Signert
                || tiltakshaversSamtykke.Status == TiltakshaverSamtykkeConstants.Arkivert)
                samtykkeTiltakshaverStatus.Status = "Signert";
            //*

            return samtykkeTiltakshaverStatus;
        }
    }

    public class SamtykkeTiltakshaverStatusVedlegg
    {
        public SamtykkeTiltakshaverStatusVedlegg(TiltakshaversSamtykkeVedlegg item)
        {
            Vedleggstype = item.Vedleggstype;
            Navn = item.Navn;
            Storrelse = item.Storrelse;
            VedleggId = item.VedleggId;
        }

        public string Vedleggstype { get; set; }
        public string Navn { get; set; }
        public string Storrelse { get; set; }
        public int VedleggId { get; set; }
    }

    public static class SamtykkeTiltakshaverExtensions
    {
        public static List<SamtykkeTiltakshaverStatusVedlegg> ToStatusModel(this ICollection<TiltakshaversSamtykkeVedlegg> source)
        {
            if (source == null)
                return null;

            return source.Select(p => new SamtykkeTiltakshaverStatusVedlegg(p)).ToList();
        }
    }
}