﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Models.ApiModels
{
    public class Status
    {
        /// <summary>
        /// I altinn API er dette MessageId. I SvarUt så kommer AltinnArkivreferanse inn som eksternRef.
        /// </summary>
        [Required]
        public string Arkivreferanse { get; set; }

        public string ForsendelsesIdSvarUt { get; set; }

        public DateTime? AltinnInnsending { get; set; }

        public DateTime? SvarUtForsendelse { get; set; }

        /// <summary>
        /// Referanse til kommunens saksår (henger sammen med sakssekvensnr)
        /// </summary>
        public int Saksaar { get; set; }

        /// <summary>
        /// Referanse til kommunens sakssekvensnummer (henger sammen med saksår)
        /// </summary>
        public long Sakssekvensnummer { get; set; }

        /// <summary>
        /// Referanse til offentlig journal (kommunens postliste)
        /// </summary>
        public string OffentligJournalUrl { get; set; }

        public string StatusInnsending { get; set; }

        public string Vedtak { get; set; }

        public DateTime? Vedtaksdato { get; set; }
        public string VedtaksDokumentUrl { get; set; }

        public List<Hendelse> Hendelser { get; set; }

        public List<Vilkaar> Vilkaar { get; set; }

        /// <summary>
        /// Om innsendingen er en distribusjonstjeneste så vil listen under fylles med status for disse distribusjoner
        /// </summary>
        public List<Distribusjon> Distribusjoner { get; set; }

        public string Tittel { get; set; }

        /// <summary>
        /// Om innsendingen lager en kvitteringsrapport som feks ved distribusjon av nabovarsel så legges lenke til denne her
        /// </summary>
        public string Kvitteringsrapport { get; set; }

        /// <summary>
        /// Om innsendingen inneholder nabovarsel så legges lenke til denne her
        /// </summary>
        public string Nabovarsel { get; set; }

        public List<FilInformasjonForNedlasting> Filnedlastinger { get; set; }

        public DateTime? Slettet { get; set; }
    }
}