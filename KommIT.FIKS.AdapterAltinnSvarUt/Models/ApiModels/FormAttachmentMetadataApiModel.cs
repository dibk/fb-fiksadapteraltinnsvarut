﻿namespace KommIT.FIKS.AdapterAltinnSvarUt.Models.ApiModels
{
    public class FormAttachmentMetadataApiModel
    {
        public static string FileSourceSystemGenerated = "SystemGenerated";
        public static string FileSourceUserProvided = "UserProvided";

        public int Id { get; set; }
        public string FileName { get; set; }
        public int Size { get; set; }
        public string AttachmentType { get; set; }
        public string MimeType { get; set; }
        public string Source { get; set; }
    }

    public static class FormAttachmentMetadataMapper
    {
        public static FormAttachmentMetadataApiModel Map(FormAttachmentMetadata formAttachmentMetadata)
        {
            return new FormAttachmentMetadataApiModel
            {
                Id = formAttachmentMetadata.Id,
                FileName = formAttachmentMetadata.FileName,
                Size = formAttachmentMetadata.Size,
                AttachmentType = formAttachmentMetadata.AttachmentType,
                MimeType = formAttachmentMetadata.MimeType,
                Source = formAttachmentMetadata.Source
            };
        }

        public static FormAttachmentMetadata Map(string archiveReference, FormAttachmentMetadataApiModel formAttachmentMetadataApiModel)
        {
            return new FormAttachmentMetadata
            {
                Id = formAttachmentMetadataApiModel.Id,
                ArchiveReference = archiveReference,
                FileName = formAttachmentMetadataApiModel.FileName,
                Size = formAttachmentMetadataApiModel.Size,
                AttachmentType = formAttachmentMetadataApiModel.AttachmentType,
                MimeType = formAttachmentMetadataApiModel.MimeType,
                Source = formAttachmentMetadataApiModel.Source
            };
        }
    }
}