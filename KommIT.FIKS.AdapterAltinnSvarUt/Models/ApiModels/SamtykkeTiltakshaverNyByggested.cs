﻿using System.ComponentModel.DataAnnotations;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Models.ApiModels
{
    public class SamtykkeTiltakshaverNyByggested
    {
        [Required]
        public string Kommunenr { get; set; }
        [Required]
        public string Gardsnr { get; set; }
        [Required]
        public string Bruksnr { get; set; }
        public string Festenr { get; set; }
        public string Seksjonsnr { get; set; }
        public string Adresselinje1 { get; set; }
        public string Postnr { get; set; }
        public string Poststed { get; set; }
        public string Bygningsnummer { get; set; }
        public string Bolignummer { get; set; }
        public string Kommunenavn { get; set; }
    }
}