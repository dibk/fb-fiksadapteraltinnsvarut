﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Models.ApiModels
{
    public class Kode
    {
        public Kode()
        {
            
        }

        public Kode(string kodeverdi, string kodebeskrivelse)
        {
            Kodeverdi = kodeverdi;
            Kodebeskrivelse = kodebeskrivelse;
        }

        public string Kodeverdi { get; set; }

        public string Kodebeskrivelse { get; set; }
    }
}