﻿using System.ComponentModel.DataAnnotations;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Models.ApiModels
{
    public class SamtykkeTiltakshaverNyTiltaktype
    {
        [Required]
        public string kodeverdi { get; set; }
        [Required]
        public string kodebeskrivelse { get; set; }
    }
}