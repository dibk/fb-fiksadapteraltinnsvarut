﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Models.ApiModels
{
    public class Hjemmel
    {
        /// <summary>
        /// Lovhjemmel
        /// </summary>
        public string Lovhjemmel { get; set; }

        /// <summary>
        /// Lovhjemmel webadresse
        /// </summary>
        public string LovhjemmelUrl { get; set; }
    }
}