﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Models.ApiModels
{
    public class Utfall
    {
       
        /// <summary>
        /// Om svaret/bahendlingen er Ja/nei-true/false
        /// </summary>
        public bool Utfallverdi { get; set; }

        /// <summary>
        /// Utfall type som passer til verdi  
        /// </summary>
        public string Utfalltype { get; set; }

        public string Utfalltypekode { get; set; }
    }
}