﻿using System;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Models.ApiModels
{
    public class FormMetadataApiModel
    {
        /// <summary>
        /// Arkivreferanse i altinn (MessageId i REST API)
        /// </summary>
        private string _archiveReference;

        public string ArchiveReference
        { get { return _archiveReference; } set { _archiveReference = value.ToUpper(); } }

        /// <summary>
        /// Kommunenr det sendes til
        /// </summary>
        public string MunicipalityCode { get; set; }

        //public virtual Municipality Municipality { get; set; }

        /// <summary>
        /// Sluttbrukersystem som gjør innsending
        /// </summary>
        public string Application { get; set; }

        /// <summary>
        /// Referanse til svarUt forsendelsesid
        /// </summary>
        public string SvarUtForsendelsesId { get; set; }

        /// <summary>
        /// Dato for altinn har arkivert forsendelse (samme som når søker sender inn og signerer)
        /// </summary>
        public DateTime? ArchiveTimestamp { get; set; }

        /// <summary>
        /// Navn på skjematype - samme som står her https://fellesbygg.dibk.no/
        /// </summary>
        public string FormType { get; set; }

        /// <summary>
        /// Tittel som sendes til SvarUt som settes sammen av felter etter regler gitt pr kommune
        /// </summary>
        public string SvarUtDocumentTitle { get; set; }

        /// <summary>
        /// Tidspunkt for når SvarUt har akseptert forsendelse
        /// </summary>
        public DateTime? SvarUtShippingTimestamp { get; set; }

        /// <summary>
        /// Referanse gitt i skjemadata som er viktig for søknadssystem/søker
        /// </summary>
        public string VaarReferanse { get; set; }

        /// <summary>
        /// Kommunens saksnummer del saksår - kommer fra skjemadata eller kan settes av kommunen i egen kvitteringstjeneste
        /// </summary>
        public int MunicipalityArchiveCaseYear { get; set; }

        /// <summary>
        /// Kommunens saksnummer del sekvensnr - kommer fra skjemadata eller kan settes av kommunen i egen kvitteringstjeneste
        /// </summary>
        public long MunicipalityArchiveCaseSequence { get; set; }

        /// <summary>
        /// Kommunens referanse til forsendelsen i sin offentlige journal(postliste) - kan settes av kommunen i egen kvitteringstjeneste
        /// </summary>
        public string MunicipalityPublicArchiveCaseUrl { get; set; }

        /// <summary>
        /// Tjenestekode i altinn
        /// </summary>
        public string ServiceCode { get; set; }

        /// <summary>
        /// Utgavenr i altinn
        /// </summary>
        public int ServiceEditionCode { get; set; }

        /// <summary>
        /// Status på innsending - Avvist/feil, Ok, Ok - men advarsler
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// Sendes via system - SvarUt/Altinn
        /// </summary>
        public string SenderSystem { get; set; }

        /// <summary>
        /// Mottaker av forsendelse
        /// </summary>
        public string SendTo { get; set; }

        /// <summary>
        /// Avsender av forsendelse
        /// </summary>
        public string SendFrom { get; set; }

        /// <summary>
        /// Antall valideringsfeil
        /// </summary>
        public int ValidationErrors { get; set; }

        /// <summary>
        /// Antall valideringsadvarsler
        /// </summary>
        public int ValidationWarnings { get; set; }

        /// <summary>
        /// Lenke til kvitteringsrapport for distribusjonssending
        /// </summary>
        public string DistributionRecieptLink { get; set; }

        /// <summary>
        /// Status i svarut
        /// </summary>
        public string SvarUtStatus { get; set; }

        /// <summary>
        /// Søkers navn
        /// </summary>
        public string ApplicantName { get; set; }

        /// <summary>
        /// Søkers adresse
        /// </summary>
        public string ApplicantAddress { get; set; }

        /// <summary>
        /// Første eiendom kommunenr i skjema
        /// </summary>
        public string PropertyFirstKnr { get; set; }

        /// <summary>
        /// Første eiendom gårdsnummer i skjema
        /// </summary>
        public string PropertyFirstGardsnr { get; set; }

        /// <summary>
        /// Første eiendom bruksnummer i skjema
        /// </summary>
        public string PropertyFirstBruksnr { get; set; }

        /// <summary>
        /// Første eiendom festenummer i skjema
        /// </summary>
        public string PropertyFirstFestenr { get; set; }

        /// <summary>
        /// Første eiendom seksjonsnummer i skjema
        /// </summary>
        public string PropertyFirstSeksjonsnr { get; set; }

        /// <summary>
        /// Første eiendom sin adresse
        /// </summary>
        public string PropertyFirstAddress { get; set; }

        /// <summary>
        /// Første tiltakstype
        /// </summary>
        public string FirstActionType { get; set; }

        /// <summary>
        /// Dato for sletting av data på denne entiteten
        /// </summary>
        public DateTime? Slettet { get; set; }
    }

    public static class FormMetadataApiMapper
    {
        public static FormMetadataApiModel Map(FormMetadata formMetadata)
        {
            return new FormMetadataApiModel()
            {
                ApplicantAddress = formMetadata.ApplicantAddress,
                ApplicantName = formMetadata.ApplicantName,
                Application = formMetadata.Application,
                ArchiveReference = formMetadata.ArchiveReference,
                ArchiveTimestamp = formMetadata.ArchiveTimestamp,
                DistributionRecieptLink = formMetadata.DistributionRecieptLink,
                SvarUtStatus = formMetadata.SvarUtStatus,
                FirstActionType = formMetadata.FirstActionType,
                FormType = formMetadata.FormType,
                MunicipalityArchiveCaseSequence = formMetadata.MunicipalityArchiveCaseSequence,
                MunicipalityArchiveCaseYear = formMetadata.MunicipalityArchiveCaseYear,
                MunicipalityCode = formMetadata.MunicipalityCode,
                MunicipalityPublicArchiveCaseUrl = formMetadata.MunicipalityPublicArchiveCaseUrl,
                PropertyFirstAddress = formMetadata.PropertyFirstAddress,
                PropertyFirstBruksnr = formMetadata.PropertyFirstBruksnr,
                PropertyFirstFestenr = formMetadata.PropertyFirstFestenr,
                PropertyFirstGardsnr = formMetadata.PropertyFirstGardsnr,
                PropertyFirstKnr = formMetadata.PropertyFirstKnr,
                PropertyFirstSeksjonsnr = formMetadata.PropertyFirstSeksjonsnr,
                SenderSystem = formMetadata.SenderSystem,
                SendFrom = formMetadata.SendFrom,
                SendTo = formMetadata.SendTo,
                ServiceCode = formMetadata.ServiceCode,
                ServiceEditionCode = formMetadata.ServiceEditionCode,
                Status = formMetadata.Status,
                SvarUtDocumentTitle = formMetadata.SvarUtDocumentTitle,
                SvarUtForsendelsesId = formMetadata.SvarUtForsendelsesId,
                SvarUtShippingTimestamp = formMetadata.SvarUtShippingTimestamp,
                VaarReferanse = formMetadata.VaarReferanse,
                ValidationErrors = formMetadata.ValidationErrors,
                ValidationWarnings = formMetadata.ValidationWarnings,
                Slettet = formMetadata.Slettet
            };
        }

        public static FormMetadata Map(FormMetadataApiModel formMetadataApiModel)
        {
            return new FormMetadata()
            {
                ApplicantAddress = formMetadataApiModel.ApplicantAddress,
                ApplicantName = formMetadataApiModel.ApplicantName,
                Application = formMetadataApiModel.Application,
                ArchiveReference = formMetadataApiModel.ArchiveReference,
                ArchiveTimestamp = formMetadataApiModel.ArchiveTimestamp,
                DistributionRecieptLink = formMetadataApiModel.DistributionRecieptLink,
                SvarUtStatus = formMetadataApiModel.SvarUtStatus,
                FirstActionType = formMetadataApiModel.FirstActionType,
                FormType = formMetadataApiModel.FormType,
                MunicipalityArchiveCaseSequence = formMetadataApiModel.MunicipalityArchiveCaseSequence,
                MunicipalityArchiveCaseYear = formMetadataApiModel.MunicipalityArchiveCaseYear,
                MunicipalityCode = formMetadataApiModel.MunicipalityCode,
                MunicipalityPublicArchiveCaseUrl = formMetadataApiModel.MunicipalityPublicArchiveCaseUrl,
                PropertyFirstAddress = formMetadataApiModel.PropertyFirstAddress,
                PropertyFirstBruksnr = formMetadataApiModel.PropertyFirstBruksnr,
                PropertyFirstFestenr = formMetadataApiModel.PropertyFirstFestenr,
                PropertyFirstGardsnr = formMetadataApiModel.PropertyFirstGardsnr,
                PropertyFirstKnr = formMetadataApiModel.PropertyFirstKnr,
                PropertyFirstSeksjonsnr = formMetadataApiModel.PropertyFirstSeksjonsnr,
                SenderSystem = formMetadataApiModel.SenderSystem,
                SendFrom = formMetadataApiModel.SendFrom,
                SendTo = formMetadataApiModel.SendTo,
                ServiceCode = formMetadataApiModel.ServiceCode,
                ServiceEditionCode = formMetadataApiModel.ServiceEditionCode,
                Status = formMetadataApiModel.Status,
                SvarUtDocumentTitle = formMetadataApiModel.SvarUtDocumentTitle,
                SvarUtForsendelsesId = formMetadataApiModel.SvarUtForsendelsesId,
                SvarUtShippingTimestamp = formMetadataApiModel.SvarUtShippingTimestamp,
                VaarReferanse = formMetadataApiModel.VaarReferanse,
                ValidationErrors = formMetadataApiModel.ValidationErrors,
                ValidationWarnings = formMetadataApiModel.ValidationWarnings,
                Slettet = formMetadataApiModel.Slettet                
            };
        }
    }
}