﻿using DIBK.FBygg.Altinn.MapperCodelist;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Http;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Models.ApiModels
{
    public class SamtykkeTiltakshaverNy : IValidatableObject
    {
        /// <summary>
        /// Navn på tiltakshaver
        /// </summary>
        [Required]
        public string Tiltakshaver { get; set; }
        /// <summary>
        /// Eventuelt orgnr for tiltakshaver
        /// </summary>
        public string TiltakshaverOrgnr { get; set; }
        /// <summary>
        /// Partstype for tiltakshaver
        /// </summary>
        /// <see href="https://register.geonorge.no/kodelister/byggesoknad/partstype"/>
        [PartstypeValidation]
        public string TiltakshaverPartstypeKode { get; set; }
        /// <summary>
        /// Navn på ansvarlig søker
        /// </summary>
        [Required]
        public string AnsvarligSoeker { get; set; }
        [Required]
        public string AnsvarligSoekerOrganisasjonsnummer { get; set; }
        /// <summary>
        /// søknad prosjektnavn
        /// </summary>
        public string Prosjektnavn { get; set; }
        /// <summary>
        /// Who sends the form
        /// </summary>
        [Required]
        public string FraSluttbrukerSystem { get; set; }

        /// <summary>
        /// Liste med byggested
        /// </summary>
        public ICollection<SamtykkeTiltakshaverNyByggested> Byggested { get; set; }
        /// <summary>
        /// Liste med tiltaktypes
        /// </summary>
        public ICollection<SamtykkeTiltakshaverNyTiltaktype> Tiltaktypes { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (TiltakshaverPartstypeKode.Equals("Privatperson", System.StringComparison.OrdinalIgnoreCase))
                yield return ValidationResult.Success;
            else
                if (string.IsNullOrEmpty(TiltakshaverOrgnr))
                yield return new ValidationResult($"TiltakshaverOrgnr is required when TiltakshaverPartstypeKode is '{TiltakshaverPartstypeKode}'");
        }
    }

    public class PartstypeValidationAttribute : ValidationAttribute
    {
        private List<string> _fallbackPartstyper = new List<string> { "Foretak", "Offentlig myndighet", "Organisasjon", "Privatperson" };
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            //if (value == null)
            //    return ValidationResult.Success;

            string valueToValidate = (string)value;

            List<string> validValues = null;

            try
            {
                validValues = GetValidPartstyper();
            }
            catch (System.Exception)
            {
                validValues = _fallbackPartstyper;
            }

            bool valueExistsInCodelist = validValues.Any(p => p.Equals(valueToValidate, System.StringComparison.OrdinalIgnoreCase));

            if (valueExistsInCodelist)
                return ValidationResult.Success;
            else
            {
                var valResult = new ValidationResult($"Value `{valueToValidate}` is invalid. Valid values are: {string.Join(", ", validValues)}");
                return valResult;
            }
        }

        private List<string> GetValidPartstyper()
        {
            var scope = GlobalConfiguration.Configuration.DependencyResolver.BeginScope();
            var codelistService = scope.GetService(typeof(ICodeListService)) as ICodeListService;

            var codelist = codelistService.GetCodelist("PartsType", RegistryType.KodelisteByggesoknad);

            if (codelist != null)
                return codelist.Keys.ToList();
            else
                throw new System.Exception("Unable to get codelist from GeoNorge");
        }
    }
}