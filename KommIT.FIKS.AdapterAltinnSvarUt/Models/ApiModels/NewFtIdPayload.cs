﻿using System.Collections.Generic;
using System.Linq;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Models.ApiModels
{
    public class NewFtIdPayload
    {
        public string Kommunenummer { get; set; }
        public int? Gaardsnummer { get; set; }
        public int? Bruksnummer { get; set; }
        public string Prosjektnavn { get; set; }
        public string Prosjektnr { get; set; }
        public string System { get; set; }

        public void Validate()
        {
            var errorProps = new List<string>();

            if (string.IsNullOrWhiteSpace(Kommunenummer))
                errorProps.Add(nameof(Kommunenummer));

            if (!Gaardsnummer.HasValue)
                errorProps.Add(nameof(Gaardsnummer));

            if (!Bruksnummer.HasValue)
                errorProps.Add(nameof(Bruksnummer));

            if (string.IsNullOrWhiteSpace(Prosjektnavn))
                errorProps.Add(nameof(Prosjektnavn));

            if (string.IsNullOrWhiteSpace(System))
                errorProps.Add(nameof(System));

            if (errorProps.Any())
            {
                var props = errorProps.Count() > 1 ?
                    $"{string.Join(", ", errorProps.Take(errorProps.Count() - 1))} og {errorProps.Last()}" :
                    errorProps.FirstOrDefault();

                throw new FtIdException($"{props} må fylles ut");
            }
        }
    }
}