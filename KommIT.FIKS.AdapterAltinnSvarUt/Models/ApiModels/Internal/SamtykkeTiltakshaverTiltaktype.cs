﻿using KommIT.FIKS.AdapterAltinnSvarUt.Models.Signatur;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Models.ApiModels.Internal
{
    public class SamtykkeTiltakshaverTiltaktype
    {
        public SamtykkeTiltakshaverTiltaktype(TiltakshaversSamtykkeTiltaktype item)
        {
            this.kodeverdi = item.kodeverdi;
            this.kodebeskrivelse = item.kodebeskrivelse;
        }

        public string kodeverdi { get; set; }
        public string kodebeskrivelse { get; set; }
    }
}