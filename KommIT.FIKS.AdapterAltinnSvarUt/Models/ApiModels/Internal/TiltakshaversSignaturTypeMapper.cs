﻿using no.kxml.skjema.dibk.tiltakshaversSignatur;
using System.Collections.Generic;
using System.Linq;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Models.ApiModels.Internal
{
    public static class TiltakshaversSignaturTypeMapper
    {
        public static TiltakshaversSignaturType Map(SamtykkeTiltakshaver tiltakshaversSamtykke)
        {
            var tiltakshaversSignatur = new TiltakshaversSignaturType();
            tiltakshaversSignatur.tiltakshaver = tiltakshaversSamtykke.TiltakshaverNavn;
            if (!string.IsNullOrEmpty(tiltakshaversSamtykke.AnsvarligSoeker) || !string.IsNullOrEmpty(tiltakshaversSamtykke.AnsvarligSoekerOrganisasjonsnummer))
            {

                tiltakshaversSignatur.ansvarligSoeker = new AnsvarligSoekerType
                {
                    navn = tiltakshaversSamtykke.AnsvarligSoeker,
                    organisasjonsnummer = tiltakshaversSamtykke.AnsvarligSoekerOrganisasjonsnummer
                };
            }

            tiltakshaversSignatur.guid = tiltakshaversSamtykke.Id;
            tiltakshaversSignatur.prosjektnavn = tiltakshaversSamtykke.Prosjektnavn;
            tiltakshaversSignatur.bekrefterSoeknadenSpecified = true;
            tiltakshaversSignatur.bekrefterSoeknaden = true;

            if (tiltakshaversSamtykke.Byggested != null && tiltakshaversSamtykke.Byggested.Any())
            {
                var byggestedList = new List<EiendomType>();
                foreach (var byggested in tiltakshaversSamtykke.Byggested)
                {

                    byggestedList.Add(new EiendomType()
                    {
                        bygningsnummer = byggested.Bygningsnummer,
                        adresse = new EiendommensAdresseType()
                        {
                            adresselinje1 = byggested.Adresselinje1,
                            postnr = byggested.Postnr,
                            poststed = byggested.Poststed
                        },
                        bolignummer = byggested.Bolignummer,
                        eiendomsidentifikasjon = new MatrikkelnummerType()
                        {
                            kommunenummer = byggested.Kommunenr,
                            gaardsnummer = byggested.Gardsnr,
                            bruksnummer = byggested.Bruksnr,
                            festenummer = byggested.Festenr,
                            seksjonsnummer = byggested.Seksjonsnr
                        },
                        kommunenavn = byggested.Kommunenavn
                    });
                }
                tiltakshaversSignatur.eiendomByggested = byggestedList.ToArray();
            }

            if (tiltakshaversSamtykke.TiltakType != null && tiltakshaversSamtykke.TiltakType.Any())
            {
                var tiltaksTypes = new List<KodeType>();
                foreach (var tiltaksType in tiltakshaversSamtykke.TiltakType)
                {
                    tiltaksTypes.Add(new KodeType()
                    {
                        kodeverdi = tiltaksType.kodeverdi,
                        kodebeskrivelse = tiltaksType.kodebeskrivelse
                    });
                }
                tiltakshaversSignatur.tiltak = tiltaksTypes.ToArray();
            }
            return tiltakshaversSignatur;
        }
    }
}