﻿using KommIT.FIKS.AdapterAltinnSvarUt.Models.Signatur;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Models.ApiModels.Internal
{
    public class SamtykkeTiltakshaverVedlegg
    {
        private TiltakshaversSamtykkeVedlegg item;

        public SamtykkeTiltakshaverVedlegg(TiltakshaversSamtykkeVedlegg item)
        {
            this.item = item;
            Vedleggstype = item.Vedleggstype;
            Navn = item.Navn;
            Referanse = item.Referanse;
            Storrelse = item.Storrelse;
            Uploaded = item.Uploaded;
            VedleggId = item.VedleggId;
        }

        public string Vedleggstype { get; set; }
        public string Navn { get; set; }
        public string Referanse { get; set; }
        public string Storrelse { get; set; }
        public bool Uploaded { get; set; }
        public int VedleggId { get; set; }
    }
}