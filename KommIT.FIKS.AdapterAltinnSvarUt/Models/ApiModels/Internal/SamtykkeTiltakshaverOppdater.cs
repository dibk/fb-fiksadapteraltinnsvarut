﻿namespace KommIT.FIKS.AdapterAltinnSvarUt.Models.ApiModels.Internal
{
    public class SamtykkeTiltakshaverOppdater
    {
        /// <summary>
        /// Id til samtykke
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Navn på tiltakshaver som signerer
        /// </summary>
        public string TiltakshaverNavn { get; set; }

        /// <summary>
        /// Signert som organisasjonsnummer om tiltakshaver er en organisasjon
        /// </summary>
        public string SignertTiltakshaverOrgnr { get; set; }

        /// <summary>
        /// Referanse til signert skjema i altinn for tiltakshavers samtykke
        /// </summary>
        public string AltinnArkivreferanse { get; set; }
    }
}