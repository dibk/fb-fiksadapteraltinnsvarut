﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Collections.Generic;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Models.ApiModels.Internal.SvarUt
{
    public class SvarUtConfig
    {
        [JsonConverter(typeof(StringEnumConverter))]
        public ConfigOwner ConfigOwner { get; set; }

        private string configOwnerId;
        public string ConfigOwnerId { get => configOwnerId?.ToUpper(); set => configOwnerId = value; }

        public List<ClassificationConfig> Classifications { get; set; }
        public List<FormConfig> FormSettings { get; set; }
        public List<AttachmentConfig> AttachmentSettings { get; set; }
    }

    public enum ConfigOwner
    {
        Municipality,
        Sektormyndighet
    }

    public class ClassificationConfig
    {
        public ClassificationConfig() { }
        public ClassificationConfig(int sorting, string name, string valueTagPattern)
        {
            Sorting = sorting;
            Name = name;
            ValueTagPattern = valueTagPattern;
        }
        public int Sorting { get; set; }
        public string Name { get; set; }
        public string ValueTagPattern { get; set; }
    }

    public class FormConfig
    {
        public FormConfig() { }

        public FormConfig(int formId, string formName, string titleTagPattern)
        {
            FormId = formId;
            FormName = formName;
            TitleTagPattern = titleTagPattern;
        }

        public int FormId { get; set; }
        public string FormName { get; set; }
        public string TitleTagPattern { get; set; }
    }

    public class AttachmentConfig
    {
        public string AttachmentTypeName { get; set; }
        public string Title { get; set; }
        public int DocumentNumber { get; set; }
        public string DocumentType { get; set; }
        public string DocumentStatus { get; set; }
        public string TilknyttetRegistreringSom { get; set; }
        public AttachmentConfig() { }

        public AttachmentConfig(string attachmentTypeName, string title, int documentNumber, string documentType, string documentStatus, string tilknyttetRegistreringSom)
        {
            AttachmentTypeName = attachmentTypeName;
            Title = title;
            DocumentNumber = documentNumber;
            DocumentType = documentType;
            DocumentStatus = documentStatus;
            TilknyttetRegistreringSom = tilknyttetRegistreringSom;
        }
    }
}