﻿using KommIT.FIKS.AdapterAltinnSvarUt.Models.Signatur;
using System;
using System.Collections.Generic;
using System.Linq;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Models.ApiModels.Internal
{
    public class SamtykkeTiltakshaver
    {
        /// <summary>
        /// Id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Navn på tiltakshaver
        /// </summary>
        public string TiltakshaverNavn { get; set; }
        /// <summary>
        /// Eventuelt orgnr for tiltakshaver
        /// </summary>
        public string TiltakshaverOrgnr { get; set; }
        /// <summary>
        /// Partstype for tiltakshaver
        /// </summary>
        /// <see cref="https://register.geonorge.no/kodelister/byggesoknad/partstype"/>
        public string TiltakshaverPartstypeKode { get; set; }

        /// <summary>
        /// Navn på ansvarlig søker
        /// </summary>
        public string AnsvarligSoeker { get; set; }
        /// <summary>
        /// Organisasjonsnummer til ansvarlig søker
        /// </summary>
        public string AnsvarligSoekerOrganisasjonsnummer { get; set; }

        /// <summary>
        /// Status
        /// </summary>
        public string Status { get; set; }
        /// <summary>
        /// Prosjekt navn
        /// </summary>
        public string Prosjektnavn { get; set; }
        /// <summary>
        /// Adresse til den første registrerte eiendommen
        /// </summary>
        public string Adresselinje1 { get; set; }

        /// <summary>
        /// Schema service Code
        /// </summary>
        public string ServiceCode { get; set; }
        /// <summary>
        /// Schema service Edition code
        /// </summary>
        public string ServiceEdition { get; set; }
        /// <summary>
        /// Who sends the scheme 
        /// </summary>
        public string FraSluttbrukerSystem { get; set; }
        /// <summary>
        /// Liste med byggested (eiendommer og adresse)
        /// </summary>

        /// <summary>
        /// Date and time when the scheme is registered in the database.
        /// </summary>
        public DateTime DateTime { get; set; }

        public ICollection<SamtykkeTiltakshaverByggested> Byggested { get; set; }

        /// <summary>
        /// Liste med vedlegg
        /// </summary>
        public ICollection<SamtykkeTiltakshaverVedlegg> Vedlegg { get; set; }
        /// <summary>
        /// List med tiltakType
        /// </summary>
        public ICollection<SamtykkeTiltakshaverTiltaktype> TiltakType { get; set; }

        public static SamtykkeTiltakshaver Map(TiltakshaversSamtykke tiltakshaversSamtykke)
        {
            var mapped = new SamtykkeTiltakshaver()
            {
                Id = tiltakshaversSamtykke.Id.ToString(),
                TiltakshaverNavn = tiltakshaversSamtykke.TiltakshaverNavn,
                TiltakshaverPartstypeKode = tiltakshaversSamtykke.TiltakshaverPartstypeKode,
                TiltakshaverOrgnr = tiltakshaversSamtykke.TiltakshaverOrgnr,
                AnsvarligSoeker = tiltakshaversSamtykke.AnsvarligSoeker,
                AnsvarligSoekerOrganisasjonsnummer = tiltakshaversSamtykke.AnsvarligSoekerOrganisasjonsnummer,
                Prosjektnavn = tiltakshaversSamtykke.Prosjektnavn,
                ServiceCode = tiltakshaversSamtykke.ServiceCode,
                ServiceEdition = tiltakshaversSamtykke.ServiceEdition,
                FraSluttbrukerSystem = tiltakshaversSamtykke.FraSluttbrukerSystem,
                DateTime = tiltakshaversSamtykke.DateCreated,
                Adresselinje1 = tiltakshaversSamtykke.GetApiListByggested()?.FirstOrDefault()?.Adresselinje1,
                Byggested = tiltakshaversSamtykke.GetApiListByggested(),
                Vedlegg = tiltakshaversSamtykke.GetApiListVedlegg(),
                TiltakType = tiltakshaversSamtykke.GetApiListTiltaktype(),
                Status = TiltakshaverSamtykkeConstants.GetStringValue(tiltakshaversSamtykke.Status)
            };

            return mapped;
        }
    }
}