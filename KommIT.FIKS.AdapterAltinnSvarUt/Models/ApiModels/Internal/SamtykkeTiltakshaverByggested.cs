﻿using KommIT.FIKS.AdapterAltinnSvarUt.Models.Signatur;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Models.ApiModels.Internal
{
    public class SamtykkeTiltakshaverByggested
    {
        private TiltakshaversSamtykkeByggested item;

        public SamtykkeTiltakshaverByggested(TiltakshaversSamtykkeByggested item)
        {
            this.item = item;
            Kommunenr = item.Kommunenr;
            Gardsnr = item.Gardsnr;
            Bruksnr = item.Bruksnr;
            Festenr = item.Festenr;
            Seksjonsnr = item.Seksjonsnr;
            Adresselinje1 = item.Adresselinje1;
            Postnr = item.Postnr;
            Poststed = item.Poststed;
            Bygningsnummer = item.Bygningsnummer;
            Bolignummer = item.Bolignummer;
            Kommunenavn = item.Kommunenavn;
        }

        public string Kommunenr { get; set; }
        public string Gardsnr { get; set; }
        public string Bruksnr { get; set; }
        public string Festenr { get; set; }
        public string Seksjonsnr { get; set; }
        public string Adresselinje1 { get; set; }
        public string Postnr { get; set; }
        public string Poststed { get; set; }
        public string Bygningsnummer { get; set; }
        public string Bolignummer { get; set; }
        public string Kommunenavn { get; set; }
    }
}