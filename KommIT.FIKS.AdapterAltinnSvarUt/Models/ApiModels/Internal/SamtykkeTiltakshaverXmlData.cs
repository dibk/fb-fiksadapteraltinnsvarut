﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Models.ApiModels.Internal
{
    public class SamtykkeTiltakshaverXmlData
    {
        public string DataFormatId { get; set; }
        public string DataFormatVersion { get; set; }
        public string XmlData { get; set; }
    }
}