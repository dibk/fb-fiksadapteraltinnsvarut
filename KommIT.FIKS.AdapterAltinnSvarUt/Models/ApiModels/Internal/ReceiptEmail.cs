﻿using System.ComponentModel.DataAnnotations;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Models.ApiModels.Internal
{
    public class ReceiptEmail
    {
        [Required]
        [EmailAddress]
        public string ToEmailAddress { get; set; }
        [Required]
        public string Subject { get; set; }
        [Required]
        public string HtmlBody { get; set; }
    }
}