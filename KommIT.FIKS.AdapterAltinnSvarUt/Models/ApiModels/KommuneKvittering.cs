﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Models.ApiModels
{
    public class KommuneKvittering
    {
        /// <summary>
        /// I SvarUt så kommer AltinnArkivreferanse inn som eksternRef
        /// </summary>
        public string AltinnArkivreferanse { get; set; }
        /// <summary>
        /// Melding kommunen vil gi til søknadssystem
        /// </summary>
        [Required]
        public string Melding { get; set; }
        /// <summary>
        /// Referanse til kommunens saksår (henger sammen med sakssekvensnr)
        /// </summary>
        public int Saksaar { get; set; }
        /// <summary>
        /// Referanse til kommunens sakssekvensnummer (henger sammen med saksår)
        /// </summary>
        public long Sakssekvensnummer { get; set; }

        /// <summary>
        /// Referanse til offentlig journal (kommunens postliste)
        /// </summary>
        public string OffentligJournalUrl { get; set; }

        /// <summary>
        /// En del av Identifikator for enten søknad eller mangelen det kvitteres for.
        /// Eks kvitteres det på mottatt søknad, så er det søknaden sitt journalår som brukes
        /// </summary>
        public int Journalaar { get; set; }

        /// <summary>
        /// En del av Identifikator for enten søknad eller mangelen det kvitteres for.
        /// Eks kvitteres det på mottatt søknad, så er det søknaden sitt journalsekvensnummer som brukes
        /// </summary>
        public int Journalsekvensnummer { get; set; }

        /// <summary>
        /// Tiltakstype. Bruk kodeliste https://register.geonorge.no/byggesoknad/tiltaktype
        /// </summary>
        public TiltaksType TiltaksType { get; set; }

        /// <summary>
        /// Referanse til virksomhet/kommune som sender kvittering
        /// </summary>
        public string Organisasjonsnummer { get; set; }

        /// <summary>
        /// Referanse til kommune som sender kvittering
        /// </summary>
        public string Kommunenummer { get; set; }

    }
}