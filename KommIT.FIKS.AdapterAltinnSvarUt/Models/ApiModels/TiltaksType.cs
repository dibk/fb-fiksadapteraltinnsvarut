﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Models.ApiModels
{
    public class TiltaksType
    {
        /// <summary>
        /// Kodeverdi for en gitt tiltakstype
        /// </summary>
        public string Kode { get; set; }
    }

}