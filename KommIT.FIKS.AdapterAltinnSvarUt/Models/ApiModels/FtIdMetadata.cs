﻿namespace KommIT.FIKS.AdapterAltinnSvarUt.Models.ApiModels
{
    public class FtIdMetadata
    {
        public FtIdMetadata(string archiveReference, string formType)
        {
            ArchiveReference = archiveReference;
            FormType = formType;
        }

        public string ArchiveReference { get; set; }
        public string FormType { get; set; }
    }
}