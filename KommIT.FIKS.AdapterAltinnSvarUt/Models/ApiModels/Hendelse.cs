﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Models.ApiModels
{
    public class Hendelse
    {
        public Hendelse()
        {
        }

        public Hendelse(string hendelse)
        {
            
            Beskrivelse = hendelse;
            Tidspunkt = DateTime.Now;
        }
       
 
        public Hendelse(string hendelse, string url)
        {
            Beskrivelse = hendelse;
            Tidspunkt = DateTime.Now;
            VisMerUrl = url;
        }


        /// <summary>
        /// Beskrivelse av hendelsen
        /// </summary>
        public string Beskrivelse { get; set; }

        /// <summary>
        /// Tidspunkt for hendelsen
        /// </summary>
        public DateTime Tidspunkt { get; set; }

        /// <summary>
        /// Referanse til offentlig journal (kommunens postliste)
        /// </summary>
        public string VisMerUrl { get; set; }
    }
}