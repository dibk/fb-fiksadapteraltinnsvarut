﻿using System.Collections.Generic;
using System.Linq;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Models.ApiModels
{
    public class AttachFtIdPayload
    {
        public string FtId { get; set; }
        public string ArchiveReference { get; set; }
        
        public void Validate()
        {
            var errorProps = new List<string>();

            if (string.IsNullOrWhiteSpace(FtId))
                errorProps.Add(nameof(FtId));

            if (string.IsNullOrWhiteSpace(ArchiveReference))
                errorProps.Add(nameof(ArchiveReference));

            if (errorProps.Any())
            {
                var props = errorProps.Count() > 1 ?
                    $"{string.Join(", ", errorProps.Take(errorProps.Count() - 1))} og {errorProps.Last()}" :
                    errorProps.FirstOrDefault();

                throw new FtIdException($"{props} må fylles ut");
            }
        }
    }
}