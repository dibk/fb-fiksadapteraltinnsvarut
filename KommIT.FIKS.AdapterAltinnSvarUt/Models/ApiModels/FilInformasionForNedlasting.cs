﻿using System;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Models.ApiModels
{
    public class FilInformasjonForNedlasting
    {
        /// <summary>
        /// Beskrivelse av nedlastingsfilen, knyttet til enum "FilTyperForNedlasting"
        /// </summary>
        public string Filbeskrivelse { get; set; }

        /// <summary>
        /// Lenke for nedlasting av fil
        /// </summary>
        public string FilURL { get; set; }

        public string Mimetype { get; set; }
        public string Filnavn { get; set; }

        public FilInformasjonForNedlasting()
        {
        }

        public FilInformasjonForNedlasting(string filUrl, string filbeskrivelse, string mimetype, string filnavn = null)
        {
            Filbeskrivelse = filbeskrivelse;
            FilURL = filUrl;
            Mimetype = mimetype;
            Filnavn = filnavn;
        }
    }

    /// <summary>
    /// Enum for filtyper ved nedlastning
    /// </summary>
    public enum FilTyperForNedlasting
    {
        /// <summary>
        /// Svar på nabovarsel pdf
        /// </summary>
        SkjemaPdf,

        /// <summary>
        /// Svar på nabovarsel xml
        /// </summary>
        MaskinlesbarXml,

        /// <summary>
        /// Svar på nabovarsel vedlegg
        /// </summary>
        SvarPaaNabovarselVedlegg,

        /// <summary>
        /// Nabovarsel kvitering til Ansvarlig soker
        /// </summary>
        KvitteringNabovarsel,

        /// <summary>
        /// Nabovarsel vedlegg til soker etter distribusjon av nabovarsel
        /// </summary>
        Nabovarsel,

        /// <summary>
        /// Vedlegg - Avkjoerselsplan
        /// </summary>
        Avkjoerselsplan,

        /// <summary>
        /// Vedlegg - Situasjonskart
        /// </summary>
        Situasjonskart,

        /// <summary>
        /// Vedlegg - Situasjonsplan
        /// </summary>
        Situasjonsplan,

        /// <summary>
        /// Vedlegg - TegningEksisterendeFasade
        /// </summary>
        TegningEksisterendeFasade,

        /// <summary>
        /// Vedlegg - RedegjoerelseGrunnforhold
        /// </summary>
        RedegjoerelseGrunnforhold,

        /// <summary>
        /// Vedlegg - ErklaeringAnsvarsrett
        /// </summary>
        ErklaeringAnsvarsrett,

        /// <summary>
        /// Vedlegg - Gjennomfoeringsplan
        /// </summary>
        Gjennomfoeringsplan,

        /// <summary>
        /// Vedlegg - BoligspesifikasjonMatrikkel
        /// </summary>
        BoligspesifikasjonMatrikkel,

        /// <summary>
        /// Vedlegg - UttalelseVedtakAnnenOffentligMyndighet
        /// </summary>
        UttalelseVedtakAnnenOffentligMyndighet,

        /// <summary>
        /// Vedlegg - Annet
        /// </summary>
        Annet,

        /// <summary>
        /// Vedlegg - Dispensasjonssoeknad
        /// </summary>
        Dispensasjonssoeknad,

        /// <summary>
        /// Vedlegg - RekvisisjonAvOppmaalingsforretning
        /// </summary>
        RekvisisjonAvOppmaalingsforretning,

        /// <summary>
        /// Vedlegg - SluttrapportForAvfallsplan
        /// </summary>
        SluttrapportForBygningsavfall,

        /// <summary>
        /// Vedlegg - Foto
        /// </summary>
        Foto,

        /// <summary>
        /// Vedlegg - Kart
        /// </summary>
        Kart,

        /// <summary>
        /// Vedlegg - KvitteringInnleveringAvfall
        /// </summary>
        KvitteringInnleveringAvfall,

        /// <summary>
        /// Vedlegg - KommentarNabomerknader
        /// </summary>
        KommentarNabomerknader,

        /// <summary>
        /// Vedlegg - Nabomerknader
        /// </summary>
        Nabomerknader,

        /// <summary>
        /// Vedlegg - Organisasjonsplan
        /// </summary>
        Organisasjonsplan,

        /// <summary>
        /// Vedlegg - RedegjoerelseEstetikk
        /// </summary>
        RedegjoerelseEstetikk,

        /// <summary>
        /// Vedlegg - RedegjoerelseForurensetGrunn
        /// </summary>
        RedegjoerelseForurensetGrunn,

        /// <summary>
        /// Vedlegg - RedegjoerelseAndreNaturMiljoeforhold
        /// </summary>
        RedegjoerelseAndreNaturMiljoeforhold,

        /// <summary>
        /// Vedlegg - RedegjoerelseSkredOgFlom
        /// </summary>
        RedegjoerelseSkredOgFlom,

        /// <summary>
        /// Vedlegg - Revisjonserklaering
        /// </summary>
        Revisjonserklaering,

        /// <summary>
        /// Vedlegg - TegningEksisterendePlan
        /// </summary>
        TegningEksisterendePlan,

        /// <summary>
        /// Vedlegg - TegningEksisterendeSnitt
        /// </summary>
        TegningEksisterendeSnitt,

        /// <summary>
        /// Vedlegg - TegningNyFasade
        /// </summary>
        TegningNyFasade,

        /// <summary>
        /// Vedlegg - TegningNyPlan
        /// </summary>
        TegningNyPlan,

        /// <summary>
        /// Vedlegg - TegningNyttSnitt
        /// </summary>
        TegningNyttSnitt,

        /// <summary>
        /// Vedlegg - AvklaringVA
        /// </summary>
        AvklaringVA,

        /// <summary>
        /// Vedlegg - ByggesaksBIM
        /// </summary>
        ByggesaksBIM,

        /// <summary>
        /// Vedlegg - AvklaringHoyspent
        /// </summary>
        AvklaringHoyspent,

        /// <summary>
        /// Vedlegg - UttalelseKulturminnemyndighet
        /// </summary>
        UttalelseKulturminnemyndighet,

        /// <summary>
        /// Vedlegg - SamtykkeArbeidstilsynet
        /// </summary>
        SamtykkeArbeidstilsynet,

        /// <summary>
        /// Vedlegg - AvkjoeringstillatelseVegmyndighet
        /// </summary>
        AvkjoeringstillatelseVegmyndighet,

        /// <summary>
        /// Vedlegg - RedegjoerelseUnntakTEK
        /// </summary>
        RedegjoerelseUnntakTEK,

        /// <summary>
        /// Vedlegg - UnderlagUtnytting
        /// </summary>
        UnderlagUtnytting,

        /// <summary>
        /// Vedlegg - Folgebrev
        /// </summary>
        Folgebrev,

        /// <summary>
        /// Vedlegg - KvitteringForLevertAvfall
        /// </summary>
        KvitteringForLevertAvfall,

        /// <summary>
        /// Vedlegg - OpphoerAnsvarsrett
        /// </summary>
        OpphoerAnsvarsrett,

        /// <summary>
        /// Vedlegg - Utomhusplan
        /// </summary>
        Utomhusplan,

        /// <summary>
        /// Vedlegg - SamtykkePlassering
        /// </summary>
        SamtykkePlassering,

        /// <summary>
        /// Vedlegg - PlanForUavhengigKontroll
        /// </summary>
        PlanForUavhengigKontroll,

        /// <summary>
        /// Vedlegg - Soknad
        /// </summary>
        Soknad,

        /// <summary>
        /// Vedlegg - BeskrivelseTypeArbeidProsess
        /// </summary>
        BeskrivelseTypeArbeidProsess,

        /// <summary>
        /// Vedlegg - Arbeidsmiljofaktorer
        /// </summary>
        Arbeidsmiljofaktorer,

        /// <summary>
        /// Vedlegg - AnsattesMedvirkning
        /// </summary>
        AnsattesMedvirkning,

        /// <summary>
        /// Vedlegg - ArbeidsgiversMedvirkning
        /// </summary>
        ArbeidsgiversMedvirkning,

        /// <summary>
        /// Vedlegg - ForUtleiebygg
        /// </summary>
        ForUtleiebygg,

        /// <summary>
        /// Vedlegg - BistandFraBHT
        /// </summary>
        BistandFraBHT,

        /// <summary>
        /// Vedlegg - Ventilasjon
        /// </summary>
        Ventilasjon,

        /// <summary>
        /// Vedlegg - AnsvarsrettSelvbygger
        /// </summary>
        AnsvarsrettSelvbygger,

        /// <summary>
        /// Vedlegg - KartPlanavgrensing
        /// </summary>
        KartPlanavgrensing,

        /// <summary>
        /// Vedlegg - ReferatOppstartsmoete
        /// </summary>
        ReferatOppstartsmoete,

        /// <summary>
        /// Vedlegg - Planinitiativ
        /// </summary>
        Planinitiativ,

        /// <summary>
        /// Vedlegg - KartDetaljert
        /// </summary>
        KartDetaljert,

        /// <summary>
        /// Vedlegg - Planprogram
        /// </summary>
        Planprogram,

        /// <summary>
        /// Vedlegg - TiltakshaverSignatur
        /// </summary>
        TiltakshaverSignatur,

        /// <summary>
        /// Vedlegg - ArbeidstakersRepresentant
        /// </summary>
        ArbeidstakersRepresentant,

        /// <summary>
        /// Vedlegg - LeietakerArbeidsgiver
        /// </summary>
        LeietakerArbeidsgiver,

        /// <summary>
        /// Vedlegg - Verneombud
        /// </summary>
        Verneombud,

        /// <summary>
        /// Vedlegg - Arbeidsmiljoutvalg
        /// </summary>
        Arbeidsmiljoutvalg,

        /// <summary>
        /// Vedlegg - Bedriftshelsetjeneste
        /// </summary>
        Bedriftshelsetjeneste,

        /// <summary>
        /// Vedlegg - AndreRedegjoerelser
        /// </summary>
        AndreRedegjoerelser,

        /// <summary>
        /// Vedlegg - BerorteParterXML
        /// </summary>
        BerorteParterXML,

        /// <summary>
        /// Vedlegg - BerorteParterXML
        /// </summary>
        SoknadOmAnsvarsrettTiltaksklasse1,

        /// <summary>
        /// Vedlegg - BerorteParterXML
        /// </summary>
        Miljoesaneringsbeskrivelse,

        /// <summary>
        /// Svar på varsel om planoppstart til berørte parter og høringsmyndigheter xml
        /// </summary>
        MaskinlesbarXmlHoeringsmyndigheter,
        /// <summary>
        /// Svar på varsling til høringsmyndigheter pdf
        /// </summary>
        SkjemaPdfHoeringsmyndigheter,

        /// <summary>
        /// Svar på varsling til høringsmyndigheter vedlegg
        /// </summary>
        SvarPaaVarselOppstartHoeringsmyndigheterVedlegg,
        /// <summary>
        /// Svar på varsling til høringsmyndigheter xml
        /// </summary>
        MaskinlesbarXmlHoeringOgOffentligEttersyn,
        /// <summary>
        /// Svar på varsling til høringsmyndigheter pdf
        /// </summary>
        SkjemaPdfHoeringOgOffentligEttersyn,
        /// <summary>
        /// Svar på varsling til høringsmyndigheter vedlegg
        /// </summary>
        SvarPaaHoeringOgOffentligEttersynVedlegg,
        /// <summary>
        /// Svar på høring og offentlig ettersyn kvittering
        /// </summary>
        DistribusjonHoeringOgOffentligEttersynKvittering,
        /// <summary>
        /// Kommentar på høring og offentlig ettersyn kvittering
        /// </summary>
        KommentarHoeringOgOffentligEttersynKvittering,
        /// <summary>
        /// Varsling til høringsmyndigheter kvittering
        /// </summary>
        VarselTilHoeringsmyndigheterKvittering,
        /// <summary>
        /// Varsel om planoppstart til berørte parter og høringsmyndigheter
        /// </summary>
        VarselTilHoeringsmyndigheter,
        /// <summary>
        /// Varsling til berørte parter kvittering
        /// </summary>
        VarselTilBeroerteParterKvittering,
        /// <summary>
        /// Varsling til berørte parter
        /// </summary>
        VarselTilBeroerteParter,
        /// <summary>
        /// Innsending av reguleringsplanforslag
        /// </summary>
        InnsendingAvReguleringsplanforslag,
        /// <summary>
        /// Innsending av reguleringsplanforslag kvittering
        /// </summary>
        InnsendingAvReguleringsplanforslagKvittering,
        /// <summary>
        /// Rapport over uttalelser til varsel om oppstart av reguleringsplanarbeid
        /// </summary>
        RapportVarselOppstartPlanarbeid,
        /// <summary>
        /// Varsel om planoppstart til berørte parter og høringsmyndigheter
        /// </summary>
        Planvarsel,
        /// <summary>
        /// Svar på varsel om planoppstart til berørte parter og høringsmyndigheter xml
        /// </summary>
        Planuttalelse,
        /// <summary>
        /// Svar på varsel om planoppstart vedlegg
        /// </summary>
        UttalelseVedlegg,
        /// <summary>
        /// Varsel om planoppstart til berørte parter og høringsmyndigheter kvittering
        /// </summary>
        PlanvarselKvittering,
        /// <summary>
        /// Søknad om igangsettingstillatelse
        /// </summary>
        SoeknadOmIgangsettingstillatelse,
        /// <summary>
        /// Kvittering for innsendt søknad om igangsettingstillatelse
        /// </summary>
        SoeknadOmIgangsettingstillatelseKvittering,
    }

    public static class FilTyperForNedlastingHelper
    {
        public static FilTyperForNedlasting GetEnumForAttachmentTypeName(string attachmentTypeName)
        {
            FilTyperForNedlasting parsedFilTyperForNedlasting;

            if (Enum.TryParse(attachmentTypeName, out parsedFilTyperForNedlasting))
            {
                return parsedFilTyperForNedlasting;
            }
            else
            {
                return FilTyperForNedlasting.Annet;
            }
        }
    }
}