using System;
using System.Collections.Generic;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Models.ApiModels
{
    public class Mangel
    {
        ///<summary>
        /// MangelType, kodeliste, KART (situasjonskart).... 
        /// </summary>
        public Kode Type { get; set; }

        /// <summary>
        /// Skal mangel relateres til et sjekkpunkt?
        /// </summary>
        public string ReferanseSjekkpunkt { get; set; }

        /// <summary>
        /// Mangelen skal kunne knyttes mot et evt vilkår
        /// </summary>
        public string ReferanseVilkaarId { get; set; }

        /// <summary>
        /// Id til tiltak
        /// </summary>
        public string ReferanseTiltakId { get; set; }

        /// <summary>
        /// MangelId tildelt av eByggesak
        /// </summary>
        public string MangelId { get; set; }

        /// <summary>
        /// Tittel på mangelen
        /// </summary>
        public string Tittel { get; set; }

        /// <summary>
        /// Melding/Beskrivelse kommunen vil gi til søknadssystem.
        /// Mangel kan være knyttet til et vilkår som er gitt i tidligere vedtak feks RS vedtak har vilkår som må besvares i IG eller FA
        /// </summary>
        public string Beskrivelse { get; set; }

        /// <summary>
        /// Dato for når mangelen ble registrert
        /// </summary>
        public DateTime? RegistrertDato { get; set; }

        


        public Mangel()
        {

        }

        public Mangel(Deviation deviation)
        {
            Type = new Kode(deviation.DeviationType, deviation.DeviationDescription);
            ReferanseSjekkpunkt = deviation.Checkpoint;
            ReferanseVilkaarId = deviation.TermsId;
            ReferanseTiltakId = deviation.TiltakId;
            MangelId = deviation.ExternalDeviationId;
            Tittel = deviation.Title;
            Beskrivelse = deviation.Description;
            RegistrertDato = deviation.RegistrationDate;
        }

        public static List<Mangel> MappingToList(ICollection<Deviation> deviations)
        {
            var mangler = new List<Mangel>();
            foreach (var deviation in deviations)
            {
                var mangel = new Mangel(deviation);
                mangler.Add(mangel);
            }
            return mangler;
        }
    }
}