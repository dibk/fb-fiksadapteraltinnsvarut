﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Models.ApiModels
{
    public class Vedtak
    {
        /// <summary>
        /// I SvarUt så kommer AltinnArkivreferanse inn som eksternRef
        /// </summary>
        [Required]
        public string AltinnArkivreferanse { get; set; }
        /// <summary>
        /// Melding kommunen vil gi til søknadssystem
        /// </summary>
        [Required]
        public string Melding { get; set; }
        /// <summary>
        /// Referanse til kommunens saksår (henger sammen med sakssekvensnr)
        /// </summary>
        public int Saksaar { get; set; }
        /// <summary>
        /// Referanse til kommunens sakssekvensnummer (henger sammen med saksår)
        /// </summary>
        public long Sakssekvensnummer { get; set; }

        /// <summary>
        /// Referanse til offentlig journal (kommunens postliste)
        /// </summary>
        public string OffentligJournalUrl { get; set; }

        public DateTime? Vedtaksdato { get; set; }

        public string Status { get; set; }

        public Vilkaar[] Vilkaar { get; set; }

        /// <summary>
        /// Referanse til virksomhet/kommune som sender vedtak
        /// </summary>
        public string Organisasjonsnummer { get; set; }
        /// <summary>
        /// Referanse til utgående dokument fra kommunen 
        /// </summary>
        public string UtgaaendeDokumentUrl { get; set; }
    }

    public class Vilkaar
    {
        public Vilkaar(string id, string prosess, string beskrivelse)
        {
            VilkaarID = id;
            VilkaarTilProsess = prosess;
            Beskrivelse = beskrivelse;
        }

        public string VilkaarID { get; set; }

        /// <summary>
        /// IG, MB, FA
        /// </summary>
        public string VilkaarTilProsess { get; set; }

        public string Beskrivelse { get; set; }
    }
}