﻿namespace KommIT.FIKS.AdapterAltinnSvarUt.Models
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.33440")]
        [System.SerializableAttribute()]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://skjema.kxml.no/dibk/DekryptertFnr")]
        [System.Xml.Serialization.XmlRootAttribute("DekryptertFnr", Namespace = "http://skjema.kxml.no/dibk/DekryptertFnr", IsNullable = false)]
        public class DecryptedFnr
        {
            public Tiltakshaver Tiltakshaver { get; set; }
        }

        public class Tiltakshaver
        {
            public string Foedselsnummer { get; set; }
        }
}