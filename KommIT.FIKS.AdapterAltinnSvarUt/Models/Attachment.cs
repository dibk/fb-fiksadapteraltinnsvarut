﻿using KommIT.FIKS.AdapterAltinnSvarUt.QueuePreparators;
using no.kxml.skjema.dibk.vedlegg;
using System;
using System.Linq;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Models
{
    public class Attachment
    {
        public static string FilenameMainFormPdf = "Skjema.pdf";
        public static string FilenameMainFormXml = "Skjema.xml";

        public string ArchiveReference { get; set; }
        public byte[] AttachmentData { get; set; }
        public string AttachmentType { get; set; }
        public string AttachmentTypeName { get; set; }
        public string FileName { get; set; }
        public bool IsEncrypted { get; set; }

        public Attachment(string archiveReference, byte[] attachmentData, string attachmentType, string attachmentTypeName, string fileName, bool isEncrypted)
        {
            ArchiveReference = archiveReference;
            AttachmentData = attachmentData;
            AttachmentType = attachmentType;
            AttachmentTypeName = attachmentTypeName;
            FileName = fileName;
            IsEncrypted = isEncrypted;
        }

        public Attachment()
        {
        }

        public BlobAttachmentMetadata GetVedleggMetadata(VedleggsopplysningerType vedleggsopplysninger)
        {
            if (vedleggsopplysninger == null) return null;

            var opplysninger = vedleggsopplysninger.vedlegg.FirstOrDefault(p => p.vedleggstype.Equals(this.AttachmentTypeName, StringComparison.OrdinalIgnoreCase)
                                                                                && p.vedleggsUrl.Equals(this.FileName, StringComparison.OrdinalIgnoreCase));
            if (opplysninger != null)
            {
                var metadata = new BlobAttachmentMetadata
                {
                    VedleggId =  opplysninger.vedleggsid,
                    VedleggFilnavn = this.FileName,
                    VedleggVersjonsnr = opplysninger.tegningsnr,
                    VedleggVersjonsdato = opplysninger.tegningsdato,
                    VedleggKategori = opplysninger.vedleggskategori,
                    VedleggIfcValidationId = opplysninger.ifcValidationId,
                    VedleggFulgtNabovarsel = opplysninger.vedleggFulgtNabovarsel,
                    VedleggBeskrivelse = opplysninger.beskrivelse

                };

                return metadata;
            }

            return null;
        }
    }
}