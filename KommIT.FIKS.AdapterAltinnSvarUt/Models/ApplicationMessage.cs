﻿namespace KommIT.FIKS.AdapterAltinnSvarUt.Models
{
    public class ApplicationMessage
    {
        public const string Key = "ApplicationMessage";
        public const string TypeSuccess = "success";
        public const string TypeError = "error";

        public string Message { get; }
        public string MessageType { get; }

        public ApplicationMessage(string message, string messageType)
        {
            Message = message;
            MessageType = messageType;
        }
    }
}