﻿namespace KommIT.FIKS.AdapterAltinnSvarUt.Models
{
    public class Receiver
    {
        public Municipality Municipality { get; }

        public Receiver(Municipality municipality)
        {
            Municipality = municipality;
        }

        public string GetOrganizationNumber() => Municipality?.OrganizationNumber;

        public string GetMunicipalityCode() => Municipality?.Code;

        public string GetName() => Municipality?.Name;
    }
}