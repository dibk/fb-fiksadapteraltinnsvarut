﻿using KommIT.FIKS.AdapterAltinnSvarUt.Services;
using System.Collections.Generic;
using System.Linq;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Models.PrefillChecklist
{
    public class PrefillChecklistInput
    {
        public string ProcessCategory { get; set; }
        public List<string> EnterpriseTerms { get; set; }
        public string DataFormatId { get; set; }
        public string DataFormatVersion { get; set; }
        public IEnumerable<string> Errors { get; set; }
        public IEnumerable<string> Warnings { get; set; }
        public IEnumerable<string> ExecutedValidations { get; set; }

        public static PrefillChecklistInput Map(ValidationResult validationResult)
        {
            return new PrefillChecklistInput()
            {
                ProcessCategory = validationResult.Soknadtype,
                EnterpriseTerms = validationResult.tiltakstyperISoeknad,
                Errors = validationResult.messages.Where(m => m.messagetype.Equals("ERROR")).Select(m => m.reference).ToList(),
                Warnings = validationResult.messages.Where(m => m.messagetype.Equals("WARNING")).Select(m => m.reference).ToList(),
                ExecutedValidations = validationResult.rulesChecked.Select(r => r.id).ToList()
            };
        }
    }
}