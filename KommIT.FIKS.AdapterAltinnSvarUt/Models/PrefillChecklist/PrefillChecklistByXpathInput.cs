﻿using KommIT.FIKS.AdapterAltinnSvarUt.Services;
using System.Collections.Generic;
using System.Linq;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Models.PrefillChecklist
{
    public class PrefillChecklistByXpathInput
    {
        public string ProcessCategory { get; set; }
        public List<string> EnterpriseTerms { get; set; }
        public string Form { get; set; }

        public static PrefillChecklistByXpathInput Map(string form, string soknadstype, List<string> tiltakstyper)
        {
            return new PrefillChecklistByXpathInput()
            {
                ProcessCategory = soknadstype,
                EnterpriseTerms = tiltakstyper,
                Form = form
            };
        }
    }
}