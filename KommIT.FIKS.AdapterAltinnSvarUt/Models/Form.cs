﻿namespace KommIT.FIKS.AdapterAltinnSvarUt.Models
{
    public class Form
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string DefaultTitle { get; set; }

        public Form() { }

        public Form(int id, string name, string defaultTitle)
        {
            Id = id;
            Name = name;
            DefaultTitle = defaultTitle;
        }
    }
}