﻿using System.Collections.Generic;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Models.ViewModels
{
    public class LogViewModel
    {
        public List<LogByArchiveReferenceViewModel> log { get; set; }
    }
}