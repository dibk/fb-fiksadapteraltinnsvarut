﻿using System.Collections.Generic;
using System.Linq;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Models.ViewModels
{
    public abstract class ViewModelMapperBase<TViewModel, TModel>
    {
        public abstract TViewModel MapToViewModel(TModel input);

        public List<TViewModel> MapToViewModel(List<TModel> listOfObjects)
        {
            return listOfObjects.Select(MapToViewModel).ToList();
        }
    }
}