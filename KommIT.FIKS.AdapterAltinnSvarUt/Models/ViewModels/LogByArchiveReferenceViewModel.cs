﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Models.ViewModels
{
    public class LogByArchiveReferenceViewModel
    {
        public string ArchiveReference { get; set; }
        public string Recipients { get; set; }
        public string Status { get; set; }
        public string FormType { get; set; }
        public string SenderApplication { get; set; }

        public string SenderType { get; set; }
        public string ServiceCode { get; set; }

        public int ServiceEditionCode { get; set; }
        public DateTime? ArchiveTimestamp { get; set; }

        public LogByArchiveReferenceViewModel(FormMetadata formMetadata)
        {
            ArchiveReference = formMetadata.ArchiveReference;
            FormType = formMetadata.FormType;
            SenderApplication = formMetadata.Application;

            Recipients = formMetadata.SendTo;
            Status = formMetadata.Status;
            SenderType = formMetadata.SenderSystem;
            ArchiveTimestamp = formMetadata.ArchiveTimestamp;
            ServiceCode = formMetadata.ServiceCode;
            ServiceEditionCode = formMetadata.ServiceEditionCode;

            //TODO recipients when distribution
        }
    }
}