﻿namespace KommIT.FIKS.AdapterAltinnSvarUt.Models.ViewModels
{
    public abstract class ViewModelTwoToOneMapperBase<TOutput, TInput1>
    {
        public abstract TOutput MapToViewModel(TInput1 input1);

    }
}