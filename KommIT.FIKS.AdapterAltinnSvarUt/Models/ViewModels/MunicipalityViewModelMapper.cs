﻿namespace KommIT.FIKS.AdapterAltinnSvarUt.Models.ViewModels
{
    public class MunicipalityViewModelMapper : ViewModelMapperBase<MunicipalityViewModel, Municipality>
    {
        public override MunicipalityViewModel MapToViewModel(Municipality input)
        {
            return new MunicipalityViewModel
            {
                Name = input.Name,
                Code = input.Code,
                OrganizationNumber = input.OrganizationNumber,
                PlanningDepartmentSpecificOrganizationNumber = input.PlanningDepartmentSpecificOrganizationNumber,
                NewMunicipalityCode = input.NewMunicipalityCode,
                ValidTo = input.ValidTo,
                ValidFrom = input.ValidFrom
            };
        }
    }
}