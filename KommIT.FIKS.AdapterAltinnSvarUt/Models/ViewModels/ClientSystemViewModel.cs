﻿using System.Collections.Generic;
using System.ComponentModel;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Models.ViewModels
{
    public class ClientSystemAdministrationViewModel
    {
        public List<ClientSystemViewModel> ClientSystems { get; set; }
    }

    public class ClientSystemViewModel
    {
        /// <summary>
        /// Systemets unike identifikator
        /// </summary>
        [DisplayName("Systemnøkkel")]
        public string SystemKey { get; set; }

        /// <summary>
        /// Visningsnavn for systemet
        /// </summary>
        [DisplayName("Systemnavn")]
        public string DisplayName { get; set; }

        /// <summary>
        /// Beskrivelse av systemet
        /// </summary>
        [DisplayName("Beskrivelse/info")]
        public string Description { get; set; }

        /// <summary>
        /// Semikolonseparert liste med verdier
        /// </summary>
        [DisplayName("System identifikatorer")]
        public string Identifiers { get; set; }
    }
}