﻿namespace KommIT.FIKS.AdapterAltinnSvarUt.Models.ViewModels
{
    public class MunicipalitySettingsViewModelMapper : ViewModelTwoToOneMapperBase<MunicipalitySettingsViewModel, Municipality>
    {
        public override MunicipalitySettingsViewModel MapToViewModel(Municipality municipality)
        {
            
            return new MunicipalitySettingsViewModel
            {
                Municipality = new MunicipalityViewModelMapper().MapToViewModel(municipality),
            };
        }

    }
}