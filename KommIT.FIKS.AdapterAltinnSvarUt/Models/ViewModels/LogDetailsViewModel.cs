﻿using KommIT.FIKS.AdapterAltinnSvarUt.Models.ApiModels;
using System;
using System.Collections.Generic;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Models.ViewModels
{
    public class LogDetailsViewModel
    {
        public string ArchiveReference { get; set; }
        public List<LogEntry> Log { get; set; }
        public Municipality Municipality { get; set; }
        public string SvarUtForsendelsesId { get; set; }
        public string SvarUtStatus { get; set; }
        public string SvarUtDocumentTitle { get; set; }
        public DateTime? ArchiveTimestamp { get; set; }
        public string FormType { get; set; }
        public DateTime? SvarUtShippingTimestamp { get; set; }
        public int MunicipalityArchiveCaseYear { get; set; }
        public long MunicipalityArchiveCaseSequence { get; set; }

        public string MunicipalityPublicArchiveCaseUrl { get; set; }

        public string DistributionRecieptLink { get; set; }
        public string SendTo { get; set; }

        public List<Vilkaar> Vilkaar { get; set; }

        public List<Distribusjon> Distribusjoner { get; set; }

        public LogDetailsViewModel(List<LogEntry> log, FormMetadata formMetadata)
        {
            if (formMetadata != null)
            {
                ArchiveReference = formMetadata.ArchiveReference;
                SvarUtForsendelsesId = formMetadata.SvarUtForsendelsesId;
                Municipality = formMetadata.Municipality;
                SvarUtDocumentTitle = formMetadata.SvarUtDocumentTitle;
                ArchiveTimestamp = formMetadata.ArchiveTimestamp;
                FormType = formMetadata.FormType;
                SvarUtShippingTimestamp = formMetadata.SvarUtShippingTimestamp;
                MunicipalityArchiveCaseYear = formMetadata.MunicipalityArchiveCaseYear;
                MunicipalityArchiveCaseSequence = formMetadata.MunicipalityArchiveCaseSequence;
                DistributionRecieptLink = formMetadata.DistributionRecieptLink;
                SendTo = formMetadata.SendTo;
            }
            Log = log;
        }

        public string GetReceiver()
        {
            if (!string.IsNullOrEmpty(SendTo))
                return SendTo;


            return GetMunicipalityName();
        }

        public string GetMunicipalityName()
        {
            if (Municipality != null)
            {
                return Municipality.Name;
            }
            else
            {
                return null;
            }
        }
    }
}