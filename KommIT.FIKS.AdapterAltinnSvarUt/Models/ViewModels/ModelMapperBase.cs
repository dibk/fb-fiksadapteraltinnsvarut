﻿using System.Collections.Generic;
using System.Linq;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Models.ViewModels
{
    public abstract class ModelMapperBase<TViewModel, TModel>
    {
        public abstract TModel MapToModel(TViewModel input);

        public List<TModel> MapToModel(List<TViewModel> listOfObjects)
        {
            return listOfObjects.Select(MapToModel).ToList();
        }

    }
}