﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Models.ViewModels
{
    /// <summary>
    ///     Mapping utility for converting between domain object and view model
    /// </summary>
    /// <typeparam name="TInput">the input object, e.g. Activity</typeparam>
    /// <typeparam name="TOutput">the output object, e.g. ActivitiesListItemViewModel</typeparam>
    public abstract class Mapper<TInput, TOutput>
    {
        public abstract IEnumerable<TOutput> MapToEnumerable(IEnumerable<TInput> inputs);

        public abstract TOutput Map(TInput input);

        public IEnumerable<TOutput> Map(IEnumerable<TInput> inputs)
        {
            var output = new List<TOutput>();
            foreach (var input in inputs)
            {
                output.Add(Map(input));
            }
            return output;
        }
    }
}