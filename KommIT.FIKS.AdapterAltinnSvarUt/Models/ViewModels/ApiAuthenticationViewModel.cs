﻿using System.ComponentModel.DataAnnotations;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Models.ViewModels
{
    public class ApiAuthenticationViewModel
    {
        [Required]
        [Display(Name = "Brukernavn")]
        public string Username { get; set; }

        [Required]
        [Display(Name = "Passord")]
        public string Password { get; set; }
    }
}