﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Encryption.TextEncyption;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Models
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.33440")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://skjema.kxml.no/dibk/gjenpartNabovarselV2/0.92")]
    [System.Xml.Serialization.XmlRootAttribute("KvitteringForNabovarsel", Namespace = "http://skjema.kxml.no/dibk/gjenpartNabovarselV2/0.92", IsNullable = false)]
    public class Naboeier
    {
        public NaboGjenboer[] naboeier { get; set; }
    }
    public class NaboGjenboer
    {
        public string encryptedFoedselsnummer { get; set; }
        public string foedselsnummer { get; set; }
    }

    /*
     * 3_KORR_OpplysningerGittINabovarselDekrypteringsNoekkel_Opplysninger gitt i nabovarsel-maskinlesbar_nøkkel-UNNTATT-OFFENTLIGHET.xml
     *
            <?xml version="1.0" encoding="utf-8"?>
            <KvitteringForNabovarsel xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns="http://skjema.kxml.no/dibk/gjenpartNabovarselV2/0.92">
              <naboeier>
                <NaboGjenboer>
                  <encryptedFoedselsnummer>ncX7R6YaiCKMNxmEku/UkLCpivaC140mq4hyGXP3q+1SqDHmh1z9x1+rO0KVcq1o5Si+Q1ISqYhokevrlYXTcJMHQhdGk+zWNlyBN1UM+zPyhVKpFdlMr6ooEj1Ar297/sjvJunTXo4/6qemwDk3h815RKhsDduAMPQEAJgj6n0=</encryptedFoedselsnummer>
                  <foedselsnummer>29106500248</foedselsnummer>
                </NaboGjenboer>
                <NaboGjenboer>
                  <encryptedFoedselsnummer>Gbreq0OKTFsJpumD8xKw7byj4LTPQ57nCYZ2IsiodMyOKC/iNFIGvKsXaie0eGmMzXZluVJpqeGSXmVvCreyfv6p9kNg40VcDi6HgwNDwRiEEyGiltFUqG24VKgy8zxpImTHz81xGgqIirlzvKXI8BAdxM6ZtswShQiaCqDNXCo=</encryptedFoedselsnummer>
                  <foedselsnummer>29106500833</foedselsnummer>
                </NaboGjenboer>
                <NaboGjenboer>
                  <encryptedFoedselsnummer>dWzBOH1/7L63fLiNXsHd8+2rIVmjm7jwDDkIc6WtgddJSM16Rw8HOi8b2RIq8VoJPFDnMY6D0t75GjZKBUtdG/KHgdiN6oyIcrMsvBpuQZyg6Pfof8fmRya8XoTqBu6EKbnhHEv2BktyZ4mJIJzimfbBwt6mre/k2WIjofNsIv0=</encryptedFoedselsnummer>
                  <foedselsnummer>19035400098</foedselsnummer>
                </NaboGjenboer>
              </naboeier>
            </KvitteringForNabovarsel>
    */


}