using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using KommIT.FIKS.AdapterAltinnSvarUt.Models.ApiModels;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Models
{
    public class Deviation
    {
        [Key]
        public Guid DeviationId { get; set; }
        public string DeviationType { get; set; }
        public string DeviationDescription { get; set; }
        public string Checkpoint { get; set; }
        public string TermsId { get; set; }
        public string TiltakId { get; set; }
        public string ExternalDeviationId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime? RegistrationDate { get; set; }
        public string Status { get; set; }
        public DateTime? LastUpdated { get; set; }
        

        [ForeignKey("DeviationLetter")]
        public Guid DeviationLetterId { get; set; }
        public virtual DeviationLetter DeviationLetter { get; set; }

        public Deviation()
        {
            DeviationId = Guid.NewGuid();
        }

        public Deviation(Mangel mangel)
        {
            DeviationId = Guid.NewGuid();
            DeviationType = mangel.Type?.Kodeverdi;
            DeviationDescription = mangel.Type?.Kodebeskrivelse;
            Checkpoint = mangel.ReferanseSjekkpunkt;
            TermsId = mangel.ReferanseVilkaarId;
            TiltakId = mangel.ReferanseTiltakId;
            ExternalDeviationId = mangel.MangelId;
            Title = mangel.Tittel;
            Description = mangel.Beskrivelse;
            RegistrationDate = mangel.RegistrertDato;
            LastUpdated = DateTime.Now;
        }

        public static List<Deviation> MapToList(Guid deviationLetterId, List<Mangel> mangler)
        {
            var deviations = new List<Deviation>();
            foreach (var mangel in mangler)
            {
                var deviation = new Deviation(mangel);
                deviation.DeviationLetterId = deviationLetterId;
                deviations.Add(deviation);
            }
            return deviations;
        }
    }
}