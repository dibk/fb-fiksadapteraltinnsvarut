﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Models.UserAccess
{
    public class APIAuthentication
    {
        [Key]
        [ForeignKey("ApplicationUser")]
        public string UserId { get; set; }

        public virtual ApplicationUser ApplicationUser { get; set; }

        public string Username { get; set; }
        public string Password { get; set; }

        public APIAuthentication()
        {
        }

        public APIAuthentication(string username, string password, string userId)
        {
            Username = username;
            Password = password;
            UserId = userId;
        }
    }
}