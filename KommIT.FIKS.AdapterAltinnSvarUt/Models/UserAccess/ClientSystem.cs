﻿namespace KommIT.FIKS.AdapterAltinnSvarUt.Models.UserAccess
{
    /// <summary>
    /// Systeminformasjon for å kunne koble bruker til system
    /// </summary>
    public class ClientSystem
    {
        /// <summary>
        /// Systemets nøkkelfelt; brukes ved mapping til bruker claims
        /// </summary>
        public string SystemKey { get; set; }

        /// <summary>
        /// Visningsnavn for systemet
        /// </summary>
        public string DisplayName { get; set; }

        /// <summary>
        /// Beskrivelse av systemet
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Semikolonseparert liste med verdier
        /// </summary>
        public string Identifiers { get; set; } //Feks semikolonseparert liste med verdier
    }
}