﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Models
{
    public class AnSaKoStatus
    {        
        [Key]
        [JsonIgnore]
        public int Id { get; set; }
        /// <summary>
        /// Statuuuuuus!
        /// </summary>
        public AnSaKoProcessStatusType AnSaKoProcessStatus { get; set; }
        /// <summary>
        /// Frist for signering
        /// </summary>
        public DateTime? SigningDeadline { get; set; }
        /// <summary>
        /// Kan feks innehalde beskrivelse av kvifor den er blitt trukket eller avvist
        /// </summary>
        public string StatusDetails { get; set; }
        public virtual DistributionForm DistributionForm { get; set; }
    }

    public enum AnSaKoProcessStatusType
    {
        /// <summary>
        /// Erklæring er åpnet av ansvarlig foretak, men ikke signert eller avvist.
        /// </summary>
        iArbeid,
        /// <summary>
        /// Erklæring er sendt til signering, men ikke signert eller avvist.
        /// </summary>
        tilSignering,
        /// <summary>
        /// Erklæring er signert med eller uten endringer fra ansvarlig foretak.
        /// </summary>
        signert,
        /// <summary>
        /// Erklæring er avvist og returnert fra ansvarlig foretak.
        /// </summary>
        avvist,
        /// <summary>
        /// Erklæring er ikke signert eller avvist innen tidsfristen.
        /// </summary>
        utgått,
        /// <summary>
        /// Erklæring er trukket tilbake fra ansvarlig søker.
        /// </summary>
        trukket,
        /// <summary>
        /// Sendinger som har feilet grunnet tekniske problemer.
        /// </summary>
        feilet,
        /// <summary>
        /// Erklæringer som er signert utenfor Fellestjenester BYGG, for eksempel på blankett.
        /// </summary>
        signertManuelt,
        /// <summary>
        /// Erklæringer hvor ansvarsområdet er avsluttet.
        /// </summary>
        avsluttet
    }
}