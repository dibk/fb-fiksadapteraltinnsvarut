﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Models
{
    public class AltinnMetadata
    {
        public string ServiceName { get; set; }
        public string ServiceCode { get; set; }
        public string ServiceEditionCode { get; set; }
        public string ServiceType { get; set; }
        public List<FormMetaData> SubFormsMetaData { get; set; }
        public List<AttachmentRule> AttachmentRules { get; set; }

    }

    public class FormMetaData
    {
        public string FormName { get; set; }

        public string DataFormatID { get; set; }
        public string DataFormatVersion { get; set; }
    }

    public class AttachmentRule
    {
        public string AttachmentTypeName { get; set; }
        public string AllowedFileTypes { get; set; }
        public int MaxAttachmentCount { get; set; }
        public int MaxFileSize { get; set; }
        public int MinAttachmentCount { get; set; }
    }
}