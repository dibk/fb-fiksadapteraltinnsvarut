﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Models.Validation
{
    [AttributeUsage(AttributeTargets.Property)]
    public class StringArrayValidationAttribute : ValidationAttribute
    {
        public bool AllowEmptyValues { get; set; }
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var stringArray = value as string[];

            if (stringArray != null)
            {
                foreach (var item in stringArray)
                {
                    if (!AllowEmptyValues)
                        if (string.IsNullOrEmpty(item))
                            if (string.IsNullOrEmpty(this.ErrorMessage))
                                return new ValidationResult($"Det er ikke lov med tomme verdier i listen {validationContext.MemberName}");
                            else
                                return new ValidationResult($"{this.ErrorMessage}");
                }

                return ValidationResult.Success;
            }

            return ValidationResult.Success;
        }
    }
}