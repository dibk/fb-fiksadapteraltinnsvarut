﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Models
{
    public class UpdateMunicipalityFormDocumentTypeModel
    {

        public string Name { get; set; }
        public string Code { get; set; }

        public string MunicipalityCode { get; set; }

        public UpdateMunicipalityFormDocumentTypeModel()
        {
        }

        public UpdateMunicipalityFormDocumentTypeModel(string code, string name, string municipalityCode)
        {
            Code = code;
            Name = name;
            MunicipalityCode = municipalityCode;
        }


    }
}