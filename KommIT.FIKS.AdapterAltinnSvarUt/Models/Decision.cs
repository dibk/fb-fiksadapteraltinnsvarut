﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Models
{
    public class Decision
    {
        public Decision()
        {
            Terms = new List<Terms>();
        }

        [Key]
        public long DecisionId { get; set; }

        [Index]
        [StringLength(maximumLength: 80)]
        public string ArchiveReference { get; set; }
        public int MunicipalityArchiveCaseYear { get; set; }
        public long MunicipalityArchiveCaseSequence { get; set; }

        public string MunicipalityPublicArchiveCaseUrl { get; set; }
        [Index]
        public DateTime? DecisionDate { get; set; }
        public string DecisionStatus { get; set; }
        public virtual ICollection<Terms> Terms { get; set; }
        public string DocumentUrl { get; set; }
    }
}