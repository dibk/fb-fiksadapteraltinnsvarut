﻿using KommIT.FIKS.AdapterAltinnSvarUt.Models.ApiModels;
using System.Collections.Generic;


namespace KommIT.FIKS.AdapterAltinnSvarUt.Models.Signatur
{
    public abstract class TiltakshaverSamtykkeAbstractClass
    {
        //public abstract TiltakshaversSamtykke tiltakshaversSamtykke { get; set; }
        /// <summary>
        /// Navn på tiltakshaver
        /// </summary>
        public abstract string Tiltakshaver { get; set; }
        /// <summary>
        /// E-post til tiltakshaver
        /// </summary>
        public abstract string TiltakshaverEpost { get; set; }
        /// <summary>
        /// Na abstract n på ansvarlig søker
        /// </summary>
        public abstract string AnsvarligSoeker { get; set; }
        /// <summary>
        /// Prosjektnavn fra xml
        /// </summary>
        public abstract string Prosjektnavn { get; set; }
        /// <summary>
        /// Or abstract anisasjonsnummer til ansvarlig søker når den ansvarlige søkeren er en organisasjon.
        /// </summary>
        public abstract string AnsvarligSoekerOrganisasjonsnummer { get; set; }

        /// <summary>
        /// Telefon til ansvarlig søker
        /// </summary>
        public abstract string AnsvarligSokerTelefon { get; set; }

        /// <summary>
        /// E-post til ansvarlig søker
        /// </summary>
        public abstract string AnsvarligSokerEpost { get; set; }

        /// <summary>
        /// Tiltakshavers org nummer hvis dette er tilgjengelig
        /// </summary>
        public abstract string TiltakshaverOrgnr { get; set; }

        /// <summary>
        /// Partstype kode for tiltakshaver https://register.geonorge.no/kodelister/byggesoknad/partstype
        /// </summary>
        public abstract string TiltakshaverPartstypeKode { get; set; }


        public abstract ICollection<SamtykkeTiltakshaverNyByggested> Byggested { get; set; }
        public abstract ICollection<SamtykkeTiltakshaverNyTiltaktype> Tiltaktypes { get; set; }

    }


}