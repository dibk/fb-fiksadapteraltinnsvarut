﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Models.Signatur
{
    public class TiltakshaversamtykkeFactory
    {
        public TiltakshaverSamtykkeBase GetTiltakshaverSamtykkeBase(string dataFormatIdAndDataFormatVersion, string xmlData)
        {
            switch (dataFormatIdAndDataFormatVersion)
            {
                case "6741:45752":
                    return new RammeSoknadV3Dfv45752SaByBase(xmlData); 
                case "6740:45751":
                    return new EttTrinnSoknadV3Dfv45751SaByBase(xmlData);
                default:
                    return null;
            }
        }
    }
}