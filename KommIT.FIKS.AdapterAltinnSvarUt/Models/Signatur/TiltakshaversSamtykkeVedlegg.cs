﻿using System.ComponentModel.DataAnnotations;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Models.Signatur
{
    public class TiltakshaversSamtykkeVedlegg
    {
        [Key]
        public int VedleggId { get; set; }

        public string Vedleggstype { get; set; }

        public string Navn { get; set; }

        public string Referanse { get; set; }

        public string Storrelse { get; set; }
        public bool Uploaded { get; set; }
        public virtual TiltakshaversSamtykke TiltakshaversSamtykke { get; set; }
    }
}