﻿using KommIT.FIKS.AdapterAltinnSvarUt.Models.ApiModels.Internal;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Models.Signatur
{
    public class TiltakshaversSamtykke
    {
        [Key]
        public Guid Id { get; set; }

        /// <summary>
        /// Navn på tiltakshaver
        /// </summary>
        public string TiltakshaverNavn { get; set; }

        /// <summary>
        /// Eventuelt orgnr for tiltakshaver
        /// </summary>
        public string TiltakshaverOrgnr { get; set; }

        /// <summary>
        /// Partstype for tiltakshaver
        /// </summary>
        /// <see cref="https://register.geonorge.no/kodelister/byggesoknad/partstype"/>
        public string TiltakshaverPartstypeKode { get; set; }

        /// <summary>
        /// Signert som fødselsnummer om tiltakshaver er person
        /// </summary>
        public string SignertTiltakshaverFnr { get; set; }

        /// <summary>
        /// Signert som organisasjonsnummer om tiltakshaver er en organisasjon
        /// </summary>
        public string SignertTiltakshaverOrgnr { get; set; }

        /// <summary>
        /// Referanse til signert skjema i altinn for tiltakshavers samtykke
        /// </summary>
        public string AltinnArkivreferanse { get; set; }

        /// <summary>
        /// Navn på ansvarlig søker
        /// </summary>
        public string AnsvarligSoeker { get; set; }

        /// <summary>
        /// Organisasjonsnummer til ansvarlig søker når den ansvarlige søkeren er en organisasjon.
        /// </summary>
        public string AnsvarligSoekerOrganisasjonsnummer { get; set; }

        /// <summary>
        /// Status på prosess: 0 - opprettet, 1 - Signert, 2 - Arkivert/slettet
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// Søknad prosjektnavn
        /// </summary>
        public string Prosjektnavn { get; set; }

        /// <summary>
        /// Schema service Code
        /// </summary>
        public string ServiceCode { get; set; }

        /// <summary>
        /// Schema service Edition code
        /// </summary>
        public string ServiceEdition { get; set; }

        /// <summary>
        /// Who sends the scheme
        /// </summary>
        public string FraSluttbrukerSystem { get; set; }

        /// <summary>
        /// Date and time when the scheme is registered in the database.
        /// </summary>
        public DateTime DateCreated { get; set; }

        /// <summary>
        /// Date and time when the entity was marked as deleted
        /// </summary>
        public DateTime? DateDeleted { get; set; }

        /// <summary>
        /// Liste med byggested (eiendommer og adresse)
        /// </summary>
        public virtual ICollection<TiltakshaversSamtykkeByggested> Byggested { get; set; }

        public ICollection<SamtykkeTiltakshaverByggested> GetApiListByggested()
        {
            ICollection<SamtykkeTiltakshaverByggested> apibyggested = new Collection<SamtykkeTiltakshaverByggested>();
            if (Byggested != null)
            {
                foreach (var item in this.Byggested)
                {
                    apibyggested.Add(new SamtykkeTiltakshaverByggested(item));
                }
            }
            return apibyggested;
        }

        /// <summary>
        /// Liste med vedlegg
        /// </summary>
        public virtual ICollection<TiltakshaversSamtykkeVedlegg> Vedlegg { get; set; }

        public ICollection<SamtykkeTiltakshaverVedlegg> GetApiListVedlegg()
        {
            ICollection<SamtykkeTiltakshaverVedlegg> apiVedlegg = new Collection<SamtykkeTiltakshaverVedlegg>();
            if (Vedlegg != null)
            {
                foreach (var item in this.Vedlegg)
                {
                    apiVedlegg.Add(new SamtykkeTiltakshaverVedlegg(item));
                }
            }
            return apiVedlegg;
        }

        /// <summary>
        /// Liste med Tiltaktype
        /// </summary>
        public ICollection<TiltakshaversSamtykkeTiltaktype> Tiltaktypes { get; set; }

        public ICollection<SamtykkeTiltakshaverTiltaktype> GetApiListTiltaktype()
        {
            ICollection<SamtykkeTiltakshaverTiltaktype> apiTiltaktype = new Collection<SamtykkeTiltakshaverTiltaktype>();
            if (Tiltaktypes != null)
            {
                foreach (var item in this.Tiltaktypes)
                {
                    apiTiltaktype.Add(new SamtykkeTiltakshaverTiltaktype(item));
                }
            }
            return apiTiltaktype;
        }
    }

    public static class TiltakshaverSamtykkeConstants
    {
        public const int Opprettet = 0;
        public const int Signert = 1;
        public const int Arkivert = 2;
        public const int Slettet = 3;

        public static string GetStringValue(int status)
        {
            var retval = "Opprettet";

            if (status == Signert)
                retval = "Signert";

            if (status == Arkivert)
                retval = "Arkivert";

            if (status == Opprettet)
                retval = "Opprettet";

            if (status == Slettet)
                retval = "Slettet";

            return retval;
        }
    }
}