﻿using KommIT.FIKS.AdapterAltinnSvarUt.Models.ApiModels;
using System.ComponentModel.DataAnnotations;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Models.Signatur
{
    public class TiltakshaversSamtykkeByggested
    {
        public TiltakshaversSamtykkeByggested()
        {
        }

        public TiltakshaversSamtykkeByggested(SamtykkeTiltakshaverNyByggested item)
        {
            this.Kommunenr = item.Kommunenr;
            this.Gardsnr = item.Gardsnr;
            this.Bruksnr = item.Bruksnr;
            this.Festenr = item.Festenr;
            this.Seksjonsnr = item.Seksjonsnr;
            this.Adresselinje1 = item.Adresselinje1;
            this.Postnr = item.Postnr;
            this.Poststed = item.Poststed;
            this.Bygningsnummer = item.Bygningsnummer;
            this.Bolignummer = item.Bolignummer;
            this.Kommunenavn = item.Kommunenavn;
        }

        [Key]
        public int ByggestedId { get; set; }

        public string Kommunenr { get; set; }
        public string Gardsnr { get; set; }
        public string Bruksnr { get; set; }
        public string Festenr { get; set; }
        public string Seksjonsnr { get; set; }
        public string Adresselinje1 { get; set; }
        public string Postnr { get; set; }
        public string Poststed { get; set; }
        public string Bygningsnummer { get; set; }
        public string Bolignummer { get; set; }
        public string Kommunenavn { get; set; }
        public virtual TiltakshaversSamtykke TiltakshaversSamtykke { get; set; }
    }
}