﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KommIT.FIKS.AdapterAltinnSvarUt.Models.ApiModels;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;
using no.kxml.skjema.dibk.etttrinnsoknadV3;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Models.Signatur
{
    public class EttTrinnSoknadV3Dfv45751SaBy : TiltakshaverSamtykkeAbstractClass
    {
        private string _tiltakshaver;
        private string _tiltakshaverEpost;
        private string _ansvarligSoeker;
        private string _ansvarligSoekerOrganisasjonsnummer;
        private string _prosjektnavn;
        private string _ansvarligSokerEpost;
        private string _ansvarligSokerTelefon;
        private string _tiltakshaverOrgnr;
        private string _tiltakshaverPartstypeKode;

        private ICollection<SamtykkeTiltakshaverNyByggested> _byggested;
        private ICollection<SamtykkeTiltakshaverNyTiltaktype> _tiltaktypes;
        public EttTrinnSoknadV3Dfv45751SaBy(string xmlData)
        {
            var form = SerializeUtil.DeserializeFromString<EttTrinnType>(xmlData);
            _tiltakshaver = form.tiltakshaver?.navn;
            _tiltakshaverEpost = FormHelpers.GetPartTypeEpost(form.tiltakshaver);
            _ansvarligSoeker = form.ansvarligSoeker?.navn;
            _ansvarligSoekerOrganisasjonsnummer = form.ansvarligSoeker?.organisasjonsnummer;
            _byggested = FormHelpers.ParseByggesteds(form.eiendomByggested);
            _tiltaktypes = FormHelpers.Parsetiltaktypes(form.beskrivelseAvTiltak.type);
            _prosjektnavn = form.metadata.prosjektnavn;
            _ansvarligSokerTelefon = FormHelpers.GetPartTypeTelefonnummer(form.ansvarligSoeker);
            _ansvarligSokerEpost = FormHelpers.GetPartTypeEpost(form.ansvarligSoeker);
            _tiltakshaverOrgnr = form.tiltakshaver?.organisasjonsnummer;
            _tiltakshaverPartstypeKode = form.tiltakshaver?.partstype?.kodeverdi;
        }
        public override string AnsvarligSokerEpost
        {
            get { return _ansvarligSokerEpost; }
            set { _ansvarligSokerEpost = value; }
        }

        public override string AnsvarligSokerTelefon
        {
            get { return _ansvarligSokerTelefon; }
            set { _ansvarligSokerTelefon = value; }
        }

        public override string Prosjektnavn
        {
            get { return _prosjektnavn; }
            set { _prosjektnavn = value; }
        }
        public override string Tiltakshaver
        {
            get { return _tiltakshaver; }
            set { _tiltakshaver = value; }
        }

        public override string TiltakshaverEpost
        {
            get { return _tiltakshaverEpost; }
            set { _tiltakshaverEpost = value; }
        }

        public override string AnsvarligSoeker
        {
            get { return _ansvarligSoeker; }
            set { _ansvarligSoeker = value; }
        }

        public override string AnsvarligSoekerOrganisasjonsnummer
        {
            get { return _ansvarligSoekerOrganisasjonsnummer; }
            set { _ansvarligSoekerOrganisasjonsnummer = value; }
        }

        public override ICollection<SamtykkeTiltakshaverNyByggested> Byggested
        {
            get { return _byggested; }
            set { _byggested = value; }
        }

        public override ICollection<SamtykkeTiltakshaverNyTiltaktype> Tiltaktypes
        {
            get { return _tiltaktypes; }
            set { _tiltaktypes = value; }
        }

        public override string TiltakshaverOrgnr
        {
            get { return _tiltakshaverOrgnr; }
            set { _tiltakshaverOrgnr = value; }
        }
        public override string TiltakshaverPartstypeKode
        {
            get { return _tiltakshaverPartstypeKode; }
            set { _tiltakshaverPartstypeKode = value; }
        }

        public static class FormHelpers
        {
            public static ICollection<SamtykkeTiltakshaverNyByggested> ParseByggesteds(EiendomType[] formEiendomByggested)
            {


                ICollection<SamtykkeTiltakshaverNyByggested> byggesteds = new List<SamtykkeTiltakshaverNyByggested>();
                foreach (EiendomType eiendomType in formEiendomByggested)
                {
                    var eiendom = new SamtykkeTiltakshaverNyByggested()
                    {
                        Adresselinje1 = eiendomType.adresse?.adresselinje1,
                        Bolignummer = eiendomType.bolignummer,
                        Bruksnr = eiendomType.eiendomsidentifikasjon?.bruksnummer,
                        Festenr = eiendomType.eiendomsidentifikasjon?.festenummer,
                        Gardsnr = eiendomType.eiendomsidentifikasjon?.gaardsnummer,
                        Seksjonsnr = eiendomType.eiendomsidentifikasjon?.seksjonsnummer,
                        Bygningsnummer = eiendomType.bygningsnummer,
                        Kommunenavn = eiendomType.kommunenavn,
                        Kommunenr = eiendomType.eiendomsidentifikasjon?.kommunenummer,
                        Postnr = eiendomType.adresse?.postnr,
                        Poststed = eiendomType.adresse?.poststed,
                    };
                    byggesteds.Add(eiendom);
                }
                return byggesteds;
            }

            public static ICollection<SamtykkeTiltakshaverNyTiltaktype> Parsetiltaktypes(KodeType[] formBeskrivelseAvTiltakTypes)
            {
                ICollection<SamtykkeTiltakshaverNyTiltaktype> tiltaktypes = new List<SamtykkeTiltakshaverNyTiltaktype>();
                foreach (var tiltakType in formBeskrivelseAvTiltakTypes)
                {
                    var typeSamtykkeTiltaktype = new SamtykkeTiltakshaverNyTiltaktype()
                    {
                        kodeverdi = tiltakType.kodeverdi,
                        kodebeskrivelse = tiltakType.kodebeskrivelse
                    };
                    tiltaktypes.Add(typeSamtykkeTiltaktype);
                }
                return tiltaktypes;
            }

            public static string GetPartTypeEpost(PartType partType)
            {
                string epost = null;
                if (!Helpers.ObjectIsNullOrEmpty(partType))
                    epost = string.IsNullOrEmpty(partType.kontaktperson?.epost) ? partType.epost : partType.kontaktperson.epost;

                return epost;
            }
            public static string GetPartTypeTelefonnummer(PartType partType)
            {
                string telefonnummer = null;
                if (!Helpers.ObjectIsNullOrEmpty(partType))
                    telefonnummer = string.IsNullOrEmpty(partType.kontaktperson?.telefonnummer) ? partType.telefonnummer : partType.kontaktperson.telefonnummer;

                return telefonnummer;
            }
        }
    }
}