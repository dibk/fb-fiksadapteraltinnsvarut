﻿using KommIT.FIKS.AdapterAltinnSvarUt.Models.ApiModels;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Models.Signatur
{
    public class TiltakshaversSamtykkeTiltaktype
    {
        public TiltakshaversSamtykkeTiltaktype()
        {
        }

        public TiltakshaversSamtykkeTiltaktype(SamtykkeTiltakshaverNyTiltaktype item)
        {
            this.kodebeskrivelse = item.kodebeskrivelse;
            this.kodeverdi = item.kodeverdi;
            this.TiltakshaversSamtykkes = new List<TiltakshaversSamtykke>();
        }

        [Key]
        public int SamtykkeTiltaktypeID { get; set; }

        public string kodeverdi { get; set; }
        public string kodebeskrivelse { get; set; }

        public virtual ICollection<TiltakshaversSamtykke> TiltakshaversSamtykkes { get; set; }
        //public virtual TiltakshaversSamtykke TiltakshaversSamtykke { get; set; }
    }
}