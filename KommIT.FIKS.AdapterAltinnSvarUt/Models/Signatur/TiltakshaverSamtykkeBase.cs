﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Models.Signatur
{
    public abstract class TiltakshaverSamtykkeBase
    {
        public abstract TiltakshaverSamtykkeAbstractClass GeTiltakshaverSamtykkeModel();
    }
    public class RammeSoknadV3Dfv45752SaByBase : TiltakshaverSamtykkeBase
    {
        private string _xmlData;


        public RammeSoknadV3Dfv45752SaByBase(string xmlData)
        {
            _xmlData = xmlData;
        }

        public override TiltakshaverSamtykkeAbstractClass GeTiltakshaverSamtykkeModel()
        {
            return new RammeSoknadV3Dfv45752SaBy(_xmlData);
        }
    }
    public class EttTrinnSoknadV3Dfv45751SaByBase : TiltakshaverSamtykkeBase
    {
        private string _xmlData;


        public EttTrinnSoknadV3Dfv45751SaByBase(string xmlData)
        {
            _xmlData = xmlData;
        }

        public override TiltakshaverSamtykkeAbstractClass GeTiltakshaverSamtykkeModel()
        {
            return new EttTrinnSoknadV3Dfv45751SaBy(_xmlData);
        }
    }
}