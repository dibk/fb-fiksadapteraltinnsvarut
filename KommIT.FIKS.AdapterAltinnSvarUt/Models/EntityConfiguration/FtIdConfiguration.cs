﻿using System.Data.Entity.ModelConfiguration;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Models.EntityConfiguration
{
    internal static class FtIdConfiguration
    {
        internal static void Configure(EntityTypeConfiguration<FtId> configuration)
        {
            configuration
                .HasKey(ftId => ftId.EAN)
                .Property(ftId => ftId.EAN)
                .HasMaxLength(18)
                .IsRequired();

            configuration
                .Property(ftId => ftId.Kommunenummer)
                .IsRequired();

            configuration
                .Property(ftId => ftId.Gaardsnummer)
                .IsRequired();

            configuration
                .Property(ftId => ftId.Bruksnummer)
                .IsRequired();

            configuration
                .Property(ftId => ftId.Prosjektnavn)
                .IsRequired();

            configuration
                .Property(ftId => ftId.System)
                .IsRequired();

            configuration
                .Property(ftId => ftId.Distributed)
                .IsRequired();
        }
    }
}