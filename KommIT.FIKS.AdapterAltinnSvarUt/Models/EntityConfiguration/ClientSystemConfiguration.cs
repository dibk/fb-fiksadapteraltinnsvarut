﻿using KommIT.FIKS.AdapterAltinnSvarUt.Models.UserAccess;
using System.Data.Entity.ModelConfiguration;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Models.EntityConfiguration
{
    internal static class ClientSystemConfiguration
    {
        internal static void Configure(EntityTypeConfiguration<ClientSystem> configuration)
        {
            configuration
                .HasKey(system => system.SystemKey)
                .Property(system => system.SystemKey)
                .IsRequired();

            configuration
                .Property(system => system.DisplayName)
                .IsRequired();
            configuration
                .Property(system => system.Identifiers)
                .IsRequired();
        }
    }
}