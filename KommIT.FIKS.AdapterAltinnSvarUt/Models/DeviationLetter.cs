﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using KommIT.FIKS.AdapterAltinnSvarUt.Models.ApiModels;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Models
{
    public class DeviationLetter
    {
        [Key]
        public Guid DeviationLetterId { get; set; }
        [Index]
        [StringLength(maximumLength: 80)]
        public string AltinnArchiveReference { get; set; }
        public int MunicipalityArchiveCaseYear { get; set; }
        public long MunicipalityArchiveCaseSequence { get; set; }
        public string MunicipalityPublicArchiveCaseUrl { get; set; }
        public int ArchiveYear { get; set; }
        public int ArchiveSequence { get; set; }
        public DateTime? RegistrationDate { get; set; }
        public string Title { get; set; }
        public string OrganizationNumber { get; set; }
        public string MunicipalityCode { get; set; }
        /// <summary>
        /// Gaardsnummer
        /// </summary>
        public int CadastralUnitNumber { get; set; }
        /// <summary>
        /// Bruksnummer
        /// </summary>
        public int PropertyUnitNumber { get; set; }
        /// <summary>
        /// Festenummer
        /// </summary>
        public int LeaseholdNumber { get; set; }
        /// <summary>
        /// Seksjonsnummer
        /// </summary>
        public int CondominiumUnitNumber { get; set; }

        public virtual ICollection<Deviation> Deviations { get; set; }

        public DeviationLetter()
        {
            DeviationLetterId = Guid.NewGuid();
        }

        public DeviationLetter(Mangelbrev mangelbrev)
        {
            DeviationLetterId = Guid.NewGuid();
            AltinnArchiveReference = mangelbrev.AltinnArkivreferanse;
            MunicipalityArchiveCaseYear = mangelbrev.Saksaar;
            MunicipalityArchiveCaseSequence = mangelbrev.Sakssekvensnummer;
            MunicipalityPublicArchiveCaseUrl = mangelbrev.OffentligJournalUrl;
            ArchiveYear = mangelbrev.Journalaar;
            ArchiveSequence = mangelbrev.Journalsekvensnummer;
            RegistrationDate = mangelbrev.DokumentetsDato;
            Title = mangelbrev.Tittel;
            OrganizationNumber = mangelbrev.Organisasjonsnummer;
            MunicipalityCode = mangelbrev.Matrikkelnummer.Kommunenummer;
            CadastralUnitNumber = mangelbrev.Matrikkelnummer.Gaardsnummer;
            PropertyUnitNumber = mangelbrev.Matrikkelnummer.Bruksnummer;
            LeaseholdNumber = mangelbrev.Matrikkelnummer.Festenummer;
            CondominiumUnitNumber = mangelbrev.Matrikkelnummer.Seksjonsnummer;
        }

    }

    
}