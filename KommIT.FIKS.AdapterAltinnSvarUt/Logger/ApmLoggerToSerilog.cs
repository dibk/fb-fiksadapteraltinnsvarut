﻿using Elastic.Apm.Logging;
using System;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Logger
{
    internal class ApmLoggerToSerilog : IApmLogger
    {
        private readonly Lazy<Serilog.ILogger> _logger;
        private readonly LogLevel _severityLevel;

        public bool IsEnabled(LogLevel level) => level >= _severityLevel;

        public ApmLoggerToSerilog(LogLevel severityLevel) 
        {
            _severityLevel = severityLevel;
            _logger = new Lazy<Serilog.ILogger>(() => Serilog.Log.Logger.ForContext<ApmLoggerToSerilog>());  
        }

        public void Log<TState>(LogLevel level, TState state, Exception e, Func<TState, Exception, string> formatter)
        {
            var serilogLevel = ConvertLogLevel(level);
            var serilogger = _logger.Value;

            if (!serilogger.IsEnabled(serilogLevel))
                return;

            var message = formatter(state, e);
            if (e == null)
                serilogger.Write(serilogLevel, message);
            else
                serilogger.Write(serilogLevel, e, message);
        }

        private static Serilog.Events.LogEventLevel ConvertLogLevel(LogLevel level)
        {
            switch (level)
            {
                case LogLevel.Trace:
                    return Serilog.Events.LogEventLevel.Verbose;

                case LogLevel.Debug:
                    return Serilog.Events.LogEventLevel.Debug;

                case LogLevel.Information:
                    return Serilog.Events.LogEventLevel.Information;

                case LogLevel.Warning:
                    return Serilog.Events.LogEventLevel.Warning;

                case LogLevel.Error:
                    return Serilog.Events.LogEventLevel.Error;

                case LogLevel.Critical:
                    return Serilog.Events.LogEventLevel.Fatal;

                case LogLevel.None:
                    return Serilog.Events.LogEventLevel.Fatal;

                default:
                    throw new ArgumentOutOfRangeException(nameof(level), level, null);
            }
        }
    }
}