﻿using Hangfire.Client;
using Hangfire.Common;
using Hangfire.Server;
using Hangfire.States;
using Hangfire.Storage;
using Serilog;
using System.Collections.Generic;
using System.Text;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Logger
{
    /// <summary>
    /// Attribute used for logging background jobs status
    /// </summary>
    public class HangfireBasicJobLogger : JobFilterAttribute, IServerFilter, IElectStateFilter, IClientFilter, IApplyStateFilter
    {
        private ILogger _logger => GetLogger();
        public void OnPerforming(PerformingContext context)
        {
            var logMessageFormatBuilder = new StringBuilder();
            logMessageFormatBuilder.Append("Background job {backgroundJobId} is {backgroundJobState} - initiating method {backgroundJobMethodName}");

            var logParams = new List<object>
            {
                context?.BackgroundJob?.Id,
                "Performing",
                context?.BackgroundJob?.Job?.Method?.Name
            };

            BuildMessageAndParams(logMessageFormatBuilder, logParams, context?.BackgroundJob?.Job?.Args);

            _logger.Information(logMessageFormatBuilder.ToString(), logParams.ToArray());
        }

        public void OnPerformed(PerformedContext context)
        {
            var logMessageFormatBuilder = new StringBuilder();
            logMessageFormatBuilder.Append("Background job {backgroundJobId} is {backgroundJobState} - initiating method {backgroundJobMethodName}");

            var logParams = new List<object>
            {
                context?.BackgroundJob?.Id,
                "Performed",
                context?.BackgroundJob?.Job?.Method?.Name
            };

            BuildMessageAndParams(logMessageFormatBuilder, logParams, context?.BackgroundJob?.Job?.Args);

            _logger.Information(logMessageFormatBuilder.ToString(), logParams.ToArray());
        }

        protected virtual ILogger GetLogger()
        { return Serilog.Log.ForContext<HangfireBasicJobLogger>(); }

        protected virtual void BuildMessageAndParams(StringBuilder logFormat, List<object> logParams, IReadOnlyList<object> args)
        {
            if (args.Count > 0)
                logFormat.Append(" with parameters");

            for (int i = 0; i < args.Count; i++)
            {
                logFormat.Append($" {i}");
                logParams.Add(args[i]);
            }
        }

        public void OnStateElection(ElectStateContext context)
        {
            var failedState = context.CandidateState as FailedState;
            if (failedState != null)
            {
                var logMessageFormatBuilder = new StringBuilder();
                logMessageFormatBuilder.Append("Background job {backgroundJobId} is {backgroundJobState} due to an exception - initiating method {backgroundJobMethodName} Exception was {backgroundJobException}");

                var logParams = new List<object>
                {
                    context?.BackgroundJob?.Id,
                    "Failing",
                    context?.BackgroundJob?.Job?.Method?.Name
                };
                BuildMessageAndParams(logMessageFormatBuilder, logParams, context?.BackgroundJob?.Job?.Args);

                _logger.Warning(failedState.Exception, logMessageFormatBuilder.ToString(), logParams.ToArray());
            }
        }

        public void OnCreating(CreatingContext context)
        {
            var logMessageFormatBuilder = new StringBuilder();
            logMessageFormatBuilder.Append("{backgroundJobState} background job for method {backgroundJobMethodName}");

            var logParams = new List<object>
            {
                "Creating",
                context?.Job?.Method?.Name
            };
            BuildMessageAndParams(logMessageFormatBuilder, logParams, context?.Job?.Args);

            _logger.Information(logMessageFormatBuilder.ToString(), logParams.ToArray());
        }

        public void OnCreated(CreatedContext context)
        {
            if (context.Exception != null)
            {
                _logger.Error(context.Exception, "Background job creation failed");
                return;
            }


            if (context.Canceled == false)
            {
                var logMessageFormatBuilder = new StringBuilder();
                logMessageFormatBuilder.Append("Background job {backgroundJobId} is {backgroundJobState} - initiating method {backgroundJobMethodName}");

                var logParams = new List<object>
                {
                    context?.BackgroundJob?.Id,
                    "Created",
                    context?.BackgroundJob?.Job?.Method?.Name
                };

                BuildMessageAndParams(logMessageFormatBuilder, logParams, context?.BackgroundJob?.Job?.Args);

                _logger.Information(logMessageFormatBuilder.ToString(), logParams.ToArray());
            }
            else
            {
                var jobName = context.Parameters["RecurringJobId"];
                _logger.Information($"OnCreated raised for {jobName}, job marked as canceled");
            }
        }

    public void OnStateApplied(ApplyStateContext context, IWriteOnlyTransaction transaction)
    {
        var logMessageFormatBuilder = new StringBuilder();
        logMessageFormatBuilder.Append("Background job {backgroundJobId} changed state from {backgroundJobFromState} to {backgroundJobToState} - initiating method {backgroundJobMethodName}");

        var logParams = new List<object>
        {
            context?.BackgroundJob?.Id,
            context?.OldStateName,
            context?.NewState?.Name,
            context?.BackgroundJob?.Job?.Method?.Name
        };

        BuildMessageAndParams(logMessageFormatBuilder, logParams, context?.BackgroundJob?.Job?.Args);

        _logger.Information(logMessageFormatBuilder.ToString(), logParams.ToArray());
    }

    public void OnStateUnapplied(ApplyStateContext context, IWriteOnlyTransaction transaction)
    {
        var logMessageFormatBuilder = new StringBuilder();
        logMessageFormatBuilder.Append("Background job {backgroundJobId} state {backgroundJobFromState} was unapplied - initiating method {backgroundJobMethodName}");

        var logParams = new List<object>
        {
            context?.BackgroundJob?.Id,
            context?.OldStateName,
            context?.BackgroundJob?.Job?.Method?.Name
        };

        BuildMessageAndParams(logMessageFormatBuilder, logParams, context?.BackgroundJob?.Job?.Args);

        _logger.Information(logMessageFormatBuilder.ToString(), logParams.ToArray());
    }
}
}