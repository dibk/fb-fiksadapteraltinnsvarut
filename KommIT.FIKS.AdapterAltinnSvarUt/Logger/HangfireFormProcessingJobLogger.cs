﻿using System.Collections.Generic;
using System.Text;
using Serilog;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Logger
{
    public class HangfireFormProcessingJobLogger : HangfireBasicJobLogger
    {
        protected override ILogger GetLogger()
        {
            return Serilog.Log.ForContext<HangfireFormProcessingJobLogger>();
        }

        protected override void BuildMessageAndParams(StringBuilder logFormat, List<object> logParams, IReadOnlyList<object> args)
        {
            if (args.Count > 0)
                logFormat.Append(" with parameters");

            for (int i = 0; i < args.Count; i++)
            {
                if (args[i].ToString().StartsWith("AR"))
                    logFormat.Append(" ArchiveReference {ArchiveReference}");
                else
                    logFormat.Append($" {i}");

                logParams.Add(args[i]);
            }
        }
    }
}