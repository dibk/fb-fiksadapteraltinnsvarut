﻿using KommIT.FIKS.AdapterAltinnSvarUt.Logger.SerilogEnrichers.WebApi;
using Serilog;
using Serilog.Configuration;
using System;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Logger.SerilogEnrichers
{
    public static class SerilogConfigurationExtensions
    {
        public static LoggerConfiguration WithBasicAuthUserName(this LoggerEnrichmentConfiguration enrichmentConfiguration,
                                                                string anonymousUsername = "(anonymous)",
                                                                string noneUsername = null)
        {
            if (enrichmentConfiguration == null) throw new ArgumentNullException(nameof(enrichmentConfiguration));
            return enrichmentConfiguration.With(new BasicAuthUsernameEnricher(anonymousUsername, noneUsername));
        }

        public static LoggerConfiguration WithCorrelationIdHeader(this LoggerEnrichmentConfiguration enrichmentConfiguration,
                                                                  string headerKey = "x-correlation-id")
        {
            if (enrichmentConfiguration == null) throw new ArgumentNullException(nameof(enrichmentConfiguration));
            return enrichmentConfiguration.With(new CorrelationIdHeaderEnricher(headerKey));
        }

        public static LoggerConfiguration WithUserId(this LoggerEnrichmentConfiguration enrichmentConfiguration,
                                                     string anonymousUsername = "(anonymous)")
        {
            if (enrichmentConfiguration == null) throw new ArgumentNullException(nameof(enrichmentConfiguration));
            return enrichmentConfiguration.With(new UserIdEnricher(anonymousUsername));
        }

        public static LoggerConfiguration WithWebApiControllerName(this LoggerEnrichmentConfiguration enrichmentConfiguration)
        {
            if (enrichmentConfiguration == null) throw new ArgumentNullException(nameof(enrichmentConfiguration));
            return enrichmentConfiguration.With(new WebApiControllerNameEnricher());
        }

        public static LoggerConfiguration WithWebApiActionName(this LoggerEnrichmentConfiguration enrichmentConfiguration)
        {
            if (enrichmentConfiguration == null) throw new ArgumentNullException(nameof(enrichmentConfiguration));
            return enrichmentConfiguration.With(new WebApiActionNameEnricher());
        }

        public static LoggerConfiguration WithClaimValue(this LoggerEnrichmentConfiguration enrichmentConfiguration,
                                                         string claimProperty,
                                                         string logEventProperty = null)
        {
            if (enrichmentConfiguration == null) throw new ArgumentNullException(nameof(enrichmentConfiguration));
            return enrichmentConfiguration.With(new ClaimValueEnricher(claimProperty, logEventProperty));
        }
    }
}