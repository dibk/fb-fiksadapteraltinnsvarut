﻿using Microsoft.AspNet.Identity;
using Serilog.Core;
using Serilog.Events;
using System;
using System.Web;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Logger.SerilogEnrichers
{
    public class UserIdEnricher : ILogEventEnricher
    {
        private readonly string _anonymousUserId;

        /// <summary>
        /// The property name added to enriched log events.
        /// </summary>
        private const string _logEventPropertyName = "UserId";

        /// <summary>
        /// Initializes a new instance of the <see cref="UserIdEnricher"/> class.
        /// </summary>
        public UserIdEnricher()
            : this("(anonymous)")
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="UserIdEnricher"/> class.
        /// </summary>
        /// <param name="anonymousUser">The anonymous username. Leave null if you do not want to use anonymous user names. By default it is (anonymous).</param>
        public UserIdEnricher(string anonymousUser = "(anonymous)")
        {
            _anonymousUserId = anonymousUser;
        }

        public void Enrich(LogEvent logEvent, ILogEventPropertyFactory propertyFactory)
        {
            if (logEvent == null)
                throw new ArgumentNullException("logEvent");

            if (HttpContext.Current != null)
            {
                HttpContextWrapper httpContextWrapper = new HttpContextWrapper(HttpContext.Current);
                if (httpContextWrapper.User != null)
                {
                    if (httpContextWrapper.User.Identity != null)
                    {
                        string userId = httpContextWrapper.User.Identity.GetUserId();
                        if (string.IsNullOrEmpty(userId))
                        {
                            Console.WriteLine("UserId is null or empty. Using anonymous user id.");
                            userId = _anonymousUserId;
                        }
                        else { Console.WriteLine("UserId is not null or empty."); }
                        var userIdProperty = new LogEventProperty(_logEventPropertyName, new ScalarValue(userId));
                        logEvent.AddOrUpdateProperty(userIdProperty);
                    }
                }
            }
        }
    }
}