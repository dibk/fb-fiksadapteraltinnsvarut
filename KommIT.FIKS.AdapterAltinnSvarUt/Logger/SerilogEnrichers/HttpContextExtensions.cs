﻿using System.Web;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Logger.SerilogEnrichers
{
    public static class HttpContextExtensions
    {
        public static bool HasRequest(this HttpContext context)
        {
            try
            {
                var request = context.Request;
                return true;
            }
            catch (System.Web.HttpException)
            {
                return false;
            }
        }
    }
}