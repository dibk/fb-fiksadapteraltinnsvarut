﻿using Serilog.Core;
using Serilog.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Logger.SerilogEnrichers
{
    public class CorrelationIdHeaderEnricher : ILogEventEnricher
    {
        private const string CorrelationIdPropertyName = "CorrelationId";
        private readonly string _headerKey;

        public CorrelationIdHeaderEnricher(string headerKey)
        {
            _headerKey = headerKey;
        }

        public void Enrich(LogEvent logEvent, ILogEventPropertyFactory propertyFactory)
        {
            if (HttpContext.Current == null)
                return;

            var correlationId = GetCorrelationId();

            var correlationIdProperty = propertyFactory.CreateProperty(CorrelationIdPropertyName, correlationId);

            logEvent.AddPropertyIfAbsent(correlationIdProperty);
        }

        private string GetCorrelationId()
        {
            if (!HttpContext.Current.HasRequest())
                return string.Empty;

            var header = string.Empty;

            IEnumerable<string> values;

            if (HttpContext.Current.Request.Headers.TryGetValue(_headerKey, out values))
            {
                header = values.FirstOrDefault();
            }
            else if (GetResponseHeaderKey(_headerKey, out values))
            {
                header = values.FirstOrDefault();
            }

            var correlationId = string.IsNullOrEmpty(header)
                                    ? Guid.NewGuid().ToString()
                                    : header;

            if (!HttpContext.Current.Response.HeadersWritten) 
                if (!ResponseHeaderKeyExists(_headerKey))
                {
                    AddResponseHeaderKey(_headerKey, correlationId);
                }

            return correlationId;
        }

        //These methods are solely made to suppress any exceptions occurring when 
        // interacting with Response.Headers. When the service starts it tends to
        // throw a PlatformNotSupported exception

        private bool GetResponseHeaderKey(string headerKey, out IEnumerable<string> value)
        {
            try
            {
                value = new List<string>() { HttpContext.Current.Response.Headers.Get(headerKey) };
                return true;
            }
            catch
            { }
            value = null;
            return false;
        }

        private bool ResponseHeaderKeyExists(string headerKey)
        {
            try
            {
                if (HttpContext.Current.Response.Headers.AllKeys.Contains(headerKey))
                    return true;
            }
            catch
            {}
            return false;
        }

        private void AddResponseHeaderKey(string headerKey, string value)
        {
            try
            {
                HttpContext.Current.Response.Headers.Add(headerKey, value);
            }
            catch
            {}
        }
    }
}
