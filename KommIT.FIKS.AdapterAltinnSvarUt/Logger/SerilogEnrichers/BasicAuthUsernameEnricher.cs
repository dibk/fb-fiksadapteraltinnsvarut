﻿using Serilog.Core;
using Serilog.Events;
using System;
using System.Text;
using System.Web;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Logger.SerilogEnrichers
{
    public class BasicAuthUsernameEnricher : ILogEventEnricher
    {
        readonly string _anonymousUsername;
        readonly string _noneUsername;

        /// <summary>
        /// The property name added to enriched log events.
        /// </summary>
        public const string UserNamePropertyName = "BasicAuthUserName";

        /// <summary>
        /// Initializes a new instance of the <see cref="BasicAuthUsernameEnricher"/> class.
        /// </summary>
        public BasicAuthUsernameEnricher()
            : this("(anonymous)")
        {}

        /// <summary>
        /// Initializes a new instance of the <see cref="BasicAuthUsernameEnricher"/> class.
        /// </summary>
        /// <param name="anonymousUsername">The anonymous username. Leave null if you do not want to use anonymous user names. By default it is (anonymous).</param>
        /// <param name="noneUsername">The none username. If there is no username to be found, it will output this username. Leave null (default) to ignore non usernames.</param>
        public BasicAuthUsernameEnricher(string anonymousUsername = "(anonymous)", string noneUsername = null)
        {
            _anonymousUsername = anonymousUsername;
            _noneUsername = noneUsername;
        }

        public void Enrich(LogEvent logEvent, ILogEventPropertyFactory propertyFactory)
        {
            if (logEvent == null)
                throw new ArgumentNullException("logEvent");

            var userName = _noneUsername;

            if (HttpContext.Current != null && HttpContext.Current.HasRequest())
            {
                var context = new HttpContextWrapper(HttpContext.Current);

                var containBasicAuth = ExtractUserNameFromHeader(context.Request.Headers["Authorization"], out userName);

                if (!containBasicAuth)
                {
                    userName = _anonymousUsername;
                }

                var userNameProperty = new LogEventProperty(UserNamePropertyName, new ScalarValue(userName));
                logEvent.AddPropertyIfAbsent(userNameProperty);
            }
        }

        public bool ExtractUserNameFromHeader(string authorizationHeader, out string username)
        {
            username = string.Empty;
            bool isSuccess = false;

            if (authorizationHeader != null && authorizationHeader.StartsWith("Basic"))
            {
                string encodedUsernamePassword = authorizationHeader.Substring("Basic ".Length).Trim();
                string decodedCredentials = Encoding.UTF8.GetString(Convert.FromBase64String(encodedUsernamePassword));

                string[] credentials = decodedCredentials.Split(':');

                if (credentials.Length == 2)
                {
                    username = credentials[0];
                    isSuccess = true;
                }
            }
            return isSuccess;
        }
    }
}