﻿namespace KommIT.FIKS.AdapterAltinnSvarUt.Logger.SerilogEnrichers.WebApi
{
    internal enum WebApiRequestInfoKey
    {
        ActionName,
        ControllerName,
        RouteUrlTemplate,
        RouteData
    }
}