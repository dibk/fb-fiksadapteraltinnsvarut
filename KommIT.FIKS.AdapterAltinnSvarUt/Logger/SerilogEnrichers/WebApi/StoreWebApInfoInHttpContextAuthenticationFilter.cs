﻿using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Filters;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Logger.SerilogEnrichers.WebApi
{
    internal class StoreWebApInfoInHttpContextAuthenticationFilter : IAuthenticationFilter
    {
        public bool AllowMultiple => false;

        public Task AuthenticateAsync(HttpAuthenticationContext context, CancellationToken cancellationToken)
        {
            context.ActionContext.StoreWebApInfoInHttpContext();
            return Task.FromResult(0);
        }

        public Task ChallengeAsync(HttpAuthenticationChallengeContext context, CancellationToken cancellationToken)
        {
            return Task.FromResult(0);
        }
    }
}