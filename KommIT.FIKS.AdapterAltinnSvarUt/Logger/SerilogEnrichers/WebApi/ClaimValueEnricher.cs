﻿using Serilog.Core;
using Serilog.Events;
using System;
using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using System.Web;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Logger.SerilogEnrichers.WebApi
{
    /// <summary>
    /// Enrich log events with the named Claim Value.
    /// </summary>
    public class ClaimValueEnricher : ILogEventEnricher
    {
        private readonly string _claimProperty;
        private readonly string _logEventProperty;

        /// <summary>
        /// Initializes a new instance of the <see cref="ClaimValueEnricher"/> class.
        /// </summary>
        public ClaimValueEnricher()
            : this(null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ClaimValueEnricher"/> class.
        /// </summary>
        /// <param name="claimProperty">The claim property name searched for value to enrich log events.</param>
        public ClaimValueEnricher(string claimProperty) : this(claimProperty, null)
        {
            _claimProperty = claimProperty;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ClaimValueEnricher"/> class.
        /// </summary>
        /// <param name="claimProperty">The claim property name searched for value to enrich log events.</param>
        /// <param name="logEventProperty">The property name added to enriched log events.</param>
        public ClaimValueEnricher(string claimProperty, string logEventProperty)
        {
            _claimProperty = claimProperty;
            _logEventProperty = logEventProperty ?? claimProperty;
        }

        /// <summary>
        /// Enrich the log event with found by name claim's value
        /// </summary>
        /// <param name="logEvent">The log event to enrich.</param>
        /// <param name="propertyFactory">Factory for creating new properties to add to the event.</param>
        public void Enrich(LogEvent logEvent, ILogEventPropertyFactory propertyFactory)
        {
            if (logEvent == null) throw new ArgumentNullException(nameof(logEvent));

            if (HttpContext.Current == null)
                return;

            if (CurrentHttpContext.Request == null)
                return;

            var user = HttpContext.Current.User;
            if (user == null)
                return;

            var claims = ((ClaimsIdentity)user.Identity).Claims;

            var value = claims?.FirstOrDefault(c => c.Type == _claimProperty)?.Value;
            if (string.IsNullOrWhiteSpace(value))
                return;

            var claimProperty = new LogEventProperty(_logEventProperty, new ScalarValue(value));
            logEvent.AddPropertyIfAbsent(claimProperty);
        }
    }

    /// <summary>
    /// This helper class is used to handle special case introduced by ASP.NET integrated pipeline
    /// when HttpContextCurrent.Request may throw instead of returning null.
    /// </summary>
    internal static class CurrentHttpContext
    {
        /// <summary>
        /// Gets the <see cref="T:System.Web.HttpRequest"/> object for the current HTTP request.
        /// </summary>
        ///
        /// <returns>
        /// The current HTTP request.
        /// </returns>

        // Attribute added to suppress possible exceptions from breaking into debugger when running in "Just my code" mode.
        [DebuggerNonUserCode]
        internal static HttpRequest Request
        {
            get
            {
                try
                {
                    return HttpContext.Current?.Request;
                }
                catch (HttpException)
                {
                    // No need to check the type of the exception - only one exception can be thrown by .Request and we want to ignore it.
                    return null;
                }
            }
        }
    }
}