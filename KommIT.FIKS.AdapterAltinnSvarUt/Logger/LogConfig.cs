﻿using Elastic.Apm.SerilogEnricher;
using Elastic.Serilog.Sinks;
using Elastic.Transport;
using KommIT.FIKS.AdapterAltinnSvarUt.Logger.SerilogEnrichers;
using Serilog;
using Serilog.Events;
using Serilog.Filters;
using System;
using System.Configuration;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Logger
{
    public class LogConfig
    {
        public static LoggerConfiguration GetLoggerConfiguration()
        {
            var logLevel = (LogEventLevel)Enum.Parse(typeof(LogEventLevel), ConfigurationManager.AppSettings["LogLevel"]);

            var elasticSearchUrl = ConfigurationManager.AppSettings["SearchEngine:ConnectionUrl"];
            var elasticUsername = ConfigurationManager.AppSettings["SearchEngine:ConnectionUsername"];
            var elasticPassword = ConfigurationManager.AppSettings["SearchEngine:ConnectionPassword"];
            var elasticIndexFormat = ConfigurationManager.AppSettings["SearchEngine:SerilogSinkIndexFormat"];

            var config = new LoggerConfiguration()
                .MinimumLevel.Is(logLevel)
                .MinimumLevel.Override("Microsoft", LogEventLevel.Information)
                .MinimumLevel.Override("Dibk.Ftpb.Integration.SvarUt.SvarUtClient", LogEventLevel.Verbose)
                .Enrich.FromLogContext()
                .Enrich.WithCorrelationIdHeader()
                .Enrich.WithElasticApmCorrelationInfo()
                .Enrich.WithWebApiControllerName()
                .Enrich.WithWebApiActionName()
                .Enrich.WithMachineName()
                .Enrich.WithClaimValue("clientsystem", "RequestingClientSystem")
                .Enrich.WithUserId()
                .WriteTo.Trace(outputTemplate: "{Timestamp:HH:mm:ss.fff} {SourceContext} [{Level}] {ArchiveReference} {Message}{NewLine}{Exception}");

            if (!string.IsNullOrEmpty(elasticSearchUrl) && !string.IsNullOrEmpty(elasticUsername) && !string.IsNullOrEmpty(elasticPassword))
            {
                config.WriteTo.Elasticsearch(new Uri[] { new Uri(elasticSearchUrl) },
                                             opts => { 
                                                 opts.DataStream = new Elastic.Ingest.Elasticsearch.DataStreams.DataStreamName(elasticIndexFormat);
                                             },
                                             tr => { tr.Authentication(new BasicAuthentication(elasticUsername, elasticPassword)); });

                //Sink for altinn usage telemetry logs
                config.WriteTo.Logger(lc => lc.Filter.ByIncludingOnly(Matching.FromSource<TelemetryLogger>())
                                .WriteTo.Elasticsearch(new Uri[] { new Uri(elasticSearchUrl) },
                                                       opts => { opts.DataStream = new Elastic.Ingest.Elasticsearch.DataStreams.DataStreamName("altinntelemetry"); },
                                                       tr => { tr.Authentication(new BasicAuthentication(elasticUsername, elasticPassword)); }));

                //Sink for SvarUt forsendelse payload logs
                config.WriteTo.Logger(lc => lc.Filter.ByIncludingOnly(Matching.WithProperty<string>("ForsendelsePayload", p => !string.IsNullOrEmpty(p)))
                                                .WriteTo.Elasticsearch(new Uri[] { new Uri(elasticSearchUrl) },
                                                       opts => { opts.DataStream = new Elastic.Ingest.Elasticsearch.DataStreams.DataStreamName("forsendelsesvarut"); },
                                                       tr => { tr.Authentication(new BasicAuthentication(elasticUsername, elasticPassword)); })
                                            .MinimumLevel.Verbose());
            }

            return config;
        }
    }
}