﻿using Serilog;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Logger
{
    public class RequestResponseLoggingMessageHandler : DelegatingHandler
    {
        private const string HttpRequestEventMessageTemplate = "HTTP {Method} {RawUrl} responded {StatusCode} in {ElapsedMilliseconds}ms";

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            var stopWatch = System.Diagnostics.Stopwatch.StartNew();

            var response = await base.SendAsync(request, cancellationToken).ConfigureAwait(false);

            stopWatch.Stop();
            Log.ForContext<RequestResponseLoggingMessageHandler>().Information(HttpRequestEventMessageTemplate, request.Method, request.RequestUri.AbsolutePath, (int)response.StatusCode, stopWatch.ElapsedMilliseconds);

            return response;
        }
    }
}