﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Logger
{
    public class TelemetryLogger
    {
        public static void LogAltinnTelemetry<T>(string serviceType, Uri serviceUri, string serviceCode, string serviceEdition)
        {
            Serilog.Log.ForContext<TelemetryLogger>()
                        .Information("Altinn {ServiceType} {ServiceCode} - {ServiceEdition} request Uri: {ServiceUri} sourceContext: {TelemetrySourceContext}", serviceType, serviceCode, serviceEdition, serviceUri, typeof(T).FullName);            
        }
    }
}