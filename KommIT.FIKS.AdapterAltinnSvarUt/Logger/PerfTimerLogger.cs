﻿using System;
using Serilog;
using System.Diagnostics;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Logger
{
    public class PerfTimerLogger : IDisposable
    {
        private readonly string _message;
        private readonly ILogger _logger = Log.ForContext<PerfTimerLogger>();
        private Stopwatch _timer;

        public PerfTimerLogger()
        {
            this._timer = new Stopwatch();
            this._timer.Start();
        }

        public PerfTimerLogger(string message)
        {
            this._timer = new Stopwatch();
            this._timer.Start();            
            this._message = message;
        }

        public void Dispose()
        {
            this._timer.Stop();
            var elapsedTime = this._timer.ElapsedMilliseconds;

            var methodName = new StackTrace().GetFrame(1).GetMethod().Name;
            if(string.IsNullOrEmpty(_message))
                _logger.Debug("{methodName} used {elapsedTime}", methodName, elapsedTime);
            else
                _logger.Debug("{methodName} used {elapsedTime} - {detailedMessage}", methodName, elapsedTime, _message);
        }
    }
}