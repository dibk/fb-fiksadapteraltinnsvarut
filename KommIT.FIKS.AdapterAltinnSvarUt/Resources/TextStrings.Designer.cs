﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace KommIT.FIKS.AdapterAltinnSvarUt.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "17.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class TextStrings {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal TextStrings() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("KommIT.FIKS.AdapterAltinnSvarUt.Resources.TextStrings", typeof(TextStrings).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Du har en melding fra Direktoratet for Byggkvalitet i Altinn.
        /// </summary>
        internal static string AltinnNotificationMessage {
            get {
                return ResourceManager.GetString("AltinnNotificationMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to DIBK-flexvarsling-1.
        /// </summary>
        internal static string AltinnNotificationTemplate {
            get {
                return ResourceManager.GetString("AltinnNotificationTemplate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to DIBK-nabo-1.
        /// </summary>
        internal static string AltinnNotificationTemplateNabo {
            get {
                return ResourceManager.GetString("AltinnNotificationTemplateNabo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Åpne svarskjema for erklæring av ansvarsrett.
        /// </summary>
        internal static string AnsvarsrettLinkText {
            get {
                return ResourceManager.GetString("AnsvarsrettLinkText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ikke angitt.
        /// </summary>
        internal static string DefaultNameAnsvarligSoeker {
            get {
                return ResourceManager.GetString("DefaultNameAnsvarligSoeker", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to noreply@noreply.no.
        /// </summary>
        internal static string DistributionFromEmail {
            get {
                return ResourceManager.GetString("DistributionFromEmail", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Nabovarsel.
        /// </summary>
        internal static string FormNameNabovarsel {
            get {
                return ResourceManager.GetString("FormNameNabovarsel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Åpne svarskjema for kontrollerklæring.
        /// </summary>
        internal static string KontrollerklaeringLinkText {
            get {
                return ResourceManager.GetString("KontrollerklaeringLinkText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to nabovarsel_kvittering.pdf.
        /// </summary>
        internal static string KvitteringNabovarselFilename {
            get {
                return ResourceManager.GetString("KvitteringNabovarselFilename", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Skjema er hentet fra Altinns tjenestearkiv for DiBK..
        /// </summary>
        internal static string LogMessageDownloadedForm {
            get {
                return ResourceManager.GetString("LogMessageDownloadedForm", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to application/pdf.
        /// </summary>
        internal static string MimeAppPdf {
            get {
                return ResourceManager.GetString("MimeAppPdf", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to application/xml.
        /// </summary>
        internal static string MimeAppXml {
            get {
                return ResourceManager.GetString("MimeAppXml", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Trykk her for å fylle ut svarskjemaet.
        /// </summary>
        internal static string NabovarselLinkText {
            get {
                return ResourceManager.GetString("NabovarselLinkText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to nabovarsel.pdf.
        /// </summary>
        internal static string NabovarselMainformFilename {
            get {
                return ResourceManager.GetString("NabovarselMainformFilename", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Svarskjema for uttalelse.
        /// </summary>
        internal static string NabovarselPlanLinkText {
            get {
                return ResourceManager.GetString("NabovarselPlanLinkText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ErklaeringOmAnsvarsrett.pdf.
        /// </summary>
        internal static string NotificationAnsvarsrettPdfFilename {
            get {
                return ResourceManager.GetString("NotificationAnsvarsrettPdfFilename", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Erklæring om ansvarsrett signert.
        /// </summary>
        internal static string NotificationAnsvarsrettPdfTitle {
            get {
                return ResourceManager.GetString("NotificationAnsvarsrettPdfTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ErklaeringOmAnsvarsrett.xml.
        /// </summary>
        internal static string NotificationAnsvarsrettXMLFilename {
            get {
                return ResourceManager.GetString("NotificationAnsvarsrettXMLFilename", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Maskinlesbar XML.
        /// </summary>
        internal static string NotificationAnsvarsrettXMLTitle {
            get {
                return ResourceManager.GetString("NotificationAnsvarsrettXMLTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Altinn melding feilet.
        /// </summary>
        internal static string NotificationExceptionSendSimpleToReportee {
            get {
                return ResourceManager.GetString("NotificationExceptionSendSimpleToReportee", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Kontrollerklaering.pdf.
        /// </summary>
        internal static string NotificationKontrollerklaeringPdfFilename {
            get {
                return ResourceManager.GetString("NotificationKontrollerklaeringPdfFilename", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Kontrollerklæring signert.
        /// </summary>
        internal static string NotificationKontrollerklaeringPdfTitle {
            get {
                return ResourceManager.GetString("NotificationKontrollerklaeringPdfTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Kontrollerklaering.xml.
        /// </summary>
        internal static string NotificationKontrollerklaeringXMLFilename {
            get {
                return ResourceManager.GetString("NotificationKontrollerklaeringXMLFilename", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Maskinlesbar XML.
        /// </summary>
        internal static string NotificationKontrollSamsvarXMLTitle {
            get {
                return ResourceManager.GetString("NotificationKontrollSamsvarXMLTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SvarPaaNabovarsel.pdf.
        /// </summary>
        internal static string NotificationNabovarselPdfFilename {
            get {
                return ResourceManager.GetString("NotificationNabovarselPdfFilename", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Nabovarsel svar signert.
        /// </summary>
        internal static string NotificationNabovarselPdfTitle {
            get {
                return ResourceManager.GetString("NotificationNabovarselPdfTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SvarPaaNabovarsel.xml.
        /// </summary>
        internal static string NotificationNabovarselXMLFilename {
            get {
                return ResourceManager.GetString("NotificationNabovarselXMLFilename", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Maskinlesbar XML.
        /// </summary>
        internal static string NotificationNabovarselXMLTitle {
            get {
                return ResourceManager.GetString("NotificationNabovarselXMLTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to OpphoerAvAnsvarsrett.pdf.
        /// </summary>
        internal static string NotificationOpphoerAnsvarsrettPdfFilename {
            get {
                return ResourceManager.GetString("NotificationOpphoerAnsvarsrettPdfFilename", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Opphør av ansvarsrett signert.
        /// </summary>
        internal static string NotificationOpphoerAnsvarsrettPdfTitle {
            get {
                return ResourceManager.GetString("NotificationOpphoerAnsvarsrettPdfTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to OpphoerAvAnsvarsrett.xml.
        /// </summary>
        internal static string NotificationOpphoerAnsvarsrettXMLFilename {
            get {
                return ResourceManager.GetString("NotificationOpphoerAnsvarsrettXMLFilename", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Maskinlesbar XML.
        /// </summary>
        internal static string NotificationOpphoerAnsvarsrettXMLTitle {
            get {
                return ResourceManager.GetString("NotificationOpphoerAnsvarsrettXMLTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Samsvarserklaering.pdf.
        /// </summary>
        internal static string NotificationSamsvarserklaeringPdfFilename {
            get {
                return ResourceManager.GetString("NotificationSamsvarserklaeringPdfFilename", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Samsvarserklæring signert.
        /// </summary>
        internal static string NotificationSamsvarserklaeringPdfTitle {
            get {
                return ResourceManager.GetString("NotificationSamsvarserklaeringPdfTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Samsvarserklaering.xml.
        /// </summary>
        internal static string NotificationSamsvarserklaeringXMLFilename {
            get {
                return ResourceManager.GetString("NotificationSamsvarserklaeringXMLFilename", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Maskinlesbar XML.
        /// </summary>
        internal static string NotificationSamsvarserklaeringXMLTitle {
            get {
                return ResourceManager.GetString("NotificationSamsvarserklaeringXMLTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to samtykketiltakshaver.pdf.
        /// </summary>
        internal static string NotificationSamtykkeTiltakshaverPdfFilename {
            get {
                return ResourceManager.GetString("NotificationSamtykkeTiltakshaverPdfFilename", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Samtykke tiltakshaver signert.
        /// </summary>
        internal static string NotificationSamtykkeTiltakshaverPdfTitle {
            get {
                return ResourceManager.GetString("NotificationSamtykkeTiltakshaverPdfTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to samtykketiltakshaver.xml.
        /// </summary>
        internal static string NotificationSamtykkeTiltakshaverXMLFilename {
            get {
                return ResourceManager.GetString("NotificationSamtykkeTiltakshaverXMLFilename", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Maskinlesbar xml.
        /// </summary>
        internal static string NotificationSamtykkeTiltakshaverXMLTitle {
            get {
                return ResourceManager.GetString("NotificationSamtykkeTiltakshaverXMLTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to varselbrev.pdf.
        /// </summary>
        internal static string PlanVarselMainformFilename {
            get {
                return ResourceManager.GetString("PlanVarselMainformFilename", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 168.
        /// </summary>
        internal static string ReNotificationPeriodInHours {
            get {
                return ResourceManager.GetString("ReNotificationPeriodInHours", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Åpne svarskjema for samsvarserklæring.
        /// </summary>
        internal static string SamsvarserklaeringLinkText {
            get {
                return ResourceManager.GetString("SamsvarserklaeringLinkText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Innsendingen med arkiv referanse {0} feilet under behandling i Fellestjenester bygg. Innsendingen blir ikke videre behandlet og må sendes på nytt. Følgende problemene ble funnet under skjema validering: .
        /// </summary>
        internal static string ShippingErrorBody {
            get {
                return ResourceManager.GetString("ShippingErrorBody", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to En feil oppstod under sending til Altinn distribusjon:.
        /// </summary>
        internal static string ShippingErrorDistribution {
            get {
                return ResourceManager.GetString("ShippingErrorDistribution", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to En feil oppstod ved lesing av skjema og uthenting av filer fra Altinn: .
        /// </summary>
        internal static string ShippingErrorDownloadAndDeserialize {
            get {
                return ResourceManager.GetString("ShippingErrorDownloadAndDeserialize", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to En feil oppstod under sending til Altinn meldingstjeneste:.
        /// </summary>
        internal static string ShippingErrorNotification {
            get {
                return ResourceManager.GetString("ShippingErrorNotification", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Feil oppstod ved validering av struktur på skjema:.
        /// </summary>
        internal static string ShippingErrorStructure {
            get {
                return ResourceManager.GetString("ShippingErrorStructure", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Melding er sendt via Fellestjenester Bygg fra Direktoratet for Byggkvalitet.
        /// </summary>
        internal static string ShippingErrorSummary {
            get {
                return ResourceManager.GetString("ShippingErrorSummary", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to En feil oppstod under sending til FIKS SvarUt:.
        /// </summary>
        internal static string ShippingErrorSvarUt {
            get {
                return ResourceManager.GetString("ShippingErrorSvarUt", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Innsending {0} feilet.  .
        /// </summary>
        internal static string ShippingErrorTitle {
            get {
                return ResourceManager.GetString("ShippingErrorTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Feil oppstod ved validering av data i skjema: .
        /// </summary>
        internal static string ShippingErrorValidation {
            get {
                return ResourceManager.GetString("ShippingErrorValidation", resourceCulture);
            }
        }
    }
}
