﻿using KommIT.FIKS.AdapterAltinnSvarUt.Services;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Altinn;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Azure;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.WS.Altinn;
using KommIT.FIKS.AdapterAltinnSvarUt.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;

namespace KommIT.FIKS.AdapterAltinnSvarUt.IntegrationTests.Common
{
    public class IntegrationTestUtils
    {
        public static ArchivedFormTaskDQBE GetItemByArchiveReference(IAltinnService whoIAm, BlobStorage blobStorage, string containerName, string archiveReference)
        {
            IFormMetadataService service = null;
            return GetItemByArchiveReference(whoIAm, blobStorage, containerName, archiveReference, service);
        }
        public static ArchivedFormTaskDQBE GetItemByArchiveReference(IAltinnService whoIAm, BlobStorage blobStorage, string containerName, string archiveReference, IFormMetadataService formMetadataService)
        {
            

            var xmlData = blobStorage.GetBlobContentAsString(containerName, BlobStorageMetadataTypeEnum.AltinnFormTaskQueueItemForm);
            string currentArchiveTimestamp = DateTime.Now.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fff");
            xmlData = Regex.Replace(xmlData,
                                    @"<ArchiveReference>.+</ArchiveReference>",
                                     "<ArchiveReference>" + archiveReference + "</ArchiveReference>");
            xmlData = Regex.Replace(xmlData,
                                    @"<ArchiveTimeStamp>.+</ArchiveTimeStamp>",
                                     "<ArchiveTimeStamp>" + currentArchiveTimestamp + "</ArchiveTimeStamp>");

            
            if (whoIAm is AltinnServiceMockVarselOppstartAvPlanarbeid)
            {
                string fristForInnspill14dagerFraNaa = DateTime.Now.AddDays(14).ToString("yyyy-MM-ddT00:00:00");
                xmlData = Regex.Replace(xmlData,
                                        @"&lt;ns6:fristForInnspill&gt;.+&lt;/ns6:fristForInnspill&gt;",
                                         "&lt;ns6:fristForInnspill&gt;" + fristForInnspill14dagerFraNaa + "&lt;/ns6:fristForInnspill&gt;");
            }

            // Finn hvoedinnsendingsnr fra nyeste DistributionType "Uttalelse til oppstart av reguleringsplanarbeid"
            // som har SignedArchiveReference = NULL
            if (whoIAm is AltinnServiceMockSvarPaaVarselOppstartAvPlanarbeid)
            {
                if (formMetadataService != null)
                {
                    string hovedInnsendingsNr = formMetadataService.GetMostRecentDistributionForsendelsesIdNotSigned();
                    xmlData = Regex.Replace(xmlData,
                                            @"&lt;hovedinnsendingsnummer&gt;.+&lt;/hovedinnsendingsnummer&gt;",
                                             "&lt;hovedinnsendingsnummer&gt;" + hovedInnsendingsNr + "&lt;/hovedinnsendingsnummer&gt;");

                    //Replace "berørt part" name
                    string newPlace = GetRandomPlaceName();
                    string newIndusty1 = GetRandomIndustryName();
                    string newIndusty2 = GetRandomIndustryName();
                    string newType = GetRandomOrgTypeName();
                    string newOrgName = $"{newPlace} {newIndusty1}- og {newIndusty2}{newType}";
                    var beroertPartIdNr = GetRandomBeroertPartIdNr();
                    
                    //Randomize berort part
                    xmlData = Regex.Replace(xmlData, @"KRANGLE VELFORENING", newOrgName);
                    xmlData = Regex.Replace(xmlData, @"910041126", beroertPartIdNr);
                }
            }

            var form = SerializeUtil.DeserializeFromString<ArchivedFormTaskDQBE>(xmlData);

            return form;
        }

        private static string GetRandomBeroertPartIdNr()
        {
            Thread.Sleep(1000);
            List<string> lst = new List<string>();
            lst.Add("910041126");
            lst.Add("910065645");
            lst.Add("910017489");
            lst.Add("910015966");

            var index = new Random().Next(lst.Count);

            return lst[index];
        }

        public static byte[] GetMainPdfForm(BlobStorage blobStorage, string containerName)
        {
            //Returning main form for Varsel om oppstart av planarbeid
            return blobStorage.GetBlobContentAsBytes(containerName, BlobStorageMetadataTypeEnum.MainForm);
        }

        private static string GetRandomIndustryName()
        {
            Thread.Sleep(1000);
            var list = new List<string> { "Adjunkt","Advokat","Advokatsekretær","Agronom","Aksjemegler","Aktivitør","Aktuar","Akupunktør","Akvakultur","Allmennlege","Aluminiumskonstruksjon","Aluminiumskonstruktør","Ambassadør","Ambulanse","Anestesi","Animatør","Anleggsgartner","Anleggsmaskinfører","Anleggsmaskinmekaniker","Antikvar","Apotek","Apotekteknikk","Arbeidsmedisin","Arealplanlegging","Arkeologi","Arkitekt","Arkitektur","Arkivar","Aromaterapi","Arrangementsplanlegging","Asfalt","Astronaut","Astronomi","Attføringskonsulent","Au pair","Audiografi","Audiopedagogikk","Automatikk","Automatisering","Avisbud","Avløser","Badevakt","Baker","Banemontering","Barista","Ungdomspsykiatri","Barnehage","Barnelege","Barnesykepleier","Barnevern","Barnevernspedagog","Bartender","Betong","Bibelarbeid","Misjonering","Menighetsarbeid","Bibliotekar","Bilklargjøring","Billakkering","Billedkunstner","Bilmekaniker","Bilselger","Bilskade","Bilskadereparasjon","Bioingeniør","Biokjemiker","Biolog","Biologi","Bioteknolog","Blikkenslager","Blomsterdekoratør","Blyglasshåndverker","Bokhandler","Bonde","Boreoperatør","Vedlikeholdsoperatør","Branningeniør","Brannkonstabel","Bryst- og endokrinkirurg","Brønnoperatør","Brønnoperatør kveilerør","Brønnoperatør sementering","Bunadtilvirker","Buntmaker","Bussjåfør","Butikk","Butikkslakter","Bygg og anlegg","Byggdrifter","Vaktmester","Byggingeniør","Båtbygger","Bøkker","Børsemaker", "Familierådgiving", "Farmasi", "Feiing", "Filigranssølvsmed", "Filologi", "Filosofi", "Finansanalyse", "Finmekanikk", "Fiske og fangst", "Fiske og havbruk", "Fiskehelse", "Akvamedisin", "Fiskehelsebiologi", "Fiskeoppdrett", "Fiske", "Flygeledelse", "Flyinstruksjon", "Flyktningkonsulent", "Flymotormekanikk", "Flystrukturmekanikk", "Forfatter", "Forgylling", "Forretningsutvikling", "Forsikringsrådgivning", "Forsking", "Foto", "Fotterapi", "Frisør", "Fylkesskogmester", "Fysiker", "Fysikk", "Fysioterapi", "Førerhundtrening", "Garnframstilling", "Gartner", "Gartnernæring", "Garveri", "Geofysikk", "Geografi", "Geologi", "Geriatri", "Gestaltterapeut", "Gipsmakeri", "Glass", "Glasshåndverking", "Grafisk design", "Gravferdskonsulent", "Gravering", "Gründer", "Guide", "Gullsmed", "Gynekolog" };
            int index = new Random().Next(list.Count);
            return list[index];
        }
        private static string GetRandomPlaceName()
        {
            var list = new List<string> { "Hjelset", "Kleive", "Hovdenakken", "Nesjestranda", "Torhaug", "Kristiansund", "Hoffland", "Ålesund", "Årset", "Myklebost", "Larsnes", "Haugsbygda", "Gjerde", "Kvalsund", "Fosnavåg", "Remøy", "Leinstrand", "Moltustranda", "Tjørvåg", "Leikong", "Dragsund", "Ulsteinvik", "Sundgot", "Hjørungavåg", "Brandal", "Hareid", "Volda", "Mork", "Ørsta", "Sjøholt", "Stranda", "Hellesylt", "Stordal", "Ikornnes", "Straumgjerde", "Sykkylven", "Skodje", "Valle", "Roald", "Leitebakk", "Giske", "Nordstrand", "Austnes", "Brattvåg", "Vatne", "Søvik", "Tomra", "Vestnes", "Fiksdal", "Isfjorden", "Åndalsnes", "Brønnsletten", "Voll", "Innfjorden", "Eidsvåg", "Rausand", "Midsund", "Steinshamn", "Aukra", "Hollingen", "Elnesvågen", "Bud", "Tornes", "Farstad", "Malme", "Sylte", "Eide", "Storbakken", "Solsletta", "Rensvik", "Tingvollvågen", "Grøa", "Hoelsand", "Sunndalsøra", "Skei-Surnadalsøra", "Glærem", "Aure", "Fiskåbygd", "Kårvåg", "Bremsnes", "Sveggen", "Batnfjordsøra", "Torvikbukt", "Sætre" };
            int index = new Random().Next(list.Count);
            return list[index];
        }

        private static string GetRandomOrgTypeName()
        {
            var list = new List<string> { "forening", "lag", "selskap", "laug", "hær", "fraksjon", "junta", "hus", "kortesje", "losje", "menighet", "samfunn", "føderasjon", "liga", "hurv" };
            int index = new Random().Next(list.Count);
            return list[index];
        }


    }
}