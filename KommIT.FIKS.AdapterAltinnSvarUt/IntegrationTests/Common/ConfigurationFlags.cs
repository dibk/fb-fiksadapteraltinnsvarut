﻿using System.Configuration;

namespace KommIT.FIKS.AdapterAltinnSvarUt.IntegrationTests.Common
{
    public class ConfigurationFlags
    {
        public static bool RunsInTestEnvironment()
        {
            var configVal = ConfigurationManager.AppSettings["RunInTestMode"];

            var retVal = false;

            if (bool.TryParse(configVal, out retVal))
                return retVal;
            else
                return false;
        }
    }
}