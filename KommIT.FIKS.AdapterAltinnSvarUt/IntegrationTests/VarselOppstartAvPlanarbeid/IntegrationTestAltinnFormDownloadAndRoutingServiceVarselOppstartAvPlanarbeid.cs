﻿using KommIT.FIKS.AdapterAltinnSvarUt.QueuePreparators;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.FtId;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Notification;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Prefill;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.SearchEngine;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Altinn
{
    public class IntegrationTestAltinnFormDownloadAndRoutingServiceVarselOppstartAvPlanarbeid : AltinnFormDownloadAndRoutingService
    {
        public IntegrationTestAltinnFormDownloadAndRoutingServiceVarselOppstartAvPlanarbeid(IAltinnService altinnService, IAltinnFormDeserializer formDeserializer, IFormShippingService formShippingsService, ILogEntryService logEntryService, IFormMetadataService formMetadataService, FormDistributionServiceV2 formDistributionService, INotificationServiceResolver notificationServiceResolver, SendCorrespondenceHelper correspondenceHelper, IApplicationLogService applicationLogService, ExportToSubmittalQueueService exportToSubmittalQueueService, ISearchEngineIndexer searchEngine, IFtIdService ftIdService)
            : base(altinnService, formDeserializer, formShippingsService, logEntryService, formMetadataService, formDistributionService, notificationServiceResolver, correspondenceHelper, applicationLogService, exportToSubmittalQueueService, searchEngine, ftIdService)
        { }
    }
}