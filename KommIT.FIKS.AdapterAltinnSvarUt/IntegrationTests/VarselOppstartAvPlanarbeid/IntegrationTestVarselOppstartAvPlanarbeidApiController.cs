﻿using KommIT.FIKS.AdapterAltinnSvarUt.IntegrationTests.Common;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Altinn;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.WS.Altinn;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Controllers
{
    [ApiExplorerSettings(IgnoreApi = true)]
    public class IntegrationTestVarselOppstartAvPlanarbeidApiController : ApiController
    {
        private readonly IntegrationTestAltinnFormDownloadAndRoutingServiceVarselOppstartAvPlanarbeid _testAltinnFormDownloadAndRoutingService;

        public IntegrationTestVarselOppstartAvPlanarbeidApiController(IntegrationTestAltinnFormDownloadAndRoutingServiceVarselOppstartAvPlanarbeid testAltinnFormDownloadAndRoutingService)
        {
            _testAltinnFormDownloadAndRoutingService = testAltinnFormDownloadAndRoutingService;
        }

        [Route("api/integrationtest/varseloppstartavplanarbeid")]
        [HttpGet]
        public HttpResponseMessage Distribute()
        {
            if (!ConfigurationFlags.RunsInTestEnvironment())
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "These are not the APIs you are looking for");
            }

            try
            {
                DateTime baseDate = new DateTime(1970, 1, 1);
                string ticksSince1970 = (DateTime.Now - baseDate).Ticks.ToString();
                string archiveReference = "AR" + ticksSince1970.Substring(ticksSince1970.Length - 8, 8);
                var integrationtestForm = new DownloadFormForProcessing()
                {
                    Queued = false,
                    FormProcessingType = FormProcessingType.Distribution,
                    ArchiveReference = archiveReference,
                    ArchivedDate = DateTime.Now,
                    ServiceCode = "5418",
                    ServiceEditionCode = 1,
                    ReporteeType = (ReporteeType)DownloadQueueReporteeType.Organisation,
                    DoProcess = true,
                    QueueFormToSubmittalQueue = true
                };
                var formsForProcessing = new List<DownloadFormForProcessing>();
                formsForProcessing.Add(integrationtestForm);
                _testAltinnFormDownloadAndRoutingService.QueueFormsReadyForProcessing(formsForProcessing);
                
                return Request.CreateResponse(HttpStatusCode.OK, $"Distribution of {archiveReference} initiated at {DateTime.Now.ToLocalTime()}");
            }
            catch (ArgumentException aex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, aex.Message);
            }
        }
    }
}

