﻿using KommIT.FIKS.AdapterAltinnSvarUt.IntegrationTests.Common;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Azure;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.WS.Altinn;
using System;
using System.Collections.Generic;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Altinn
{
    public class AltinnServiceMockSvarPaaVarselOppstartAvPlanarbeid : IAltinnService
    {
        private readonly BlobStorage _blobStorage;
        private readonly IFormMetadataService _formMetadataService;
        private readonly string CONTAINER_NAME_SVAR_PAA_VARSEL_OPPSTART_AV_PLANARBEID = "integration-test-svar-paa-varsel-oppstart-av-planarbeid";
        public AltinnServiceMockSvarPaaVarselOppstartAvPlanarbeid(BlobStorage blobStorage, IFormMetadataService formMetadataService)
        {
            _blobStorage = blobStorage;
            _formMetadataService = formMetadataService;
        }
        public void EmptyQueue(string csvServiceCodes)
        {
            throw new NotImplementedException();
        }

        public List<DownloadQueueItemBE> GetAllAltinnDlqPendingForms()
        {
            throw new NotImplementedException();
        }

        public List<string> GetFormsReadyFromAltinn(string csvServiceCodes)
        {
            throw new NotImplementedException();
        }

        public ArchivedFormTaskDQBE GetItemByArchiveReference(string archiveReference)
        {
            return IntegrationTestUtils.GetItemByArchiveReference(this, _blobStorage, CONTAINER_NAME_SVAR_PAA_VARSEL_OPPSTART_AV_PLANARBEID, archiveReference, _formMetadataService);
        }

        public byte[] GetPdfForm(string ArchiveReference, string DataFormatId, string DataFormatVersion)
        {
            //Returning main form for Varsel om oppstart av planarbeid
            return IntegrationTestUtils.GetMainPdfForm(_blobStorage, CONTAINER_NAME_SVAR_PAA_VARSEL_OPPSTART_AV_PLANARBEID);
        }

        //public void GetPdfForms(FormData formData)
        //{
        //    throw new NotImplementedException();
        //}

        public bool PurgeItem(string archiveReference)
        {
            //Nothing to be purged
            return true;
        }
    }
}