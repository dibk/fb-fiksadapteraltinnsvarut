﻿using KommIT.FIKS.AdapterAltinnSvarUt.IntegrationTests.Common;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.Azure;
using KommIT.FIKS.AdapterAltinnSvarUt.Services.WS.Altinn;
using System;
using System.Collections.Generic;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Services.Altinn
{
    public class AltinnServiceMockATILSoknadOmSamtykkeV2 : IAltinnService
    {
        private readonly BlobStorage _blobStorage;
        private readonly string CONTAINER_NAME_VARSEL_OPPSTART_AV_PLANARBEID = "integration-test-atil-samtykke-v2";
        public AltinnServiceMockATILSoknadOmSamtykkeV2(BlobStorage blobStorage)
        {
            _blobStorage = blobStorage;
        }
        public void EmptyQueue(string csvServiceCodes)
        {
            throw new NotImplementedException();
        }

        public List<DownloadQueueItemBE> GetAllAltinnDlqPendingForms()
        {
            throw new NotImplementedException();
        }

        public List<string> GetFormsReadyFromAltinn(string csvServiceCodes)
        {
            throw new NotImplementedException();
        }

        public ArchivedFormTaskDQBE GetItemByArchiveReference(string archiveReference)
        {
            return IntegrationTestUtils.GetItemByArchiveReference(this, _blobStorage, CONTAINER_NAME_VARSEL_OPPSTART_AV_PLANARBEID, archiveReference);
        }

        public byte[] GetPdfForm(string ArchiveReference, string DataFormatId, string DataFormatVersion)
        {
            //Returning main form for Varsel om oppstart av planarbeid
            return IntegrationTestUtils.GetMainPdfForm(_blobStorage, CONTAINER_NAME_VARSEL_OPPSTART_AV_PLANARBEID);
        }


        public bool PurgeItem(string archiveReference)
        {
            //Nothing to be purged
            return true;
        }
    }
}