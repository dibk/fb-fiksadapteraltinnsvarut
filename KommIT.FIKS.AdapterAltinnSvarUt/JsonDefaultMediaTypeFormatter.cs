﻿using Newtonsoft.Json;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;

namespace KommIT.FIKS.AdapterAltinnSvarUt
{
    public class JsonDefaultMediaTypeFormatter : JsonMediaTypeFormatter
    {
        public JsonDefaultMediaTypeFormatter()
        {
            SupportedMediaTypes.Add(new MediaTypeHeaderValue("application/json"));
            SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/json"));
            SerializerSettings.Re‌ferenceLoopHandling = ReferenceLoopHandling.Ignore;
            SerializerSettings = new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore };
        }
    }
}
