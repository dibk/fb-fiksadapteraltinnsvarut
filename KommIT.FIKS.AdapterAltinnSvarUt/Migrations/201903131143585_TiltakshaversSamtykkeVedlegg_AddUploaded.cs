namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TiltakshaversSamtykkeVedlegg_AddUploaded : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TiltakshaversSamtykkeVedleggs", "Uploaded", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.TiltakshaversSamtykkeVedleggs", "Uploaded");
        }
    }
}
