namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateFormMetadata : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.FormMetadata", "Status_State", "dbo.Status");
            DropIndex("dbo.FormMetadata", new[] { "Status_State" });
            DropPrimaryKey("dbo.Status");
            CreateTable(
                "dbo.EventLogs",
                c => new
                    {
                        Value = c.String(nullable: false, maxLength: 128),
                        Description = c.String(),
                        LastUpdated = c.DateTime(nullable: false),
                        Skjema_ArchiveReference = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Value)
                .ForeignKey("dbo.FormMetadata", t => t.Skjema_ArchiveReference)
                .Index(t => t.Skjema_ArchiveReference);
            
            AddColumn("dbo.FormMetadata", "ReferanseIdSendt", c => c.String());
            AddColumn("dbo.FormMetadata", "ReferanseIdSignert", c => c.String());
            AddColumn("dbo.FormMetadata", "AnsvarligSoeker", c => c.String());
            AddColumn("dbo.Status", "Value", c => c.String(nullable: false, maxLength: 128));
            AddPrimaryKey("dbo.Status", "Value");
            DropColumn("dbo.FormMetadata", "Status_State");
            DropColumn("dbo.Status", "State");
            DropColumn("dbo.Status", "Description");
            DropColumn("dbo.Status", "Timestamp");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Status", "Timestamp", c => c.DateTime(nullable: false));
            AddColumn("dbo.Status", "Description", c => c.String());
            AddColumn("dbo.Status", "State", c => c.String(nullable: false, maxLength: 128));
            AddColumn("dbo.FormMetadata", "Status_State", c => c.String(maxLength: 128));
            DropForeignKey("dbo.EventLogs", "Skjema_ArchiveReference", "dbo.FormMetadata");
            DropIndex("dbo.EventLogs", new[] { "Skjema_ArchiveReference" });
            DropPrimaryKey("dbo.Status");
            DropColumn("dbo.Status", "Value");
            DropColumn("dbo.FormMetadata", "AnsvarligSoeker");
            DropColumn("dbo.FormMetadata", "ReferanseIdSignert");
            DropColumn("dbo.FormMetadata", "ReferanseIdSendt");
            DropTable("dbo.EventLogs");
            AddPrimaryKey("dbo.Status", "State");
            CreateIndex("dbo.FormMetadata", "Status_State");
            AddForeignKey("dbo.FormMetadata", "Status_State", "dbo.Status", "State");
        }
    }
}
