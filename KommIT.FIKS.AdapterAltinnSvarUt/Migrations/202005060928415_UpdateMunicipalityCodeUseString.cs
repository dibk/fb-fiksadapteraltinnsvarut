namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateMunicipalityCodeUseString : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.DeviationLetters", "MunicipalityCode", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.DeviationLetters", "MunicipalityCode", c => c.Int(nullable: false));
        }
    }
}
