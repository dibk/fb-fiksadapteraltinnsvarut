namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addDistributionRecieptLink : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FormMetadata", "DistributionRecieptLink", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.FormMetadata", "DistributionRecieptLink");
        }
    }
}
