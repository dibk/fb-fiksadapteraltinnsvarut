namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class update_distributionform : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DistributionForms", "SubmitAndInstantiatePrefilled", c => c.DateTime());
            AddColumn("dbo.DistributionForms", "Signed", c => c.DateTime());
            AddColumn("dbo.DistributionForms", "DistributionStatus", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.DistributionForms", "DistributionStatus");
            DropColumn("dbo.DistributionForms", "Signed");
            DropColumn("dbo.DistributionForms", "SubmitAndInstantiatePrefilled");
        }
    }
}
