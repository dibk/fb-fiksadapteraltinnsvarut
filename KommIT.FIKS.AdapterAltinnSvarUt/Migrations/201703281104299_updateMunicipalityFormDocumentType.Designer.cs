// <auto-generated />
namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class updateMunicipalityFormDocumentType : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(updateMunicipalityFormDocumentType));
        
        string IMigrationMetadata.Id
        {
            get { return "201703281104299_updateMunicipalityFormDocumentType"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
