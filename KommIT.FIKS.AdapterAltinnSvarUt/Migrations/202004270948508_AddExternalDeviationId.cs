namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddExternalDeviationId : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Deviations", "ExternalDeviationId", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Deviations", "ExternalDeviationId");
        }
    }
}
