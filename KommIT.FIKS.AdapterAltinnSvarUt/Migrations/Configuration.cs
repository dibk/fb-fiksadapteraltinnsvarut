using KommIT.FIKS.AdapterAltinnSvarUt.Models;
using KommIT.FIKS.AdapterAltinnSvarUt.Services;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using CsvHelper;
    using CsvHelper.Configuration;
    using KommIT.FIKS.AdapterAltinnSvarUt.Models.UserAccess;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Configuration;
    using System.Data.Entity.Migrations;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Text;

    internal sealed class Configuration : DbMigrationsConfiguration<ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "KommIT.FIKS.AdapterAltinnSvarUt.Models.ApplicationDbContext";
            this.CommandTimeout = 60 * 20;
        }

        protected override void Seed(ApplicationDbContext context)
        {
            SeedDefaultUserRoles();

            SeedDefaultAdministrator(context);

            SeedForms(context);
            
            if (ConfigurationManager.AppSettings["RunInTestMode"] == "true")
            {
                SeedTestMunicipalities(context);
            }
        }

        private static void SeedDefaultUserRoles()
        {
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>());
            if (!roleManager.RoleExists(ApplicationRole.Administrator))
            {
                roleManager.Create(new IdentityRole(ApplicationRole.Administrator));
            }
            if (!roleManager.RoleExists(ApplicationRole.User))
            {
                roleManager.Create(new IdentityRole(ApplicationRole.User));
            }
            if (!roleManager.RoleExists(ApplicationRole.SystemIntegrator))
            {
                roleManager.Create(new IdentityRole(ApplicationRole.SystemIntegrator));
            }
        }

        private static void SeedDefaultAdministrator(ApplicationDbContext context)
        {
            string adminEmail = "support@arkitektum.no";

            if (!context.Users.Any(u => u.UserName == adminEmail))
            {
                var userStore = new UserStore<ApplicationUser>(context);
                var userManager = new UserManager<ApplicationUser>(userStore);

                var user = new ApplicationUser { UserName = adminEmail, Email = adminEmail, EmailConfirmed = true };
                userManager.Create(user, "24Pils");
                userManager.AddToRole(user.Id, ApplicationRole.Administrator);
            }
        }

        private static void SeedForms(ApplicationDbContext context)
        {
            if (context.Forms.Count() == 0)
            {
                context.Forms.AddOrUpdate(x => x.Id,
                new Form(1, "Nabovarsel", PropertyIdentifierExtractor.FormatDefault),
                new Form(2, "S�knad om tillatelse til tiltak uten ansvarsrett", PropertyIdentifierExtractor.FormatDefault),
                new Form(3, "S�knad om tillatelse til tiltak", PropertyIdentifierExtractor.FormatDefaultRammesoeknad),
                new Form(4, "S�knad om igangsettingstillatelse", PropertyIdentifierExtractor.FormatDefault),
                new Form(5, "S�knad om midlertidig brukstillatelse", PropertyIdentifierExtractor.FormatDefault),
                new Form(6, "Erkl�ring om ansvarsrett", PropertyIdentifierExtractor.FormatDefault),
                new Form(7, "Gjennomf�ringsplan", PropertyIdentifierExtractor.FormatDefault),
                new Form(8, "S�knad om ferdigattest", PropertyIdentifierExtractor.FormatDefault),
                new Form(9, "S�knad om tillatelse i ett trinn", PropertyIdentifierExtractor.FormatDefaultRammesoeknad),
                new Form(10, "Samsvarserkl�ring", PropertyIdentifierExtractor.FormatDefault),
                new Form(11, "Svar p� nabovarsel", PropertyIdentifierExtractor.FormatDefault),
                new Form(12, "S�knad om personlig ansvarsrett", PropertyIdentifierExtractor.FormatDefault),
                new Form(13, "Distribusjonstjeneste for erkl�ring om ansvarsrett", PropertyIdentifierExtractor.FormatDefault),
                new Form(14, "Gjenpart nabovarsel", PropertyIdentifierExtractor.FormatDefault),
                new Form(15, "Avfallsplan", PropertyIdentifierExtractor.FormatDefault),
                new Form(16, "Opph�r av ansvarsrett", PropertyIdentifierExtractor.FormatDefault),
                new Form(17, "Gjennomf�ringsplanV2", PropertyIdentifierExtractor.FormatDefault)
                );
            }

        }


        private static void SeedTestMunicipalities(ApplicationDbContext context)
        {
            context.Municipalities.AddOrUpdate(x => x.Code,
                new Municipality("9999", "910297937", "Testkonto TT02 (Hafslo Revisjon)"), // test account for Hafslo Revisjon company in Altinn TT02
                new Municipality("9998", "910065246", "MURE KOMMUNE (Tieto)"),
                new Municipality("9997", "910065254", "SNEKKRE KOMMUNE (Evry)"),
                new Municipality("9996", "910065289", "LIME KOMMUNE (Acos)"),
                new Municipality("9995", "910065297", "KLIPPE KOMMUNE"),
                new Municipality("9994", "910065300", "BANKE KOMMUNE")
            );
        }

        private static void SeedStatus(ApplicationDbContext context)
        {
            context.Status.AddOrUpdate(
                new Status("Feilet"),
                new Status("Sendt"),
                new Status("Signert"),
                new Status("Kvittering sendt")
            );
        }

        private static CsvReader GetReader(StreamReader streamReader)
        {
            var config = new CsvConfiguration(CultureInfo.GetCultureInfo("nb"))
            {
                HeaderValidated = null,
                Delimiter = ",",
                HasHeaderRecord = true,
                MissingFieldFound = null,
            };

            var csvReader = new CsvReader(streamReader, config);
            

            return csvReader;
        }
        private static void SeedMunicipalitiesInitial(ApplicationDbContext context)
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            string resourceName = "KommIT.FIKS.AdapterAltinnSvarUt.Models.SeedData.0-kommuner_eksisterende_prod.csv";

            using (Stream stream = assembly.GetManifestResourceStream(resourceName))
            using (StreamReader reader = new StreamReader(stream, Encoding.UTF8))
            using (CsvReader csvReader = GetReader(reader))
            {
                var municipalities = csvReader.GetRecords<Municipality>().ToArray();

                //Compare with existing
                foreach (var item in municipalities)
                {
                    var byCode = context.Municipalities.Where(m => m.Code.Equals(item.Code)).FirstOrDefault();
                    if (byCode == null)
                    {
                        item.NewMunicipalityCode = null;

                        context.Municipalities.Add(item);
                        context.SaveChanges();
                    }
                }
            }
        }

        private static void SeedMunicipalities(ApplicationDbContext context)
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            string resourceName = "KommIT.FIKS.AdapterAltinnSvarUt.Models.SeedData.1-kommuner_kun_knr_endring.csv";

            using (Stream stream = assembly.GetManifestResourceStream(resourceName))
            using (StreamReader reader = new StreamReader(stream, Encoding.UTF8))
            using (CsvReader csvReader = GetReader(reader))
            {
                var municipalities = csvReader.GetRecords<Municipality>().ToArray();

                //Compare with existing
                foreach (var item in municipalities)
                {
                    var byCode = context.Municipalities.Where(m => m.Code.Equals(item.Code)).FirstOrDefault();
                    if (byCode == null)
                    {
                        item.Name = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(item.Name.ToLower());
                        item.NewMunicipalityCode = null;

                        context.Municipalities.Add(item);
                        context.SaveChanges();
                    }
                }
            }


        }

        private static void SeedMunicipalities_Reform2020_UpdateExisting(ApplicationDbContext context)
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            string resourceName = "KommIT.FIKS.AdapterAltinnSvarUt.Models.SeedData.2-kommuner_i_relasjon.csv";

            using (Stream stream = assembly.GetManifestResourceStream(resourceName))
            using (StreamReader reader = new StreamReader(stream, Encoding.UTF8))
            using (CsvReader csvReader = GetReader(reader))
            {
                var municipalities = csvReader.GetRecords<Municipality>().ToArray();

                //Compare with existing
                foreach (var item in municipalities)
                {
                    var existingMunicipiality = context.Municipalities.Where(m => m.Code.Equals(item.Code)).FirstOrDefault();
                    if (existingMunicipiality == null)
                    {
                        existingMunicipiality = context.Municipalities.Where(m => m.Name.Trim().Equals(item.Name.Trim(), StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();
                    }

                    //Updates with the code of the related municipality                            
                    if (existingMunicipiality != null)
                    {
                        existingMunicipiality.NewMunicipalityCode = item.NewMunicipalityCode;
                        existingMunicipiality.ValidTo = item.ValidTo?.AddHours(23).AddMinutes(59).AddSeconds(59);
                        existingMunicipiality.ValidFrom = null;

                        context.SaveChanges();
                    }
                }
            }
        }

        private static void SeedMunicipalitiesReform2020_Add_new(ApplicationDbContext context)
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            string resourceName = "KommIT.FIKS.AdapterAltinnSvarUt.Models.SeedData.3-kommuner_nye.csv";

            using (Stream stream = assembly.GetManifestResourceStream(resourceName))
            using (StreamReader reader = new StreamReader(stream, Encoding.UTF8))
            using (CsvReader csvReader = GetReader(reader))
            {
                var municipalities = csvReader.GetRecords<Municipality>().ToArray();

                //Compare with existing
                foreach (var item in municipalities)
                {
                    //Checks existence
                    var existingMunicipiality = context.Municipalities.Where(m => m.Code.Equals(item.Code)).FirstOrDefault();
                    if (existingMunicipiality == null)
                    {
                        //Adds the new municipality
                        item.Name = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(item.Name.ToLower());
                        item.NewMunicipalityCode = null;

                        context.Municipalities.AddOrUpdate(item);
                        context.SaveChanges();
                    }
                }
            }
        }

        //private static void SeedMunicipalities(ApplicationDbContext context)
        //{
        //    if (context.Municipalities.Count() == 0) //Hvis tom s� skal initiell opprettelse skje
        //    {
        //        context.Municipalities.AddOrUpdate(x => x.Code,
        //        new Municipality("0101", "959159092", "Halden"),
        //        new Municipality("0104", "944383476", "Moss"),
        //        new Municipality("0105", "938801363", "Sarpsborg"),
        //        new Municipality("0106", "940039541", "Fredrikstad"),
        //        new Municipality("0111", "964947082", "Hvaler"),
        //        new Municipality("0118", "940875560", "Aremark"),
        //        new Municipality("0119", "964944334", "Marker"),
        //        new Municipality("0121", "964947171", "R�mskog"),
        //        new Municipality("0122", "964947260", "Tr�gstad"),
        //        new Municipality("0123", "864947352", "Spydeberg"),
        //        new Municipality("0124", "840894312", "Askim"),
        //        new Municipality("0125", "944345035", "Eidsberg"),
        //        new Municipality("0127", "941962726", "Skiptvet"),
        //        new Municipality("0128", "945372281", "Rakkestad"),
        //        new Municipality("0135", "940802652", "R�de"),
        //        new Municipality("0136", "959272492", "Rygge"),
        //        new Municipality("0137", "959272581", "V�ler i �stfold"),
        //        new Municipality("0138", "964947449", "Hob�l"),
        //        new Municipality("0211", "943485437", "Vestby"),
        //        new Municipality("0213", "960507878", "Ski"),
        //        new Municipality("0214", "964948798", "�s"),
        //        new Municipality("0215", "963999089", "Frogn"),
        //        new Municipality("0216", "944383565", "Nesodden"),
        //        new Municipality("0217", "944384081", "Oppeg�rd"),
        //        new Municipality("0219", "935478715", "B�rum"),
        //        new Municipality("0220", "944382038", "Asker"),
        //        new Municipality("0221", "948164256", "Aurskog-H�land"),
        //        new Municipality("0226", "942645295", "S�rum"),
        //        new Municipality("0227", "964949115", "Fet"),
        //        new Municipality("0228", "952540556", "R�lingen"),
        //        new Municipality("0229", "964949581", "Enebakk"),
        //        new Municipality("0230", "842566142", "L�renskog"),
        //        new Municipality("0231", "938275130", "Skedsmo"),
        //        new Municipality("0233", "971643870", "Nittedal"),
        //        new Municipality("0234", "864949762", "Gjerdrum"),
        //        new Municipality("0235", "933649768", "Ullensaker"),
        //        new Municipality("0236", "938679088", "Nes i Akershus"),
        //        new Municipality("0237", "964950113", "Eidsvoll"),
        //        new Municipality("0238", "964950202", "Nannestad"),
        //        new Municipality("0239", "939780777", "Hurdal"),
        //        new Municipality("0301", "971040823", "Oslo"),
        //        new Municipality("0402", "944117784", "Kongsvinger"),
        //        new Municipality("0403", "970540008", "Hamar"),
        //        new Municipality("0412", "864950582", "Ringsaker"),
        //        new Municipality("0415", "964950679", "L�ten"),
        //        new Municipality("0417", "970169717", "Stange"),
        //        new Municipality("0418", "964950768", "Nord-Odal"),
        //        new Municipality("0419", "964947716", "S�r-Odal"),
        //        new Municipality("0420", "964948054", "Eidskog"),
        //        new Municipality("0423", "964948143", "Grue"),
        //        new Municipality("0425", "964948232", "�snes"),
        //        new Municipality("0426", "871034222", "V�ler i Hedmark"),
        //        new Municipality("0427", "952857991", "Elverum"),
        //        new Municipality("0428", "864948502", "Trysil"),
        //        new Municipality("0429", "940152496", "�mot"),
        //        new Municipality("0430", "964948887", "Stor-Elvdal"),
        //        new Municipality("0432", "940028515", "Rendalen"),
        //        new Municipality("0434", "964948976", "Engerdal"),
        //        new Municipality("0436", "940192404", "Tolga"),
        //        new Municipality("0437", "940837685", "Tynset"),
        //        new Municipality("0438", "939984194", "Alvdal"),
        //        new Municipality("0439", "939885684", "Folldal"),
        //        new Municipality("0441", "943464723", "Os i Hedmark"),
        //        new Municipality("0501", "945578564", "Lillehammer"),
        //        new Municipality("0502", "940155223", "Gj�vik"),
        //        new Municipality("0511", "939849831", "Dovre"),
        //        new Municipality("0512", "964949204", "Lesja"),
        //        new Municipality("0513", "961381096", "Skj�k"),
        //        new Municipality("0514", "959377677", "Lom"),
        //        new Municipality("0515", "939607706", "V�g�"),
        //        new Municipality("0516", "839893132", "Nord-Fron"),
        //        new Municipality("0517", "939617671", "Sel"),
        //        new Municipality("0519", "941827195", "S�r-Fron"),
        //        new Municipality("0520", "939864970", "Ringebu"),
        //        new Municipality("0521", "961381185", "�yer"),
        //        new Municipality("0522", "961381274", "Gausdal"),
        //        new Municipality("0528", "964949859", "�stre Toten"),
        //        new Municipality("0529", "971028300", "Vestre Toten"),
        //        new Municipality("0532", "961381363", "Jevnaker"),
        //        new Municipality("0533", "961381452", "Lunner"),
        //        new Municipality("0534", "961381541", "Gran"),
        //        new Municipality("0536", "961381630", "S�ndre Land"),
        //        new Municipality("0538", "861381722", "Nordre Land"),
        //        new Municipality("0540", "961381819", "S�r-Aurdal"),
        //        new Municipality("0541", "933038173", "Etnedal"),
        //        new Municipality("0542", "961381908", "Nord-Aurdal"),
        //        new Municipality("0543", "961382157", "Vestre Slidre"),
        //        new Municipality("0544", "961382068", "�ystre Slidre"),
        //        new Municipality("0545", "961382246", "Vang"),
        //        new Municipality("0602", "939214895", "Drammen"),
        //        new Municipality("0604", "942402465", "Kongsberg"),
        //        new Municipality("0605", "940100925", "Ringerike"),
        //        new Municipality("0612", "960010833", "Hole"),
        //        new Municipality("0615", "964951462", "Fl�"),
        //        new Municipality("0616", "964951640", "Nes i Buskerud"),
        //        new Municipality("0617", "964952612", "Gol"),
        //        new Municipality("0618", "964952701", "Hemsedal"),
        //        new Municipality("0619", "864952992", "�l"),
        //        new Municipality("0620", "944889116", "Hol"),
        //        new Municipality("0621", "964962766", "Sigdal"),
        //        new Municipality("0622", "964962855", "Kr�dsherad"),
        //        new Municipality("0623", "970491589", "Modum"),
        //        new Municipality("0624", "954597482", "�vre Eiker"),
        //        new Municipality("0625", "841088832", "Nedre Eiker"),
        //        new Municipality("0626", "857566122", "Lier"),
        //        new Municipality("0627", "938709866", "R�yken"),
        //        new Municipality("0628", "964963193", "Hurum"),
        //        new Municipality("0631", "940898862", "Flesberg"),
        //        new Municipality("0632", "964963282", "Rollag"),
        //        new Municipality("0633", "964950946", "Nore og Uvdal"),
        //        new Municipality("0701", "964951284", "Horten"),
        //        new Municipality("0702", "838500382", "Holmestrand"),
        //        new Municipality("0704", "950611839", "T�nsberg"),
        //        new Municipality("0706", "960572602", "Sandefjord"),
        //        new Municipality("0709", "948930560", "Larvik"),
        //        new Municipality("0711", "939516107", "Svelvik"),
        //        new Municipality("0713", "938971471", "Sande i Vestfold"),
        //        new Municipality("0714", "864951732", "Hof"),
        //        new Municipality("0716", "983885497", "Re"),
        //        new Municipality("0719", "964952078", "Andebu"),
        //        new Municipality("0720", "964952167", "Stokke"),
        //        new Municipality("0722", "964952256", "N�tter�y"),
        //        new Municipality("0723", "964952345", "Tj�me"),
        //        new Municipality("0728", "964952434", "Lardal"),
        //        new Municipality("0805", "939991034", "Porsgrunn"),
        //        new Municipality("0806", "938759839", "Skien"),
        //        new Municipality("0807", "938583986", "Notodden"),
        //        new Municipality("0811", "864953042", "Siljan"),
        //        new Municipality("0814", "940244145", "Bamble"),
        //        new Municipality("0815", "963946902", "Krager�"),
        //        new Municipality("0817", "933277461", "Drangedal"),
        //        new Municipality("0819", "964963371", "Nome"),
        //        new Municipality("0821", "962276172", "B� i Telemark"),
        //        new Municipality("0822", "964963460", "Sauherad"),
        //        new Municipality("0826", "864963552", "Tinn"),
        //        new Municipality("0827", "964963649", "Hjartdal"),
        //        new Municipality("0828", "964963738", "Seljord"),
        //        new Municipality("0829", "964963827", "Kviteseid"),
        //        new Municipality("0830", "964964343", "Nissedal"),
        //        new Municipality("0831", "939772766", "Fyresdal"),
        //        new Municipality("0833", "964964521", "Tokke"),
        //        new Municipality("0834", "964964610", "Vinje"),
        //        new Municipality("0901", "964977402", "Ris�r"),
        //        new Municipality("0904", "864964702", "Grimstad"),
        //        new Municipality("0906", "940493021", "Arendal"),
        //        new Municipality("0911", "964964998", "Gjerstad"),
        //        new Municipality("0912", "964965048", "Veg�rshei"),
        //        new Municipality("0914", "964965781", "Tvedestrand"),
        //        new Municipality("0919", "946439045", "Froland"),
        //        new Municipality("0926", "964965404", "Lillesand"),
        //        new Municipality("0928", "964965870", "Birkenes"),
        //        new Municipality("0929", "864965962", "�mli"),
        //        new Municipality("0935", "864966012", "Iveland"),
        //        new Municipality("0937", "964966109", "Evje og Hornnes"),
        //        new Municipality("0938", "964966397", "Bygland"),
        //        new Municipality("0940", "964966575", "Valle"),
        //        new Municipality("0941", "958814968", "Bykle"),
        //        new Municipality("1001", "963296746", "Kristiansand"),
        //        new Municipality("1002", "964968519", "Mandal"),
        //        new Municipality("1003", "964083266", "Farsund"),
        //        new Municipality("1004", "964967369", "Flekkefjord"),
        //        new Municipality("1014", "936846777", "Vennesla"),
        //        new Municipality("1017", "938091900", "Songdalen"),
        //        new Municipality("1018", "964967091", "S�gne"),
        //        new Municipality("1021", "964966931", "Marnardal"),
        //        new Municipality("1026", "964966842", "�seral"),
        //        new Municipality("1027", "964966753", "Audnedal"),
        //        new Municipality("1029", "964966664", "Lindesnes"),
        //        new Municipality("1032", "946485764", "Lyngdal"),
        //        new Municipality("1034", "964963916", "H�gebostad"),
        //        new Municipality("1037", "964964076", "Kvinesdal"),
        //        new Municipality("1046", "964964165", "Sirdal"),
        //        new Municipality("1101", "944496394", "Eigersund"),
        //        new Municipality("1102", "964965137", "Sandnes"),
        //        new Municipality("1103", "964965226", "Stavanger"),
        //        new Municipality("1106", "944073787", "Haugesund"),
        //        new Municipality("1111", "964965692", "Sokndal"),
        //        new Municipality("1112", "964966486", "Lund"),
        //        new Municipality("1114", "970490361", "Bjerkreim"),
        //        new Municipality("1119", "964969590", "H�"),
        //        new Municipality("1120", "864969682", "Klepp"),
        //        new Municipality("1121", "859223672", "Time"),
        //        new Municipality("1122", "964978573", "Gjesdal"),
        //        new Municipality("1124", "948243113", "Sola"),
        //        new Municipality("1127", "934945514", "Randaberg"),
        //        new Municipality("1129", "939347801", "Forsand"),
        //        new Municipality("1130", "964978751", "Strand"),
        //        new Municipality("1133", "864979092", "Hjelmeland"),
        //        new Municipality("1134", "964979189", "Suldal"),
        //        new Municipality("1135", "964979367", "Sauda"),
        //        new Municipality("1141", "938633029", "Finn�y"),
        //        new Municipality("1142", "964979545", "Rennes�y"),
        //        new Municipality("1144", "964979634", "Kvits�y"),
        //        new Municipality("1145", "964979723", "Bokn"),
        //        new Municipality("1146", "964979812", "Tysv�r"),
        //        new Municipality("1149", "940791901", "Karm�y"),
        //        new Municipality("1151", "964979901", "Utsira"),
        //        new Municipality("1160", "988893226", "Vindafjord"),
        //        new Municipality("1201", "964338531", "Bergen"),
        //        new Municipality("1211", "959435375", "Etne"),
        //        new Municipality("1216", "864967272", "Sveio"),
        //        new Municipality("1219", "834210622", "B�mlo"),
        //        new Municipality("1221", "939866914", "Stord"),
        //        new Municipality("1222", "944073310", "Fitjar"),
        //        new Municipality("1223", "959412340", "Tysnes"),
        //        new Municipality("1224", "964967636", "Kvinnherad"),
        //        new Municipality("1227", "944439927", "Jondal"),
        //        new Municipality("1228", "964967814", "Odda"),
        //        new Municipality("1231", "964967903", "Ullensvang"),
        //        new Municipality("1232", "944227121", "Eidfjord"),
        //        new Municipality("1233", "971159928", "Ulvik"),
        //        new Municipality("1234", "844162502", "Granvin"),
        //        new Municipality("1235", "960510542", "Voss"),
        //        new Municipality("1238", "944233199", "Kvam"),
        //        new Municipality("1241", "964968896", "Fusa"),
        //        new Municipality("1242", "964968985", "Samnanger"),
        //        new Municipality("1243", "844458312", "Os i Hordaland"),
        //        new Municipality("1244", "941139787", "Austevoll"),
        //        new Municipality("1245", "964338809", "Sund"),
        //        new Municipality("1246", "951996777", "Fjell"),
        //        new Municipality("1247", "964338442", "Ask�y"),
        //        new Municipality("1251", "961821967", "Vaksdal"),
        //        new Municipality("1252", "964969302", "Modalen"),
        //        new Municipality("1253", "864338712", "Oster�y"),
        //        new Municipality("1256", "951549770", "Meland"),
        //        new Municipality("1259", "938766223", "�ygarden"),
        //        new Municipality("1260", "954748634", "Rad�y"),
        //        new Municipality("1263", "935084733", "Lind�s"),
        //        new Municipality("1264", "948350823", "Austrheim"),
        //        new Municipality("1265", "944041036", "Fedje"),
        //        new Municipality("1266", "945627913", "Masfjorden"),
        //        new Municipality("1401", "935473578", "Flora"),
        //        new Municipality("1411", "938497524", "Gulen"),
        //        new Municipality("1412", "964967458", "Solund"),
        //        new Municipality("1413", "964967547", "Hyllestad"),
        //        new Municipality("1416", "944439838", "H�yanger"),
        //        new Municipality("1417", "937498764", "Vik"),
        //        new Municipality("1418", "970184252", "Balestrand"),
        //        new Municipality("1419", "964967725", "Leikanger"),
        //        new Municipality("1420", "936401651", "Sogndal"),
        //        new Municipality("1421", "964968063", "Aurland"),
        //        new Municipality("1422", "954681890", "L�rdal"),
        //        new Municipality("1424", "954679721", "�rdal"),
        //        new Municipality("1426", "964968241", "Luster"),
        //        new Municipality("1428", "964968330", "Askvoll"),
        //        new Municipality("1429", "864968422", "Fjaler"),
        //        new Municipality("1430", "963923988", "Gaular"),
        //        new Municipality("1431", "963923600", "J�lster"),
        //        new Municipality("1432", "963923511", "F�rde"),
        //        new Municipality("1433", "963923899", "Naustdal"),
        //        new Municipality("1438", "959318166", "Bremanger"),
        //        new Municipality("1439", "942952880", "V�gs�y"),
        //        new Municipality("1441", "970541705", "Selje"),
        //        new Municipality("1443", "938521816", "Eid"),
        //        new Municipality("1444", "835095762", "Hornindal"),
        //        new Municipality("1445", "964969124", "Gloppen"),
        //        new Municipality("1449", "963989202", "Stryn"),
        //        new Municipality("1502", "944020977", "Molde"),
        //        new Municipality("1504", "942953119", "�lesund"),
        //        new Municipality("1505", "991891919", "Kristiansund"),
        //        new Municipality("1511", "964978662", "Vanylven"),
        //        new Municipality("1514", "822534422", "Sande i M�re og Romsdal"),
        //        new Municipality("1515", "964978840", "Her�y i M�re og Romsdal"),
        //        new Municipality("1516", "964979456", "Ulstein"),
        //        new Municipality("1517", "964979278", "Hareid"),
        //        new Municipality("1519", "939760946", "Volda"),
        //        new Municipality("1520", "939461450", "�rsta"),
        //        new Municipality("1523", "864978932", "�rskog"),
        //        new Municipality("1524", "939396268", "Norddal"),
        //        new Municipality("1525", "964980098", "Stranda"),
        //        new Municipality("1526", "964980276", "Stordal"),
        //        new Municipality("1528", "964980365", "Sykkylven"),
        //        new Municipality("1529", "964980454", "Skodje"),
        //        new Municipality("1531", "964980543", "Sula"),
        //        new Municipality("1532", "964980721", "Giske"),
        //        new Municipality("1534", "964980810", "Haram"),
        //        new Municipality("1535", "939901965", "Vestnes"),
        //        new Municipality("1539", "864980902", "Rauma"),
        //        new Municipality("1543", "864981062", "Nesset"),
        //        new Municipality("1545", "964981159", "Midsund"),
        //        new Municipality("1546", "964981248", "Sand�y"),
        //        new Municipality("1547", "964981337", "Aukra"),
        //        new Municipality("1548", "845241112", "Fr�na"),
        //        new Municipality("1551", "945685263", "Eide"),
        //        new Municipality("1554", "962378064", "Aver�y"),
        //        new Municipality("1557", "964981426", "Gjemnes"),
        //        new Municipality("1560", "964981515", "Tingvoll"),
        //        new Municipality("1563", "964981604", "Sunndal"),
        //        new Municipality("1566", "964981892", "Surnadal"),
        //        new Municipality("1567", "940138051", "Rindal"),
        //        new Municipality("1571", "962350526", "Halsa"),
        //        new Municipality("1573", "945012986", "Sm�la"),
        //        new Municipality("1576", "988913898", "Aure"),
        //        new Municipality("1601", "942110464", "Trondheim"),
        //        new Municipality("1612", "940158893", "Hemne"),
        //        new Municipality("1613", "964982309", "Snillfjord"),
        //        new Municipality("1617", "938772924", "Hitra"),
        //        new Municipality("1620", "964982597", "Fr�ya"),
        //        new Municipality("1621", "964982686", "�rland"),
        //        new Municipality("1622", "840098222", "Agdenes"),
        //        new Municipality("1624", "944305483", "Rissa"),
        //        new Municipality("1627", "944422315", "Bjugn"),
        //        new Municipality("1630", "964982864", "�fjord"),
        //        new Municipality("1632", "944315438", "Roan"),
        //        new Municipality("1633", "944350675", "Osen"),
        //        new Municipality("1634", "964983003", "Oppdal"),
        //        new Municipality("1635", "940083672", "Rennebu"),
        //        new Municipality("1636", "958731647", "Meldal"),
        //        new Municipality("1638", "958731558", "Orkdal"),
        //        new Municipality("1640", "939898743", "R�ros"),
        //        new Municipality("1644", "937697767", "Holt�len"),
        //        new Municipality("1648", "970187715", "Midtre Gauldal"),
        //        new Municipality("1653", "938726027", "Melhus"),
        //        new Municipality("1657", "939865942", "Skaun"),
        //        new Municipality("1662", "958731469", "Kl�bu"),
        //        new Municipality("1663", "971035560", "Malvik"),
        //        new Municipality("1664", "971197609", "Selbu"),
        //        new Municipality("1665", "864983472", "Tydal"),
        //        new Municipality("1702", "840029212", "Steinkjer"),
        //        new Municipality("1703", "942875967", "Namsos"),
        //        new Municipality("1711", "835231712", "Mer�ker"),
        //        new Municipality("1714", "939958851", "Stj�rdal"),
        //        new Municipality("1717", "944482253", "Frosta"),
        //        new Municipality("1718", "944426124", "Leksvik"),
        //        new Municipality("1719", "938587051", "Levanger"),
        //        new Municipality("1721", "938587418", "Verdal"),
        //        new Municipality("1724", "964981981", "Verran"),
        //        new Municipality("1725", "940015456", "Namdalseid"),
        //        new Municipality("1736", "964982031", "Sn�sa"),
        //        new Municipality("1738", "972417963", "Lierne"),
        //        new Municipality("1739", "964982120", "R�yrvik"),
        //        new Municipality("1740", "864982212", "Namsskogan"),
        //        new Municipality("1742", "940010853", "Grong"),
        //        new Municipality("1743", "959220476", "H�ylandet"),
        //        new Municipality("1744", "939896600", "Overhalla"),
        //        new Municipality("1748", "964982775", "Fosnes"),
        //        new Municipality("1749", "845153272", "Flatanger"),
        //        new Municipality("1750", "944976728", "Vikna"),
        //        new Municipality("1751", "944350497", "N�r�y"),
        //        new Municipality("1755", "944484574", "Leka"),
        //        new Municipality("1756", "997391853", "Inder�y"),
        //        new Municipality("1804", "972418013", "Bod�"),
        //        new Municipality("1805", "959469059", "Narvik"),
        //        new Municipality("1811", "964983380", "Bindal"),
        //        new Municipality("1812", "944810277", "S�mna"),
        //        new Municipality("1813", "964983291", "Br�nn�y"),
        //        new Municipality("1815", "941017975", "Vega"),
        //        new Municipality("1816", "940651034", "Vevelstad"),
        //        new Municipality("1818", "872417982", "Her�y i Nordland"),
        //        new Municipality("1820", "938712441", "Alstahaug"),
        //        new Municipality("1822", "945034572", "Leirfjord"),
        //        new Municipality("1824", "844824122", "Vefsn"),
        //        new Municipality("1825", "940643112", "Grane"),
        //        new Municipality("1826", "944716904", "Hattfjelldal"),
        //        new Municipality("1827", "945114878", "D�nna"),
        //        new Municipality("1828", "939600515", "Nesna"),
        //        new Municipality("1832", "846316442", "Hemnes"),
        //        new Municipality("1833", "872418032", "Rana"),
        //        new Municipality("1834", "940667852", "Lur�y"),
        //        new Municipality("1835", "940192692", "Tr�na"),
        //        new Municipality("1836", "945717173", "R�d�y"),
        //        new Municipality("1837", "970189866", "Mel�y"),
        //        new Municipality("1838", "845901422", "Gildesk�l"),
        //        new Municipality("1839", "961147867", "Beiarn"),
        //        new Municipality("1840", "972417734", "Saltdal"),
        //        new Municipality("1841", "972418021", "Fauske"),
        //        new Municipality("1845", "972417750", "S�rfold"),
        //        new Municipality("1848", "962299385", "Steigen"),
        //        new Municipality("1849", "970542507", "Hamar�y"),
        //        new Municipality("1850", "972417769", "Tysfjord"),
        //        new Municipality("1851", "945468661", "L�dingen"),
        //        new Municipality("1852", "959469237", "Tjeldsund"),
        //        new Municipality("1853", "940642140", "Evenes"),
        //        new Municipality("1854", "945152931", "Ballangen"),
        //        new Municipality("1856", "945037687", "R�st"),
        //        new Municipality("1857", "964994307", "V�r�y"),
        //        new Municipality("1859", "863320852", "Flakstad"),
        //        new Municipality("1860", "942570619", "Vestv�g�y"),
        //        new Municipality("1865", "938644500", "V�gan"),
        //        new Municipality("1866", "958501420", "Hadsel"),
        //        new Municipality("1867", "945452676", "B� i Nordland "),
        //        new Municipality("1868", "845152012", "�ksnes"),
        //        new Municipality("1870", "847737492", "Sortland"),
        //        new Municipality("1871", "945624809", "And�y"),
        //        new Municipality("1874", "945962151", "Moskenes"),
        //        new Municipality("1902", "940101808", "Troms�"),
        //        new Municipality("1903", "972417971", "Harstad"),
        //        new Municipality("1911", "972417998", "Kv�fjord"),
        //        new Municipality("1913", "959469326", "Sk�nland"),
        //        new Municipality("1917", "959469792", "Ibestad"),
        //        new Municipality("1919", "959469415", "Gratangen"),
        //        new Municipality("1920", "959469881", "Lavangen"),
        //        new Municipality("1922", "864993982", "Bardu"),
        //        new Municipality("1923", "961416388", "Salangen"),
        //        new Municipality("1924", "972418005", "M�lselv"),
        //        new Municipality("1925", "940755603", "S�rreisa"),
        //        new Municipality("1926", "864994032", "Dyr�y"),
        //        new Municipality("1927", "944824030", "Tran�y"),
        //        new Municipality("1928", "939900993", "Torsken"),
        //        new Municipality("1929", "943730040", "Berg"),
        //        new Municipality("1931", "939807314", "Lenvik"),
        //        new Municipality("1933", "940208580", "Balsfjord"),
        //        new Municipality("1936", "940330408", "Karls�y"),
        //        new Municipality("1938", "840014932", "Lyngen"),
        //        new Municipality("1939", "964994129", "Storfjord"),
        //        new Municipality("1940", "940363586", "G�ivuotna - K�fjord"),
        //        new Municipality("1941", "941812716", "Skjerv�y"),
        //        new Municipality("1942", "943350833", "Nordreisa"),
        //        new Municipality("1943", "940331102", "Kv�nangen"),
        //        new Municipality("2002", "972418048", "Vard�"),
        //        new Municipality("2003", "964993602", "Vads�"),
        //        new Municipality("2004", "964830533", "Hammerfest"),
        //        new Municipality("2011", "945475056", "Guovdageaidnu - Kautokeino"),
        //        new Municipality("2012", "944588132", "Alta"),
        //        new Municipality("2014", "963063237", "Loppa"),
        //        new Municipality("2015", "964830711", "Hasvik"),
        //        new Municipality("2017", "964830622", "Kvalsund"),
        //        new Municipality("2018", "941087957", "M�s�y"),
        //        new Municipality("2019", "938469415", "Nordkapp"),
        //        new Municipality("2020", "959411735", "Porsanger"),
        //        new Municipality("2021", "963376030", "K�r�sjohka - Karasjok"),
        //        new Municipality("2022", "940400392", "Lebesby"),
        //        new Municipality("2023", "934266811", "Gamvik"),
        //        new Municipality("2024", "962388108", "Berlev�g"),
        //        new Municipality("2025", "943505527", "Deatnu - Tana"),
        //        new Municipality("2027", "839953062", "Unj�rga - Nesseby"),
        //        new Municipality("2028", "938795592", "B�tsfjord"),
        //        new Municipality("2030", "942110286", "S�r-Varanger")
        //        );
        //    }
        //}
    }
}
