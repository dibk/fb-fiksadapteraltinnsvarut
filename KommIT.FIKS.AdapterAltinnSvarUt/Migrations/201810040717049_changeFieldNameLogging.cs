namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changeFieldNameLogging : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.LogEntries", "OnlyInternal", c => c.Boolean(nullable: false));
            DropColumn("dbo.LogEntries", "ShowExternal");
        }
        
        public override void Down()
        {
            AddColumn("dbo.LogEntries", "ShowExternal", c => c.Boolean(nullable: false));
            DropColumn("dbo.LogEntries", "OnlyInternal");
        }
    }
}
