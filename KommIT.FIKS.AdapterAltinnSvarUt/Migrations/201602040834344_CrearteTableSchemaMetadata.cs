namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CrearteTableSchemaMetadata : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SchemaMetadatas",
                c => new
                    {
                        ArchiveReference = c.String(nullable: false, maxLength: 128),
                        MunicipalityCode = c.String(),
                    })
                .PrimaryKey(t => t.ArchiveReference);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.SchemaMetadatas");
        }
    }
}
