namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class extendFormMetadata : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FormMetadata", "ServiceCode", c => c.String());
            AddColumn("dbo.FormMetadata", "ServiceEditionCode", c => c.Int(nullable: false));
            AddColumn("dbo.FormMetadata", "Status", c => c.String());
            AddColumn("dbo.FormMetadata", "SenderSystem", c => c.String());
            AddColumn("dbo.FormMetadata", "SendTo", c => c.String());
            AddColumn("dbo.FormMetadata", "SendFrom", c => c.String());
            AddColumn("dbo.FormMetadata", "ValidationErrors", c => c.Int(nullable: false));
            AddColumn("dbo.FormMetadata", "ValidationWarnings", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.FormMetadata", "ValidationWarnings");
            DropColumn("dbo.FormMetadata", "ValidationErrors");
            DropColumn("dbo.FormMetadata", "SendFrom");
            DropColumn("dbo.FormMetadata", "SendTo");
            DropColumn("dbo.FormMetadata", "SenderSystem");
            DropColumn("dbo.FormMetadata", "Status");
            DropColumn("dbo.FormMetadata", "ServiceEditionCode");
            DropColumn("dbo.FormMetadata", "ServiceCode");
        }
    }
}
