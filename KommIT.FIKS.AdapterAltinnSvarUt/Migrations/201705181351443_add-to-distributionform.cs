namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addtodistributionform : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DistributionForms", "RecieptSentArchiveReference", c => c.String());
            AddColumn("dbo.DistributionForms", "RecieptSent", c => c.DateTime());
            AddColumn("dbo.DistributionForms", "ErrorMessage", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.DistributionForms", "ErrorMessage");
            DropColumn("dbo.DistributionForms", "RecieptSent");
            DropColumn("dbo.DistributionForms", "RecieptSentArchiveReference");
        }
    }
}
