﻿namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemovedMunicipalityAttachmentSettings : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.MunicipalityAttachmentSettings", "MunicipalityCode", "dbo.Municipalities");
            DropIndex("dbo.MunicipalityAttachmentSettings", new[] { "MunicipalityCode" });
            DropTable("dbo.MunicipalityAttachmentSettings");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.MunicipalityAttachmentSettings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DocumentNumber = c.Int(nullable: false),
                        AttatchmentTypeName = c.String(),
                        Title = c.String(),
                        DocumentType = c.String(),
                        DocumentStatus = c.String(),
                        TilknyttetRegistreringSom = c.String(),
                        MunicipalityCode = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateIndex("dbo.MunicipalityAttachmentSettings", "MunicipalityCode");
            AddForeignKey("dbo.MunicipalityAttachmentSettings", "MunicipalityCode", "dbo.Municipalities", "Code", cascadeDelete: true);
        }
    }
}
