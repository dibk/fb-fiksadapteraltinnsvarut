﻿namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class SaByNewColumnsAndRenaming : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.TiltakshaversSamtykkes", name: "Tiltakshaver", newName: "TiltakshaverNavn");
            RenameColumn(table: "dbo.TiltakshaversSamtykkes", name: "TiltakshaverFoedselsnummer", newName: "SignertTiltakshaverFnr");            
            RenameColumn(table: "dbo.TiltakshaversSamtykkes", name: "TiltakshaverOrganisasjonsnummer", newName: "SignertTiltakshaverOrgnr");

            AddColumn("dbo.TiltakshaversSamtykkes", "TiltakshaverOrgnr", c => c.String());
            AddColumn("dbo.TiltakshaversSamtykkes", "TiltakshaverPartstypeKode", c => c.String());
        }

        public override void Down()
        {
            RenameColumn(table: "dbo.TiltakshaversSamtykkes", name: "TiltakshaverNavn", newName: "Tiltakshaver");
            RenameColumn(table: "dbo.TiltakshaversSamtykkes", name: "SignertTiltakshaverFnr", newName: "TiltakshaverFoedselsnummer");
            RenameColumn(table: "dbo.TiltakshaversSamtykkes", name: "SignertTiltakshaverOrgnr", newName: "TiltakshaverOrganisasjonsnummer");

            DropColumn("dbo.TiltakshaversSamtykkes", "TiltakshaverPartstypeKode");
            DropColumn("dbo.TiltakshaversSamtykkes", "TiltakshaverOrgnr");
        }
    }
}
