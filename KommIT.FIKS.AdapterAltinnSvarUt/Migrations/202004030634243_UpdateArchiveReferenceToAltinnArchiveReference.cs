namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateArchiveReferenceToAltinnArchiveReference : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DeviationLetters", "AltinnArchiveReference", c => c.String());
            DropColumn("dbo.DeviationLetters", "ArchiveReference");
        }
        
        public override void Down()
        {
            AddColumn("dbo.DeviationLetters", "ArchiveReference", c => c.String());
            DropColumn("dbo.DeviationLetters", "AltinnArchiveReference");
        }
    }
}
