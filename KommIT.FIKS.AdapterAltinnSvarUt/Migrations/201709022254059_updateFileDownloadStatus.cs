namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateFileDownloadStatus : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FileDownloadStatus", "TimeReceived", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.FileDownloadStatus", "TimeReceived");
        }
    }
}
