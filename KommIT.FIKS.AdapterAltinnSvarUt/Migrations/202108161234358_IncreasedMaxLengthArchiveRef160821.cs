namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IncreasedMaxLengthArchiveRef160821 : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.DistributionForms", new[] { "InitialArchiveReference" });
            DropIndex("dbo.DistributionForms", new[] { "SignedArchiveReference" });
            AlterColumn("dbo.DistributionForms", "InitialArchiveReference", c => c.String(maxLength: 40));
            AlterColumn("dbo.DistributionForms", "SignedArchiveReference", c => c.String(maxLength: 40));
            CreateIndex("dbo.DistributionForms", "InitialArchiveReference");
            CreateIndex("dbo.DistributionForms", "SignedArchiveReference");
        }
        
        public override void Down()
        {
            DropIndex("dbo.DistributionForms", new[] { "SignedArchiveReference" });
            DropIndex("dbo.DistributionForms", new[] { "InitialArchiveReference" });
            AlterColumn("dbo.DistributionForms", "SignedArchiveReference", c => c.String(maxLength: 20));
            AlterColumn("dbo.DistributionForms", "InitialArchiveReference", c => c.String(maxLength: 20));
            CreateIndex("dbo.DistributionForms", "SignedArchiveReference");
            CreateIndex("dbo.DistributionForms", "InitialArchiveReference");
        }
    }
}
