namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addApiAuthentication : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.APIAuthentications",
                c => new
                    {
                        Username = c.String(nullable: false, maxLength: 128, unicode: false),
                        Password = c.String(),
                        UserId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Username)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.APIAuthentications", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.APIAuthentications", new[] { "UserId" });
            DropTable("dbo.APIAuthentications");
        }
    }
}
