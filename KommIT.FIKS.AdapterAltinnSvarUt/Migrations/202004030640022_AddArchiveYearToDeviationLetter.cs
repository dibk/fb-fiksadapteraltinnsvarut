namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddArchiveYearToDeviationLetter : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DeviationLetters", "ArchiveYear", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.DeviationLetters", "ArchiveYear");
        }
    }
}
