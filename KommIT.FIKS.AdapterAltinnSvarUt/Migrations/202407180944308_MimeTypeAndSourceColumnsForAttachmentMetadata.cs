﻿namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class MimeTypeAndSourceColumnsForAttachmentMetadata : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FormAttachmentMetadatas", "MimeType", c => c.String());
            AddColumn("dbo.FormAttachmentMetadatas", "Source", c => c.String());
        }

        public override void Down()
        {
            DropColumn("dbo.FormAttachmentMetadatas", "Source");
            DropColumn("dbo.FormAttachmentMetadatas", "MimeType");
        }
    }
}