// <auto-generated />
namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class TiltakshaverSignaturAddOrganisasjonsnummer : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(TiltakshaverSignaturAddOrganisasjonsnummer));
        
        string IMigrationMetadata.Id
        {
            get { return "201906030758481_TiltakshaverSignaturAddOrganisasjonsnummer"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
