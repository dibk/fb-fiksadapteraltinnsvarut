// <auto-generated />
namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class AddCascadeDeleteOnActivity : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddCascadeDeleteOnActivity));
        
        string IMigrationMetadata.Id
        {
            get { return "201708170749215_AddCascadeDeleteOnActivity"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
