namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addReferenceId : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Activities", "ReferenceId", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Activities", "ReferenceId");
        }
    }
}
