namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFormWorkflowLog : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.FormWorkflowLogs",
                c => new
                    {
                        ArchicveReference = c.String(nullable: false, maxLength: 20),
                        FtbId = c.String(maxLength: 40),
                    })
                .PrimaryKey(t => t.ArchicveReference)
                .Index(t => t.FtbId);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.FormWorkflowLogs", new[] { "FtbId" });
            DropTable("dbo.FormWorkflowLogs");
        }
    }
}
