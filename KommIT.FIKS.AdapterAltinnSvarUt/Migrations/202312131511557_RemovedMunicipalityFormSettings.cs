﻿namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemovedMunicipalityFormSettings : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.MunicipalityFormSettings", "FormId", "dbo.Forms");
            DropForeignKey("dbo.MunicipalityFormSettings", "MunicipalityCode", "dbo.Municipalities");
            DropIndex("dbo.MunicipalityFormSettings", new[] { "FormId" });
            DropIndex("dbo.MunicipalityFormSettings", new[] { "MunicipalityCode" });
            DropTable("dbo.MunicipalityFormSettings");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.MunicipalityFormSettings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        FormId = c.Int(nullable: false),
                        MunicipalityCode = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateIndex("dbo.MunicipalityFormSettings", "MunicipalityCode");
            CreateIndex("dbo.MunicipalityFormSettings", "FormId");
            AddForeignKey("dbo.MunicipalityFormSettings", "MunicipalityCode", "dbo.Municipalities", "Code", cascadeDelete: true);
            AddForeignKey("dbo.MunicipalityFormSettings", "FormId", "dbo.Forms", "Id", cascadeDelete: true);
        }
    }
}
