namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddForsendelsesIdToSchemaMetadata : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SchemaMetadatas", "ForsendelsesId", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.SchemaMetadatas", "ForsendelsesId");
        }
    }
}
