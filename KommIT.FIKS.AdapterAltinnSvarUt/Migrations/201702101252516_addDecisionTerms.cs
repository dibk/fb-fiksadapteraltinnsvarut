namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addDecisionTerms : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Decisions",
                c => new
                    {
                        DecisionId = c.Long(nullable: false, identity: true),
                        ArchiveReference = c.String(),
                        MunicipalityArchiveCaseYear = c.Int(nullable: false),
                        MunicipalityArchiveCaseSequence = c.Long(nullable: false),
                        MunicipalityPublicArchiveCaseUrl = c.String(),
                        DecisionDate = c.DateTime(),
                        DecisionStatus = c.String(),
                    })
                .PrimaryKey(t => t.DecisionId);
            
            CreateTable(
                "dbo.Terms",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        TermsId = c.String(),
                        TermForProcess = c.String(),
                        TermDescription = c.String(),
                        Decision_DecisionId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Decisions", t => t.Decision_DecisionId)
                .Index(t => t.Decision_DecisionId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Terms", "Decision_DecisionId", "dbo.Decisions");
            DropIndex("dbo.Terms", new[] { "Decision_DecisionId" });
            DropTable("dbo.Terms");
            DropTable("dbo.Decisions");
        }
    }
}
