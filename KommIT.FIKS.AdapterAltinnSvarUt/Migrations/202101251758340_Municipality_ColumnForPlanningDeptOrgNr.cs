namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Municipality_ColumnForPlanningDeptOrgNr : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Municipalities", "PlanningDepartmentSpecificOrganizationNumber", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Municipalities", "PlanningDepartmentSpecificOrganizationNumber");
        }
    }
}
