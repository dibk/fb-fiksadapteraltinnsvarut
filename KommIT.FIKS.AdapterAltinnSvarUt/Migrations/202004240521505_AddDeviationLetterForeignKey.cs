namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDeviationLetterForeignKey : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Deviations", "DeviationLetter_DeviationId", "dbo.DeviationLetters");
            DropIndex("dbo.Deviations", new[] { "DeviationLetter_DeviationId" });
            RenameColumn(table: "dbo.Deviations", name: "DeviationLetter_DeviationId", newName: "DeviationId");
            AlterColumn("dbo.Deviations", "DeviationId", c => c.Long(nullable: false));
            CreateIndex("dbo.Deviations", "DeviationId");
            AddForeignKey("dbo.Deviations", "DeviationId", "dbo.DeviationLetters", "DeviationId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Deviations", "DeviationId", "dbo.DeviationLetters");
            DropIndex("dbo.Deviations", new[] { "DeviationId" });
            AlterColumn("dbo.Deviations", "DeviationId", c => c.Long());
            RenameColumn(table: "dbo.Deviations", name: "DeviationId", newName: "DeviationLetter_DeviationId");
            CreateIndex("dbo.Deviations", "DeviationLetter_DeviationId");
            AddForeignKey("dbo.Deviations", "DeviationLetter_DeviationId", "dbo.DeviationLetters", "DeviationId");
        }
    }
}
