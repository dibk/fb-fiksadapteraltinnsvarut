namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddsTableForAttachmentMetadata : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.FormAttachmentMetadatas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ArchiveReference = c.String(maxLength: 128),
                        FileName = c.String(),
                        Size = c.Int(nullable: false),
                        AttachmentType = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.FormMetadata", t => t.ArchiveReference)
                .Index(t => t.ArchiveReference);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.FormAttachmentMetadatas", "ArchiveReference", "dbo.FormMetadata");
            DropIndex("dbo.FormAttachmentMetadatas", new[] { "ArchiveReference" });
            DropTable("dbo.FormAttachmentMetadatas");
        }
    }
}
