namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddMoreFieldsToFormMetadata : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FormMetadata", "ArchiveTimestamp", c => c.DateTime());
            AddColumn("dbo.FormMetadata", "FormType", c => c.String());
            AddColumn("dbo.FormMetadata", "TitleStringForSvarUt", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.FormMetadata", "TitleStringForSvarUt");
            DropColumn("dbo.FormMetadata", "FormType");
            DropColumn("dbo.FormMetadata", "ArchiveTimestamp");
        }
    }
}
