﻿namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddColForDeletion_and_RenameExistingCol_SaBy : DbMigration
    {
        public override void Up()
        {
            RenameColumn("dbo.TiltakshaversSamtykkes", "DateTime", "DateCreated");
            AddColumn("dbo.TiltakshaversSamtykkes", "DateDeleted", c => c.DateTime());
        }
        
        public override void Down()
        {
            RenameColumn("dbo.TiltakshaversSamtykkes","DateCreated",  "DateTime");
            DropColumn("dbo.TiltakshaversSamtykkes", "DateDeleted");
        }
    }
}
