namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateExternalDeviationType : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Deviations", "ExternalDeviationId", c => c.Guid());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Deviations", "ExternalDeviationId", c => c.String());
        }
    }
}
