namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class fixNameinMapTableSaBy : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.TiltakshaversSamtikkeTSTiltaktypes", newName: "TiltakshaversSamtykkeTSTiltaktypes");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.TiltakshaversSamtykkeTSTiltaktypes", newName: "TiltakshaversSamtikkeTSTiltaktypes");
        }
    }
}
