namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SaveSvarUtShippingInformation : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FormMetadata", "SvarUtForsendelsesId", c => c.String());
            AddColumn("dbo.FormMetadata", "SvarUtShippingTimestamp", c => c.DateTime());
            DropColumn("dbo.FormMetadata", "ForsendelsesId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.FormMetadata", "ForsendelsesId", c => c.String());
            DropColumn("dbo.FormMetadata", "SvarUtShippingTimestamp");
            DropColumn("dbo.FormMetadata", "SvarUtForsendelsesId");
        }
    }
}
