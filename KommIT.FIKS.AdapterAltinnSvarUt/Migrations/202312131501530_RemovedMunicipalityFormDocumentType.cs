﻿namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemovedMunicipalityFormDocumentType : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.MunicipalityFormDocumentTypes", "MunicipalityCode", "dbo.Municipalities");
            DropIndex("dbo.MunicipalityFormDocumentTypes", new[] { "MunicipalityCode" });
            DropTable("dbo.MunicipalityFormDocumentTypes");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.MunicipalityFormDocumentTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MunicipalityCodeKey = c.String(),
                        MunicipalityCodeValue = c.String(),
                        MunicipalityCode = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateIndex("dbo.MunicipalityFormDocumentTypes", "MunicipalityCode");
            AddForeignKey("dbo.MunicipalityFormDocumentTypes", "MunicipalityCode", "dbo.Municipalities", "Code");
        }
    }
}
