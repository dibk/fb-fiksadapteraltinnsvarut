namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addWizTiltakshaver : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TiltakshaversSamtykkes",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Tiltakshaver = c.String(),
                        AltinnArkivreferanse = c.String(),
                        AnsvarligSoeker = c.String(),
                        Status = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.TiltakshaversSamtykkes");
        }
    }
}
