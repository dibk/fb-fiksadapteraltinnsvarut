namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_PostDistributionMetaData_table : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PostDistributionMetaDatas",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ReferenceId = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.PostDistributionMetaDatas");
        }
    }
}
