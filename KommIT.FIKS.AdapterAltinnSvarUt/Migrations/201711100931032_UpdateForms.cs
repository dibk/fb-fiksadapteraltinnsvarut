namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateForms : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FileDownloadStatus", "FormName", c => c.String());
            DropColumn("dbo.FileDownloadStatus", "FormType");
        }
        
        public override void Down()
        {
            AddColumn("dbo.FileDownloadStatus", "FormType", c => c.Int(nullable: false));
            DropColumn("dbo.FileDownloadStatus", "FormName");
        }
    }
}
