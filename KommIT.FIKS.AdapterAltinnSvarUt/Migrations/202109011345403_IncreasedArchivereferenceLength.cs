namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IncreasedArchivereferenceLength : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.FileDownloadStatus", new[] { "ArchiveReference" });
            DropPrimaryKey("dbo.FormWorkflowLogs");
            AlterColumn("dbo.FileDownloadStatus", "ArchiveReference", c => c.String(maxLength: 40));
            AlterColumn("dbo.FormWorkflowLogs", "ArchicveReference", c => c.String(nullable: false, maxLength: 40));
            AddPrimaryKey("dbo.FormWorkflowLogs", "ArchicveReference");
            CreateIndex("dbo.FileDownloadStatus", "ArchiveReference");
        }
        
        public override void Down()
        {
            DropIndex("dbo.FileDownloadStatus", new[] { "ArchiveReference" });
            DropPrimaryKey("dbo.FormWorkflowLogs");
            AlterColumn("dbo.FormWorkflowLogs", "ArchicveReference", c => c.String(nullable: false, maxLength: 20));
            AlterColumn("dbo.FileDownloadStatus", "ArchiveReference", c => c.String(maxLength: 20));
            AddPrimaryKey("dbo.FormWorkflowLogs", "ArchicveReference");
            CreateIndex("dbo.FileDownloadStatus", "ArchiveReference");
        }
    }
}
