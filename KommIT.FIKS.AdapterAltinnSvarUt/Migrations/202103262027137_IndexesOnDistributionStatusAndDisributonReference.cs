namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IndexesOnDistributionStatusAndDisributonReference : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.DistributionForms", "DistributionStatus");
            CreateIndex("dbo.DistributionForms", "DistributionReference");
        }
        
        public override void Down()
        {
            DropIndex("dbo.DistributionForms", new[] { "DistributionReference" });
            DropIndex("dbo.DistributionForms", new[] { "DistributionStatus" });
        }
    }
}
