namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addActivityOrder : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Activities", "OrderNumber", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Activities", "OrderNumber");
        }
    }
}
