namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateReceiptTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Receipts",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        AltinnArchiveReference = c.String(),
                        MunicipalityArchiveCaseYear = c.Int(nullable: false),
                        MunicipalityArchiveCaseSequence = c.Long(nullable: false),
                        MunicipalityPublicArchiveCaseUrl = c.String(),
                        ArchiveYear = c.Int(nullable: false),
                        ArchiveSequence = c.Int(nullable: false),
                        TiltaksId = c.String(),
                        OrganizationNumber = c.String(),
                        MunicipalityCode = c.String(),
                        Message = c.String(),
                        ReferenceDeviationId = c.Guid(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Receipts");
        }
    }
}
