namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddReceiptDate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Receipts", "ReceiptDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Receipts", "ReceiptDate");
        }
    }
}
