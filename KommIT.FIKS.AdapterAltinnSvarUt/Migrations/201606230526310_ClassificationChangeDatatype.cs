namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ClassificationChangeDatatype : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.MunicipalityClassifications", "Sorting", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.MunicipalityClassifications", "Sorting", c => c.String());
        }
    }
}
