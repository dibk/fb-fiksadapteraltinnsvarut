namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddArchiveSequenceToDeviationLetter : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DeviationLetters", "ArchiveSequence", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.DeviationLetters", "ArchiveSequence");
        }
    }
}
