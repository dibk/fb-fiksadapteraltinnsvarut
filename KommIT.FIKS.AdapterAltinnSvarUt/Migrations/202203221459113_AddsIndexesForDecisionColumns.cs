﻿namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddsIndexesForDecisionColumns : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Decisions", "ArchiveReference", c => c.String(maxLength: 40));
            CreateIndex("dbo.Decisions", "ArchiveReference");
            CreateIndex("dbo.Decisions", "DecisionDate");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Decisions", new[] { "DecisionDate" });
            DropIndex("dbo.Decisions", new[] { "ArchiveReference" });
            AlterColumn("dbo.Decisions", "ArchiveReference", c => c.String());
        }
    }
}
