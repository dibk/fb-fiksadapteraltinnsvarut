namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addMunicipalityFormDocumentType : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MunicipalityFormDocumentTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MunicipalityCodeKey = c.String(),
                        MunicipalityCodeValue = c.String(),
                        MunicipalityCode = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Municipalities", t => t.MunicipalityCode, cascadeDelete: true)
                .Index(t => t.MunicipalityCode);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MunicipalityFormDocumentTypes", "MunicipalityCode", "dbo.Municipalities");
            DropIndex("dbo.MunicipalityFormDocumentTypes", new[] { "MunicipalityCode" });
            DropTable("dbo.MunicipalityFormDocumentTypes");
        }
    }
}
