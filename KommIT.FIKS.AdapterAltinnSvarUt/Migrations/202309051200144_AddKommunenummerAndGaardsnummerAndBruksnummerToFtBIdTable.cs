﻿namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddKommunenummerAndGaardsnummerAndBruksnummerToFtBIdTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FtBIds", "Kommunenummer", c => c.String(nullable: false));
            AddColumn("dbo.FtBIds", "Gaardsnummer", c => c.Int(nullable: false));
            AddColumn("dbo.FtBIds", "Bruksnummer", c => c.Int(nullable: false));
            AddColumn("dbo.FtBIds", "Prosjektnummer", c => c.String());
            AddColumn("dbo.FtBIds", "Prosjektnavn", c => c.String());
            DropColumn("dbo.FtBIds", "ProjectNo");
            DropColumn("dbo.FtBIds", "ProjectName");
        }
        
        public override void Down()
        {
            AddColumn("dbo.FtBIds", "ProjectName", c => c.String());
            AddColumn("dbo.FtBIds", "ProjectNo", c => c.String());
            DropColumn("dbo.FtBIds", "Prosjektnavn");
            DropColumn("dbo.FtBIds", "Prosjektnummer");
            DropColumn("dbo.FtBIds", "Bruksnummer");
            DropColumn("dbo.FtBIds", "Gaardsnummer");
            DropColumn("dbo.FtBIds", "Kommunenummer");
        }
    }
}
