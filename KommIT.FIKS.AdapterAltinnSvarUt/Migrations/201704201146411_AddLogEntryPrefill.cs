namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddLogEntryPrefill : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.LogEntryDistributionStatus",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        InternalId = c.String(nullable: false, maxLength: 128),
                        Status = c.String(),
                        Timestamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.LogEntryPrefills", t => t.InternalId, cascadeDelete: true)
                .Index(t => t.InternalId);
            
            CreateTable(
                "dbo.LogEntryPrefills",
                c => new
                    {
                        InternalId = c.String(nullable: false, maxLength: 128),
                        Foretak = c.String(),
                        OrgNr = c.String(),
                        Ansvarsomraade = c.String(),
                        AltinnServiceReference = c.String(),
                        LogEntryId = c.Int(nullable: false),
                        ArchiveReferencePrefill = c.String(),
                        VaarReferanse = c.String(),
                        Timestamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.InternalId)
                .ForeignKey("dbo.LogEntries", t => t.LogEntryId, cascadeDelete: true)
                .Index(t => t.LogEntryId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.LogEntryDistributionStatus", "InternalId", "dbo.LogEntryPrefills");
            DropForeignKey("dbo.LogEntryPrefills", "LogEntryId", "dbo.LogEntries");
            DropIndex("dbo.LogEntryPrefills", new[] { "LogEntryId" });
            DropIndex("dbo.LogEntryDistributionStatus", new[] { "InternalId" });
            DropTable("dbo.LogEntryPrefills");
            DropTable("dbo.LogEntryDistributionStatus");
        }
    }
}
