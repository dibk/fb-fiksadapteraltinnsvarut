namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TiltakshaverSignaturUpdates : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TiltakshaversSamtykkes", "Prosjektnavn", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.TiltakshaversSamtykkes", "Prosjektnavn");
        }
    }
}
