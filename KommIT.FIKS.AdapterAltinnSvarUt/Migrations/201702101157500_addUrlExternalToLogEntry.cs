namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addUrlExternalToLogEntry : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.LogEntries", "ShowExternal", c => c.Boolean(nullable: false));
            AddColumn("dbo.LogEntries", "Url", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.LogEntries", "Url");
            DropColumn("dbo.LogEntries", "ShowExternal");
        }
    }
}
