namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MoveMatrikkelInformationFromDeviationToDeviationLetter : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DeviationLetters", "CadastralUnitNumber", c => c.Int(nullable: false));
            AddColumn("dbo.DeviationLetters", "PropertyUnitNumber", c => c.Int(nullable: false));
            AddColumn("dbo.DeviationLetters", "LeaseholdNumber", c => c.Int(nullable: false));
            AddColumn("dbo.DeviationLetters", "CondominiumUnitNumber", c => c.Int(nullable: false));
            AlterColumn("dbo.DeviationLetters", "MunicipalityCode", c => c.Int(nullable: false));
            DropColumn("dbo.DeviationLetters", "MatrikkelNumber");
            DropColumn("dbo.Deviations", "MunicipalityCode");
            DropColumn("dbo.Deviations", "CadastralUnitNumber");
            DropColumn("dbo.Deviations", "PropertyUnitNumber");
            DropColumn("dbo.Deviations", "LeaseholdNumber");
            DropColumn("dbo.Deviations", "CondominiumUnitNumber");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Deviations", "CondominiumUnitNumber", c => c.Int(nullable: false));
            AddColumn("dbo.Deviations", "LeaseholdNumber", c => c.Int(nullable: false));
            AddColumn("dbo.Deviations", "PropertyUnitNumber", c => c.Int(nullable: false));
            AddColumn("dbo.Deviations", "CadastralUnitNumber", c => c.Int(nullable: false));
            AddColumn("dbo.Deviations", "MunicipalityCode", c => c.Int(nullable: false));
            AddColumn("dbo.DeviationLetters", "MatrikkelNumber", c => c.String());
            AlterColumn("dbo.DeviationLetters", "MunicipalityCode", c => c.String());
            DropColumn("dbo.DeviationLetters", "CondominiumUnitNumber");
            DropColumn("dbo.DeviationLetters", "LeaseholdNumber");
            DropColumn("dbo.DeviationLetters", "PropertyUnitNumber");
            DropColumn("dbo.DeviationLetters", "CadastralUnitNumber");
        }
    }
}
