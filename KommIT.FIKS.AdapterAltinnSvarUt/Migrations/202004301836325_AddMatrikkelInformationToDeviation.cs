namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddMatrikkelInformationToDeviation : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Deviations", "MunicipalityCode", c => c.Int(nullable: false));
            AddColumn("dbo.Deviations", "CadastralUnitNumber", c => c.Int(nullable: false));
            AddColumn("dbo.Deviations", "PropertyUnitNumber", c => c.Int(nullable: false));
            AddColumn("dbo.Deviations", "LeaseholdNumber", c => c.Int(nullable: false));
            AddColumn("dbo.Deviations", "CondominiumUnitNumber", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Deviations", "CondominiumUnitNumber");
            DropColumn("dbo.Deviations", "LeaseholdNumber");
            DropColumn("dbo.Deviations", "PropertyUnitNumber");
            DropColumn("dbo.Deviations", "CadastralUnitNumber");
            DropColumn("dbo.Deviations", "MunicipalityCode");
        }
    }
}
