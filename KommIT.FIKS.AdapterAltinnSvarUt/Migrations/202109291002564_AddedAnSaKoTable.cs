namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedAnSaKoTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AnSaKoStatus",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AnSaKoProcessStatus = c.Int(nullable: false),
                        SigningDeadline = c.DateTime(),
                        StatusDetails = c.String(),
                        DistributionForm_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DistributionForms", t => t.DistributionForm_Id, cascadeDelete: true)
                .Index(t => t.DistributionForm_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AnSaKoStatus", "DistributionForm_Id", "dbo.DistributionForms");
            DropIndex("dbo.AnSaKoStatus", new[] { "DistributionForm_Id" });
            DropTable("dbo.AnSaKoStatus");
        }
    }
}
