namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveForeignKeyMunicipalityFromLogEntry : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.LogEntries", name: "MunicipalityCode", newName: "LogMunicipality_Code");
            RenameIndex(table: "dbo.LogEntries", name: "IX_MunicipalityCode", newName: "IX_LogMunicipality_Code");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.LogEntries", name: "IX_LogMunicipality_Code", newName: "IX_MunicipalityCode");
            RenameColumn(table: "dbo.LogEntries", name: "LogMunicipality_Code", newName: "MunicipalityCode");
        }
    }
}
