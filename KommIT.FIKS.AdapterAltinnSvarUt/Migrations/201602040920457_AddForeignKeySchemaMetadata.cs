namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddForeignKeySchemaMetadata : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.LogEntries", "LogMunicipality_Code", "dbo.Municipalities");
            DropIndex("dbo.LogEntries", new[] { "LogMunicipality_Code" });
            AlterColumn("dbo.LogEntries", "ArchiveReference", c => c.String(maxLength: 128));
            AlterColumn("dbo.SchemaMetadatas", "MunicipalityCode", c => c.String(nullable: false, maxLength: 128));
            CreateIndex("dbo.LogEntries", "ArchiveReference");
            CreateIndex("dbo.SchemaMetadatas", "MunicipalityCode");
            AddForeignKey("dbo.SchemaMetadatas", "MunicipalityCode", "dbo.Municipalities", "Code", cascadeDelete: true);
            AddForeignKey("dbo.LogEntries", "ArchiveReference", "dbo.SchemaMetadatas", "ArchiveReference");
            DropColumn("dbo.LogEntries", "LogMunicipality_Code");
        }
        
        public override void Down()
        {
            AddColumn("dbo.LogEntries", "LogMunicipality_Code", c => c.String(maxLength: 128));
            DropForeignKey("dbo.LogEntries", "ArchiveReference", "dbo.SchemaMetadatas");
            DropForeignKey("dbo.SchemaMetadatas", "MunicipalityCode", "dbo.Municipalities");
            DropIndex("dbo.SchemaMetadatas", new[] { "MunicipalityCode" });
            DropIndex("dbo.LogEntries", new[] { "ArchiveReference" });
            AlterColumn("dbo.SchemaMetadatas", "MunicipalityCode", c => c.String());
            AlterColumn("dbo.LogEntries", "ArchiveReference", c => c.String());
            CreateIndex("dbo.LogEntries", "LogMunicipality_Code");
            AddForeignKey("dbo.LogEntries", "LogMunicipality_Code", "dbo.Municipalities", "Code");
        }
    }
}
