namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RefactorNameInFormMetadata : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FormMetadata", "SvarUtDocumentTitle", c => c.String());
            DropColumn("dbo.FormMetadata", "TitleStringForSvarUt");
        }
        
        public override void Down()
        {
            AddColumn("dbo.FormMetadata", "TitleStringForSvarUt", c => c.String());
            DropColumn("dbo.FormMetadata", "SvarUtDocumentTitle");
        }
    }
}
