namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateTableDeviationLetter : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DeviationLetters",
                c => new
                    {
                        DeviationId = c.Long(nullable: false, identity: true),
                        ArchiveReference = c.String(),
                        MunicipalityArchiveCaseYear = c.Int(nullable: false),
                        MunicipalityArchiveCaseSequence = c.Long(nullable: false),
                        MunicipalityPublicArchiveCaseUrl = c.String(),
                        RegistrationDate = c.DateTime(),
                        Message = c.String(),
                        OrganizationNumber = c.String(),
                    })
                .PrimaryKey(t => t.DeviationId);
            
            CreateTable(
                "dbo.Deviations",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        DeviationType = c.String(),
                        Checkpoint = c.String(),
                        Description = c.String(),
                        RegistrationDate = c.DateTime(),
                        Status = c.String(),
                        DeviationLetter_DeviationId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DeviationLetters", t => t.DeviationLetter_DeviationId)
                .Index(t => t.DeviationLetter_DeviationId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Deviations", "DeviationLetter_DeviationId", "dbo.DeviationLetters");
            DropIndex("dbo.Deviations", new[] { "DeviationLetter_DeviationId" });
            DropTable("dbo.Deviations");
            DropTable("dbo.DeviationLetters");
        }
    }
}
