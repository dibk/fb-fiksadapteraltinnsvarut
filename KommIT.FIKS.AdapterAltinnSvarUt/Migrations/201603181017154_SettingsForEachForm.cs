namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SettingsForEachForm : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.MunicipalitySettings", "MunicipalityCode", "dbo.Municipalities");
            DropIndex("dbo.MunicipalitySettings", new[] { "MunicipalityCode" });
            CreateTable(
                "dbo.MunicipalityFormSettings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        FormId = c.Int(nullable: false),
                        MunicipalityCode = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Forms", t => t.FormId, cascadeDelete: true)
                .ForeignKey("dbo.Municipalities", t => t.MunicipalityCode, cascadeDelete: true)
                .Index(t => t.FormId)
                .Index(t => t.MunicipalityCode);
            
            CreateTable(
                "dbo.Forms",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        DefaultTitle = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            DropTable("dbo.MunicipalitySettings");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.MunicipalitySettings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Titles = c.String(),
                        MunicipalityCode = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id);
            
            DropForeignKey("dbo.MunicipalityFormSettings", "MunicipalityCode", "dbo.Municipalities");
            DropForeignKey("dbo.MunicipalityFormSettings", "FormId", "dbo.Forms");
            DropIndex("dbo.MunicipalityFormSettings", new[] { "MunicipalityCode" });
            DropIndex("dbo.MunicipalityFormSettings", new[] { "FormId" });
            DropTable("dbo.Forms");
            DropTable("dbo.MunicipalityFormSettings");
            CreateIndex("dbo.MunicipalitySettings", "MunicipalityCode");
            AddForeignKey("dbo.MunicipalitySettings", "MunicipalityCode", "dbo.Municipalities", "Code", cascadeDelete: true);
        }
    }
}
