namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Printlog : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DistributionForms", "Printed", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.DistributionForms", "Printed");
        }
    }
}
