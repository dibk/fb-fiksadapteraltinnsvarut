namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_initialExternalSystemReference : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DistributionForms", "InitialExternalSystemReference", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.DistributionForms", "InitialExternalSystemReference");
        }
    }
}
