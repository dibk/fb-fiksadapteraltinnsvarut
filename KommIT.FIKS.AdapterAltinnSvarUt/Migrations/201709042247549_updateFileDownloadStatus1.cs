namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateFileDownloadStatus1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FileDownloadStatus", "MimeType", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.FileDownloadStatus", "MimeType");
        }
    }
}
