namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateTiltakshaverModel : DbMigration
    {
        public override void Up()
        {
            //AddColumn("dbo.TiltakshaversSamtykkeByggesteds", "Adresselinje1", c => c.String());
            AddColumn("dbo.TiltakshaversSamtykkeByggesteds", "Postnr", c => c.String());
            AddColumn("dbo.TiltakshaversSamtykkeByggesteds", "Poststed", c => c.String());
            AddColumn("dbo.TiltakshaversSamtykkeByggesteds", "Bygningsnummer", c => c.String());
            AddColumn("dbo.TiltakshaversSamtykkeByggesteds", "Bolignummer", c => c.String());
            AddColumn("dbo.TiltakshaversSamtykkeByggesteds", "Kommunenavn", c => c.String());
            //DropColumn("dbo.TiltakshaversSamtykkeByggesteds", "Adresse");
            RenameColumn("dbo.TiltakshaversSamtykkeByggesteds", "Adresse", "Adresselinje1");
        }
        
        public override void Down()
        {
            //AddColumn("dbo.TiltakshaversSamtykkeByggesteds", "Adresse", c => c.String());
            DropColumn("dbo.TiltakshaversSamtykkeByggesteds", "Kommunenavn");
            DropColumn("dbo.TiltakshaversSamtykkeByggesteds", "Bolignummer");
            DropColumn("dbo.TiltakshaversSamtykkeByggesteds", "Bygningsnummer");
            DropColumn("dbo.TiltakshaversSamtykkeByggesteds", "Poststed");
            DropColumn("dbo.TiltakshaversSamtykkeByggesteds", "Postnr");
            //DropColumn("dbo.TiltakshaversSamtykkeByggesteds", "Adresselinje1");
            RenameColumn("dbo.TiltakshaversSamtykkeByggesteds", "Adresselinje1", "Adresse");

        }
    }
}
