namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_distributionform : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.FormMetadata", "DistributionService_ArchiveReference", "dbo.FormMetadata");
            DropForeignKey("dbo.EventLogs", "Skjema_ArchiveReference", "dbo.FormMetadata");
            DropIndex("dbo.EventLogs", new[] { "Skjema_ArchiveReference" });
            DropIndex("dbo.FormMetadata", new[] { "DistributionService_ArchiveReference" });
            CreateTable(
                "dbo.DistributionForms",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        InitialArchiveReference = c.String(),
                        SubmitAndInstantiatePrefilledFormTaskReceiptId = c.String(),
                        ExternalSystemReference = c.String(),
                        SignedArchiveReference = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            DropColumn("dbo.FormMetadata", "ReferanseIdSendt");
            DropColumn("dbo.FormMetadata", "ReferanseIdSignert");
            DropColumn("dbo.FormMetadata", "Ansvarsomraade");
            DropColumn("dbo.FormMetadata", "AnsvarligSoeker");
            DropColumn("dbo.FormMetadata", "Discriminator");
            DropColumn("dbo.FormMetadata", "DistributionService_ArchiveReference");
            DropTable("dbo.EventLogs");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.EventLogs",
                c => new
                    {
                        Value = c.String(nullable: false, maxLength: 128),
                        Description = c.String(),
                        LastUpdated = c.DateTime(nullable: false),
                        Skjema_ArchiveReference = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Value);
            
            AddColumn("dbo.FormMetadata", "DistributionService_ArchiveReference", c => c.String(maxLength: 128));
            AddColumn("dbo.FormMetadata", "Discriminator", c => c.String(nullable: false, maxLength: 128));
            AddColumn("dbo.FormMetadata", "AnsvarligSoeker", c => c.String());
            AddColumn("dbo.FormMetadata", "Ansvarsomraade", c => c.String());
            AddColumn("dbo.FormMetadata", "ReferanseIdSignert", c => c.String());
            AddColumn("dbo.FormMetadata", "ReferanseIdSendt", c => c.String());
            DropTable("dbo.DistributionForms");
            CreateIndex("dbo.FormMetadata", "DistributionService_ArchiveReference");
            CreateIndex("dbo.EventLogs", "Skjema_ArchiveReference");
            AddForeignKey("dbo.EventLogs", "Skjema_ArchiveReference", "dbo.FormMetadata", "ArchiveReference");
            AddForeignKey("dbo.FormMetadata", "DistributionService_ArchiveReference", "dbo.FormMetadata", "ArchiveReference");
        }
    }
}
