namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Indexing_archivereferences_used_by_statusAPI : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.LogEntries", new[] { "ArchiveReference" });
            AlterColumn("dbo.DistributionForms", "InitialArchiveReference", c => c.String(maxLength: 20));
            AlterColumn("dbo.DistributionForms", "SignedArchiveReference", c => c.String(maxLength: 20));
            AlterColumn("dbo.FileDownloadStatus", "ArchiveReference", c => c.String(maxLength: 20));
            CreateIndex("dbo.DistributionForms", "InitialArchiveReference");
            CreateIndex("dbo.DistributionForms", "SignedArchiveReference");
            CreateIndex("dbo.FileDownloadStatus", "ArchiveReference");
            CreateIndex("dbo.LogEntries", "ArchiveReference");
        }
        
        public override void Down()
        {
            DropIndex("dbo.LogEntries", new[] { "ArchiveReference" });
            DropIndex("dbo.FileDownloadStatus", new[] { "ArchiveReference" });
            DropIndex("dbo.DistributionForms", new[] { "SignedArchiveReference" });
            DropIndex("dbo.DistributionForms", new[] { "InitialArchiveReference" });
            AlterColumn("dbo.FileDownloadStatus", "ArchiveReference", c => c.String());
            AlterColumn("dbo.DistributionForms", "SignedArchiveReference", c => c.String());
            AlterColumn("dbo.DistributionForms", "InitialArchiveReference", c => c.String());
            CreateIndex("dbo.LogEntries", "ArchiveReference");
        }
    }
}
