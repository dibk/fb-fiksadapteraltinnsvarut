﻿namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFtBIdTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.FtBIds",
                c => new
                    {
                        EAN = c.String(nullable: false, maxLength: 18),
                        ProjectNo = c.String(),
                        ProjectName = c.String(),
                        System = c.String(nullable: false),
                        Distributed = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.EAN);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.FtBIds");
        }
    }
}
