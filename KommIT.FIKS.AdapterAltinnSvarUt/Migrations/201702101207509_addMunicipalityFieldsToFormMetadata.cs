namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addMunicipalityFieldsToFormMetadata : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FormMetadata", "MunicipalityArchiveCaseYear", c => c.Int(nullable: false));
            AddColumn("dbo.FormMetadata", "MunicipalityArchiveCaseSequence", c => c.Long(nullable: false));
            AddColumn("dbo.FormMetadata", "MunicipalityPublicArchiveCaseUrl", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.FormMetadata", "MunicipalityPublicArchiveCaseUrl");
            DropColumn("dbo.FormMetadata", "MunicipalityArchiveCaseSequence");
            DropColumn("dbo.FormMetadata", "MunicipalityArchiveCaseYear");
        }
    }
}
