﻿namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddClientSystemTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ClientSystems",
                c => new
                    {
                        SystemKey = c.String(nullable: false, maxLength: 128),
                        DisplayName = c.String(nullable: false),
                        Description = c.String(),
                        Identifiers = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.SystemKey);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.ClientSystems");
        }
    }
}
