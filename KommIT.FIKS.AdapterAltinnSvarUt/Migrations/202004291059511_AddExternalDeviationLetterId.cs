namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddExternalDeviationLetterId : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DeviationLetters", "ExternalDeviationLetterId", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.DeviationLetters", "ExternalDeviationLetterId");
        }
    }
}
