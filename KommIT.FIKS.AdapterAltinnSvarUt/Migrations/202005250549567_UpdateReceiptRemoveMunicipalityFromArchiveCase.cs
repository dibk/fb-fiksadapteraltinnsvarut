namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateReceiptRemoveMunicipalityFromArchiveCase : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Receipts", "ArchiveCaseYear", c => c.Int(nullable: false));
            AddColumn("dbo.Receipts", "ArchiveCaseSequence", c => c.Long(nullable: false));
            AddColumn("dbo.Receipts", "PublicArchiveCaseUrl", c => c.String());
            DropColumn("dbo.Receipts", "MunicipalityArchiveCaseYear");
            DropColumn("dbo.Receipts", "MunicipalityArchiveCaseSequence");
            DropColumn("dbo.Receipts", "MunicipalityPublicArchiveCaseUrl");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Receipts", "MunicipalityPublicArchiveCaseUrl", c => c.String());
            AddColumn("dbo.Receipts", "MunicipalityArchiveCaseSequence", c => c.Long(nullable: false));
            AddColumn("dbo.Receipts", "MunicipalityArchiveCaseYear", c => c.Int(nullable: false));
            DropColumn("dbo.Receipts", "PublicArchiveCaseUrl");
            DropColumn("dbo.Receipts", "ArchiveCaseSequence");
            DropColumn("dbo.Receipts", "ArchiveCaseYear");
        }
    }
}
