namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addFileDownloadStatus : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.FileDownloadStatus",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ArchiveReference = c.String(),
                        Guid = c.Guid(nullable: false),
                        FormType = c.Int(nullable: false),
                        FileType = c.Int(nullable: false),
                        Filename = c.String(),
                        BlobLink = c.String(),
                        FileAccessCount = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.FileDownloadStatus");
        }
    }
}
