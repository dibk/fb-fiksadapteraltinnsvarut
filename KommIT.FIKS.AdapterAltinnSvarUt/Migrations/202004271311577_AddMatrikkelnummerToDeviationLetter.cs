namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddMatrikkelnummerToDeviationLetter : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DeviationLetters", "MatrikkelNumber", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.DeviationLetters", "MatrikkelNumber");
        }
    }
}
