namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EnterpriseTerms_field_added : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Activities", "EnterpriseTerms", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Activities", "EnterpriseTerms");
        }
    }
}
