// <auto-generated />
namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class IndexesOnDistributionStatusAndDisributonReference : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(IndexesOnDistributionStatusAndDisributonReference));
        
        string IMigrationMetadata.Id
        {
            get { return "202103262027137_IndexesOnDistributionStatusAndDisributonReference"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
