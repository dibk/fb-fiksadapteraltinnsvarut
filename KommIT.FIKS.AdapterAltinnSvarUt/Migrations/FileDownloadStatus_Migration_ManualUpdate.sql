UPDATE FileDownloadStatus SET ContainerName = LOWER(Guid) WHERE ContainerName IS null;

UPDATE FileDownloadStatus SET FtpbFileType = CASE FileType 
                    WHEN 0 THEN 'SkjemaPdf'
                    WHEN 1 THEN 'MaskinlesbarXml'
                    WHEN 2 THEN 'SvarPaaNabovarselVedlegg'
                    WHEN 3 THEN 'KvitteringNabovarsel'
                    WHEN 4 THEN 'Nabovarsel'
                    WHEN 5 THEN 'Avkjoerselsplan'
                    WHEN 6 THEN 'Situasjonskart'
                    WHEN 7 THEN 'Situasjonsplan'
                    WHEN 8 THEN 'TegningEksisterendeFasade'
                    WHEN 9 THEN 'RedegjoerelseGrunnforhold'
                    WHEN 10 THEN 'ErklaeringAnsvarsrett'
                    WHEN 11 THEN 'Gjennomfoeringsplan'
                    WHEN 12 THEN 'BoligspesifikasjonMatrikkel'
                    WHEN 13 THEN 'UttalelseVedtakAnnenOffentligMyndighet'
                    WHEN 14 THEN 'Annet'
                    WHEN 15 THEN 'Dispensasjonssoeknad'
                    WHEN 16 THEN 'RekvisisjonAvOppmaalingsforretning'
                    WHEN 17 THEN 'SluttrapportForBygningsavfall'
                    WHEN 18 THEN 'Foto'
                    WHEN 19 THEN 'Kart'
                    WHEN 20 THEN 'KvitteringInnleveringAvfall'
                    WHEN 21 THEN 'KommentarNabomerknader'
                    WHEN 22 THEN 'Nabomerknader'
                    WHEN 23 THEN 'Organisasjonsplan'
                    WHEN 24 THEN 'RedegjoerelseEstetikk'
                    WHEN 25 THEN 'RedegjoerelseForurensetGrunn'
                    WHEN 26 THEN 'RedegjoerelseAndreNaturMiljoeforhold'
                    WHEN 27 THEN 'RedegjoerelseSkredOgFlom'
                    WHEN 28 THEN 'Revisjonserklaering'
                    WHEN 29 THEN 'TegningEksisterendePlan'
                    WHEN 30 THEN 'TegningEksisterendeSnitt'
                    WHEN 31 THEN 'TegningNyFasade'
                    WHEN 32 THEN 'TegningNyPlan'
                    WHEN 33 THEN 'TegningNyttSnitt'
                    WHEN 34 THEN 'AvklaringVA'
                    WHEN 35 THEN 'ByggesaksBIM'
                    WHEN 36 THEN 'AvklaringHoyspent'
                    WHEN 37 THEN 'UttalelseKulturminnemyndighet'
                    WHEN 38 THEN 'SamtykkeArbeidstilsynet'
                    WHEN 39 THEN 'AvkjoeringstillatelseVegmyndighet'
                    WHEN 40 THEN 'RedegjoerelseUnntakTEK'
                    WHEN 41 THEN 'UnderlagUtnytting'
                    WHEN 42 THEN 'Folgebrev'
                    WHEN 43 THEN 'KvitteringForLevertAvfall'
                    WHEN 44 THEN 'OpphoerAnsvarsrett'
                    WHEN 45 THEN 'Utomhusplan'
                    WHEN 46 THEN 'SamtykkePlassering'
                    WHEN 47 THEN 'PlanForUavhengigKontroll'
                    WHEN 48 THEN 'Soknad'
                    WHEN 49 THEN 'BeskrivelseTypeArbeidProsess'
                    WHEN 50 THEN 'Arbeidsmiljofaktorer'
                    WHEN 51 THEN 'AnsattesMedvirkning'
                    WHEN 52 THEN 'ArbeidsgiversMedvirkning'
                    WHEN 53 THEN 'ForUtleiebygg'
                    WHEN 54 THEN 'BistandFraBHT'
                    WHEN 55 THEN 'Ventilasjon'
                    WHEN 56 THEN 'AnsvarsrettSelvbygger'
                    WHEN 57 THEN 'KartPlanavgrensing'
                    WHEN 58 THEN 'ReferatOppstartsmoete'
                    WHEN 59 THEN 'Planinitiativ'
                    WHEN 60 THEN 'KartDetaljert'
                    WHEN 61 THEN 'Planprogram'
                    WHEN 62 THEN 'TiltakshaverSignatur'
                    WHEN 63 THEN 'ArbeidstakersRepresentant'
                    WHEN 64 THEN 'LeietakerArbeidsgiver'
                    WHEN 65 THEN 'Verneombud'
                    WHEN 66 THEN 'Arbeidsmiljoutvalg'
                    WHEN 67 THEN 'Bedriftshelsetjeneste'
                    WHEN 68 THEN 'AndreRedegjoerelser'
                    WHEN 69 THEN 'BerorteParterXML'
                    WHEN 70 THEN 'SoknadOmAnsvarsrettTiltaksklasse1'
                    WHEN 71 THEN 'Miljoesaneringsbeskrivelse'
                    WHEN 72 THEN 'MaskinlesbarXmlHoeringsmyndigheter'
                    WHEN 73 THEN 'SkjemaPdfHoeringsmyndigheter'
                    WHEN 74 THEN 'SvarPaaVarselOppstartHoeringsmyndigheterVedlegg'
                    WHEN 75 THEN 'MaskinlesbarXmlHoeringOgOffentligEttersyn'
                    WHEN 76 THEN 'SkjemaPdfHoeringOgOffentligEttersyn'
                    WHEN 77 THEN 'SvarPaaHoeringOgOffentligEttersynVedlegg'
                    WHEN 78 THEN 'DistribusjonHoeringOgOffentligEttersynKvittering'
                    WHEN 79 THEN 'KommentarHoeringOgOffentligEttersynKvittering'
                    WHEN 80 THEN 'VarselTilHoeringsmyndigheterKvittering'
                    WHEN 81 THEN 'VarselTilHoeringsmyndigheter'
                    WHEN 82 THEN 'VarselTilBeroerteParterKvittering'
                    WHEN 83 THEN 'VarselTilBeroerteParter'
                    WHEN 84 THEN 'InnsendingAvReguleringsplanforslag'
                    WHEN 85 THEN 'InnsendingAvReguleringsplanforslagKvittering'
                    WHEN 86 THEN 'RapportVarselOppstartPlanarbeid'
                    WHEN 87 THEN 'Planvarsel'
                    WHEN 88 THEN 'Planuttalelse'
                    WHEN 89 THEN 'UttalelseVedlegg'
                    WHEN 90 THEN 'PlanvarselKvittering'
                    WHEN 91 THEN 'SoeknadOmIgangsettingstillatelse'
                    WHEN 92 THEN 'SoeknadOmIgangsettingstillatelseKvittering'
                END
                WHERE FtpbFileType IS null;