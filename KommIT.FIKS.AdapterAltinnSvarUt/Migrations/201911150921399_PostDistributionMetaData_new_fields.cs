namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PostDistributionMetaData_new_fields : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PostDistributionMetaDatas", "DateCreated", c => c.DateTime(nullable: false));
            AddColumn("dbo.PostDistributionMetaDatas", "DateChanged", c => c.DateTime(nullable: false));
            AddColumn("dbo.PostDistributionMetaDatas", "NotificationStatus", c => c.String());
            AddColumn("dbo.PostDistributionMetaDatas", "NotificationChannel", c => c.String());

            Sql("UPDATE dbo.PostDistributionMetaDatas SET DateCreated = GetDate() WHERE DateCreated = '1900-01-01 00:00:00.000'");
            Sql("UPDATE dbo.PostDistributionMetaDatas SET DateChanged = GetDate() WHERE DateChanged = '1900-01-01 00:00:00.000'");
        }
        
        public override void Down()
        {
            DropColumn("dbo.PostDistributionMetaDatas", "NotificationChannel");
            DropColumn("dbo.PostDistributionMetaDatas", "NotificationStatus");
            DropColumn("dbo.PostDistributionMetaDatas", "DateChanged");
            DropColumn("dbo.PostDistributionMetaDatas", "DateCreated");
        }
    }
}
