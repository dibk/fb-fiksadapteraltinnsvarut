﻿namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RenameFtBIdToFtId : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.FtBIds", newName: "FtIds");
            DropIndex("dbo.FormWorkflowLogs", new[] { "FtbId" });
            AddColumn("dbo.FormWorkflowLogs", "FtId", c => c.String(maxLength: 80));
            CreateIndex("dbo.FormWorkflowLogs", "FtId");
            DropColumn("dbo.FormWorkflowLogs", "FtbId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.FormWorkflowLogs", "FtbId", c => c.String(maxLength: 80));
            DropIndex("dbo.FormWorkflowLogs", new[] { "FtId" });
            DropColumn("dbo.FormWorkflowLogs", "FtId");
            CreateIndex("dbo.FormWorkflowLogs", "FtbId");
            RenameTable(name: "dbo.FtIds", newName: "FtBIds");
        }
    }
}
