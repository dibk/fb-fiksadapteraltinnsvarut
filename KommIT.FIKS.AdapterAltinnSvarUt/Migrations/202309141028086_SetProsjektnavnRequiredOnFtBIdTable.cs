﻿namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SetProsjektnavnRequiredOnFtBIdTable : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.FtBIds", "Prosjektnavn", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.FtBIds", "Prosjektnavn", c => c.String());
        }
    }
}
