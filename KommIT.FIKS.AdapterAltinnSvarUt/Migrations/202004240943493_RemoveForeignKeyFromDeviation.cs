namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveForeignKeyFromDeviation : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Deviations", "DeviationId", "dbo.DeviationLetters");
            DropIndex("dbo.Deviations", new[] { "DeviationId" });
            DropColumn("dbo.Deviations", "DeviationId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Deviations", "DeviationId", c => c.Long(nullable: false));
            CreateIndex("dbo.Deviations", "DeviationId");
            AddForeignKey("dbo.Deviations", "DeviationId", "dbo.DeviationLetters", "DeviationId", cascadeDelete: true);
        }
    }
}
