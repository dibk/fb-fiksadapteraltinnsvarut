namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddForeignKeyMunicipality : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.LogEntries", name: "LogMunicipality_Code", newName: "MunicipalityCode");
            RenameIndex(table: "dbo.LogEntries", name: "IX_LogMunicipality_Code", newName: "IX_MunicipalityCode");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.LogEntries", name: "IX_MunicipalityCode", newName: "IX_LogMunicipality_Code");
            RenameColumn(table: "dbo.LogEntries", name: "MunicipalityCode", newName: "LogMunicipality_Code");
        }
    }
}
