namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TiltakshaversSamtykkeTiltaktype : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TiltakshaversSamtykkeTiltaktypes",
                c => new
                    {
                        SamtykkeTiltaktypeID = c.Int(nullable: false, identity: true),
                        kodeverdi = c.String(),
                        kodebeskrivelse = c.String(),
                    })
                .PrimaryKey(t => t.SamtykkeTiltaktypeID);
            
            CreateTable(
                "dbo.TiltakshaversSamtikkeTSTiltaktypes",
                c => new
                    {
                        TiltakshaversSamtykkeId = c.Guid(nullable: false),
                        SamtykkeTiltaktypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.TiltakshaversSamtykkeId, t.SamtykkeTiltaktypeId })
                .ForeignKey("dbo.TiltakshaversSamtykkes", t => t.TiltakshaversSamtykkeId, cascadeDelete: true)
                .ForeignKey("dbo.TiltakshaversSamtykkeTiltaktypes", t => t.SamtykkeTiltaktypeId, cascadeDelete: true)
                .Index(t => t.TiltakshaversSamtykkeId)
                .Index(t => t.SamtykkeTiltaktypeId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TiltakshaversSamtikkeTSTiltaktypes", "SamtykkeTiltaktypeId", "dbo.TiltakshaversSamtykkeTiltaktypes");
            DropForeignKey("dbo.TiltakshaversSamtikkeTSTiltaktypes", "TiltakshaversSamtykkeId", "dbo.TiltakshaversSamtykkes");
            DropIndex("dbo.TiltakshaversSamtikkeTSTiltaktypes", new[] { "SamtykkeTiltaktypeId" });
            DropIndex("dbo.TiltakshaversSamtikkeTSTiltaktypes", new[] { "TiltakshaversSamtykkeId" });
            DropTable("dbo.TiltakshaversSamtikkeTSTiltaktypes");
            DropTable("dbo.TiltakshaversSamtykkeTiltaktypes");
        }
    }
}
