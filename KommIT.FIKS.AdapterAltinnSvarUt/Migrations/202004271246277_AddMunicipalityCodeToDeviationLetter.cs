namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddMunicipalityCodeToDeviationLetter : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DeviationLetters", "MunicipalityCode", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.DeviationLetters", "MunicipalityCode");
        }
    }
}
