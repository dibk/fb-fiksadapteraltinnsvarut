﻿namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MangelId_til_string : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Deviations", "ExternalDeviationId", c => c.String());
            AlterColumn("dbo.DeviationLetters", "AltinnArchiveReference", c => c.String(maxLength: 40));
            CreateIndex("dbo.DeviationLetters", "AltinnArchiveReference");
        }
        
        public override void Down()
        {
            DropIndex("dbo.DeviationLetters", new[] { "AltinnArchiveReference" });
            AlterColumn("dbo.Deviations", "ExternalDeviationId", c => c.Guid());
            AlterColumn("dbo.DeviationLetters", "AltinnArchiveReference", c => c.String());
        }
    }
}
