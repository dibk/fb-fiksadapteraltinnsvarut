namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TiltakshaverSignaturAddOrganisasjonsnummer : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TiltakshaversSamtykkes", "AnsvarligSoekerOrganisasjonsnummer", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.TiltakshaversSamtykkes", "AnsvarligSoekerOrganisasjonsnummer");
        }
    }
}
