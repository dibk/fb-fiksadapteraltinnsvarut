namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Classification : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MunicipalityClassifications",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Sorting = c.String(),
                        OrganizingPrinciple = c.String(),
                        OrganizingValue = c.String(),
                        MunicipalityCode = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Municipalities", t => t.MunicipalityCode, cascadeDelete: true)
                .Index(t => t.MunicipalityCode);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MunicipalityClassifications", "MunicipalityCode", "dbo.Municipalities");
            DropIndex("dbo.MunicipalityClassifications", new[] { "MunicipalityCode" });
            DropTable("dbo.MunicipalityClassifications");
        }
    }
}
