namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_distributionType : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DistributionForms", "DistributionType", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.DistributionForms", "DistributionType");
        }
    }
}
