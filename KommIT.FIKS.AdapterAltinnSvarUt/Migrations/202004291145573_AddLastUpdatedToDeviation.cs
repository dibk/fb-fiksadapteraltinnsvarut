namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddLastUpdatedToDeviation : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Deviations", "LastUpdated", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Deviations", "LastUpdated");
        }
    }
}
