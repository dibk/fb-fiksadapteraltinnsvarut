namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class kommitadapteraltinnsvarut : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.LogEntries", "Type", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.LogEntries", "Type");
        }
    }
}
