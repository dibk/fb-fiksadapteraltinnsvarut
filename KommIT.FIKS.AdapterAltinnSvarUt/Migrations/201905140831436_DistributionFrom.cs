namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DistributionFrom : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DistributionForms", "DistributionReference", c => c.Guid(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.DistributionForms", "DistributionReference");
        }
    }
}
