﻿namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FormMetadata_slettet_column : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FormMetadata", "Slettet", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.FormMetadata", "Slettet");
        }
    }
}
