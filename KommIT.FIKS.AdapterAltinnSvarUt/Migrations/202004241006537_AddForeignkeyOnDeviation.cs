namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddForeignkeyOnDeviation : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.Deviations");
            AddColumn("dbo.Deviations", "DeviationId", c => c.Guid(nullable: false));
            AddColumn("dbo.Deviations", "DeviationLetterId", c => c.Guid(nullable: false));
            AddPrimaryKey("dbo.Deviations", "DeviationId");
            CreateIndex("dbo.Deviations", "DeviationLetterId");
            AddForeignKey("dbo.Deviations", "DeviationLetterId", "dbo.DeviationLetters", "DeviationLetterId", cascadeDelete: true);
            DropColumn("dbo.Deviations", "Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Deviations", "Id", c => c.Long(nullable: false, identity: true));
            DropForeignKey("dbo.Deviations", "DeviationLetterId", "dbo.DeviationLetters");
            DropIndex("dbo.Deviations", new[] { "DeviationLetterId" });
            DropPrimaryKey("dbo.Deviations");
            DropColumn("dbo.Deviations", "DeviationLetterId");
            DropColumn("dbo.Deviations", "DeviationId");
            AddPrimaryKey("dbo.Deviations", "Id");
        }
    }
}
