namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RenameSchemaMetadataToFormMetadata : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.SchemaMetadatas", newName: "FormMetadata");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.FormMetadata", newName: "SchemaMetadatas");
        }
    }
}
