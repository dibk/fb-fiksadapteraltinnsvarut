﻿namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FileDownloadStatus_nyeKolonner : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FileDownloadStatus", "ContainerName", c => c.String(maxLength: 80));
            AddColumn("dbo.FileDownloadStatus", "FtpbFileType", c => c.String(maxLength: 80));
            CreateIndex("dbo.FileDownloadStatus", "ContainerName");
            CreateIndex("dbo.FileDownloadStatus", "FtpbFileType");
        }
        
        public override void Down()
        {
            DropIndex("dbo.FileDownloadStatus", new[] { "FtpbFileType" });
            DropIndex("dbo.FileDownloadStatus", new[] { "ContainerName" });
            DropColumn("dbo.FileDownloadStatus", "FtpbFileType");
            DropColumn("dbo.FileDownloadStatus", "ContainerName");
        }
    }
}
