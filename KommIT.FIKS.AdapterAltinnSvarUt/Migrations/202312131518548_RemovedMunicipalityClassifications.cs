﻿namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemovedMunicipalityClassifications : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.MunicipalityClassifications", "MunicipalityCode", "dbo.Municipalities");
            DropIndex("dbo.MunicipalityClassifications", new[] { "MunicipalityCode" });
            DropTable("dbo.MunicipalityClassifications");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.MunicipalityClassifications",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Sorting = c.Int(nullable: false),
                        OrganizingPrinciple = c.String(),
                        OrganizingValue = c.String(),
                        MunicipalityCode = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateIndex("dbo.MunicipalityClassifications", "MunicipalityCode");
            AddForeignKey("dbo.MunicipalityClassifications", "MunicipalityCode", "dbo.Municipalities", "Code", cascadeDelete: true);
        }
    }
}
