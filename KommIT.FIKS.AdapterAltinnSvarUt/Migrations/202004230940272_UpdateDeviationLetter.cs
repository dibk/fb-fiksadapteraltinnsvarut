namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateDeviationLetter : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DeviationLetters", "Title", c => c.String());
            AddColumn("dbo.Deviations", "DeviationDescription", c => c.String());
            AddColumn("dbo.Deviations", "TermsId", c => c.String());
            AddColumn("dbo.Deviations", "TiltakId", c => c.String());
            AddColumn("dbo.Deviations", "Title", c => c.String());
            DropColumn("dbo.DeviationLetters", "Message");
        }
        
        public override void Down()
        {
            AddColumn("dbo.DeviationLetters", "Message", c => c.String());
            DropColumn("dbo.Deviations", "Title");
            DropColumn("dbo.Deviations", "TiltakId");
            DropColumn("dbo.Deviations", "TermsId");
            DropColumn("dbo.Deviations", "DeviationDescription");
            DropColumn("dbo.DeviationLetters", "Title");
        }
    }
}
