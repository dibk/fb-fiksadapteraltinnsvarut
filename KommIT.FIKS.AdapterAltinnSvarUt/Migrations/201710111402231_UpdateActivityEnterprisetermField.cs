namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateActivityEnterprisetermField : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.EnterpriseTerms",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(),
                        ActivityId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Activities", t => t.ActivityId, cascadeDelete: true)
                .Index(t => t.ActivityId);
            
            DropColumn("dbo.Activities", "EnterpriseTerms");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Activities", "EnterpriseTerms", c => c.String());
            DropForeignKey("dbo.EnterpriseTerms", "ActivityId", "dbo.Activities");
            DropIndex("dbo.EnterpriseTerms", new[] { "ActivityId" });
            DropTable("dbo.EnterpriseTerms");
        }
    }
}
