namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddNewLogTables : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.LogEntryPrefills", "LogEntryId", "dbo.LogEntries");
            DropForeignKey("dbo.LogEntryDistributionStatus", "InternalId", "dbo.LogEntryPrefills");
            DropIndex("dbo.LogEntryPrefills", new[] { "LogEntryId" });
            DropIndex("dbo.LogEntryDistributionStatus", new[] { "InternalId" });
            CreateTable(
                "dbo.Status",
                c => new
                    {
                        State = c.String(nullable: false, maxLength: 128),
                        Description = c.String(),
                        Timestamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.State);
            
            AddColumn("dbo.FormMetadata", "VaarReferanse", c => c.String());
            AddColumn("dbo.FormMetadata", "Ansvarsomraade", c => c.String());
            AddColumn("dbo.FormMetadata", "Discriminator", c => c.String(nullable: false, maxLength: 128));
            AddColumn("dbo.FormMetadata", "Status_State", c => c.String(maxLength: 128));
            AddColumn("dbo.FormMetadata", "DistributionService_ArchiveReference", c => c.String(maxLength: 128));
            CreateIndex("dbo.FormMetadata", "Status_State");
            CreateIndex("dbo.FormMetadata", "DistributionService_ArchiveReference");
            AddForeignKey("dbo.FormMetadata", "Status_State", "dbo.Status", "State");
            AddForeignKey("dbo.FormMetadata", "DistributionService_ArchiveReference", "dbo.FormMetadata", "ArchiveReference");
            DropTable("dbo.LogEntryPrefills");
            DropTable("dbo.LogEntryDistributionStatus");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.LogEntryDistributionStatus",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Status = c.String(),
                        Timestamp = c.DateTime(nullable: false),
                        InternalId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.LogEntryPrefills",
                c => new
                    {
                        InternalId = c.String(nullable: false, maxLength: 128),
                        Foretak = c.String(),
                        OrgNr = c.String(),
                        Ansvarsomraade = c.String(),
                        AltinnServiceReference = c.String(),
                        ArchiveReferencePrefill = c.String(),
                        VaarReferanse = c.String(),
                        Timestamp = c.DateTime(nullable: false),
                        LogEntryId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.InternalId);
            
            DropForeignKey("dbo.FormMetadata", "DistributionService_ArchiveReference", "dbo.FormMetadata");
            DropForeignKey("dbo.FormMetadata", "Status_State", "dbo.Status");
            DropIndex("dbo.FormMetadata", new[] { "DistributionService_ArchiveReference" });
            DropIndex("dbo.FormMetadata", new[] { "Status_State" });
            DropColumn("dbo.FormMetadata", "DistributionService_ArchiveReference");
            DropColumn("dbo.FormMetadata", "Status_State");
            DropColumn("dbo.FormMetadata", "Discriminator");
            DropColumn("dbo.FormMetadata", "Ansvarsomraade");
            DropColumn("dbo.FormMetadata", "VaarReferanse");
            DropTable("dbo.Status");
            CreateIndex("dbo.LogEntryDistributionStatus", "InternalId");
            CreateIndex("dbo.LogEntryPrefills", "LogEntryId");
            AddForeignKey("dbo.LogEntryDistributionStatus", "InternalId", "dbo.LogEntryPrefills", "InternalId", cascadeDelete: true);
            AddForeignKey("dbo.LogEntryPrefills", "LogEntryId", "dbo.LogEntries", "Id", cascadeDelete: true);
        }
    }
}
