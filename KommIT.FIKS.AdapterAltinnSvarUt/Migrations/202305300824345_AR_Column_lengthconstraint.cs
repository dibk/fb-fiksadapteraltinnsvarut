﻿namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AR_Column_lengthconstraint : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.DistributionForms", new[] { "InitialArchiveReference" });
            DropIndex("dbo.DistributionForms", new[] { "SignedArchiveReference" });
            DropIndex("dbo.Decisions", new[] { "ArchiveReference" });
            DropIndex("dbo.DeviationLetters", new[] { "AltinnArchiveReference" });
            DropIndex("dbo.FileDownloadStatus", new[] { "ArchiveReference" });
            DropIndex("dbo.FormWorkflowLogs", new[] { "FtbId" });
            DropPrimaryKey("dbo.FormWorkflowLogs");
            AlterColumn("dbo.DistributionForms", "InitialArchiveReference", c => c.String(maxLength: 80));
            AlterColumn("dbo.DistributionForms", "SignedArchiveReference", c => c.String(maxLength: 80));
            AlterColumn("dbo.Decisions", "ArchiveReference", c => c.String(maxLength: 80));
            AlterColumn("dbo.DeviationLetters", "AltinnArchiveReference", c => c.String(maxLength: 80));
            AlterColumn("dbo.FileDownloadStatus", "ArchiveReference", c => c.String(maxLength: 80));
            AlterColumn("dbo.FormWorkflowLogs", "ArchicveReference", c => c.String(nullable: false, maxLength: 80));
            AlterColumn("dbo.FormWorkflowLogs", "FtbId", c => c.String(maxLength: 80));
            AddPrimaryKey("dbo.FormWorkflowLogs", "ArchicveReference");
            CreateIndex("dbo.DistributionForms", "InitialArchiveReference");
            CreateIndex("dbo.DistributionForms", "SignedArchiveReference");
            CreateIndex("dbo.Decisions", "ArchiveReference");
            CreateIndex("dbo.DeviationLetters", "AltinnArchiveReference");
            CreateIndex("dbo.FileDownloadStatus", "ArchiveReference");
            CreateIndex("dbo.FormWorkflowLogs", "FtbId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.FormWorkflowLogs", new[] { "FtbId" });
            DropIndex("dbo.FileDownloadStatus", new[] { "ArchiveReference" });
            DropIndex("dbo.DeviationLetters", new[] { "AltinnArchiveReference" });
            DropIndex("dbo.Decisions", new[] { "ArchiveReference" });
            DropIndex("dbo.DistributionForms", new[] { "SignedArchiveReference" });
            DropIndex("dbo.DistributionForms", new[] { "InitialArchiveReference" });
            DropPrimaryKey("dbo.FormWorkflowLogs");
            AlterColumn("dbo.FormWorkflowLogs", "FtbId", c => c.String(maxLength: 40));
            AlterColumn("dbo.FormWorkflowLogs", "ArchicveReference", c => c.String(nullable: false, maxLength: 40));
            AlterColumn("dbo.FileDownloadStatus", "ArchiveReference", c => c.String(maxLength: 40));
            AlterColumn("dbo.DeviationLetters", "AltinnArchiveReference", c => c.String(maxLength: 40));
            AlterColumn("dbo.Decisions", "ArchiveReference", c => c.String(maxLength: 40));
            AlterColumn("dbo.DistributionForms", "SignedArchiveReference", c => c.String(maxLength: 40));
            AlterColumn("dbo.DistributionForms", "InitialArchiveReference", c => c.String(maxLength: 40));
            AddPrimaryKey("dbo.FormWorkflowLogs", "ArchicveReference");
            CreateIndex("dbo.FormWorkflowLogs", "FtbId");
            CreateIndex("dbo.FileDownloadStatus", "ArchiveReference");
            CreateIndex("dbo.DeviationLetters", "AltinnArchiveReference");
            CreateIndex("dbo.Decisions", "ArchiveReference");
            CreateIndex("dbo.DistributionForms", "SignedArchiveReference");
            CreateIndex("dbo.DistributionForms", "InitialArchiveReference");
        }
    }
}
