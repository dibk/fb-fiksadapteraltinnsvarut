namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateApiAuthenticationPK : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.APIAuthentications", new[] { "UserId" });
            DropPrimaryKey("dbo.APIAuthentications");
            AlterColumn("dbo.APIAuthentications", "Username", c => c.String());
            AlterColumn("dbo.APIAuthentications", "UserId", c => c.String(nullable: false, maxLength: 128));
            AddPrimaryKey("dbo.APIAuthentications", "UserId");
            CreateIndex("dbo.APIAuthentications", "UserId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.APIAuthentications", new[] { "UserId" });
            DropPrimaryKey("dbo.APIAuthentications");
            AlterColumn("dbo.APIAuthentications", "UserId", c => c.String(maxLength: 128));
            AlterColumn("dbo.APIAuthentications", "Username", c => c.String(nullable: false, maxLength: 128));
            AddPrimaryKey("dbo.APIAuthentications", "Username");
            CreateIndex("dbo.APIAuthentications", "UserId");
        }
    }
}
