namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCascadeDeleteOnActivity : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Actions", "Activity_Id", "dbo.Activities");
            DropForeignKey("dbo.LawReferances", "Activity_Id", "dbo.Activities");
            DropIndex("dbo.Actions", new[] { "Activity_Id" });
            DropIndex("dbo.LawReferances", new[] { "Activity_Id" });
            RenameColumn(table: "dbo.Actions", name: "Activity_Id", newName: "ActivityId");
            RenameColumn(table: "dbo.LawReferances", name: "Activity_Id", newName: "ActivityId");
            AlterColumn("dbo.Actions", "ActivityId", c => c.Int(nullable: false));
            AlterColumn("dbo.LawReferances", "ActivityId", c => c.Int(nullable: false));
            CreateIndex("dbo.Actions", "ActivityId");
            CreateIndex("dbo.LawReferances", "ActivityId");
            AddForeignKey("dbo.Actions", "ActivityId", "dbo.Activities", "Id", cascadeDelete: true);
            AddForeignKey("dbo.LawReferances", "ActivityId", "dbo.Activities", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.LawReferances", "ActivityId", "dbo.Activities");
            DropForeignKey("dbo.Actions", "ActivityId", "dbo.Activities");
            DropIndex("dbo.LawReferances", new[] { "ActivityId" });
            DropIndex("dbo.Actions", new[] { "ActivityId" });
            AlterColumn("dbo.LawReferances", "ActivityId", c => c.Int());
            AlterColumn("dbo.Actions", "ActivityId", c => c.Int());
            RenameColumn(table: "dbo.LawReferances", name: "ActivityId", newName: "Activity_Id");
            RenameColumn(table: "dbo.Actions", name: "ActivityId", newName: "Activity_Id");
            CreateIndex("dbo.LawReferances", "Activity_Id");
            CreateIndex("dbo.Actions", "Activity_Id");
            AddForeignKey("dbo.LawReferances", "Activity_Id", "dbo.Activities", "Id");
            AddForeignKey("dbo.Actions", "Activity_Id", "dbo.Activities", "Id");
        }
    }
}
