namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFormMetadataFieldsForLog : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FormMetadata", "SvarUtStatus", c => c.String());
            AddColumn("dbo.FormMetadata", "ApplicantName", c => c.String());
            AddColumn("dbo.FormMetadata", "ApplicantAddress", c => c.String());
            AddColumn("dbo.FormMetadata", "PropertyFirstKnr", c => c.String());
            AddColumn("dbo.FormMetadata", "PropertyFirstGardsnr", c => c.String());
            AddColumn("dbo.FormMetadata", "PropertyFirstBruksnr", c => c.String());
            AddColumn("dbo.FormMetadata", "PropertyFirstFestenr", c => c.String());
            AddColumn("dbo.FormMetadata", "PropertyFirstSeksjonsnr", c => c.String());
            AddColumn("dbo.FormMetadata", "PropertyFirstAddress", c => c.String());
            AddColumn("dbo.FormMetadata", "FirstActionType", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.FormMetadata", "FirstActionType");
            DropColumn("dbo.FormMetadata", "PropertyFirstAddress");
            DropColumn("dbo.FormMetadata", "PropertyFirstSeksjonsnr");
            DropColumn("dbo.FormMetadata", "PropertyFirstFestenr");
            DropColumn("dbo.FormMetadata", "PropertyFirstBruksnr");
            DropColumn("dbo.FormMetadata", "PropertyFirstGardsnr");
            DropColumn("dbo.FormMetadata", "PropertyFirstKnr");
            DropColumn("dbo.FormMetadata", "ApplicantAddress");
            DropColumn("dbo.FormMetadata", "ApplicantName");
            DropColumn("dbo.FormMetadata", "SvarUtStatus");
        }
    }
}
