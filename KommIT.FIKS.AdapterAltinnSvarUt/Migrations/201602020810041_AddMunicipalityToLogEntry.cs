namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddMunicipalityToLogEntry : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.LogEntries", "LogMunicipality_Code", c => c.String(maxLength: 128));
            CreateIndex("dbo.LogEntries", "LogMunicipality_Code");
            AddForeignKey("dbo.LogEntries", "LogMunicipality_Code", "dbo.Municipalities", "Code");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.LogEntries", "LogMunicipality_Code", "dbo.Municipalities");
            DropIndex("dbo.LogEntries", new[] { "LogMunicipality_Code" });
            DropColumn("dbo.LogEntries", "LogMunicipality_Code");
        }
    }
}
