namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removedChecklist : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Actions", "ActivityId", "dbo.Activities");
            DropForeignKey("dbo.EnterpriseTerms", "ActivityId", "dbo.Activities");
            DropForeignKey("dbo.LawReferances", "ActivityId", "dbo.Activities");
            DropForeignKey("dbo.Activities", "Activity_Id", "dbo.Activities");
            DropIndex("dbo.Activities", new[] { "Activity_Id" });
            DropIndex("dbo.Actions", new[] { "ActivityId" });
            DropIndex("dbo.EnterpriseTerms", new[] { "ActivityId" });
            DropIndex("dbo.LawReferances", new[] { "ActivityId" });
            DropTable("dbo.Activities");
            DropTable("dbo.Actions");
            DropTable("dbo.EnterpriseTerms");
            DropTable("dbo.LawReferances");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.LawReferances",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LawReferanceDescription = c.String(),
                        LawReferanceUrl = c.String(),
                        ActivityId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.EnterpriseTerms",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(),
                        ActivityId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Actions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ActionValue = c.Boolean(nullable: false),
                        ActionType = c.String(),
                        ActionTypeCode = c.String(),
                        ActivityId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Activities",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ReferenceId = c.String(),
                        ParentReferenceId = c.String(),
                        Municipality = c.String(),
                        Name = c.String(),
                        ActivityType = c.String(),
                        Description = c.String(),
                        Category = c.String(),
                        Processcategory = c.String(),
                        Milestone = c.String(),
                        HasRule = c.Boolean(nullable: false),
                        Rule = c.String(),
                        Activity_Id = c.Int(),
                        ValidFrom = c.DateTime(),
                        ValidTo = c.DateTime(),
                        Updated = c.DateTime(nullable: false),
                        OrderNumber = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateIndex("dbo.LawReferances", "ActivityId");
            CreateIndex("dbo.EnterpriseTerms", "ActivityId");
            CreateIndex("dbo.Actions", "ActivityId");
            CreateIndex("dbo.Activities", "Activity_Id");
            AddForeignKey("dbo.Activities", "Activity_Id", "dbo.Activities", "Id");
            AddForeignKey("dbo.LawReferances", "ActivityId", "dbo.Activities", "Id", cascadeDelete: true);
            AddForeignKey("dbo.EnterpriseTerms", "ActivityId", "dbo.Activities", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Actions", "ActivityId", "dbo.Activities", "Id", cascadeDelete: true);
        }
    }
}
