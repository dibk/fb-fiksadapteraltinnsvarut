namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserNotification : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserNotifications",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ArchiveReference = c.String(),
                        MessageReference = c.String(),
                        FirstNotification = c.DateTime(nullable: false),
                        SecondNotification = c.DateTime(nullable: false),
                        EmailAdress = c.String(),
                        SMSText = c.String(),
                        EmailSubject = c.String(),
                        EmailBody = c.String(),
                        Status = c.Int(nullable: false),
                        NotificaitonReceipt = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.UserNotifications");
        }
    }
}
