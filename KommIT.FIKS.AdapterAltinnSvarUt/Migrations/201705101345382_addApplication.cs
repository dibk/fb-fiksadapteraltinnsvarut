namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addApplication : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FormMetadata", "Application", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.FormMetadata", "Application");
        }
    }
}
