// <auto-generated />
namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class update_distributionform : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(update_distributionform));
        
        string IMigrationMetadata.Id
        {
            get { return "201705161503342_update_distributionform"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
