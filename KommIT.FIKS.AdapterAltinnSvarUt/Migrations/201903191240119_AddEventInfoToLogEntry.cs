namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddEventInfoToLogEntry : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.LogEntries", "EventId", c => c.String());
            AddColumn("dbo.LogEntries", "EventDuration", c => c.Long());
        }
        
        public override void Down()
        {
            DropColumn("dbo.LogEntries", "EventDuration");
            DropColumn("dbo.LogEntries", "EventId");
        }
    }
}
