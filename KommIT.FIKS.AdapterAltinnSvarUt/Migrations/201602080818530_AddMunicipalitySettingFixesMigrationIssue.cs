namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddMunicipalitySettingFixesMigrationIssue : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.SchemaMetadatas", "MunicipalityCode", "dbo.Municipalities");
            DropIndex("dbo.SchemaMetadatas", new[] { "MunicipalityCode" });
            CreateTable(
                "dbo.MunicipalitySettings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Titles = c.String(),
                        MunicipalityCode = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Municipalities", t => t.MunicipalityCode, cascadeDelete: true)
                .Index(t => t.MunicipalityCode);
            
            AlterColumn("dbo.SchemaMetadatas", "MunicipalityCode", c => c.String(maxLength: 128));
            CreateIndex("dbo.SchemaMetadatas", "MunicipalityCode");
            AddForeignKey("dbo.SchemaMetadatas", "MunicipalityCode", "dbo.Municipalities", "Code");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SchemaMetadatas", "MunicipalityCode", "dbo.Municipalities");
            DropForeignKey("dbo.MunicipalitySettings", "MunicipalityCode", "dbo.Municipalities");
            DropIndex("dbo.MunicipalitySettings", new[] { "MunicipalityCode" });
            DropIndex("dbo.SchemaMetadatas", new[] { "MunicipalityCode" });
            AlterColumn("dbo.SchemaMetadatas", "MunicipalityCode", c => c.String(nullable: false, maxLength: 128));
            DropTable("dbo.MunicipalitySettings");
            CreateIndex("dbo.SchemaMetadatas", "MunicipalityCode");
            AddForeignKey("dbo.SchemaMetadatas", "MunicipalityCode", "dbo.Municipalities", "Code", cascadeDelete: true);
        }
    }
}
