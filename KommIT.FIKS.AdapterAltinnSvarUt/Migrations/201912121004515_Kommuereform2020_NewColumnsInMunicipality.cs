namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Kommuereform2020_NewColumnsInMunicipality : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Municipalities", "ValidTo", c => c.DateTime());
            AddColumn("dbo.Municipalities", "ValidFrom", c => c.DateTime());
            AddColumn("dbo.Municipalities", "NewMunicipalityCode", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Municipalities", "NewMunicipalityCode");
            DropColumn("dbo.Municipalities", "ValidFrom");
            DropColumn("dbo.Municipalities", "ValidTo");
        }
    }
}
