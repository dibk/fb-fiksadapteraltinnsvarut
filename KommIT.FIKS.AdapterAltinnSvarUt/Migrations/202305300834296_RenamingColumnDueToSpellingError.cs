﻿namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RenamingColumnDueToSpellingError : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.FormWorkflowLogs");
            RenameColumn("dbo.FormWorkflowLogs", "ArchicveReference", "ArchiveReference");
            AddPrimaryKey("dbo.FormWorkflowLogs", "ArchiveReference");
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.FormWorkflowLogs");
            RenameColumn("dbo.FormWorkflowLogs", "ArchiveReference", "ArchicveReference");
            AddPrimaryKey("dbo.FormWorkflowLogs", "ArchicveReference");
        }
    }
}
