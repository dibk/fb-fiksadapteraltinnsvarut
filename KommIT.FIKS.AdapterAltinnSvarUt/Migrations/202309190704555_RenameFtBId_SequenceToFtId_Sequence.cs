﻿namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RenameFtBId_SequenceToFtId_Sequence : DbMigration
    {
        public override void Up()
        {
            Sql(@"
                DROP SEQUENCE dbo.FtBId_Sequence;
                GO                    
            ");

            Sql(@"
                CREATE SEQUENCE dbo.FtId_Sequence
                    AS int
                    START WITH 1
                    MAXVALUE 9999999;
                GO                    
            ");
        }
        
        public override void Down()
        {
            Sql(@"
                DROP SEQUENCE dbo.FtId_Sequence;
                GO                    
            ");

            Sql(@"
                CREATE SEQUENCE dbo.FtBId_Sequence
                    AS int
                    START WITH 1
                    MAXVALUE 9999999;
                GO                    
            ");
        }
    }
}
