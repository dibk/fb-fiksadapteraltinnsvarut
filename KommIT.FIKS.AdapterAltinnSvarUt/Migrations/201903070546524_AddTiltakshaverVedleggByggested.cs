namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTiltakshaverVedleggByggested : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TiltakshaversSamtykkeByggesteds",
                c => new
                    {
                        ByggestedId = c.Int(nullable: false, identity: true),
                        Kommunenr = c.String(),
                        Gardsnr = c.String(),
                        Bruksnr = c.String(),
                        Festenr = c.String(),
                        Seksjonsnr = c.String(),
                        Adresse = c.String(),
                        TiltakshaversSamtykke_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.ByggestedId)
                .ForeignKey("dbo.TiltakshaversSamtykkes", t => t.TiltakshaversSamtykke_Id)
                .Index(t => t.TiltakshaversSamtykke_Id);
            
            CreateTable(
                "dbo.TiltakshaversSamtykkeVedleggs",
                c => new
                    {
                        VedleggId = c.Int(nullable: false, identity: true),
                        Vedleggstype = c.String(),
                        Navn = c.String(),
                        Referanse = c.String(),
                        Storrelse = c.String(),
                        TiltakshaversSamtykke_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.VedleggId)
                .ForeignKey("dbo.TiltakshaversSamtykkes", t => t.TiltakshaversSamtykke_Id)
                .Index(t => t.TiltakshaversSamtykke_Id);
            
            AddColumn("dbo.TiltakshaversSamtykkes", "TiltakshaverFoedselsnummer", c => c.String());
            AddColumn("dbo.TiltakshaversSamtykkes", "TiltakshaverOrganisasjonsnummer", c => c.String());
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TiltakshaversSamtykkeVedleggs", "TiltakshaversSamtykke_Id", "dbo.TiltakshaversSamtykkes");
            DropForeignKey("dbo.TiltakshaversSamtykkeByggesteds", "TiltakshaversSamtykke_Id", "dbo.TiltakshaversSamtykkes");
            DropIndex("dbo.TiltakshaversSamtykkeVedleggs", new[] { "TiltakshaversSamtykke_Id" });
            DropIndex("dbo.TiltakshaversSamtykkeByggesteds", new[] { "TiltakshaversSamtykke_Id" });
            DropColumn("dbo.TiltakshaversSamtykkes", "TiltakshaverOrganisasjonsnummer");
            DropColumn("dbo.TiltakshaversSamtykkes", "TiltakshaverFoedselsnummer");
            DropTable("dbo.TiltakshaversSamtykkeVedleggs");
            DropTable("dbo.TiltakshaversSamtykkeByggesteds");
        }
    }
}
