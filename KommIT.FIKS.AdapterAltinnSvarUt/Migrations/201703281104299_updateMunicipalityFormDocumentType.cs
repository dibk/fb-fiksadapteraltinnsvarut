namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateMunicipalityFormDocumentType : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.MunicipalityFormDocumentTypes", "MunicipalityCode", "dbo.Municipalities");
            DropIndex("dbo.MunicipalityFormDocumentTypes", new[] { "MunicipalityCode" });
            AlterColumn("dbo.MunicipalityFormDocumentTypes", "MunicipalityCode", c => c.String(maxLength: 128));
            CreateIndex("dbo.MunicipalityFormDocumentTypes", "MunicipalityCode");
            AddForeignKey("dbo.MunicipalityFormDocumentTypes", "MunicipalityCode", "dbo.Municipalities", "Code");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MunicipalityFormDocumentTypes", "MunicipalityCode", "dbo.Municipalities");
            DropIndex("dbo.MunicipalityFormDocumentTypes", new[] { "MunicipalityCode" });
            AlterColumn("dbo.MunicipalityFormDocumentTypes", "MunicipalityCode", c => c.String(nullable: false, maxLength: 128));
            CreateIndex("dbo.MunicipalityFormDocumentTypes", "MunicipalityCode");
            AddForeignKey("dbo.MunicipalityFormDocumentTypes", "MunicipalityCode", "dbo.Municipalities", "Code", cascadeDelete: true);
        }
    }
}
