namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SvarUtForsendelsesStatus_add_municipality_name : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SvarUtForsendelsesStatus", "MunicipalityName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.SvarUtForsendelsesStatus", "MunicipalityName");
        }
    }
}
