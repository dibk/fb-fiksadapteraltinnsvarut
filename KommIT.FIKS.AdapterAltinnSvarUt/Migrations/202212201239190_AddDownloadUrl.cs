﻿namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class AddDownloadUrl : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Decisions", "DocumentUrl", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Decisions", "DocumentUrl");
        }
    }
}
