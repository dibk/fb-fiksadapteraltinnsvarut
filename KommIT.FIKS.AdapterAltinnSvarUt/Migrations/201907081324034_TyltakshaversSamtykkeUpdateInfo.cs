namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TyltakshaversSamtykkeUpdateInfo : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TiltakshaversSamtykkes", "ServiceCode", c => c.String());
            AddColumn("dbo.TiltakshaversSamtykkes", "ServiceEdition", c => c.String());
            AddColumn("dbo.TiltakshaversSamtykkes", "FraSluttBrukerSistem", c => c.String());
            AddColumn("dbo.TiltakshaversSamtykkes", "DateTime", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.TiltakshaversSamtykkes", "DateTime");
            DropColumn("dbo.TiltakshaversSamtykkes", "FraSluttBrukerSistem");
            DropColumn("dbo.TiltakshaversSamtykkes", "ServiceEdition");
            DropColumn("dbo.TiltakshaversSamtykkes", "ServiceCode");
        }
    }
}
