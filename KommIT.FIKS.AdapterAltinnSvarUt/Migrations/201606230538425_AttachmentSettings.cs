namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AttachmentSettings : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MunicipalityAttachmentSettings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DocumentNumber = c.Int(nullable: false),
                        AttatchmentTypeName = c.String(),
                        Title = c.String(),
                        DocumentType = c.String(),
                        DocumentStatus = c.String(),
                        TilknyttetRegistreringSom = c.String(),
                        MunicipalityCode = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Municipalities", t => t.MunicipalityCode, cascadeDelete: true)
                .Index(t => t.MunicipalityCode);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MunicipalityAttachmentSettings", "MunicipalityCode", "dbo.Municipalities");
            DropIndex("dbo.MunicipalityAttachmentSettings", new[] { "MunicipalityCode" });
            DropTable("dbo.MunicipalityAttachmentSettings");
        }
    }
}
