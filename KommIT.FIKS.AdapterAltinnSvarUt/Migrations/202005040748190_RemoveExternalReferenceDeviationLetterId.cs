namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveExternalReferenceDeviationLetterId : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.DeviationLetters", "ExternalDeviationLetterId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.DeviationLetters", "ExternalDeviationLetterId", c => c.String());
        }
    }
}
