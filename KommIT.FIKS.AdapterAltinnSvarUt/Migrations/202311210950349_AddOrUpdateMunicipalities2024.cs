﻿namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System.Collections.Generic;
    using System.Data.Entity.Migrations;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Reflection.Emit;
    using System.Text;

    public partial class AddOrUpdateMunicipalities2024 : DbMigration
    {
        private List<string> GetContent(string resourceName)
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            List<string> content = new List<string>();

            using (Stream stream = assembly.GetManifestResourceStream(resourceName))
            using (StreamReader reader = new StreamReader(stream, Encoding.UTF8))
            {
                while (!reader.EndOfStream)
                {
                    content.Add(reader.ReadLine());
                }
            }
            return content;
        }

        public override void Up()
        {
            // Legg produksjonsdata fra 20.11.2023 dersom det ikke finnes i databasen fra før.
            string resourceName = "KommIT.FIKS.AdapterAltinnSvarUt.Models.MigrationData.Prod20112023.csv";
            var municipalitiesProd = GetContent(resourceName);

            foreach (var row in municipalitiesProd)
            {
                var values = row.Split(',');
                var code = values[0].ToString();
                var organizationNumber = string.IsNullOrEmpty(values[1].ToString()) ? "NULL" : $"'{values[1]}'";
                var name = string.IsNullOrEmpty(values[2].ToString()) ? "NULL" : $"'{values[2]}'";
                var validTo = string.IsNullOrEmpty(values[3]) ? "NULL" : $"'{values[3]}'";
                var validFrom = string.IsNullOrEmpty(values[4]) ? "NULL" : $"'{values[4]}'";
                var newMunicipalityCode = string.IsNullOrEmpty(values[5].ToString()) ? "NULL" : $"'{values[5]}'";
                var planningDepartmentSpecificOrganizationNumber =
                    string.IsNullOrEmpty(values[6]) ? "NULL" : $"'{values[6]}'";

                string insertOrUpdateQuery = $@"
                        IF NOT EXISTS (SELECT 1 FROM Municipalities WHERE Code = '{code}')
                        BEGIN
                            INSERT INTO Municipalities (Code, OrganizationNumber, Name, ValidTo, ValidFrom, NewMunicipalityCode, PlanningDepartmentSpecificOrganizationNumber)
                            VALUES ('{code}', {organizationNumber}, {name}, {validTo}, {validFrom}, {newMunicipalityCode}, {planningDepartmentSpecificOrganizationNumber});
                        END
                    ";


                Sql(insertOrUpdateQuery);


                // Oppdatering av navn på eksisterende kommuner
                Sql($@"UPDATE Municipalities
                                    SET Name = 'Våler'
                                    WHERE Code = '3419'");

                Sql($@"UPDATE Municipalities
                                    SET Name = 'Os'
                                    WHERE Code = '3430'");

                Sql($@"UPDATE Municipalities
                                    SET Name = 'Sande'
                                    WHERE Code = '1514'");

                Sql($@"UPDATE Municipalities
                                    SET Name = 'Bø'
                                    WHERE Code = '1867'");

            }

            // Kommuner som forsvinner/deles opp fra 2024

            string resourceNameMunicipalities = "KommIT.FIKS.AdapterAltinnSvarUt.Models.MigrationData.Kommuner_som_forsvinner_2024.csv";
            var municipalities = GetContent(resourceNameMunicipalities);

            foreach (var municipality in municipalities)
            {
                string endMunicipalityQuery = $@"
                            IF EXISTS (SELECT 1 FROM Municipalities WHERE Code = '{municipality}')
                            BEGIN
                                UPDATE Municipalities
                                SET ValidTo = '2023-12-31 23:59:59.000'
                                WHERE Code = '{municipality}';
                            END
                        ";

                Sql(endMunicipalityQuery);
            }

            // Oppdater med nye kommuner gjeldende fra 1.1.2024
            string resourceNewMuniciapalities = "KommIT.FIKS.AdapterAltinnSvarUt.Models.MigrationData.Nye_kommunenummer_2024.csv";
            var updatedMunicipalities = GetContent(resourceNewMuniciapalities);

            foreach (var row in updatedMunicipalities.Skip(1)) // Skip header
            {
                var values = row.Split(';');
                var oldCode = values[0].Trim();
                var name = string.IsNullOrEmpty(values[1].Trim()) ? "NULL" : $"'{values[1]}'";
                var newCode = string.IsNullOrEmpty(values[2].Trim()) ? "NULL" : $"'{values[2]}'";
                var orgNr = string.IsNullOrEmpty(values[3].Trim()) ? "NULL" : $"'{values[3]}'";

                // Legg til nye kommuner
                if (string.IsNullOrWhiteSpace(oldCode))
                {
                    string insertNewMunicipalityQuery = $@"
                            IF NOT EXISTS (SELECT 1 FROM Municipalities WHERE Code = {newCode})
                            BEGIN
                                INSERT INTO Municipalities (Code, OrganizationNumber, Name, ValidFrom)
                                VALUES ({newCode}, {orgNr}, {name}, '2024-01-01 00:00:00.000');
                            END
                        ";

                    Sql(insertNewMunicipalityQuery);
                }
                else
                {
                    // Sjekk om det gamle kommunenummeret finnes i databasen.
                    // Ja - Oppdater og legg til ny rad med informasjon fra tidligere rad
                    // Nei - Legg til ny rad

                    string updateAndInsertQuery = $@"
                            IF EXISTS (SELECT 1 FROM Municipalities WHERE Code = '{oldCode}')
                            BEGIN
                                UPDATE Municipalities
                                SET ValidTo = '2023-12-31 23:59:59.000',
                                    NewMunicipalityCode = {newCode}
                                WHERE Code = '{oldCode}';

                                IF NOT EXISTS (SELECT 1 FROM Municipalities WHERE Code = {newCode})
                                BEGIN
                                    INSERT INTO Municipalities (Code, OrganizationNumber, Name, ValidFrom)
                                    SELECT {newCode} AS Code,
                                       OrganizationNumber,
                                       {name} AS Name,
                                       '2024-01-01 00:00:00.000' AS ValidFrom
                                    FROM Municipalities
                                    WHERE Code = '{oldCode}';
                                END
                                ELSE
                                BEGIN
                                    UPDATE Municipalities
                                    SET Name = {name},
                                        OrganizationNumber = {orgNr}
                                    WHERE Code = {newCode};
                                END
                            END
                            ELSE
                            BEGIN
                                IF NOT EXISTS (SELECT 1 FROM Municipalities WHERE Code = {newCode})
                                BEGIN
                                    INSERT INTO Municipalities (Code, OrganizationNumber, Name, ValidFrom)
                                    VALUES ({newCode}, {orgNr}, {name}, '2024-01-01 00:00:00.000');
                                END
                            END
                        ";

                    Sql(updateAndInsertQuery);
                }
            }
        }

        public override void Down()
        {
        }
    }
}