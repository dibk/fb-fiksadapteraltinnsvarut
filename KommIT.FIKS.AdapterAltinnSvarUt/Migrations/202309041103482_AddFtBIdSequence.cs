﻿namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AddFtBIdSequence : DbMigration
    {
        public override void Up()
        {
            Sql(@"
                CREATE SEQUENCE dbo.FtBId_Sequence
                    AS int
                    START WITH 1
                    MAXVALUE 9999999;
                GO                    
            ");
        }
        
        public override void Down()
        {
            Sql(@"
                DROP SEQUENCE dbo.FtBId_Sequence;
                GO                    
            ");
        }
    }
}
