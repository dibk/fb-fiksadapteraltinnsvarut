namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddMunicipalityModel : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Municipalities",
                c => new
                    {
                        Code = c.String(nullable: false, maxLength: 128),
                        OrganizationNumber = c.String(),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Code);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Municipalities");
        }
    }
}
