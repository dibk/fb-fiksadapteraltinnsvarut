namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SvarUtForsendelsesStatus : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SvarUtForsendelsesStatus",
                c => new
                    {
                        ArchiveReference = c.String(nullable: false, maxLength: 128),
                        SvarUtForsendelsesId = c.String(nullable: false, maxLength: 128),
                        ForsendelseStatus = c.String(),
                        EndretStatusDato = c.DateTime(),
                        FormMetadata_ArchiveReference = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.ArchiveReference, t.SvarUtForsendelsesId })
                .ForeignKey("dbo.FormMetadata", t => t.FormMetadata_ArchiveReference)
                .Index(t => t.FormMetadata_ArchiveReference);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SvarUtForsendelsesStatus", "FormMetadata_ArchiveReference", "dbo.FormMetadata");
            DropIndex("dbo.SvarUtForsendelsesStatus", new[] { "FormMetadata_ArchiveReference" });
            DropTable("dbo.SvarUtForsendelsesStatus");
        }
    }
}
