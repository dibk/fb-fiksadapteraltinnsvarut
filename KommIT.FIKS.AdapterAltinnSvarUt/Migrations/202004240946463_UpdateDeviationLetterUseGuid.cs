namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateDeviationLetterUseGuid : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.DeviationLetters");
            AddColumn("dbo.DeviationLetters", "DeviationLetterId", c => c.Guid(nullable: false));
            AddPrimaryKey("dbo.DeviationLetters", "DeviationLetterId");
            DropColumn("dbo.DeviationLetters", "DeviationId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.DeviationLetters", "DeviationId", c => c.Long(nullable: false, identity: true));
            DropPrimaryKey("dbo.DeviationLetters");
            DropColumn("dbo.DeviationLetters", "DeviationLetterId");
            AddPrimaryKey("dbo.DeviationLetters", "DeviationId");
        }
    }
}
