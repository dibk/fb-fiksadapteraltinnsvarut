namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddActivity : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Activities",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Municipality = c.String(),
                        Name = c.String(),
                        ActivityType = c.String(),
                        Description = c.String(),
                        Category = c.String(),
                        Processcategory = c.String(),
                        Milestone = c.String(),
                        HasRule = c.Boolean(nullable: false),
                        Rule = c.String(),
                        ValidFrom = c.DateTime(),
                        ValidTo = c.DateTime(),
                        Updated = c.DateTime(nullable: false),
                        Activity_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Activities", t => t.Activity_Id)
                .Index(t => t.Activity_Id);
            
            CreateTable(
                "dbo.Actions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ActionValue = c.Boolean(nullable: false),
                        ActionType = c.String(),
                        ActionTypeCode = c.String(),
                        Activity_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Activities", t => t.Activity_Id)
                .Index(t => t.Activity_Id);
            
            CreateTable(
                "dbo.LawReferances",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LawReferanceDescription = c.String(),
                        LawReferanceUrl = c.String(),
                        Activity_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Activities", t => t.Activity_Id)
                .Index(t => t.Activity_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Activities", "Activity_Id", "dbo.Activities");
            DropForeignKey("dbo.LawReferances", "Activity_Id", "dbo.Activities");
            DropForeignKey("dbo.Actions", "Activity_Id", "dbo.Activities");
            DropIndex("dbo.LawReferances", new[] { "Activity_Id" });
            DropIndex("dbo.Actions", new[] { "Activity_Id" });
            DropIndex("dbo.Activities", new[] { "Activity_Id" });
            DropTable("dbo.LawReferances");
            DropTable("dbo.Actions");
            DropTable("dbo.Activities");
        }
    }
}
