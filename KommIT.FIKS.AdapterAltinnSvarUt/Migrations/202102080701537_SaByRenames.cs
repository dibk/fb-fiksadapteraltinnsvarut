namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SaByRenames : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TiltakshaversSamtykkes", "FraSluttbrukerSystem", c => c.String());
            DropColumn("dbo.TiltakshaversSamtykkes", "FraSluttBrukerSistem");
        }
        
        public override void Down()
        {
            AddColumn("dbo.TiltakshaversSamtykkes", "FraSluttBrukerSistem", c => c.String());
            DropColumn("dbo.TiltakshaversSamtykkes", "FraSluttbrukerSystem");
        }
    }
}
