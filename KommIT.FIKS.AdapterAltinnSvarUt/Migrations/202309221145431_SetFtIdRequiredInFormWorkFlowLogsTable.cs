﻿namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SetFtIdRequiredInFormWorkFlowLogsTable : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.FormWorkflowLogs", new[] { "FtId" });
            AlterColumn("dbo.FormWorkflowLogs", "FtId", c => c.String(nullable: false, maxLength: 80));
            CreateIndex("dbo.FormWorkflowLogs", "FtId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.FormWorkflowLogs", new[] { "FtId" });
            AlterColumn("dbo.FormWorkflowLogs", "FtId", c => c.String(maxLength: 80));
            CreateIndex("dbo.FormWorkflowLogs", "FtId");
        }
    }
}
