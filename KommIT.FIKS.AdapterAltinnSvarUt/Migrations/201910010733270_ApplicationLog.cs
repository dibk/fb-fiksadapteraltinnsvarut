namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ApplicationLog : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ApplicationLogs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Timestamp = c.DateTime(nullable: false),
                        SequenceTag = c.String(),
                        Message = c.String(),
                        Type = c.String(),
                        Reference = c.String(),
                        DataFormatId = c.String(),
                        DataFormatVersion = c.String(),
                        CodeIdentifier = c.String(),
                        Trace = c.String(),
                        ProcessLabelId = c.String(),
                        EventDuration = c.Long(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.ApplicationLogs");
        }
    }
}
