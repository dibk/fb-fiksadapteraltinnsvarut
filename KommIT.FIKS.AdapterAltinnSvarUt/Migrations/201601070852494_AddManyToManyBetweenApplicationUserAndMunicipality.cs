namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddManyToManyBetweenApplicationUserAndMunicipality : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MunicipalityApplicationUser",
                c => new
                    {
                        MunicipalityCode = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.MunicipalityCode, t.UserId })
                .ForeignKey("dbo.Municipalities", t => t.MunicipalityCode, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.MunicipalityCode)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MunicipalityApplicationUser", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.MunicipalityApplicationUser", "MunicipalityCode", "dbo.Municipalities");
            DropIndex("dbo.MunicipalityApplicationUser", new[] { "UserId" });
            DropIndex("dbo.MunicipalityApplicationUser", new[] { "MunicipalityCode" });
            DropTable("dbo.MunicipalityApplicationUser");
        }
    }
}
