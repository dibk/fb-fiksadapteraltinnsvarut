namespace KommIT.FIKS.AdapterAltinnSvarUt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateApiAuthentication : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.APIAuthentications", new[] { "Username" });
            DropPrimaryKey("dbo.APIAuthentications");
            AlterColumn("dbo.APIAuthentications", "Username", c => c.String(nullable: false, maxLength: 128));
            AddPrimaryKey("dbo.APIAuthentications", "Username");
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.APIAuthentications");
            AlterColumn("dbo.APIAuthentications", "Username", c => c.String(maxLength: 128, unicode: false));
            CreateIndex("dbo.APIAuthentications", "Username", unique: true);
        }
    }
}
