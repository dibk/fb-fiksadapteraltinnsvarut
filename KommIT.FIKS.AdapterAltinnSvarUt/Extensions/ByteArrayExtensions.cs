﻿using System.IO;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Extensions
{
    public static class ByteArrayExtensions
    {
        public static Stream ToStream(this byte[] byteArray)
        {
            return new MemoryStream(byteArray);
        }

        public static Stream ToStream (this string s)
        {
            return new MemoryStream(System.Text.Encoding.UTF8.GetBytes(s));
        }
    }
}