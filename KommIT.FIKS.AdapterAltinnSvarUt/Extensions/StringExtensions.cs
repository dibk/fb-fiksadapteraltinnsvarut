﻿namespace KommIT.FIKS.AdapterAltinnSvarUt.Extensions
{
    public static class StringExtensions
    {
        public static bool IsNullOrEmpty(this string s)
        {
            return string.IsNullOrEmpty(s);
        }

        public static bool IsNullOrWhiteSpace(this string s)
        {
            return string.IsNullOrWhiteSpace(s);
        }

        public static string ToHtmlEncodedString(this string s)
        {
            return System.Web.HttpUtility.HtmlEncode(s);
        }
    }
}