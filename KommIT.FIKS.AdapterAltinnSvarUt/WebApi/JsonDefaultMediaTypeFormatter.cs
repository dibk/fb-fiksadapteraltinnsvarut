﻿using Newtonsoft.Json;
using System;
using System.Net.Http.Formatting;
using WebApi.Hal;

namespace KommIT.FIKS.AdapterAltinnSvarUt.WebApi
{
    public class JsonDefaultMediaTypeFormatter : JsonMediaTypeFormatter
    {
        public JsonDefaultMediaTypeFormatter()
        {
            SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
            SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
        }

        public override bool CanWriteType(Type type)
        {
            //Should not support the type Representation since it is used for application/hal+json
            return !typeof(Representation).IsAssignableFrom(type);
        }

        public override bool CanReadType(Type type)
        {
            //Should not support the type Representation since it is used for application/hal+json
            return !typeof(Representation).IsAssignableFrom(type);
        }
    }
}
