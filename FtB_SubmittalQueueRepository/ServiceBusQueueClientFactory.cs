﻿namespace FtB_SubmittalQueueRepository
{
    public class ServiceBusQueueClientFactory : IServiceBusQueueClientFactory
    {
        private readonly string _serviceBusConnectionString;

        public ServiceBusQueueClientFactory(string serviceBusConnectionString)
        {
            _serviceBusConnectionString = serviceBusConnectionString;
        }

        public Azure.Messaging.ServiceBus.ServiceBusSender GetQueueClientFor(string queueName)
        {
            
            var c = new Azure.Messaging.ServiceBus.ServiceBusClient(_serviceBusConnectionString);
            return c.CreateSender(queueName);
        }
    }
}
