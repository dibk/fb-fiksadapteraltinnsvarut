﻿namespace FtB_SubmittalQueueRepository
{
    public interface ISubmittalQueueItem
    {
        string ArchiveReference { get; set; }
    }
}
