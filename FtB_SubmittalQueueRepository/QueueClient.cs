﻿using Newtonsoft.Json;
using Serilog;
using System.Text;
using System.Threading.Tasks;

namespace FtB_SubmittalQueueRepository
{
    public class QueueClient : IQueueClient
    {
        //private readonly ILogger<QueueClient> _logger;
        private readonly ILogger _logger = Log.ForContext<QueueClient>();
        private readonly IServiceBusQueueClientFactory _serviceBusQueueClientFactory;
        private string _queueName;

        public QueueClient(IServiceBusQueueClientFactory serviceBusQueueClientFactory, string queueName)
        {
            _serviceBusQueueClientFactory = serviceBusQueueClientFactory;
            _queueName = queueName;
        }
        public async Task AddToQueue(ISubmittalQueueItem queueItem)
        {
            try
            {
                var queueClient = _serviceBusQueueClientFactory.GetQueueClientFor(_queueName);
                var payload = JsonConvert.SerializeObject(queueItem);
                _logger.Information("Queued form for further processing: queueName: {0}, message: {1}", _queueName, payload);
                var message = new Azure.Messaging.ServiceBus.ServiceBusMessage(Encoding.UTF8.GetBytes(payload));
                await queueClient.SendMessageAsync(message);
            }
            catch (System.Exception ex)
            {
                //_logger.LogError(e, "Error occurred while communicating with Azure Service Bus");
                _logger.Error(ex.Message, "Error occurred while communicating with Azure Service Bus");
                throw;
            }

        }

    }
}
