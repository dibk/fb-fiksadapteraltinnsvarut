﻿namespace FtB_SubmittalQueueRepository
{
    public interface IServiceBusQueueClientFactory
    {
        Azure.Messaging.ServiceBus.ServiceBusSender GetQueueClientFor(string queueName);

    }
}
