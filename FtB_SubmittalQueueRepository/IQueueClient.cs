﻿using System.Threading.Tasks;

namespace FtB_SubmittalQueueRepository
{
    public interface IQueueClient
    {
        Task AddToQueue(ISubmittalQueueItem queueItem);
    }
}
