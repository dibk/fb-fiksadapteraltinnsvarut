﻿using Dibk.Ftpb.Api.Clients.HttpClients;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Text;

namespace Dibk.Ftpb.Api.Clients
{
    /// <summary>
    /// Configuration for the service
    /// </summary>
    public static class ServiceConfiguration
    {
        /// <summary>
        /// Adds clients for the FTPB APIs.
        /// NOTE: Must be made extendable to add handling of retries , circuit breakers, etc. Like extension AddTransientHttpErrorPolicy
        /// </summary>
        /// <param name="services"></param>
        /// <param name="baseUri"></param>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public static IServiceCollection AddFtpbApiClient(this IServiceCollection services, Uri baseUri, string userName, string password)
        {
            var auth = Encoding.ASCII.GetBytes($"{userName}:{password}");
            var authHeader = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(auth));

            var t = services.AddHttpClient("ftpbApiClient", opt =>
            {
                opt.BaseAddress = baseUri;
                opt.DefaultRequestHeaders.Authorization = authHeader;
                opt.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            });

            services.AddScoped<DistributionFormsHttpClient>();
            services.AddScoped<FileDownloadStatusHttpClient>();
            services.AddScoped<FormMetadataHttpClient>();
            services.AddScoped<FtIdHttpClient>();
            services.AddScoped<LogEntriesHttpClient>();
            services.AddScoped<MunicipalityHttpClient>();

            return services;
        }
    }
}