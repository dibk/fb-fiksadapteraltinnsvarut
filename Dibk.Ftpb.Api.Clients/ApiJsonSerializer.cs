﻿using System;
using System.Text.Json;

namespace Dibk.Ftpb.Api.Clients
{
    public class ApiJsonSerializer
    {
        private static readonly JsonSerializerOptions _jsonSerializerOptions = new JsonSerializerOptions
        {
            PropertyNameCaseInsensitive = true,
            AllowTrailingCommas = true
        };

        /// <summary>
        /// Deserialize json string to object beeing case insensitive
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="content"></param>
        /// <returns></returns>
        public static T Deserialize<T>(string content)
        {
            if (content == null)
                throw new ArgumentNullException(nameof(content));

            T result = JsonSerializer.Deserialize<T>(content, _jsonSerializerOptions);

            return result;
        }
    }
}