﻿namespace Dibk.Ftpb.Api.Models
{
    public class FormAttachmentMetadataApiModel
    {
        public static string FileSourceSystemGenerated = "SystemGenerated";
        public static string FileSourceUserProvided = "UserProvided";

        public int Id { get; set; }
        public string FileName { get; set; }
        public int Size { get; set; }
        public string AttachmentType { get; set; }
        public string MimeType { get; set; }
        public string Source { get; set; }
    }
}