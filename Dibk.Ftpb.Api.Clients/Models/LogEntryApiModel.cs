﻿using System;
using System.Text;

namespace Dibk.Ftpb.Api.Models
{
    public class LogEntryApiModel
    {
        public int Id { get; set; }

        public DateTime Timestamp { get; set; }
        public string? ArchiveReference { get; set; }
        public string? Message { get; set; }
        public string? Type { get; set; }
        public bool OnlyInternal { get; set; }
        public string? Url { get; set; }
        public string? EventId { get; set; }
        public long? EventDuration { get; set; }

        /// <summary>
        /// Override ToString for å gi en mer fornuftig loggutskrift
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            var builder = new StringBuilder(Message);
            builder.Append(", ArchiveReference: ").Append(ArchiveReference);
            builder.Append(", Type: ").Append(Type);
            if (!string.IsNullOrEmpty(EventId))
                builder.Append(", Event: ").Append(EventId);

            if (EventDuration != null)
                builder.Append(", Duration: ").Append(EventDuration).Append("ms");

            return builder.ToString();
        }
    }
}