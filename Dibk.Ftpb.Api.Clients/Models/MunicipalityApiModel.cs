﻿using System;

namespace Dibk.Ftpb.Api.Models
{
    /// <summary>
    /// Modell for kommuneinformasjon i FtPB
    /// </summary>
    public class MunicipalityApiModel
    {
        /// <summary>
        /// Kommunenummer
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// Kommunenavn
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Organisasjonsnummer til kommunens byggesaksavdeling
        /// </summary>
        public string? OrganizationNumber { get; set; }
        /// <summary>
        /// Organisasjonsnummer til kommunens planavdeling
        /// </summary>
        public string? PlanningDepartmentSpecificOrganizationNumber { get; set; }
        /// <summary>
        /// Kommunens organisasjonsnummer etter kommunereform
        /// </summary>
        public string? NewMunicipalityCode { get; set; }
        /// <summary>
        /// Kommunenummeret gyldig til. Dette er ifm kommunereformen
        /// </summary>
        public DateTime? ValidTo { get; set; }
        /// <summary>
        /// Kommunenummeret gyldig fra. Dette er ifm kommunereformen
        /// </summary>
        public DateTime? ValidFrom { get; set; }
    }
}