﻿using System;
using System.Text.Json.Serialization;

namespace Dibk.Ftpb.Api.Models
{
    public class DistributionFormApiModel
    {
        /// <summary>
        /// ID for instansen
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// ID på innsendt distribusjonstjeneste (opprinnelsen)
        /// </summary>
        public string? InitialArchiveReference { get; set; }

        /// <summary>
        /// Sluttbrukersystem sin referanse til forsendelsene (Hovedinnsendingsnr i distribusjon skjema datamodell)
        /// </summary>
        public string? InitialExternalSystemReference { get; set; }

        /// <summary>
        /// Type distribusjonstjeneste
        /// </summary>
        public string? DistributionType { get; set; }

        /// <summary>
        /// Referanse til preutfyllingsfunsjonen
        /// </summary>
        public string? SubmitAndInstantiatePrefilledFormTaskReceiptId { get; set; }

        /// <summary>
        /// Tidspunkt prefill er sendt
        /// </summary>
        public DateTime? SubmitAndInstantiatePrefilled { get; set; }

        /// <summary>
        /// Sluttbrukersystem sin referanse til forsendelse (VaarReferanse i skjema datamodell)
        /// </summary>
        public string? ExternalSystemReference { get; set; }

        /// <summary>
        /// Referanse til signert preutfylt innsending
        /// </summary>
        public string? SignedArchiveReference { get; set; }

        /// <summary>
        /// Tidspunkt mottak av signert skjema
        /// </summary>
        public DateTime? Signed { get; set; }

        /// <summary>
        /// Referanse til videresendt melding med signert skjema (receiptSent)
        /// </summary>
        public string? RecieptSentArchiveReference { get; set; }

        /// <summary>
        /// Tidspunkt videresending av signert skjema (receiptSent)
        /// </summary>
        public DateTime? RecieptSent { get; set; }

        /// <summary>
        /// Feilmelding ved status=error
        /// </summary>
        public string? ErrorMessage { get; set; }

        /// <summary>
        /// Status på distribusjonen
        /// </summary>

        public DistributionStatus DistributionStatus { get; set; }

        /// <summary>
        /// Om distribusjonen har gått til print pga reservert
        /// </summary>
        public bool Printed { get; set; }

        /// <summary>
        /// Indikerer om denne distribusjonen ble kombinert til en forsendelse i Altinn eller print, null for forsendelse , og inneholder en GUID om det er en combinert
        /// </summary>

        public Guid DistributionReference { get; set; }

        /// <summary>
        /// Status på AnSaKo prosessen
        /// </summary>
        public AnSaKoStatusApiModel? AnSaKoStatus { get; set; }
    }

    /// <summary>
    /// Status på distribusjonen
    /// </summary>
    public enum DistributionStatus
    {
        error,
        submittedPrefilled,
        signed,
        receiptSent
    }

    /// <summary>
    /// AmSaKo status
    /// </summary>
    public class AnSaKoStatusApiModel
    {
        [JsonIgnore]
        public int Id { get; set; }

        /// <summary>
        /// Statuuuuuus!
        /// </summary>
        public AnSaKoProcessStatusType AnSaKoProcessStatus { get; set; }

        /// <summary>
        /// Frist for signering
        /// </summary>
        public DateTime? SigningDeadline { get; set; }

        /// <summary>
        /// Kan feks innehalde beskrivelse av kvifor den er blitt trukket eller avvist
        /// </summary>
        public string? StatusDetails { get; set; }
    }

    /// <summary>
    /// Enum for AnSaKo status
    /// </summary>
    public enum AnSaKoProcessStatusType
    {
        /// <summary>
        /// Erklæring er åpnet av ansvarlig foretak, men ikke signert eller avvist.
        /// </summary>
        iArbeid,

        /// <summary>
        /// Erklæring er sendt til signering, men ikke signert eller avvist.
        /// </summary>
        tilSignering,

        /// <summary>
        /// Erklæring er signert med eller uten endringer fra ansvarlig foretak.
        /// </summary>
        signert,

        /// <summary>
        /// Erklæring er avvist og returnert fra ansvarlig foretak.
        /// </summary>
        avvist,

        /// <summary>
        /// Erklæring er ikke signert eller avvist innen tidsfristen.
        /// </summary>
        utgått,

        /// <summary>
        /// Erklæring er trukket tilbake fra ansvarlig søker.
        /// </summary>
        trukket,

        /// <summary>
        /// Sendinger som har feilet grunnet tekniske problemer.
        /// </summary>
        feilet,

        /// <summary>
        /// Erklæringer som er signert utenfor Fellestjenester BYGG, for eksempel på blankett.
        /// </summary>
        signertManuelt,

        /// <summary>
        /// Erklæringer hvor ansvarsområdet er avsluttet.
        /// </summary>
        avsluttet
    }
}