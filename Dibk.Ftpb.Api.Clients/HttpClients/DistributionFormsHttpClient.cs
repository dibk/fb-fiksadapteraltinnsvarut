﻿using Dibk.Ftpb.Api.Models;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Api.Clients.HttpClients
{
    /// <summary>
    /// Client for adding DistributionForms to a form
    /// </summary>
    public class DistributionFormsHttpClient : BaseHttpClient
    {
        /// <summary>
        /// Creates a new client
        /// </summary>
        /// <param name="httpClientFactory"></param>
        /// <param name="logger"></param>
        public DistributionFormsHttpClient(IHttpClientFactory httpClientFactory, ILogger<DistributionFormsHttpClient> logger) : base(httpClientFactory, logger)
        { }

        /// <summary>
        /// <inheritdoc cref="CreateAsync(string, IEnumerable{DistributionFormApiModel}, CancellationToken)"/>
        /// </summary>
        /// <param name="archiveReference"></param>
        /// <param name="distributionForms"></param>
        /// <returns></returns>
        public async Task CreateAsync(string archiveReference, IEnumerable<DistributionFormApiModel> distributionForms)
        {
            await CreateAsync(archiveReference, distributionForms, CancellationToken.None);
        }

        /// <summary>
        /// Create distributionForms for a form
        /// </summary>
        /// <param name="archiveReference"></param>
        /// <param name="distributionForms"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <exception cref="HttpRequestException">NotFound if the form isn't found. Conflict if one of the distributionForms already exist</exception>"
        public async Task CreateAsync(string archiveReference, IEnumerable<DistributionFormApiModel> distributionForms, CancellationToken cancellationToken)
        {
            if (distributionForms == null)
                throw new ArgumentNullException(nameof(distributionForms));

            if (!distributionForms.Any())
                throw new ArgumentException(nameof(distributionForms));

            var requestUri = $"api/formlogdata/{archiveReference}/distributions";
            var json = JsonSerializer.Serialize(distributionForms);
            var stringContent = new StringContent(json, Encoding.UTF8, "application/json");

            try
            {
                using var result = await _httpClient.PostAsync(requestUri, stringContent, cancellationToken);
                result.EnsureSuccess();
            }
            catch (HttpRequestException hre)
            {
                if (hre.GetStatusCode() == System.Net.HttpStatusCode.NotFound)
                {
                    _logger.LogError(hre, $"Form doesn't exist. Unable to create distributionForms for form with archiveReference {archiveReference}");
                    throw;
                }

                if (hre.GetStatusCode() == System.Net.HttpStatusCode.Conflict)
                {
                    _logger.LogError(hre, $"DistributionForm with one of the id's already exist: {string.Join(",", distributionForms.Select(s => s.Id).ToList())}");
                    throw;
                }

                _logger.LogError(hre, $"Unexpected error occurred when retreiving distributionForm for archiveReference {archiveReference}");
                throw;
            }
        }

        /// <summary>
        /// <inheritdoc cref="GetAllAsync(string, CancellationToken)"/>
        /// </summary>
        /// <param name="archiveReference"></param>
        /// <returns></returns>
        public async Task<IEnumerable<DistributionFormApiModel>?> GetAllAsync(string archiveReference)
        {
            return await GetAllAsync(archiveReference, CancellationToken.None);
        }

        /// <summary>
        /// Get all distributionForms for a form
        /// </summary>
        /// <param name="archiveReference"></param>
        /// <param name="cancellationToken"></param>
        /// <returns>Null if there isn't any distributionForms for on the form</returns>
        public async Task<IEnumerable<DistributionFormApiModel>?> GetAllAsync(string archiveReference, CancellationToken cancellationToken)
        {
            if (string.IsNullOrEmpty(archiveReference))
                throw new ArgumentNullException(nameof(archiveReference));

            var requestUri = $"api/formlogdata/{archiveReference}/distributions";

            try
            {
                using var result = await _httpClient.GetAsync(requestUri, cancellationToken);
                result.EnsureSuccess();

                var content = await result.Content.ReadAsStringAsync();
                return  ApiJsonSerializer.Deserialize<IEnumerable<DistributionFormApiModel>>(content);
            }
            catch (HttpRequestException hre)
            {
                if (hre.GetStatusCode() == System.Net.HttpStatusCode.NotFound)
                {
                    _logger.LogWarning(hre, $"Unable to find distributionForm with for archiveReference {archiveReference}");
                    return null;
                }

                _logger.LogError(hre, $"Unexpected error occurred when retreiving distributionForm for archiveReference {archiveReference}");
                throw;
            }
        }

        /// <summary>
        /// <inheritdoc cref="GetAsync(Guid, CancellationToken)"/>
        /// </summary>
        /// <param name="distributionId"></param>
        /// <returns></returns>
        public async Task<DistributionFormApiModel?> GetAsync(Guid distributionId)
        {
            return await GetAsync(distributionId, CancellationToken.None);
        }

        /// <summary>
        /// Get a distributionForm by id
        /// </summary>
        /// <param name="distributionId"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="HttpRequestException">NotFound if the distributionForm isn't found</exception>""
        public async Task<DistributionFormApiModel?> GetAsync(Guid distributionId, CancellationToken cancellationToken)
        {
            if (distributionId == Guid.Empty)
                throw new ArgumentNullException(nameof(distributionId));

            var requestUri = $"api/formlogdata/distributions/{distributionId}";

            try
            {
                using var result = await _httpClient.GetAsync(requestUri, cancellationToken);
                result.EnsureSuccess();

                var content = await result.Content.ReadAsStringAsync();
                return ApiJsonSerializer.Deserialize<DistributionFormApiModel>(content);
            }
            catch (HttpRequestException hre)
            {
                if (hre.GetStatusCode() == System.Net.HttpStatusCode.NotFound)
                {
                    _logger.LogWarning(hre, $"Unable to find distributionForm with id {distributionId}");
                    return null;
                }

                _logger.LogError(hre, $"Unexpected error occurred when retreiving distributionForm with id {distributionId}");
                throw;
            }
        }

        /// <summary>
        /// <inheritdoc cref="UpdateAsync(string, Guid, DistributionFormApiModel, CancellationToken)"/>
        /// </summary>
        /// <param name="archiveReference"></param>
        /// <param name="distributionId"></param>
        /// <param name="distributionForm"></param>
        /// <returns></returns>
        public async Task UpdateAsync(string archiveReference, Guid distributionId, DistributionFormApiModel distributionForm)
        {
            await UpdateAsync(archiveReference, distributionId, distributionForm, CancellationToken.None);
        }

        /// <summary>
        /// Update a distributionForm
        /// </summary>
        /// <param name="archiveReference"></param>
        /// <param name="distributionId"></param>
        /// <param name="distributionForm"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <exception cref="HttpRequestException">NotFound if the distributionForm isn't found</exception>""
        public async Task UpdateAsync(string archiveReference, Guid distributionId, DistributionFormApiModel distributionForm, CancellationToken cancellationToken)
        {
            if (string.IsNullOrEmpty(archiveReference))
                throw new ArgumentNullException(nameof(archiveReference));

            if (distributionId == Guid.Empty)
                throw new ArgumentNullException(nameof(distributionId));

            if (distributionForm == null)
                throw new ArgumentNullException(nameof(distributionForm));

            var requestUri = $"api/formlogdata/{archiveReference}/distributions/{distributionId}";
            var json = JsonSerializer.Serialize(distributionForm);
            var stringContent = new StringContent(json, Encoding.UTF8, "application/json");

            try
            {
                using var result = await _httpClient.PutAsync(requestUri, stringContent, cancellationToken);
                result.EnsureSuccess();
            }
            catch (HttpRequestException hre)
            {
                if (hre.GetStatusCode() == System.Net.HttpStatusCode.NotFound)
                {
                    _logger.LogError(hre, $"Unable to update distributionForm with id {distributionId} owned by form with archiveReference {archiveReference}");
                    throw;
                }

                _logger.LogError(hre, $"Unexpected error occurred when updating distributionForm with id {distributionId} and owned by form with archiveReference {archiveReference}");
                throw;
            }
        }
    }
}