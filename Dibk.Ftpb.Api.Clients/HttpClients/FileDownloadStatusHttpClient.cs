﻿using Dibk.Ftpb.Api.Models;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Api.Clients.HttpClients
{
    /// <summary>
    /// Client for adding fileDownloadStatus to a form
    /// </summary>
    public class FileDownloadStatusHttpClient : BaseHttpClient
    {
        /// <summary>
        /// Creates a new client
        /// </summary>
        /// <param name="httpClientFactory"></param>
        /// <param name="logger"></param>
        public FileDownloadStatusHttpClient(IHttpClientFactory httpClientFactory, ILogger<FileDownloadStatusHttpClient> logger) : base(httpClientFactory, logger)
        { }

        /// <summary>
        /// <inheritdoc cref="CreateAsync(FileDownloadStatusApiModel, CancellationToken)"/>
        /// </summary>
        /// <param name="fileDownloadStatus"></param>
        /// <returns></returns>
        public async Task<FileDownloadStatusApiModel> CreateAsync(FileDownloadStatusApiModel fileDownloadStatus)
        {
            return await CreateAsync(fileDownloadStatus, CancellationToken.None);
        }

        /// <summary>
        /// Creates a fileDownloadStatus for a form
        /// </summary>
        /// <param name="fileDownloadStatus"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <exception cref="HttpRequestException">NotFound if the form isn't found. BadRequest if the fileDownloadStatus is invalid</exception>
        public async Task<FileDownloadStatusApiModel> CreateAsync(FileDownloadStatusApiModel fileDownloadStatus, CancellationToken cancellationToken)
        {
            if (fileDownloadStatus == null)
                throw new ArgumentNullException(nameof(fileDownloadStatus));

            var requestUri = $"api/formlogdata/{fileDownloadStatus.ArchiveReference}/filedownloads";
            var json = JsonSerializer.Serialize(fileDownloadStatus);
            var stringContent = new StringContent(json, Encoding.UTF8, "application/json");

            try
            {
                using var result = await _httpClient.PostAsync(requestUri, stringContent, cancellationToken);
                result.EnsureSuccess();

                var jsonString = await result.Content.ReadAsStringAsync();

                return ApiJsonSerializer.Deserialize<FileDownloadStatusApiModel>(jsonString);
            }
            catch (HttpRequestException hre)
            {
                if (hre.GetStatusCode() == System.Net.HttpStatusCode.NotFound)
                {
                    _logger.LogError(hre, $"Unable to create fileDownloadStatus for archiveReference {fileDownloadStatus?.ArchiveReference}");
                    throw;
                }

                if (hre.GetStatusCode() == System.Net.HttpStatusCode.BadRequest)
                {
                    _logger.LogError(hre, $"Unable to create fileDownloadStatus for archiveReference {fileDownloadStatus?.ArchiveReference}");
                    throw;
                }

                _logger.LogError(hre, $"Unexpected error occurred when creating fileDownloadStatus for archiveReference {fileDownloadStatus?.ArchiveReference}");
                throw;
            }
        }

        /// <summary>
        /// <inheritdoc cref="UpdateAsync(FileDownloadStatusApiModel, CancellationToken)"/>
        /// </summary>
        /// <param name="fileDownloadStatus"></param>
        /// <returns></returns>
        public async Task UpdateAsync(FileDownloadStatusApiModel fileDownloadStatus)
        {
            await UpdateAsync(fileDownloadStatus, CancellationToken.None);
        }

        /// <summary>
        /// Updates a fileDownloadStatus for a form
        /// </summary>
        /// <param name="fileDownloadStatus"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <exception cref="HttpRequestException">NotFound if the form isn't found. BadRequest if the fileDownloadStatus is invalid</exception>"
        public async Task UpdateAsync(FileDownloadStatusApiModel fileDownloadStatus, CancellationToken cancellationToken)
        {
            if (fileDownloadStatus == null)
                throw new ArgumentNullException(nameof(fileDownloadStatus));

            var requestUri = $"api/formlogdata/{fileDownloadStatus.ArchiveReference}/filedownloads";
            var json = JsonSerializer.Serialize(fileDownloadStatus);
            var stringContent = new StringContent(json, Encoding.UTF8, "application/json");

            try
            {
                using var result = await _httpClient.PutAsync(requestUri, stringContent, cancellationToken);
                result.EnsureSuccess();
            }
            catch (HttpRequestException hre)
            {
                if (hre.GetStatusCode() == System.Net.HttpStatusCode.NotFound)
                {
                    _logger.LogError(hre, $"Unable to update fileDownloadStatus for archiveReference {fileDownloadStatus?.ArchiveReference}");
                    throw;
                }

                _logger.LogError(hre, $"Unexpected error occurred when updating fileDownloadStatus for archiveReference {fileDownloadStatus?.ArchiveReference}");
                throw;
            }
        }

        /// <summary>
        /// <inheritdoc cref="GetAsync(string)"/>
        /// </summary>
        /// <param name="archiveReference"></param>
        /// <returns></returns>
        public async Task<List<FileDownloadStatusApiModel>?> GetAsync(string archiveReference)
        {
            return await GetAsync(archiveReference, CancellationToken.None);
        }

        /// <summary>
        /// Get fileDownloadStatus for a form
        /// </summary>
        /// <param name="archiveReference"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <exception cref="HttpRequestException">NotFound if the form isn't found</exception>
        public async Task<List<FileDownloadStatusApiModel>?> GetAsync(string archiveReference, CancellationToken cancellationToken)
        {
            if (string.IsNullOrWhiteSpace(archiveReference))
                throw new ArgumentNullException(nameof(archiveReference));

            var requestUri = $"api/formlogdata/{archiveReference}/filedownloads";

            try
            {
                using var result = await _httpClient.GetAsync(requestUri, cancellationToken);

                result.EnsureSuccess();

                var content = await result.Content.ReadAsStringAsync();

                return ApiJsonSerializer.Deserialize<List<FileDownloadStatusApiModel>>(content);
            }
            catch (HttpRequestException hre)
            {
                if (hre.GetStatusCode() == System.Net.HttpStatusCode.NotFound)
                {
                    _logger.LogError(hre, $"Unable to get fileDownloadStatus for archiveReference {archiveReference}");
                    throw;
                }

                _logger.LogError(hre, $"Unexpected error occurred when getting fileDownloadStatus for archiveReference {archiveReference}");
                throw;
            }
        }
    }
}