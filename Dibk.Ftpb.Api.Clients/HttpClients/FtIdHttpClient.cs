﻿using Microsoft.Extensions.Logging;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Api.Clients.HttpClients
{
    /// <summary>
    /// Client for attaching FtId to an archive reference
    /// </summary>
    public class FtIdHttpClient : BaseHttpClient
    {
        /// <summary>
        /// Creates a new client
        /// </summary>
        /// <param name="httpClientFactory"></param>
        /// <param name="logger"></param>
        public FtIdHttpClient(IHttpClientFactory httpClientFactory, ILogger<FtIdHttpClient> logger) : base(httpClientFactory, logger)
        { }

        /// <summary>
        /// <inheritdoc cref="AddFtId(string, string, CancellationToken)"/>
        /// </summary>
        /// <param name="archiveReference"></param>
        /// <param name="ftId"></param>
        /// <returns></returns>
        public async Task AddFtId(string archiveReference, string ftId)
        {
            await AddFtId(archiveReference, ftId, CancellationToken.None);
        }

        /// <summary>
        /// Logs ftId to the database
        /// </summary>
        /// <param name="archiveReference"></param>
        /// <param name="ftId"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task AddFtId(string archiveReference, string ftId, CancellationToken cancellationToken)
        {
            var requestUri = "api/ftId/attach";

            try
            {
                var payload = new AttachFtIdApiModel
                {
                    FtId = ftId,
                    ArchiveReference = archiveReference
                };

                var json = JsonSerializer.Serialize(payload);
                var stringContent = new StringContent(json, Encoding.UTF8, "application/json");

                using var result = await _httpClient.PostAsync(requestUri, stringContent, cancellationToken);
                result.EnsureSuccess();
            }
            catch (HttpRequestException hre)
            {
                if (hre.GetStatusCode() == System.Net.HttpStatusCode.BadRequest)
                {
                    _logger.LogError(hre, $"Unable to attach FtId {ftId} to archive reference {archiveReference}");
                    throw;
                }

                _logger.LogError(hre, $"Unexpected error occurred when attaching FtId {ftId} to archive reference {archiveReference}");
                throw;
            }
        }
    }

    internal class AttachFtIdApiModel
    {
        public string FtId { get; set; }
        public string ArchiveReference { get; set; }
    }
}