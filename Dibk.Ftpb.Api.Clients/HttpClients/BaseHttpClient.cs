﻿using Microsoft.Extensions.Logging;
using System.Net.Http;

namespace Dibk.Ftpb.Api.Clients.HttpClients
{
    /// <summary>
    /// Base class for Alfa-API http clients
    /// </summary>
    public class BaseHttpClient
    {
        private readonly IHttpClientFactory _httpClientFactory;
        protected HttpClient _httpClient => _httpClientFactory.CreateClient("ftpbApiClient");
        protected ILogger _logger { get; set; }

        /// <summary>
        /// Creates a new client
        /// </summary>
        /// <param name="httpClientFactory"></param>
        /// <param name="logger"></param>
        public BaseHttpClient(IHttpClientFactory httpClientFactory, ILogger logger)
        {
            _httpClientFactory = httpClientFactory;
            _logger = logger;
        }
    }
}