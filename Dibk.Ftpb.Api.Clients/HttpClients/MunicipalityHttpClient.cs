﻿using Dibk.Ftpb.Api.Models;
using Microsoft.Extensions.Logging;
using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Api.Clients.HttpClients
{
    /// <summary>
    /// Client for retrieving information about a municipality
    /// </summary>
    public class MunicipalityHttpClient : BaseHttpClient
    {
        /// <summary>
        /// Creates a new client
        /// </summary>
        /// <param name="httpClientFactory"></param>
        /// <param name="logger"></param>
        public MunicipalityHttpClient(IHttpClientFactory httpClientFactory, ILogger<MunicipalityHttpClient> logger) : base(httpClientFactory, logger)
        { }

        /// <summary>
        /// <inheritdoc cref="GetAsync(string, CancellationToken)"/>
        /// </summary>
        /// <param name="municipalityCode"></param>
        /// <returns></returns>
        public async Task<MunicipalityApiModel> GetAsync(string municipalityCode)
        {
            return await GetAsync(municipalityCode, CancellationToken.None);
        }

        /// <summary>
        /// Retrieves information about a municipality
        /// </summary>
        /// <param name="municipalityCode"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        public async Task<MunicipalityApiModel> GetAsync(string municipalityCode, CancellationToken cancellationToken)
        {
            if (string.IsNullOrEmpty(municipalityCode))
                throw new ArgumentNullException(nameof(municipalityCode));

            try
            {
                var requestUri = $"api/municipalities/{municipalityCode}";

                using var result = await _httpClient.GetAsync(requestUri, cancellationToken);
                result.EnsureSuccess();

                var content = await result.Content.ReadAsStringAsync();

                return ApiJsonSerializer.Deserialize<MunicipalityApiModel>(content);
            }
            catch (HttpRequestException hre)
            {
                if (hre.GetStatusCode() == System.Net.HttpStatusCode.NotFound)
                {
                    _logger.LogError(hre, $"Unable retreive municipality for code {municipalityCode}");
                    throw;
                }

                _logger.LogError(hre, $"Unexpected error occurred when retreiving municipality for code {municipalityCode}");
                throw;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "An error occurred while trying to get municipality with code {MunicipalityCode}", municipalityCode);
                throw;
            }
        }
    }
}