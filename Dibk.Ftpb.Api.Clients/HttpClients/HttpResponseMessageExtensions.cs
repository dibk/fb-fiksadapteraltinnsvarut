﻿using System.Net;
using System.Net.Http;

namespace Dibk.Ftpb.Api.Clients.HttpClients
{
    /// <summary>
    /// Extension to Set status code on HttpRequestException
    /// </summary>
    public static class HttpResponseMessageExtensions
    {
        /// <summary>
        /// Makes it get the statusCode from the HttpResponseMessage via the exception
        /// </summary>
        /// <param name="httpResponseMessage"></param>
        public static void EnsureSuccess(this HttpResponseMessage httpResponseMessage)
        {
            try
            {
                httpResponseMessage.EnsureSuccessStatusCode();
            }
            catch (HttpRequestException ex) when (ex.SetStatusCode(httpResponseMessage.StatusCode))
            {
                // Intentionally left empty. Will never be reached.
            }
        }
    }

    /// <summary>
    /// Extensions for HttpRequestException
    /// </summary>
    public static class HttpRequestExceptionExtensions
    {
        private const string StatusCodeKeyName = "StatusCode";

        /// <summary>
        /// Set the status code on the HttpRequestException
        /// </summary>
        /// <param name="httpRequestException"></param>
        /// <param name="httpStatusCode"></param>
        /// <returns></returns>
        public static bool SetStatusCode(this HttpRequestException httpRequestException, HttpStatusCode httpStatusCode)
        {
            httpRequestException.Data[StatusCodeKeyName] = httpStatusCode;

            return false;
        }

        /// <summary>
        /// Get the status code from the HttpRequestException
        /// </summary>
        /// <param name="httpRequestException"></param>
        /// <returns></returns>
        public static HttpStatusCode? GetStatusCode(this HttpRequestException httpRequestException)
        {
            return (HttpStatusCode?)httpRequestException.Data[StatusCodeKeyName];
        }
    }
}