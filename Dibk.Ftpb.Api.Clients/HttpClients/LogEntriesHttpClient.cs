﻿using Dibk.Ftpb.Api.Models;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Api.Clients.HttpClients
{
    /// <summary>
    /// Client for adding log entries to Alfa logs
    /// </summary>
    public class LogEntriesHttpClient : BaseHttpClient
    {
        /// <summary>
        /// Creates a new client
        /// </summary>
        /// <param name="httpClientFactory"></param>
        /// <param name="logger"></param>
        public LogEntriesHttpClient(IHttpClientFactory httpClientFactory, ILogger<LogEntriesHttpClient> logger) : base(httpClientFactory, logger)
        { }

        /// <summary>
        /// <inheritdoc cref="CreateAsync(string, IEnumerable{LogEntryApiModel}, CancellationToken)"/>
        /// </summary>
        /// <param name="archiveReference"></param>
        /// <param name="logEntries"></param>
        /// <returns></returns>
        public async Task CreateAsync(string archiveReference, IEnumerable<LogEntryApiModel> logEntries)
        {
            await CreateAsync(archiveReference, logEntries, CancellationToken.None);
        }

        /// <summary>
        /// Adds log entries to Alfa logs
        /// </summary>
        /// <param name="archiveReference"></param>
        /// <param name="logEntries"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        public async Task CreateAsync(string archiveReference, IEnumerable<LogEntryApiModel> logEntries, CancellationToken cancellationToken)
        {
            if (string.IsNullOrEmpty(archiveReference))
                throw new ArgumentNullException(nameof(archiveReference));

            if (logEntries == null)
                throw new ArgumentNullException(nameof(logEntries));

            var requestUri = $"api/formlogdata/{archiveReference}/logentries";
            var json = JsonSerializer.Serialize(logEntries);
            var stringContent = new StringContent(json, Encoding.UTF8, "application/json");

            try
            {
                using var result = await _httpClient.PostAsync(requestUri, stringContent, cancellationToken);
                result.EnsureSuccess();
            }
            catch (HttpRequestException hre)
            {
                if (hre.GetStatusCode() == System.Net.HttpStatusCode.NotFound)
                {
                    _logger.LogError(hre, $"Unable to add log entries for archiveReference {archiveReference} since it doesn't exist");
                    throw;
                }

                _logger.LogError(hre, $"Unexpected error occurred adding log entries for archiveReference {archiveReference}");
                throw;
            }
        }
    }
}