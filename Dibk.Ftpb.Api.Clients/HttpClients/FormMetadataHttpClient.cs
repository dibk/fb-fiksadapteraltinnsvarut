﻿using Dibk.Ftpb.Api.Models;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Api.Clients.HttpClients
{
    /// <summary>
    /// Client for performing CRUD operations on formmetadata
    /// </summary>
    public class FormMetadataHttpClient : BaseHttpClient
    {
        /// <summary>
        /// Creates a new client
        /// </summary>
        /// <param name="httpClientFactory"></param>
        /// <param name="logger"></param>
        public FormMetadataHttpClient(IHttpClientFactory httpClientFactory, ILogger<FormMetadataHttpClient> logger) : base(httpClientFactory, logger)
        { }

        /// <summary>
        /// <inheritdoc cref="GetAsync(string, CancellationToken)"/>
        /// </summary>
        /// <param name="archiveReference"></param>
        /// <returns></returns>
        public async Task<FormMetadataApiModel> GetAsync(string archiveReference)
        { return await GetAsync(archiveReference, CancellationToken.None); }

        /// <summary>
        /// Get formmetadata for an archiveReference
        /// </summary>
        /// <param name="archiveReference"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        public async Task<FormMetadataApiModel> GetAsync(string archiveReference, CancellationToken cancellationToken)
        {
            if (string.IsNullOrEmpty(archiveReference))
                throw new ArgumentNullException(nameof(archiveReference));

            var requestUri = $"api/formlogdata/{archiveReference}/formmetadata";

            try
            {
                using var result = await _httpClient.GetAsync(requestUri, cancellationToken);
                result.EnsureSuccess();

                var content = await result.Content.ReadAsStringAsync();

                var retVal = ApiJsonSerializer.Deserialize<FormMetadataApiModel>(content);

                return retVal;
            }
            catch (HttpRequestException hre)
            {
                if (hre.GetStatusCode() == System.Net.HttpStatusCode.NotFound)
                {
                    _logger.LogError(hre, $"Unable to get formmetadata for archiveReference {archiveReference}");
                    throw;
                }

                _logger.LogError(hre, $"Unexpected error occurred when getting formmetadata for archiveReference {archiveReference}");
                throw;
            }
        }

        /// <summary>
        /// <inheritdoc cref="CreateAsync(FormMetadataApiModel, CancellationToken)"/>
        /// </summary>
        /// <param name="formMetadata"></param>
        /// <returns></returns>
        public async Task CreateAsync(FormMetadataApiModel formMetadata)
        {
            await CreateAsync(formMetadata, CancellationToken.None);
        }

        /// <summary>
        /// Create formmetadata
        /// </summary>
        /// <param name="formMetadata"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        public async Task CreateAsync(FormMetadataApiModel formMetadata, CancellationToken cancellationToken)
        {
            if (formMetadata == null)
                throw new ArgumentNullException(nameof(formMetadata));

            var requestUri = $"api/formlogdata/formmetadata";
            var json = JsonSerializer.Serialize(formMetadata);
            var stringContent = new StringContent(json, Encoding.UTF8, "application/json");

            try
            {
                using var result = await _httpClient.PostAsync(requestUri, stringContent, cancellationToken);
                result.EnsureSuccess();
            }
            catch (HttpRequestException hre)
            {
                if (hre.GetStatusCode() == System.Net.HttpStatusCode.Conflict)
                {
                    _logger.LogError(hre, $"Unable to create formmetadata for archiveReference {formMetadata?.ArchiveReference} since it already exists.");
                    throw;
                }

                _logger.LogError(hre, $"Unexpected error occurred when creating formmetadata for archiveReference {formMetadata?.ArchiveReference}");
                throw;
            }
        }

        /// <summary>
        /// <inheritdoc cref="UpdateAsync(FormMetadataApiModel, CancellationToken)"/>
        /// </summary>
        /// <param name="formMetadata"></param>
        /// <returns></returns>
        public async Task UpdateAsync(FormMetadataApiModel formMetadata)
        {
            await UpdateAsync(formMetadata, CancellationToken.None);
        }

        /// <summary>
        /// Updates formmetadata
        /// </summary>
        /// <param name="formMetadata"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        public async Task UpdateAsync(FormMetadataApiModel formMetadata, CancellationToken cancellationToken)
        {
            if (formMetadata == null)
                throw new ArgumentNullException(nameof(formMetadata));

            var requestUri = $"api/formlogdata/{formMetadata.ArchiveReference}/formmetadata";
            var json = JsonSerializer.Serialize(formMetadata);
            var stringContent = new StringContent(json, Encoding.UTF8, "application/json");

            try
            {
                using var result = await _httpClient.PutAsync(requestUri, stringContent, cancellationToken);
                result.EnsureSuccess();
            }
            catch (HttpRequestException hre)
            {
                if (hre.GetStatusCode() == System.Net.HttpStatusCode.NotFound)
                {
                    _logger.LogError(hre, $"Unable to update formmetadata for archiveReference {formMetadata?.ArchiveReference}");
                    throw;
                }

                _logger.LogError(hre, $"Unexpected error occurred when updating formmetadata with archiveReference {formMetadata?.ArchiveReference}");
                throw;
            }
        }

        /// <summary>
        /// <inheritdoc cref="AddAttachmentMetadataAsync(string, List{FormAttachmentMetadataApiModel}, CancellationToken)"/>
        /// </summary>
        /// <param name="archiveReference"></param>
        /// <param name="attachmentMetadata"></param>
        /// <returns></returns>
        public async Task AddAttachmentMetadataAsync(string archiveReference, List<FormAttachmentMetadataApiModel> attachmentMetadata)
        {
            await AddAttachmentMetadataAsync(archiveReference, attachmentMetadata, CancellationToken.None);
        }

        /// <summary>
        /// Adds attachment metadata to a form
        /// </summary>
        /// <param name="archiveReference"></param>
        /// <param name="attachmentMetadata"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        public async Task AddAttachmentMetadataAsync(string archiveReference, List<FormAttachmentMetadataApiModel> attachmentMetadata, CancellationToken cancellationToken)
        {
            if (string.IsNullOrEmpty(archiveReference))
                throw new ArgumentNullException(nameof(archiveReference));

            if (attachmentMetadata == null || attachmentMetadata.Count == 0)
                throw new ArgumentNullException(nameof(attachmentMetadata));

            var requestUri = $"api/formlogdata/{archiveReference}/formmetadata/attachments";
            var json = JsonSerializer.Serialize(attachmentMetadata);
            var stringContent = new StringContent(json, Encoding.UTF8, "application/json");

            try
            {
                using var result = await _httpClient.PostAsync(requestUri, stringContent, cancellationToken);
                result.EnsureSuccess();
            }
            catch (HttpRequestException hre)
            {
                if (hre.GetStatusCode() == System.Net.HttpStatusCode.NotFound)
                {
                    _logger.LogError(hre, $"Unable to add attachment metadata for archiveReference {archiveReference}");
                    throw;
                }

                _logger.LogError(hre, $"Unexpected error occurred when adding attachment metadata for archiveReference {archiveReference}");
                throw;
            }
        }
    }
}